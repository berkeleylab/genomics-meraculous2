#!/bin/bash

if [ $# -lt 1 ]
then
  echo "Usage: $0 <destination> " >&2
  exit 0
fi

installdir=`readlink -f $1`

if [ -d ./build ]
then
    echo "Error:"
    echo "Older build found in the current directory. Please delete/rename it before running the installer"
    exit 0
fi

mkdir $installdir
mkdir  build
cd build
cmake -DCMAKE_INSTALL_PREFIX=${installdir} -DCMAKE_BUILD_TYPE=Release ..
make VERBOSE=1 2>&1 | tee ${installdir}/make.log
make install
cd ..

