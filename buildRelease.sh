#!/bin/bash
set -e

DEST=$1
if [ -d .git ]
then
  MERAC_VERSION=$(git describe --tag)
  echo ${MERAC_VERSION} >MERAC_VERSION
fi
MERAC_VERSION=$(cat MERAC_VERSION)
MYCODE=$(pwd)

cd $DEST
buildname=Meraculous-${MERAC_VERSION}
echo "Building ${buildname}"
rm -rf ${buildname}

set -x
git clone ${MYCODE} ${buildname}
cp -p ${MYCODE}/MERAC_VERSION ${buildname}
rm -rf ${buildname}/.git
tar=${DEST}/${buildname}.tar.gz
tar -czf ${tar} ${buildname}
set +x

rm -rf ${buildname}
echo "Release is in ${tar}"

