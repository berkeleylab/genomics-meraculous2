#!/bin/bash -l

module purge
module load PrgEnv-gnu/7.1
module load perl
module load boost/1.63.0

set -ex

cd build
make install
cd ..
cp .genepool_deploy/module_dependencies $PREFIX/.deps
