#!/bin/bash -l

module purge
module load PrgEnv-gnu/4.9
module load perl
module load boost/1.57.0

set -ex

if [ -e build ]
then 
    chmod -R u+w build
    rm -r build
fi


mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=${PREFIX} -DCMAKE_BUILD_TYPE=Release ..
make



