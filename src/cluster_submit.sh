#!/bin/bash


## Author: Eugene Goltsman <egoltsman@lbl.gov>
## DOE Joint Genome Institute
##
## This script builds and executes a qsub command to submit a task-array job to a SLURM or SGE-like cluster.
## It acts as an abstraction layer between Meraculous code and an actual cluster software. This allows 
## users to customize the submition model without modifying the main code base.
##
##
##  


set -e



if [[ $# -eq 0 ]] ; then
    echo 'Usage:  $0 -n <name> -o <out_dir> -p <project> -r <ram_req> -s <n_slots_per_task> -t <n_tasks> -w <wallclock_req> [ -R <full_node_Reserv> | -S <overSubscribe>  <template_script>'  
    exit 0
fi


####  This configuration is geared for a submission of a job array to a SLURM or an SGE-type cluster.  Modify this script to fit your cluster environment.


## environment variables to pass along
env_vars="MERACULOUS_ROOT"

submit_bin=""
opts_const=""
opts=""

if [[ $SLURM_ROOT ]]; then
    submit_bin="sbatch"
    opts_const="--export=$env_vars -D $cwd "
    opts+=$sbatch_opts_const
elif  [[ $SGE_ROOT ]]; then
    submit_bin="qsub"
    opts_const=" -v $env_vars -cwd -r n -b n -S /bin/bash -j y"
    opts+=$qsub_opts_const
else
    echo "Error: no cluster scheduler software found!" >&2
    exit 1
    
fi


##  The command line options are supplied by Meraculous, but you can control if and how they get passed to the submitter

pe="pe_slots"

project=
name=
out_dir=
n_tasks=
n_slots=
ram_req=
wclk_req=



if [[ $SGE_ROOT ]]; then
    
    while getopts e:n:o:p:r:s:t:w:RS opt; do

	case $opt in
	    n)  # name for the job
		name=$OPTARG
		opts+=" -N $name"
		;;
	    o)  # default stdout directory - this is where uncaptured stdout plus any cluster scheduler logs will go
		out_dir=$OPTARG
		opts+=" -o $out_dir"
		;;
	    e)  # default stderr directory - this is where uncaptured stderr will go
		out_dir=$OPTARG
		opts+=" -e $out_dir"
		;;
	    p)  # project name
		project=$OPTARG
		opts+=" -P $project"
		;;
	    r)  # cluster ram request (will be applied *per slot* if n_slots is set
		ram_req=$OPTARG
		opts+=" -l ram.c=$ram_req -l h_vmem=$ram_req"
		;;
	    s)  # number of slots requested per task
		n_slots=$OPTARG
		opts+=" -pe $pe $n_slots -R y"
		;;
	    t)  # number of parallel tasks in the job
		n_tasks=$OPTARG
		opts+=" -t 1-$n_tasks"
		;;
	    w)  # wallclock request
		wclk_req=$OPTARG
		opts+=" -l h_rt=$wclk_req"
		;;
	    S)  # no effect
		;;
	    R)  # full node reservation
		opts+=" -R y"
		;;

	esac
    
    done

elif [[ $SLURM_ROOT ]]; then 

    while getopts e:n:o:p:r:s:t:w:RS opt; do

	case $opt in
	    n)  # name for the job
		name=$OPTARG
		opts+=" -J $name"
		;;
	    o)  # default stdout directory - this is where uncaptured stdout plus any cluster scheduler logs will go
		out_dir=$OPTARG
		opts+=" --workdir=$out_dir"
		;;
	    e)  # default stderr directory - this is where uncaptured stderr will go
		# same as stdout
		;;
	    p)  # project name
		proj=$OPTARG
		opts+=" --wckey=$proj"
		;;
	    r)  # cluster ram request 
		ram_req=$OPTARG
		;;
	    s)  # number of slots requested per task
		n_slots=$OPTARG
		;;
	    t)  # number of parallel tasks in the job
		n_tasks=$OPTARG
		opts+=" --array=1-$n_tasks -N1"
		;;
	    w)  # wallclock request
		wclk_req=$OPTARG
		opts+=" --time=$wclk_req"
		;;
	    S)  # share (obersubscribe) node with other users/jobs, resources permitting  
		opts+=" --oversubscribe"  #(not compatible with --exclusive)
		;;
	    R)  # fulld node reservation   
		opts+=" --exclusive" # (not compatible with --oversubscribe)
		;;

	esac
    done
    # memory request changes it's meaning for multi-threaded jobs
    if [[ $n_slots -gt 1 ]]; then
	opts+=" -c $n_slots  --mem-per-cpu=${ram_req}" 
    else
	opts+=" --mem=${ram_req}"
    fi
fi


shift $((OPTIND - 1))

template_script=$1

submit_cmd="$submit_bin $opts $template_script"
echo "$submit_cmd" >&2


jobID=

if [[ $SGE_ROOT ]]; then
    jobID=`$submit_cmd | awk '{print $3}'`
elif [[ $SLURM_ROOT ]]; then 
    jobID=`$submit_cmd | awk '{print $4}'`
fi

if [[ ! $jobID =~ ^[0-9]* ]]
then
    echo "Error: Could not get back a valid job id!" >&2
    exit 1
fi

# the resulting job id must be returned as evidence of successful scheduling;  it will be used by Meraculous for monitoring the job's progress
echo -e "$jobID"


