srf=$1
contigsIn=$2
contigsOut=$3
module load samtools
samtools faidx $contigsIn
cp $contigsIn $contigsOut
grep "\.cp" $srf | cut -f 3 | sed 's|[+-]||g' | sed -r 's|(Contig[0-9]*)\.cp([0-9]*)|\1\t\1.cp\2|g' > copyContigs.list
awk -v c=$contigsIn '{system("samtools faidx "c" "$1 "| sed \"s/"$1"/"$2"/g\" ") }' copyContigs.list >> $contigsOut
