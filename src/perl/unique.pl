#!/usr/bin/env perl

use warnings;

$n_args = @ARGV;
if (($n_args != 1) && ($n_args != 2) && ($n_args != 3)) {
    print "usage: unique.pl <input file> <<column>> <<sum column?>>\n";
    exit;
}

open(F,$ARGV[0]) || die "Couldn't open file $ARGV[0]\n";
my $use_col = -1;

if ($n_args > 1) {
    $use_col = $ARGV[1] - 1;
    if ($use_col < 0) {
	die "error: column specification must be >= 1\n";
    }
}

my %sums = ();
my $do_sum = 0;
my $sum_col = 0;
if ($n_args == 3) {
    $do_sum = 1;
    $sum_col = $ARGV[2] - 1;
}

my %attempts = ();

while (my $i = <F>) {
    chomp $i;

    my $val = 0;
    if ($use_col != -1) {
	my @cols = split(/\s+/,$i);
#	my @cols = split(/\t/,$i);
	my $n_cols = @cols;
	if ($use_col+1 > $n_cols) {
	    warn "Warning: column specification exceeds number of columns in line: $i\n";
	    next;
	}
	$i = $cols[$use_col];
	if ($do_sum == 1) {
	    $val = $cols[$sum_col];
	} 
    }

    if (exists($attempts{$i})) {
	$attempts{$i} += 1;
	if ($do_sum == 1) {
	    $sums{$i} += $val;
	}
    } else {
	$attempts{$i} = 1;
	if ($do_sum == 1) {
	    $sums{$i} = $val;
	}
    }
}
close F;

while (my ($id, $count) = each(%attempts)) {
    if ($do_sum == 1) {
	my $sum = $sums{$id};
	print "$id\t$count\t$sum\n";
    } else {
	print "$id\t$count\n";
    }
}


