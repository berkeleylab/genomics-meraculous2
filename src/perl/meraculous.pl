#!/usr/bin/env perl
#
# meraculous.pl - drives the meraculous genome assembler

=pod
Overall logging and error handling strategy:
----------------------------------------------

As much as possible, errors are logged to a Log object to ensure consistancy.
Exceptions to this occur when creating these logging objects - in which case an error is 
sent via print statement

Errors found at startup are printed to stdout via the startup_log() object, and any die 
statements are caught via the startup eval block.
Currently, we just exit, which causes the an END block to be run.


After startup, i.e during the main stage loop, errors are detected both through eval/die 
statements and return codes such as JGI_FAILURE & JGI_SUCCESS constants.

=cut


###################################################################
# Module Inclusions                                               #
###################################################################

#
# Enforce good programming style.
#
use strict;

#
# Turn on lots of warnings.
#
use warnings;

#
# Include system modules
#
use POSIX qw (ceil time difftime);
use File::Basename;    # for splitting filnamees
use File::Copy;
use File::Path;
use Getopt::Long;      #for processing command line options.
use Data::Dumper;      # for dumping tables to a file
use Cwd;
use Sys::Hostname;
use Time::HiRes;      #for high-resolution time functions
use Text::Wrap;

#
# Include JGI modules.
#
use lib "$ENV{MERACULOUS_ROOT}/lib";
use M_Constants;
use M_Utility;
use M_Job_Set;
use M_Generic_Config;
use M_LibData;
use Log::Log4perl qw(:easy);


# runtime root directory
my $release_root = $ENV{'MERACULOUS_ROOT'};

# current perl interpreter for local perl scripts.  
#my $perl = $^X;

my $SCRIPT_ID = "meraculous.pl";
my $VERSION_ID = "Version 2.2.6";
my $AUTHOR_ID = "Eugene Goltsman, Isaac Ho";

if (@ARGV == 0) 
{
   script_usage();
}


use constant INPUT_SUMMARY_FILE => "INPUT_REPORT.txt";

use constant SET_IN_STONE_PARAMS => qw/
                             mer_size
			     num_prefix_blocks 
                             local_num_procs
                             cluster_slots_per_task
                             /;


# objects for loading local and user parameters
my $pUserParams;
my $pLocalParams;
my $pLibInfo;

# log objects
my $startup_log;
my $pLog;


# variables for command line argumets 
our $assembly_dir;
my $g_restarting;
my $g_resuming;
my $g_stage;
my $start_stage;
my $stop_stage;
my $cleanupLevel = 1;
my $debug = 1;  # temporarily hardcoded


# stage names
my @stages = (
    "meraculous_import",
    "meraculous_mercount",
    "meraculous_mergraph",
    "meraculous_ufx",
    "meraculous_contigs",
    "meraculous_bubble",
    "meraculous_merblast",
    "meraculous_ono",
    "meraculous_gap_closure",
    "meraculous_final_results"
    );



#### END block here catches any innapropriate exit() calls made from external modules during startup,
#### They should be die statements.
END
{    
    if ($?)
    {
	print STDOUT "meraculous run exited prematurely: $?\n";
    }
}

###################################################################
#                                                                 #
#  The Main Script Start                                          #
#                                                                 #
###################################################################

{
  
  # A string to identify this function.
  my $function_id = "main";
  
  # user-defined config file
  my $userParamsFileName;

  # auto-set internal parameters
  my $localParamsFileName;

  # optional default config file (in case of redundant params, those in user-supplied file will take precedence)
  my $defaultParamsFileName = "$ENV{HOME}/.meraculous.conf";

  # parameter definition files
  my $userDefFileName  = "meraculous.params.defs";
  my $localDefFileName = "meraculous.local.params.defs";

  #
  # Set the umask value to standardize the manner in which files are
  # created.
  #
  my $previous_umask = umask(002);

  #
  # Create a startup logging object
  #
  Log::Log4perl->easy_init( $INFO );
  $startup_log = get_logger();

  if ( !defined($startup_log) )
  {
     print "Error: Cannot create startup log object\n";
    die;
  }

  my $cmdLine = $0;
  foreach my $arg( @ARGV )
  {
    $cmdLine .= " ".$arg;
  }

  my $pid = $$;
  my $host = hostname();
  my $run_pids_history = "run_pids.history";  # will be used to check current run pid against to make sure we don't have parallel runs overwriting each other
  my $remote_job_sentinel = JOB_SET_REMOTE_SENTINEL; # will be used to check if there are runaway jobs still running on the cluster
  my $clusterDeleteJobBin = CLUSTER_DELETE_JOB_EXEC;
  my $run_start_time = [Time::HiRes::gettimeofday()];


  eval          # now that we have a log object, catch all exceptions up until the main loop.
  {
      
    my $pwd = cwd();

    #
    # Additional sanity checks on the runtime root dir
    #

    if ( ! -e $release_root )
    {
	$startup_log->error("Error: The directory defined as Meraculous runtime root directory does't exist." );
	die;
    }
    if ($release_root !~ /\/$/) { $release_root .= '/';}    #add final / since it is a path
    $startup_log->info("Meraculous runtime root directory: $release_root\n");


    #
    # Get the command line options
    #

    # variables to be restricted to the startup eval block
      
    my $label;
    my $checkpoint_dir = "checkpoints";
    my ( $stepping, $archive);
    my ( $resume, $restart );  
  
    unless (
      &GetOptions(
        'c|config=s', \$userParamsFileName,
        'label=s',    \$label,
        'restart',  \$restart,
        'resume',   \$resume,
        'start=s',    \$start_stage,
        'stop=s',     \$stop_stage,
        'step',       \$stepping,
        'archive',    \$archive,
#	'debug',      \$debug,
	'dir=s',      \$assembly_dir,
	'cleanup_level=i', \$cleanupLevel,
        'h|help',     \&script_usage,
        'v|version',  \&script_version
	)
	)
    {
	$startup_log->error("ERROR: Unable to process the command line arguments");
	script_usage();
	die;
    }
      
    
    # Check cmd line options

    if ( defined($resume) && defined($restart) )
    {
      $startup_log->error("ERROR: Cannot use both -resume and -restart");
      die;
    }

    if ( defined($start_stage) && ! defined($restart) )
    {
      $startup_log->error("ERROR: Cannot use -start without -restart");
      die;
    }

    if ( defined($start_stage) && defined($resume)  )
    {
      $startup_log->error("ERROR: Cannot use -start with -resume");
      die;
    }

    if ( defined($archive) && ! defined($restart) )
    {
      $startup_log->error("ERROR: Cannot use -archive without -restart");
      die;
    }

    if ( (defined($resume) || defined($restart)) && defined($label) )
    {
      $startup_log->error("ERROR: Cannot use -label when restarting or resuming;  Use -dir if you want to specify a particular run");
      die;
    }

    if ( ($cleanupLevel > 2) || ($cleanupLevel < 0) )
    {
      $startup_log->error("ERROR: User parameter -cleanup_level must be between 0 and 2");
      die;
    }
	  
      
    # translate these user command-line params which may be undefined into global flags that are always 0 or 1
    $g_resuming = ( defined( $resume ) );
    $g_restarting = ( defined( $restart ) );


    # use the user-specified params file
    if ( not defined($userParamsFileName) )
    {
      $startup_log->error("ERROR: User config file (-c) must be specified");
      die;
    }

    #
    # Make sure user config file exists
    #
    if ( !-e $userParamsFileName )
    {
      $startup_log->error("ERROR: Config file: $userParamsFileName not found");
      die;
    }

    #
    # Look for parameter definition files in the local directory and if not found use the default ones in the install path
    #
    if ( !-e "./$userDefFileName" )
    {
	$userDefFileName = $release_root . "etc/meraculous/" . $userDefFileName;
    }      
    if ( !-e "./$localDefFileName" )
    {
	$localDefFileName = $release_root . "etc/meraculous/" . $localDefFileName;
    }      

    {
	foreach my $def_file ( $userDefFileName, $localDefFileName )
	{
	    if ( !-e $def_file )
	    {
		$startup_log->error("ERROR: Parameter definition file: $def_file not found!");
		die;
	    }
	}
    }
      
    #
    #  Create user parameters object and load baseline user parameters
    #
    $startup_log->info("Loading run parameters from $userParamsFileName ...\n");
    $pUserParams = M_Generic_Config->new( $userDefFileName, $startup_log, $userParamsFileName );
    if ( !defined $pUserParams || $@ )
    {
	$startup_log->error("Error: Cannot load user parameters!");
	die;
    }
    if (-e $defaultParamsFileName )
    {
	$startup_log->info("Loading global defaults from $defaultParamsFileName ...\n");
	open(D,"<$defaultParamsFileName");
	while(<D>) 
	{ 
	    chomp;  s/^\s+//;  s/\s+$//;
	    next unless length; 
	    next if ($_ =~ /^#/);
	    my ($param, $value) = split /\s+/, $_; 
	    #add defaults to the user params object which will be used from here on
	    unless ( exists( ${$pUserParams}{$param} ) )	    {
		$pUserParams->put_param($param, $value, $startup_log);
	    }	    
	}
	close D;
    }
    
    #
    # Set up directories and checkpoints
    #

    if ( not defined($stop_stage))
    {
	$stop_stage = last_stage();
    }
   
    validate_stage_name( $start_stage, $startup_log );
    validate_stage_name( $stop_stage,  $startup_log );

    if ( $g_resuming || $g_restarting )  
    {

      $startup_log->info("Setting the stage for resume/restart..");

      if ( ! defined($assembly_dir) ) 
      {
	$startup_log->error("ERROR: Could not determine the latest run directory.  Please specify explicity with '-dir'");
	die;
      }

      if ( $assembly_dir !~ /^\// )    #  make it absolute
      {
	  $assembly_dir = "$pwd/$assembly_dir"; 
      }

      if ( not -d $assembly_dir )
      {
        $startup_log->error("ERROR: $assembly_dir is not a valid resume/restart directory");
        die;
      }

      # Check that none of the previously started runs are still running, then record the PID of this one 
      open (PIDS, "<$assembly_dir/$run_pids_history") || die $startup_log->error("Could not open file $run_pids_history");  
      while (<PIDS>)
      {
	  chomp;
	  my $oldPID = $_;
	  my $exists = kill 0, $oldPID;
	  if ($exists) 
	  { 
	      $startup_log->error("ERROR: An older execution of this run is still running (PID $oldPID). Cannot continue.");
	      die;
	  }
      }
      close PIDS;   
      `echo $pid >> $assembly_dir/$run_pids_history`;

      # Check that the last job submitted by the previous run is in fact no longer running on the cluster
      if ( my @f = glob("$assembly_dir/*/$remote_job_sentinel") )
      {
	  my $orphan_jid = `cat $f[0]`;  #there should be only one sentinel file
	  chomp $orphan_jid;
	  $startup_log->error("ERROR: Found evidence of a runaway cluster job (Job ID $orphan_jid). Please kill this job before any resume/restart attempts!\
                               In the future, please use the run-term script to safely terminate a run");
	  unlink $f[0];
	  die;
      }

      $checkpoint_dir = "$assembly_dir/".$checkpoint_dir;

      ### Setup start stage      
      my $last_failed_stage;    # stage at which last run failed

      determine_checkpoint( \@stages, $checkpoint_dir, \$last_failed_stage, $startup_log );
      if ( not defined($start_stage) or stage_is_after( $start_stage, $last_failed_stage ) )
      {                         #user did not specify or specified a stage that has not run yet
        $start_stage = $last_failed_stage;
      }
      if ( stage_is_after($start_stage, $stop_stage) )
      {
	  $startup_log->error("ERROR: Stop stage ($stop_stage) can't precede the start stage ($start_stage). When not explicitly set, the start stage is determined based on the latest .chkpt file.");
	  die;
      }
      $localParamsFileName = "$checkpoint_dir/$start_stage.local.params";


      if ($g_restarting)   ### Cleanup any old stage directories, checkpoints, and recreate the local params file
      {    

        cleanup_for_restart($start_stage, $assembly_dir, $checkpoint_dir, $archive, $startup_log);
	system("touch $localParamsFileName");
	if ($?) {die $startup_log->error("could not initialize local parameter file $localParamsFileName: ($?)")}


	# if there are previous stages, inherit the local params from the last local.params file
	if ( defined (stage_before($start_stage)) )
	{
	    my $prior_localParamFile = "$checkpoint_dir/".stage_before($start_stage).".local.params";
	    $pLocalParams = M_Generic_Config->new( $localDefFileName, $startup_log, $prior_localParamFile );
	    # reset and update resume checkpoints
	    $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "none", $startup_log );
	}
	# otherwise initialize the local params file
	else
	{
	    # Record parameters that should remain unchanged throughout the run.
	    $pLocalParams = M_Generic_Config->new( $localDefFileName, $startup_log, $localParamsFileName );
	    foreach (SET_IN_STONE_PARAMS)
	    {
		my $corePrmValue = $pUserParams->get_param( $_, $startup_log );
		if ( defined($corePrmValue) ) { $pLocalParams->put_param($_, $corePrmValue, $startup_log ) }
	    }	  
	}
      } 
      else   ### we're resuming, so load from the existing current local params file
      {
	  $pLocalParams = M_Generic_Config->new( $localDefFileName, $startup_log, $localParamsFileName );
      }

      #verify that the core parameters have not been changed by the user since the last attempt
      foreach ( SET_IN_STONE_PARAMS )
      {
	  my $corePrmValue_USER = $pUserParams->get_param( $_, $startup_log );
	  my $corePrmValue_LOCAL = $pLocalParams->get_param( $_, $startup_log );
	  if ( defined $corePrmValue_LOCAL && ($corePrmValue_USER  ne  $corePrmValue_LOCAL))
	  {
	      die $startup_log->error("Error: Parameter $_ cannot be changed in the middle of the run!");		    
	  }
      }

    }


    else     #### a new run
    {
      $startup_log->info("Setting the stage for the new run..");
      $start_stage = first_stage(); 

      $label = create_label( $pUserParams, $startup_log ) if ( ! defined($label) );

      ### Create a timestamp-based working directory, if not specfied on command line
      if ( defined( $assembly_dir ) && ( $assembly_dir !~ /^\// ) )  { $assembly_dir = "$pwd/$assembly_dir" }
      else { $assembly_dir = create_assembly_directory_name($label) }
      system("mkdir -p $assembly_dir");
      if ($?) {die $startup_log->error("could not create directory $assembly_dir: ($?)")}

      `echo $pid >> $assembly_dir/$run_pids_history`;

      ### Setup checkpoint directory
      $checkpoint_dir = "$assembly_dir/".$checkpoint_dir;
      mkdir "$checkpoint_dir";

      ### Create local parameters object
      $localParamsFileName = "$checkpoint_dir/$start_stage.local.params";
      system("touch $localParamsFileName");
      if ($?) {die $startup_log->error("could not initialize local parameter file $localParamsFileName: ($?)")}

      $pLocalParams = M_Generic_Config->new( $localDefFileName, $startup_log, $localParamsFileName );
      $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "none", $startup_log ); 

      ### Record parameters that should remain unchanged throughout the run. User may try to change them in the user params file
      ### but will be slapped if the value is different in the local params file
      foreach (SET_IN_STONE_PARAMS)
      {
	  my $corePrmValue = $pUserParams->get_param( $_, $startup_log );
	  if ( defined($corePrmValue) ) { $pLocalParams->put_param($_, $corePrmValue, $startup_log ) }
      }
    }    # end of else (not a previous run)


    #
    # Create library info object which is a derivative of library-specific parameters stored in pUserParams and pLocalParams.
    #
    $pLibInfo = M_LibData->new($pUserParams, $pLocalParams, $startup_log);
    if ( !defined $pLibInfo || $@ )
    {
	$startup_log->error("Error: Cannot load library info!");
	die;
    }


    #
    # Find the appropriate Meraculous and Perl directories and update local params file with these values
    #
#    if ( compute_platform_specific_paths( $pLocalParams, $pUserParams, $startup_log ) != JGI_SUCCESS )
#    {
#	$startup_log->error("could not compute platform specific paths!");
#	die;
#    }


    if ( defined($stepping) )
    {
	$stop_stage = $start_stage;
    }

  };    #end of startup eval block


  #goes to END block above if uncaught exception during startu
  if ($@)
  {
    print STDERR $@;
    exit(JGI_FAILURE);
  }

  #
  # Setup running log directory
  #

  my $logDir = "$assembly_dir/log";

  $startup_log->info("Prep work complete. From here on will channel all messages to appropriate logs ...\n");
  $startup_log->info("Logging to $logDir/ ...\n\n");
  
  # create running log object  
  my $conf_ref = \qq (
      
      # Default main logger to record everything to appropriate log files using the specified appenders
      log4perl.logger                          = INFO, rootlogInfo, rootlogDebug, rootlogErrors, rootlogWarn, Screen

      ### Custom filters for separating debug, info, warn, error to different files

      # Filter to match levels ERROR to FATAL
      log4perl.filter.MatchError = Log::Log4perl::Filter::LevelRange
#      log4perl.filter.MatchError.LevelMin      = ERROR
      log4perl.filter.MatchError.LevelMin      = WARN
      log4perl.filter.MatchError.LevelMax      = FATAL
      log4perl.filter.MatchError.AcceptOnMatch = true

      # Filter to match level WARN
      log4perl.filter.MatchWarn  = Log::Log4perl::Filter::LevelMatch
      log4perl.filter.MatchWarn.LevelToMatch  = WARN
      log4perl.filter.MatchWarn.AcceptOnMatch = true

      # Filter to match level INFO
      log4perl.filter.MatchInfo  = Log::Log4perl::Filter::LevelMatch
      log4perl.filter.MatchInfo.LevelToMatch  = INFO
      log4perl.filter.MatchInfo.AcceptOnMatch = true

      # screen output appender  
      log4perl.appender.Screen                  = Log::Log4perl::Appender::ScreenColoredLevels
      log4perl.appender.Screen.stderr           = 1
      log4perl.appender.Screen.Filter           = MatchError
      log4perl.appender.Screen.layout           = Log::Log4perl::Layout::PatternLayout
      log4perl.appender.Screen.layout.ConversionPattern = %d %F{1} %M %L> %m %n

      # root level debug log appender:  uncategorized loggers, plus anything with level INFO or higher end up here too
      log4perl.appender.rootlogDebug            = Log::Log4perl::Appender::File
      log4perl.appender.rootlogDebug.mkdir      = sub { mkdir '$logDir' unless(-e '$logDir') }
      log4perl.appender.rootlogDebug.filename   = $logDir/meraculous.log
      log4perl.appender.rootlogDebug.layout     = Log::Log4perl::Layout::PatternLayout
      log4perl.appender.rootlogDebug.layout.ConversionPattern = %n %d %F{1} %M %L> %m %n

      # root level error log appender
      log4perl.appender.rootlogErrors           = Log::Log4perl::Appender::File
      log4perl.appender.rootlogErrors.mkdir     = sub { mkdir '$logDir' unless(-e '$logDir') }
      log4perl.appender.rootlogErrors.filename  = $logDir/errors.log
      log4perl.appender.rootlogErrors.Filter    = MatchError
      log4perl.appender.rootlogErrors.layout    = Log::Log4perl::Layout::PatternLayout
      log4perl.appender.rootlogErrors.layout.ConversionPattern = %d %F{1} %M %L> %m %n %n

      # root level warn log appender
      log4perl.appender.rootlogWarn            = Log::Log4perl::Appender::File
      log4perl.appender.rootlogWarn.mkdir      = sub { mkdir '$logDir' unless(-e '$logDir') }
      log4perl.appender.rootlogWarn.Filter     = MatchWarn
      log4perl.appender.rootlogWarn.filename   = $logDir/warn.log
      log4perl.appender.rootlogWarn.layout     = Log::Log4perl::Layout::PatternLayout
      log4perl.appender.rootlogWarn.layout.ConversionPattern = %d %F{1} %M %L> %m %n

      # root level info log appender
      log4perl.appender.rootlogInfo            = Log::Log4perl::Appender::File
      log4perl.appender.rootlogInfo.mkdir      = sub { mkdir '$logDir' unless(-e '$logDir') }
      log4perl.appender.rootlogInfo.filename   = $logDir/info.log
      log4perl.appender.rootlogInfo.Filter     = MatchInfo
      log4perl.appender.rootlogInfo.layout     = Log::Log4perl::Layout::PatternLayout
      log4perl.appender.rootlogInfo.layout.ConversionPattern = %d %F{1} %M %L> %m %n
      ); 

  Log::Log4perl->init( $conf_ref );
  $pLog = Log::Log4perl->get_logger("main");
  if ( !defined($pLog) )
  {
    $startup_log->error("Error: Cannot create main logging object!\n");
    die;
  }

  undef $startup_log;

  if ( defined($debug) )  {
    $pLog->level($DEBUG);  # messages of type $pLog->debug() and higher will be printed
  }

  # save a copy of the current user parameter file for future generations
  my $date = `date +%Y%m%d-%H%M%S`; 
  $pUserParams->config_print_all( $pLog, "$logDir/".basename($userParamsFileName).".bak.$date" );


  # write out some convienience scripts for controlling this run
  my %signal_map = ( 'term'      , "TERM",
		     'kill'      , "-",
		     'pause'     , "STOP",
		     'resume'    , "CONT",
                 );


  while (my ($script,$signal) = each %signal_map)
  {      
    $script = "$assembly_dir/run-$script";
    open ( SCRIPT, '>', $script) or warn "Can't open $script: $!";

    if (($signal eq "TERM" || $script =~ /kill$/) && $pUserParams->get_param( USER_PARAM_use_cluster,  $pLog ))
    {  
     # if terminating a run in a cluster mode, first qdel any remote jobs
	print SCRIPT 
	    "if ls $assembly_dir/*/$remote_job_sentinel &> /dev/null\n".
	    "then \n".
	    "\tactiveRemoteJobID=`cat $assembly_dir/*/$remote_job_sentinel` \n".
	    "\t $clusterDeleteJobBin \$activeRemoteJobID \n".
	    "\trm $assembly_dir/*/$remote_job_sentinel \n".
	    "fi \n"
    }
    print SCRIPT "kill -$signal -$$ \n";
    close SCRIPT;
    system("chmod a+x $script");
  }  
  
  # catch signals when these scripts are called or when the process is terminated externally  (except for the KILL signal which we can't catch)
  $SIG{INT} = $SIG{TERM} = sub{ warn "Caught Zap!\n"; $pLog->info( "Caught an external INT/TERM signal" ); exit };

  # redirect stdout and stderr to a log file
  open( my $oldout, ">&STDOUT" ) or warn "Can't dup STDOUT: $!";    #save old stdout
  open( STDOUT, '>>', "$logDir/stdout.dump" ) or warn "Can't redirect STDOUT: $!";
# open( STDERR, ">&STDOUT" ) or warn "Can't dup STDOUT: $!";

  $pLog->info("$SCRIPT_ID, $VERSION_ID");

  if ($g_resuming)  {
      $pLog->info( "\n\n\n<<<<<<<<<<<<<<<<<RESUMING>>>>>>>>>>>>>>>>>>>" );
  }
  elsif ($g_restarting)  {
      $pLog->info( "\n\n\n<<<<<<<<<<<<<<<<<RESTARTING>>>>>>>>>>>>>>>>>>>" );
  }
  else {
      $pLog->info( "<<<<<<<<<<<<<<<<<START OF RUN>>>>>>>>>>>>>>>>>>>" );
  }

  # log the server name
  $pLog->info( "Local server: $host" );

  # write command-line params to log
  $pLog->info( "Command Line: ".$cmdLine );

  # log PID
  $pLog->info( "Native PID: $pid" );

  # log the parameters
  my $userParamsString = "User parameters:\n";
  while (my ($key, $value) = each (%{$pUserParams}))   {
      #skip boilerplate object items which are typically capitalized
      next if ($key =~ /^[A-Z]/);
      #if the value is a hash ref, print the contents
      if (ref($value) eq 'HASH') { $userParamsString .= "\t$key:\t$value->{$_}\n" for keys %{$value} }
      else                       { $userParamsString .= "\t$key:\t$value\n" }
  }
  $pLog->info("$userParamsString");

  if (($g_restarting) || ($g_resuming))
  {
      my $localParamsString = "Local parameters current at resume/restart instance:\n";
      $localParamsString .= `cat $localParamsFileName`;
      $pLog->info("$localParamsString");
  } 

  # save current directory
  my $saved_dir = cwd();



  #
  # Finally, execute all stages
  #

  my $status = JGI_SUCCESS;

  for ( $g_stage = $start_stage ; defined($g_stage) ; $g_stage = stage_after($g_stage) )
  {
    $pLog->warn( "RUNNING STAGE: $g_stage" );

    ## create a local params file for a possible mid-stage resume
    write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );

    my $stage_dir = "$assembly_dir/$g_stage";
    ## create stage directory if it doesn't exist
    system("mkdir -p $stage_dir");
    chdir("$stage_dir") or die $pLog->error("Error: Can't cd into $stage_dir ($?)");
    my $args = '($stage_dir)';
    my $stage_start_time = [Time::HiRes::gettimeofday()];


    ## check inputs for stage
    my $check_input_function = "check_inputs_stage_$g_stage";    # create function name
    $status = eval $check_input_function;
    if ( $@ || $status != JGI_SUCCESS )                        # $@: a die statement was caught
    {
      $status = JGI_FAILURE;
      $pLog->error("ERROR: Missing or invalid inputs for stage $g_stage!\n$@");
      last;
    }

    ## R U N   T H E   S T A G E
    my $stage_function = "run_$g_stage";    # define function name
    $status = eval $stage_function . $args;

    my $stage_total_time = Time::HiRes::tv_interval($stage_start_time);

    if ( $@ || $status != JGI_SUCCESS )    # $@: a die statement was caught
    {
      $status = JGI_FAILURE;
      $pLog->error("Stage $g_stage failed \($stage_total_time seconds\)\n$@");
      last;
    }

    ## check outputs for stage
    my $check_output_function = "check_outputs_stage_$g_stage"; 
    $status = eval $check_output_function . $args;
    if ( $@ || $status != JGI_SUCCESS )                          # $@: a die statement was caught
    {
      $status = JGI_FAILURE;
      $pLog->error("Outputs for stage $g_stage not valid.\n$@");
      last;
    }

    ##create checkpoint for stage
    if ( create_checkpoint( $g_stage, "$assembly_dir/checkpoints", $pLog ) != JGI_SUCCESS )
    {
      $status = JGI_FAILURE;
      $pLog->error("Checkpoint creation failed!");
      last;
    }
    
    unless ($cleanupLevel == 0)
    {
	my $cleanup_stage_function = \&{"cleanup_stage_$g_stage"};
	if ( $cleanup_stage_function->($stage_dir) != JGI_SUCCESS )
	{
	    $status = JGI_FAILURE;
	    $pLog->error("Stage cleanup failed!");
	    last;
	}
    }

    ## unset any stage-specific resume checkpoints 
    $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "none", $pLog ); 

    ## update the current parms file
    write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );

    $pLog->info("STAGE COMPLETED: $g_stage \($stage_total_time seconds\)\n");
    last if ( $g_stage eq $stop_stage );
  }
  
  #restore original directory
  chdir $saved_dir;
  # restore STDOUT
  open( STDOUT, ">&", $oldout );    


  if ( $status == JGI_SUCCESS )
  {
    $pLog->warn( "RUN COMPLETED SUCCESSFULLY !" );
  }
  else
  {
      $pLog->warn( "RUN DID NOT COMPLETE !" );
  }

  my $total_time = Time::HiRes::tv_interval($run_start_time);
  $pLog->warn( "Total run time: $total_time seconds." );

  $pLog->warn( "Thank you for running Meraculous!" );

  undef $g_stage;

  #
  # Restore the old umask value.
  #
  umask($previous_umask);

  exit($status);
}


######################## End of main script #############################




#------------------------------------------------------------------------
# STAGES
#------------------------------------------------------------------------


### Stage: meraculous_import
#----------------------------
# The purpose of this stage is to:
# - organize input files with links that are catalogued by libprefix
# - provide a way to easily map each of these files with a library name/insert size info
# - validate the format and the pairing scheme of the input sequence files
# - divide the input into parallelizable chunks
# - generate human-readable stats of the input dataset based on a sub-sampling of reads
# - create a load-balanced set of prefix blocks based on mer frequecies in a sub-sampling of reads
#
# All input fastqs are linked as "meraculous_import/source_reads/$libPrefix.fastq.$i"
#
sub check_inputs_stage_meraculous_import
{
    return JGI_SUCCESS;
}
sub run_meraculous_import
{
  my ($work_dir) = @_;
  my $cmd; my $out; my $err;
  my @commands = ();
  my @stdouts = ();
  my @stderrs = ();
  my @workingDirs = ();

  # executables
  my $splitNvalidate =      $release_root.PATH_SPLIT_READS;
  my $loadBalanceMercount = $release_root.PATH_LOAD_BALANCE_MERCOUNT;
  my $mercounter =          $release_root.PATH_MERCOUNTER_56;

  # user parameters
  my $mer_size        = $pUserParams->get_param( USER_PARAM_mer_size,     $pLog );
  my $skip_validation = $pUserParams->get_param( USER_PARAM_no_read_validation, $pLog );
  my $genome_size     = $pUserParams->get_param( USER_PARAM_genome_size,     $pLog );
  my $numPrefixBlocks = $pUserParams->get_param( USER_PARAM_num_prefix_blocks, $pLog )|| 1;

  my $threads       = ( $pUserParams->get_param( USER_PARAM_use_cluster, $pLog ) ? 
			$pUserParams->get_param( USER_PARAM_cluster_slots_per_task, $pLog ) :
			$pUserParams->get_param( USER_PARAM_local_num_procs, $pLog ) ) 
                     || 1;

  my $min_mer_size = 11;
  if ($mer_size < $min_mer_size)  {
      $pLog->error("Error in user parameters:  K-mer size ($mer_size) is smaller than the minumum allowed ($min_mer_size) !");
      return JGI_FAILURE;
  }
  if (0 == $mer_size % 2)  {
      $pLog->error("Error in user parameters: K-mer size ($mer_size) must be an odd integer !");
      return JGI_FAILURE;
  }

  my $samplingRate = 0.001;   

  # use a specially compiled mercounter for long mers
  $mercounter = $release_root.PATH_MERCOUNTER_128  if ($mer_size > 56);

  my @libsDone = ();  
#  my $skeletonLib;

  if ( $g_resuming )
  {
      my $localResumeCheckpoint = $pLocalParams->get_param( LOCAL_PARAM_local_resume_checkpoint, $pLog );

      # use the hard record of detected quality offsets as a signal that the library has been processed
      # (we don't need the offset values here since they've already been loaded into the libInfo object).
      my @dummy;
      $pLocalParams->get_key_values( $pLog, LOCAL_PARAM_lib_qoffset, \@libsDone, \@dummy);
      goto $localResumeCheckpoint unless  ($localResumeCheckpoint eq "none");
  }

  ### begin parsing sequence files
  $pLog->info("Parsing library info and sequence files...");

  foreach my $libName ( @{ $pLibInfo->get_lib_names($pLog)} )
  {
      $pLog->info("Processing library $libName ...");
      if ( $g_resuming  && ($libName ~~ @libsDone) )  # check that we haven't already processed this library during prior attempts
      {
	  $pLog->info("Skipping library $libName - already processed...");
	  next;
      }

      if ( $libName !~ /^[A-Z0-9]*$/)
      {
	  $pLog->error("Error in user parameters: Invalid library name $libName :  only digits and capital letters are allowed.");
	  return JGI_FAILURE;
      }

      my $libAvgReadLen = $pLibInfo->get_lib_prop($libName, LIB_PROP_readLen, $pLog);

      my $libDownsampleRate  = $pLibInfo->get_lib_prop($libName, LIB_PROP_downsampleRate, $pLog );

      if ( $libDownsampleRate > 1.0) 
      {
	  $pLog->error("Error in user parameters: Invalid downsampling rate:  must be between 0 and 1");
          return JGI_FAILURE;
      }


      
#      if ( $libName =~ /^PBIO/)  # once we have full pb support will replace this with a true lib_seq argument
#      {
#	  $pLog->info("Warning: Library $libName is recognized as a PacBio library and will be used only during scaffolding. All library flags will be ignored!");
#	  $skeletonLib=1;
#      }
#    if ( ! $skeletonLib && $libAvgReadLen > 500)
#    {
#	$pLog->error("Error in user parameters: Read length ($libAvgReadLen) is above the currently supported cutoff of 500 bp).");
#	return JGI_FAILURE;
#    }    


      if ($libAvgReadLen < $mer_size) 
      {
	  $pLog->error("Error in user parameters: Read length cannot be smaller than the selected k-mer size!  Please set to appropriate value and restart!");
	  return JGI_FAILURE;
      }
    
      my $filesOrWildcards = $pLibInfo->get_lib_prop($libName, LIB_PROP_fastqs, $pLog);

      $filesOrWildcards =~ s/^\s+//;
      $filesOrWildcards =~ s/\s+$//;
      my @w =  split( /\,/, $filesOrWildcards); 
      foreach(@w)
      {
	  if ( $_ !~ /^\// )    # if a relative path,  assume the location is the same as that of the run folder;  make it absolute
	  {
	      $_ = "$assembly_dir/../$_";    
	  }
	  system("ls $_ > /dev/null 2>&1");
	  if ($?)
	  {
	      $pLog->error("Error in user parameters:  No files found matching $_") ;
	      return JGI_FAILURE; 
	  }
	  
      }
      
      # pick random file from the list defined by the wildcard and get the q-score offset value for the whole lib based on it
      my @f =  glob( $w[0] );
      my $randomFile = $f[rand @f];
      my $qOffset;
      unless (get_qscore_offset( $randomFile, \$qOffset, $pLog) == JGI_SUCCESS)
      {
	  $pLog->error("ERROR: Could not determine a valid qscore offset from file $randomFile");
	  return JGI_FAILURE;
      }

      # save the offset value for this lib  as pLibInfo property and also as a local param to be used later 
      $pLibInfo->set_lib_prop($libName, LIB_PROP_qOffset, $qOffset, $pLog);
      $pLocalParams->put_param( LOCAL_PARAM_lib_qoffset, "$libName $qOffset", $pLog );
      

      # create systematically named symlinks to the source fastq files
      my @links;
      my $links_dir = "$work_dir/source_reads";
      create_source_links($libName, \@w, \@links, $links_dir, $pLog);
      unless (@links)
      {
	  $pLog->error("ERROR: Could not create symlinks to the data specified by the user");
	  return JGI_FAILURE;
      }

      # if ( $skeletonLib)  
      # {
      # 	  # don't do any splitting/sampling, link directly to original file
      # 	  my $x=0;
      # 	  foreach my $link ( @links )
      # 	  {
      # 	      if ($link =~ /[\.gz|\.bz2|\.z]$/) 
      # 	      { 
      # 		  $pLog->error("ERROR: Compressed fastq files are not allowed for skeleton libraries at this point");
      # 		  return JGI_FAILURE;
      # 	      }
      # 	      $cmd = "ln -s $link $work_dir/${libName}.fastq.$x ";
      # 	      run_single_cmd( $cmd, undef, undef, undef, $pLog);
      # 	  }	
      # 	  undef $skeletonLib;
      # 	  next;
      # }


      ### split each input file or pair, validate read names, and take a sampling


      my $readsPerChunk = int( 5E+08 / $libAvgReadLen );   # shoot for ~500mb of total sequence per chunk
      if ($readsPerChunk % 2) { $readsPerChunk++; }  # make sure it's an even number so that we don't split the last pair

      my $x=0;
      foreach my $linkOrPair ( @links )
      {
	  $cmd = "perl $splitNvalidate -f \"$linkOrPair\" -p $libName.fastq.$x  -r $samplingRate -s $readsPerChunk ";
	  $cmd = $cmd . " -N" if ($skip_validation); 
	  $cmd = $cmd . " -d $libDownsampleRate" if ($libDownsampleRate);
	  push( @commands, $cmd);
	  push( @stderrs, "$work_dir/$libName.splitNvalidate.$x.err" );
	  push( @workingDirs, "$work_dir" );
	  $x++;
      }
      run_command_set( "split-validate.$libName", $pUserParams, \@commands, undef, \@stderrs, \@workingDirs, $pLocalParams, $pLog, undef, undef, undef); 
      #update local params with this lib's info
      write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );
  }

  $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "split_complete", $pLog );
  write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );


 split_complete:


  ### build additional data structures for the Inputs Report using the sampling of reads

  $pLog->info("Generating read quality stats based on the sampling of reads ...");
  my $prefixerInfoFile = "$work_dir/prefixer.sample.info";
  open( SEQINFO, ">$prefixerInfoFile" );  # will be filled with properties of reads from a random sampling; will be used during prefix load balancing.

  open( INPUT_REPORT, ">$work_dir/".INPUT_SUMMARY_FILE );  # Human-readable input data summary.
  print INPUT_REPORT "== Input Data Summary ==\n ";
  print INPUT_REPORT "\n".
      sprintf("%12s%16s%12s%12s%22s%12s%12s ", "lib", "estNumReads","readLen","estTotalSeq","qAvg [ min - max ]","pctQ20Bases","estQ20Depth")."\n";

  foreach my $libName ( @{ $pLibInfo->get_lib_names($pLog)} )
  {
    my $libQOffset =  $pLibInfo->get_lib_prop($libName, LIB_PROP_qOffset, $pLog);
    my %fastqInfoHashForLib = ();

    my @sample_files = glob("$work_dir/$libName.fastq.*.sample");
    unless (@sample_files) {
	$pLog->error("ERROR: No .sample files found for library $libName"); 
	return JGI_FAILURE;
    }
    foreach my $fastqFileSample (@sample_files)
    {
	my $smpRate = $samplingRate;
	if ( -z $fastqFileSample ) {
	    $pLog->warn("Warning:  There were too few reads to generate a sampling of sufficient size. Will use the original fastq files for load balancing...");
	    $fastqFileSample = "$work_dir/source_reads/$libName.fastq.0_00001";
	    $smpRate = 1;
	}

	open(my $fh, "<$fastqFileSample") || die $pLog->error("Can't open file $fastqFileSample for reading");
	my $fastqInfoHashRefForSample = get_fastq_info( $fh, $libQOffset, $pLog);
	close $fh;
	unless ( $fastqInfoHashRefForSample->{ "numReads" } )  {
	    $pLog->error("Error: failed to obtain valid read info from $fastqFileSample!");
	    return JGI_FAILURE;
	}

	# you got the info for a sample, but you need to correct it to project
	#   for the whole fastq file (or pair of files)
	my $sourceNumReads += ( $fastqInfoHashRefForSample->{ "numReads" } / $smpRate ); # estimate original number of reads based on the sample size and sampling rate

	# adjust: numBases
	my $avgReadLen = $fastqInfoHashRefForSample->{ "numBases" } / $fastqInfoHashRefForSample->{ "numReads" }; 
        my $sourceNumBases = $sourceNumReads * $avgReadLen;

	# adjust: numQ20Bases
	my $sourceNumQ20Bases = $fastqInfoHashRefForSample->{ "numQ20Bases" } * ( $sourceNumReads / $fastqInfoHashRefForSample->{ "numReads" });

        # collate data
	foreach my $key( keys( %{ $fastqInfoHashRefForSample } ) )
	{
  	    if ( !exists( $fastqInfoHashForLib{ $key } ) )
	    {
		$fastqInfoHashForLib{ $key } = 0;
	    }

	    # now the key is guaranteed to be in the lib hash
	    # so, we must process according to the type of key
	    my $value = $fastqInfoHashRefForSample->{ $key };
	    if ( $key eq "numReads" )
	    {
		$fastqInfoHashForLib{ $key } += $sourceNumReads;
	    }
	    elsif ( $key eq "numBases" )
	    {
		$fastqInfoHashForLib{ $key } += $sourceNumBases;
	    }
	    elsif ( $key eq "numQ20Bases" )
	    {
		$fastqInfoHashForLib{ $key } += $sourceNumQ20Bases;
	    }
	    elsif ( $key eq "qMin" )
	    {
		if ( $value < $fastqInfoHashForLib{ $key } && $value > 0 )
		{
		    $fastqInfoHashForLib{ $key } = $value;
		}
	    }
	    elsif ( $key eq "qMax" )
	    {
		if ( $value > $fastqInfoHashForLib{ $key } )
		{
		    $fastqInfoHashForLib{ $key } = $value;
		}
	    }
	    elsif( $key eq "qAvg" )
	    {
		# we'll update this one later, once we know that all the keys which it depends on have been updated
	    }
	    else
	    {
		$fastqInfoHashForLib{ $key } += $value;
	    }
	}
	
	# now that we've updated the rest of the keys in the lib-hash, update the qAvg
	my $newAvg = $fastqInfoHashForLib{ "qAvg" } * ( ($fastqInfoHashForLib{ "numBases" } - $sourceNumBases) / $fastqInfoHashForLib{ "numBases" } )
	    + $fastqInfoHashRefForSample->{ "qAvg" } * ( $sourceNumBases / $fastqInfoHashForLib{ "numBases" } );
	$fastqInfoHashForLib{"qAvg"} = $newAvg;


	# if the library was earmarked for contig generation, add properties of this sample to the  prefix_balancing.sample.info file
	if ($pLibInfo->get_lib_prop($libName, LIB_PROP_forContigging, $pLog)) {
	    print SEQINFO "$fastqFileSample\tF\tU\t$libQOffset\n";
	}
    }

    print INPUT_REPORT 	sprintf( "%12s", $libName).
	sprintf( "%16d", $fastqInfoHashForLib{ "numReads" }).
	sprintf( "%12.1f", $fastqInfoHashForLib{ "numBases" } / $fastqInfoHashForLib{ "numReads" } ).
	sprintf( "%10.1f", $fastqInfoHashForLib{ "numBases" } / 1000000000 )."Gb".
	sprintf( "%8.1f", $fastqInfoHashForLib{ "qAvg" } )." [ ".
	sprintf( "%3s",$fastqInfoHashForLib{ "qMin" })." - ".
	sprintf( "%3s",$fastqInfoHashForLib{ "qMax" })." ]".
	sprintf( "%11.2f", 100.0 * $fastqInfoHashForLib{ "numQ20Bases" } / $fastqInfoHashForLib{ "numBases" } )."%".
	sprintf( "%12.1f", $fastqInfoHashForLib{ "numQ20Bases" } / 1000000000 / $genome_size  )."\n";

  }
 
  close INPUT_REPORT;
  close SEQINFO;


  # Use samples from qualifying libs for prefix balancing

  $pLog->info("Generating a load-balanced set of mer prefix blocks ...");

  if (! -s $prefixerInfoFile ) 
  {
      $pLog->error("ERROR: Can't perform load balancing:  $prefixerInfoFile is empty!   Please verify that at least one library is selected for contig generation. ");
      return JGI_FAILURE;
  }

  # mercount the sample
  my $sampleMercounts = "$work_dir/prefixer.sample.mercount";
  $cmd = "$mercounter $mer_size 1 $threads A+C+G+T $sampleMercounts $prefixerInfoFile 1";
  $err = "${sampleMercounts}.err";
  run_single_cmd( $cmd, $sampleMercounts, $err, 1, $pLog); 

  my $sampleMercountsSorted = "$work_dir/prefixer.sample.mercount.sorted";
  $cmd = "sort -S 2G -k 1,1 ${sampleMercounts}.*[0-9]";
  $err = "${sampleMercountsSorted}.err";
  run_single_cmd( $cmd, $sampleMercountsSorted, $err, 1, $pLog);

  # load balance the mercount
  my $prefixBlocksFilename = get_dna_prefix_list_filename( $assembly_dir );
  $cmd = "perl $loadBalanceMercount $sampleMercountsSorted $numPrefixBlocks";
  $err = "${prefixBlocksFilename}.err";
  run_single_cmd( $cmd, $prefixBlocksFilename, $err, 1, $pLog);

  return JGI_SUCCESS;
}

sub check_outputs_stage_meraculous_import
{
  return JGI_SUCCESS;
}

sub cleanup_stage_meraculous_import
{
    # this should still allow any stage to run or be redone
    my ($work_dir) = @_;
    my $touchfile = "$work_dir/CLEANED-UP.1";

    $pLog->info("Deleting intermediate files..");

    unlink glob "$work_dir/*.fastq.*[0-9].sample";
    unlink "$work_dir/prefixer.sample.info";
    unlink "$work_dir/prefixer.sample.mercount";
    unlink glob "$work_dir/prefixer.sample.mercount.*[0-9]";
    unlink "$work_dir/prefixer.sample.mercount.sorted";
    `touch $touchfile`;

    # if aggressive cleanup is selected we delete additional intermediate files that are not needed in the 
    # subsequent stages but which may be required if user wants to rerun this stage or a prior one.
    if ($cleanupLevel ==2)
    {
    }
    return JGI_SUCCESS;
}







### Stage: meraculous_mercount
#----------------------------
# The purpose of this stage is to:
# - count frequency of every k-mer found in the libraries selected for contig generation
# - build a depth histogram based on these frequencies to help evaluate the libraries and determine minimal depth cutoffs
#
# At the end of the stage there is a mercount.0.[ACTG] file for each block of prefixes, plus the total histogram of all k-mer frequencies
#
sub check_inputs_stage_meraculous_mercount
{
    if ($g_restarting)    {
	$pLog->info("Checking integrity of prior stages... ");
	if (is_stage_cleaned_up( (stage_before($g_stage)), $assembly_dir, $pLog)) { 
	    $pLog->error("Integrity check failed! ");
	    return JGI_FAILURE;
	}
	$pLog->info("ok");
    }
    return JGI_SUCCESS;
}
sub run_meraculous_mercount
{
  my ($work_dir) = @_;
  my $cmd; my $out; my $err;
  my @commands;
  my @stdouts;
  my @stderrs;
  my @workingDirs;

  my $mer_size      =   $pUserParams->get_param( USER_PARAM_mer_size,     $pLog )
                    ||   $pLocalParams->get_param( LOCAL_PARAM_autoselected_mer_size, $pLog );

  my $minMercountToReport         = $pUserParams->get_param( USER_PARAM_min_mercount_to_report, $pLog ) || 2;
  my $meraculous_min_depth_cutoff = $pUserParams->get_param( USER_PARAM_min_depth_cutoff, $pLog );
  my $diploid_mode                = $pUserParams->get_param( USER_PARAM_diploid_mode, $pLog ) || 0;

  my $threads       = ( $pUserParams->get_param( USER_PARAM_use_cluster, $pLog ) ? 
			$pUserParams->get_param( USER_PARAM_cluster_slots_per_task, $pLog ) :
			$pUserParams->get_param( USER_PARAM_local_num_procs, $pLog ) ) 
                     || 1;


  # stage inputs
  my @prefixes = get_dna_prefixes( get_dna_prefix_list_filename( $assembly_dir ), $pLog );

  # executables
  my $mercounter = ($mer_size <= 56 ? $release_root.PATH_MERCOUNTER_56 : $release_root.PATH_MERCOUNTER_128);
  my $randomList = $release_root . PATH_RANDOM_LIST2;
  my $histogram = $release_root . PATH_HISTOGRAM;
  my $unique =     $release_root.PATH_UNIQUE;
  my $findDmin =   $release_root.PATH_FINDDMIN;
  my $kha =        $release_root.PATH_KHA;



  if ( $g_resuming ) { 
      my $localResumeCheckpoint = $pLocalParams->get_param( LOCAL_PARAM_local_resume_checkpoint, $pLog );
      goto $localResumeCheckpoint unless  ($localResumeCheckpoint eq "none");
  } 

  my @fq_info_files;
  ### build fastq info files for reads that will be used for contigging
  foreach my $libName ( @{ $pLibInfo->get_lib_names($pLog)} )
  {
      next unless ( $pLibInfo->get_lib_prop($libName, LIB_PROP_forContigging, $pLog) )  ;
      my $libQOffset =  $pLibInfo->get_lib_prop($libName, LIB_PROP_qOffset, $pLog);
      push(@fq_info_files, generate_fastq_info_files($assembly_dir, $work_dir, $libName, 0, 0, $libQOffset, $pLog));
  }


  my $combinedFqInfoFile = "$work_dir/fastq.info";
  $cmd = "cat ".join(' ', @fq_info_files);
  $out = "$combinedFqInfoFile";
  run_single_cmd( $cmd, $out, undef, undef, $pLog);

  if ( -z $combinedFqInfoFile ) 
  {
      $pLog->error( "ERROR: Couldn't populate fastq info file!  Please check that at least one library is earmarked for contiging in the config file.");
      return JGI_FAILURE;
  }
  unlink @fq_info_files;

  $pLog->info("Generating mer counts...");

  foreach my $prefix_set( @prefixes )
  {
      my $prefixList = join('+',@{$prefix_set}) ;
      $cmd = "$mercounter $mer_size $minMercountToReport $threads $prefixList $work_dir/mercount.0.".$prefix_set->[0]." $combinedFqInfoFile 1"; 
      push( @commands, $cmd );
      # no stdouts;  each thread will create a separate output file
      push( @stderrs, "$work_dir/mercount.0.".$prefix_set->[0].".err" );
      push( @workingDirs, "$work_dir" );
  }
  run_command_set( "mercounter", $pUserParams, \@commands, undef, \@stderrs, \@workingDirs, $pLocalParams, $pLog, undef, undef, 1);

  $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "mercount_complete", $pLog );
    write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );

mercount_complete:

  $pLog->info("Generating mer depth histogram...");

### generate histogram from the mercounts

  run_single_cmd("mkdir -p $work_dir/hist.tmp", undef, undef, undef, $pLog );
  for( my $j=0; $j < scalar( @prefixes ); $j++ )  {
      push( @commands, "cat $work_dir/mercount.0.". $prefixes[ $j ]->[0]."*[0-9]" . "| perl $histogram - 2 1" );
      push( @stdouts, "$work_dir/hist.tmp/mercount.0.".$prefixes[ $j ]->[0].".hist" );
      push( @stderrs, "$work_dir/hist.tmp/mercount.0.".$prefixes[ $j ]->[0].".hist.err" );
      push( @workingDirs, "$work_dir" );
  }
  my $ram_request_override = 10;
  run_command_set( "mercountHistograms", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, $ram_request_override, \&validate_stdout, undef );


  $cmd = "cat $work_dir/hist.tmp/mercount.0.*.hist | grep \"|\" | perl $unique - 2 7 | sort -n -k 1 | awk '{print \$1,\$3}'";
  $out = "$work_dir/mercount.hist";
  $err = "$work_dir/mercount.hist.err";
  run_single_cmd( $cmd, $out, $err, 1, $pLog);

  $cmd = "echo \"set terminal png; set output '$work_dir/mercount.png'; set log y; set xlabel 'k-mer freq'; set ylabel 'nr. of distinct k-mers'; ".
          "plot [2:500] '$work_dir/mercount.hist' using 1:2 with boxes\" | gnuplot";
  $err = "$work_dir/gnuplot.err";
  run_single_cmd_OK_to_fail( $cmd, undef, $err, $pLog);
  
  unless ( $meraculous_min_depth_cutoff )
  {
    # perform automated min-depth detection
    $pLog->info("Performing automated min-depth cutoff detection...");
    $cmd = "perl $findDmin $work_dir/mercount.hist 2> mercount.dmin.err ";
    my $ret_val;
    if (run_local_cmd($cmd, \$ret_val, $pLog, \$meraculous_min_depth_cutoff) != JGI_SUCCESS)
    {
	$pLog->warn("WARNING: Couldn't determine min-depth cutoff based on distinct kmer frequency distribution.  Will try again based on total kmer occurrence...");
	$cmd = "cat $work_dir/mercount.hist | awk '{print \$1,\$2*\$1}'| perl $findDmin - 2> mercount.occurrence.dmin.err ";
	if (run_local_cmd($cmd, \$ret_val, $pLog, \$meraculous_min_depth_cutoff) != JGI_SUCCESS)
	{
	    return JGI_FAILURE;
	}
    }
    unless ( $meraculous_min_depth_cutoff > 1 && $meraculous_min_depth_cutoff < 100 )
    {
	$pLog->error("ERROR: findDmin returned illegal value of $meraculous_min_depth_cutoff. Examine the mercount.hist and try to determine and set min_depth_cutoff manually, then restart.");
	return JGI_FAILURE;
    }
    $pLog->info( "findDMin: setting to $meraculous_min_depth_cutoff" );
    $pLocalParams->put_param( LOCAL_PARAM_autocalibrated_min_depth_cutoff, $meraculous_min_depth_cutoff, $pLog );
  }


  # generate kha plot
  $cmd = "cat $work_dir/mercount.hist | awk '\$1 > $meraculous_min_depth_cutoff' | perl $kha " . ($diploid_mode ? "-S" : ""). " -h - ";
  $out = "$work_dir/kha.plot";
  run_single_cmd( $cmd, $out, undef, undef, $pLog );


  $cmd = "echo \"set terminal png; set output 'kha.png'; set log x; set xlabel 'k-mer depth / peak depth'; set ylabel '% k-mers'; plot 'kha.plot' using 1:2\" | gnuplot";
  $err = "$work_dir/gnuplot.err";
  run_single_cmd_OK_to_fail( $cmd, undef, $err, $pLog);


  return JGI_SUCCESS;
}

sub check_outputs_stage_meraculous_mercount
{
    my ($work_dir) = @_;

    my $date = `date +%Y%m%d-%H%M%S`;   
    my $mer_size     =   $pUserParams->get_param( USER_PARAM_mer_size,     $pLog )
                    ||   $pLocalParams->get_param( LOCAL_PARAM_autoselected_mer_size, $pLog );

    my $meraculous_min_depth_cutoff = $pUserParams->get_param( USER_PARAM_min_depth_cutoff, $pLog ) 
	                           || $pLocalParams->get_param( LOCAL_PARAM_autocalibrated_min_depth_cutoff, $pLog );

    open( H, "<$work_dir/mercount.hist" );

    my $volume = 0; # nr of kmers
    my $cntU = 0;  # nr of *unique* sequences of size k
    while (<H>) 
    {
	my @line = split(/\s+/, $_);
	next unless ($line[0] > $meraculous_min_depth_cutoff);
	$cntU += $line[1];
	$volume += ( $line[0]*$line[1] );
    }
    close H;
    my $merDepthWAvg = $volume / $cntU;

    my $stageValidFile = "$work_dir/stage_validation.$date";
    open( F, ">$stageValidFile" );

    print F "Total ${mer_size}-mers (over ${meraculous_min_depth_cutoff}x) : $volume\n";
    print F "Total unique sequeces (over ${meraculous_min_depth_cutoff}x) : $cntU\n";
    print F "Weighted average ${mer_size}-mer depth : $merDepthWAvg\n \n";
    close F;
    unless( $merDepthWAvg > 5) {    # require that we have at least 15x of mer depth
   	$pLog->error( "Stage validation failure:  Insufficient peak k-mer depth - must be at least 5x \n");
   	$pLog->error( "\n", `cat $stageValidFile`);
   	return JGI_FAILURE;
    }

    return JGI_SUCCESS;
}

sub cleanup_stage_meraculous_mercount
{
    # this should still allow any stage to run or be redone
    my ($work_dir) = @_;
    my $touchfile = "$work_dir/CLEANED-UP.1"; 
    $pLog->info("Deleting intermediate files..");
    rmtree "$work_dir/hist.tmp";
    `touch $touchfile`;

    # if aggressive cleanup is selected we delete additional intermediate files that are not needed in the
    # subsequent stages but which may be required if user wants to rerun this stage or a prior one.
    if ($cleanupLevel ==2)
    {
    }
    return JGI_SUCCESS;
}







### Stage: meraculous_mergraph
#----------------------------
# The purpose of this stage is to:
# - build a list of counts of all possible 1bp extensions for each mer
# - mers with frequency below meraculous_min_depth_cutoff are excluded
#
# The output is a list of mers with counts for every possile base extension on either side:  (ACTGN x 2 = 10 fields)
#
sub check_inputs_stage_meraculous_mergraph
{
    if ($g_restarting)    {
	$pLog->info("Checking integrity of prior stages... ");
	if (is_stage_cleaned_up( (stage_before($g_stage)), $assembly_dir, $pLog)) { 
	    $pLog->error("Integrity check failed! ");
	    return JGI_FAILURE;
	}
	$pLog->info("ok");
    }
    return JGI_SUCCESS;
}

sub run_meraculous_mergraph
{
  my ($work_dir) = @_;
  my $cmd;
  my @commands;
  my @stdouts;
  my @stderrs;
  my @workingDirs;

  my $mer_size      =   $pUserParams->get_param( USER_PARAM_mer_size,     $pLog )
                    ||   $pLocalParams->get_param( LOCAL_PARAM_autoselected_mer_size, $pLog );

  my $threads       = ( $pUserParams->get_param( USER_PARAM_use_cluster, $pLog ) ? 
			$pUserParams->get_param( USER_PARAM_cluster_slots_per_task, $pLog ) :
			$pUserParams->get_param( USER_PARAM_local_num_procs, $pLog ) ) 
                    || 1; 
  my $meraculous_min_depth_cutoff = $pUserParams->get_param( USER_PARAM_min_depth_cutoff, $pLog ) 
                                 || $pLocalParams->get_param( LOCAL_PARAM_autocalibrated_min_depth_cutoff, $pLog );


  # stage inputs

  my @prefixes = get_dna_prefixes( get_dna_prefix_list_filename( $assembly_dir ), $pLog );  #prefix sets
  my $fastqInfoFile = "$assembly_dir/".stage_before($g_stage)."/fastq.info";
  if ( ! -e $fastqInfoFile )     {
      $pLog->error( "ERROR: Couldn't find  $fastqInfoFile");
      return JGI_FAILURE;
  }

  # executables
  my $mergrapher = ($mer_size <= 56 ? $release_root.PATH_MERGRAPH_56 : $release_root.PATH_MERGRAPH_128);


  $pLog->info("Generating the mer graph...");

  foreach my $prefix_set( @prefixes )
  {
      my $prefixList = join('+',@{$prefix_set}) ; #individual prefixes
      my $mercountFile = "$assembly_dir/".stage_before($g_stage)."/mercount.0.".$prefix_set->[0];
      $cmd = "$mergrapher $mercountFile $mer_size  $meraculous_min_depth_cutoff $prefixList $threads 64 $work_dir/mergraph.0.".$prefix_set->[0]." $fastqInfoFile  2>  $work_dir/mergraph.0.".$prefix_set->[0].".err  &&  cat $work_dir/mergraph.0.".$prefix_set->[0].".*[0-9]";
      push( @commands, $cmd );
      push( @stdouts, "$work_dir/mergraph.0.".$prefix_set->[0] );
      push( @workingDirs, $work_dir );
      # since this is a multi-command, the stderr redirection that matters to us had to be written directly into
      # the first part of the command; any other stderr will be captured via default mechanisms
  }
  run_command_set( "mergrapher", $pUserParams, \@commands, \@stdouts, undef, \@workingDirs, $pLocalParams, $pLog, undef, undef, 1 );  

  return JGI_SUCCESS;

} 

sub check_outputs_stage_meraculous_mergraph
{

    my ($work_dir) = @_;
    my $date = `date +%Y%m%d-%H%M%S`;   
 
    my @a = `grep '^Read ' $work_dir/mergraph.0.*.err`;
    my $mersRead = sum( \@a, 1 );

    @a = `grep '^Added ' $work_dir/mergraph.0.*.err`;
    my $mersIn= sum( \@a, 1 );

    @a = `grep '^Printed ' $work_dir/mergraph.0.*.err`;
    my $mersOut= sum( \@a, 1 );
    
    @a = `grep '^Excluded ' $work_dir/mergraph.0.*.err`;
    my $excl= sum( \@a, 1 );

    my $stageValidFile = "$work_dir/stage_validation.$date";
    open( F, ">$stageValidFile" );

    print F "Mers read in: $mersRead\n";
    print F "Mers meeting the depth cutoff: $mersIn\n";
    print F "Mers used in the mer graph: $mersOut\n";
    print F "Mers excluded: $excl\n";

   if (($mersOut ne '') && ($mersOut != 0) && ($mersOut == $mersIn - $excl)) {
     	return JGI_SUCCESS;
   }
   else {
   	$pLog->error( "Stage validation failure:  incomplete outputs! \n");
   	$pLog->error( "\n", `cat $stageValidFile`);
   	return JGI_FAILURE;
   }
   close F;
}

sub cleanup_stage_meraculous_mergraph
{
    # this should still allow any stage to run or be redone
    my ($work_dir) = @_;
    my $touchfile = "$work_dir/CLEANED-UP.1"; 

    $pLog->info("Deleting intermediate files..");
    unlink glob "$work_dir/mergraph.*[0-9]";
    `touch $touchfile`;

    # if aggressive cleanup is selected we delete additional intermediate files that are not needed in the
    # subsequent stages but which may be required if user wants to rerun this stage or a prior one.
    if ($cleanupLevel ==2)
    {	
    }
    return JGI_SUCCESS;
}







### Stage: meraculous_ufx
#----------------------------
# The purpose of this stage is to:
#
# - assign each mer to one of the following categories:  UU, UX, UF, XX, XF, FF, where 'U' is a non-N base
# - avoid extensions that don't meet the quality/depth cutoff
# 
# The ouput is a list of mers and their UFX code
#


sub check_inputs_stage_meraculous_ufx
{
    if ($g_restarting)    {
	$pLog->info("Checking integrity of prior stages... ");
	if (is_stage_cleaned_up( (stage_before($g_stage)), $assembly_dir, $pLog)) { 
	    $pLog->error("Integrity check failed! ");
	    return JGI_FAILURE;
	}
	$pLog->info("ok");
    }
    return JGI_SUCCESS;
}

sub run_meraculous_ufx
{
  my ($work_dir) = @_;
  my $cmd;
  my @commands;
  my @stdouts;
  my @stderrs;
  my @workingDirs;

  my $mer_size                    = $pUserParams->get_param( USER_PARAM_mer_size,     $pLog )
                                ||   $pLocalParams->get_param( LOCAL_PARAM_autoselected_mer_size, $pLog );
  my $meraculous_min_depth_cutoff = $pUserParams->get_param( USER_PARAM_min_depth_cutoff, $pLog ) 
                                 || $pLocalParams->get_param( LOCAL_PARAM_autocalibrated_min_depth_cutoff, $pLog );

  my $mergraph_depth_pct_cutoff = $pUserParams->get_param( USER_PARAM_mergraph_depth_pct_cutoff, $pLog );


  # stage inputs
  my @prefixes = get_dna_prefixes( get_dna_prefix_list_filename( $assembly_dir ), $pLog );

  # executables
  my $mer2UFX = $release_root.PATH_MER2UFX;

  $pLog->info("Determining unique high quality extensions for each mer above the depth cutoff...");

  foreach my $prefix( @prefixes )
  {
      my $mergraphFile = "$assembly_dir/".stage_before($g_stage)."/mergraph.0.".$prefix->[0] ;
      push( @commands, "perl $mer2UFX -M $mergraphFile -D $meraculous_min_depth_cutoff" . ($mergraph_depth_pct_cutoff ? " -d $mergraph_depth_pct_cutoff" : "" ));
      push( @stdouts, "$work_dir/UFX.".$prefix->[0] );
      push( @stderrs, "$work_dir/UFX.".$prefix->[0].".err" );
      push( @workingDirs, "$work_dir" );
  }
  my $ram_request_override = 10; 
  run_command_set( "UFX", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, $ram_request_override, \&validate_stdout, undef);     

  return JGI_SUCCESS;
}

sub check_outputs_stage_meraculous_ufx
{
    my ($work_dir) = @_;
    my $date = `date +%Y%m%d-%H%M%S`;   
    my $countU = 0;
    my $countF = 0;
    my $countX = 0;

  foreach my $f (glob "$work_dir/UFX*[ACTG].err" )
  {
      my %counts;
      my @l;
      open (F, "<$f");
      while (<F>)
      {
	  next unless ($_ =~ /^[ACTGXF][ACTGXF]\s+/);
	  @l = split(/\t/, $_);
	  $counts{$l[0]} = $l[1];
      }
      close F;
      
      for (keys %counts)
      {
	  $countU += $counts{$_} if ($_ =~ /[ACTG][ACTG]/);
	  $countF += $counts{$_} if ($_ =~ /F[ACTGXF]/ || $_ =~ /[ACTGXF]F/);
	  $countX += $counts{$_} if ($_ =~ /X[ACTGXF]/ || $_ =~ /[ACTGXF]X/);
      }
  }
  
    my $stageValidFile = "$work_dir/stage_validation.$date";
    open( F, ">$stageValidFile" );

    print F "Mers extendable in both directions (UU):  $countU\n";
    print F "Mers extending into a fork in at least one direction (F): $countF\n";
    print F "Mers with no possible extension in at least one direction  (X): $countX\n";
    close F;

    if (! $countU) {
	$pLog->error( "Stage validation failure:  no uniquely extending kmers found. \n");
        return JGI_FAILURE;
    }

    # stop if over 10% of mers contain forks or terminations
    if ( ( $countF / $countU > 10 ) || ( $countX / $countU > 10 )) {
    	$pLog->error( "Stage validation failure:  too many k-mers without unique extensions!  Please check the mercount plot for signs of abnormal k-mer distribution. \n");
    	$pLog->error( "\n", `cat $stageValidFile`);
    	return JGI_FAILURE;
    }
    else {
      	return JGI_SUCCESS;
    }

    return JGI_SUCCESS;
}

sub cleanup_stage_meraculous_ufx
{
    # this should still allow any stage to run or be redone
    my ($work_dir) = @_;
    my $touchfile = "$work_dir/CLEANED-UP.1";

    $pLog->info("Deleting intermediate files..");

    # if aggressive cleanup is selected we delete additional intermediate files that are not needed in
    # subsequent stages but which may be required if user wants to rerun this stage or a prior one.
    if ($cleanupLevel == 2)
    {
	$touchfile = "$assembly_dir/".stage_before($g_stage)."/CLEANED-UP.2";
        unlink glob "$assembly_dir/".stage_before($g_stage)."/mergraph.*[ACTG]";
        `touch $touchfile`;
    }

    return JGI_SUCCESS;
}






### Stage: meraculous_contigs
#----------------------------
# The purpose of this stage is to:
#
# - load the UFX mers (essentially the k-mer graph) into a set of  efficient hash structures
# - build an initial set of contigs exclusively from mers of type UU 
#
# The output is a set of "UUtigs" in Fasta format

sub check_inputs_stage_meraculous_contigs
{
    if ($g_restarting)    {
	$pLog->info("Checking integrity of prior stages... ");
	if (is_stage_cleaned_up( (stage_before($g_stage)), $assembly_dir, $pLog)) { 
	    $pLog->error("Integrity check failed! ");
	    return JGI_FAILURE;
	}
	$pLog->info("ok");
    }
  return JGI_SUCCESS;
}

sub run_meraculous_contigs
{
  my ($work_dir) = @_;
  my $cmd;
  my @commands = ();
  my @stdouts = ();  
  my @stderrs = ();  
  my @workingDirs = ();


  my $mer_size                    = $pUserParams->get_param( USER_PARAM_mer_size,     $pLog )
                                 || $pLocalParams->get_param( LOCAL_PARAM_autoselected_mer_size, $pLog );

  my $meraculous_min_depth_cutoff = $pUserParams->get_param( USER_PARAM_min_depth_cutoff, $pLog) 
                                  || $pLocalParams->get_param( LOCAL_PARAM_autocalibrated_min_depth_cutoff, $pLog );
 
  my $diploid_mode                = $pUserParams->get_param( USER_PARAM_diploid_mode, $pLog ) || 0;

  my $threads                   = ( $pUserParams->get_param( USER_PARAM_use_cluster, $pLog ) ? 
				    $pUserParams->get_param( USER_PARAM_cluster_slots_per_task, $pLog ) :
				    $pUserParams->get_param( USER_PARAM_local_num_procs, $pLog ) ) 
                                || 1; 

  # stage inputs  
  my $UFXglob = "$assembly_dir/".stage_before($g_stage)."/UFX.*[ACTG]";
  for my $f (glob($UFXglob)) {
      if ( ! -e $f )     {
	  $pLog->error( "ERROR: Couldn't find  $f");
	  return JGI_FAILURE;
      }
  }

  # executables
  my $meraculous =  ($mer_size <= 56 ? $release_root.PATH_MERACULOUS_56 : $release_root.PATH_MERACULOUS_128);



  $pLog->info("Generating UU-contigs...");

  my $min_contig_size = ( $diploid_mode ? $mer_size : 2 * $mer_size );   
  #usage: [ ufx_file_wildcard ] [ mer_size ] [ numThreads ] [ out_prefix ] [ expected_num_elements ] [ min contig size ] [ debugMode ]
  push( @commands, "$meraculous \"$UFXglob\" $mer_size $threads UUtigs.fa 0 $min_contig_size 0 \"\" " );
  push( @stdouts, "$work_dir/UUtigs.fa" );
  push( @stderrs, "$work_dir/UUtigs.err" );
  push( @workingDirs, "$work_dir" );
  run_command_set( "UUtigger", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, undef, \&validate_stdout, 1);

  return JGI_SUCCESS;

}

sub check_outputs_stage_meraculous_contigs
{
    my ($work_dir) = @_;
    unless ( -s "$work_dir/UUtigs.fa" )
    {
	$pLog->error( "Stage validation failure: no UUtigs were generated!" );
	return JGI_FAILURE;
    }
    return JGI_SUCCESS;
}

sub cleanup_stage_meraculous_contigs
{
    # this should still allow any stage to run or be redone
    my ($work_dir) = @_;
    my $touchfile = "$work_dir/CLEANED-UP.1";

    $pLog->info("Deleting intermediate files..");
    `touch $touchfile`;

    # if aggressive cleanup is selected we delete additional intermediate files that are not needed in
    # subsequent stages but which may be required if user wants to rerun this stage or a prior one.
    if ($cleanupLevel == 2)
    {
	$touchfile = "$assembly_dir/".stage_before($g_stage)."/CLEANED-UP.2";
        unlink glob "$assembly_dir/".stage_before($g_stage)."/UFX.*[ACTG]";
        `touch $touchfile`;
    }

    return JGI_SUCCESS;
}





### Stage: bubble
#----------------------------
# The purpose of this stage is to:
# 
# - build depth statistics for UUtigs
# - optionally, interrogates ends of UUtigs to detect "bubbles" in the graph caused by diploidy and then merge qualifying UUtigs into new contigs called "haplotigs".
#
# The main outputs are: 
# haplotigs.fa          - a new set of contigs where diploid haplotypes have been "flattened"
# UUtigs.mer_depth           - a file of normalized mer depth averages for each contig
# haplotigs.depth.hist  - a histogram of mer depths in the new contigs, where diplotigs and the isotigs are expected to produce distinct peaks
#
sub check_inputs_stage_meraculous_bubble
{
    if ($g_restarting)    {
	$pLog->info("Checking integrity of prior stages... ");
	if (is_stage_cleaned_up( stage_before($g_stage), $assembly_dir, $pLog) || is_stage_cleaned_up("meraculous_mercount", $assembly_dir, $pLog)) { 
	    $pLog->error("Integrity check failed! ");
	    return JGI_FAILURE;
	}
	$pLog->info("ok");
    }
    return JGI_SUCCESS;
}

sub run_meraculous_bubble
{
  my ($work_dir) = @_;
  my $cmd; my $out; my $err;
  my @commands = ();
  my @stdouts = ();
  my @stderrs = ();
  my @workingDirs = ();

  my $mer_size =                     $pUserParams->get_param( USER_PARAM_mer_size,     $pLog )
                                  || $pLocalParams->get_param( LOCAL_PARAM_autoselected_mer_size, $pLog );

  my $meraculous_min_depth_cutoff =  $pUserParams->get_param( USER_PARAM_min_depth_cutoff, $pLog ) 
                                  || $pLocalParams->get_param( LOCAL_PARAM_autocalibrated_min_depth_cutoff, $pLog );
 
  my $diploid_mode =                 $pUserParams->get_param( USER_PARAM_diploid_mode, $pLog ) || 0;

  my $noStrictHaplotypes =             $pUserParams->get_param( USER_PARAM_no_strict_haplotypes, $pLog );

  my $threads =                    ( $pUserParams->get_param( USER_PARAM_use_cluster, $pLog ) ? 
				     $pUserParams->get_param( USER_PARAM_cluster_slots_per_task, $pLog ) :
				     $pUserParams->get_param( USER_PARAM_local_num_procs, $pLog ))
                                  || 1;

  my $nodes =                        $pUserParams->get_param( USER_PARAM_use_cluster, $pLog ) ?
                                     $pUserParams->get_param( USER_PARAM_cluster_num_nodes, $pLog ) : 
				     1;

  # stage inputs 
  my @prefixes = get_dna_prefixes( get_dna_prefix_list_filename( $assembly_dir ), $pLog );
  my $UUtigsFile = "$assembly_dir/".stage_before($g_stage)."/UUtigs.fa";
  my $ceaFile =    "$assembly_dir/".stage_before($g_stage)."/UUtigs.fa.cea";  #got created along with UUtigs.fa

  for my $f ($UUtigsFile, $ceaFile) {
      if ( ! -e $f )     {
	  $pLog->error( "ERROR: Couldn't find  $f");
	  return JGI_FAILURE;
      }
  }

  # executables
  my $contigMerDepth = ($mer_size <= 56 
			? $release_root.PATH_CONTIG_MER_DEPTH_56 
			: $release_root.PATH_CONTIG_MER_DEPTH_128);

  my $bubbleScout    = $release_root.PATH_BUBBLE_SCOUT;

  my $merBlast       = ($mer_size <= 56 
			? $release_root.PATH_MERBLAST_56 
			: $release_root.PATH_MERBLAST_128);

  my $bubblePopper   = $release_root.PATH_BUBBLE_POPPER;

  my $histogram      = $release_root.PATH_HISTOGRAM;

  my $findDmin       = $release_root.PATH_FINDDMIN;


  #stage outputs
  my $mergedContigMerDepthFile = "$work_dir/UUtigs.mer_depth";  # created by contigMerDepth
  my $bubblesFile = "$work_dir/bubbles.fa"; # created by bubbleScout
  my $bubbleDataFile = "$work_dir/bubbleData";  # created by bubbleScout 
  my $bubbleMapFile = "$work_dir/readsToBubbles.blastMap";  #created by merblast
  my $bubbleMapFilePfx = $bubbleMapFile;


  if ( $g_resuming ) {   
      my $localResumeCheckpoint = $pLocalParams->get_param( LOCAL_PARAM_local_resume_checkpoint, $pLog );
      goto $localResumeCheckpoint unless  ($localResumeCheckpoint eq "none");
  } 


 $pLog->info( "Computing mer depth in the UUtigs..." );

  foreach my $prefix_set ( @prefixes )
  {
    my $prefixList = join('+',@{$prefix_set}) ; #individual prefixes
    my $mercountFile = "$assembly_dir/meraculous_mercount/mercount.0.".$prefix_set->[0] ;
    #Usage: $contigMerDepth [ mercount_file_prefix ] [ mer_size ] [ min_depth ] [ prefixListToHash ] [ numThreads ] [ outputPrefix ] [ contigsFile ]
    $cmd = "$contigMerDepth $mercountFile $mer_size $meraculous_min_depth_cutoff $prefixList $threads UUtigs.mer_depth.".$prefix_set->[0] ." $UUtigsFile";

    push( @commands, $cmd );
    push( @stdouts, "$work_dir/UUtigs.mer_depth.".$prefix_set->[0].".tmp" );
    push( @stderrs, "$work_dir/UUtigs.mer_depth.".$prefix_set->[0].".err" );
    push( @workingDirs, "$work_dir" );
  }
  run_command_set( "contigMerDepth", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, undef,  undef, 1);


  # merge mer depth data across all contigMerDepth outputs 
  my @contigMerDepthFiles = glob("$work_dir/UUtigs.mer_depth.*[0-9]");
  unless( merge_contig_mer_depths(\@contigMerDepthFiles, $mergedContigMerDepthFile, $pLog) eq JGI_SUCCESS )
  {
      $pLog->error("ERROR: Merging of contig mer depths failed! ");
      return JGI_FAILURE;
  }
  

  $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "contigMerDepth_complete", $pLog );
  write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );

 contigMerDepth_complete:


  unless ($diploid_mode) {
      return JGI_SUCCESS;
  }


  ### detect bubbles

  $pLog->info( "Finding bubbles among UUtigs..." );

  # Usage:  ./bubbleScout.pl <-c ceaFile> <-f fastaFile> [ -o outPrefix -V(erbose)]
  # writes bubbletig sequences to stdout and bubble info to bubbleData
  $cmd = "perl $bubbleScout  -c $ceaFile -f $UUtigsFile";
  $out = $bubblesFile;
  $err = $bubblesFile.".err";
  push( @commands, $cmd );
  push( @stdouts, $out );
  push( @stderrs, $err );
  push( @workingDirs, "$work_dir" );
  run_command_set( "bubbleScout", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, undef, \&validate_stdout, undef);


  $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "bubbleScout_complete", $pLog );
  write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );
  
 bubbleScout_complete:


  ### map reads to bubbles

  $pLog->info( "Mapping reads to bubbletigs..." );
  my @fastqIFs;
  foreach my $libName ( @{ $pLibInfo->get_lib_names($pLog)} )  {
      my $libQOffset =  $pLibInfo->get_lib_prop($libName, LIB_PROP_qOffset, $pLog);
      push( @fastqIFs, generate_fastq_info_files($assembly_dir, $work_dir, $libName, $threads, $nodes, $libQOffset, $pLog) );
  }

  my $nContigs = `grep -c '>' $bubblesFile`;
  chomp $nContigs;
  my $merBlastFlags = " -5 0";  # no gapped extensions      

  for my $f (0..$#fastqIFs)
  {
      # reserve names for temporary threaded outputs; make sure they're in numerical order
      my @sortedThreadOuts = ();
      for my $t (0..($threads-1)) { push ( @sortedThreadOuts, "$bubbleMapFilePfx.f$f.$t") }    	    
      
      $cmd = "$merBlast  $bubblesFile \"-\" $mer_size $fastqIFs[$f] ${bubbleMapFilePfx}.f$f $threads ".($nContigs+1)." $merBlastFlags 2> ${bubbleMapFilePfx}.f$f.err && cat ".(join(" ", @sortedThreadOuts));
      # since this is a multi-command, the stderr redirection that matters to us had to be written directly into the first part of the command; any other stderr will be captured via default mechanisms
      push( @commands, $cmd );
      push( @stdouts, "${bubbleMapFilePfx}.f$f" );
      push( @workingDirs, "$work_dir" );
  }
  run_command_set( "bubbleMap", $pUserParams, \@commands, \@stdouts, undef, \@workingDirs, $pLocalParams, $pLog, undef, \&validate_stdout, 1);

   unlink glob "$bubbleMapFilePfx.f*[0-9].*[0-9]";  # ret rid of tmp threaded outs right away
#   $cmd = "cat ${bubbleMapFilePfx}.f*[0-9]";
#   $out = $bubbleMapFile;
#   run_single_cmd( $cmd, $out, undef, undef, $pLog);


  $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "bubbleMap_complete", $pLog );
  write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );
  
 bubbleMap_complete:


  ### Resolve contig-bubble-contig paths

  $pLog->info( "Resolving bubbles and building haplotigs..." );

  my $maskMode = $diploid_mode;
  my $maxAvgInsSize = 0;
  foreach my $libName ( @{ $pLibInfo->get_lib_names($pLog)} )  {
      my $libInsSize = $pLibInfo->get_lib_prop($libName, LIB_PROP_estInsAvg, $pLog);
      if ($libInsSize > $maxAvgInsSize) {
	  $maxAvgInsSize = $libInsSize;
      }
  }
  my $bubbleMapGlob = "${bubbleMapFilePfx}.f*[0-9]";
  #Usage: ./bubblePopper.pl <-b bubbleDataFile> <-f fastaFile> <-d depthFile> <-m merLength>  [ -K maskingMode ] [ -M blastMap] [ -R requireCloneLinkageForDiplotigs ]  [ -V(erbose)] 
  $cmd = "perl $bubblePopper -b $bubbleDataFile -f $UUtigsFile -d $mergedContigMerDepthFile -m $mer_size -I $maxAvgInsSize -M \"$bubbleMapGlob\" -K $maskMode" . ($noStrictHaplotypes ? "" : " -R");     
  $out = "$work_dir/haplotigs.fa";
  $err = "$work_dir/haplotigs.err";
  push( @commands, $cmd );
  push( @stdouts, $out );
  push( @stderrs, $err );
  push( @workingDirs, "$work_dir" );
  run_command_set( "bubblePop", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, undef, \&validate_stdout, undef);

  $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "bubblePop_complete", $pLog );
  write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );
  
 bubblePop_complete:


  my $haplotigsInfoFile = "$work_dir/haplotigs.list";  # got created by bubblePopper
  my $isotigsDepthHist = "$work_dir/isotigs.depth.hist";
  my $diplotigsDepthHist = "$work_dir/diplotigs.depth.hist";
  my $allTigsDepthHist = "$work_dir/haplotigs.depth.hist";


  $cmd = "perl $histogram $haplotigsInfoFile 5 4";  # smoothing this one since this will be used to auto-detect the diploid depth threshold
  $out = $allTigsDepthHist;
  $err = $allTigsDepthHist.".err";
  run_single_cmd( $cmd, $out, $err, undef, $pLog );


  $cmd = "grep isotig $haplotigsInfoFile | perl $histogram - 5 1";
  $out = $isotigsDepthHist;
  $err = $isotigsDepthHist.".err";
  run_single_cmd( $cmd, $out, $err, undef, $pLog );

  $cmd = "grep diplotig $haplotigsInfoFile | perl $histogram - 5 1";
  $out = $diplotigsDepthHist;
  $err = $diplotigsDepthHist.".err";
  run_single_cmd( $cmd, $out, $err, undef, $pLog );



  my $allTigsDepthHistPNG = $allTigsDepthHist . ".png";
#      my $avgDepth = `head -1 $allTigsDepthHist | awk '{print $7}' | sed 's|[<>]||g'`;
  open H, $allTigsDepthHist; my @l=split(/\s+/, <H>); my $avgDepth = $l[6]; $avgDepth =~ s/\<//;
  close H;
  my $maxXrange = int(4*$avgDepth);

  $cmd = "echo \"set terminal png truecolor enhanced; set output '$allTigsDepthHistPNG'; set xlabel 'k-mer depth'; set ylabel 'nr. of contigs'; set style fill transparent solid 0.5; ".
      "plot [1:$maxXrange] '$isotigsDepthHist' using 2:7 with boxes title 'isotigs', '$diplotigsDepthHist' using 2:7 with boxes title 'diplotigs'\" | gnuplot";
  $err = "$work_dir/gnuplot.err";
  run_single_cmd_OK_to_fail( $cmd, undef, $err, $pLog);


  my $bubble_depth_threshold             = $pUserParams->get_param( USER_PARAM_bubble_depth_threshold, $pLog );
  if ( ! $bubble_depth_threshold )
  {
      # perform automated haplotig depth cutoff detection
      $pLog->info("Performing automated bubble depth threshold detection...");

      $cmd = "perl $findDmin $allTigsDepthHist 1 2> haplotigs.dmin.err ";
      my $ret_val;
      if (run_local_cmd($cmd, \$ret_val, $pLog, \$bubble_depth_threshold) != JGI_SUCCESS)
      {
	  return JGI_FAILURE;
      }

      if ( ! $bubble_depth_threshold )
      {
	  $pLog->error("WARNING: Unable to auto-detect a valid bubble_depth_threshold value.  Examine the file $allTigsDepthHistPNG and try setting it manually");
      }
      else {
	  $pLocalParams->put_param( LOCAL_PARAM_autocalibrated_bubble_depth_threshold,  $bubble_depth_threshold, $pLog );
      }
  }

  return JGI_SUCCESS;
}

sub check_outputs_stage_meraculous_bubble
{
    my ($work_dir) = @_;
    unless ( -s "$work_dir/UUtigs.mer_depth" )
    {
	$pLog->error( "Stage validation failure: no contig mer depths were generated!" );
	return JGI_FAILURE;
    }    
    if ( $pUserParams->get_param( USER_PARAM_diploid_mode, $pLog ))
    {
	unless ( -s "$work_dir/haplotigs.fa" ) 
	{
	    $pLog->error( "Stage validation failure: no haplotigs were generated!" );
	    return JGI_FAILURE;
	}
    }
    return JGI_SUCCESS;
}

sub cleanup_stage_meraculous_bubble
{
    my ($work_dir) = @_;
    my $touchfile = "$work_dir/CLEANED-UP.1";

    $pLog->info("Deleting intermediate files..");
    unlink glob "$work_dir/UUtigs.mer_depth.*[0-9]";
    unlink glob "$work_dir/UUtigs.mer_depth.*.tmp";
    unlink glob "$work_dir/readsToBubbles.blastMap.*[0-9]";
    unlink glob "$work_dir/readsToBubbles.blastMap";
    
    `touch $touchfile`;

    # if aggressive cleanup is selected we delete additional intermediate files that are not needed in
    # subsequent stages but which may be required if user wants to rerun this stage or a prior one.
    if ($cleanupLevel == 2)
    {
	unlink glob "$assembly_dir/meraculous_mercount/mercount.*[ACTG]";
	unlink glob "$assembly_dir/meraculous_mercount/mercount.*[0-9]";  #can't delete these earlier bc contigMerDepth relies on them
	$touchfile = "$assembly_dir/meraculous_mercount/CLEANED-UP.2";
	`touch $touchfile`;
    }

    return JGI_SUCCESS;
}




### Stage: meraculous_merblast
#----------------------------
# The purpose of this stage is to:
#
# - If diploid assembly, filter out half-depth isotigs (diploid_mode 1 only)
# - Map reads from libraries earmarked for ono onto contigs.  d
#
# The output is a single blastMap.$libName.*.merged file for each participating library
#

sub check_inputs_stage_meraculous_merblast
{
    if ($g_restarting)    {
	$pLog->info("Checking integrity of prior stages... ");
	if (is_stage_cleaned_up( stage_before($g_stage), $assembly_dir, $pLog) || is_stage_cleaned_up("meraculous_import", $assembly_dir, $pLog)) { 
	    $pLog->error("Integrity check failed! ");
	    return JGI_FAILURE;
	}
	$pLog->info("ok");
    }
    return JGI_SUCCESS;
}
sub run_meraculous_merblast
{
  my ($work_dir) = @_;
  my $cmd; my $out; my $err;
  my @commands = (); my @stdouts = (); my @stderrs = (); my @workingDirs = ();

  my $memLimit =              ( $pUserParams->get_param( USER_PARAM_use_cluster, $pLog ) ?
                                $pUserParams->get_param( USER_PARAM_cluster_ram_request, $pLog ) :
				$pUserParams->get_param( USER_PARAM_local_max_memory, $pLog ));
   # overwrite with stage-specific memory limit
  if ( $pUserParams->get_param( USER_PARAM_use_cluster, $pLog ) && $pUserParams->get_param( "cluster_ram_".${g_stage}, $pLog ) )  {
      $memLimit = $pUserParams->get_param( "cluster_ram_".${g_stage}, $pLog );
  }

  my $threads =             ( $pUserParams->get_param( USER_PARAM_use_cluster, $pLog ) ? 
			      $pUserParams->get_param( USER_PARAM_cluster_slots_per_task, $pLog ) :
			      $pUserParams->get_param( USER_PARAM_local_num_procs, $pLog ))
                            || 1;

  my $mer_size =              $pUserParams->get_param( USER_PARAM_mer_size,     $pLog )
                           || $pLocalParams->get_param( LOCAL_PARAM_autoselected_mer_size, $pLog );

  my $diploid_mode =          $pUserParams->get_param( USER_PARAM_diploid_mode, $pLog ) || 0;

  my $nodes =               ( $pUserParams->get_param( USER_PARAM_use_cluster, $pLog ) && $pUserParams->get_param( USER_PARAM_cluster_num_nodes, $pLog ) ) ?
			      $pUserParams->get_param( USER_PARAM_cluster_num_nodes, $pLog ) :
                              1;


  # stage inputs
  my $contigMerDepthFile = "$assembly_dir/".stage_before($g_stage)."/UUtigs.mer_depth";
  my $remoteContigsFa;
  my $remoteContigsInfo;
  if ( $diploid_mode ) {
      $remoteContigsFa = "$assembly_dir/".stage_before($g_stage)."/haplotigs.fa";
      $remoteContigsInfo = "$assembly_dir/".stage_before($g_stage)."/haplotigs.list";
  }
  else {
      $remoteContigsFa = "$assembly_dir/meraculous_contigs/UUtigs.fa";       
  } 

  for my $f ($contigMerDepthFile, $remoteContigsFa) {
      if ( ! -e $f )     {
	  $pLog->error( "ERROR: Couldn't find  $f");
	  return JGI_FAILURE;
      }
  }

  # executables
  my $screenList2 = $release_root. PATH_SCREEN_LIST2; 
  my $fastaSplitter = $release_root.PATH_FASTASPLITTER;
  my $merBlast = ($mer_size <= 56 ? $release_root.PATH_MERBLAST_56 : $release_root.PATH_MERBLAST_128);
  my $mergeMerBlasts = $release_root . PATH_MERGE_BLASTS;




  ### from here on, use only entries earmarked for ono and/or gap closure  (i.e. they have a non-zero ono set id and positive insert avg)
  my @libsForMerblast;
  my $maxInsSize = 0;
  foreach my $libName ( @{ $pLibInfo->get_lib_names($pLog)} )  {
      my $libSetId = $pLibInfo->get_lib_prop($libName, LIB_PROP_forScaffRound, $pLog);
      my $libUseForGapClosing = $pLibInfo->get_lib_prop($libName, LIB_PROP_forGapClosing, $pLog);
      if ( ( $libSetId == 0) && ($libUseForGapClosing == 0) ) { next; }
      my $libInsSize = $pLibInfo->get_lib_prop($libName, LIB_PROP_estInsAvg, $pLog);
      my $libInsSdev = $pLibInfo->get_lib_prop($libName, LIB_PROP_estInsSdev, $pLog);
      if ( $libInsSize+($libInsSdev*2)  > $maxInsSize) { 
	  $maxInsSize = $libInsSize+($libInsSdev*2);
      }
      push(@libsForMerblast, $libName);
  }
  if (! @libsForMerblast )  {
      $pLog->error( "ERROR: No libraries are earmarked for scaffolding and/or gap closure in the config file!" );
      return JGI_FAILURE;
  }

  my $crf = "$work_dir/contigs.crf";
  my $contigsFile = "$work_dir/contigs.fa";



  if ( $g_resuming ) {   
      my $localResumeCheckpoint = $pLocalParams->get_param( LOCAL_PARAM_local_resume_checkpoint, $pLog );
      goto $localResumeCheckpoint unless  ($localResumeCheckpoint eq "none");
  } 


  # update contig info depending on whether we're starting with UUtigs or haplotigs
  if ( $diploid_mode ) {
      $pLog->info( "Applying depth filter to haplotigs ..." );
      my $filteredContigsFile = "$work_dir/haplotigs.filtered.fa";

      my $bubble_depth_threshold = $pUserParams->get_param( USER_PARAM_bubble_depth_threshold, $pLog ) 
	                      || $pLocalParams->get_param( LOCAL_PARAM_autocalibrated_bubble_depth_threshold, $pLog );

      if (! $bubble_depth_threshold) {
	  $pLog->error("ERROR: Diploid-haploid depth threshold could not be auto-detected!  Plese set bubble_depth_threshold to a non-zero value and resume. ");
	  return JGI_FAILURE;
      }

      unless (apply_filters($diploid_mode, $maxInsSize, $remoteContigsFa, $remoteContigsInfo, $filteredContigsFile, $bubble_depth_threshold, $crf, $pLog) eq JGI_SUCCESS)      {
	  $pLog->error("ERROR: Could not apply depth filters to contigs.");
	  return JGI_FAILURE;
      }
      $cmd = "ln -sf ./".basename($filteredContigsFile)." $contigsFile";  # relative symlink for portability
      run_single_cmd( $cmd, undef, undef, undef, $pLog);
  }
  else { 
      $cmd = "perl -ane '\$F[1]+=($mer_size-1); print \"\$F[0]\t\$F[1]\t\$F[2]\n\"' $contigMerDepthFile";
      $out = $crf;
      run_single_cmd( $cmd, $out, undef, undef, $pLog); 
      $cmd = "ln -sf ../meraculous_contigs/".basename($remoteContigsFa)." $contigsFile";  #  relative symlink for portability
      run_single_cmd( $cmd, undef, undef, undef, $pLog);
  }

  $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "blasts_started", $pLog );
  write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );

 blasts_started:

  my $numContigs = `cat $crf | perl -ane 'END {print \$n} \$n++'`;

  if ( ! $numContigs ) {
      $pLog->error("ERROR: No contigs found in $crf  !");
      return JGI_FAILURE;
  }

  #  partition the contigs in order to fit under the memory limit
  my $nContigChunks;
  if ($memLimit) {
      # As a crude approximation we expect the hash to use 54 bytes per mer when running a _56mer compilation and 82 bytes for _128mer 
      my $bytes = ($mer_size <= 56 ? 54 : 82);
      my $contigHashSize = `cat $crf |perl -ane 'END {print \$sum} \$sum+=((\$F[1]-($mer_size-1))*$bytes)' `;
      $contigHashSize *= 1.1;  # give it a 10% cushion
      $contigHashSize = $contigHashSize/1000000000;  #convert to Gb
      $nContigChunks = ceil($contigHashSize/$memLimit);  # i.e. we need to split the contigs into this many pieces in order to fit under the current memory limit
      if ($nContigChunks > 1) 
      {
	  unlink("$work_dir/contigs.fa.*[0-9]");
	  $pLog->info( "Splitting contigs into $nContigChunks parts in order to fit under the memory limit of $memLimit Gb ... " );
	  $cmd= "perl $fastaSplitter $contigsFile $nContigChunks";
	  $out = undef;
	  $err= "${contigsFile}.split.err";
	  run_single_cmd( $cmd, $out, $err, 1, $pLog);	      
      }
      else {
	  $cmd = "ln -sf ./".basename($contigsFile)." ${contigsFile}.0";  # relative symlink for portability
	  run_single_cmd( $cmd, undef, undef, undef, $pLog);
      }
  }
  else { 
      $nContigChunks = 1;
      $cmd = "ln -sf ./".basename($contigsFile)." ${contigsFile}.0";  # relative symlink for portability
      run_single_cmd( $cmd, undef, undef, undef, $pLog);
  }
  my @contigsChunks = glob "${contigsFile}.*[0-9]";

  # # Further slice each contig chunk for parallel input
  # if ($threads > 1) {
  #     foreach my $cChunk (@contigsChunks) {
  # 	  $cmd = "perl $fastaSplitter $cChunk  $threads";
  # 	  $out = undef;
  # 	  $err= "${cChunk}.split.err";
  #         run_single_cmd( $cmd, $out, $err, 1, $pLog);
  #     }
  # }


  #  If there's a surplus of nodes, we can benefit by spliting the input reads as well; 
  my $maxFqChunks = ($nodes > $nContigChunks) ? ceil($nodes/$nContigChunks) : 1;


  $pLog->info( "Mapping reads from qualifying libraries to the contigs..." );

  my %fastqIFsForLib;

  foreach my $libName( @libsForMerblast )
  {
    my $libQOffset =  $pLibInfo->get_lib_prop($libName, LIB_PROP_qOffset, $pLog);
    my @fastqIFs = generate_fastq_info_files($assembly_dir, $work_dir, $libName, $threads, $maxFqChunks, $libQOffset, $pLog);

    $fastqIFsForLib{$libName} = $#fastqIFs;

    for my $f (0..$#fastqIFs)
    {
	for my $c (0..$#contigsChunks)
	{

#	    my $contigsStr = ($threads > 1) ? "\"$contigsChunks[$c].*[0-9]\"" : $contigsChunks[$c];

	    # reserve names for temporary threaded outputs; make sure they're in numerical order
	    my @sortedThreadOuts = ();
	    for my $t (0..($threads-1)) { push ( @sortedThreadOuts, "$work_dir/blastMap.$libName.c$c.f$f.$t") }    	    
	    my $merBlastFlags = ( $diploid_mode == 2) ? " -5 1 1" : " -5 1 0"; # add a flag to avoid seeding to lowercase regions but allow mixed case extension 

	    push ( @commands, "$merBlast $contigsChunks[$c] \"-\" $mer_size $fastqIFs[$f] $work_dir/blastMap.$libName.c$c.f$f $threads ".($numContigs+1)."$merBlastFlags 2> $work_dir/blastMap.$libName.c$c.f$f.err  &&  cat ".(join(" ", @sortedThreadOuts)) ); 
	    push ( @stdouts, "$work_dir/blastMap.$libName.c$c.f$f" );
	    push ( @workingDirs, "$work_dir" );
	    # since this is a multi-command, the stderr redirection that matters to us had to be written directly into
	    # the first part of the command; any other stderr will be captured via default mechanisms
	}
    }
    run_command_set( "merBlast_$libName", $pUserParams, \@commands, \@stdouts, undef, \@workingDirs, $pLocalParams, $pLog, undef, undef, 1 );
  }



  $pLog->info( "Merging the read maps..." );
  foreach my $libName( @libsForMerblast )
  {
      # merge all maps pertaining to the same chunk of reads
      for my $f ( 0..$fastqIFsForLib{$libName} )
      {
 	push ( @commands, "$mergeMerBlasts \"$work_dir/blastMap.$libName.c*.f$f\" 1" );
 	push ( @stdouts, "$work_dir/blastMap.$libName.f$f.merged" );
 	push ( @stderrs, "$work_dir/blastMap.$libName.f$f.merged.err" );
 	push ( @workingDirs, "$work_dir" );
      }
  }
  my $ram_request_override = 10; 
  run_command_set( "mergeMaps", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog,  $ram_request_override, \&validate_stdout, 0); 

  return JGI_SUCCESS;
}

sub check_outputs_stage_meraculous_merblast
{
  return JGI_SUCCESS;
}

sub cleanup_stage_meraculous_merblast
{
    # this should still allow any stage to run or be redone
    my ($work_dir) = @_;
    my $touchfile = "$work_dir/CLEANED-UP.1";

    $pLog->info("Deleting intermediate files..");
    unlink glob "$work_dir/contigs.fa.*[0-9]";
    unlink glob "$work_dir/blastMap.*[0-9]"; 
    
    `touch $touchfile`;

    # if aggressive cleanup is selected we delete additional intermediate files that are not needed in the 
    # subsequent stages but which may be required if user wants to rerun this stage or a prior one.
    if ($cleanupLevel ==2)
    {
    }
    return JGI_SUCCESS;

}

  

### Stage: meraculous_ono
#----------------------------
# The purpose of this stage is to:
#
# - For each library in a set (grouped according to the params file), determine the actual 
#   insert-size distribution based on read pairs mapping to the same contig in the proper orientation.
# - Generate a gap size model for each library ([library].gapModel)
# - Generate linkage information for each set based on above (linkFile.n)  and build scaffolds (*.srf)
# - Iterate this for the next ono set, bootstrapping off of the scaffolds from the previous round.
#
# The output is the scaffolds file ( p[n].[round].srf ) with the highest scaffodl N50, where n is the min number 
# of links required to link any two contigs.
#
sub check_inputs_stage_meraculous_ono
{
    if ($g_restarting)    {
	$pLog->info("Checking integrity of prior stages... ");
	if (is_stage_cleaned_up( stage_before($g_stage), $assembly_dir, $pLog) || is_stage_cleaned_up("meraculous_import", $assembly_dir, $pLog)) { 
	    $pLog->error("Integrity check failed! ");
	    return JGI_FAILURE;
	}
	$pLog->info("ok");
    }
    return JGI_SUCCESS;
}
sub run_meraculous_ono
{
  my ($work_dir) = @_;
  my $cmd; my $out; my $err;
  my @commands = (); my @stdouts = (); my @stderrs = (); my @workingDirs = ();

  # user params
  my $meraculous_min_depth_cutoff =  $pUserParams->get_param( USER_PARAM_min_depth_cutoff, $pLog ) 
                                  || $pLocalParams->get_param( LOCAL_PARAM_autocalibrated_min_depth_cutoff, $pLog );

  my $mer_size =                     $pUserParams->get_param( USER_PARAM_mer_size,     $pLog )
                                  || $pLocalParams->get_param( LOCAL_PARAM_autoselected_mer_size, $pLog );

  my $diploid_mode =                 $pUserParams->get_param( USER_PARAM_diploid_mode, $pLog ) || 0;

  my $diploid_depth_threshold =       $pUserParams->get_param( USER_PARAM_bubble_depth_threshold, $pLog ) 
                                  || $pLocalParams->get_param( LOCAL_PARAM_autocalibrated_bubble_depth_threshold, $pLog );

  my $noStrictHaplotypes =             $pUserParams->get_param( USER_PARAM_no_strict_haplotypes, $pLog );

  my $fallbackOnEst =                $pUserParams->get_param( USER_PARAM_fallback_on_est_insert_size, $pLog );

  my $minMatch = $mer_size;
  my %blastMaps;
  my $prevRoundMaxInsert;
  my $libName;
  my @setLibs;
  my $startingSet = 1;
  my %bootstrappedInsertAverages;
  my %bootstrappedInsertSdevs;

  my $ram_request_override =10; 

  # stage inputs

  my $inputContigsFa = "$assembly_dir/".stage_before($g_stage)."/contigs.fa";;
  my $crf = "$assembly_dir/".stage_before($g_stage)."/contigs.crf";

  # executables
  my $splinter = $release_root . PATH_SPLINTER;
  my $spanner = $release_root . PATH_SPANNER;
  my $blastMapAnalyzer = $release_root . PATH_BLASTMAPANALYZER;
  my $bmaToLinks = $release_root . PATH_BMA_TO_LINKS;
  my $ono = $release_root.PATH_ONO;
  my $haplotyper = $release_root.PATH_HAPLOTYPER;
  my $n50 = $release_root.PATH_N50;
  my $optimize = $release_root.PATH_OPTIMIZE;
  my $screenList2 = $release_root. PATH_SCREEN_LIST2; 

  # output files
  my $prevRoundSrf; 
  my $spannerMaskList;
  my $finalSrf = "$work_dir/scaffolds.srf";
  my $linkDataFile; my $linkFile;
  my $bmaDataFile; my $bmaFile;
  my $contigsFile = "$work_dir/contigs.fa";




  ### from here on, use only libraries earmarked for ono

  my @libsForONO;
  foreach $libName ( @{ $pLibInfo->get_lib_names($pLog)} )
  {
      if ( $pLibInfo->get_lib_prop($libName, LIB_PROP_forScaffRound, $pLog) == 0) { next; }
      push(@libsForONO, $libName);
  }
  if (! @libsForONO )
  {
      $pLog->error( "ERROR: No libraries are earmarked for scaffolding in the config file!" );
      return JGI_FAILURE;
  }

  my @setInfo = $pLibInfo->create_set_info($pLog);

  # jump to a resume checkpoint if it exists
  if ( $g_resuming ) 
  {
      my $localResumeCheckpoint =  $pLocalParams->get_param( LOCAL_PARAM_local_resume_checkpoint, $pLog );
        # determine the set id to start from in case we're resuming in the middle of rounds.
        if (defined $pLocalParams->get_param( LOCAL_PARAM_prev_round_srf, $pLog ) )
	{
	    $prevRoundSrf = $pLocalParams->get_param( LOCAL_PARAM_prev_round_srf, $pLog );
	    $prevRoundSrf =~ /\.(\d+)\.srf$/;
	    $startingSet = $1+1;
	    $pLog->info( "Found scaffold info from the last successfully completed round..." );      
	    $prevRoundMaxInsert = $pLocalParams->get_param( LOCAL_PARAM_prev_round_max_insert, $pLog );	    
	}
      goto $localResumeCheckpoint  unless  ($localResumeCheckpoint eq "none"); 
  } 


  #
  # Splinting: linking contigs traversed by a common read
  #

  $pLog->info("Scaffolding contigs that are splinted by mapped reads...");
  run_single_cmd( "mkdir -p SPLINTING", undef, undef, undef, $pLog);


  # use only libs earmarked for the first round
  foreach $libName (@{ $setInfo[1] })
  {
      my $lib5P_wiggleRoom =     $pLibInfo->get_lib_prop($libName, LIB_PROP_5primeWiggleRoom, $pLog);
      my $libUseForGapClosing =  $pLibInfo->get_lib_prop($libName, LIB_PROP_forGapClosing, $pLog);
      next if (  !  $libUseForGapClosing );
      

      foreach my $blastMap ( glob "$assembly_dir/".stage_before($g_stage)."/blastMap.$libName.f*.merged")
      {
	  $cmd = "perl $splinter -b $blastMap -m $minMatch ".
	      ( $lib5P_wiggleRoom  ? " -F $lib5P_wiggleRoom " : "" );
	  push ( @commands, $cmd );
	  push ( @stdouts, "$work_dir/SPLINTING/".(basename($blastMap)).".splints.bma" );
	  push ( @stderrs, "$work_dir/SPLINTING/".(basename($blastMap)).".splints.bma.err" );
	  push( @workingDirs, "$work_dir" );
      }
  }
  if (! @commands)
  {
      $pLog->warn( "WARNING: No libraries were deemed suitable for splinting.  Will scaffold off of original contigs.. ");
      goto splinting_complete;
  }
  run_command_set( "splinter", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, $ram_request_override, undef, undef);  # no output validation here bc we're not guaranteed that any good splints are found

 $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "splinting_complete", $pLog );
  write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );

 splinting_complete:



 #
 # Scaffolding rounds
 #

  $pLog->info("Scaffolding using mapped paired ends ...");

  # group libraries by set  (set 0 will be empty because of the filter above)
  my $maxSetId = scalar( @setInfo );  
  for( my $setId = $startingSet; $setId < $maxSetId; $setId++ )
  {
    # @setInfo is an array of lists.  The idx = the setId.  The values are the lib names
    #   that belong to that setId.  If the idx == 0, we should skip that set ( 0 is special case for no ono )
    # Due to user sloppiness, it is actually legal to specify non-consecutive setIds
    #   so we need to check and see if a given setId is null or not.  If the user
    #   specifies libs for setId 1 and 5, for instance, then an array from [0...5] inclusive will be created, with null values for setId 0, 2, 3, 4.

    if ( ! defined( $setInfo[ $setId ] ) || scalar( @{ $setInfo[ $setId ] } ) == 0 ) { next; }

    $pLog->info( "Beginning scaffolding round $setId..." );

    run_single_cmd( "mkdir -p ROUND_$setId", undef, undef, undef, $pLog);

    @setLibs = @{ $setInfo[ $setId ] };
    my $innieListStr = "";

 
    # initiate a config file to be used in link generation (bmaToLinks.pl)
    $bmaDataFile = "$work_dir/ROUND_$setId/bmaData.$setId";
    open( BMA_DATA_OUT, ">$bmaDataFile" ) || die $pLog->error("couldn't open $bmaDataFile");
    # initiate a config file to be used in scaffolding (oNo.pl)
    $linkDataFile = "$work_dir/ROUND_$setId/linkData.$setId";
    open( LINK_DATA_OUT, ">$linkDataFile" ) || die $pLog->error("couldn't open $linkDataFile");
    my $linkDataLibStr = "";
    my $linkDataInsertSizeStr = "";
    my $linkDataInsertSdevStr = "";
    $linkFile = "$work_dir/ROUND_$setId/linkFile.$setId";    

    my $maxAvgInsertThisRound = 0;


    $pLog->info( "Determining actual insert sizes for libraries in set $setId..." );

    foreach $libName( @setLibs )
    {
      my $libInsertAvg =      $pLibInfo->get_lib_prop($libName, LIB_PROP_estInsAvg, $pLog);
      my $libInsertSdev =     $pLibInfo->get_lib_prop($libName, LIB_PROP_estInsSdev, $pLog);
      my $libIsRevComp =         $pLibInfo->get_lib_prop($libName, LIB_PROP_isRevComped, $pLog);
      my $libHasInnieArtifact =  $pLibInfo->get_lib_prop($libName, LIB_PROP_hasInnieArtifact, $pLog);
      my $lib5P_wiggleRoom =     $pLibInfo->get_lib_prop($libName, LIB_PROP_5primeWiggleRoom, $pLog);

       @{$blastMaps{$libName}} = (glob "$assembly_dir/".stage_before($g_stage)."/blastMap.$libName.f*.merged");

       foreach my $blastMap ( @{$blastMaps{$libName}} )
       {
	   # XXX - -M minFreqReported may be needed
	   $cmd = "perl $blastMapAnalyzer -b $blastMap -m $minMatch ".
	       "-I $libInsertAvg:$libInsertSdev ".
	       ( $libIsRevComp ? " -R " : "" ).
	       ( $libHasInnieArtifact ? " -A " : "" ).
	       ( defined( $prevRoundSrf ) ? " -S $prevRoundSrf " : "" ).
	       ( $lib5P_wiggleRoom  ? " -F $lib5P_wiggleRoom " : "" );

	   push ( @commands, $cmd );
	   push ( @stdouts, "$work_dir/ROUND_$setId/".(basename($blastMap)).".insertSize.dist" );
	   push ( @stderrs, "$work_dir/ROUND_$setId/".(basename($blastMap)).".insertSize.dist.err" );
	   push ( @workingDirs, "$work_dir" );
       }
    }
    run_command_set( "insertSizeDist.$setId", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, $ram_request_override, undef, undef);


    # 2. full blast map analysis using the calculated insert sizes

    $pLog->info( "Building pair profiles for libraries in set $setId..." );

    my @badLibs;

    foreach $libName( @setLibs )
    {
	my $libInsertAvg =         $pLibInfo->get_lib_prop($libName, LIB_PROP_estInsAvg, $pLog);
	my $libInsertSdev =        $pLibInfo->get_lib_prop($libName, LIB_PROP_estInsSdev, $pLog);
	my $libIsRevComp =         $pLibInfo->get_lib_prop($libName, LIB_PROP_isRevComped, $pLog);
	my $libHasInnieArtifact =  $pLibInfo->get_lib_prop($libName, LIB_PROP_hasInnieArtifact, $pLog);
	my $lib5P_wiggleRoom =     $pLibInfo->get_lib_prop($libName, LIB_PROP_5primeWiggleRoom, $pLog);


	my $libInsertDistGlob = "$work_dir/ROUND_$setId/blastMap.$libName.*.insertSize.dist";
	unless (calculate_avg_lib_insert_size($libInsertDistGlob, $libHasInnieArtifact, \$bootstrappedInsertAverages{$libName}, \$bootstrappedInsertSdevs{$libName}))
	# if there was not enough suitable data to calculate insert size distribution, 
	# either fall back on the user estimate or skip this library altogether
	{
	    $pLog->error( "WARNING:  Couldn't build insert size distribution for library $libName!  Plese check that the contigs/scaffolds are long enough to map read pairs from this library!");
	    if ( $fallbackOnEst ) 
	    {
		$pLog->info( "Will fall back on user's estimated avg and stdev values... " );
		$bootstrappedInsertAverages{$libName} = $libInsertAvg;
		$bootstrappedInsertSdevs{$libName} =  $libInsertSdev;
	    }
	    else
	    {
		$pLog->info( "Will not use this library for scaffolding... " );
		push @badLibs, $libName;
		next;
	    }	    
	}

	if ( $bootstrappedInsertAverages{$libName} > $maxAvgInsertThisRound) { $maxAvgInsertThisRound = $bootstrappedInsertAverages{$libName} }
	
	if ( $prevRoundMaxInsert && ( $bootstrappedInsertAverages{$libName} < $prevRoundMaxInsert ) )
	{
	    $pLog->error("ERROR: The average insert size for library $libName is smaller than the largest insert size in the previous scaffolding round. Consider re-defining or re-ordering your sets in the config file.");
	    return JGI_FAILURE;
	}
	
	if ($diploid_mode == 2  && defined($prevRoundSrf)) {
	    # compile a list of contigs to skip based on previous rounds' ono outputs
	    $spannerMaskList = "$work_dir/ROUND_$setId/mask.in";
	    my $cmd = "cat $work_dir/p*mask.out";  
	    my $out = $spannerMaskList;
	    run_single_cmd( $cmd, $out, undef, undef, $pLog);
	}

	foreach my $blastMap ( @{$blastMaps{$libName}} )
	{
	    $cmd = "perl $spanner -b $blastMap -m $minMatch -i $bootstrappedInsertAverages{$libName}:$bootstrappedInsertSdevs{$libName} ".
                ( $libHasInnieArtifact ? " -I " : "" ).
                ( $libIsRevComp ? " -R " : "" ).
                ( defined( $prevRoundSrf ) ? " -S $prevRoundSrf " : "" ).
                ( defined( $spannerMaskList ) ? " -X $spannerMaskList " : "" ).
		( $lib5P_wiggleRoom  ? " -F $lib5P_wiggleRoom " : "" );
	    
            push ( @commands, $cmd );
            push ( @stdouts, "$work_dir/ROUND_$setId/".(basename($blastMap)).".spans.bma" );
            push ( @stderrs, "$work_dir/ROUND_$setId/".(basename($blastMap)).".spans.bma.err" );
            push( @workingDirs, "$work_dir" );
	}


  	# save the new insert size values as pLibInfo properlies and also as local params to be used later
	$pLibInfo->set_lib_prop($libName, LIB_PROP_trueInsAvg, $bootstrappedInsertAverages{$libName}, $pLog);
	$pLibInfo->set_lib_prop($libName, LIB_PROP_trueInsSdev, $bootstrappedInsertSdevs{$libName}, $pLog);
	$pLocalParams->put_param( LOCAL_PARAM_lib_insert_size_recalc, "$libName $bootstrappedInsertAverages{$libName} $bootstrappedInsertSdevs{$libName}", $pLog );

    }
    if ($#badLibs == $#setLibs)  #there were no good libraries in this set
    {
	$pLog->error("ERROR: None of the libraries in set $setId were deemed suitable for scaffolding!  Please check library parameters or consider excluding this set altogether." );
	return JGI_FAILURE;
    }
    run_command_set( "spanner.$setId", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, $ram_request_override, undef, undef); 



    #3. populate data lists needed for scaffolding using libraries of this set

    foreach $libName(@setLibs)
    {
	next if ($libName ~~ @badLibs);
	my $libIsRevComp =         $pLibInfo->get_lib_prop($libName, LIB_PROP_isRevComped, $pLog);
	my $libHasInnieArtifact =  $pLibInfo->get_lib_prop($libName, LIB_PROP_hasInnieArtifact, $pLog);
	my $libUseForGapClosing =  $pLibInfo->get_lib_prop($libName, LIB_PROP_forGapClosing, $pLog);
  
	# add the splinting bmas if this library qualifies 
	if ( $setId == 1 && $libUseForGapClosing )
	{
	    foreach my $bmaFile(glob("$work_dir/SPLINTING/blastMap.${libName}.*.splints.bma")) {
		run_single_cmd("ln -sf ../SPLINTING/".basename($bmaFile)." $work_dir/ROUND_$setId/", undef, undef, undef, $pLog);   # make relative symlink for better portability
	    }
	}
	
	print BMA_DATA_OUT "$libName\t$bootstrappedInsertAverages{$libName}\t$bootstrappedInsertSdevs{$libName}\t$work_dir/ROUND_$setId/blastMap.${libName}.*.bma\n";	
	
	if ( length( $innieListStr ) ) { $innieListStr .= ","; }
	if ( $libHasInnieArtifact ) { $innieListStr .= "$libName"; }
	
	if ( length( $linkDataLibStr ) ) { $linkDataLibStr .= ","; }
	$linkDataLibStr .= "$libName";
	if ( length( $linkDataInsertSizeStr ) ) { $linkDataInsertSizeStr .= "," ; }
	$linkDataInsertSizeStr .= $bootstrappedInsertAverages{$libName};
	if ( length( $linkDataInsertSdevStr ) ) { $linkDataInsertSdevStr .= ","; }
	$linkDataInsertSdevStr .= $bootstrappedInsertSdevs{$libName} ;
	
    }
    close BMA_DATA_OUT;

    print LINK_DATA_OUT "$linkDataLibStr\t$linkDataInsertSizeStr\t$linkDataInsertSdevStr\t$linkFile\n";
    close LINK_DATA_OUT;

    #4. generate scaffolds using min links cutoff from 3 to 10, then pick the best set based on N50
    $pLog->info("Generating scaffolds for oNo round $setId ...");

    push( @commands, "perl $bmaToLinks -m $mer_size -b $bmaDataFile "
	  .( length( $innieListStr ) ? "-I $innieListStr " : "" )
 	  .( $prevRoundMaxInsert ? "-P $prevRoundMaxInsert " : "" )  );
    push( @stdouts, "$linkFile" );
    push( @stderrs, "${linkFile}.err" );
    push( @workingDirs, "$work_dir" );
    run_command_set( "bmaToLinks.$setId", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, undef, \&validate_stdout, undef);


    
    my $fMax = 10;
    
    #in diploid modes we expect a lot of variant-induced forks, so we keep the p-cutoff at or below the overall min_depth cutoff so as not to lose them.
    if ($diploid_mode && $meraculous_min_depth_cutoff < $fMax) {
	$fMax = $meraculous_min_depth_cutoff+1;
    }
    
    for ( my $f = 3; $f <= $fMax; $f++ )
    {
	my $srfFile = "$work_dir/ROUND_$setId/p$f.$setId.srf";
	$cmd = "perl $ono -m $mer_size -l $linkDataFile ".( defined( $prevRoundSrf ) ? " -s $prevRoundSrf " : " -c $crf " )." -p $f ".( $diploid_mode ? " -P $diploid_mode -D $diploid_depth_threshold" : "" );
	push ( @commands, $cmd );
	push ( @stdouts, "$srfFile" );
	push ( @stderrs, "${srfFile}.err" );
	push ( @workingDirs, "$work_dir" );
    }
    run_command_set( "oNo.$setId", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, undef, \&validate_stdout, undef);


    my %N50s;
    my $n50file;
    for( my $f = 3; $f <= $fMax; $f++ )
    {
        my $srfFile = "$work_dir/ROUND_$setId/p$f.$setId.srf";
	$cmd = "grep CONTIG $srfFile | perl $optimize -f - -g 1 -v 5 | awk '{print \$1,\$5}' | perl $n50 - 1 ";
	$out = "$work_dir/ROUND_$setId/p$f.$setId.N50";
	run_single_cmd( $cmd, $out, undef, undef, $pLog);

	$n50file = "$work_dir/ROUND_$setId/p$f.$setId.N50";
	open(FH, '<', "$n50file") || die $pLog->error("Couldn't open $n50file");
	while (<FH>){
	    my @line = split(/\s+/, $_);
	    if ($line[4] > 0.5){
		$N50s{$f} = $line[2];
		last;
	    }
	}
	close FH;
    }
    my $topN50p = (sort {$N50s{$b} <=> $N50s{$a}} keys %N50s)[0];
    my $topN50srf = "$work_dir/ROUND_$setId/p$topN50p.$setId.srf";

    $pLog->debug("Best p-cutoff for Round $setId: $topN50p");

    `cp $topN50srf $work_dir/`;
    $prevRoundSrf = "$work_dir/p$topN50p.$setId.srf";
    $pLocalParams->put_param( LOCAL_PARAM_prev_round_srf, $prevRoundSrf, $pLog );

    if ($diploid_mode == 2) {
	for ( my $f = 3; $f <= 10; $f++ ) {
	    if ( $f == $topN50p ) {
		`mv $work_dir/p$f.mask.out  $work_dir/p$f.$setId.mask.out`;
	    } else {
		unlink "$work_dir/p$f.mask.out";
	    }
	}
    }    


    # make a record, soft and hard, of the largest insert size average for this round (to be used in the following rounds or upon resume)
    $prevRoundMaxInsert = $maxAvgInsertThisRound;
    $pLocalParams->put_param( LOCAL_PARAM_prev_round_max_insert, $prevRoundMaxInsert, $pLog );
    write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );
  }
  ### end of scaffolding rounds


  $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "ono_complete", $pLog );
  write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );

 ono_complete:

  `cp  $inputContigsFa $contigsFile`;
  if ($diploid_mode) {

      if ($diploid_mode == 2)     {
	  # add any newly created contig copy-contigs to the current contigs file	
	  my $cmd = "grep \"\.cp\" $prevRoundSrf | cut -f 3 | sed 's|[+-]||g' | sed -r 's|\.cp[0-9]*||g' | perl $screenList2 -l - -f $inputContigsFa -k | sed '/>.*/ s/\$/.cp/' >> $contigsFile";
	  run_single_cmd( $cmd, undef, undef, undef, $pLog);

	  # This mode involves lower-case masking, so we convert all sequence back to uppercase
	  open(FAIN,"<$contigsFile");
	  open(FAOUT,">${contigsFile}.uc");
	  while(<FAIN>) {
	      unless ($_ =~ /^>/) {
		  $_ =~ tr/actgn/ACTGN/;
	      }
	      print FAOUT $_;
	  } 
	  close FAIN; close FAOUT;
	  `mv ${contigsFile}.uc $contigsFile`;
	  
      }
      if ($diploid_mode == 1 )     {
	  if ( $noStrictHaplotypes ) {
	      my $cmd = "ln -s ./".basename($prevRoundSrf)." $finalSrf";
              run_single_cmd( $cmd, undef, undef, undef, $pLog);

	      # create a list of alternative variant singleton scaffolds.  Here we just take all _p2 containing scaffolds.
	      $cmd = "awk '\$3 ~ /_p2/{print \$1}' $finalSrf";
	      my $out = "$work_dir/alt.variant.scaffold.list";
	      my $err = "$work_dir/alt.variant.scaffold.list.err";
	      run_single_cmd( $cmd, $out, $err, undef, $pLog);
	  }
	  else  {
	      #  We resolved bubbles in a haplotype-sensitive fashion, but we scaffolded in a haplotype-agnostig fashion, 
	      #  so now we need to phase the variant diplotigs in the scaffolds to a consistent haplotype.
	      my $haploTypedSrf = "${prevRoundSrf}.htyped";
	      my $bubbleClonesMapFile = "$assembly_dir/meraculous_bubble/clones2diplotigs.map";
	      push( @commands, "perl $haplotyper -s $prevRoundSrf -c $bubbleClonesMapFile ");
	      push( @stdouts, $haploTypedSrf );
	      push( @stderrs, $haploTypedSrf.".err" );
	      push( @workingDirs, "$work_dir" );
	      run_command_set( "haplotyper", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, undef, \&validate_stdout, undef);

	      my $cmd = "ln -s ./".basename($haploTypedSrf)." $finalSrf";
	      run_single_cmd( $cmd, undef, undef, undef, $pLog);
	  }
      }
      else {
	  my $cmd = "ln -s ./".basename($prevRoundSrf)." $finalSrf";
	  run_single_cmd( $cmd, undef, undef, undef, $pLog);

      }
  }	
  else {
      my $cmd = "ln -s ./".basename($prevRoundSrf)." $finalSrf";
      run_single_cmd( $cmd, undef, undef, undef, $pLog);
  }

  return JGI_SUCCESS;
}

sub check_outputs_stage_meraculous_ono
{
    return JGI_SUCCESS;
}

sub cleanup_stage_meraculous_ono
{
    # this should still allow any stage to run or be redone
    my ($work_dir) = @_;

    $pLog->info("Deleting intermediate files..");

    unlink glob "$work_dir/SPLINTING/*.bma";
    unlink glob "$work_dir/ROUND_*/*.bma";
    unlink glob "$work_dir/ROUND_*/*.insertSize.dist";
    unlink "$work_dir/tmp.fa";
    `touch $work_dir/CLEANED-UP.1`;

    # if aggressive cleanup is selected we delete additional intermediate files that are not needed in the 
    # subsequent stages but which may be required if user wants to rerun this stage or a prior one.
    if ($cleanupLevel ==2)
    {
    }
    return JGI_SUCCESS;
}






### Stage: meraculous_gap_closure
#----------------------------
# The purpose of this stage is to:
#
# - Use the gap size and pairing information obtained in the previous stage to walk across gaps between contigs 
# 
#
# The output is the final set of scaffolds of 1kb and over in total length
#
sub check_inputs_stage_meraculous_gap_closure
{
    if ($g_restarting)    {
	$pLog->info("Checking integrity of prior stages... ");
	if (is_stage_cleaned_up( stage_before($g_stage), $assembly_dir, $pLog) || is_stage_cleaned_up("meraculous_import", $assembly_dir, $pLog) || is_stage_cleaned_up("meraculous_merblast", $assembly_dir, $pLog)) {
	    $pLog->error("Integrity check failed! "); 
	    return JGI_FAILURE;
	}
	$pLog->info("ok");
    }
    return JGI_SUCCESS;
}
sub run_meraculous_gap_closure
{
  my ($work_dir) = @_;
  my $cmd; my $out; my $err;
  my @commands = (); my @stdouts = (); my @stderrs = (); my @workingDirs = ();

  # user params
  my $meraculous_min_depth_cutoff = $pUserParams->get_param( USER_PARAM_min_depth_cutoff, $pLog )       || $pLocalParams->get_param( LOCAL_PARAM_autocalibrated_min_depth_cutoff, $pLog ) ;
  my $bubble_depth_threshold =      $pUserParams->get_param( USER_PARAM_bubble_depth_threshold, $pLog ) || $pLocalParams->get_param( LOCAL_PARAM_autocalibrated_bubble_depth_threshold, $pLog );
  my $mer_size =                    $pUserParams->get_param( USER_PARAM_mer_size,     $pLog )           || $pLocalParams->get_param( LOCAL_PARAM_autoselected_mer_size, $pLog );

  my $diploid_mode =                $pUserParams->get_param( USER_PARAM_diploid_mode, $pLog ) || 0;

  my $aggressive_flag =             $pUserParams->get_param( USER_PARAM_gap_close_aggressive, $pLog );

  my $merauderRepeatCopyCount =     $pUserParams->get_param( USER_PARAM_gap_close_rpt_depth_ratio, $pLog ) || 2;  

  my $fallbackOnEst =                $pUserParams->get_param( USER_PARAM_fallback_on_est_insert_size, $pLog );

  my $nodes =                      ( $pUserParams->get_param( USER_PARAM_use_cluster, $pLog ) && $pUserParams->get_param( USER_PARAM_cluster_num_nodes, $pLog ) ) ?
			             $pUserParams->get_param( USER_PARAM_cluster_num_nodes, $pLog ) :  1 ;

  my $cores =                      ( $pUserParams->get_param( USER_PARAM_use_cluster, $pLog ) ? 
				     $pUserParams->get_param( USER_PARAM_cluster_slots_per_task, $pLog ) :
				     $pUserParams->get_param( USER_PARAM_local_num_procs, $pLog ))
                                  || 1;     # In this stage we don't employ multi-threaded processes. Instead, this value is used when partitioning the gapData files to help produce reasonably-sized chunks. 
                                            # We're bending the rules a little here since cluster_slots_per_task is supposed to be for threaded tasks only, but we're using it as a general measure of the 
                                            # number of cores available

  # stage_inputs

  my $contigsFile = "$assembly_dir/".stage_before($g_stage)."/contigs.fa";
  my $crf = "$assembly_dir/meraculous_merblast/contigs.crf";
  my $srf = "$assembly_dir/".stage_before($g_stage)."/scaffolds.srf";

  # executables
  my $unique = $release_root.PATH_UNIQUE;
  my $gapDivider =  $release_root.PATH_GAPDIVIDER;
  my $gapPlacer = $release_root.PATH_GAP_PLACER;
  my $merauder = $release_root.PATH_MERAUDER;
  my $scaffReportToFasta = $release_root.PATH_SCAFF_REPORT_TO_FASTA;
  my $screenList2 = $release_root.PATH_SCREEN_LIST2;
  my $scaffold2contig = $release_root.PATH_SCAFFOLD2CONTIG;



  my $gapClosuresFile = "$work_dir/merauder.gapClosures";
  my @blastMaps;
  my $gapDataFileList = "$work_dir/merauder.gapDataInfo";


  ### from here on, use only the libs earmarked for gap closure

  my @libsForGaps;
  $pLog->info("Generating gap closing information ...");

  # determine a unified q-score encoding offset to convert everything to.  Use the majority rule since different libraries can initially have
  #  different formats defined for them.  This offset will be used at several steps during gap closing, thus we're setting it apriori.
  # Also determine the maximum insert size across all libraries.  This will be used as the maximum legth a  contig can be extended

  my $cnt33 = my $cnt64 = 0;
  my $globalqOffset;
  my $maxAvgInsertSize = 0;

  foreach my $libName ( @{ $pLibInfo->get_lib_names($pLog)} )
  {
      next unless $pLibInfo->get_lib_prop($libName, LIB_PROP_forGapClosing, $pLog);
      push(@libsForGaps, $libName);

      my $libQOffset =  $pLibInfo->get_lib_prop($libName, LIB_PROP_qOffset, $pLog);
      if ($libQOffset == 64 ) { $cnt64++; }
      if ($libQOffset == 33 ) { $cnt33++; }

      my $libInsertAvg = $pLibInfo->get_lib_prop($libName, LIB_PROP_trueInsAvg, $pLog);
	  if (! defined $libInsertAvg )
	  {
	      if ($fallbackOnEst) {
		  $pLog->info("Warning: could not obtain true (assembly-based) insert average for library $libName; Will fall back on user-provided estimates");
		  $libInsertAvg = $pLibInfo->get_lib_prop($libName, LIB_PROP_estInsAvg, $pLog);
	      }
	      elsif (! $pLibInfo->get_lib_prop($libName, LIB_PROP_forScaffRound, $pLog)) {
		  $pLog->info("Warning: library $libName wasn't used for scaffolding, so we don't have accurate insert size info for it. Will fall back on user-provided estimates");
		  $libInsertAvg = $pLibInfo->get_lib_prop($libName, LIB_PROP_estInsAvg, $pLog);		  
	      }
	      else {
		  $pLog->error("ERROR: Could not obtain true (assembly-based) insert average for library $libName!");
		  return JGI_FAILURE;
	      }
	  }

      if ($libInsertAvg > $maxAvgInsertSize) { $maxAvgInsertSize = $libInsertAvg; }
  }

  if (! $cnt33 && ! $cnt64 )
  {
      $pLog->error( "ERROR: Could not determine the quality offsets for the gap-closing libraries.  Please make sure that at least one library is earmarked for gap cl\
osure!");
      return JGI_FAILURE;
  }
  $globalqOffset = (($cnt64 >= $cnt33) ? 64 : 33);

  if (! $maxAvgInsertSize )
  {
      $pLog->error( "ERROR: Could not determine the maximum insert size accross all gap-closing libraries.  Please make sure that at least one library is earmarked for gap closure!");
      return JGI_FAILURE;
  }

  if ( $g_resuming ) { 
      my $localResumeCheckpoint = $pLocalParams->get_param( LOCAL_PARAM_local_resume_checkpoint, $pLog );
      goto $localResumeCheckpoint unless  ($localResumeCheckpoint eq "none");
  }


  open( GAP_DATA_FILE_LIST, ">$gapDataFileList" );
  foreach my $libName (@libsForGaps)
  {
      my $libInsertAvg =         $pLibInfo->get_lib_prop($libName, LIB_PROP_trueInsAvg, $pLog);
      my $libInsertSdev =        $pLibInfo->get_lib_prop($libName, LIB_PROP_trueInsSdev, $pLog);
      my $lib5P_wiggleRoom =     $pLibInfo->get_lib_prop($libName, LIB_PROP_5primeWiggleRoom, $pLog);
      my $libQOffset =           $pLibInfo->get_lib_prop($libName, LIB_PROP_qOffset, $pLog);
      my $libIsRevComp =         $pLibInfo->get_lib_prop($libName, LIB_PROP_isRevComped, $pLog);

      @blastMaps = glob "$assembly_dir/meraculous_merblast/blastMap.${libName}.f*.merged";
      for my $blastMap (@blastMaps)
      {
	  $blastMap =~ /\S+\.f(\d+)\.merged$/m;
	  my $i = $1;
	  my $fastqInfoFile = "$assembly_dir/meraculous_merblast/${libName}.fastq.info.$i";
	  $cmd = "perl $gapPlacer -b $blastMap -m $mer_size -i $libInsertAvg:$libInsertSdev -s $srf -f $fastqInfoFile -c $contigsFile" .
	      ( $lib5P_wiggleRoom   ? " -F $lib5P_wiggleRoom " : "" ).
	      ( $libIsRevComp       ? " -R " : "" ).
	      ( ($diploid_mode == 1 && $aggressive_flag) ? " -A " : "" );  # swap in reads from alt variant diplotigs 
	  push ( @commands, "$cmd" );
	  push ( @stdouts, "$work_dir/gapData.${libName}.$i" );
	  push ( @stderrs, "$work_dir/gapData.${libName}.$i.err" );
	  push( @workingDirs, "$work_dir" );   
	  print GAP_DATA_FILE_LIST "$work_dir/gapData.${libName}.$i\t$libQOffset\n";
      }
  }
  close GAP_DATA_FILE_LIST;
  run_command_set( "gapPlacer", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, undef, \&validate_stdout, undef);

  $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "gap_placement_complete", $pLog );
  write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );

gap_placement_complete:

  ### merge library-specific gap data so that each gap has complete information. Then divide.

  run_single_cmd( "mkdir -p $work_dir/GAPS", undef, undef, undef, $pLog);

  # partition the gap data 
  my $nGaps = `awk '{print \$1,\$2,\$3,\$4,\$5}' $work_dir/gapData.*[0-9] | sort -u| wc -l`;
  chomp $nGaps;
  my $n = ceil($nGaps/($nodes*$cores));

  # gapDivider will adjust all qscore ASCII encodings to a single globally chosen offset (33 or 64).  
  # This offset is determined based on the majority offset across all participating libs.  This allows to use input data with mixed qscore offsets.
  $cmd = "perl $gapDivider -o $work_dir/GAPS/gapData. -n $n -g $gapDataFileList -s $srf -Q $globalqOffset";   
  $out = undef;
  $err = "gapDivider.err";
  run_single_cmd( $cmd, $out, $err, 1, $pLog);

  $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "gap_data_complete", $pLog );
  write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );

gap_data_complete:

  ### Close the gaps

  $pLog->info("Closing gaps ...");

  my @gapDataFiles = glob("$work_dir/GAPS/gapData.*[0-9]");
  if (! @gapDataFiles ) 
      {
	 $pLog->error("ERROR: No files found matching $work_dir/GAPS/gapData.*[0-9]");
	 return JGI_FAILURE;
      }

  foreach my $gapDataFileTemp ( @gapDataFiles )
  {
    chomp $gapDataFileTemp;
    $cmd = "$merauder -V -i $maxAvgInsertSize -s $srf -m $mer_size -c $contigsFile -g $gapDataFileTemp -D $meraculous_min_depth_cutoff -R $merauderRepeatCopyCount -Q $globalqOffset".($aggressive_flag ? " -A " : "").( $diploid_mode ? " -P " : "" );

	push ( @commands, $cmd );
    push ( @stdouts, "$gapDataFileTemp.closures" );
	push ( @stderrs, "$gapDataFileTemp.closures.err" );
	push( @workingDirs, "$work_dir" );   
  }

  run_command_set( "gapCloser", $pUserParams, \@commands, \@stdouts, \@stderrs, \@workingDirs, $pLocalParams, $pLog, undef, undef, undef);  # no output validation here bc we're not guaranteed that any gaps will be closed.

  $cmd = "find $work_dir/GAPS/ -name \"*.closures\" -exec cat {} \\;";
  $out = $gapClosuresFile;
  $err = "$gapClosuresFile.err";
  run_single_cmd( $cmd, $out, $err, 1, $pLog);


  $pLocalParams->put_param( LOCAL_PARAM_local_resume_checkpoint, "gap_closing_complete", $pLog );
  write_stage_params_file( $pLocalParams, $g_stage, $pLog, $assembly_dir );

 gap_closing_complete:

  $pLog->info("Building final scaffolds ...");

  my $unfilteredScaffoldFile = "$work_dir/final.scaffolds.fa.unfiltered";
  $cmd = "perl $scaffReportToFasta -c $contigsFile -s $srf -g $gapClosuresFile";
  $out = $unfilteredScaffoldFile;
  $err = $unfilteredScaffoldFile.".err";
  run_single_cmd( $cmd, $out, $err, 1, $pLog);

  my $singleHaplotypeScaffoldFile = undef;
  my $altVariantsFile = undef;
  if ($diploid_mode == 1 ) {
      # screen out alt variant scaffolds
      $altVariantsFile = "$assembly_dir/meraculous_ono/alt.variant.scaffold.list";
      $singleHaplotypeScaffoldFile = "$work_dir/final.scaffolds.unfiltered.single-haplotype.fa";
      $cmd = "perl $screenList2 -l $altVariantsFile -f $unfilteredScaffoldFile";  
      $out=  $singleHaplotypeScaffoldFile;
      $err = $singleHaplotypeScaffoldFile.".err";
      run_single_cmd( $cmd, $out, $err, 1, $pLog);
  }



  # screen out tiny scaffolds ( esp. 0-length ones ): anything less than "k"
  my $min_scaffold_size_to_report = 1000;
  my $filteredScaffoldList = "$work_dir/final.scaffolds.list";
  my $filteredScaffoldFile = "$work_dir/final.scaffolds.fa";

  my $fastaLengthsHashRef = fasta_lengths($unfilteredScaffoldFile, $pLog);
  open (FILTERED, ">$filteredScaffoldList");
  for ( keys %$fastaLengthsHashRef )  { if ($$fastaLengthsHashRef{$_} >= $min_scaffold_size_to_report) { print FILTERED "$_\n" } }
  close FILTERED;

  $cmd = "perl $screenList2 -l $filteredScaffoldList -f ".  ($diploid_mode == 1 ? $singleHaplotypeScaffoldFile : $unfilteredScaffoldFile)." -k -c 1";
  $out = $filteredScaffoldFile;
  $err = undef;
  run_single_cmd( $cmd, $out, $err, 1, $pLog);

  $cmd = "perl $scaffold2contig $filteredScaffoldFile";
  $out=  "$work_dir/final.contigs.fa";
  $err = "$work_dir/final.contigs.err";
  run_single_cmd( $cmd, $out, $err, 1, $pLog);
  

  return JGI_SUCCESS;
}
sub check_outputs_stage_meraculous_gap_closure
{
    return JGI_SUCCESS;
}

sub cleanup_stage_meraculous_gap_closure
{
    # this should still allow any stage to run or be redone
    my ($work_dir) = @_;
    my $touchfile;
    $pLog->info("Deleting intermediate files..");
    unlink glob "$work_dir/*gapData.*[0-9]";
    unlink glob "$work_dir/GAPS/*gapData.*[0-9]";
    unlink glob "$work_dir/GAPS/gapData*closures";
    $touchfile = "$work_dir/CLEANED-UP.1";
    `touch $touchfile`;

    # if aggressive cleanup is selected we delete additional intermediate files that are not needed in the 
    # subsequent stages but which may be required if user wants to rerun this stage or a prior one.
    if ($cleanupLevel ==2)
    {
	unlink glob "$assembly_dir/".stage_before($g_stage)."/ROUND_*/*srf";
	unlink glob "$assembly_dir/".stage_before($g_stage)."/*srf";
	$touchfile = "$assembly_dir/".stage_before($g_stage)."/CLEANED-UP.2";
        `touch $touchfile`;

	unlink glob "$assembly_dir/meraculous_merblast/blastMap.*merged";
	$touchfile = "$assembly_dir/meraculous_merblast/CLEANED-UP.2";
	`touch $touchfile`;
    }
    return JGI_SUCCESS;
}






### Stage: meraculous_final_results
#----------------------------
# The purpose of this stage is to:
#
# - Generate a basic summary report on the final assembly
# - Delete various intermediate files from all prior stage directories in order to reduce disk space footprint
#
# The output is a brief summary of the assembly inputs and results
#
sub check_inputs_stage_meraculous_final_results
{
    return JGI_SUCCESS;
}

sub run_meraculous_final_results
{
  my ($work_dir) = @_;

  my $diploid_mode =                $pUserParams->get_param( USER_PARAM_diploid_mode, $pLog );
  my $genome_size =                 $pUserParams->get_param( USER_PARAM_genome_size, $pLog );
  $genome_size *= (10**9);
  
  # stage inputs
  my $finalScaffoldsFile = "$assembly_dir/meraculous_gap_closure/final.scaffolds.fa";
  my $finalContigsFile = "$assembly_dir/meraculous_gap_closure/final.contigs.fa";
  my $uutigsFile = "$assembly_dir/meraculous_contigs/UUtigs.fa";
  my $inputSummaryFile = "$assembly_dir/meraculous_import/".INPUT_SUMMARY_FILE ;

  run_single_cmd( "ln -s ../".stage_before($g_stage)."/".basename($finalScaffoldsFile)." $work_dir/", undef, undef, undef, $pLog);

  $pLog->info("Generating summary report ...");

  open( SUMMARY_REPORT, ">SUMMARY.txt" );
  print SUMMARY_REPORT "Assembly_dir: ".$assembly_dir."\n\n";
  print SUMMARY_REPORT "== Key Parameters ==\n\n";
  print SUMMARY_REPORT "Library info:\n";
  print SUMMARY_REPORT sprintf("%12s%18s%18s%16s%26s%18s", "Name", "Insert_estimated", "Insert_calculated", "For_contigs?", "For_scaffolding?(round)", "For_gap-closure?")."\n";

  foreach my $libName ( @{ $pLibInfo->get_lib_names($pLog)} )
  {
      my $libInsertEst =         $pLibInfo->get_lib_prop($libName, LIB_PROP_estInsAvg, $pLog);
      my $libInsertRecalc  =     $pLibInfo->get_lib_prop($libName, LIB_PROP_trueInsAvg, $pLog);
      my $libUseForContigs =     $pLibInfo->get_lib_prop($libName, LIB_PROP_forContigging, $pLog);
      my $libSetId =             $pLibInfo->get_lib_prop($libName, LIB_PROP_forScaffRound, $pLog);
      my $libUseForGapClosing =  $pLibInfo->get_lib_prop($libName, LIB_PROP_forGapClosing, $pLog);

      print SUMMARY_REPORT 
	  sprintf( "%12s", $libName).
	  sprintf( "%18s", $libInsertEst).
	  sprintf( "%18s", ($libInsertRecalc || "n/a") ).
	  sprintf( "%16s", ($libUseForContigs ? "y" : "n") ).
	  sprintf( "%26s", ($libSetId ? $libSetId : "n") ).
	  sprintf( "%18s", ($libUseForGapClosing? "y" : "n") )."\n";
  }
  print SUMMARY_REPORT "\nMer_size: ".($pUserParams->get_param( USER_PARAM_mer_size, $pLog ) 
                                    || $pLocalParams->get_param( LOCAL_PARAM_autoselected_mer_size, $pLog ) )."\n";
  print SUMMARY_REPORT "\nMin_depth: ".( $pUserParams->get_param( USER_PARAM_min_depth_cutoff, $pLog ) 
                                    || $pLocalParams->get_param( LOCAL_PARAM_autocalibrated_min_depth_cutoff, $pLog ))."\n";

  if ($diploid_mode) {
      print SUMMARY_REPORT "\nDiploid mode: $diploid_mode\n";
      print SUMMARY_REPORT "\nDiploid_depth_threshold: ".( $pUserParams->get_param( USER_PARAM_bubble_depth_threshold, $pLog ) 
                                    || $pLocalParams->get_param( LOCAL_PARAM_autocalibrated_bubble_depth_threshold, $pLog ))."\n";
  }
  print SUMMARY_REPORT "\n\n";

  open( INPUTS_REPORT, "<$inputSummaryFile" ) || die $pLog->error("Cannot open $inputSummaryFile");
  while (<INPUTS_REPORT>) { 
      print SUMMARY_REPORT $_; 
  }
  close INPUTS_REPORT;


  if ( -s $finalScaffoldsFile) {
      # print final scaffold stats
      my $finalScaffoldSizeInfo = getFastaSizeInfo( $finalScaffoldsFile, $pLog, $genome_size );
      $finalScaffoldSizeInfo->{ "label" } = "Final Scaffolds\t";
      my $finalContigSizeInfo = getFastaSizeInfo( $finalContigsFile, $pLog, $genome_size );
      $finalContigSizeInfo->{ "label" } = "Final Contigs\t";
      my $preGapClosedContigSizeInfo = getFastaSizeInfo( $uutigsFile, $pLog, $genome_size );
      $preGapClosedContigSizeInfo->{ "label" } = "Starting UUtigs\t";
      
      print SUMMARY_REPORT "\n\n";
      print SUMMARY_REPORT "== Assembly Stats ===\n\n";
      print SUMMARY_REPORT "Description     \tcnt\ttotal\tmin\tmax\tN50_stats\tNG50_stats\n";
      print SUMMARY_REPORT "-" x 78;
      print SUMMARY_REPORT "\n";

      foreach my $s ( $finalScaffoldSizeInfo, $finalContigSizeInfo, $preGapClosedContigSizeInfo )
      {
	  print SUMMARY_REPORT 
	      $s->{ "label" }."\t".
	      $s->{ "cnt" }."\t".
	      sprintf( "%.1f", $s->{ "totalBases" }/1000000 )."Mb\t".
	      sprintf( "%.1f", $s->{"minSize"}/1000 )."Kb\t".
	      sprintf( "%.1f", $s->{"maxSize"}/1000 )."Kb\t".
	      $s->{ "N50Cnt" }." > ".sprintf( "%.1f", $s->{"N50Size"}/1000  )."Kb\t".
	      #"totalling ". sprintf( "%.1f", $s->{ "N50TotalBases" } / 1000000 )."Mb\t".
	      ($genome_size ?  $s->{ "NG50Cnt" }." > ".sprintf( "%.1f", $s->{"NG50Size"}/1000  )."Kb\n"  :  "\n");
      }
  }
  print SUMMARY_REPORT "\n\n";
  close SUMMARY_REPORT;

  return JGI_SUCCESS;
}

sub check_outputs_stage_meraculous_final_results
{
  return JGI_SUCCESS;
}

sub cleanup_stage_meraculous_final_results
{
    my ($work_dir) = @_;
    my $touchfile = "$work_dir/CLEANED-UP.1";
    # if aggressive cleanup is selected we delete additional intermediate files that are not needed in
    # subsequent stages but which may be required if user wants to rerun this stage or a prior one.
    if ($cleanupLevel ==2)
    {
	# Delete fragmented+renamed copies of the original sequence files.  Symlinks to the original reads are not touched
	$pLog->info("Deleting temporary copies of the original sequence files...");	
	unlink glob "$assembly_dir/meraculous_import/*fastq.*_*";
	$touchfile = "$assembly_dir/meraculous_import/CLEANED-UP.2";
        `touch $touchfile`;
    }

    return JGI_SUCCESS;
}



#-----------------------------------------------------------------
# FUNCTION DEFINITIONS
#-----------------------------------------------------------------


# Place any function definitions here.  A sample skeleton function is
# provided.
#
# Function: function_skeleton
# ---------------------------
# This is a skeleton outline of a local function.
# This function takes the following arguments:
#
# $_[0]     The first argument.
#
# $_[n]     The last argument.
#
# This function returns the following values:
#
# JGI_SUCCESS     The function successfully finished.
#
# JGI_FAILURE     Something went wrong.
#
sub function_skeleton
{

  # Please refer to the above function description for details of
  # what these variables are.
  my $in = @_;

  # A string to identify this function.
  my $function_id = "function_skeleton";

  #
  # If one gets here, everything must have worked, so return
  # success.
  #
  return JGI_SUCCESS;
}




#################################################################################
#
# Stage maintenance functions
#

sub first_stage
{
  return $stages[0];
}

sub last_stage
{
  return $stages[$#stages];
}

# Function: stage_before
# ---------------------------
# Returns the stage before the stage given as argument, as found in the global @stages array
# This function takes the following arguments:
#
# $_[0]               - stage name
#
# This function returns the following values:
#
# $stages[ $i - 1 ]   - previous stage
#
# undef               - stage not found in the @stages array (gloval variable)
#
sub stage_before
{
  my ($stage) = @_;

  for ( my $i = 1 ; $i <= $#stages ; $i++ )
  {
    next if ( $stage ne $stages[$i] );
    return $stages[ $i - 1 ];
  }
  return undef;
}

# Function: stage_after
# ---------------------------
# Returns the stage name following the stage given as argument, as found in the global @stages array 
# This function takes the following arguments:
#
# $_[0]               - stage name
#
# This function returns the following values:
#
# $stages[ $i + 1 ]   - next stage
#
# undef               - stage not found in the @stages array (gloval variable)
#
sub stage_after
{
  my ($stage) = @_;

  for ( my $i = 0 ; $i < $#stages ; $i++ )
  {
    next if ( $stage ne $stages[$i] );
    return $stages[ $i + 1 ];
  }
  return undef;
}


# Function: stage_is_after
# ---------------------------
# Checks if stage A is after stage B in the global @stages array 
# This function takes the following arguments:
#
# $_[0]               - stage1 name
# $_[1]               - stage2 name
#
# This function returns the following values:
#
# 1               - stage2 is before stage1 in the @stages array
#
# 0               - stage1 is before stage2 in the @stages array
# 0               - stage not found in the @stages array
#
sub stage_is_after
{
  my ( $stage1, $stage2 ) = @_;

  for ( my $i = 0 ; $i <= $#stages ; $i++ )
  {
    return 0 if ( $stage1 eq $stages[$i] );
    return 1 if ( $stage2 eq $stages[$i] );
  }
  return 0;
}


# Function: validate_stage_name
# ---------------------------
# Checks that the stage name is present in the global @stages array and sends the die signal if it doesnt
# This function takes the following arguments:
#
# $_[0]               - stage name
# $_[1]               - reference to the logging object
#
# This function returns the following values:
#
# undef               - stage name exists
#
sub validate_stage_name
{
  my ( $stage, $log ) = @_;

  my %stages = map { $_ => 1 } @stages;
  if ( defined $stage and not exists $stages{$stage} )
  {
    $log->error( "ERROR: $stage is not a valid stage.  Valid stages are:\n" . join( ', ', @stages ) );
    die;
  }
}


# Function: write_stage_params_file
# ---------------------------
# Writes a local parameter file for a given stage
# This function takes the following arguments:
#
# $_[0]                - reference to the local parameters object
# $_[1]                - stage name
# $_[2]                - reference to the logging object
# $_[3]                - global assembly directory path
#
sub write_stage_params_file
{
  my ( $pLocalParams, $stage, $pLog, $assembly_dir ) = @_;

  if ( defined($stage) )
  {
    my $local_params_file_name = "$assembly_dir/checkpoints/$stage.local.params";
    if ( $pLocalParams->config_print_all( $pLog, $local_params_file_name ) != JGI_SUCCESS )
    {
      $pLog->error("Params file creation failed for stage $stage!");
      die;
    }
  }
}





##############################################################
#
# Job/command execution functions
#



# Function: run_command_set
# -----------------------
# Configures and submits a set of commands for parallel execution either locally or to the cluster. 
# Calls M_Job_Set::run_job_set to perform the actual execution
#
# This function takes the following arguments:
#
# $_[0]        A name for this job set
# $_[1]        A reference to the user parameter object       
# $_[2]        A reference to an array containing the commands that will be executed
# $_[3]        A reference to an array containing the corresponding std output file names
# $_[4]        A reference to an array containing the corresponding std error file names
# $_[5]        A reference to an array containing the corresponding working directories
# $_[6]        A reference to the local parameter object
# $_[7]        A reference to the logging object
# $_[8]        Optional: A flag to override the global or stage-specific memory request value with a set-specific value  (typically used for low-memory cluster jobs)
# $_[9]        Optional: A reference to a function that will be called to check/validate the ouputs
# $_[10]        Optional: A flag to use with sets of threaded processes.  Ensures that no more than one job per node/host is executed at a time.
#
#
# This function returns the following values:
#
# JGI_SUCCESS     The function successfully finished.
#
sub run_command_set
{
    my ( $id, $pUserParams, $cmd_set_ref, $stdout_set_ref, $stderr_set_ref, $working_dir_set_ref, $pLocalParams, $pLog, $ram_request_override, $validation_function_ref, $mthreaded) = @_;  

    if ( !defined( $id ) ) { $id = "unnamed"; }
    if ( !defined( $validation_function_ref ) ) { $validation_function_ref = 0; }

    if ( @$cmd_set_ref  == 0 ) 
    {
	die $pLog->error("ERROR! Empty command set! Please verify that the expected inputs are in place!");

    }
    my $jobSetDir = $working_dir_set_ref->[0]."/JOB_SET_DIR.$id";
    run_single_cmd("mkdir -p $jobSetDir", undef, undef, undef, $pLog);

    my $bulk_cmd_report = $cmd_set_ref->[0];
    if (defined $stdout_set_ref)
    {
	$bulk_cmd_report .= " > ".$stdout_set_ref->[0];
    }
    if (defined $stderr_set_ref)
    {
	$bulk_cmd_report .= " 2> ".$stderr_set_ref->[0];
    }   
    $pLog->debug( "bulk job set of size ".scalar( @{ $cmd_set_ref } ). ":  $bulk_cmd_report\n" );


    #
    # Initialize the job set parameters,
    #
    
    my %job_parameters =
	(
	 (JOB_SET_SKIP_COMPLETED) => 1,       # always leave this on; if the user didn't want to resume, then the pipeline should've cleared out the touchfiles anyhow
	);
    # default to local run type  
    my $run_type = JOB_SET_RUN_TYPE_LOCAL;
    my $use_cluster = $pUserParams->get_param( USER_PARAM_use_cluster, $pLog );


    # maximum number of parallel processes for this job set  
    # (currently valid for local runs only;  cluster submitions are up for the scheduler to manage, so there is no max)

    my $max_jobs = $pUserParams->get_param( USER_PARAM_local_num_procs, $pLog ) || 1 ;  # default to 1 if user didn't specify a non-zero value for local_num_procs

    # override with stage-specific limit on the number of parallel jobs 
    my $num_procs_thisStage = $pUserParams->get_param( "num_procs_".${g_stage}, $pLog );
    if ( $num_procs_thisStage )
    {
	$max_jobs=$num_procs_thisStage;
    }

    # if the jobs use threading a single process can potentially occupy all the cores, so we'd always want to run just one processs in this case
    if ( $mthreaded )
    {
	$max_jobs=undef;
    }

    my $max_retry = $pUserParams->get_param( USER_PARAM_local_max_retries, $pLog );
    my $local_max_memory = $pUserParams->get_param( USER_PARAM_local_max_memory, $pLog );

    # define parameters for running on the cluster.
    if ( $use_cluster )
    {

	$run_type = JOB_SET_RUN_TYPE_CLUSTER;

	#$max_jobs = $pUserParams->get_param( USER_PARAM_cluster_num_jobs, $pLog );
        # currently disabled since cluster submitions are always done via a single multi-task array job
	$max_jobs = undef;   

	$max_retry = $pUserParams->get_param( USER_PARAM_cluster_max_retries, $pLog );

	my $cluster_submit_misc_options = " -n $id -o $jobSetDir -e $jobSetDir";

	my $cluster_project = $pUserParams->get_param( USER_PARAM_cluster_project, $pLog );
	my $cluster_queue = $pUserParams->get_param( USER_PARAM_cluster_queue, $pLog );
	my $cluster_slots_per_task = $pUserParams->get_param( USER_PARAM_cluster_slots_per_task, $pLog );
	my $cluster_nodes = $pUserParams->get_param( USER_PARAM_cluster_num_nodes, $pLog );
	my $cluster_ram_request = $pUserParams->get_param( USER_PARAM_cluster_ram_request, $pLog );
	my $cluster_ram_thisStage = $pUserParams->get_param( "cluster_ram_".${g_stage}, $pLog ); 
	my $cluster_walltime = $pUserParams->get_param( USER_PARAM_cluster_walltime, $pLog );
	my $cluster_walltime_thisStage = $pUserParams->get_param( "cluster_walltime_".${g_stage}, $pLog );

	if ( $cluster_project )
	{
	    $cluster_submit_misc_options .= " -p $cluster_project";
	}
	if ( $cluster_ram_thisStage )
	{
	    $cluster_ram_request = $cluster_ram_thisStage;
	}
	if ( $cluster_walltime_thisStage )
	{
	    $cluster_walltime = $cluster_walltime_thisStage;
	}

	if ( $ram_request_override ) 
	{
	    $cluster_ram_request = $ram_request_override;
	}

	if ( $cluster_walltime )
	{
	    $cluster_submit_misc_options .= " -w $cluster_walltime";
	}

	if ( $mthreaded && $cluster_slots_per_task > 1) 
	{
	    $cluster_submit_misc_options .= " -s $cluster_slots_per_task";


	    if  ( $cluster_ram_request)
	    {
		if  ( $cluster_ram_request >= 100)  {  # request exclusive node reservation for high-memory, multi-threaded jobs
		    $cluster_submit_misc_options .= " -R";
		}
		elsif ($cluster_ram_request < 48) {   # explicitly share resources with other jobs/users (allows faster scheduling) 
		    $cluster_submit_misc_options .= " -S";
		}

		#Adjust ram request so that it's later applied per core
		$cluster_ram_request = ceil($cluster_ram_request / $cluster_slots_per_task);
	    }
	}

	if ( $cluster_ram_request )
	{
	    $cluster_submit_misc_options .= " -r ${cluster_ram_request}G";
	}


	$job_parameters{ (JOB_SET_SUBMIT_MISC_OPTIONS) } = $cluster_submit_misc_options;
	$job_parameters{ (JOB_SET_CLUSTER_QUEUE) } = $cluster_queue;
    }



    $job_parameters{ (JOB_SET_RUN_TYPE) } = $run_type;
    $job_parameters{ (JOB_SET_MAX_SUBMIT) } = $max_jobs;
    $job_parameters{ (JOB_SET_MAX_RETRIES) } = $max_retry;
    $job_parameters{ (JOB_SET_COMMANDS) } = $cmd_set_ref;
    $job_parameters{ (JOB_SET_STDOUTS) } = $stdout_set_ref;
    $job_parameters{ (JOB_SET_STDERRS) } = $stderr_set_ref;
    $job_parameters{ (JOB_SET_RUN_DIR) } = $jobSetDir;
    $job_parameters{ (JOB_SET_EXECUTION_DIRS) } = $working_dir_set_ref;
    $job_parameters{ (JOB_SET_LINKED_SCRIPT_TEMPLATE) } = $working_dir_set_ref->[0]."/linkedScript.template";
    $job_parameters{ (JOB_SET_VALIDATION_FUNCTOR) } = $validation_function_ref;


    my %memtimeLogFilesH = ();

    my @newCmdSet = ();
    my $memtime = $release_root.PATH_MEMTIME;

#    #cap memory allocation on local processes. Cluster job sets don't need this since the scheduler already does this based on $cluster_ram_request
#    if ($local_max_memory && not $use_cluster )  
#    {
#	$memtime = $memtime . " -m " . ($local_max_memory * 1024 * 1024);  
#    }


    # create memtime log files
    my $memtimeFile;
    for( my $i = 0; $i < @{ $cmd_set_ref }; $i++ )
    {
	if ( $stdout_set_ref->[ $i ])
	{ 
	    $memtimeFile = "$jobSetDir/".(basename($stdout_set_ref->[ $i ])).".memtime";
	}
	else # stdout file wasn't defined, use generic prefix
	{
	    $memtimeFile = "$jobSetDir/$i.memtime";
	}
	
	my $newCmd = "$memtime -f $memtimeFile ".$cmd_set_ref->[ $i ];
	push @newCmdSet, $newCmd;
	$memtimeLogFilesH{$memtimeFile} = $newCmd;   #map memtimes files to commands for later logging
    }
    $job_parameters{ (JOB_SET_COMMANDS) } = \@newCmdSet;


    #
    # Run the commands.
    #
    if (M_Job_Set::run_job_set(\%job_parameters) != JGI_SUCCESS)
    {
	die $pLog->error("ERROR: Job set did not complete successfully!  Please review all .err files associated with this job set.");
    }


    #
    # Log the commands and the corresponding memtime output
    #
    foreach my $memtimeFile  (keys %memtimeLogFilesH ) 
    {
	open( M, "<$memtimeFile") ||  die $pLog->error("Can't open $memtimeFile : $!");
	while (<M>) { chomp; $pLog->debug( "memtime: ( ".$memtimeLogFilesH{$memtimeFile}." ) ".$_ ); }
	close M;
    }
    
    @$cmd_set_ref = (); @$stdout_set_ref = (); @$stderr_set_ref = (); @$working_dir_set_ref = ();
    
    return( JGI_SUCCESS );
}



# Function: run_single_cmd
# -----------------------
# Wraps a system command and sends it for execution to the generic run_local_cmd()
# This function takes the following arguments:
#
# $_[0]     A string containing the to-be-run command
# $_[1]     The logging object for the command
# $_[2]     A flag to turn off the capturing of memtime output 
#
#
# This function returns the following values:
#
# JGI_SUCCESS     The function successfully finished.
#
# Note:  This function throws a die exception instead of returning a status value since there are so many
#        calls to it, checking each one for errors would be tedious.
#        However the line number and name of the caller is logged
#
sub run_single_cmd
{
  my ( $cmd, $stdout, $stderr, $memtimeFlag, $pLog ) = @_;
  
  my $date = `date +%Y%m%d-%H%M%S`;   
  chomp $date;

  if (defined $stdout) {$cmd .= " > $stdout"}
  if (defined $stderr) {$cmd .= " 2> $stderr"} else {$cmd .= " 2> sys_cmd.${date}.err"}

  my $memtimeLog;
  if (defined $memtimeFlag) {
      my $pathMemtime = $release_root.PATH_MEMTIME;
      $memtimeLog = (defined $stdout ? "$stdout.memtime" : "sys_cmd.${date}.memtime");
      $cmd = "$pathMemtime -f $memtimeLog " . "$cmd";
  }
  my $ret_val;
  if (run_local_cmd($cmd, \$ret_val, $pLog) != JGI_SUCCESS)
  {
      my $line_num = ( caller(0) )[2];
      die $pLog->error( "ERROR: System command at line $line_num failed:\t (Called from ", M_Generic_Config::called_from(), ")" );    # we throw a die exception instead of returning a status value;  see function header

  }
  # dump memtime output to the log file
  if (defined $memtimeFlag) {
      open( M, "<$memtimeLog") ||  die $pLog->error("Can't open $memtimeLog : $!");
      while(<M>) { chomp; $pLog->debug( "memtime: ($cmd) $_" ); }
      close M;
  }

  return JGI_SUCCESS;
}


sub run_single_cmd_OK_to_fail
{
  my ( $cmd, $stdout, $stderr, $pLog ) = @_;
  
  my $date = `date +%Y%m%d-%H%M%S`;   
  chomp $date;

  if (defined $stdout) {$cmd .= " > $stdout"}
  if (defined $stderr) {$cmd .= " 2> $stderr"} else {$cmd .= " 2> sys_cmd.${date}.err"}

  my $ret_val;
  if (run_local_cmd($cmd, \$ret_val, $pLog) != JGI_SUCCESS)
  {
      my $line_num = ( caller(0) )[2];
      $pLog->warn( "WARNING: System command at line $line_num failed:\t (Called from ", M_Generic_Config::called_from(), ")" );  

  }
  return JGI_SUCCESS;
}




#
# Function: validate_stdout
# ---------------------------------------
# Checks whether the file (presumably stddout of a process) exists and is not empty 
#
# This function takes the following arguments:
# $_[0] - string containing a path to a file
# $_[1] - reference to the logging object
#
# Returns:
#
# JGI_SUCCESS  - files exist and are not empty
#
sub validate_stdout 
{
    my ($stdout, $pLog) = @_;
    if ( -s $stdout )   # a single non-empty stdout file exists
    {  
	return JGI_SUCCESS;
    }
    # elsif ( my @files=glob("${stdout}.*[0-9]") )  
    # # If the parent process was threaded we will have ouputs split into .1, .2, .3, etc, 
    # # and instead of being the output file, the specified $stdout string serves as the prefix to the full names
    # # WARNING! This is a fragile assumption that's dependent on how a particular process chooses to name its multiple outputs. Try to avoid this scenario!
    # {
    #  	my $valid = 0;
    #  	foreach (@files)
    #  	{
    #  	    if (-s $_) { $valid++; }
    #  	}
    #  	if ($valid == $#files + 1)
    #  	{
    # 	    $pLog->debug( "Passed validation: (@files)");
    #  	    return JGI_SUCCESS;
    #  	}
    #  	else
    #  	{
    # 	    $pLog->debug( "Validation Failed: (@files)");
    #  	    return JGI_FAILURE;
    #  	}
    # }
    else
    {
	$pLog->debug( "Output validation failed: file doesn't exist or is empty: $stdout");
	return JGI_FAILURE;
    }

}





####################################################
#
# Housekeeping functions
#

#
# Function: cleanup_for_restart
# -----------------------
# Removes or archives stage directories 
# This function takes the following arguments:
#
# $_[0]    Stage to begin from
# $_[1]    Path to the run directory
# $_[2]    Path to directory where checkpoints and local params files live
# $_[3]    A flag to enable archiving of the stage directories prior to cleanup
# $_[4]    A reference to the logging object 
#
# This function returns the following values:
#
# JGI_SUCCESS        The function successfully finished.
#
sub cleanup_for_restart
{
  my ($start_stage,$assembly_dir, $checkpoint_dir, $archive, $startup_log ) = @_;

  if (defined($archive)) 
  {
  # move old stage dirs to one directory (old)
      $startup_log->info("Archiving old stage directories for clean restart...");

      my $old_dir = "$assembly_dir/old";
      run_single_cmd( "mkdir $old_dir", undef, undef, undef, $startup_log) if ( not -d $old_dir );    # create old dir
      for ( my $stage = $start_stage ; defined($stage) ; $stage = stage_after($stage) )
      {
	  my $stage_dir = "$assembly_dir/$stage";
	  if ( -d "$stage_dir" )
	  { 
	      system( "mv $stage_dir $old_dir" ); 
	  }
      }
  }
  else
  {
  # delete old stage dirs
      $startup_log->info("Deleting old stage directories for clean restart...");

      for ( my $stage = $start_stage ; defined($stage) ; $stage = stage_after($stage) )
      {
	  my $stage_dir = "$assembly_dir/$stage";
	  if ( -d "$stage_dir" )
	  { 
	      system( "rm -r $stage_dir" ); 
	  }
      }
  }

  # remove old checkpoints and parameter files
  for ( my $stage = $start_stage ; defined($stage) ; $stage = stage_after($stage) )
  {
      unlink( "$checkpoint_dir/$stage" . QC_CHECKPOINT_SUFFIX ); 
      unlink("$checkpoint_dir/$stage.local.params");
  }    
  
  
  return JGI_SUCCESS;
}


#
# Function: is_stage_cleaned_up
# -----------------------
# Checks if stage directories have evidence that they've been agressively cleaned up in the past
#
# This function takes the following arguments:
#
# $_[0]    stage name
#
# This function returns the following values:
#
# JGI_SUCCESS     Stage directory has NOT been cleaned up in a prohibitive way
#
# JGI_FAILURE     Stage directory has been cleaned up agressively, so downstream stages can't run
#
sub is_stage_cleaned_up
{
    my ($stage, $assembly_dir, $pLog) = @_;
        if (-e "$assembly_dir/" . $stage . "/CLEANED-UP.2")
        {
            $pLog->error( "ERROR: Stage $stage had been cleaned up in a way that prohibits re-execution of downstream stages!" );
            return JGI_FAILURE;
        }
    return JGI_SUCCESS;
}



#
# Function: create_assembly_directory_name
# -----------------------
# Creates the run name based on the input label and the date-time stamp
#
# This function takes the following arguments:
#
# $_[0]     The label string that will be part of the directory name
#
# Returns the new assembly directory name
#
# 
#
sub create_assembly_directory_name
{
  my ($label) = @_;

  my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) = localtime();
  $year += 1900;    #the year starts from 1900
  my $timestamp = sprintf( '%4d-%02d-%02d_%02dh%02dm%02ds', $year, $mon + 1, $mday, $hour, $min, $sec );   #which gives YYYY-MM-DD_HHhMMmSSs
  my $pwd       = cwd();

  return $pwd . "/" . $label . "_" . $timestamp;
}



# Function: compute_platform_specific_paths  -CURRENTLY MOSTLY A DUMMY
# ---------------------------
# Computes the platform-specific path for the Meraculous root and the Perl binary
# so that different stages can be run/restarted/resumed in different environments seamlessly
#
# This function takes the following arguments:
#
#    pLocalParams - pointer to the local params object
#    pUserParams - pointer to the user params object
#    pLog - pointer to log object
#
# This function returns the following values:
#
# JGI_SUCCESS     The function successfully finished.
#
sub compute_platform_specific_paths
{
  my ( $pLocalParams, $pUserParams, $pLog ) = @_;
  #
  # Identify the platform and the running environment
  #
  my $os_name = `uname -m`;
  $os_name =~ s/^\s+//;
  $os_name =~ s/\s+$//;

  $pLog->info("os name: $os_name...\n");
  if ( $os_name eq "x86_64" )
  {
    $pLog->info("running on 64-bit Linux...\n");
  }
  elsif ( $os_name eq "i686" )
  {
    $pLog->info("running on 32-bit Linux...\n");
  }

  my $perl         = $^X;
#  $pLocalParams->put_param( LOCAL_PARAM_perl,      $perl, $pLog );

  return JGI_SUCCESS;
}


#
# Function: create_label
# -----------------------
# Creates a label out of the genus species and strain user parameters if given
#
# This function takes the following arguments:
#
# $_[0]     A reference to the user parameters object
# $_[1]     A reference to the logging object
#
# This function returns the following values:
#
# $label    The label string
#
sub create_label
{
  my ( $pUserParams, $log ) = @_;
  my $label = 'run';    # default label

  #check if the user specified a genus+species , strain
  my ( $genus, $species, $strain );
  $genus   = $pUserParams->get_param( USER_PARAM_genus,   $log );
  $species = $pUserParams->get_param( USER_PARAM_species, $log );
  $strain  = $pUserParams->get_param( USER_PARAM_strain,  $log );
  if ( defined($genus) && defined($species) )
  {
    $label = $genus . '_' . $species;
    $label .= '_'.$strain if ( defined($strain) );
  }

  return $label;
}


sub get_dna_prefix_list_filename
{
  my ( $assembly_dir ) = @_;
  
  return( "$assembly_dir/meraculous_import/prefixBlocks.list" );
}


sub get_dna_prefixes
{
  my ( $prefixListFile, $pLog ) = @_;

    
  my @prefixes;
  if ( !open( F, $prefixListFile ) )
  {
    die $pLog->error( "couldn't open prefixlistfile $prefixListFile\n" );
  }

  while( <F> )
  {
    chomp;
    my @prefixRange=split();
    push @prefixes,  \@prefixRange;
  }
  return @prefixes;
}  



   
#
# Function: generate_fastq_info_files
# ----------------------
# A function for generating fastq info tables
#
# $_[0]     - run directory path
# $_[1]     - output directory path
# $_[2]     - library name
# $_[3]     - number of entries per chunk (typically >= number of threads)
# $_[4]     - max number of chunks to divide the data into
# $_[5]     - quality value encoding offset to use for all fastqs
# $_[6]     - logging object reference
# 
#
# This function returns the following values:
#
# If $_[3] > 0 returns an array contating paths to info files
# Otherwise returns a path to the single generated info file
#
sub generate_fastq_info_files
{
    my ($assembly_dir, $work_dir, $libName, $nThreads, $maxChunks, $qOffset, $pLog) = @_;

    my @fastqs_for_lib = glob "$assembly_dir/meraculous_import/${libName}.fastq.*[0-9]";
    if ( ! @fastqs_for_lib )  
    {
	die $pLog->error( "ERROR: No fastq files found for library $libName ." );
    }
    
    if ($maxChunks && $nThreads)
    {
	my $nFasqsPerIF = ceil( (scalar @fastqs_for_lib) / $maxChunks); 
	# make sure we pack them tight
	if ($nFasqsPerIF < $nThreads ) 
	{
	    $nFasqsPerIF = $nThreads;
	}
	my @fastqIFs;
	my $f = 0;
	while (@fastqs_for_lib)
	{
	    my $fastqInfoFile = "$work_dir/$libName.fastq.info.$f";
	    open( SEQINFO, ">$fastqInfoFile" ) || die $pLog->error("could not open file $fastqInfoFile for input");       
	    
	    my @chunk = splice(@fastqs_for_lib,0,$nFasqsPerIF);
	    foreach my $fq( @chunk )
	    { 
		print SEQINFO $fq."\tF\tU\t$qOffset\n"; 
	    }
	    close SEQINFO;
	    push @fastqIFs, $fastqInfoFile;
	    $f++;
	}
	return @fastqIFs;
    }
    else 
    {
	my $fastqInfoFile = "$work_dir/$libName.fastq.info.0";
	open( SEQINFO, ">$fastqInfoFile" ) || die $pLog->error("could not open file $fastqInfoFile for input");
	foreach my $fq( @fastqs_for_lib )
	{
	    print SEQINFO $fq."\tF\tU\t$qOffset\n";
	}
	close SEQINFO;
	return $fastqInfoFile;
    }
}



#
# Function: script_usage
# ----------------------
# A function for printing the script usage, invoked when
# the "-h" flag is found on the command line.
# This function takes the following arguments:
#
# $_[0]     [Optional] A variable to indicate the status of the script.
#           If this is set to JGI_FAILURE, the function will return
#           after printing its output, to avoid interfering with any
#           error logging being done by the calling script.
#
# Otherwise, this function exits from the script upon completion,
# returning JGI_SUCCESS.
#
sub script_usage
{

  # Please refer to the above function description for details of
  # what these variables are.
  my ($script_status) = @_;

  print "\nCommand line arguments for $SCRIPT_ID ($VERSION_ID):\n";
  print "\n";
  print <<END_HERE ;
meraculous.pl 

  Required:
  -c|config <config file>         : user config file 

  Optional:
  -label   <label>                : provide a label name for new runs ( Default: 'run' )
  -dir     <directory>            : provide a run directory name  ( Default: latest run )
  -restart                        : restart a previously failed assembly
  -resume                         : restart but preserve any partial results    
  -step                           : execute one stage and stop
  -start   <stage>                : re-run starting with this stage
  -stop    <stage>                : stop after this stage
  -archive                        : save any old stage directories (valid only with -restart)
  -cleanup_level [0|1|2]          : decide how agressively the pipeline should clean up intermediate data ( Default: 1)
                                      0 - do not delete any intermediate outputs (disk space footprint may be huge)
                                      1 - delete files that are not used in any of the subsequent stages and that are generally not informative to the user
                                      2 - delete files as soon as possible.  WARNING!!! You will not be able to rerun the
                                          stages individually once they have completed!

  -h|help                         : you guessed it: this usage page
  -v|version                      : about this program


The default configuration file is 'meraculous.params', which must be present

The default label name is  <genus>_<species>_[strain] if these are defined in 
the configuration file, and 'run' otherwise;

-resume/-restart : If no directory is given, the most recently run dir. is used.

Invalid command line combinations:
  -restart with     -resume
  -label   with    -restart or -resume
  -start   without -restart or -resume
  -archive without -restart


Please contact Eugene Goltsman at egoltsman\@lbl.gov if you encounter any problems.
  
END_HERE
  #
  # If the script status isn't defined, or is set to something other
  # than failure, exit the script with success.
  #
  if ( ( !defined($script_status) ) || ( $script_status ne JGI_FAILURE ) )
  {
    exit(JGI_SUCCESS);
  }
}



#
# Function: script_version
# ------------------------
# A function for printing the script version, invoked when
# the "-v" flag is found on the command line.
# This function takes the following arguments:
#
# $_[0]     [Optional] A variable to indicate the status of the script.
#           If this is set to JGI_FAILURE, the function will return
#           after printing its output, to avoid interfering with any
#           error logging being done by the calling script.
#
# Otherwise, this function exits from the script upon completion,
# returning a value of JGI_SUCCESS.
#
sub script_version
{

  # Please refer to the above function description for details of
  # what these variables are.
  my ($script_status) = @_;

  print "$SCRIPT_ID ($VERSION_ID)\n";
  print "Author: $AUTHOR_ID\n";

  #
  # If the script status isn't defined, or is set to something other
  # than failure, exit the script with success.
  #
  if ( ( !defined($script_status) ) || ( $script_status ne JGI_FAILURE ) )
  {
    exit(JGI_SUCCESS);
  }
}




#
# Function: create_source_links
# ---------------------------------------
# Creates symbolic links to the original input sequence files.  The links are named in a generic manner
# and are stored in the array that's provided as a reference.
# This function takes the following arguments:
#
# $_[0]    library name
# $_[1]    a reference to the array of filenames or wildcards
# $_[2]    a reference to an empty array to store link names
# $_[3]    path to the output directory
# $_[4]    a reference to the logging object
# 
sub create_source_links
{
    my ($libName, $wildcardsREF, $linksREF, $link_dir, $pLog) = @_;
    my @w = @$wildcardsREF;
    my %wildcardSet;
    my $x = 0;
    run_single_cmd( "mkdir -p $link_dir", undef, undef, undef, $pLog);

    # there were two sets of wildcards/files given, meaning reads 1 and 2 are in separate files
    if ( scalar(@w) == 2  )
    {
	$pLog->debug("Two files or wildcards given as input. Will process as sets of Fwd and Rev reads.");

	$wildcardSet{'F'} = [ glob( "$w[0]" ) ];
	$wildcardSet{'R'} = [ glob( "$w[1]" ) ];

	# Sort to make sure the files in both sets are in the same order. 
	# Since any naming convention is accepted, the fragile assumption is that the two n'th files in each sorted set will contain matching pairs.
	# If this is not the case the reads will not pass validation downstream
	@{$wildcardSet{'F'}} = sort {lc($a) cmp lc($b)} @{$wildcardSet{'F'}};
	@{$wildcardSet{'R'}} = sort {lc($a) cmp lc($b)} @{$wildcardSet{'R'}};

	die $pLog->error("ERROR: Different number of files in $w[0] and $w[1]") unless (@{$wildcardSet{'F'}} == @{$wildcardSet{'R'}});

	foreach my $seqFile( @{$wildcardSet{'F'}} )
	{
	    # read 1's
	    my $linkNameF = "$link_dir/$libName.fastq.$x.F";
	    if ($seqFile =~ /\.gz$/) { 
		$linkNameF .= ".gz"; 
	    }
	    if ($seqFile =~ /\.bz2$/) { 
		$linkNameF .= ".bz2"; 
	    }
            run_single_cmd( "ln -sf $seqFile $linkNameF", undef, undef, undef, $pLog);


	    # read 2's
	    $seqFile = shift @{$wildcardSet{'R'}};
	    my $linkNameR = "$link_dir/$libName.fastq.$x.R";
	    if ($seqFile =~ /\.gz$/) { 
		$linkNameR .= ".gz"; 
	    }
	    if ($seqFile =~ /\.bz2$/) { 
		$linkNameR .= ".bz2"; 
	    }
	    run_single_cmd( "ln -sf $seqFile $linkNameR", undef, undef, undef, $pLog);

	    push( @$linksREF, "$linkNameF $linkNameR"); 
	    $x++;
	}
    }
    elsif ( scalar(@w) == 1  ) # one file or wildcard was given.  Process reads as interleaved pairs
    {
	$wildcardSet{'I'} = [ glob( "$w[0]" ) ];
	foreach my $seqFile( @{$wildcardSet{'I'}} )
	{
	    my $linkNameI = "$link_dir/$libName.fastq.$x.I";
	    if ($seqFile =~ /\.gz$/) { 
		$linkNameI .= ".gz";
	    }
	    if ($seqFile =~ /\.bz2$/) { 
		$linkNameI .= ".bz2";
	    }
	    run_single_cmd( "ln -sf $seqFile $linkNameI", undef, undef, undef, $pLog);
	    push( @$linksREF, $linkNameI); 
	    $x++;
	}
    }
    else { 
	die $pLog->error("Error parsing wildcard - too many entries: @w \n");
    }

    return JGI_SUCCESS;
}



#
# Function: merge_contig_mer_depths
# ---------------------------------------
# Aggregates contig mer depth info across multiple files 
# This function takes the following arguments:
#
# $_[0]     a reference to an array of filenames
# $_[1]     a string specifying the output file name
#
sub merge_contig_mer_depths
{
    my $fileListRef = shift;
    my $fileOut = shift;
    my $log = shift;
    my %t;
    my %n;
    my @col;

    for my $file (@$fileListRef)
    {
	open(F, "<$file") || die "$!\n";
	while (<F>)
	{
	    @col = split /\s+/;
	    if (! @col) { 
		die $log->error( "Could not parse input file $file");
	    }
	    $t{$col[0]} += $col[1]*$col[2];
	    $n{$col[0]}+= $col[1];
	}
    }
#    if ((! keys %t) || (! keys %n )) {
#	die $log->error( "Could not get any data out of the inputs");
#    }

    
    open(OUT, ">$fileOut") || die "$!\n";
    
    # sort by contig name using Schwartzian Transform
    my @sorted = map  { $_->[0] }
    sort { $a->[1] <=> $b->[1] }
    map  { /\S+(\d+)/ and [$_, $1] }
    keys %t;

    for my $c (@sorted)
    {
	my $m = $t{$c} / $n{$c};
	print OUT "$c\t$n{$c}\t$m\n";
    }
    close F;
    close OUT;

    return JGI_SUCCESS;
}


#
# Function: calculate_avg_lib_insert_size
# ---------------------------------------
# Calculates average insert size and stdev from multiple distributions
# This function takes the following arguments:
#
# $_[0]  
# $_[1]
# $_[2]
# $_[3]
# $_[4]
# $_[5]
# $_[6]
#
sub calculate_avg_lib_insert_size
{
    my ($distFilesGlob, $libHasInnieArtifact, $insertAvgRef, $insertSdevRef) = @_;

    my @insertDist;
    foreach my $distFile (glob("$distFilesGlob"))
    {
	push @insertDist, (split( /\n/, `cat $distFile` ));
    }
    my $total = 0;
    my $totalCnt = 0;
    foreach my $x ( @insertDist )
    {
	unless ( $x =~ /\#/ ) 
	{
	    my ( $size, $cnt ) = split( /\t/, $x );
	    
	    # illumina long insert libs currently show a tail of
	    #   mispaired reads below 1Kb in span; we ignore these
	    #   in the insert dist calculation, and in the later oNo
	    unless ( $libHasInnieArtifact && $size < 1000 )
	    {
		$total += $size * $cnt;
		$totalCnt += $cnt;
	    }
	}
    }
    if ( ! $totalCnt ) { 
	return 0;
    }
    else {
	$$insertAvgRef = $total / $totalCnt;	    
	$total = 0;  # reuse the counter
	foreach my $x ( @insertDist )
	{
	    unless ( $x =~ /\#/ )
	    {
		my ( $size, $cnt ) = split( /\t/, $x );
		unless( $libHasInnieArtifact && $size < 1000 )
		{
		    $total += ( $size ** 2 - $$insertAvgRef ** 2 ) * $cnt;
		}
	    }    
	}
	$$insertSdevRef = int( ( sqrt( $total / $totalCnt ) ) + 0.5 );
	$$insertAvgRef = int ( $$insertAvgRef + 0.5 );
    }
}



sub apply_filters
{
    my ($diploid_mode, $maxInsSize, $remoteContigsFa, $remoteContigsInfo, $filteredContigsFile, $bubble_depth_threshold, $crf, $pLog) = @_;

    my $screenList2 = $release_root.PATH_SCREEN_LIST2;

    # create depth-filtered contig set;   
    #  diploid_mode 1 means we filter out isotigs BELOW the bubble_depth_threshold
    #  diploid_mode 2 means we include all contigs but label isotigs that are ABOVE the threshold as 'isotigDD' while the rest are labeled 'isotigSD'.  Diplotigs with a low depth relative to it's alt var sister are filtered out.
    #

    my $cmd = "";

    if ($diploid_mode == 1) {
	# add double-depth isotigs
	$cmd = "grep \"^isotig\" $remoteContigsInfo | awk ' !(\$5 <= $bubble_depth_threshold && \$3 <= $maxInsSize) { print \$1 }' | $screenList2 -l - -f $remoteContigsFa -k -c 1 > $filteredContigsFile";
	run_single_cmd( $cmd, undef, undef, undef, $pLog );

	# add all diplotigs (expect to be double-depth)
	$cmd = "grep \"^diplotig\" $remoteContigsInfo | awk '{ print \$1 }' | $screenList2 -l - -f $remoteContigsFa -k -c 1 >> $filteredContigsFile";
	run_single_cmd( $cmd, undef, undef, undef, $pLog );

	# create depth-filtered crf
	$cmd = "grep \"^isotig\" $remoteContigsInfo | perl -ane 'unless ( \$F[4] <= $bubble_depth_threshold && \$F[2] <= $maxInsSize) { print \$F[0].\"\\t\".\$F[1].\"\\t\".\$F[4].\"\\n\" }' > $crf";
	run_single_cmd( $cmd, undef, undef, undef, $pLog );
	$cmd = "grep \"^diplotig\" $remoteContigsInfo | perl -ane 'print \$F[0].\"\\t\".\$F[1].\"\\t\".\$F[4].\"\\n\" ' >> $crf";
	run_single_cmd( $cmd, undef, undef, undef, $pLog );

    }
    elsif ($diploid_mode == 2) {
	# additionally, filter out diploits if their b-tig-based depth is low relative to it's sister-diplotig's depth, meaning the bubble(s) was likely induced by a sequencing error. 
	my $varDepthRatioCutoff = 0.25;
	my %contigSeq = ();
	my %diplotigInfo = ();
	my %isotigInfo = ();

	open (F, "<", $remoteContigsFa) or die $pLog->error("Can't open the file  $remoteContigsFa ! ");
	my ($currentEntry, $currentSeq);
	while (my $line = <F>) {
	    chomp $line;
	    if ($line =~ /^>/) {
		if (defined($currentEntry)) {
		    $contigSeq{$currentEntry} = $currentSeq;
		}
		$currentSeq = "";
		($currentEntry) = $line =~ /^>(.+)$/;
	    } else {
		$currentSeq .= $line;
	    }
	}
	if (defined($currentEntry)) {
	    $contigSeq{$currentEntry} = $currentSeq;
	}
	close F;

	open  (FILTERED, ">", $filteredContigsFile) or  die  $pLog->error("Can't open the file  $filteredContigsFile ");
	open  (CRF, ">", $crf) or  die  $pLog->error("Can't open the file $crf ");
	$Text::Wrap::columns = 50;

	open (IN, "<", $remoteContigsInfo) or die $pLog->error("Can't open the file $remoteContigsInfo ! ");
	my (@dInfoP1,  @dInfoP2); 
	my ($P1_is_hair, $P2_is_hair)
;
	while (my $line =<IN>)	{
	    chomp ($line);
	    my($name,$length,$nLinkertigs,$nBubbles,$merDepth,$origIDs) = split("\t", $line);
	    if ($name =~ /^diplotig\d+_p1/) {
		@dInfoP1 = ($name,$length,$nLinkertigs,$nBubbles,$merDepth,$origIDs) ;
		my $P1_is_hair;
		next;
	    }
	    elsif ($name =~ /^diplotig\d+_p2/) {
		@dInfoP2 = ($name,$length,$nLinkertigs,$nBubbles,$merDepth,$origIDs) ;
		# print both records if legitimate haplo-paths

		my $diP1 = $dInfoP1[0];
		my $diP2 = $dInfoP2[0];

		### don't print out "hair" variants, i.e. it's likely seq error;  Remove the lc masking from the *other* sequence
		if ( $dInfoP1[3] == 1 && 
		     ($dInfoP1[4]/$dInfoP2[4] < $varDepthRatioCutoff) 
		    ) {
		    $P1_is_hair = 1;
		    $contigSeq{$diP2} =~ tr/acgt/ACGT/;  
		}
		elsif ( $dInfoP2[3] == 1 && 
			($dInfoP2[4]/$dInfoP1[4] < $varDepthRatioCutoff) 
		    ) {
		    $P2_is_hair = 1;
		    $contigSeq{$diP1} =~ tr/acgt/ACGT/;  
		}

		unless($P1_is_hair) {
		    my $diP1 = $dInfoP1[0];
		    print FILTERED ">$diP1\n".wrap('','',$contigSeq{$diP1})."\n";
		    print CRF "$diP1\t".$dInfoP1[1]."\t".$dInfoP1[4]."\n";
		}
		unless($P2_is_hair) {
		    my $diP2 = $dInfoP2[0];
		    print FILTERED ">$diP2\n".wrap('','',$contigSeq{$diP2})."\n";
		    print CRF "$diP2\t".$dInfoP2[1]."\t".$dInfoP2[4]."\n";
		}

		undef $P1_is_hair; undef $P2_is_hair;
		@dInfoP1 = (); @dInfoP2 = ();
	    }
	    else {  # an isotig
		my $newName;
		if ($merDepth  < $bubble_depth_threshold ) {
		    ($newName = $name) =~ s/isotig/isotigSD/;
		}
		else {
		    ($newName = $name) =~ s/isotig/isotigDD/;
		}
		my $seq = $contigSeq{$name};
		print FILTERED ">$newName\n".wrap('','',$seq)."\n";
		print CRF "$newName\t$length\t$merDepth\n";
	    }		
	}
	close IN;
	close FILTERED;
	close CRF;
    }
    else {
	$pLog->error("Invalid diploid_mode value ($diploid_mode)");
	return JGI_FAILURE;
    }
    return JGI_SUCCESS;
}




#////////////////////////////////////////////////////

#######################################################
#
# Inactive functions dump
#

# #
# # Function: check_files_exist:
# # ----------------------------
# # Checks that all files in a given list exist in the given directroy
# #
# # $_[0]     [Required] Directory name
# # $_[1]     [Required] A reference to a list of filenames
# # $_[3]     [Required] A log object
# #
# # returns  JGI_SUCCESS if completed successfully
# # otherwise returns JGI_FAILURE
# #
# sub check_files_exist
# {
#   my ( $directory, $files, $pLog ) = @_;
#   my $index = 0;

#   while ( $index < $#$files + 1 )
#   {
#     unless ( -e "$directory/$$files[$index]" )
#     {
#       $pLog->error( " $$files[$index] not found in $directory", called_from() );    #maybe this is not an error?
#       return JGI_FAILURE;                                                               # or continue on and check all files?
#     }
#     $index++;
#   }

#   return JGI_SUCCESS;
# }


# sub codeToMer
# {
#   my ( $code, $merLen ) = @_;

#   if ( $code >= 4**$merLen ) { return( "" ); } 

#   my $buffer = itoa( $code, 4, $merLen );  # convert to base 4 numerical string
   
#   $buffer =~ tr/[0123]/[ACGT]/;
#   return( $buffer );
# }


# # converts a base 10 number to a given base ( less than 10 )
# sub to_base
# {
#   my $base   = shift;
#   my $number = shift;
#   die if ( $base > 10 );
#   my @nums = (0..9);
#   return $nums[0] if $number == 0;
#   my $rep = ""; # this will be the end value.
#   while( $number > 0 )
#   {
#     $rep = $nums[$number % $base] . $rep;
#     $number = int( $number / $base );
#   }
#   return $rep;
# }



# #
# # Function: create_subdirectory_structure
# # ---------------------------------------
# # Creates a partitioned subdirectory structure, integer-indexed with 
# # a start index of 0, and returns a hashtable mapping with these
# # subdirectories as well as writing the hashtable to a file called
# # "directory_structure".
# #
# # This function takes the following arguments:
# #
# # $_[0]     A string specifying the top-level directory under which the 
# #      partitioned subdirectories will be created.
# #
# # $_[1]     The number of files to create
# #
# # $_[2]     [Optional] The number of files to create per subdirectory.  If
# #      undefined, the default value of 100 will be used.
# #
# # $_[3]     A reference to a logging object
# #
# # $_[4]     A reference to a hashtable will be returned, where the keys are the
# #           ID# (file index) and the values are the subdirectories with full
# #      path information prepended.
# #
# # This function returns the following values:
# #
# # JGI_SUCCESS     The directories were successfully created.
# #
# # JGI_FAILURE     The directories could not be created.
# #
# sub create_subdirectory_structure 
# {
#   my ($work_dir, $total_number_files, $number_files_per_subdir, $log, $directories_ref) = @_;

#   my $function_id = "create_subdirectory_structure";
#   my $directory_structure = DIR_STRUCTURE_FILENAME;

#   if (! -d $work_dir) 
#   {
#     $log->error( "Directory does not exist! ($work_dir)" );
#     return JGI_FAILURE; 
#   }

#   # default to 100 if files per subdirectory is not specified
#   if (! defined($number_files_per_subdir))
#   {
#     $number_files_per_subdir = 100;
#   }
#   my $number_of_dir = ceil($total_number_files / $number_files_per_subdir);

#   if (create_dir_structure($work_dir, $number_of_dir, $log) == JGI_FAILURE) { return JGI_FAILURE; }

#   my $file_id = 0;
#   my ($i,$j);
#   for ($i=0; $i<$number_of_dir; $i++)
#   {
#     for ($j=0; $j<$number_files_per_subdir; $j++) 
#     {
#       $directories_ref->[$file_id] = "$work_dir/$i";
#       $file_id ++;
#       if ($file_id >= $total_number_files) { 
#         last;
#       }
#     }
#   }

#   # write the subdirectory structure to a file called directory_structure
#   open (DESTINATIONFILE, ">$work_dir/$directory_structure");
#   print DESTINATIONFILE Dumper $directories_ref;
#   close(DESTINATIONFILE);

#   return JGI_SUCCESS;
# }

# #
# # Function: lookup_subdirectory_structure
# # ---------------------------------------
# # Accesses the "directory_structure" file created by create_subdirectory_structure
# # and returns a hashtable reference with the information.
# #
# # This function takes the following arguments:
# #
# # $_[0]     A string specifying the top-level directory under which the 
# #      partitioned subdirectories lie.
# #
# # Returns:
# #
# # A reference to a hashtable, where the keys are the ID# (file index) and the values 
# # are the subdirectories with full path information prepended.
# #
# sub lookup_subdirectory_structure {
#   my ($work_dir, $log) = @_;
#   my $directory_structure = DIR_STRUCTURE_FILENAME;

#   if (! -e "$work_dir/$directory_structure") {
#     $log->error("$work_dir/$directory_structure not found!");
#     die;
#   }

#   return do "$work_dir/$directory_structure";
# }





