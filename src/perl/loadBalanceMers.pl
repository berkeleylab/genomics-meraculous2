#! /usr/bin/env perl

use strict;
# Args:
# - mercount ( sorted in asc order )
# - number of chunks

# OUTPUT - list of prefix start/prefix end pairs

sub encodeMer
{
    my ( $mer ) = @_;
    my $code = 0;
    my $slotVal = 1;
    for( my $i = length( $mer ) - 1; $i >=0; $i-- )
    {
	my $val = -1;
	my $x = ( substr( $mer, $i, 1 ) );
	
	if( $x eq "A" ){ $val = 0; }
	elsif( $x eq "C" ){ $val = 1; }
	elsif( $x eq "G" ){ $val = 2; }
	elsif( $x eq "T" ){ $val = 3; }
	else{ return( -1 ); }
      
    
	$code += ( $val * $slotVal );
	$slotVal *= 4;
    }
    return( $code );
}


sub decodeMer
{
    my ( $x, $merLen ) = @_;
    
    my $remainder = $x;
    my $mer = "";
    for( my $slot = $merLen - 1; $slot >= 0; $slot-- )
    {
	my $valInSlot =  int( $remainder / 4**$slot );
        my $base = "";

	if( $valInSlot == 0 ) { $base = "A"; }
	elsif( $valInSlot == 1 ) { $base = "C"; }
	elsif( $valInSlot == 2 ) { $base = "G"; }
	elsif( $valInSlot == 3 ) { $base = "T"; }
	else{ die "illegal value $valInSlot for $x merLen $merLen\n"; }
	
        $mer .= $base;

        $remainder -= $valInSlot * 4**$slot;
    }
    return( $mer );
}

# be careful not to send it the mer that is fully maxed out...like TTTT
sub getNextMer
{
    my( $mer ) = @_;

    return( decodeMer( encodeMer( $mer ) + 1, length( $mer ) ) );
}

sub squashPrefixes
{
    my $prefixes = shift;
    my @newprefixes;
    my %p;
    my $squashable = -1;
    while ( $squashable != 0 ) {   # loop over until there are no prefixes on this line that can be squashed
	$squashable = 0;
	@newprefixes=();

	# tally up all the k-1 "heads" of the prefixes
	my $head;
	foreach my $prefix(@$prefixes) {
	    if (length($prefix) > 1) 
	    {
		$head = substr $prefix, 0, eval(length($prefix)-1);
	    }
	    else #dummy condition;  single-base prefix is not squashable by definition, but we still need to keep all the 'heads' accounted for
	    {
		$head = $prefix;  
	    }
	    $p{$head}++; 
	}
	#squash prefixes
	foreach $head (keys %p) {
	    if ( $p{$head} == '4' ) 
	    {  # there are 4 prefixes starting with this sequence, meaning this prefix can be squashed 
		$squashable = 1;
		push(@newprefixes, $head);  # squashed to a single k-1 bp prefix in place of 4 original ones
	    }
	    else 
	    {   # keep the 4 unchanged original prefixes
		my @a = grep {/^${head}[ACTG]?$/} @$prefixes;
		push(@newprefixes, @a);
	    }
	}
	%p = ();
	@$prefixes = sort @newprefixes; 
	print STDERR "prefixes after a round of squashing:  @newprefixes \n";

    }
}


if ( scalar( @ARGV ) != 2 )
{
    print "usage: loadBalanceMers.pl <mercount_file> <num_of_prefix_blocks>\n";
    exit;
}

open( F, $ARGV[0]) || die "couldn't open $ARGV[0]\n";

my $numChunks = $ARGV[1];

unless ($numChunks > 0)
{
    die "Error: number of prefix blocks must be greater than 0\n";
}

my $prefixLen = int( $numChunks ** ( 1/4 ) ) * 2;

# first scan: tally up total # of mers
my $totalMers = 0;
while( <F> )
{
  my( $mer, $cnt ) = split;
  $totalMers += $cnt;
}

sysseek( F, 0, 0 );

my $idealMersPerChunk = int( $totalMers / $numChunks );
my $runningSurplus = 0;  # how far from the ideal the total number of mers is
                         # amongst the divided set
my $totalInCurrentChunk = 0;
my $lastPrefix = "";
my $startPrefix = "A"x$prefixLen;
my $maxPrefix = "T"x$prefixLen;

my @prefixesInChunk;

while( <F> )
{
  # assume in sorted order
  # divide greedily

  my ( $mer, $cnt ) = split;

  my $prefix = substr( $mer, 0, $prefixLen );

  if ( !($prefix eq $lastPrefix) && !( $lastPrefix eq "" ) )
  {
      push(@prefixesInChunk, $lastPrefix);
     if ( $totalInCurrentChunk >= $idealMersPerChunk )
     {
       # split off the chunk here
	 print STDERR "prefixes in chunk: @prefixesInChunk \n";
	 squashPrefixes(\@prefixesInChunk);
	 print join(" ",@prefixesInChunk), "\n";

       # reset
       $totalInCurrentChunk = 0;
       @prefixesInChunk=();
       if ( !( $lastPrefix eq $maxPrefix ) ) { 
	   $startPrefix = getNextMer( $lastPrefix );
       }
     }

  }
   
  $totalInCurrentChunk += $cnt;
  $lastPrefix = $prefix;
}

push(@prefixesInChunk, $lastPrefix);

print STDERR "prefixes in chunk: @prefixesInChunk \n";
squashPrefixes(\@prefixesInChunk);
print join(" ",@prefixesInChunk);

