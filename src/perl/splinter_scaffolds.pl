#!/usr/bin/env perl
#
# splinter.pl by Jarrod Chapman <jchapman@lbl.gov> Fri Jun 12 12:58:11 PDT 2009
# Copyright 2009 Jarrod Chapman. All rights reserved.
#

use warnings;
use Getopt::Std;

my %opts = ();
my $validLine = getopts('b:S:F:T:X:', \%opts);
my @required = ("b");
my $nRequired = 0;
map {$nRequired += exists($opts{$_})} @required;
$validLine &= ($nRequired == @required);
if ($validLine != 1) {
    print STDERR "Usage: ./splinter.pl <-b blastMapGlob> <<-S scaffoldReportFile>> <<-F fivePrimeWiggleRoom>> <<-T threePrimeWiggleRoom>> <<-X excludeList >>\n";
    exit;
}

my $date = `date`;
chomp $date;
print STDERR "$date\nsplinter.pl";
while (my ($opt,$val) = each(%opts)) {
    print STDERR " -$opt=$val";
}
print STDERR "\n";

my $blastMapFileGlob = $opts{"b"};
my @blastMapFiles = glob($blastMapFileGlob);

my $srfFile = undef;
if (exists($opts{"S"})) {
    $srfFile = $opts{"S"};
}

my $fivePrimeWiggleRoom = 5;
if (exists($opts{"F"})) {
    $fivePrimeWiggleRoom = $opts{"F"};
}
my $threePrimeWiggleRoom = 5;
if (exists($opts{"T"})) {
    $threePrimeWiggleRoom = $opts{"T"};
}

my $excludeList;
my %excludeSubjects;
if (exists($opts{"X"})) {
    $excludeList = $opts{"X"};
}


my $currentRead = undef;
my $currentReadAlignments = 0;
my $readInfo = "";

my %rejected = ();
my @rejectReasons = ("FORMAT","MINLEN","UNINFORMATIVE","TRUNCATED","SINGLETON", "IN_EXCLUDE_LIST");
foreach my $r (@rejectReasons) {
    $rejected{$r} = 0;
}


if (defined($excludeList)) {
    print STDERR "Parsing exclusion list file: $excludeList ...\n";
    open (X,$excludeList) || die "Couldn't open $excludeList\n";
    while (<X>) {
	chomp;
	$excludeSubjects{$_} = 1;
    }
}



# If an existing scaffold report exists, load scaffolding information
 
my %contigScaffoldMap = ();  # contigName -> contigOrientation scaffoldName . scaffoldStart . scaffoldEnd 
my %scaffLengths = ();    # scaffoldName -> scaffoldLength
 
if (defined($srfFile)) {
    
    print STDERR "Reading scaffold report file: $srfFile...\n";
    open (S,$srfFile) || die "Couldn't open $srfFile\n";
    while (my $line = <S>) {
        chomp $line;
        my @cols = split(/\t/,$line);
        if ($cols[1] =~ /^CONTIG/) {
            my ($scaffID,$contigID,$sStart,$sEnd) = @cols[0,2,3,4];
            
            if (exists($scaffLengths{$scaffID})) {
                if ($sEnd > $scaffLengths{$scaffID}) {
                    $scaffLengths{$scaffID} = $sEnd;
                }
            } else {
                $scaffLengths{$scaffID} = $sEnd;
            }
            
            my ($cStrand,$cName) = $contigID =~ /^([\+\-])(.+)$/;
            
            if (exists($contigScaffoldMap{$cName})) {
                die "Error in $srfFile: Current implementation limits contigs to a single scaffold location: $cName\n";
            }
            
            $contigScaffoldMap{$cName} = "$cStrand$scaffID.$sStart.$sEnd";
            
        }
    }
    close S;
    
    print STDERR "Done.\n";
}



my $totalHitsFound = 0;
my $totalSplintsFound = 0;

foreach my $blastMapFile (@blastMapFiles) {

    print STDERR "Reading blast map file: $blastMapFile...\n";
    open (B,$blastMapFile) || die "Couldn't open $blastMapFile\n";
    
    while (my $line = <B>) {
	chomp $line;
	if ($line =~ /^BLAST_TYPE/) {
	    next;
	}
	
	my ($blastType,$query,$qStart,$qStop,$qLength,$subject,$sStart,$sStop,$sLength,
	    $strand,$score,$eValue,$identities,$alignLength) = split(/\t/,$line);
	
	$totalHitsFound++;

	if ( defined $excludeList && exists $excludeSubjects{$subject} ) {
	    $rejected{"IN_EXCLUDE_LIST"}++;
            next;
	}
	
	my $unalignedStart = $qStart-1;
	my $projectedStart = undef;
	if ($strand eq "Plus") {
	    $projectedStart = $sStart - $unalignedStart;
	} else {
	    $projectedStart = $sStop + $unalignedStart;
	}
	
	my $startStatus = undef;
	
	my $projectedOff = 0;
	if ($projectedStart < 1) {
	    $projectedOff = 1-$projectedStart;
	} elsif ($projectedStart > $sLength) {
	    $projectedOff = $projectedStart-$sLength;
	}
	my $missingStartBasesQ = $unalignedStart-$projectedOff;
	
	if ($unalignedStart == 0) {
	    $startStatus = "FULL";
	} elsif ( ($projectedOff > 0) && ($missingStartBasesQ < $fivePrimeWiggleRoom) ) {
	    $startStatus = "GAP";
	} elsif ($unalignedStart < $fivePrimeWiggleRoom) {
	    $startStatus = "INC";
	} else {
	    $startStatus = "TRUNC";
	}

	my $unalignedEnd = $qLength-$qStop;
	my $projectedEnd = undef;
	if ($strand eq "Plus") {
	    $projectedEnd = $sStop + $unalignedEnd;
	} else {
	    $projectedEnd = $sStart - $unalignedEnd;
	}
	
	my $endStatus = undef;
	
	$projectedOff = 0;
	if ($projectedEnd < 1) {
	    $projectedOff = 1-$projectedEnd;
	} elsif ($projectedEnd > $sLength) {
	    $projectedOff = $projectedEnd-$sLength;
	}
	my $missingEndBases = $unalignedEnd-$projectedOff;
	
	if ($unalignedEnd == 0) {
	    $endStatus = "FULL";
	} elsif ( ($projectedOff > 0) && ($missingEndBases < $threePrimeWiggleRoom) ) {
	    $endStatus = "GAP";
	} elsif ($unalignedEnd < $threePrimeWiggleRoom) {
	    $endStatus = "INC";
	} else {
	    $endStatus = "TRUNC";
	}


	# Convert to scaffold coordinate system if srfFile is specified
	if (defined($srfFile)) {
	    
		my $mapInfo = $contigScaffoldMap{$subject};
		unless ($mapInfo) {
		    $rejected{"NO_SCAFFOLD_INFO"}++;
		    next;
		}
		my ($contigScaffStrand,$scaffID,$contigScaffStart,$contigScaffEnd) = $mapInfo =~ /^([\+\-])(.+)\.(\d+)\.(\d+)$/;

		unless(exists($scaffLengths{$scaffID})) {
		    die "Error: Length information for scaffold $scaffID missing.\n";
		}
		my $scaffLen = $scaffLengths{$scaffID};
		$contigScaffStrand = ($contigScaffStrand eq "+") ? "Plus" : "Minus";
		my $scaffStrand = ($contigScaffStrand eq $strand) ? "Plus" : "Minus";
		
		my $scaffStart = ($contigScaffStrand eq "Plus") ? ($contigScaffStart + $sStart - 1) : ($contigScaffEnd - $sStop + 1);
		my $scaffStop = ($contigScaffStrand eq "Plus") ? ($contigScaffStart + $sStop - 1) : ($contigScaffEnd - $sStart + 1);
		
		$subject = $scaffID;
		$sLength = $scaffLen;
		$strand = $scaffStrand;
		$sStart = $scaffStart;
		$sStop = $scaffStop;

		$projectedStart = ($strand eq "Plus") ? $sStart - $unalignedStart : $sStop + $unalignedStart;
		$projectedEnd = ($strand eq "Plus") ? $sStop + $unalignedEnd : $sStart - $unalignedEnd;

		$line = join("\t",$blastType,$query,$qStart,$qStop,$qLength,
			     $subject,$sStart,$sStop,$sLength,$strand,$score,$eValue,$identities,$alignLength);

	}

	# Assess alignment location relative to scaffold (pointing out, pointing in, or in the middle)
	
	my $location = "UNK";
	my $endDistance = $qLength;
	
	if ($strand eq "Plus") {
	    if ($projectedStart > ($sLength-$endDistance)) {
		$location = "OUT";
	    } elsif ($projectedEnd < $endDistance) {
		$location = "IN";
	    } else {
		$location = "MID";
	    }
	} else {
	    if ($projectedStart < $endDistance) {
		$location = "OUT";
	    } elsif ($projectedEnd > ($sLength-$endDistance)) {
		$location = "IN";
	    } else {
		$location = "MID";
	    }
	}
	
	my $readStatus = "$startStatus.$endStatus.$location";

	if ($readStatus =~ /TRUNC/) {
	    $rejected{"TRUNCATED"}++;
	    next;
	}

	unless ($readStatus =~ /GAP/) {
	    $rejected{"UNINFORMATIVE"}++;
	    next;
	}
	
	if (defined($currentRead)) {
	    if ($query eq $currentRead) {
		$readInfo .= "$readStatus\t$line\n";
		$currentReadAlignments++;
	    } else {
		if ($currentReadAlignments > 1) {
		    $totalSplintsFound += processRead($readInfo);
		} else {
		    $rejected{"SINGLETON"}++;
		}
		$currentRead = $query;
		$readInfo = "$readStatus\t$line\n";
		$currentReadAlignments = 1;
	    }
	} else {
	    $currentRead = $query;
	    $readInfo = "$readStatus\t$line\n";
	    $currentReadAlignments = 1;
	}
    }
    close B;
    print STDERR "Done.\n";

    if (defined($currentRead)) {
	if ($currentReadAlignments > 1) {
	    $totalSplintsFound += processRead($readInfo);
	} else {
	    $rejected{"SINGLETON"}++;
	}
    }
}
print STDERR "Total alignments found: $totalHitsFound\n";
print STDERR "Unused alignments:\n";
my $totalRejected =0;
foreach my $r (@rejectReasons) {
    my $n = $rejected{$r};
    if ($n) {
	print STDERR "\t$r\t$n\n";
	$totalRejected += $n;
    }
}
print STDERR "\tTotal unused = $totalRejected\n";
print STDERR "Total splints found = $totalSplintsFound\n";

$date = `date`;
chomp $date;
print STDERR "Done. $date\n";


# -------------
# |SUBROUTINES|
# -------------

sub processRead {
    my ($info) = @_;
    my @alignments = split(/\n/,$info);
    my $nAligns = scalar(@alignments);
    my $nSplintsFound = 0;

    my %alignInfo = ();
    for (my $a = 0; $a < $nAligns; $a++) {
	$alignInfo{$a} = [split(/\t/,$alignments[$a])];
	#get rid of any spaces in read name
	$alignInfo{$a}->[2] =~ s/\s/_/;
    }


    
    if (defined($srfFile)) {
	# consolidate multiple alignments to the same scaffold
	my %alignInfoConsolidated;
	my %alignInfoPerSubjIdx;

	for (my $a = 0; $a < $nAligns; $a++) {
	    my $subj = $alignInfo{$a}->[6];
	    push (@{$alignInfoPerSubjIdx{$subj}}, $a);
	}

	my $sa =0;
	foreach my $subj (keys %alignInfoPerSubjIdx) {
	    # Consolidate all alignments to this subject into one;

	    my @sortedSubjAlignIdx = sort {$alignInfo{$a}->[3] <=> $alignInfo{$b}->[3]}  @{$alignInfoPerSubjIdx{$subj}} ;

	    # Alignments are sorted by qStart, so we only need to "grow" it in one direction on the query
	    $alignInfoConsolidated{$sa} = $alignInfo{$sortedSubjAlignIdx[0]};
	    foreach (@sortedSubjAlignIdx) {
		my ($qStop,$sStart,$sStop,$strand) = @{$alignInfo{$_}}[4,7,8,10];
		if ( $qStop > $alignInfoConsolidated{$sa}->[4] &&   ($sStop > $alignInfoConsolidated{$sa}->[8] || $sStart < $alignInfoConsolidated{$sa}->[7]) ) {
		    $alignInfoConsolidated{$sa}->[4] = $qStop;
		     # alignment to subjects can be + or -, so we move the sStart & sStop  accordingly.
		    if ($strand eq "Plus") {
			$alignInfoConsolidated{$sa}->[8] = $sStop;
		    } else { # shift bask the sStart
			$alignInfoConsolidated{$sa}->[7] = $sStart;
		    }
		}
	    }
	    $sa++;
	}
	#override original alignInfo with one-per-subject consolidated alignments
	%alignInfo = %alignInfo;
	$nAligns = $sa;

    }



    my @sortedAlignIndex = sort {$alignInfo{$a}->[3] <=> $alignInfo{$b}->[3]} keys(%alignInfo);

    my @dataSlice = (0,2,3,4,5,6,7,8,9,10);
#   $readStatus,$query,$qStart,$qStop,$qLength,$subject,$sStart,$sStop,$sLength,$strand

    my %finalSplints;
#   $splintLabel => [0]$readStatus,$query,$qStart,$qStop,$qLength,$subject,$sStart,$sStop,$sLength,$strand
#                   [1]$readStatus,$query,$qStart,$qStop,$qLength,$subject,$sStart,$sStop,$sLength,$strand

    for (my $a = 0; $a < $nAligns-1; $a++) {
	my $thisOne = $sortedAlignIndex[$a];
	my $thisInfo = $alignInfo{$thisOne};
	my @thisInfo = @{$thisInfo}[@dataSlice];
	my ($thisStatus,$thisQStart,$thisQStop,$thisObj,$thisSStart,$thisSStop,$thisStrand) = @thisInfo[0,2,3,5,6,7,9];
	my ($thisStartStat,$thisStopStat,$thisDirStat) = $thisStatus =~ /^(.+)\.(.+)\.(.+)$/;


	my $nextOne = $sortedAlignIndex[$a+1];
	my $nextInfo = $alignInfo{$nextOne};
	my @nextInfo = @{$nextInfo}[@dataSlice];
	my ($nextStatus,$nextQStart,$nextQStop,$nextObj,$nextSStart,$nextSStop,$nextStrand) = @nextInfo[0,2,3,5,6,7,9];
	unless ($nextObj ne $thisObj) {
	    $rejected{"UNINFORMATIVE"}++;
	    next;
	}
	my $splintLabel;
	my ($nextStartStat,$nextStopStat,$nextDirStat) = $nextStatus =~ /^(.+)\.(.+)\.(.+)$/;
	 	    
	if (($thisStopStat eq "GAP") && ($nextStartStat eq "GAP")) {
	    my $thisObjEnd = ($thisStrand eq "Plus") ? "3" : "5";
	    my $nextObjEnd = ($nextStrand eq "Plus") ? "5" : "3";
	    
	    $splintLabel = "$thisObj.$thisObjEnd.$nextObj.$nextObjEnd";
	    
	    # if (exists $finalSplints{$splintLabel}) {
	    # 	# If the read already produced a splint for the same two objects, expand the recorded splint coorinates
		
	    # 	my ($qStop,$sStart,$sStop) = @{$finalSplints{$splintLabel}->[0]}[3,6,7];
	    # 	if ($thisQStop > $qStop  &&  ($thisSStop > $sStop || $thisSStart < $sStart ))  {
	    # 	    # alignments are sorted by qStart, so we only need to "grow" the updated splint in one direction on the query			
	    # 	    $finalSplints{$splintLabel}->[0][3] = $thisQStop;
	    # 	    # alignment to subjects can be + or -, so we move the sStart & sStop  accordingly.
	    # 	    if ($thisStrand eq "Plus") { 
	    # 		$finalSplints{$splintLabel}->[0][7] = $thisSStop;
	    # 	    } else { # shift bask the sStart
	    # 		$finalSplints{$splintLabel}->[0][6] = $thisSStart; 
	    # 	    }
	    # 	}
		
	    # 	($qStop,$sStart,$sStop) = @{$finalSplints{$splintLabel}->[1]}[3,6,7];
	    # 	if ($nextQStop > $qStop  &&  ($nextSStop > $sStop || $nextSStart < $sStart ))  {
	    # 	    # alignments are sorted by qStart, so we only need to "grow" the updated splint in one direction on the query			
	    # 	    $finalSplints{$splintLabel}->[1][3] = $nextQStop;
	    # 	    # alignment to subjects can be + or -, so we move the sStart & sStop  accordingly.
	    # 	    if ($nextStrand eq "Plus") { 
	    # 		$finalSplints{$splintLabel}->[1][7] = $nextSStop;
	    # 	    } else { # shift bask the sStart
	    # 		$finalSplints{$splintLabel}->[1][6] = $nextSStart; 
	    # 	    }
	    # 	}
	    # }
	    # else {
	    $finalSplints{$splintLabel}[0] = \@thisInfo;
	    $finalSplints{$splintLabel}[1] = \@nextInfo;
	    
#	    }
	}
	
    }

    while (my ($splintLabel, $objPairAR) = each %finalSplints) {
	my ($thisObj,$thisObjEnd,$nextObj,$nextObjEnd) = split(/\./, $splintLabel);
	my @thisInfo = @{ $objPairAR->[0] };
	my @nextInfo = @{ $objPairAR->[1] };

	#if objects are scaffolds, is this splint still valid in the context of the two scaffolds?  (GAP/INC/FULL etc. end statuses are wrt original contigs)
	if (defined($srfFile)) {
	    my ($thisSStart,$thisSStop,$thisSLen) = @thisInfo[6,7,8];
	    if ( ($thisObjEnd == 3 && ($thisSLen - $thisSStop > $threePrimeWiggleRoom)) 
		 ||
		($thisObjEnd == 5 && $thisSStart > $fivePrimeWiggleRoom) ){
		print STDERR "NOT A VALID INTER-SCAFFOLD SPLINT: $thisObj.$thisObjEnd\t[@thisInfo]\t$nextObj.$nextObjEnd\t[@nextInfo]\n";
		$rejected{"TRUNCATED"}++;
		next;
	    }
	    my ($nextSStart,$nextSStop,$nextSLen) = @nextInfo[6,7,8];	    
	    if ( ($nextObjEnd == 3 && ($nextSLen - $nextSStop > $threePrimeWiggleRoom)) 
		 ||
		 ($nextObjEnd == 5 && $nextSStart > $fivePrimeWiggleRoom) ){
		print STDERR "NOT A VALID INTER-SCAFFOLD SPLINT: $thisObj.$thisObjEnd\t[@thisInfo]\t$nextObj.$nextObjEnd\t[@nextInfo]\n";
		$rejected{"TRUNCATED"}++;
		next;
	    }

	}
	$nSplintsFound++;
	print "SINGLE\tSPLINT\t$thisObj.$thisObjEnd\t[@thisInfo]\t$nextObj.$nextObjEnd\t[@nextInfo]\n";
    }
    return $nSplintsFound;
}

