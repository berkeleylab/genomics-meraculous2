#! /usr/bin/env perl

use warnings;
use strict;

if ( scalar( @ARGV ) != 1 )
{
	print "usage: <scaffold_seq_file>\nOUT: contigs in fasta format\n";
	exit;
}

my @names = ();
my %seq = ();
my $name = '';

open(IN,$ARGV[0]) || die "Couldn't open file $ARGV[0]\n";
while (<IN>)
{
    chomp;
    if (/^>(.*)$/) { $name = $1; push @names, $name; next; }
    $seq{$name} .= $_;
}
close IN;



my @contigIds = ();
my @seqHashList = ();

foreach my $name (@names)
{
    my $remainingSeq = $seq{$name};
    my $contigCnt = 1;
    my $scaffoldStart = 1;
    my $scaffoldStop = 1;
    while ( $remainingSeq =~ /(\S+?)(N{10,})(\S+)/ )
    {
	my $contig = $1;
	my $gap = $2;
	$remainingSeq = $3;	
	$scaffoldStop = $scaffoldStart + length( $contig ) - 1;
	print ">${name}.${contigCnt}.${scaffoldStart}-${scaffoldStop}\n";
	while ($contig =~ /(.{1,60})/g)
	{
	    print $1, "\n";
	}
	$contigCnt++;
	$scaffoldStart += length( $contig ) + length( $gap );
    }
    # write the remainder of this scaffold entry
    my $contig = $remainingSeq;
    $scaffoldStop = $scaffoldStart + length( $contig );
    print ">${name}.${contigCnt}.${scaffoldStart}-${scaffoldStop}\n";
    while ($contig =~ /(.{1,60})/g)
    {
	print $1, "\n";
    }
}
