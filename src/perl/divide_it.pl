#!/usr/bin/env perl
#script by Jarrod Chapman <jarrod@mays.berkeley.edu> 



$n_args = @ARGV;
if ($n_args != 2) {
    print "Subdivide a fasta file into subfiles of <entries> entries\n";
    print "Usage: ./divide_it.pl <fasta file> <entries>\n";
    exit;
}

open(F,$ARGV[0]) || die "Couldn't open file $ARGV[0]\n";
my $entries = $ARGV[1];

my $done = 0;
my $filecount = 0;
my $entrycount = 0;
my $outfile = $ARGV[0] . "." . $filecount; 
open (OUT, ">$outfile");
$filecount += 1;

while (my $i = <F>) {
    if ($i =~ />/) {
	$entrycount += 1;
	if ($entrycount > $entries) {
	    close OUT;
	    $outfile = $ARGV[0] . "." . $filecount; 
	    open (OUT, ">$outfile");
	    $filecount += 1;
	    $entrycount = 1;
	}
    }
    print OUT $i;
}
close OUT;
close F;
