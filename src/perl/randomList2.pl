#!/usr/bin/env perl
#
# randomList2.pl by Jarrod Chapman <jchapman@lbl.gov> Mon May 17 14:18:58 PDT 2004
# Copyright 2004 Jarrod Chapman. All rights reserved.
#

use warnings;

my $n_args = @ARGV;
if ($n_args != 3) {
    print "Usage: ./randomList2.pl <file> <accept prob> <return limit>\n";
    exit;
}

open(F,$ARGV[0]) || die "Couldn't open $ARGV[0]\n";
my $pAccept = $ARGV[1];
my $returnLimit = $ARGV[2];

my $returned = 0;
while (my $line = <F>) {
    if ($returned >= $returnLimit) {
	last;
    }
    my $randomNumber = rand();
    if ($randomNumber < $pAccept) {
	print "$line";
	$returned++;
    }
}
close F;

