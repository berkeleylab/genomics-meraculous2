#!/usr/bin/env perl

###############################################################################
#  SCRIPT:  split_and_validate_reads.pl                                       #
#  Validates Illumina-style read names and pairing tags in fastq-             #
#  formatted files,  enforces the interleaved pairing of read1 and read2,     #
#  then, optionally, groups reads into chunks of specified number.            #
#  Automatically unzips files with .gz or .z extensions.                      #
###############################################################################

use strict;
use warnings;

use File::Basename;

use lib "$ENV{MERACULOUS_ROOT}/lib";
use M_Utility qw(parse_fastq_header);

###############################################################################
#  GLOBAL SETTINGS                                                            #
###############################################################################

my ($SCRIPT) = ($0 =~ /([^\/]+)$/);  # script name - strip path from script name
 

###############################################################################
#  CHECK COMMAND LINE OPTIONS                                                 #
###############################################################################
 
use Getopt::Std;
my %opts=();

 
unless (getopts('c:d:f:o:p:r:s:M:Nh' ,  \%opts) ) {
    print "${SCRIPT}: ERROR - Unknown option(s) AND/OR option(s) without argument\n";
    exit -1;
}
 

###############################################################################
#  CHECK ARGUMENTS                                                            #
###############################################################################
 
if (keys(%opts) < 2 || keys(%opts) > 7 || defined($opts{h})) {
    print <<END_HERE ;

    $SCRIPT -f < seq_file | "seq_file1 seq_file2" > < -s <n_sequences> | -c <n_chunks> >  [-o qscore_offest] [-p output_prefix] [-r prob:max]

    Validates Illumina-style header and q-score format, enforces interleaved pairing, and groups reads into chunks of specified size
    Automatically unzips files with .gz, .z or .bz2 extensions

    Required, mutually-exclusive params:
      -s <n_sequences> :  split output into chunks of this many reads
      -c <n_chunks>    :  split output into this many chunks of equal size

    Optional params:
      -o <q_offset>    :  validate that all qscores match the specified Phred-offest value (33 or 64)
      -p <prefix>      :  output prefix
      -r <prob> <max>  :  set aside a random sampling of the input reads at the specified probabilty and up to the specified maximum number of reads
      -d <rate>        :  downsample the entire data set to this rate (i.e. KEEP this fraction of reads)
      -N               :  (NoValidate) turns off validation of pairing, header, sequence, and qscores; reads will be printed as is
      -M <n_reads>     :  (MaxValidate) validate only the first n reads

END_HERE
exit 0;
}


###############################################################################
#  MAIN                                                                       #
###############################################################################

select STDERR; $| = 1;  # make unbuffered - standard error
select STDOUT; $| = 1;  # make unbuffered - standard output
 
my @files   = split(/\s+/,$opts{f}); 
my $dual;
if (@files == 2) { $dual = ""; }
elsif(@files > 2) { die("ERROR: too many input files. Max is 2 files\n");}

my $prefix = $opts{p};

my $filename = basename($files[0]); #there is only one output prefix, so we're setting it based on the first file, unless set on the cmd line
if ($filename =~ /^(\S+)(\.gz|\.z|\.bz2)$/) 
{
    $prefix = $1 unless (defined($prefix)); 
}
else
{
    $prefix = $filename unless (defined($prefix));
}

my $offset = $opts{o};
if ( (defined($offset)) && ($offset != 33 && $offset != 64) )
{
    die("ERROR: unsupported qscore offset: $offset \n");
}
my $nSeqsMax = defined($opts{s}) ? $opts{s} : undef ; 
my $nChunks = defined($opts{c}) ? $opts{c} : undef ;

if ( defined($opts{s}) && defined($opts{c})  )
{
    die("ERROR: parameters -s and -c are mutually exclusive\n");
}

my $noValidate = $opts{N};
my $maxValidate = $opts{M};


# variables for random sampling of reads
my $pAccept;            # random sampling probability (if -r )
my $returnLimit;        # max number of random samples
if (defined $opts{r}) 
{
    my @randomOpts = split(/\:/,$opts{r});
    $pAccept = $randomOpts[0];
    $returnLimit = $randomOpts[1];

    open (SAMPLE, ">${prefix}.sample") || die ("ERROR: can't create file\n");
}


my $downsampleOutRate = defined $opts{d} ? $opts{d} : undef;

### pre-calculate the ASCII->num translations for qstring validation
my %map;
unless ( defined($noValidate) || ! defined($offset) )
{
    %map = map { chr($_) => ($_-$offset)." " } 0x00..0xFF;
    while ( my ($k, $v) = each %map ) { undef $map{$k} unless ($v > 0 && $v < 42) }  # keep only chars that map to allowed phred scores
    
}


### process the reads

my ($count1, $count2) = process_fastq(\@files, $prefix, \*SAMPLE);



unless ($noValidate){
    if ($count1 == 0 || $count2 == 0)    {
	print STDERR "ERROR: no pairs found in $filename\n";
	exit(1);
    }
    if ($count1 != $count2)    {
	print STDERR "ERROR:  unequal number of fwd and rev reads\n";
	exit(1);
    }
}
 


sub process_fastq {

#  Parse a singe fastq read. Read validation assumes interleaved pairing and will complain if any pairs are missing or out of order. 
#  Turn off validation (=N) if this is not the case


    my ($filesAR, $prefix, $samp_fh) = @_;

    my $file = $$filesAR[0];
    my $file2 = defined $dual ? $$filesAR[1] : undef;

    my $count1 = my $count2 = 0;    #counters for /1 and /2 reads

    my ($fc_coords, $fc_coords2);
    my ($direction, $direction2);
    my ($seq, $seq2);
    my ($qstr, $qstr2);
    my ($barcode, $barcode2);
    my $validPair = "";


    my $n_files = 0;        # number of files created
    my $index = 0;          # number of sequences of current file
    my $returned=0;         # random sampling counter
    my $rr=0;               # round-robin counter
    my $bin;



    if (basename($file) =~ /^(\S+)(\.gz|\.z)$/) 
    {
	open (IN,  "gunzip -c $file |") || die "can't open compressed file $file\n";
    }
    elsif (basename($file) =~ /^(\S+)(\.bz2)$/) 
    {
	open (IN,  "bunzip2 -c $file |") || die "can't open compressed file $file\n";
    }

    else 
    {
	open (IN, $file) || die "can't open file $file\n";
    }

    if (defined $file2) 
    {
	if (basename($file2) =~ /^(\S+)(\.gz|\.z)$/) 
	{
	    open (IN2,  "gunzip -c $file2 |") || die "can't open compressed file $file2\n";
	}
	elsif (basename($file2) =~ /^(\S+)(\.bz2)$/) 
	{
	    open (IN2,  "bunzip2 -c $file2 |") || die "can't open compressed file $file2\n";
	}
	
	else 
	{
	    open (IN2, $file2) || die "can't open file $file2\n";
	}

    }

    my $out_filename;      

    # create filehandles if we have a predetermined number of chunks
    my @filehandles;
    if (defined $nChunks) 
    {
	for (my $i=0; $i<$nChunks; $i++)
	{
	    #localize the FILE glob
	    local *OUT;
	    $out_filename = sprintf "${prefix}_%05d", $i;
	    open (OUT, ">$out_filename") || die "can't open file $out_filename\n";
	    push (@filehandles, *OUT);
	}
    }

    # read two reads at a time, either from the single fastq or from the two paired files in parallel.
    while (my $r1 = ( <IN> . <IN> . <IN> . <IN> )) {

	my $r2 = defined $file2 ? ( <IN2> . <IN2> . <IN2> . <IN2> ) : (<IN> . <IN> . <IN> . <IN>) ;

 	# downsample all reads by skipping pairs at set prob
 	next if (defined $downsampleOutRate && ( rand() > $downsampleOutRate) );

	# validate reads
	unless ($noValidate || (defined $maxValidate && ($count1+$count2) >= $maxValidate))
	{
	    ($fc_coords, $barcode, $direction, $seq, $qstr) = validate_read($r1);
	    unless ($fc_coords && $direction && $seq && $qstr) {die ("Read format validation failed!\n ($r1)");}
	    $count1++;

	    ($fc_coords2, $barcode2, $direction2, $seq2, $qstr2) = validate_read($r2);	
	    unless ($fc_coords2 && $direction2 && $seq2 && $qstr2) {die ("Read format validation failed!\n ($r2)");}
	    $count2++;

	    unless (($fc_coords eq $fc_coords2) && ($direction ne $direction2)) {die ("ERROR: invalid pairing; read1 and read2 are not a pair!\n ($r1------\n$r2)");}

	}
	$validPair = "$r1"."$r2";

	if (! $validPair ) {
	    die "ERROR: couldn't get a valid pair of reads from fastq file!\n";
	}

        #parse into bins 

        if ($nChunks)  # output round robin into pre-defined bins;
        {
	    $bin = $filehandles[$rr];
	    print $bin $validPair;
	    $rr++;
	    if ($rr == $nChunks) {$rr=0}
        }
        else  # create new bins as we go
	{
	    if ($index == 0) 
	    {
		# first pass
		$out_filename = sprintf "${prefix}_%05d", ++$n_files;
		open (OUT, ">$out_filename") || die "ERROR: can't create file\n";
		print OUT $validPair;
		
		$index += 2;
	    } 
	    elsif ($index >= $nSeqsMax) 
	    {
		# completed file
		close OUT;
		$out_filename = sprintf "${prefix}_%05d", ++$n_files;
		open (OUT, ">$out_filename") || die "ERROR: can't create file\n";
		print OUT $validPair;
		$index = 2;
	    }
	    else 
	    {
		# add to current file
		print OUT $validPair;
		$index += 2;
	    }
	}
	next unless (defined $opts{r});


	# print each read at a set probability to the random sampling bins
	next if ( defined($returnLimit) && $returned >= $returnLimit );

	if ( rand() < $pAccept) {
	    print $samp_fh $r1,$r2;
	    $returned += 2;
	}
    }
    close IN;
    close IN2 if ($dual);
    close OUT;
    foreach my $fh (@filehandles) { close $fh }

    return ($count1, $count2);
}



sub validate_read {

    my $read = shift;
    my $format;
    my $fc_coords;
    my $barcode;
    my $direction;

    my ($header, $seq, $qhdr, $qstr) = split /\n/, $read;
    unless ($header && $seq && $qstr) {
	# not a fastq read
	print STDERR "ERROR: entry not recognized as a fastq read:\n";
        print STDERR $read;
        return 0;
    }

    ($format, $fc_coords, $direction, $barcode)  = parse_fastq_header($header);
    
    unless (defined $format ) {
	print STDERR "ERROR: read header not in a recognized naming convention\n";
        return 0;
    }
    if ($seq =~ m/[^AaCcTtGgNn]/) 
    {
	print STDERR "ERROR: invalid sequence: $seq\n";
	return 0;
    }
    if (defined $offset)
    {
	if (! validate_qstr($qstr)) 
	{
	    print STDERR "ERROR: invalid quality string: $qstr\n";
	    return 0;
	}
    }
    return ($fc_coords, $barcode, $direction, $seq, $qstr);
}

sub validate_qstr {

    my $qualstring = $_[0];
    chomp $qualstring; 
    for (split //, $qualstring) {
	if ( ! $map{$_} )  
	{
	    print STDERR "ERROR: qscore string doesn't agree with the specified encoding/offset\n";
	    return 0;
	}
    }	    
    return 1;
}





