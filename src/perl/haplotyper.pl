#!/usr/bin/env perl 
#
# haplotyper.pl
# Eugene Goltsman <egoltsman@lbl.gov>
# Last revision 02/24/2016

use warnings;
use strict;

use Getopt::Std;

my %opts = ();
my $validLine = getopts('s:c:V', \%opts);
my @required = ("s","c");
my $nRequired = 0;
map {$nRequired += exists($opts{$_})} @required;
$validLine &= ($nRequired == @required);
if ($validLine != 1) {
    print STDERR "Usage: ./haplotyper.pl  <-s srfFile> <-c clonesMapFile> [ -V(erbose)] \n";
    exit;
}

my $srfFile = $opts{"s"};
my $clonesMapFile = $opts{"c"};
my $verbose = $opts{"V"};
my $min_clones = 1;


my $srfOutFile = $srfFile.".htyped";

my %contigsClonesHoH = ();  # diplotigName -> hash of clones that mapped to it
my %clonesContigsHoA = ();  # clone -> array of diplotigs that it mapped to



open(MAP,$clonesMapFile) || die "Can't open $clonesMapFile for reading";
print STDERR "Reading clone map file: $clonesMapFile ...\n";

while (my $line = <MAP>) {
    chomp $line;
    my @fields = split(/\t/,$line);
    my $c = $fields[0];
    my @clones =  split(/\,/,$fields[1]);
    for my $clone (@clones) {
	push (@{$clonesContigsHoA{$clone}}, $c);
    }
}
close MAP;
print STDERR "Done.\n";


for my $clone (keys %clonesContigsHoA) {
    # if this clone mapped to more than one contig it will be useful in haplotyping
    if (@{$clonesContigsHoA{$clone}} > 1) {
	for my $c ( @{$clonesContigsHoA{$clone}}) {
	    $contigsClonesHoH{$c}{$clone} = 1; #load contig-clone pairs into hash of hashes for quick searching
	}
    }
}

open (OUT,">$srfOutFile") || die "Couldn't open $srfOutFile for writing\n";

print STDERR "Reading scaffold report file: $srfFile ...\n";
open (S,$srfFile) || die "Couldn't open $srfFile\n";


# Scaffolds are divided into three groups which will later get merged and re-sorted.
my %variant1SR;  # verified/fixed variant1 scaffolds 
my %variant2SR;  # single-diplotig variant2 scaffolds  (could contain _p1 or _p2 diplotigs)
my %tempVariant2SR;  # initial single-diplotig variant2  scaffolds ( contain only _p2 diplotigs)
my %invariantSR;  # haploid scaffolds (made up of isotigs only)

my @scaffoldDiplotigs;
my %convertedVariant2contigs;  #list of diplotigs that participated in a _p1 -> _p2 swap
my $scaffName;
my $scaffoldRecord;

my $nBlessedViaMaxDepth = 0;	
my $nBlessedViaLinkage = 0;	


while (my $line = <S>) {
    chomp $line;
    my @cols = split(/\t/,$line);

    if (! defined $scaffoldRecord) {  #first line
	@scaffoldDiplotigs = ();
	$scaffoldRecord = "$line\n";
	$scaffName = $cols[0]; 
    }
    elsif ( $cols[0] eq $scaffName  ) {  # same scaffold; continue adding to it
	$scaffoldRecord .= "$line\n";
    } 
    else { 	# new scaffold, update everything for the previous scaffold and reset
	$scaffName =~ /^Scaffold(\d+)/;
	my $sNum = $1;
	if ( scalar(@scaffoldDiplotigs) > 1 ) {
	    $variant1SR{$sNum} = phase_haplotypes($scaffoldRecord, \@scaffoldDiplotigs, \%contigsClonesHoH);
	    if (! defined $variant1SR{$sNum}) { die "Error: Could't bless all diplotigs in $scaffName\n"; }
	}		
	elsif ( scalar(@scaffoldDiplotigs) == 1 && $scaffoldDiplotigs[0] =~ /_p1/ ) {
	    $variant1SR{$sNum} = $scaffoldRecord;
	}
	elsif ( scalar(@scaffoldDiplotigs) == 1 && $scaffoldDiplotigs[0] =~ /_p2/ ) {
	    $tempVariant2SR{$sNum} = $scaffoldRecord;
	}
	else {
	    $invariantSR{$sNum} = $scaffoldRecord;
	}

	@scaffoldDiplotigs = (); 
	$scaffName = $cols[0]; 
	$scaffoldRecord = "$line\n";
    }

    if ( ($cols[1] =~ /^CONTIG/) && ($cols[2] =~ /diplotig\d+/)) {
	my ($contigName) = $cols[2] =~ /^[\+\-](.+)$/;
	push (@scaffoldDiplotigs, $contigName);
    }
}
# process last scaffold 
$scaffName =~ /^Scaffold(\d+)/;
my $sNum = $1;
if ( scalar(@scaffoldDiplotigs) > 1 ) {
    print STDERR "\nPhasing $scaffName ...\n" if ($verbose);
    $variant1SR{$sNum} = phase_haplotypes($scaffoldRecord, \@scaffoldDiplotigs, \%contigsClonesHoH);
    if (! defined $variant1SR{$sNum}) { die "Error: Could't haplotype all diplotigs in $scaffName\n"; }
}		
elsif ( scalar(@scaffoldDiplotigs) == 1 && $scaffoldDiplotigs[0] =~ /_p1/ ) {
    $variant1SR{$sNum} = $scaffoldRecord;
}
elsif ( scalar(@scaffoldDiplotigs) == 1 && $scaffoldDiplotigs[0] =~ /_p2/ ) {
    $tempVariant2SR{$sNum} = $scaffoldRecord;
}
else {
    $invariantSR{$sNum} = $scaffoldRecord;
}

close S;

print STDERR "\n\n";
print STDERR "Total variant contig pairs blessed via kmer depth:  $nBlessedViaMaxDepth\n";
print STDERR "Total variant contig pairs blessed via clone linkage:  $nBlessedViaLinkage\n";

# when building variant-1 scaffolds we've swapped some _p2 diplotigs for _p1, so now we need to do the opposite swap in the corresponding variant-2 scaffolds 
for my $scaf (keys %tempVariant2SR) {
    my @cols = split(/\t/,$tempVariant2SR{$scaf});
    my ($ori, $contigName) = $cols[2] =~ /^([\+\-])(.+)$/;
    if (exists($convertedVariant2contigs{$contigName})) {
	my $altVarContigName = convert_to_alt_var_name($contigName);
	$cols[2] = $ori.$altVarContigName;
	$variant2SR{$scaf} = join("\t",@cols);
    }
    else {
	$variant2SR{$scaf} = $tempVariant2SR{$scaf};
    }
}

my %combinedSR = (%variant1SR, %variant2SR, %invariantSR);
for my $key (sort {$a<=>$b} keys %combinedSR ) {
    print OUT "$combinedSR{$key}";
} 
close OUT;

my $altVariantScaffList = "alt.variant.scaffold.list";
open(ALTVARS,">alt.variant.scaffold.list") || die "Couldn't open file for writing!";
for my $key (sort {$a<=>$b} keys %variant2SR ) {
    print ALTVARS "Scaffold$key\n";
}
close ALTVARS;



sub phase_haplotypes {
    # Enforce a consistent haplotype across all diplotigs in the scaffold
    # We do this via recursive binning of diplotigs based on common clones.  Since the clones were mapped
    # to haplotype-specific parts of the diplotigs, this ensures that any two diplotigs sharing the same clone must be from the same haplotype.
    my ($scaffReportEntry, $scaffoldDiplotigsAR, $contigsClonesHoHR ) = @_;
    
    my %blessedIdx;  # Index of diplotigs blessed into the same  haplotype.  The alternative variant diplotigs of this set implicitly make up the other haplotype.
                     # Items are not necessarily added in the order they're in the input srf, but the index maintains that orderl
    my @newlyBlessed; # diplotigs blessed during the CURRENT pass through the list

    my @scaffoldDiplotigs = @$scaffoldDiplotigsAR;
    my @altVarDiplotigs;
    for my $dVar1 (@scaffoldDiplotigs) {
	my $dVar2 = convert_to_alt_var_name($dVar1);
	push (@altVarDiplotigs, $dVar2);
    }


    # make a list of diplotigs that lack linkage info apriori
    my %unlinkable = ();
    for (my $n=1; $n <= $#scaffoldDiplotigs; $n++) {
	unless ( exists $$contigsClonesHoHR{$scaffoldDiplotigs[$n]} || exists $$contigsClonesHoHR{$altVarDiplotigs[$n]} ) {  $unlinkable{$n} = 1 }
    }

    # bless the first diplotig of the scaffold based on depth alone 
    my $blessedDiplotig = bless_through_dMax($scaffoldDiplotigs[0], $altVarDiplotigs[0], $contigsClonesHoHR);
    push(@newlyBlessed, $blessedDiplotig);
    $blessedIdx{'0'} = $blessedDiplotig;
    $nBlessedViaMaxDepth++;	

    my $pass=0;
    while (@newlyBlessed){
	# keep checking diplotig pairs agains most recently blessed set
	$pass++;
	print STDERR "\tPass $pass\n" if ($verbose);
	my @blessedInPrevPass = @newlyBlessed;  
	@newlyBlessed = ();
	for (my $n=1; $n <= $#scaffoldDiplotigs; $n++) {
	    next if ( exists $unlinkable{$n}  );
	    next if ( exists $blessedIdx{$n} );  
	    # check this diplotig and its alt variant for likage with diplotigs blessed during the previous pass
	    my $blessedDiplotig = bless_variant($n, $scaffoldDiplotigs[$n], $altVarDiplotigs[$n], $contigsClonesHoHR, \@blessedInPrevPass);
	    if (defined($blessedDiplotig)) {
		push(@newlyBlessed, $blessedDiplotig);
		$blessedIdx{$n} = $blessedDiplotig;
		$nBlessedViaLinkage++;
		print STDERR "\tBlessed $blessedDiplotig via clone linkage.\n" if ($verbose);
	    }
	}
	next if (@newlyBlessed);

	# couldn't find any new diplotigs to bless via linkage, so we bless the next one based on depth alone 
	print STDERR "\tCLONE LINKAGE EXHAUSTED..." if ($verbose);
	for (my $n=1; $n <= $#scaffoldDiplotigs; $n++) {
	    next if ( exists $blessedIdx{$n} );
            next if ( exists $unlinkable{$n}  );  
	    my $blessedDiplotig = bless_through_dMax($scaffoldDiplotigs[$n], $altVarDiplotigs[$n], $contigsClonesHoHR);
	    push(@newlyBlessed, $blessedDiplotig);
	    $blessedIdx{$n} = $blessedDiplotig;
	    $nBlessedViaMaxDepth++;	
	    print STDERR "\tBlessed $blessedDiplotig  based on majority depth.\n" if ($verbose);
	    last;
	}
    }

    # now arbitrarily bless the unlinkable
    for my $n (keys %unlinkable) {
	my $blessedDiplotig = bless_through_dMax($scaffoldDiplotigs[$n], $altVarDiplotigs[$n], $contigsClonesHoHR);
	$blessedIdx{$n} = $blessedDiplotig;
	$nBlessedViaMaxDepth++;	
	print STDERR "\tBlessed unlinkable $blessedDiplotig  based on majority depth.\n" if ($verbose);
    }

    unless ( scalar(keys %blessedIdx) == scalar(@scaffoldDiplotigs) ) { 
	return undef;
    }
    
    # update the scaffold record so that it matches the variant-1 haplo-group content. If a diplotig is not in the haplo-group, we assume 
    # its alt. variant must be, so we swap them 
    my $newScaffReportEntry = $scaffReportEntry;
    for (my $i=0; $i <= $#scaffoldDiplotigs; $i++) {
	unless ($scaffoldDiplotigs[$i] eq $blessedIdx{$i}) {
	    $newScaffReportEntry =~ s/$scaffoldDiplotigs[$i]\t/$altVarDiplotigs[$i]\t/;
	    # record any swapped-in _p2 diplotigs so that we can update the variant-2 scaffolds later
	    $convertedVariant2contigs{$altVarDiplotigs[$i]} = 1;
	}
    }
    return $newScaffReportEntry;
}

sub bless_variant {
    # Check each variant diplotig from the pair against the reference set and return the one with at least $min_clones common clones
    my ($n, $d1, $d2, $contigsClonesHoHR, $refGroupAR) = @_;
    my $cloneCnt_d1 = 0;
    my $cloneCnt_d2 = 0;


#    print STDERR "DEBUG: $d1 | $d2 ...\n";
    for my $blessedContig (@$refGroupAR) {
	# check every clone mapped to $d1 for presence in the previously blessed diplotig
	$cloneCnt_d1 = 0;
	$cloneCnt_d2 = 0;
	for my $clone (keys %{ $contigsClonesHoHR->{$d1}}) {
	    if (exists( $contigsClonesHoHR->{$blessedContig}{$clone})) {
                # common clone found
#		print STDERR "DEBUG: \t$d1, $blessedContig:   common clone $clone\n";
                $cloneCnt_d1++;
		last if ($cloneCnt_d1 >= $min_clones);
            }
	}
	for my $clone (keys %{ $contigsClonesHoHR->{$d2}}) {
	    if (exists( $contigsClonesHoHR->{$blessedContig}{$clone})) {
#		print STDERR "DEBUG: \t$d2, $blessedContig:   common clone $clone\n";
                # common clone found
                $cloneCnt_d2++;
		last if ($cloneCnt_d2 >= $min_clones);
            }
	}
	if (($cloneCnt_d1 >= $min_clones) && ($cloneCnt_d2 >= $min_clones)) {
	    # ambiguous linkage to this contig
#	    print STDERR "\tAmbiguous haplotype linkage  ($d1 : $d2).\n";
	    next;
	}
	if (($cloneCnt_d1 >= $min_clones) || ($cloneCnt_d2 >= $min_clones)) {
	    # we've found enough links; no need to continue checking against the rest of the contigs
	    last; 
	}
    }

    if ( ($cloneCnt_d1 < $min_clones) && ($cloneCnt_d2 < $min_clones)) {
#	print STDERR "\tDEBUG:  insufficient haplotype linkage for either diplotig  ($d1: $cloneCnt_d1 linking clones;  $d2: $cloneCnt_d2 linking clones).\n";
	return undef;
    }
    return ($cloneCnt_d1 >= $min_clones) ? $d1 : $d2;
}

sub bless_through_dMax {
    # assign to haplo-group based strictly on clone depth 
    my ($d1, $d2, $contigsClonesHoHR) = @_;

    my $depth_d1 = scalar(keys %{ $contigsClonesHoHR->{$d1}});
    my $depth_d2 = scalar(keys %{ $contigsClonesHoHR->{$d2}});

    my $blessed = ($depth_d1 >= $depth_d2) ? $d1 : $d2;
    return $blessed;
}

sub convert_to_alt_var_name {
    # This is a generic function to convert between diplotig varian name into its alternative variant
    # Swap in new conversion rules as needed
    my $name = shift;
    my $newname;
    if ($name =~ /_p1/) { ($newname = $name) =~ s/_p1/_p2/ }
    elsif ($name =~ /_p2/) { ($newname = $name) =~ s/_p2/_p1/ }
    return $newname;
}
