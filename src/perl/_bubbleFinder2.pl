#!/usr/bin/env perl 
#
# bubbleFinder.pl by Jarrod Chapman <jchapman@lbl.gov> Sat Jan 29 20:12:18 PST 2011
# Copyright 2011 Jarrod Chapman. All rights reserved.
#

use warnings;
use strict;

use Getopt::Std;

use lib "$ENV{MERACULOUS_ROOT}/lib";
use M_Constants;
use M_Utility qw(parse_fastq_header);

use Text::Wrap;
$Text::Wrap::columns = 50;

use List::Util 'sum';

my %opts = ();
my $validLine = getopts('c:f:d:q:t:V', \%opts);
my @required = ("c","f","d");
my $nRequired = 0;
map {$nRequired += exists($opts{$_})} @required;
$validLine &= ($nRequired == @required);
if ($validLine != 1) {
    print "Usage: ./bubbleFinder.pl <-c ceaFile> <-f fastaFile> <-d depthFile> [ -q fastqInfoFile -t threads -V(erbose)] \n";
    exit;
}

my $ceaFile = $opts{"c"};
my $fastaFile = $opts{"f"};
my $cmdFile = $opts{"d"};
my $fastqInfoFile  = $opts{"q"};
my $threads = $opts{"t"};
my $verbose = $opts{"V"};


my $merLength = undef;
my $hairLength = undef;

my %contigInfo = ();  # map from contig to cea info
my %tipInfo = ();     # map from mer-tip pair to all contigs they flank

my %bubbleMap = ();  # map from contigs in bubbles to their bubble IDs 
my %bubbletigs = (); # map from bubble ID to mer-tips defining the bubble 
my %linkertigs = (); # map from non-bubble contigs to their mer-tips

my $nCompleteBubbles = 0;
my $nBlessedPaths = 0;


print STDERR "Reading contig end data from $ceaFile...\n";

open (F,$ceaFile) || die "Couldn't open $ceaFile\n";
while (my $line = <F>) {
    chomp $line;
    my ($contig,$prevCode,$prevBase,$firstMer,$contigLength,$lastMer,$nextBase,$nextCode) = split(/\s+/,$line);
    unless (defined($merLength)) {
	$merLength = length($firstMer);
	$hairLength = 2*$merLength - 1;
    }

    ($prevCode) = $prevCode =~ /\[(..)\]/;
    ($nextCode) = $nextCode =~ /\[(..)\]/;
    ($prevBase) = $prevBase =~ /\((.)\)/;
    ($nextBase) = $nextBase =~ /\((.)\)/;

    my $prevMer = $prevBase . $firstMer;
    $prevMer = substr($prevMer,0,$merLength);

    my $nextMer = $lastMer . $nextBase; 
    $nextMer = substr($nextMer,1,$merLength);
    
    # hair and sub-hair must be FU-UF or UF-FU

    if ($contigLength <= $hairLength) {
	unless ( ( ($prevCode =~ /F[ACGT]/) && ($nextCode =~ /[ACGT]F/) ) || ( ($prevCode =~ /[ACGT]F/) && ($nextCode =~ /F[ACGT]/) ) ) {
	    next;
	}
    } 

    my $statusField = ($contigLength > $hairLength) ? 1 : 0;
    $contigInfo{$contig} = "$prevCode:$prevBase:$firstMer:$contigLength:$lastMer:$nextBase:$nextCode:$statusField";

    if (($prevCode =~ /F[ACGT]/) || ($nextCode =~ /[ACGT]F/)) {

	my $extensions = "";
	if ($prevCode =~ /F[ACGT]/) {
	    $extensions = "$prevMer.";
	} else {
# I think this is a bug
#	    $extensions = "0";    
# should be
	    $extensions = "0.";    
	}
	if ($nextCode =~ /[ACGT]F/) {
	    $extensions .= "$nextMer";
	} else {
	    $extensions .= "0";
	}

	$linkertigs{$contig} = $extensions;

   } elsif (($prevCode =~ /[ACGT]F/) && ($nextCode =~ /F[ACGT]/)) {

	my $tips = $prevMer . "." . $nextMer;
	my $rcTips = reverse($tips);
	$rcTips =~ tr/ACGT/TGCA/;

	my $rcFlag = "+";
	my $tipKey = $tips;
	if ($rcTips lt $tips) {
	    $tipKey = $rcTips;
	    $rcFlag = "-";
	}
    
	if (exists($tipInfo{$tipKey})) {
	    $tipInfo{$tipKey} .= ",$rcFlag$contig";
	} else {
	    $tipInfo{$tipKey} = "$rcFlag$contig";
	}

	$bubbleMap{$contig} = 0;

    } else {

    }

}
close F;

print STDERR "Done.\n";

# store contig sequence for later use 

print STDERR "Reading contig sequence from $fastaFile...\n";

my %contigSequences = ();
my $currentEntry = undef;
my $currentSeq = "";
open (F,$fastaFile) || die "Couldn't open $fastaFile\n";
while (my $line = <F>) {
    chomp $line;
    if ($line =~ /^>/) {
        if (defined($currentEntry) && exists($contigInfo{$currentEntry})) {
	    $contigSequences{$currentEntry} = $currentSeq;
        }
        $currentSeq = "";
        ($currentEntry) = $line =~ /^>(.+)$/;
    } else {
        $currentSeq .= $line;
    }
}
if (defined($currentEntry) && exists($contigInfo{$currentEntry})) {
    $contigSequences{$currentEntry} = $currentSeq;
}
close F;

print STDERR "Done.\n";

# store contig mean depth for later use 

print STDERR "Reading contig depth info from $cmdFile...\n";

open (F,$cmdFile) || die "Couldn't open $cmdFile\n";
while (my $line = <F>) {
    chomp $line;

    my ($contigID,$nMers,$meanDepth) = split(/\s+/,$line);

    if (exists($contigInfo{$contigID})) {
	$contigInfo{$contigID} .= ":$nMers:$meanDepth";
    }
}
close F;

print STDERR "Done.\n";


my $bubbleID = 0;

# find bubbles from convergent tips

print STDERR "Finding bubbles...\n";

while (my ($tipKey,$contigs) = each(%tipInfo)) {
    my @contigs = split(/\,/,$contigs);
    my $nContigs = scalar(@contigs);

    if ($nContigs == 2 ) {        # a valid diploid bubble

	$bubbleID++;
	$bubbletigs{$bubbleID} = $tipKey;

	print STDERR "BUBBLE $bubbleID $tipKey [@contigs]\n" if ($verbose);

	foreach my $contigID (@contigs) {
	    my ($rcFlag,$contig) = $contigID =~ /^([\+\-])(.+)/;
	   
	    unless(exists($bubbleMap{$contig})) {
		die "Inconsistency in bubbleMap for contig $contig.\n";
	    } else {
		$bubbleMap{$contig} = $bubbleID;
	    }
	}
    }
}

print STDERR "Done.\n";

# Build the bubble-contig graph (%pointsTo contains the edge info)

print STDERR "Building bubble-contig graph...\n";

my %pointsTo = ();

while (my ($bubbleID,$links) = each(%bubbletigs)) {
    my ($in,$out) = $links =~ /^(.+)\.(.+)$/;
    my $rcLinks = reverse($links);
    $rcLinks =~ tr/ACGT/TGCA/;
    my ($rcin,$rcout) = $rcLinks =~ /^(.+)\.(.+)$/;

    if (exists($pointsTo{$in})) {
	$pointsTo{$in} .= ",B+$bubbleID";
    } else {
	$pointsTo{$in} = "B+$bubbleID";
    }
    
    if (exists($pointsTo{$rcin})) {
	$pointsTo{$rcin} .= ",B-$bubbleID";
    } else {
	$pointsTo{$rcin} = "B-$bubbleID";
    }
}

while (my ($contigID,$links) = each(%linkertigs)) {
    my ($in,$out) = $links =~ /^(.+)\.(.+)$/;
    my $rcLinks = reverse($links);
    $rcLinks =~ tr/ACGT/TGCA/;
    my ($rcin,$rcout) = $rcLinks =~ /^(.+)\.(.+)$/;

    if ($in) {
	if (exists($pointsTo{$in})) {
	    $pointsTo{$in} .= ",C+$contigID";
	} else {
	    $pointsTo{$in} = "C+$contigID";
	}
    }
    
    if ($rcin) {
	if (exists($pointsTo{$rcin})) {
	    $pointsTo{$rcin} .= ",C-$contigID";
	} else {
	    $pointsTo{$rcin} = "C-$contigID";
	}
    }
}

print STDERR "Done.\n";


my $MODE;
my $contigsClonesHoHR;
if (defined $fastqInfoFile) {
     $MODE = "haploPaths";
     print STDERR "\nWill map reads to bubbles and attempt to phase bubble-paths ...\n";

     print STDERR "Mapping reads to bubbles...\n";

     # make a fasta file out of contigs that we've defined as bubbletigs
     my $bubbleContigsFile = "bubble_UUtigs.fa";
     open(FA, ">$bubbleContigsFile") || die "Couldn't open file $bubbleContigsFile for writing";
     foreach my $cid (keys %bubbleMap) {
	 if ($bubbleMap{$cid}) {   # we found a bubble contig
	     print FA ">$cid\n".wrap('','',$contigSequences{$cid})."\n";
	 }
     }
     close FA;
     
     # map the reads, saving the clone names
     $contigsClonesHoHR = merblast_reads($bubbleContigsFile);

     print STDERR "Done.\n";
}
else {
    $MODE = "maxDepth";  # maxLength, minLength, maxDepth, haploPaths, or all
}


# traverse the bubble-contig graph, starting from each contig

open (LIST, ">haplotigs.list") || die "Couldn't open haplotigs.list for writing \n";

print STDERR "Generating diplotigs...\n";

my %visited = ();
my $diplotigID = 0;

while (my ($contigID,$links) = each(%linkertigs)) {

    my $current = "C+$contigID";
    my $currentID = "C$contigID";

    if (exists($visited{$currentID})) {
	next;
    } else {
	$visited{$currentID} = 1;
    }

    my @downstream = ();
    my $next = getNext($current);

    while ($next) {

	my ($type,$strand,$id) = $next =~ /^([CB])([\+\-])(.+)$/;

	my $nextID = $type . $id;

	if (exists($visited{$nextID})) {
	    warn "warning: Inconsistency encountered in forward walk from $contigID at $nextID.  Truncating.\n";
	    warn "warning: [$type][$strand][$id] [$nextID] [$visited{$nextID}]\n";
	    $next = 0;
	} else {
	    $visited{$nextID} = 1;
	    push (@downstream,$next);
	    $current = $next;
	    $next = getNext($current);
	}
    }

    $current = "C-$contigID";

    my @upstream = ();
    $next = getNext($current);

    while ($next) {

	my ($type,$strand,$id) = $next =~ /^([CB])([\+\-])(.+)$/;

	my $nextID = $type . $id;
	my $rStrand = "-";
	if ($strand eq "-") {
	    $rStrand = "+";
	}

	if (exists($visited{$nextID})) {
	    warn "warning: Inconsistency encountered in reverse walk from $contigID at $nextID.  Truncating.\n";
	    warn "warning: [$type][$strand][$id] [$nextID] [$visited{$nextID}]\n";
	    $next = 0;
	} else {
	    $visited{$nextID} = 1;
	    unshift(@upstream,"$type$rStrand$id");
	    $current = $next;
	    $next = getNext($current);
	}
    }

    # remove leading and trailing bubbles

    my $nUpstream = scalar(@upstream);
    if ($nUpstream > 0) {
	my $first = $upstream[0];
	my ($type,$strand,$id) = $first =~ /^([CB])([\+\-])(.+)$/;
	if ($type eq "B") {
	    shift(@upstream);
	}
    }
    my $nDownstream = scalar(@downstream);
    if ($nDownstream > 0) {
	my $last = $downstream[-1];
	my ($type,$strand,$id) = $last =~ /^([CB])([\+\-])(.+)$/;
	if ($type eq "B") {
	    pop(@downstream);
	}
    }

    my $nTigs = 1 + scalar(@upstream) + scalar(@downstream);

    if ($nTigs > 1) {       # a Contig-Bubble-Contig string


	my $finalSequence = "";
	$diplotigID++;
	my $CBCstring = "diplotig$diplotigID: ";
	my $nContigMers = 0;
	my $weightedContigDepth = 0;
	my $nBubbles = 0; 
	my $nLinkertigs = 0;

	my @haploPaths = ();   # two paths representing separate haplotypes

	my $warningString = "";

	my @tigs = (@upstream,"C+$contigID",@downstream);
	my $idx = 0;
	foreach my $tig (@tigs) {


	    $CBCstring .=  "$tig->";
	    print STDERR "\n$CBCstring\n" if ($verbose);
	    my ($type,$strand,$id) = $tig =~ /^([CB])([\+\-])(.+)$/;

	    if ($type eq "B") {

		# A bubble consists of two bubbletigs. 
		# Extract each bubbletig's id, strand, and depth info
		# Determine each bubbletig's sequence, reversing and clipping as needed

		$nBubbles++;  
		$nCompleteBubbles++;

		my $links = $bubbletigs{$id};
		my $btigs = $tipInfo{$links};
		my @currentBubbleBtigs = split(/\,/,$btigs);

		unless (scalar(@currentBubbleBtigs) == 2) {
		    $warningString .= "warning: Only two-path bubbles are currently supported. ($tig = [@currentBubbleBtigs])\n";
		}
		
		my %btigSeqs = ();
		my %btigDepths = ();

		foreach my $btig (@currentBubbleBtigs) {
		    my ($s,$cid) = $btig =~ /^([\+\-])(.+)$/;

		    labelContig($cid,2, \%contigInfo);
		    
		    my $depth = getContigInfo($cid,9);
		    $btigDepths{$btig} =  sprintf("%.2f", $depth);

		    my $seq = $contigSequences{$cid};
		    if ($s ne $strand) {
			my $rcSeq = reverse($seq);
			$rcSeq =~ tr/ACGT/TGCA/;
			$seq = $rcSeq;
		    }
		    $btigSeqs{$btig} = $seq;
		}
		
		my @sortedBtigs = sort {length($btigSeqs{$a}) <=> length($btigSeqs{$b})} @currentBubbleBtigs;
		my @btigLens = map {length($btigSeqs{$_})} @sortedBtigs;

		my $minLen = $btigLens[0];
		my $minClipLen = $minLen - (2*$merLength - 4);
		my $tailSeq = "";
		my @seqPaths = ();
		my @suspendedBtigs = ();

		if ($minClipLen < 0) {
		    # The smaller of the two btigs is too short for redundant end clipping, so instead we clip $minClipLen bp from the tail of the last C-tig (to be used later)
		    if ($MODE eq "haploPaths") {
			my @lastCtigInfo = split(/\:/,${$haploPaths[0]}[-1]);     #"$id:$type:$seq:$depth:0"
			my $lastCtigSeq = $lastCtigInfo[2];
			$tailSeq = substr( $lastCtigSeq, $minClipLen);
			for (my $n = 0; $n < -$minClipLen; $n++) {
			    chop $lastCtigSeq;
			}
			# update the haplo-paths with the chopped contig info  (same for both since it's a C-tig)
			$lastCtigInfo[2] = $lastCtigSeq;		       
			${$haploPaths[0]}[-1] = join(':',@lastCtigInfo);
			${$haploPaths[1]}[-1] = join(':',@lastCtigInfo);

		    } 
		    else {
			$tailSeq = substr($finalSequence,$minClipLen);
			for (my $n = 0; $n < -$minClipLen; $n++) {
			    chop $finalSequence;
			}
		    }
		}

		my $maxDepthIndex = 0;
		my $maxDepth = 0;
		my $btigIndex = 0;

		foreach my $btig (@sortedBtigs) {

		    print STDERR "\nProcessing bubbletig $btig...\n\n" if ($verbose);
		    my $seq = $btigSeqs{$btig};
		    my $btigLen = length($seq);
#		    print STDERR "DEBUG: btig $btig: $seq\n" if ($verbose);
#		    print STDERR "DEBUG: btigLen: $btigLen\n" if ($verbose);
		    my $clipLen = $btigLen - (2*$merLength - 4);
		    if ($clipLen < 0) {
			#too small to clip; Instead using the saved trimmed sequence from the end of the previous C-tig
			my $diff = $clipLen - $minClipLen;
			my $addBack = substr($tailSeq,0,$diff);
			push(@seqPaths,$addBack);
		    } else {
			my $clipSeq = substr($seq,$merLength-2,$clipLen);
			push(@seqPaths,$tailSeq.$clipSeq);
#			print STDERR "DEBUG: Clipped; btigLen after clipping: $clipLen\n" if ($verbose);
#			print STDERR "DEBUG: The current bubble sequence is ".length($tailSeq)." bp of previous contig's tail, plus the $clipLen bp of this btig\n" if ($verbose);
		    }
		    
		    print STDERR "bubble path: $seqPaths[-1]\n" if ($verbose);


		    my $depth = $btigDepths{$btig};
		    if ($depth >= $maxDepth) {
			$maxDepth = $depth;
			$maxDepthIndex = $btigIndex;
		    }

		    if ($MODE eq "haploPaths") {
			# determine which of the two running haplo-paths this btig belongs to and update the haplo-path

			my ($btigInfoString, $pathIdx);			
			my ($s,$cid) = $btig =~ /^([\+\-])(.+)$/;
			unless ($nBubbles == 1) {  # skip first bubble, i.e. we can't possibly link it to anything
			    ($btigInfoString, $pathIdx) = map_btig_to_haplo_path( $cid, $type, $seqPaths[-1], $depth, \@haploPaths, $contigsClonesHoHR);
			}
			if (defined($pathIdx) && defined($btigInfoString)) {
			    if ( scalar(@{$haploPaths[$pathIdx]}) >= ($nBubbles+$nLinkertigs) ) {  
				# we've already mapped a btig from this bubble to this path, so we need to pull back and resolve the conflict 
				print STDERR "Warning:  this btig maps to the same path as did its bubble-brother! Postponing the path assignment...\n" if ($verbose);
				push(@suspendedBtigs, $btigInfoString  );
			    }
			    else {
				push( @{$haploPaths[$pathIdx]}, $btigInfoString );  # add this btig to the selected path
			    }
			}
			else {
			    # the btig couldn't be assigned to a path by mapped reads so we pospone the assignment 
			    $btigInfoString = "$cid:$type:$seqPaths[-1]:$depth:0";
			    print STDERR "Postponing the path assignments...\n" if ($verbose);			    
			    push(@suspendedBtigs, $btigInfoString );
			}
		    }
		    $btigIndex++;
		}

		if ($MODE eq "haploPaths") {
		    
		    if (scalar @suspendedBtigs == 1)  {
		    # one of the btigs wasn't mapped to a haplo-path, but the other was, so we assign the suspended one to the still vacant path
			my $vacantPathIndex = ( $#{$haploPaths[0]} > $#{$haploPaths[1]} ? 1 : 0 );
			print STDERR "\nAssigned suspended btig to vacant path $vacantPathIndex\n" if ($verbose);
			push( @{$haploPaths[$vacantPathIndex]}, $suspendedBtigs[0] ); 
		    }
		    elsif ((scalar @suspendedBtigs == 2  ) && $nBubbles == 1) {
			# first bubble,so we make an arbitrary path assignment
			print STDERR "\nAssigned suspended btigs @suspendedBtigs to paths based on majority mer depth.\n"  if ($verbose);
			push( @{$haploPaths[0]}, $suspendedBtigs[$maxDepthIndex]);
			push( @{$haploPaths[1]}, $suspendedBtigs[(abs($maxDepthIndex-1))]);
		    }
		    elsif ((scalar @suspendedBtigs == 2 ) && $nBubbles > 1) {
			print STDERR "\nCould not make a linkage-reinforced extension of the haplo-paths.  Terminating diplotig$diplotigID\n"  if ($verbose);
			$nBubbles--;
			terminate_diplotig(\@tigs, $idx, \@sortedBtigs, \%contigInfo); 
			if ($idx < $#tigs) {
			    # if this is not the end of the chain, print current diplotig sequences, then reset everything and start a new diplotig			    
			    print_paths(\@haploPaths, $nLinkertigs, $nBubbles);
			    $diplotigID++;
			    $CBCstring = "diplotig$diplotigID: ";
			    $nContigMers = 0;
			    $weightedContigDepth = 0;
			    $nBubbles = 0;
			    $nLinkertigs = 0;
			    @haploPaths = ();
			    @currentBubbleBtigs = ();
			    $idx++;
			    next;
			}
		    }
		    
		  

		} elsif ($MODE eq "maxLength") {
		    
		    $finalSequence .= $seqPaths[-1];

		} elsif ($MODE eq "minLength") {

		    $finalSequence .= $seqPaths[0];

		} elsif ($MODE eq "maxDepth") {
		    
		    $finalSequence .= $seqPaths[$maxDepthIndex];
		}
	    # end of  ($type eq B) {

	    } elsif ($type eq "C") {

		print STDERR "\nProcessing linkertig $id...\n\n" if ($verbose);

		$nLinkertigs++;
		labelContig($id, 2, \%contigInfo);

		my $depth = getContigInfo($id,9);
		$depth =  sprintf("%.2f", $depth);
		my $nMers = getContigInfo($id,8);
		$nContigMers += $nMers;
		$weightedContigDepth += $nMers*$depth;
		
		my $seq = $contigSequences{$id};
		if ($strand eq "-") {
		    my $rcSeq = reverse($seq);
		    $rcSeq =~ tr/ACGT/TGCA/;
		    $seq = $rcSeq;
		}
      		print STDERR "C-tig $id: $seq\n" if ($verbose);

		if ($MODE eq "haploPaths") {

		    # We mask all C-tig sequences to avoid ambiguous read mapping down the line. 
		    # We leave the first and last (k-1)/2 bases unmasked. This will make sure that the bubbletig will have at least a k-sized stretch of unique sequence while the C-tig won't
		    my $maskLen = length($seq) - ($merLength-1);
		    substr($seq,(($merLength-1)/2), $maskLen) =~ tr/ACGT/acgt/; 
		    # add the contig sequence to BOTH haplo-paths without contributing the depth info since we only count bubbletig depths in this scenario
		    push(@{$haploPaths[0]}, "$id:$type:$seq:0:0" );
		    push(@{$haploPaths[1]}, "$id:$type:$seq:0:0" );
		}
		else {
		    $finalSequence .= "$seq";
		}
	    }
	    $idx++;
	    
	}
	if ($MODE eq "haploPaths") {
	# print BOTH alignment-supported haplo-paths
	    unless (scalar @{$haploPaths[0]} == scalar @{$haploPaths[1]}) { 
		die "Invalid haplo-paths:  different number of contigs!\n  @{$haploPaths[0]}\n  @{$haploPaths[1]}\n";
	    }
	    print_paths(\@haploPaths, $nLinkertigs, $nBubbles);
	}
	else	{
	    # print the "composite" diplotig info  (here we use contig depths only and ignore individual bubbletigs)
	    my $name = "diplotig$diplotigID";
	    my $length = length($finalSequence);
	    print LIST "$name\t$length\t$nLinkertigs\t$nBubbles\t".($weightedContigDepth/$nContigMers)."\n";
	    printFasta($name,$finalSequence);
	}

	if ($warningString) {
	    warn $warningString;
	}
    }
}

my $isotigID = 0;
my $nContigsTotal = 0;
foreach my $cid (keys(%contigInfo)) {

    my $contigStatus = getContigInfo($cid,7);
    if ($contigStatus == 1) {
	$isotigID++;
	my $seq = $contigSequences{$cid};
	my $name = "isotig$isotigID";
	my $length = length($seq);
	my $depth = getContigInfo($cid,9);	
	print LIST "$name\t$length\t1\t0\t$depth\t$cid\n";
	printFasta($name,$seq);
    }
    if ($contigStatus != 0) {
	$nContigsTotal++;
    }
}
close LIST;
if (! $isotigID) { die "No contigs matched minimum length requirements \n"}

print STDERR "Done.\n\n\n";

print STDERR "Total contigs over length cutoff: $nContigsTotal\n";
print STDERR "Total bubbles: ".(scalar keys %bubbleMap)."\n";
print STDERR "Complete bubbles (non-bubble uutig on both ends) : $nCompleteBubbles \n";
print STDERR "Total diplotigs: $diplotigID\n";
print STDERR "Total isotigs: $isotigID\n";
if ($MODE eq "haploPaths") {
    print STDERR "Total blessed paths: $nBlessedPaths \n";
}

print STDERR "Done.\n";

# utilities

sub getContigInfo {
    my ($contig, $field) = @_;

    my $contigInfo = $contigInfo{$contig};
    my @contigInfo = split(/:/,$contigInfo);

    my $nFields = scalar(@contigInfo);
    unless ($field < $nFields) {
	die "getContigInfo ( $contig , $field ) failed.  Not enough fields in $contigInfo.\n";
    }

    my $info = $contigInfo[$field];
    return $info;
}


sub labelContig {
    my ($contig,$label,$contigInfoHR) = @_;

    my $contigInfo = $$contigInfoHR{$contig};
    my @contigInfo = split(/:/,$contigInfo);
    $contigInfo[7] = $label;
    $contigInfo = join(":",@contigInfo);
    $$contigInfoHR{$contig} = $contigInfo;    
}


sub getNext {
    my ($tigID) = @_;

    my ($type,$strand,$id) = $tigID =~ /^([BC])([\+\-])(.+)$/;

    my $links = "";
    if ($type eq "C") {
	unless (exists($linkertigs{$id})) {
	    die "LINKERTIGS NOT DEFINED for [$id]\n";
	}
	$links = $linkertigs{$id};
    } else {
	unless (exists($bubbletigs{$id})) {
	    die "BUBBLETIGS NOT DEFINED for [$id]\n";
	}

	$links = $bubbletigs{$id};
    }
    my ($in,$out) = $links =~ /^(.+)\.(.+)$/;
    my $rcLinks = reverse($links);
    $rcLinks =~ tr/ACGT/TGCA/;
    my ($rcin,$rcout) = $rcLinks =~ /^(.+)\.(.+)$/;

    my $next = 0;
    if ($strand eq "-") {
	if (($rcout) && (exists($pointsTo{$rcout}))) {
	    $next = $pointsTo{$rcout};
	}
    } else {
	if (($out) && exists($pointsTo{$out})) {
	    $next = $pointsTo{$out};
	}
    }

    my @next = split(/\,/,$next);
    if ($#next > 0) {
	warn "warning: getNext finds multiple connections for $tigID (@next). Terminating.\n";
	$next = 0;
    }

    return $next;

}

sub printFasta {
    my ($name,$seq) = @_;
    my $seqLen = length($seq);

    my $bpl = 50;

    my $nLines = sprintf("%d",$seqLen/$bpl);
    if ($seqLen%$bpl != 0) {
        $nLines++;
    }

    print ">$name\n";
    for (my $j = 0;$j<$nLines;$j++) {
        my $seqLine = substr($seq,$j*$bpl,$bpl);
        print "$seqLine\n";
    }
}

sub map_btig_to_haplo_path {

    my ($cid, $type, $seq, $depth, $haploPathsAR, $contigsClonesHoHR) = @_;

    # @haploPaths is an array of TWO arrays in which the elements are strings of type "$contig:$type:$seq"
    # 
    my $linked = 0;	
    my $pathIdx;
    my $infoString = "";
    my @clonesMappedHere = keys %{ $contigsClonesHoHR->{$cid}};

    for (my $i=0; $i<=1; $i++)    {
	my @path = @{ $$haploPathsAR[$i] };
	my $nSupportLinkage = 0;
        my $minSupport = 2;
	my $pathLinksHR = is_linked_to_path( \@path, $contigsClonesHoHR, \@clonesMappedHere, $minSupport);
	if ( keys %$pathLinksHR ) {		
	    $nSupportLinkage = sum values %$pathLinksHR;
	    print STDERR "$cid is linked to haplo-path $i by $nSupportLinkage mapped clones\n" if ($verbose);	    

	    my @linkedToBtigs;
	    foreach my $bt (keys %$pathLinksHR) {
		if ($$pathLinksHR{$bt} > 0) {
		    push(@linkedToBtigs, $bt);
		}
	    }
	    $infoString = "$cid:$type:$seq:$depth:$nSupportLinkage:".(join(",", @linkedToBtigs));	    
	    $pathIdx = $i;
	    $linked++;
	}	    
    }
    if ($linked == 0)	{
	print STDERR "Couldn't link $cid (".length($seq)." bp, ".scalar(@clonesMappedHere)." clones)  to either path!\n " if ($verbose);
	return undef;
    }
    elsif ($linked > 1)	{
	print STDERR "$cid fits both paths ..\n" if ($verbose);
	return undef;
    }
    else {
	return ($infoString, $pathIdx);
    }
}


#
# Use mapping info to determine if contig is linked to the given path and check for disbalance of contig mer depths 
# btwn the contig and the rest of the path (indicates seq error)
# 
sub is_linked_to_path {
    my ($pathAR, $contigsClonesHoHR, $clonesMappedHereAR, $min) = @_;

#    print STDERR "DEBUG: clones mapped to this btig: \n@$clonesMappedHereAR\n";

    my %pathLinks;
    # initiale a list of btigs in the path, i.e. we can potentially link to them
    foreach my $c (@$pathAR) {
	my @a = split(/:/,$c);
        my $cid = $a[0];
	my $type = $a[1];
	if ($type eq "B") {	
	    $pathLinks{$cid} = 0;
	}
    }

    foreach my $btig (keys %pathLinks) {
    # check if clone is mapped to the btig that's already in the path AND to the current btig
	foreach my $clone (@$clonesMappedHereAR ){
	    if (exists $contigsClonesHoHR->{$btig}{$clone}) {
		$pathLinks{$btig}++;
	    }
	}
    }


    unless (%pathLinks) { return undef }
    if ( (sum values %pathLinks) >= $min ) { 	
	return \%pathLinks;
    }
    else {
	return undef;
    }
}


#
# Find reads and clones mapping to a bubbletig
#
sub merblast_reads {
    my ($contigsFile) = @_;

    my $mapFilePref = "bubble_UUtigs.blastMap";
    my $merblast = $merLength <= 56 ? $ENV{'MERACULOUS_ROOT'}."/".PATH_MERBLAST_56 : $ENV{'MERACULOUS_ROOT'}."/".PATH_MERBLAST_112;
    my %contigsClonesHOH;
    my $nContigs = `grep -c '>' $contigsFile`;
    chomp $nContigs;
    my $cmd = "$merblast $contigsFile $merLength $fastqInfoFile $mapFilePref $threads $nContigs -5 0 2> merblast.err";
    system($cmd);
    if ($?) {die "system command failed with status $? ($cmd) "}     

    open(MAP,"cat $mapFilePref*[0-9] |") || die "Can't open blast map files for reading";
    while (my $line = <MAP>) {
	my @fields = split(/\t/,$line);
	my ($r, $c) = ($fields[2], $fields[6]);
	my (undef,$clone,$end) = parse_fastq_header($r);

	# Since the contigs are uutigs, any read that mapped to a contig must therefore include at least one btig-specific position
	$contigsClonesHOH{$c}{$clone} = 1;
    }
    close MAP;
    return \%contigsClonesHOH;
}


#
# Use contig mer depths to determine the "dominant" haplo-path    !! MOSTLY DISABLED SINCE WE WANT BOTH PATHS
#
sub bless_haplopaths {
    my $haploPathsAR = shift;   # a reference to an array of refs to arrays
    my $blessedHaploSeq;
    my @haploSeqs;
    my @pathNMers;
    my @pathMerDepthSum;
    my @pathMerDepthAvg;

    my  $i=0;
    foreach my $pathRef (@$haploPathsAR) {
	my $pathNMers = 0;
	my $pathLength = 0;
	foreach my $c (@$pathRef) {
	    my @a = split(/:/,$c);
	    my ($cid, $type, $seq, $depth) = @a[0..3];
	    my $nMers = getContigInfo($cid,8);
	    $haploSeqs[$i] .= $seq;
	    if ($type eq 'B') {  # we only consider btigs in the overall depth calculation
		$pathNMers[$i] += $nMers;
		$pathMerDepthSum[$i] += ($depth * $nMers);   
	    }
	}
	$pathMerDepthAvg[$i] = $pathMerDepthSum[$i]/$pathNMers[$i];
	$i++;
    }	    
    return ($haploSeqs[0], $haploSeqs[1], $pathMerDepthAvg[0], $pathMerDepthAvg[1]);
}


#
# Tie up loose ends when the diplotig chain can't be continued past the current bubble
#
sub terminate_diplotig {
    my ($tigsAR, $idx, $currBtigsAR, $contigInfoHR) = @_;

    # re-label the current pair of btigs back to "isotigs"
    foreach my $btig (@$currBtigsAR) {
	my ($s,$cid) = $btig =~ /^([\+\-])(.+)$/;
	labelContig($cid,1,$contigInfoHR);
    }
    print STDERR "Terminated the C-B-C chain of ".(scalar(@$tigsAR))." at element $idx. \n" if ($verbose);
    if ( $idx == (($#$tigsAR) -1 )) {
	# if there're no more bubbles left in the chain, cut off the next C-tig so that the chain ends on the last C-tig we processed
	pop(@$tigsAR);
    }
}


sub print_paths {
    my $haploPathsAR = shift;
    my $nLinkertigs = shift;
    my $nBubbles = shift;

    my ($haploSeq, $haploSeqAlt, $pathMerDepthAvg, $pathMerDepthAvgAlt) = bless_haplopaths($haploPathsAR); 
    $nBlessedPaths++; $nBlessedPaths++;
    
    # mask the first and last k-2 bases of both final sequences  to avoid ambiguous read mapping down the line	    
    substr($haploSeq,-(($merLength-1)/2) ) =~ tr/ACGT/acgt/;
    substr($haploSeq,0,(($merLength-1)/2) ) =~ tr/ACGT/acgt/;
    
    substr($haploSeqAlt,-(($merLength-1)/2) ) =~ tr/ACGT/acgt/;
    substr($haploSeqAlt,0,(($merLength-1)/2) ) =~ tr/ACGT/acgt/;
    
    
    # print the individual diplotig path info
    my $name = "diplotig${diplotigID}_p1";
    my $length = length($haploSeq);
    print LIST "$name\t$length\t$nLinkertigs\t$nBubbles\t$pathMerDepthAvg\n";
    printFasta($name,$haploSeq);
    
    $name = "diplotig${diplotigID}_p2";
    $length = length($haploSeqAlt);
    print LIST "$name\t$length\t$nLinkertigs\t$nBubbles\t$pathMerDepthAvgAlt\n";
    printFasta($name,$haploSeqAlt);
}


sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}

