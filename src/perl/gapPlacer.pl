#! /usr/bin/env perl
#
# gapPlacer.pl by Jarrod Chapman <jchapman@lbl.gov> Fri Jun 12 12:58:11 PDT 2009
# Copyright 2009 Jarrod Chapman. All rights reserved.
#

use strict;
use warnings;

use lib "$ENV{MERACULOUS_ROOT}/lib";
use M_Utility;
use Getopt::Std;

my %opts = ();
my $validLine = getopts('b:m:i:s:f:c:AGPF:T:X:R', \%opts);
my @required = ("b","m","i","s","f","c");
my $nRequired = 0;
map {$nRequired += exists($opts{$_})} @required;
$validLine &= ($nRequired == @required);
if ($validLine != 1) {
    print STDERR "Usage: ./gapPlacer.pl <-b blastMapGlob> <-m merSize> <-i insertSize:sigma> <-s scaffoldReportFile> <-f fastqFileList> <-c contigFastaFile> <<-X avoidContigsList>> <<-G(zipped?)>> <<-F fivePrimeWiggleRoom>> <<-T threePrimeWiggleRoom>> <<-P(noPairProjection)>> <<-A(AggregateVariants)>>\n";
    exit;
}

while (my ($opt,$val) = each(%opts)) {
    print STDERR " -$opt=$val";
}
print STDERR "\n";

my $blastMapFileGlob = $opts{"b"};
my @blastMapFiles = glob($blastMapFileGlob);

my $merSize = $opts{"m"};

my ($insertSize,$insertSigma) = $opts{"i"} =~ /^(\d+)\:(\d+)$/;
my $projectZ = 2;
my $maxProject = $insertSize+$projectZ*$insertSigma;
my $minProject = $insertSize-$projectZ*$insertSigma;

my $srfFile = $opts{"s"};
my $contigFastaFile = $opts{"c"};
my $fastqFileList = $opts{"f"};

my $gzipped = 0;
if (exists($opts{"G"})) {
    $gzipped = 1;
}

my $pairProjection = 1;
if (exists($opts{"P"})) {
    $pairProjection = 0;
}

my $fivePrimeWiggleRoom = 5;
if (exists($opts{"F"})) {
    $fivePrimeWiggleRoom = $opts{"F"};
}
my $threePrimeWiggleRoom = 5;
if (exists($opts{"T"})) {
    $threePrimeWiggleRoom = $opts{"T"};
}

my $reverseComplement = 0;
if (exists($opts{"R"})) {
    $reverseComplement = 1;
}

my $truncate = 0;
if (exists($opts{"U"})) {
    $truncate = $opts{"U"};
}

my $avoidContigsFile;
my %contigsToAvoid = ();  # read alignments to these contigs will be skipped;  any gaps around these contigs can still be closed from the other side
if (exists($opts{"X"})) {
    $avoidContigsFile = $opts{"X"};
}


my $aggregateVariants = 0;
if (exists($opts{"A"})) { #treat alignments to alternative diplotig pairs (diplotigX_p1 & diplotigX_p2) as mutually applicable;  the fragile assumption is that one of the alt variants is a singleton scaffold!
    $aggregateVariants = 1;
}


my %rejected = ();
my @rejectReasons = ("IN_AVOID_LIST","FORMAT","MINLEN","UNINFORMATIVE","5-TRUNCATED","3-TRUNCATED","SINGLETON","NO_ASSOCIATED_GAP",
		     "TRUNC5_FAIL","TRUNC3_FAIL", "NO_SCAFFOLD_INFO", "POTENTIAL_INNIE", "POTENTIAL_SHORTY");
foreach my $r (@rejectReasons) {
    $rejected{$r} = 0;
}

my @gapInfo = ();  # gaps indexed along srf file [scaff, prevContigEnd, nextContigEnd, scaffStart:scaffEnd:uncertainty, (read1, read2, ...)]
my %contigGapMap = ();  # map from contig to flanking gap indices [5'-gapID,3'-gapID]
my %readData = ();  # read name -> sequence:quality
my %scaffInfo = (); # scaffName -> [nContigs,scaffLen]


if ($avoidContigsFile) {
    open (X,$avoidContigsFile) || die "Couldn't open $avoidContigsFile\n";
    while (my $line = <X>) {
	chomp $line;
	$contigsToAvoid{$line}=1;
    }
    close X;
}


print STDERR "Reading scaffold report file: $srfFile...\n";
open (S,$srfFile) || die "Couldn't open $srfFile\n";
my $gapIndex = 0;
my $currentScaff = "NULL";
my $prevContigEnd = undef;
my $scaffCoord = 0;
my $openGap = 0;
while (my $line = <S>) {
    chomp $line;
    my @cols = split(/\t/,$line);
    my $scaffID = $cols[0];
    
    unless (exists($scaffInfo{$scaffID})) {
	$scaffInfo{$scaffID} = [0,0];
    }

    if ($cols[1] =~ /^CONTIG/) {   # a contig
 	my ($contigID,$sStart,$sEnd) = @cols[2,3,4];
	my ($cStrand,$cName) = $contigID =~ /^([\+\-])(.+)$/;

	$prevContigEnd = ($cStrand eq "+") ? "$cName.3" : "$cName.5";
	$scaffCoord = $sEnd;
	$scaffInfo{$scaffID}->[1] = $sEnd;
	$scaffInfo{$scaffID}->[0]++;
	$contigGapMap{$cName} = [undef,undef];   # place holder for flanking gap ids

	if ($openGap) {
	    if ($cStrand eq "+") {
		$contigGapMap{$cName}->[0] = $gapIndex-1;
		$gapInfo[$gapIndex-1]->[2] = "$cName.5";
	    } else {
		$contigGapMap{$cName}->[1] = $gapIndex-1;
		$gapInfo[$gapIndex-1]->[2] = "$cName.3";
	    }	    
	}
	$openGap = 0;

    } else {  # a gap
 	my ($gapSize,$uncertainty) = @cols[2,3];
	my $nextScaffCoord = $scaffCoord+$gapSize+1;
	$gapInfo[$gapIndex] = [$scaffID,$prevContigEnd,"?","$scaffCoord:$nextScaffCoord:$uncertainty"];  # '?' is a place holder for the downstream contig flanking this gap

	my ($cName,$cEnd) = $prevContigEnd =~ /^(.+)\.([35])$/;
	if ($cEnd eq "5") {
	    $contigGapMap{$cName}->[0] = $gapIndex;
	} else {
	    $contigGapMap{$cName}->[1] = $gapIndex;
	}

	$gapIndex++;
	$openGap = 1;
    }
			       
}
close S;

if (keys %contigsToAvoid >= keys %contigGapMap) {
    die "Error:  Number of contigs found in the srf file must be greater than the number of contigs in the exclusion list!\n";
}

print STDERR "Done.\n";
my $totalGaps = $gapIndex;
my $alignedGapReads = 0;
my $projectedGapReads = 0;


foreach my $blastMapFile (@blastMapFiles) {

    print STDERR "Reading blast map file: $blastMapFile...\n";
    open (B,$blastMapFile) || die "Couldn't open $blastMapFile\n";

    my $currentPair = undef;
    my $currentPairAlignments = 0;
    my $pairInfo = "";
    my %readInfo = ();  # readName -> [namingConvention,cloneName,end,index];  
    
    while (my $line = <B>) {
	chomp $line;	
	my ($blastType,$query,$qStart,$qStop,$qLength,$subject,$sStart,$sStop,$sLength,
	    $strand,$score,$eValue,$identities,$alignLength) = split(/\t/,$line);

        next if ($blastType eq "BLAST_TYPE");


#	if ($blastType ne "BLASTN") {
#	    $rejected{"FORMAT"}++;
#	    next;
#	}
#	if ($identities < $merSize) {
#	    $rejected{"MINLEN"}++;
#	    next;
#	}


	# If there's no gap associated with the contig, skip the alignment 
	unless (defined($contigGapMap{$subject}->[0]) || defined($contigGapMap{$subject}->[1])) { 
	    if ($aggregateVariants) { # if the alignment can be useful with the alt variant, don't throw it out. Assign it to the variant instead. 
		my $subjectAltV;
		if ($subject =~/^diplotig(\d+)_p([12])/) {
		    $subjectAltV = "diplotig".$1."_p".($2 == 1 ? 2 : 1);
		    unless ( defined($contigGapMap{$subjectAltV}->[0]) || defined($contigGapMap{$subjectAltV}->[1])) {  
			$rejected{"NO_ASSOCIATED_GAP"}++;
			next;
		    }
		    $subject = $subjectAltV; 
		}
	    }
	    else {
		$rejected{"NO_ASSOCIATED_GAP"}++;
		next;
	    }
	}


	if ( $avoidContigsFile ) {
	    if (exists $contigsToAvoid{$subject}) {
		$rejected{"IN_AVOID_LIST"}++;
		next;
	    }
	}
	    


	# Assess alignment for completeness (do this before scaffold coordinate conversion!)
	# Important: Truncations are performed before reverse complementation
	# and apply to the end of the actual read
	my $unalignedStart = $qStart-1;
	my $projectedStart = undef;
	if ($strand eq "Plus") {
	    $projectedStart = $sStart - $unalignedStart;
	} else {
	    $projectedStart = $sStop + $unalignedStart;
	}
	

	my $startStatus = undef;
	
	my $projectedOff = 0;
	if ($projectedStart < 1) {
	    $projectedOff = 1-$projectedStart;
	} elsif ($projectedStart > $sLength) {
	    $projectedOff = $projectedStart-$sLength;
	}
	my $missingStartBases = $unalignedStart-$projectedOff;
	
	# classify this alignment

	if ($unalignedStart == 0) {
	    $startStatus = "FUL";
	} elsif ( ($projectedOff > 0) && ($missingStartBases < $fivePrimeWiggleRoom) ) {
	    $startStatus = "GAP";
	} elsif (($unalignedStart < $fivePrimeWiggleRoom) || ($truncate == 5)) {
	    $startStatus = "INC";
	} else {
	    $rejected{"5-TRUNCATED"}++;
	    next;
	}

	my $unalignedEnd = $qLength-$qStop;
	my $projectedEnd = undef;
	if ($strand eq "Plus") {
	    $projectedEnd = $sStop + $unalignedEnd;
	} else {
	    $projectedEnd = $sStart - $unalignedEnd;
	}
	
	my $endStatus = undef;
	
	$projectedOff = 0;
	if ($projectedEnd < 1) {
	    $projectedOff = 1-$projectedEnd;
	} elsif ($projectedEnd > $sLength) {
	    $projectedOff = $projectedEnd-$sLength;
	}
	my $missingEndBases = $unalignedEnd-$projectedOff;
	
	if ($unalignedEnd == 0) {
	    $endStatus = "FUL";
	} elsif ( ($projectedOff > 0) && ($missingEndBases < $threePrimeWiggleRoom) ) {
	    $endStatus = "GAP";
	} elsif (($unalignedEnd < $threePrimeWiggleRoom) || ($truncate == 3)) {
	    $endStatus = "INC";
	} else {
	    $rejected{"3-TRUNCATED"}++;
	    next;
	}

	my $readStatus = "$startStatus.$endStatus";

	# Re-orient alignment if requested
	if ($reverseComplement) {
	    $strand = ($strand eq "Plus") ? "Minus" : "Plus";
	    my $qStartOrig = $qStart;
	    $qStart = $qLength-$qStop+1;
	    $qStop = $qLength-$qStartOrig+1;
	    $readStatus = "$endStatus.$startStatus";
	}

	# Form the alignment string
	my $alnStr = join("\t",$blastType,$query,$qStart,$qStop,$qLength,
			 $subject,$sStart,$sStop,$sLength,$strand,$score,$eValue,$identities,$alignLength);


	# Parse pair name and end from the query line 
	my ($format,$pairName,$pairEnd,$index) = M_Utility::parse_fastq_header($query);
        unless (defined($pairName) && defined($pairEnd))
        { die "Error: couldn't extract pairing info from the read name:  $query\n" }

	@{$readInfo{$query}} = ($format,$pairName,$pairEnd,$index);

	# Determine this read's placement wrt gaps

	if (defined($currentPair)) {
	    if ($pairName eq $currentPair) {
		$pairInfo .= "$readStatus\t$alnStr\n";
		$currentPairAlignments++;
	    } else {
		my $result = processPair($pairInfo, \%readInfo); 
		my ($nAligned,$nProjected) = $result =~ /^(\d+):(\d+)$/;
		$alignedGapReads += $nAligned;
		$projectedGapReads += $nProjected;
		
		$currentPair = $pairName;
		$pairInfo = "$readStatus\t$alnStr\n";
		$currentPairAlignments = 1;
	    }
	} else {
	    $currentPair = $pairName;
	    $pairInfo = "$readStatus\t$alnStr\n";
	    $currentPairAlignments = 1;
	}
    }
    close B;
    print STDERR "Done.\n";

    if (defined($currentPair)) {
	my $result = processPair($pairInfo, \%readInfo);
	my ($nAligned,$nProjected) = $result =~ /^(\d+):(\d+)$/;
	$alignedGapReads += $nAligned;
	$projectedGapReads += $nProjected;
    }
}

print STDERR "Unused alignments:\n";
foreach my $r (@rejectReasons) {
    my $n = $rejected{$r};
    if ($n) {
	print STDERR "\t$r\t$n\n";
    }
}
print STDERR "Total reads placed in gaps = $alignedGapReads (aligned) + $projectedGapReads (projected)\n";


open (FOF,"<$fastqFileList") || die "Couldn't open $fastqFileList\n";
while (<FOF>)
{
    my @FOFline = split /\s+/, $_;
    my $seqFile = $FOFline[0];
    print STDERR "Reading sequence file $seqFile...\n";

    if ($gzipped) {
        open (S,"gunzip -c $seqFile |") || die "Couldn't open pipe to $seqFile\n";
    } else {
        open (S,$seqFile) || die "Couldn't open $seqFile\n";
    }

    while (my $line = <S>) {
        my $readName = undef;
        my $nts = undef;
        my $quals = undef;

	chomp $line;
	unless ($line =~ s/^@//) {
	    die "Invalid fastQ $line\n";
	}
	$readName = $line;

	$line = <S>;
	chomp $line;
	$nts = $line;

	$line = <S>;
	unless ($line =~ /^\+/) {
	    die "Invalid fastQ $line\n";
	}
	$line = <S>;
	chomp $line;
	$quals = $line;

	if (exists($readData{$readName})) {
#	    unless ( length($nts) >= $merSize ) {
#		print STDERR "Warning: Read $readName is shorter than the chosen kmer size! Will not use this read for gap closure. \n";
#		delete $readData{$readName};
#		next;
#	    }
	    my $orient = $readData{$readName};
#	    if ($orient eq "-" ) {

	    # Reverse-complement gap-filling sequence where needed.
	    # (if we've artificially reversed the alignments we need to reverse the sequences where the alignment is "positive")
	    my $needToRevCompSeq = ($reverseComplement && ($orient eq "+") ||  (! $reverseComplement && ($orient eq "-")) ) ? 1 : 0;
	    if ($needToRevCompSeq) {
		$nts = reverse($nts);
                $nts =~ tr/ACGTacgt/TGCAtgca/;
                $quals = reverse($quals);
	    }
	    $readData{$readName} = "$nts:$quals";
	}
    }
    close S;
    print STDERR "Done.\n";
}
close FOF;



# Read in contig sequence

print STDERR "Reading $contigFastaFile...\n";

my %contigSequence = ();
my $currentEntry = undef;
my $seq = "";
open (F,$contigFastaFile) || die "Couldn't open $contigFastaFile\n";
while (my $line = <F>) {
    chomp $line;
    if ($line =~ /^>/) {
        if (defined($currentEntry)) {
	    $contigSequence{$currentEntry} = $seq;
	}
        $seq = "";
        ($currentEntry) = $line =~ /^>(.+)$/;
    } else {
        $seq .= $line;
    }
}
close F;

if (defined($currentEntry)) {
    $contigSequence{$currentEntry} = $seq;
}
print STDERR "Done.\n";

for (my $g=0;$g<$totalGaps;$g++) {
    my ($scaff,$prevContigEnd,$nextContigEnd,$coords,@reads) = @{$gapInfo[$g]};

    my $nReads = scalar(@reads);
    print STDERR "gap$g [$scaff:$prevContigEnd<-->$nextContigEnd]: $nReads reads assigned.\n";

    my ($prevContig,$prevEnd) = $prevContigEnd =~ /^(.+)\.([35])$/;
    unless (exists($contigSequence{$prevContig})) {
	die "Sequence not found for contig $prevContig\n";
    }
    my $prevSeq = $contigSequence{$prevContig};
    my $primer1 = ($prevEnd eq "5") ? substr($prevSeq,0,$merSize) : substr($prevSeq,-$merSize);
    if ($prevEnd eq "5") {
	$primer1 = reverse($primer1);
	$primer1 =~ tr/ACGTacgt/TGCAtgca/;
    }

    my ($nextContig,$nextEnd) = $nextContigEnd =~ /^(.+)\.([35])$/;
    unless (exists($contigSequence{$nextContig})) {
	die "Sequence not found for contig $nextContig\n";
    }
    my $nextSeq = $contigSequence{$nextContig};
    my $primer2 = ($nextEnd eq "5") ? substr($nextSeq,0,$merSize) : substr($nextSeq,-$merSize);
    if ($nextEnd eq "3") {
	$primer2 = reverse($primer2);
	$primer2 =~ tr/ACGTacgt/TGCAtgca/;
    }

    my ($leftScaffCoord,$rightScaffCoord,$uncertainty) = split(/:/,$coords);
    my $gapSize = $rightScaffCoord-$leftScaffCoord-1;
    print "$scaff\t$prevContigEnd\t$primer1\t$nextContigEnd\t$primer2\t$gapSize\t$uncertainty";
    foreach my $r (@reads) {
	if ( $readData{$r} eq '+' || $readData{$r} eq '-' || ! exists($readData{$r}) ) {   
	    # if not all reads have pairs, we may have expected a projection that doesn't exist, so we omit those here.
	    warn "Warning: no valid sequence found for read $r. Omitting.\n";
	    next;
	}
	print "\t$readData{$r}";
    }
    print "\n";
}


print STDERR "Done. \n";

# -------------
# |SUBROUTINES|
# -------------

sub processPair {
    my ($alnInfo, $readInfoHR) = @_;
    my @alignments = split(/\n/,$alnInfo);
    my $nAligns = scalar(@alignments);

    # Alignment fields:
    # 0=readStatus,1=BLASTN,2=$query,3=$qStart,4=$qStop,5=$qLength,6=$subject,7=$sStart,8=$sStop,9=$sLength,10=$strand

    my $nPlaced = 0;
    my $nProjected = 0;

    my %readGapPairs = ();
    my %readsToAlignments = ();

    my %orientationMap = ();
    my @combos = ("LPlus5","LPlus3","LMinus5","LMinus3",
		  "RPlus5","RPlus3","RMinus5","RMinus3");

    # e.g. "LPlus5" means the read is aligned on the Plus strand to the contig to the Left of the gap
    # whose 5' end is adjacent to the gap

    my @orients = ("-","+","+","-",
		   "+","-","-","+");
    @orientationMap{@combos} = @orients;

    my $maxAlignedScaffLength = 0;

    for (my $a = 0; $a < $nAligns; $a++) {

	my @alignInfo = split(/\t/,$alignments[$a]);
	my ($read5status,$read3status) = $alignInfo[0] =~ /^(...)\.(...)$/;
	my ($readName,$contigName,$strand) = @alignInfo[2,6,10];

	# If there's no gap associated with the contig, skip it
	unless (defined($contigGapMap{$contigName}->[0]) || defined($contigGapMap{$contigName}->[1])) {
	    next;
	}
    
	# If the read aligns into a gap, add a read<->gap pair
	if ($read5status eq "GAP") {
	    my $gapID = ($strand eq "Plus") ? $contigGapMap{$contigName}->[0] : $contigGapMap{$contigName}->[1];
	    if (defined($gapID)) {
		$readGapPairs{"$readName:$gapID"} = "$strand:$contigName";
#		print STDERR "DEBUG: 5'Read => Gap assignment: $readName:$read5status:$strand => $contigName:$gapID\n";
	    }
	}
	if ($read3status eq "GAP") {
	    my $gapID = ($strand eq "Plus") ? $contigGapMap{$contigName}->[1] : $contigGapMap{$contigName}->[0];
	    if (defined($gapID)) {
		$readGapPairs{"$readName:$gapID"} = "$strand:$contigName";
#		print STDERR "DEBUG: 3'Read => Gap assignment: $readName:$read3status:$strand => $contigName:$gapID\n";
	    }
	}
	       

	# one alignment is recorded for each read to be used to project the read's mate if needed
	# the alignment to the largest scaffold is retained
	# each read is assigned to a single contig/scaffold for this purpose
	# mate pairs may only be projected into gap(s) on a single scaffold by this construction
	# (although direct read alignments may place reads in gaps on multiple scaffolds)

	my $gapID = ($strand eq "Plus") ? $contigGapMap{$contigName}->[1] : $contigGapMap{$contigName}->[0];
	if (defined($gapID)) {
	    my $scaffID = $gapInfo[$gapID]->[0];
	    my $scaffLength = $scaffInfo{$scaffID}->[1];
	    if (exists $readsToAlignments{"$readName"})  {
		if ($scaffLength > $maxAlignedScaffLength) {
		    $readsToAlignments{"$readName"} = $a;
		    $maxAlignedScaffLength = $scaffLength;
		}
	    }
	    else {
		$readsToAlignments{"$readName"} = $a;
	    }		
	}
    }


    # For reads aligning directly into gaps, add read to the corresponding gap's info array and record its orientation wrt the gap
    while (my ($rg,$sc) = each(%readGapPairs)) {

	my ($r,$g) = $rg =~ /^(.+):(\d+)$/;
	my ($s,$c) = $sc =~ /^(.+):(.+)$/;

	my $leftContig = $gapInfo[$g]->[1];
	my $rightContig = $gapInfo[$g]->[2];
	my ($lName,$lEnd) = $leftContig =~ /^(.+)\.([35])$/;
	my ($rName,$rEnd) = $rightContig =~ /^(.+)\.([35])$/;
	my $side = undef;
	my $end = undef;
	if ($c eq $lName) {
    $side = "L";
	    $end = $lEnd;
	} elsif ($c eq $rName) {
	    $side = "R";
	    $end = $rEnd;
	} else {
	    die "Error: Neither [$leftContig] nor [$rightContig] match $c\n";
	}
	my $combo = "$side$s$end";
	my $orient = $orientationMap{$combo};

	push(@{$gapInfo[$g]},$r);
	$readData{$r} = $orient;
	$nPlaced++;
    }

    unless ($pairProjection == 1) {
	return "$nPlaced:$nProjected";
    }



    # Projected pairs
 
    while (my ($r,$a) = each(%readsToAlignments)) {

	my $mateName;
	my ($format, $prefix, $end, $index) = @{$readInfoHR->{$r}};

	my $mateEnd = ( $end eq '1' ? '2' : '1' );

	if ( ($format eq "ILMN-1.5") || ($format eq "generic-paired") )	{
	    $mateName = $prefix.$index."/".$mateEnd;
	}
	elsif ($format eq "ILMN-1.8")	{
	    $mateName = $prefix." ".$mateEnd.$index;
	}
	elsif ($format eq "HA")	{
	    $mateName = $prefix.$index."-R".$mateEnd;
	}
	
	unless (defined $mateName) {
	    die "Error: Could not determine mate name for read $r\n";
	}

	delete $readInfoHR->{$r};

	# Attempt to project the unplaced mate into a gap
	# assumes unplaced mate is same length as the placed one
	unless (exists($readsToAlignments{"$mateName"})) { #If the mate has an alignment, don't project it

	    my @alignInfo = split(/\t/,$alignments[$a]);
	    my ($rStart,$rStop,$rLen,$cName,$cStart,$cStop,$cLen,$strand) = @alignInfo[3..10];
	    my $unalignedStart = $rStart-1;
	    my $farProjection = $maxProject-$unalignedStart;
	    my $nearProjection = $minProject-$rLen-$unalignedStart;

	    # in the coordinate system of the aligned contig, the mate is projected to lie between nearProjection and farProjection
	    my $startGap = undef;
	    if ($strand eq "Plus") {
		$farProjection = $cStart+$farProjection;
		$nearProjection = $cStart+$nearProjection;
		$startGap = $contigGapMap{$cName}->[1];	# the 3'-contig-end gap		
	    } else {
		$farProjection = $cStop-$farProjection;
		$nearProjection = $cStop-$nearProjection;
		$startGap = $contigGapMap{$cName}->[0];	# the 5'-contig-end gap
	    }

	    if (defined($startGap)) {
		my $scaffID = $gapInfo[$startGap]->[0];

		my $testGap = $startGap;
		my ($testScaff,$leftContig,$rightContig,$gapCoords) = @{$gapInfo[$testGap]};

		my ($lcName,$lcEnd) = $leftContig =~ /^(.+)\.([35])$/;
		my ($rcName,$rcEnd) = $rightContig =~ /^(.+)\.([35])$/;
		my ($leftGapCoord,$rightGapCoord) = $gapCoords =~ /^(\d+):(\d+):/;
		my $iterator = undef;
		my $contigOrigin = undef;

		# find anchor-contig origin position in scaffold coords, assign direction of scan via iterator,
		# and transform projection bounds to scaffold coordinate system

		my $leftProjection = undef;
		my $rightProjection = undef;
		my $orient = undef;

		if ($lcName eq $cName) { # the contig to the left of the gap is aligned to the projector
		    $iterator = 1;
		    $orient = $orientationMap{"L$strand$lcEnd"};  
		    if ($strand eq "Plus") {
			$contigOrigin = $leftGapCoord-$cLen+1;
			$leftProjection = $contigOrigin+$nearProjection;
			$rightProjection = $contigOrigin+$farProjection;
		    } else {
			$contigOrigin = $leftGapCoord;
			$leftProjection = $contigOrigin-$nearProjection;
			$rightProjection = $contigOrigin-$farProjection;
		    }
		} elsif ($rcName eq $cName) { # the contig to the right of the gap is aligned to the projector
		    $iterator = -1;
		    $orient = $orientationMap{"R$strand$rcEnd"}; 
		    if ($strand eq "Plus") {
			$contigOrigin = $rightGapCoord+$cLen-1;
			$leftProjection = $contigOrigin-$farProjection;
			$rightProjection = $contigOrigin-$nearProjection;
		    } else {
			$contigOrigin = $rightGapCoord;
			$leftProjection = $contigOrigin+$farProjection;
			$rightProjection = $contigOrigin+$nearProjection;
		    }
		} else {
		    die "Error: Flanking contigs ($lcName [$testGap] $rcName) do not match anchor contig $cName.\n"; 
		}

		# The orientation of the projected read is opposite the projector
		if (defined($orient)) {
		    $orient = ($orient eq "+") ? "-" : "+";
		}

		# Iterate over gaps projecting mate into each gap that is within range

		while (1) {
		    my $placed = 0;
		    my ($l,$r) = ($leftGapCoord < $rightGapCoord) ? ($leftGapCoord,$rightGapCoord) : ($rightGapCoord,$leftGapCoord);
		    if (($rightProjection > $l) && ($leftProjection < $r)) {
			$placed = 1;
		    } elsif ( (($iterator == 1) && ($l > $rightProjection)) ||  
			      (($iterator == -1) && ($r < $leftProjection)) )  {
			last;
		    }

		    if ($placed) {
			push(@{$gapInfo[$testGap]},$mateName);
#			print STDERR "DEBUG: Projection $mateName assigned to gap $testGap \n";
			$readData{$mateName} = $orient;
			$nProjected++;
		    }

		    $testGap += $iterator;
		    if (($testGap < 0) || ($testGap == $totalGaps)) {
			last;
		    }

		    ($testScaff,$leftContig,$rightContig,$gapCoords) = @{$gapInfo[$testGap]};
		    unless ($testScaff eq $scaffID) {
			last;
		    }
		    ($leftGapCoord,$rightGapCoord) = $gapCoords =~ /^(\d+):(\d+):/;
		}
	    }
	}
    }

    return "$nPlaced:$nProjected";
}

