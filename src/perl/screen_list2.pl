#!/usr/bin/env perl

use warnings;

use Getopt::Std;

my %opts = ();
my $valid_line = getopts('l:f:n:c:kraCM', \%opts); 

if ( ($valid_line != 1) ||
     (!(exists ($opts{"l"}))) || 
     (!(exists ($opts{"f"}))) ) {

    print "Filter a fasta file for/against a list of entry names.\n";
    print "Usage: ./screen_list2.pl <-l list> <-f fasta file> <<-k(eep)>> <<-c column to filter on>> <<-n new name col>> <<-r(egexp)>> <<-a(llDefLine)>> <<-C(lones)>><<-M(ask)>>\n";
    exit;
}

my $matchClones = 0;
if (exists($opts{"C"})) {
    $matchClones = 1;
}

my $maskWLower = 0;
if (exists($opts{"M"})) {
    $maskWLower = 1;
}

my $regexp_search = 0;
if (exists($opts{"r"})) {
    $regexp_search = 1;
}

my $invert_sense = 0;
if (exists($opts{"k"})) {
    $invert_sense = 1;
}

my $allDefLine = 0;
if (exists($opts{"a"})) {
    $allDefLine = 1;
}

my $filter_col = 0;
if (exists($opts{"c"})) {
    $filter_col = $opts{"c"}-1;
}

my $name_col = $filter_col;
if (exists($opts{"n"})) {
    $name_col = $opts{"n"}-1;
}

my $list_file = $opts{"l"};

open(F,$list_file) || die "Couldn't open file $list_file\n";
my %bad_ones = (); 
while (my $i = <F>) {
    chomp $i;
    my @cols = split(/\s+/,$i);
    my $id = $cols[$filter_col];
    $val = $cols[$name_col];
    if ($matchClones == 1) {
	my ($clone) = $id =~ /(.+)\..+/;		
	$bad_ones{$clone} = $clone;
    } else {
	$bad_ones{$id} = $val;
    }
}
close F;

my $fasta_file = $opts{"f"};

open(F,$fasta_file) || die "Couldn't open file $fasta_file\n";

my $on_the_list = 0;
my $name = "";
while (my $i = <F>) {
    if ($i =~ />/) {

	my ($id) = $i =~ />(\S+)/;


	if ($regexp_search == 1) {
	    $on_the_list = 0;
	    foreach $key (keys (%bad_ones)) {
		my $tempid = $id;
		$tempid =~ s/\|/#/g;
		my $tempkey = $key;
		$tempkey =~ s/\|/#/g;
		if ($tempid =~ /$tempkey/) {
		    $on_the_list = 1;
		    $name = $bad_ones{$key};
		}
	    }
	} elsif ($matchClones == 1) {
	    $on_the_list = 0;	    
	    $id =~ /(.+)\..+/;		
	    my $clone = $1;
	    if (exists($bad_ones{$clone})) {
		$on_the_list = 1;
	    }
	} else {		
	    $on_the_list = 0;	    
	    if (exists($bad_ones{$id})) {
		$on_the_list = 1;
		$name = $bad_ones{$id};
	    }
	}
    }

    if (($invert_sense == 0) && ($on_the_list == 0)) {
	print $i;
    } elsif (($invert_sense == 1) &&  ($on_the_list == 1)) {
	if ($i =~ />/) {
	    if (($regexp_search == 1)||($allDefLine == 1)||($matchClones == 1)) {
		print "$i";
	    } else {
		print ">$name\n";
	    }
	} 
	else {
	    if ($maskWLower == 1) {
		$i =~ tr/ACTGN/actgn/;		
	    }
	    print $i;	    
	}
    }
}
close F;
