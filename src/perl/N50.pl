#!/usr/bin/env perl

use warnings;
use Getopt::Long;

my $usage = "Usage: N50.pl [-G <genome_size>] <input file> <0:fasta|1:name/size>\n";

my $genome_size = 0;
if (! GetOptions('G=i' => \$genome_size)) {
    die $usage;
}

if (@ARGV != 2) {
    die $usage;
}

my $input_style = $ARGV[1];
if (($input_style != 0) && ($input_style != 1)) {
    die $usage;
}

if ($genome_size < 0) {
    die $usage;
}

open(F,$ARGV[0]) || die "Couldn't open $ARGV[0]\n";;

my $sequence = "";
my $id = "NO_CURRENT_ID";
my $n_bases = 0;
my $total_bases = 0;
my %name2size = ();

# fasta input
if ($input_style == 0) {

    while (my $i = <F>) {
	chomp $i;
	
	if ($i =~ /^>/) {
	    
	    if ($id ne "NO_CURRENT_ID") {
		$n_bases = length($sequence);
#	    print "$id\t$n_bases\n";
		if (exists($name2size{$id})) {
		    warn "Warning $id multiply defined - results inaccurate\n";
		}
		$name2size{$id} = $n_bases;
		$total_bases += $n_bases;
		
		$sequence = "";
	    }
	    
	    ($id) = $i =~ /^>(\S+)/;
	    
	} else {
	    
	    $sequence .= $i;
	}
    }

    $n_bases = length($sequence);

#print "$id\t$n_bases\n";
    if (exists($name2size{$id})) {
	warn "Warning $id multiply defined - results inaccurate\n";
    }
    $name2size{$id} = $n_bases;
    $total_bases += $n_bases;

} else { 

    while (my $i = <F>) {
	chomp $i;

	($id, $n_bases) = $i =~ /^(\S+)\s+(\d+)/;

#	    print "$id\t$n_bases\n";
	if (exists($name2size{$id})) {
	    warn "Warning $id multiply defined - results inaccurate\n";
	}
	$name2size{$id} = $n_bases;
	$total_bases += $n_bases;
    }
}

close F;

my @size_sorted_names = sort {$name2size{$b} <=> $name2size{$a}} keys(%name2size);

my $running_total = 0;
my $n_seqs = 0;
foreach(@size_sorted_names) {
    my ($name,$size) = ($_,$name2size{$_});
    $running_total += $size;
    $n_seqs++;
    my $fractionTotalBp = $running_total/$total_bases;  # for N50 calc
    my $fractionGenSize = '';  #for NG50 calc
    if ($genome_size) {
	$fractionGenSize = $running_total/$genome_size;
    }
    print "$n_seqs\t$name\t$size\t$running_total\t$fractionTotalBp\t$fractionGenSize\n";
}
