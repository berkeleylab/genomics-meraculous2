#!/usr/bin/env perl
#
# optimize2.pl by Jarrod Chapman <jchapman@lbl.gov> Thu Jul 14 08:46:34 PDT 2005
# Copyright 2005 Jarrod Chapman. All rights reserved.
#

use warnings;

use Getopt::Std;
my %opts = ();
my $validLine = getopts('f:g:v:d:c:m:r', \%opts);
my @required = ("f","g","v",);
my $nRequired = 0;
map {$nRequired += exists($opts{$_})} @required;
$validLine &= ($nRequired == @required);
if ($validLine != 1) {
    print "Usage: ./optimize2.pl <<-d columnDelimiter>> <-v valueColumn> <-f file> <-g groupColumn> <<-r(andomBest)>> <<-c closeEnough(0-1)>> <<-m maxBest>>\n";
    exit;
}

my $delimiter = '\s+';
if (exists($opts{"d"})) {
   $delimiter = $opts{"d"};
}
my $printRandom = 0;
if (exists($opts{"r"})) {
    $printRandom = 1;
}
my $closeEnough = 0;
if (exists($opts{"c"})) {
    $closeEnough = $opts{"c"};
}
my $maxBest  = 0;
if (exists($opts{"m"})) {
    $maxBest = $opts{"m"};
}

my $valueCol = $opts{"v"};
my $file = $opts{"f"};
my $groupCol = $opts{"g"};

open (F,$file) || die "Couldn't open $file\n";
my %bestLine = ();
my %bestValue = ();

while (my $line = <F>) {
#    chomp $line;
    my @cols = split(/$delimiter/,$line);
    my $nCols = scalar(@cols);
    if (($groupCol > $nCols) || ($valueCol > $nCols)) {
	warn "Warning: column specification exceeds number of columns in line: $line\n";
    } else {
	$group = $cols[$groupCol-1];
	$value = $cols[$valueCol-1];
	if (!exists($bestValue{$group})) {
	    $bestValue{$group} = $value;
	    $bestLine{$group} = $line;
	} elsif ($bestValue{$group} < $value) {
	    $bestValue{$group} = $value;
	    my @oldBest = split(/\n/,$bestLine{$group});
	    $bestLine{$group} = $line;
	    foreach my $oldLine (@oldBest) {
		my @oldCols = split(/$delimiter/,$oldLine);
		my $oldValue = $oldCols[$valueCol-1];
		if ($oldValue >= (1-$closeEnough)*$value) {
		    $bestLine{$group} .= "$oldLine\n";
		}
	    }
	} elsif ($value >= (1-$closeEnough)*$bestValue{$group}) {
	    $bestLine{$group} .= $line;
	}
    }
}
close F;

foreach my $group (sort {$bestValue{$a} <=> $bestValue{$b}} keys(%bestValue)) {
    my $bestText = $bestLine{$group};
    if ($printRandom) {
	my @bests = split(/\n/,$bestText);
	my $pick = $bests[rand(@bests)];
	print "$pick\n";
    } elsif ($maxBest) {
	my @bests = split(/\n/,$bestText);
	my $nBest = scalar @bests;
	if ($nBest <= $maxBest) {
	    print $bestText;
	}
    } else {
	print $bestText;
    }
}
