#!/usr/bin/env perl 
#
# bubblePopper.pl
# Eugene Goltsman <egoltsman@lbl.gov>
# Last revision 02/24/2016

use warnings;
use strict;

use Getopt::Std;
use lib "$ENV{MERACULOUS_ROOT}/lib";

use List::Util 'sum';

my %opts = ();
my $validLine = getopts('b:f:d:m:I:M:K:RV', \%opts);
my @required = ("b","f","d","m");
my $nRequired = 0;
map {$nRequired += exists($opts{$_})} @required;
$validLine &= ($nRequired == @required);
if ($validLine != 1) {
    print "Usage: ./bubblePopper.pl <-b bubbleDataFile> <-f fastaFile> <-d depthFile> <-m merLength> [ -I maxInsSize] [ -K maskingMode ] [ -M \"blastMapGlob\"] [ -R requireCloneLinkageInDiplotigs[ -V(erbose)] \n";
    exit;
}

my $bubbleDataFile = $opts{"b"};
my $fastaFile = $opts{"f"};
my $cmdFile = $opts{"d"};
my $merLength = $opts{"m"};
my $verbose = $opts{"V"};
my $blastMapGlob = $opts{"M"};
my $insSize = $opts{"I"};
my $requireCloneLinkageInDiplotigs = $opts{"R"};    # As we move down the Contig-Bubble-Contig chain, the diplotig gets extended only if there's consistent clone linkage to the previous bubble. 
                                                    # Otherwise the diplotig is terminated and a new one is started.  Gives better haplotype consistency but often at the expense of N50
my $maskMode = $opts{"K"};
if ( ! defined $maskMode ) {$maskMode = 0;}


my $hairLength = 2*$merLength - 1;

my %contigInfo = ();  # map from contig to cmd info
my %bubbletigs = (); # map from bubble ID to mer-tips defining the bubble 
my %tipInfo = ();    # map from mer-tips to a pair of bubbletigs they flank
my %linkertigs = (); # map from non-bubble contigs to their mer-tips
my %contigsClonesHoH = ();  # map from contig to a hash of clones that mapped to it

my $nCompleteBubbles = 0;
my $nBlessedPaths = 0;

my $MODE = "";
if ($blastMapGlob) {
    $MODE = "haploPaths";

    if (!glob ($blastMapGlob)) {
	die "Error: no files matching the glob $blastMapGlob \n";
    }
} else {
    $MODE = "maxDepth";
}


print STDERR "Parsing bubbletig and linkertig data from $bubbleDataFile...\n";

open(BDATA,$bubbleDataFile)|| die "Can't open $bubbleDataFile for reading";
while (my $line = <BDATA>) {
    chomp $line;
    my @fields = split(/\s+/,$line);
    if ($fields[0] eq "LINKER") {
	my $contig = $fields[1];
	my $linkMers = $fields[2];
	$linkertigs{$contig} = $linkMers;
    }
    elsif ($fields[0] eq "BUBBLE") {
	my $bubbleID = $fields[1];
	my $linkMers = $fields[2];
	my $btigs = $fields[3];
	$bubbletigs{$bubbleID} = $linkMers;
	$tipInfo{$linkMers} = $btigs;
    }

}
close BDATA;




if ($MODE eq "haploPaths") {

     foreach my $blastMap (glob ($blastMapGlob)) {
	 
	 print STDERR "Parsing read->bubble mapping data from $blastMap...\n";
 
	 open(MAP,$blastMap) || die "Can't open $blastMap for reading";
 

	 # for effiiency, don't parse every read name to determine naming convention. If the first read looks like 
	 # a generic fwd/rev read name, assume the rest are named this way.  Otherwise, parse each read.  
	 my $format;
	 my $first = <MAP>;
	 my @f = split(/\t/,$first);
	 
	 if ($f[2] =~ /^\S+\s+[12]\:[YN]\:\d+\:[\sACTGN0-9+]*.*$/)  # illumina 1.8 naming
	 {
	     $format = 1;
	 }
	 elsif ( $f[2] =~ /^\S+\-R[12]/ )  #HudsonAlpha naming
	 {
	     $format = 2;
	 }
	 elsif ( $f[2] =~ /^\S+\/[12]/ )  # generic header ending with /1 or /2
	 {
	     $format = 3; 
	 }
	 
	 
	 seek MAP, 0, 0;  # rewind
	 
	 while (my $line = <MAP>) {
	     my @fields = split(/\t/,$line);
	     my ($r, $c) = ($fields[2], $fields[6]);
	     
	     my ($clone);
	     
	     if ($format == 1)  # illumina 1.8 naming
	     {
		 ($clone) = $r =~ /^(\S+)\s+[12].*/;
	     }
	     elsif ( $format == 2 )  #HudsonAlpha naming
	     {
		 ($clone) = $r =~ /^(\S+)\-R[12]/; 
	     }
	     elsif ( $format == 3  )  # generic header ending with /1 or /2
	     {
		 ($clone) = $r =~ /^(\S+)\/[12]/;
	     }
	     
	     
	     unless (defined $clone) {
		 die "Can't parse fastq header in:  $r";
	     }
	     # Since the contigs are uutigs, any read that mapped to a contig must therefore include at least one btig-specific position
#	$contigsClonesHoH{$c}{$clone} = 1;
	     if ( $contigsClonesHoH{$c}{$clone} ) {
		 $contigsClonesHoH{$c}{$clone}++;
	     } else {
		 $contigsClonesHoH{$c}{$clone} = 1;
	     }
	 }
	 close MAP;
     }
}



print STDERR "Reading contig depth info from $cmdFile...\n";

open (F,$cmdFile) || die "Couldn't open $cmdFile\n";
while (my $line = <F>) {
    chomp $line;
    my ($contigID,$nMers,$meanDepth) = split(/\s+/,$line);

    my $contigLength = $nMers + ($merLength-1);
    my $contigLabel = ($contigLength > $hairLength) ? 1 : 0;

    $contigInfo{$contigID} = [$nMers,$meanDepth,$contigLabel];
}
close F;

print STDERR "Done.\n";


# store contig sequence for later use 

print STDERR "Reading contig sequence from $fastaFile...\n";

my %contigSequences = ();
my $currentEntry = undef;
my $currentSeq = "";
open (F,$fastaFile) || die "Couldn't open $fastaFile\n";
while (my $line = <F>) {
    chomp $line;
    if ($line =~ /^>/) {
        if (defined($currentEntry)) {
	    $contigSequences{$currentEntry} = $currentSeq;
        }
        $currentSeq = "";
        ($currentEntry) = $line =~ /^>(.+)$/;
    } else {
        $currentSeq .= $line;
    }
}
if (defined($currentEntry)) {
    $contigSequences{$currentEntry} = $currentSeq;
}
close F;

print STDERR "Done.\n";


# Build the bubble-contig graph (%pointsTo contains the edge info)

print STDERR "Building bubble-contig graph...\n";

my %pointsTo = ();

while (my ($bubbleID,$linkMers) = each(%bubbletigs)) {
    my ($in,$out) = $linkMers =~ /^(.+)\.(.+)$/;
    my $rcLinkMers = reverse($linkMers);
    $rcLinkMers =~ tr/ACGT/TGCA/;
    my ($rcin,$rcout) = $rcLinkMers =~ /^(.+)\.(.+)$/;

    if (exists($pointsTo{$in})) {
	$pointsTo{$in} .= ",B+$bubbleID";
    } else {
	$pointsTo{$in} = "B+$bubbleID";
    }
    
    if (exists($pointsTo{$rcin})) {
	$pointsTo{$rcin} .= ",B-$bubbleID";
    } else {
	$pointsTo{$rcin} = "B-$bubbleID";
    }
}

while (my ($contigID,$linkMers) = each(%linkertigs)) {
    my ($in,$out) = $linkMers =~ /^(.+)\.(.+)$/;
    my $rcLinkMers = reverse($linkMers);
    $rcLinkMers =~ tr/ACGT/TGCA/;
    my ($rcin,$rcout) = $rcLinkMers =~ /^(.+)\.(.+)$/;

    if ($in) {
	if (exists($pointsTo{$in})) {
	    $pointsTo{$in} .= ",C+$contigID";
	} else {
	    $pointsTo{$in} = "C+$contigID";
	}
    }
    
    if ($rcin) {
	if (exists($pointsTo{$rcin})) {
	    $pointsTo{$rcin} .= ",C-$contigID";
	} else {
	    $pointsTo{$rcin} = "C-$contigID";
	}
    }
}

print STDERR "Done.\n";


# traverse the bubble-contig graph, starting from each contig

open (LIST, ">haplotigs.list") || die "Couldn't open file for writing \n";

if ($MODE eq "haploPaths") {
    open (CLONES, ">clones2diplotigs.map") || die "Couldn't open file for writing \n";
}

print STDERR "Generating diplotigs...\n";

my %visited = ();
my $diplotigID = 0;

while (my ($contigID,$linkMers) = each(%linkertigs)) {
    my $current = "C+$contigID";
    my $currentID = "C$contigID";

    if (exists($visited{$currentID})) {
	next;
    } else {
	$visited{$currentID} = 1;
    }

    my @downstream = ();
    my $next = getNext($current);

    while ($next) {
	my ($type,$strand,$id) = $next =~ /^([CB])([\+\-])(.+)$/;
	my $nextID = $type . $id;

	if (exists($visited{$nextID})) {
	    warn "warning: Inconsistency encountered in forward walk from $contigID at $nextID.  Truncating.\n";
	    warn "warning: [$type][$strand][$id] [$nextID] [$visited{$nextID}]\n";
	    $next = 0;
	} else {
	    $visited{$nextID} = 1;
	    push (@downstream,$next);
	    $current = $next;
	    $next = getNext($current);
	}
    }

    $current = "C-$contigID";

    my @upstream = ();
    $next = getNext($current);

    while ($next) {
	my ($type,$strand,$id) = $next =~ /^([CB])([\+\-])(.+)$/;
	my $nextID = $type . $id;
	my $rStrand = "-";
	if ($strand eq "-") {
	    $rStrand = "+";
	}

	if (exists($visited{$nextID})) {
	    warn "warning: Inconsistency encountered in reverse walk from $contigID at $nextID.  Truncating.\n";
	    warn "warning: [$type][$strand][$id] [$nextID] [$visited{$nextID}]\n";
	    $next = 0;
	} else {
	    $visited{$nextID} = 1;
	    unshift(@upstream,"$type$rStrand$id");
	    $current = $next;
	    $next = getNext($current);
	}
    }

    # remove leading and trailing bubbles

    my $nUpstream = scalar(@upstream);
    if ($nUpstream > 0) {
	my $first = $upstream[0];
	my ($type,$strand,$id) = $first =~ /^([CB])([\+\-])(.+)$/;
	if ($type eq "B") {
	    shift(@upstream);
	}
    }
    my $nDownstream = scalar(@downstream);
    if ($nDownstream > 0) {
	my $last = $downstream[-1];
	my ($type,$strand,$id) = $last =~ /^([CB])([\+\-])(.+)$/;
	if ($type eq "B") {
	    pop(@downstream);
	}
    }

    my $nTigs = 1 + scalar(@upstream) + scalar(@downstream);

    if ($nTigs > 1) {       # a Contig-Bubble-Contig string

	print STDERR "\n\nNEW (C-B-C)n CHAIN OF $nTigs \n\n" if ($verbose);

	my $finalSequence = "";
	$diplotigID++;
	my $CBCstring = "diplotig$diplotigID: ";
	my $nContigMers = 0;
	my $weightedContigDepth = 0;
	my $nBubbles = 0; 
	my $nLinkertigs = 0;
#	my @discardedBtigs = ();  
	my @haploPaths = ();   # two paths representing separate haplotypes
	my @clonesMappedToPath = ();
	my $warningString = "";

	my @tigs = (@upstream,"C+$contigID",@downstream);
	my $idx = 0;
	foreach my $tig (@tigs) {


	    $CBCstring .=  "$tig->";
	    print STDERR "\n$CBCstring\n" if ($verbose);
	    my ($type,$strand,$id) = $tig =~ /^([CB])([\+\-])(.+)$/;

	    if ($type eq "B") {

		# A bubble consists of two bubbletigs. 
		# Extract each bubbletig's id, strand, and depth info
		# Determine each bubbletig's sequence, reversing and clipping as needed

		$nBubbles++;  
		$nCompleteBubbles++;

		my $linkMers = $bubbletigs{$id};
		my $btigs = $tipInfo{$linkMers};		
		my @currentBubbleBtigs = split(/\,/,$btigs);

		unless (scalar(@currentBubbleBtigs) == 2) {
		    $warningString .= "warning: Only two-path bubbles are currently supported. ($tig = [@currentBubbleBtigs])\n";
		}
		
		my %btigSeqs = ();
		my %btigDepths = ();

		foreach my $btig (@currentBubbleBtigs) {
		    my ($s,$cid) = $btig =~ /^([\+\-])(.+)$/;

		    my $label = 2;
		    $contigInfo{$cid}->[2] = $label;
		    
		    my $depth = $contigInfo{$cid}->[1];
		    $btigDepths{$btig} =  sprintf("%.2f", $depth);

		    my $seq = $contigSequences{$cid};
		    if ($s ne $strand) {
			my $rcSeq = reverse($seq);
			$rcSeq =~ tr/ACGT/TGCA/;
			$seq = $rcSeq;
		    }
		    $btigSeqs{$btig} = $seq;
		}
		
		my @sortedBtigs = sort {length($btigSeqs{$a}) <=> length($btigSeqs{$b})} @currentBubbleBtigs;
		my @btigLens = map {length($btigSeqs{$_})} @sortedBtigs;

		my $minLen = $btigLens[0];
		my $minClipLen = $minLen - (2*$merLength - 4);
		my $tailSeq = "";
		my @seqPaths = ();
		my @suspendedBtigs = ();

		if ($minClipLen < 0) {
		    # The smaller of the two btigs is too short for redundant end clipping, so instead we clip $minClipLen bp from the tail of the last C-tig 
		    if ($MODE eq "haploPaths") {
			my @lastCtigInfo = split(/\:/,${$haploPaths[0]}[-1]);     #"$id:$type:$seq:$depth:0"
			my $lastCtigSeq = $lastCtigInfo[2];
			$tailSeq = substr( $lastCtigSeq, $minClipLen);
			for (my $n = 0; $n < -$minClipLen; $n++) {
			    chop $lastCtigSeq;
			}
			# update the haplo-paths with the chopped contig info  (same for both since it's a C-tig)
			$lastCtigInfo[2] = $lastCtigSeq;		       
			${$haploPaths[0]}[-1] = join(':',@lastCtigInfo);
			${$haploPaths[1]}[-1] = join(':',@lastCtigInfo);

		    } 
		    else {
			$tailSeq = substr($finalSequence,$minClipLen);
			for (my $n = 0; $n < -$minClipLen; $n++) {
			    chop $finalSequence;
			}
		    }
		}

		my $maxDepthIndex = 0;
		my $maxDepth = 0;
		my $btigIndex = 0;

		foreach my $btig (@sortedBtigs) {

		    print STDERR "\nProcessing bubbletig $btig...\n\n" if ($verbose);
		    my $seq = $btigSeqs{$btig};
		    my $btigLen = length($seq);
		    print STDERR "btig $btig: $seq\n" if ($verbose);
		    print STDERR "btigLen: $btigLen\n" if ($verbose);
		    my $clipLen = $btigLen - (2*$merLength - 4);
		    if ($clipLen < 0) {
			#too small to clip; Instead using the saved trimmed sequence from the end of the previous C-tig
			my $diff = $clipLen - $minClipLen;
			my $addBack = substr($tailSeq,0,$diff);
			push(@seqPaths,$addBack);
		    } else {
			my $clipSeq = substr($seq,$merLength-2,$clipLen);
			push(@seqPaths,$tailSeq.$clipSeq);
		    }
		    
		    print STDERR "bubble path: $seqPaths[-1]\n" if ($verbose);


		    my $depth = $btigDepths{$btig};
		    if ($depth >= $maxDepth) {
			$maxDepth = $depth;
			$maxDepthIndex = $btigIndex;
		    }

		    if ($MODE eq "haploPaths") {
			# determine which of the two running haplo-paths this btig belongs to and update the haplo-path

			my ($btigInfoString, $pathIdx);			
			my ($s,$cid) = $btig =~ /^([\+\-])(.+)$/;
			unless ($nBubbles == 1) {  # skip first bubble, i.e. we can't possibly link it to anything
			    ($btigInfoString, $pathIdx) = map_btig_to_haplo_path( $cid, $type, $seqPaths[-1], $depth, \@haploPaths, \%contigsClonesHoH);
			}
			if (defined($pathIdx) && defined($btigInfoString)) {
			    if ( scalar(@{$haploPaths[$pathIdx]}) >= ($nBubbles+$nLinkertigs) ) {  
				# we've already mapped a btig from this bubble to this path, so we need to pull back and resolve the conflict 
				print STDERR "Warning:  this btig maps to the same path as did its bubble-brother! Postponing the path assignment...\n" if ($verbose);
				push(@suspendedBtigs, $btigInfoString  );
			    }
			    else {
				push( @{$haploPaths[$pathIdx]}, $btigInfoString );  # add this btig to the selected path
				push( @{$clonesMappedToPath[$pathIdx]},  ( keys %{ $contigsClonesHoH{$cid}}) );  #save all clones mapping to this path - will be printed to a file
			    }
			}
			else {
			    # the btig couldn't be assigned to a path by mapped reads so we pospone the assignment 
			    $btigInfoString = "$cid:$type:$seqPaths[-1]:$depth:0";
			    print STDERR "Postponing the path assignments...\n" if ($verbose);			    
			    push(@suspendedBtigs, $btigInfoString );
			}
		    }
		    $btigIndex++;
		}

		if ($MODE eq "haploPaths" && (scalar @suspendedBtigs)) {
		    
		    if (scalar @suspendedBtigs == 1)  {
		    # one of the btigs wasn't mapped to a haplo-path, but the other one was, so we assign the umapped btig to the still vacant path
			my $vacantPathIndex = ( $#{$haploPaths[0]} > $#{$haploPaths[1]} ? 1 : 0 );
			print STDERR "\nAssigned suspended btig to vacant path $vacantPathIndex\n" if ($verbose);
			push( @{$haploPaths[$vacantPathIndex]}, $suspendedBtigs[0] ); 

                        #save all clones mapping to this path - will be printed to a file
			my @info = split(/\:/, $suspendedBtigs[0]);
			my $cid = $info[0];
			push( @{$clonesMappedToPath[$vacantPathIndex]},  ( keys %{ $contigsClonesHoH{$cid}}) );  #save all clones mapping to this path - will be printed to a file
		    }
		    elsif ((scalar @suspendedBtigs == 2 ) && $nBubbles > 1 && $requireCloneLinkageInDiplotigs ) {
			print STDERR "\nCould not make a linkage-reinforced extension of the haplo-paths.  Terminating diplotig$diplotigID\n"  if ($verbose);
			$nBubbles--;
			terminate_diplotig(\@tigs, $idx, \@sortedBtigs, \%contigInfo); 
			if ($minClipLen < 0 ) {
			    # if we had to clip overlapping sequence from the upstream C-tig, add back the clipped sequence to the end of both haplopaths
			    my @lastCtigInfo = split(':',${$haploPaths[0]}[-1]);     #"$id:$type:$seq:$depth:0"
			    $lastCtigInfo[2] .= $tailSeq;
			    ${$haploPaths[0]}[-1] = ${$haploPaths[1]}[-1] = join(':', @lastCtigInfo);
			}
			if ($idx != ($#tigs - 1)) {
			    # if this is not the end of the chain, print current diplotig sequences, then reset everything and start a new diplotig			    
			    print_paths(\@haploPaths, $nLinkertigs, $nBubbles, $diplotigID, \@clonesMappedToPath);
			    $diplotigID++;
			    $CBCstring = "diplotig$diplotigID: ";
			    $nContigMers = 0;
			    $weightedContigDepth = 0;
			    $nBubbles = 0;
			    $nLinkertigs = 0;
			    @haploPaths = ();
			    @currentBubbleBtigs = ();
			    $idx++;
			    @clonesMappedToPath = ();
			    next;
			}
			else {
			    last;
			}
		    }
		    else {
			# make an arbitrary assignment
			print STDERR "\nAssigned suspended btigs @suspendedBtigs to paths based on majority mer depth.\n"  if ($verbose);
			push( @{$haploPaths[0]}, $suspendedBtigs[$maxDepthIndex]);
			push( @{$haploPaths[1]}, $suspendedBtigs[(abs($maxDepthIndex-1))]);

			#save all clones mapping to these two paths - will be printed to a file
			my @info = split(/\:/, $suspendedBtigs[$maxDepthIndex]);
                        my $cid = $info[0];
			push( @{$clonesMappedToPath[0]},  ( keys %{ $contigsClonesHoH{$cid}}) ); 

			@info = split(/\:/, $suspendedBtigs[(abs($maxDepthIndex-1))]);
			$cid = $info[0];
			push( @{$clonesMappedToPath[1]},  ( keys %{ $contigsClonesHoH{$cid}}) ); 

		    }		    
	  

		} elsif ($MODE eq "maxLength") {
		    
		    $finalSequence .= $seqPaths[-1];

		} elsif ($MODE eq "minLength") {

		    $finalSequence .= $seqPaths[0];

		} elsif ($MODE eq "maxDepth") {
		    
		    my $chosenIndex = $maxDepthIndex;
#		    my $discardedIndex = abs($maxDepthIndex-1);
		    $finalSequence .= $seqPaths[$chosenIndex];
#		    push (@discardedBtigs, $sortedBtigs[$discardedIndex]);
#		    print STDERR "\nB-tig $sortedBtigs[$chosenIndex] chosen for the mosaic path.\nB-tig $sortedBtigs[$discardedIndex] discarded\n" if ($verbose);

		}
	    # end of  ($type eq B) {

	    } elsif ($type eq "C") {

		print STDERR "\nProcessing linkertig $id...\n\n" if ($verbose);

		$nLinkertigs++;
		my $label = 2;
		$contigInfo{$id}->[2] = $label;

		my $depth = $contigInfo{$id}->[1];
		$depth =  sprintf("%.2f", $depth);
		my $nMers = $contigInfo{$id}->[0];
		$nContigMers += $nMers;
		$weightedContigDepth += $nMers*$depth;
		
		my $seq = $contigSequences{$id};
		if ($strand eq "-") {
		    my $rcSeq = reverse($seq);
		    $rcSeq =~ tr/ACGT/TGCA/;
		    $seq = $rcSeq;
		}
      		print STDERR "C-tig $id: $seq\n" if ($verbose);

		if ($MODE eq "haploPaths") {
		    if ($maskMode == 2) {
			# We mask all C-tig sequences to avoid ambiguous read mapping down the line. 
			# Because of redundancy clipping of btigs, we leave the first and last (k-1)/2 bases of the C-tig unmasked. 
			# This will ensure that the btig will have at least a k-sized stretch of unique sequence while the C-tig won't.
			my $maskLen = length($seq) - ($merLength-1);
			substr($seq,(($merLength-1)/2), $maskLen) =~ tr/ACGT/acgt/; 
		    }
		    # add the contig sequence to BOTH haplo-paths without contributing the depth info since we only count bubbletig depths in this scenario
		    push(@{$haploPaths[0]}, "$id:$type:$seq:0:0" );
		    push(@{$haploPaths[1]}, "$id:$type:$seq:0:0" );
		}
		else {
		    $finalSequence .= "$seq";
		}
	    }
	    $idx++;
	    
	}
	if ($MODE eq "haploPaths") {
	# print BOTH alignment-supported haplo-paths
	    unless (scalar @{$haploPaths[0]} == scalar @{$haploPaths[1]}) { 
		die "Invalid haplo-paths:  different number of contigs!\n  @{$haploPaths[0]}\n  @{$haploPaths[1]}\n";
	    }
	    print_paths(\@haploPaths, $nLinkertigs, $nBubbles, $diplotigID, \@clonesMappedToPath);
	}
	else	{
	    # print the "composite" diplotig info  (here we use contig depths only and ignore individual bubbletigs)
	    my $name = "diplotig$diplotigID";
	    my $length = length($finalSequence);
	    print LIST "$name\t$length\t$nLinkertigs\t$nBubbles\t".($weightedContigDepth/$nContigMers)."\n";
	    printFasta($name,$finalSequence);
	}

	if ($warningString) {
	    warn $warningString;
	}
    }
}

my $isotigID = 0;
foreach my $cid (keys(%contigInfo)) {

    my $contigLabel = $contigInfo{$cid}->[2];
    if ($contigLabel == 1) {
	$isotigID++;
	my $seq = $contigSequences{$cid};
	my $name = "isotig$isotigID";
	my $length = length($seq);
	my $depth = $contigInfo{$cid}->[1];
	print LIST "$name\t$length\t1\t0\t$depth\t$cid\n";
	printFasta($name,$seq);
    }
}
close LIST;
close CLONES;

if (! $isotigID) { die "No contigs matched minimum length requirements \n"}

print STDERR "Done.\n\n\n";

print STDERR "Total complete bubbles (non-bubble uutig on both ends) : $nCompleteBubbles \n";

print STDERR "Total isotigs: $isotigID\n";
if ($MODE eq "haploPaths") {
    print STDERR "Total blessed paths (diplotig pairs): $nBlessedPaths \n";
}
else {
    print STDERR "Total diplotigs: $diplotigID\n";
}

print STDERR "Done.\n";



#### utilities


sub getNext {
    my ($tigID) = @_;

    my ($type,$strand,$id) = $tigID =~ /^([BC])([\+\-])(.+)$/;

    my $linkMers = "";
    if ($type eq "C") {
	unless (exists($linkertigs{$id})) {
	    die "LINKERTIGS NOT DEFINED for [$id]\n";
	}
	$linkMers = $linkertigs{$id};
    } else {
	unless (exists($bubbletigs{$id})) {
	    die "BUBBLETIGS NOT DEFINED for [$id]\n";
	}

	$linkMers = $bubbletigs{$id};
    }
    my ($in,$out) = $linkMers =~ /^(.+)\.(.+)$/;
    my $rcLinkMers = reverse($linkMers);
    $rcLinkMers =~ tr/ACGT/TGCA/;
    my ($rcin,$rcout) = $rcLinkMers =~ /^(.+)\.(.+)$/;

    my $next = 0;
    if ($strand eq "-") {
	if (($rcout) && (exists($pointsTo{$rcout}))) {
	    $next = $pointsTo{$rcout};
	}
    } else {
	if (($out) && exists($pointsTo{$out})) {
	    $next = $pointsTo{$out};
	}
    }

    my @next = split(/\,/,$next);
    if ($#next > 0) {
	warn "warning: getNext finds multiple connections for $tigID (@next). Terminating.\n";
	$next = 0;
    }

    return $next;

}


sub printFasta {
    my ($name,$seq) = @_;
    my $seqLen = length($seq);

    my $bpl = 50;

    my $nLines = sprintf("%d",$seqLen/$bpl);
    if ($seqLen%$bpl != 0) {
        $nLines++;
    }

    print ">$name\n";
    for (my $j = 0;$j<$nLines;$j++) {
        my $seqLine = substr($seq,$j*$bpl,$bpl);
        print "$seqLine\n";
    }
}



sub map_btig_to_haplo_path {

    my ($cid, $type, $seq, $depth, $haploPathsAR, $contigsClonesHoHR) = @_;

    # @haploPaths is an array of TWO arrays in which the elements are strings of type "$contig:$type:$seq"
    # 
    my $linked = 0;	
    my $pathIdx;
    my $infoString = "";
    my @clonesMappedHere = keys %{ $contigsClonesHoHR->{$cid}};

#    print STDERR "DEBUG: clones mapped to btig $cid: @clonesMappedHere\n" if ($verbose);

    for (my $i=0; $i<=1; $i++)    {
	print STDERR "Looking for linkage w path $i...\n" if ($verbose);	    
	my @path = @{ $$haploPathsAR[$i] };
	my $nSupportLinkage = 0;
	my $pathLinksHR = is_linked_to_path( \@path, $contigsClonesHoHR, \@clonesMappedHere);
	if ( keys %$pathLinksHR ) {		
	    $nSupportLinkage = sum values %$pathLinksHR;
	    print STDERR "$cid is linked to haplo-path $i by at least $nSupportLinkage mapped clones\n" if ($verbose);	    

	    my @linkedToBtigs;
	    foreach my $bt (keys %$pathLinksHR) {
		if ($$pathLinksHR{$bt} > 0) {
		    push(@linkedToBtigs, $bt);
		}
	    }
	    $infoString = "$cid:$type:$seq:$depth:$nSupportLinkage:".(join(",", @linkedToBtigs));	    
	    $pathIdx = $i;
	    $linked++;
	}	    
    }
    if ($linked == 0)	{
	print STDERR "Couldn't link $cid (".length($seq)." bp, ".scalar(@clonesMappedHere)." clones)  to either path!\n " if ($verbose);
	return undef;
    }
    elsif ($linked > 1)	{
	print STDERR "$cid fits both paths ..\n" if ($verbose);
	return undef;
    }
    else {
	return ($infoString, $pathIdx);
    }
}


#
# Use common mapped clones to determine if contig is linked to the given path
#
sub is_linked_to_path {
    my ($pathAR, $contigsClonesHoHR, $clonesMappedHereAR) = @_;
    my %pathLinks;
    my $min = 2;
    my $cap = 2;  # stop when this many supporting links are found (optimization)
    my $maxSeqBack = (defined $insSize) ? $insSize : 1000;  # how far back in the path to go (in bases)

    # check if clone is mapped to the btigs the path AND to the current btig
    my $pathLinksTotal = 0;
    my $seenBtigs = 0;
    my $seenSeq = 0;
    my $i = $#$pathAR; 
    my @linkingClones = ();
    while ($i) {
     	$i--;  #skip last tig right away - it should be a C or a conflicting btig from this same bubble.
    	my $c = $pathAR->[$i];
    	my @a = split(/:/,$c);
    	my $cid = $a[0];
    	my $type = $a[1];
	my $len = length($a[2]);
	$seenSeq += $len;
    	next unless ($type eq "B");
	$seenBtigs++;

	$pathLinks{$cid} = 0; 
	print STDERR "Checking linkage w btig $i ($cid)..\n" if ($verbose);
	foreach my $clone (@$clonesMappedHereAR ){
	    if (exists $contigsClonesHoHR->{$cid}{$clone}) {
		push(@linkingClones, $clone);
		$pathLinks{$cid}++;
		$pathLinksTotal++;
		last if ($pathLinksTotal == $cap);
	    }
	}

	last if ($pathLinksTotal == $cap);      
	last if ($seenSeq >= $maxSeqBack);

    }

#    unless (%pathLinks) { return undef }
    if ( $pathLinksTotal >= $min ) { 	
	print STDERR "\tlinked by [ @linkingClones ] seenSeq: $seenSeq bp, seenBtigs: $seenBtigs  \n" if ($verbose);
	return \%pathLinks;
    }
    else {
	return undef;
    }
}



#
# Use contig mer depths to determine the "dominant" haplo-path
#
sub bless_haplopaths {
    my $haploPathsAR = shift;   # a reference to an array of arrays
    my $blessedHaploSeq;
    my @haploSeqs;
    my @pathNMers;
    my @pathMerDepthSum;
    my @pathMerDepthAvg;
    my @pathContigsList;

    my  $i=0;
    foreach my $pathRef (@$haploPathsAR) {
	my $pathNMers = 0;
	my $pathLength = 0;
	foreach my $c (@$pathRef) {
	    my @a = split(/:/,$c);
	    my ($cid, $type, $seq, $depth) = @a[0..3];
	    my $nMers = $contigInfo{$cid}->[0];
	    $haploSeqs[$i] .= $seq;
	    $pathContigsList[$i] .= "$type:$cid,";
	    if ($type eq 'B') {  # we only consider btigs in the overall depth calculation
		$pathNMers[$i] += $nMers;
		$pathMerDepthSum[$i] += ($depth * $nMers);   
	    }
	}
	$pathMerDepthAvg[$i] = $pathMerDepthSum[$i]/$pathNMers[$i];
	$i++;
    }	    
    return (\@haploSeqs, \@pathMerDepthAvg, \@pathContigsList);
}


#
# Tie up loose ends when the diplotig chain can't be continued past the current bubble
#
sub terminate_diplotig {
    my ($tigsAR, $idx, $currBtigsAR, $contigInfoHR) = @_;

    # re-label the current pair of btigs back to "isotigs"
    foreach my $btig (@$currBtigsAR) {
	my ($s,$cid) = $btig =~ /^([\+\-])(.+)$/;
	my $label = 1;
	$contigInfoHR->{$cid}->[2] = $label;

    }
    print STDERR "Terminated the C-B-C chain of ".(scalar(@$tigsAR))." at element $idx. \n" if ($verbose);
}


sub print_paths {
    my ($haploPathsAR, $nLinkertigs, $nBubbles, $diplotigID, $clonesToPathAR ) = @_;

    my ( $haploSeqsAR, $pathMerDepthAvgAR, $pathContigsListAR) = bless_haplopaths($haploPathsAR); 
    $nBlessedPaths = $nBlessedPaths+2;
      
    for (my $i=1; $i<=2; $i++) {
	my ($haploSeq, $pathMerDepthAvg, $pathContigsList) = ($$haploSeqsAR[$i-1], $$pathMerDepthAvgAR[$i-1], $$pathContigsListAR[$i-1]);
	
	if ($maskMode == 2) {
	    # To complete the masking scheme, mask the first and last (k-1)/2 bases 	    
	    substr($haploSeq,-(($merLength-1)/2) ) =~ tr/ACGT/acgt/;
	    substr($haploSeq,0,(($merLength-1)/2) ) =~ tr/ACGT/acgt/;
	}
#	if (($maskMode == '1') && ($i eq 1)) {
#	    # unmask everything in path 1
#	    $haploSeq=~ tr/acgt/ACGT/;
#	}
	# print the individual diplotig path info
	my $name = "diplotig${diplotigID}_p${i}";
	my $length = length($haploSeq);
	print LIST "$name\t$length\t$nLinkertigs\t$nBubbles\t$pathMerDepthAvg\t$pathContigsList\n";
	printFasta($name,$haploSeq);

	my @uniqueClonesInPath = uniq($$clonesToPathAR[$i-1]);

#	my %counts;
#	$counts{$_}++ for @{$$clonesToPathAR[$i-1]};
#	foreach (keys %counts) {
#	    push (@uniqueClonesInPath, $_) if ($counts{$_} == 1);
#	}
	my $clonesStr = join(",", @uniqueClonesInPath);
	print CLONES "$name\t$clonesStr\n";

    }
}


sub uniq {
    my $arrRef = shift;
    my %seen;
    return grep { !$seen{$_}++ } @$arrRef;
}
