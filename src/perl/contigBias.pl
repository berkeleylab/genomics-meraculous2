#!/usr/bin/env perl
#
# contigBias.pl by Jarrod Chapman <jchapman@lbl.gov> Wed Aug 31 13:18:36 PDT 2011
# Copyright 2011 Jarrod Chapman. All rights reserved.
#
use warnings;

use Getopt::Std;
my %opts = ();
my $validLine = getopts('f:b:m:', \%opts);
my @required = ("f","b","m");
my $nRequired = 0;
map {$nRequired += exists($opts{$_})} @required;
$validLine &= ($nRequired == @required);
if ($validLine != 1) {
    print "Usage: ./contigBias.pl <-b gcBiasFile> <-f fastaFile> <-m meanContigDepth>\n";
    exit;
}

my $biasFile = $opts{"b"};
my $fastaFile = $opts{"f"};
my $meanContigDepth = $opts{"m"};

my %gcBias = ();
my $merSize = 0;
open (B,$biasFile) || die "Couldn't open $biasFile\n";
while (my $line = <B>) {
    chomp $line;
    my ($nGC,$freq,$mean,$median,$mode) = split(/\t/,$line);
    $merSize = $nGC;
    if ($mean eq "NA") {
	next;
    }
    $gcBias{$nGC} = $mean/$meanContigDepth;
}
close B;

my $matchString = "[ACGT]{$merSize}";

my $currentEntry = undef;
my $seq = "";
open (F,$fastaFile) || die "Couldn't open $fastaFile\n";
while (my $line = <F>) {
    chomp $line;
    if ($line =~ /^>/) {
        if (defined($currentEntry)) {
            processSequence($currentEntry,$seq);
        }
        $seq = "";
        ($currentEntry) = $line =~ />(\S+)/;
    } else {
        $seq .= $line;
    }
}
close F;

if (defined($currentEntry)) {
    processSequence($currentEntry,$seq);
}

sub processSequence {
    my ($name,$seq) = @_;
    my $seqLen = length($seq);
    
    unless ($seqLen >= $merSize) {
        return;
    }

    my $totalBias = 0;
    my $nMers = 0;

    while ($seq =~ /($matchString)/g) {
        my $mer = $1;
        pos($seq) -= ($merSize-1);
	my $cg = $mer =~ tr/[CG]//;

	if (exists($gcBias{$cg})) {
	    $totalBias += $gcBias{$cg};
	    $nMers++;
	}
    }

    my $bias = sprintf("%.2f",$totalBias/$nMers);
    print "$name\t$bias\n";

}
