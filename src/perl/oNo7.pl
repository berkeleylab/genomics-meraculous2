#!/usr/bin/env perl
#
# oNo.pl by Jarrod Chapman & Eugene Goltsman <egoltsman@lbl.gov>
# Version 7.  Last revision 02/24/2016
# Copyright 2009 Jarrod Chapman. All rights reserved.
#

use warnings;
use Getopt::Std;

my %opts = ();
my $validLine = getopts('m:l:p:s:c:B:D:P:V', \%opts);
my @required = ("m","l");
my $nRequired = 0;
map {$nRequired += exists($opts{$_})} @required;
$validLine &= ($nRequired == @required);

$validLine &= (exists($opts{"s"}) || exists($opts{"c"}));

if ($validLine != 1) {
    print "Usage: ./oNo7.pl <-m merSize> <-l linkDataFile> (<-s scaffoldReportFile> || <-c contigReportFile>)  <<-p pairThreshold>> <<-B blessedLinkFile>>  <<-P(olymorphicMode) [1|2]>> <<-D diploidDepthThreshold>> <<-V(erbose)>>\n";
    exit;
}

my $date = `date`;
chomp $date;
print STDERR "$date\noNo7";
while (my ($opt,$val) = each(%opts)) {
    print STDERR " -$opt $val";
}
print STDERR "\n";

my $merSize = $opts{"m"};
my $fastaFile = $opts{"f"};
my $linkDataFile = $opts{"l"};
my $ceaFile = $opts{"e"};
my $cmdFile = $opts{"d"};

my $srfFile = undef;
my $crfFile = undef;
if (exists($opts{"s"})) {
    $srfFile = $opts{"s"};
} elsif (exists($opts{"c"})) {
    $crfFile = $opts{"c"};
}

my $pairThreshold = 2;
if (exists($opts{"p"})) {
    $pairThreshold = $opts{"p"};
}

my $polymorphicMode = 0;
if (exists($opts{"P"})) {
    $polymorphicMode = $opts{"P"};
}

my $diploidDepthCutoff = 0;
if (exists($opts{"D"})) {
    $diploidDepthCutoff = $opts{"D"};
}

if ($polymorphicMode && !$diploidDepthCutoff) {
    die "Error: Polymorphic mode (-P) requires a non-zero diploid depth threshold (-D) ";
}

my $blessedLinkFile = undef;
my %blessedLinks = ();
if (exists($opts{"B"})) {
    $blessedLinkFile = $opts{"B"};
    open (B,$blessedLinkFile) || die "Couldn't open $blessedLinkFile\n";
    while (my $line = <B>) {
	chomp $line;
	my ($s1,$s2) = split(/\s+/,$line);
	my $bless = ($s1 lt $s2) ? "$s1.$s2" : "$s2.$s1";
	$blessedLinks{$bless} = 1;
    }
    close B;
}
my $verbose = 0;
if (exists($opts{"V"})) {
    $verbose = 1;
}


# Read linkData file and store library information

my %libInfo = ();
my @libs = ();
open (L,$linkDataFile) || die "Couldn't open $linkDataFile\n";
while (my $line = <L>) {
    chomp $line;
    my ($libs,$insertSizes,$stdDevs,$linkFile) = split(/\t/,$line);
    my @l = split(/\,/,$libs);
    my @i = split(/\,/,$insertSizes);
    my @s = split(/\,/,$stdDevs);

    my $nLibs = scalar(@l);
    for (my $l = 0; $l < $nLibs; $l++) {
	$libInfo{$l[$l]} = [$i[$l],$s[$l],$linkFile];
	push(@libs,$l[$l]);
    }
}
close L;

my @sortedLibs = sort {$libInfo{$a}->[0] <=> $libInfo{$b}->[0]} @libs;

my %linkFiles = ();
my @linkFileOrder = ();
foreach my $lib (@sortedLibs) {
    my $linkFile = $libInfo{$lib}->[2];
    unless (exists($linkFiles{$linkFile})) {
	push(@linkFileOrder,$linkFile);
	$linkFiles{$linkFile} = 1;
    }
}

my $nLinkFiles = scalar(@linkFileOrder);

my %onoObjectLengths = ();
my %onoObjectDepths = (); 
my %contigDepths = ();
my %depthHist = ();
my $peakDepth = undef;

my $maxInsertSize = $libInfo{$sortedLibs[-1]}->[0];
print STDERR "Maximum insert size: $maxInsertSize\n";
my $suspendable = $maxInsertSize/2;

my %oldCopyContigs = ();         # for $polymorphicMode 2 only;  list of contigs in the input srf that are copies of originals (copies made in previous rounds)
my %altVariantObjects = ();   # for $polymorphicMode 1 only;  list of haplotype variant contigs/scaffolds
my %objectsMarked = ();        # for $polymorphicMode 2 only;  objects qualified with one of %oblectLabels,

##################################################
# Different types of objects used in ono. (Relevant for $polymorphicMode only)   
# Depending on whether the inputs are contigs or scaffolds, different policies apply as to how the objects are used.

my %objectLabels = ();
$objectLabels{"UNMARKED"} = 0;
$objectLabels{"DD-TIG"} = 1;  # diploid-depth contig/singleton
$objectLabels{"SD-TIG"} = 3;  # single-depth ungenotyped  contig/singleton
$objectLabels{"TRUE-SCAF"} = 4;    # multi-contig scaffold;  asumed heterozygous even if it contains homozygous elements
$objectLabels{"COPY-TIG"} = 5;    # an object that has been copied or is a copy created duing DD-SD collision resolution
$objectLabels{"DIPLOTIG"} = 6;    # one of a pair of resolved contig-bubble-contig "haplopaths"

#################################################

if (defined($crfFile)) {

    print STDERR "Reading $crfFile...\n";

    open (C,$crfFile) || die "Couldn't open $crfFile\n";
    while (my $line = <C>) {
	chomp $line;
	my @cols = split(/\t/,$line);
	if ($#cols != 2) {
	    die "$crfFile  not in exlpected format\n";
	}
	my $contig= $cols[0];
	my $length = $cols[1];
	
	$onoObjectLengths{$contig} = $length;

	my $depth = $cols[2];
	$contigDepths{$contig} = $depth;
	
	if ($polymorphicMode) {
	    #add to the the depth histogram, ommiting haplotype variant contigs
	    unless ($depth < $diploidDepthCutoff) {
		my $roundedDepth = sprintf("%.0f",$depth);
		$depthHist{$roundedDepth} += $length;
	    }
	} else {
	    my $roundedDepth = sprintf("%.0f",$depth);
	    $depthHist{$roundedDepth} += $length;
	}

	if ($polymorphicMode == 1) {
	    # create a map of alternative variant diplotigs
	    # only consider alternative variants of _p2 diplotigs (since these are the ones whose linkage we'll be transforming later)
	    if ( $contig =~ /_p2/) {
		(my $altVarDiplotigName = $contig) =~ s/_p2/_p1/; 
		$altVariantObjects{$contig}[0] = $altVarDiplotigName;  # hash of arrays just to be consitent with the format used for scaffolds 
	    } 
	}
    
	if ($polymorphicMode == 2) {

	    if ($contig =~ /^diplotig/) {
		$objectsMarked{$contig} = $objectLabels{"DIPLOTIG"};
	    }
	    else {
		$objectsMarked{$contig} = $objectLabels{"UNMARKED"};  
	    }


	    # if ($contig =~ /^isotig/ ) { 
	    # 	if ($contigDepths{$contig} >= $diploidDepthCutoff) {  
	    # 	    $objectsMarked{$contig} = $objectLabels{"DD-TIG"};
	    # 	}
	    # 	else {
	    # 	    $objectsMarked{$contig} = $objectLabels{"SD-TIG"};
	    # 	}
	    # }
		# else {
	    # 	$objectsMarked{$contig} = $objectLabels{"DIPLOTIG"};
	    #}
	}
    }

    close C;

    %onoObjectDepths = %contigDepths;

    print STDERR "Done.\n";

}


# If an existing scaffold report exists, load scaffolding information
 
my %scaffReport = ();  # scaffoldName -> srfFileLines 

if (defined($srfFile)) {
    
    print STDERR "Reading scaffold report file: $srfFile...\n";
    open (S,$srfFile) || die "Couldn't open $srfFile\n";

    my %scaffRealBases = ();
    my %scaffDepthSum = ();
    my %scaffContigs = ();
    my %contigToScaffoldMap;

    while (my $line = <S>) {
        chomp $line;
        my @cols = split(/\t/,$line);
	if (scalar(@cols) < 4 || scalar(@cols) > 6) {
	    die "$srfFile  not in exlpected format:\n$line\n";
	}

	if (exists($scaffReport{$cols[0]})) {
	    $scaffReport{$cols[0]} .= "$line\n";
	} else {
	    $scaffReport{$cols[0]} = "$line\n";
	}

        if ($cols[1] =~ /^CONTIG/) {
            my ($scaffID,$contigID,$sStart,$sEnd) = @cols[0,2,3,4];
	    my ($contigOri, $contigName) = $contigID =~ /^([\+\-])(.+)$/;
	    push(@{$scaffContigs{$scaffID}}, $contigName);
	    
	    if ($polymorphicMode == 1) {
		# kepp track of contig's positional into wrt to the scaffold		
	        $contigToScaffoldMap{$contigName}=[$scaffID,$contigOri,$sStart,$sEnd];
	    }
	    my $depth = $cols[5];
	    my $cLen = $sEnd-$sStart+1;
	    $contigDepths{$contigName} = $depth; 
	    $scaffDepthSum{$scaffID} += $cLen*$depth;
	    $scaffRealBases{$scaffID} += $cLen;

	    if ($polymorphicMode) {
		#add to the the depth histogram, ommiting haplotype variant contigs
		unless ($depth < $diploidDepthCutoff) {
		    my $roundedDepth = sprintf("%.0f",$depth);
		    $depthHist{$roundedDepth} += $cLen;
		}
	    } else {
		my $roundedDepth = sprintf("%.0f",$depth);
		$depthHist{$roundedDepth} += $cLen;
	    }

            if (exists($onoObjectLengths{$scaffID})) {
                if ($sEnd > $onoObjectLengths{$scaffID}) {
                    $onoObjectLengths{$scaffID} = $sEnd;
                }
            } else {
                $onoObjectLengths{$scaffID} = $sEnd;
            }
	}
    }
    close S;

    while (my ($s,$d) = each(%scaffDepthSum)) {
	$onoObjectDepths{$s} = $d/$scaffRealBases{$s};
    }

    if ($polymorphicMode == 1) {
	# create a map of alternative variant scaffolds
	foreach (keys %contigToScaffoldMap) {
	    next unless ( $_ =~ /_p2/);  # only look for alternative variants of _p2-containing scaffolds (since these are the ones whose linkage we'll be transforming later)
	    my $scaffID = $contigToScaffoldMap{$_}[0];
	    # we assume that all _p2 diplotigs are in single-contig scaffolds, but we're not explicitly enforcing this
	    if ( ! defined $altVariantObjects{$scaffID} ) { 
		(my $altVarDiplotigName = $_) =~ s/_p2/_p1/ ;
		my @altVariantInfo = @{$contigToScaffoldMap{$altVarDiplotigName}};
		$altVariantObjects{$scaffID} = \@altVariantInfo;   #scaff containing p2 => scaff containing p1 plus p1's positional info
	    }
	}
    }


    if ($polymorphicMode == 2) {
	#classify the scaffolds
	foreach (keys %scaffContigs) {
	    if (scalar @{$scaffContigs{$_}} > 1) {
		$objectsMarked{$_} = $objectLabels{"TRUE-SCAF"}; 
            }
	    else {  #a singleton scaffold classified based on the contig it contains
		my $contig = $scaffContigs{$_}[0];
		if ($contig =~ /\.cp$/) {
		    $oldCopyContigs{$contig} = 1;
		    $objectsMarked{$_} = $objectLabels{"COPY-TIG"};
		}
		elsif ($contig =~ /^diplotig/) {
		    $objectsMarked{$_} = $objectLabels{"DIPLOTIG"};
		}
		else {
		    $objectsMarked{$_} = $objectLabels{"UNMARKED"};
		}
	    }
	}
    }

    print STDERR "Done.\n";
}


# determine the "peak" depth;  will be considered the modal depth for this genome, against which all objects will be compared
my $maxBases = 0;
while (my ($d,$b) = each(%depthHist)) {
    if ($b > $maxBases) {
	$peakDepth = $d;
	$maxBases = $b;
    }
}

if ($polymorphicMode == 2) {
  # test if the peak depth (ignoring variant isotigs)  is within 20% of expected diploid peak (peak = 2*$diploidDepthCutoff/1.5)
    my $expectDipDepthLow = 0.8 * (1.33 * $diploidDepthCutoff);
    my $expectDipDepthHigh = 1.2 * (1.33 * $diploidDepthCutoff);
    if ($peakDepth < $expectDipDepthLow || $peakDepth > $expectDipDepthHigh) {
	my $obsPeakD = $peakDepth;
	$peakDepth = $diploidDepthCutoff * 1.33;
	print STDERR "WARNING: observed modal homozygous depth ($obsPeakD) is too far off from the expected (2*diploidDepthCutoff/1.5), possibly due to lack of homozygous contigs.\n";
	print STDERR "         Will use an approximation..\n"; 
    }
}
 
print STDERR "Modal depth picked at $peakDepth.\n";


# Build up contig linkage table using linkFiles

my %links = ();
for (my $lf = 0; $lf < $nLinkFiles; $lf++) {
    my $linkFile = $linkFileOrder[$lf];
    print STDERR "Reading link file $linkFile...\n";

    open (L,$linkFile) || die "Couldn't open $linkFile\n";
    while (my $line = <L>) {
	chomp $line;
	my @cols = split(/\t/,$line);
	my $linkType = $cols[0];
	my $ends = $cols[1]; # /^(.+)\.([35])<=>(.+)\.([35])$/;    
	my @linkData = @cols[2..$#cols];


	if ( $polymorphicMode == 1 ) {
	    ## Here we simplify the graph by transforming any variant-2 linked ends into the corresponding variant-1.  
	    ## After this step all variant-2 objects will be unlinked, i.e. they will remain in singleton scaffolds

	    my @ends = split /<=>/, $ends;  

	    if (defined $crfFile) {
		# We're expecting diplotigs variant pairs in the input, which we assume are in the same orientation and of similar size. Here we simply substitute variant-1 names
		# for the variant-2 names in the original link, keeping the gap size estimate and orientation unchanged.
		foreach my $linkedEnd (@ends) {
		    my ($c, $end) = split /\./, $linkedEnd;
		    next unless (defined($altVariantObjects{$c}) );  # $c is a variant-2 object

		    my @altObj = @{$altVariantObjects{$c}};    # @altObj is the corresponding variant-1 object with related info
		    my $altObjName = $altObj[0];
#		    print STDERR "DEBUG: transformed $ends\ninto\n";
		    $ends =~ s/$c\./$altObjName\./;  
#		    print STDERR "$ends\n";
		}
	    }
	    elsif (defined($srfFile)) {  			
		# we're dealing w scaffolds, so we need to adjust the gap estimate based on the positions of the variant-1 contigs within the scaffold
		my $gapEstimate = $linkData[1];
		my $adjGapEstimate = $gapEstimate;
		foreach my $linkedEnd (@ends) {
		    my ($s, $end) = split /\./, $linkedEnd;
		    next unless (defined($altVariantObjects{$s}) );  # $s must be a variant-2 object

		    my @altObj = @{$altVariantObjects{$s}};    # @altObj is the corresponding variant-1 object with related info
		    my $altObjName = $altObj[0];
		    $ends =~ s/$s\./$altObjName\./;       # $ends now refers to the variant-1 object
		    my $cOri = $altObj[1];
		    my $cStart = $altObj[2];
		    my $cStop = $altObj[3];
		    my $cLen = $onoObjectLengths{$altObjName};

		    # adjust relative orientation and gap estimate; 
		    # treating splints and spans the same
		    if ($cOri eq "-") { 
			$end = ( $end eq "5" ? "3": "5" ) ;
			$ends =~ s/$altObjName\.([35])/$altObjName\.$end/; 
		    }
		    $adjGapEstimate = ( $end eq "5" ? ($adjGapEstimate-$cStart+1) : ($adjGapEstimate-($cLen-$cStop+1)));   #adjust gap estimate after each end is processed
		}
		if ( abs($adjGapEstimate) > abs($gapEstimate) ) {
		    # the adjusted gaps size in the transformed link cannot be larger than the original
		    print STDERR "Invalid gap size after link tansformation  ($adjGapEstimate, $gapEstimate); Omitting this link...\n";
		    print STDERR "\t$line  ......>\n";
		    print STDERR "\t$ends\t $adjGapEstimate\n";
		    next;
		} 
		# skip cases where both ends in a link got transposed to the same alt variant scaffold,  i.e., we don't need this link at all
		my ($o1, $o2) = $ends =~ /^(.+)\.[35]<=>(.+)\.[35]$/;
		if ($o1 eq $o2) {
#		    print STDERR "DEBUG: both ends in a link got transposed to the same alt variant scaffold;  Omitting this link...\n";
#		    print STDERR "\t$line  ......>\n";
#		    print STDERR "\t$ends\t $adjGapEstimate\n";
		    next;
		}
		$linkData[1] = $adjGapEstimate; 
	    }
	}
	my $linkInfo = join(":",$lf,$linkType,@linkData);

	if (exists($links{$ends})) {
	    push(@{$links{$ends}},$linkInfo);
	}
	elsif ( $polymorphicMode == 1 ) {
	    # since we've transformed some links into their alt variants 
	    # and can now have more than one link for the same two ends, 
	    # we check if the flip-flopped version of the link exists
	    my ($e1, $e2) = split(/<=>/, $ends);
	    my $endsFlipFlop = $e2."<=>".$e1;
	    if (exists($links{$endsFlipFlop})) {
		push(@{$links{$endsFlipFlop}},$linkInfo);
	    }
	    else {
		$links{$ends} = [$linkInfo];
	    }
	}
	else {
	    $links{$ends} = [$linkInfo];
	}
    }
    close L;

    print STDERR "Done.\n";
}


# Build ties between each pair of object ends consolidating splint/span links

my %endTies = ();
my $nTies = 0;

foreach my $linkedEnds (keys(%links)) {

    my @linkData = @{$links{$linkedEnds}};
    my @splints = ();
    my @spans = ();
    my $maxLikelihoodSplint = undef;
    my $maxSplintCount = 0;
    my $minUncertaintySpan = undef;
    my $minSpanUncertainty = 100000;

    my $blessedLink = 0;
    if (defined($blessedLinkFile)) {
	my ($s1,$s2) = $linkedEnds =~ /^(.+)\.[35]<=>(.+).[35]$/;
	my $bless = ($s1 lt $s2) ? "$s1.$s2" : "$s2.$s1";
	if (exists($blessedLinks{$bless})) {
	    $blessedLink = 1;
	    print STDERR "BLESSED: $linkedEnds\n";
	}
    }

    foreach my $link (@linkData) {
	my ($linkFile,$linkType,@linkInfo) = split(/:/,$link);
	if ($linkType eq "SPLINT") {
	    
	    my ($splintCounts,$gapEstimate) = @linkInfo;
	    my ($nUsedSplints,$nAnomalousSplints,$nAllSplints) = $splintCounts =~ /^(\d+)\|(\d+)\|(\d+)$/;
	    if ( ($nUsedSplints >= $pairThreshold) || $blessedLink) {
		my $splint = "$linkFile:$nUsedSplints:$gapEstimate";
		push(@splints,$splint);
		unless (defined($maxLikelihoodSplint)) {
		    $maxLikelihoodSplint = $splint;
		    $maxSplintCount = $nUsedSplints;
		}
		if ($nUsedSplints > $maxSplintCount) {
		    $maxLikelihoodSplint = $splint;
		    $maxSplintCount = $nUsedSplints;
		}
	    }

	} elsif ($linkType eq "SPAN") {

	    my ($spanCounts,$gapEstimate,$gapUncertainty) = @linkInfo;
	    my ($nAnomalousSpans,$nUsedSpans) = $spanCounts =~ /^(\d+)\|(\d+)$/;
	    if (($nUsedSpans >= $pairThreshold) || $blessedLink) {
		my $span = "$linkFile:$nUsedSpans:$gapEstimate:$gapUncertainty";
		push(@spans,$span);
		unless (defined($minUncertaintySpan)) {
		    $minUncertaintySpan = $span;
		    $minSpanUncertainty = $gapUncertainty;
		}
		if ($gapUncertainty < $minSpanUncertainty) {
		    $minUncertaintySpan = $span;
		    $minSpanUncertainty = $gapUncertainty;
		}
	    }

	} else {
	    die "Invalid linkType [$linkType] in link: $link\n";
	}
    }

    my $minimumGapSize = -($merSize-2);

    my $nSplintGap = undef;
    my $splintGapEstimate = undef;
    my $maxSplintError = 2;
    my $splintAnomaly = 0;
    if (defined($maxLikelihoodSplint)) {
	my ($l0,$n0,$g0) = split(/:/,$maxLikelihoodSplint);
	$splintGapEstimate = ($g0 < $minimumGapSize) ? $minimumGapSize : $g0;
	$nSplintGap = $n0;
	foreach my $splint (@splints) {
	    my ($l,$n,$g) = split(/:/,$splint);
	    my $splintGapDiff = abs($g-$splintGapEstimate);
	    if ($splintGapDiff > $maxSplintError) {
		$splintAnomaly = 1;
		warn "Warn: splint anomaly for $linkedEnds ($l,$n,$g) not consistent with ($l0,$n0,$g0) [$splintGapEstimate]\n" if ($verbose);
	    }
	}
    }

    my $nSpanGap = undef;
    my $spanGapEstimate = undef;
    my $spanGapUncertainty = undef;
    my $maxSpanZ = 3;
    my $spanAnomaly = 0;
    if (defined($minUncertaintySpan)) {
	my ($l0,$n0,$g0,$u0) = split(/:/,$minUncertaintySpan);
	$spanGapEstimate = ($g0 < $minimumGapSize) ? $minimumGapSize : $g0;
	$spanGapUncertainty = ($u0 < 1) ? 1 : $u0;
	$nSpanGap = $n0;
	foreach my $span (@spans) {
	    my ($l,$n,$g,$u) = split(/:/,$span);
	    my $spanGapZ = abs($g-$spanGapEstimate)/($u+1);
	    if ($spanGapZ > $maxSpanZ) {
		$spanAnomaly = 1;
		warn "Warn: span anomaly for $linkedEnds ($l,$n,$g,$u) not consistent with ($l0,$n0,$g0,$u0) [$spanGapEstimate:$spanGapUncertainty]\n" if ($verbose);
	    }
	}
    }

    my $gapEstimate = undef;
    my $gapUncertainty = undef;
    my $nGapLinks = undef;

    if (defined($splintGapEstimate)) {
	$gapEstimate = $splintGapEstimate;
	$gapUncertainty = 1;
	$nGapLinks = $nSplintGap;
    } elsif (defined($spanGapEstimate)) {
	$gapEstimate = $spanGapEstimate;
	$gapUncertainty = $spanGapUncertainty;
	$nGapLinks = $nSpanGap;
    } else {
	next;
    }

    if (defined($spanGapEstimate) && defined($splintGapEstimate)) {
	my $gapZ = ($splintGapEstimate-$spanGapEstimate)/$spanGapUncertainty;
	if (abs($gapZ) > $maxSpanZ) {
	    warn "Warn: splint/span anomaly for $linkedEnds ($nSplintGap:$splintGapEstimate) not consistent with ($nSpanGap:$spanGapEstimate:$spanGapUncertainty)\n" if ($verbose);
	}
    }

    my ($end1,$end2) = $linkedEnds =~ /(.+)\<\=\>(.+)/;
    
    
    if (exists($endTies{$end1})) {
	push (@{$endTies{$end1}},"$nGapLinks.$gapEstimate:$gapUncertainty.$end2");
    } else {
	$endTies{$end1} = ["$nGapLinks.$gapEstimate:$gapUncertainty.$end2"];
    }
    
    if (exists($endTies{$end2})) {
	push (@{$endTies{$end2}},"$nGapLinks.$gapEstimate:$gapUncertainty.$end1");
    } else {
	$endTies{$end2} = ["$nGapLinks.$gapEstimate:$gapUncertainty.$end1"];
    }

    $nTies++;
}


print STDERR "$nTies ties made.\n";

my @sortedByLen = sort {($onoObjectLengths{$b} <=> $onoObjectLengths{$a}) || ($onoObjectDepths{$b} <=> $onoObjectDepths{$a})} (keys(%onoObjectLengths));

my %endLabels = ();
$endLabels{"UNMARKED"} = 0;
$endLabels{"SELF_TIE"} = 1;
$endLabels{"TIE_COLLISION"} = 2;
$endLabels{"DEPTH_DROP"} = 3;
$endLabels{"NO_TIES"} = 4;
$endLabels{"TIE_SPLIT_DD"} = 5;

my %endMarks = ();
my %tieSplitsDD = ();

print STDERR "Marking ends...\n";

foreach my $piece (@sortedByLen) {
    my $end = "$piece.5";
    my $endMark = markEnd($end);
    $endMarks{$end} = $endLabels{$endMark};
    
    $end = "$piece.3";
    $endMark = markEnd($end);
    $endMarks{$end} = $endLabels{$endMark};
}

print STDERR "Done marking ends.\n";

my %suspended = ();
my %bestTie = ();
my %bestTiedBy = ();
my @disambiguatedEnds;

print STDERR "Finding best ties...\n";

# Initially only unmarked ends are allowed to have best ties. 

foreach my $piece (@sortedByLen) {
    print STDERR "\n === $piece ===\n";
    if (exists($suspended{$piece})) {
	print STDERR "\nBEST_TIE: (skipping suspended)\t$piece\t$suspended{$piece}\n\n";
	next;
    }
    my $end = "$piece.5";
    my $endMark = $endMarks{$end};
    my $bestTie = $endMark;
    unless ($endMark) {
	$bestTie = bestTie($end);
	$bestTie{$end} = $bestTie;

	if ($bestTie) {
	    if (exists($bestTiedBy{$bestTie})) {
		$bestTiedBy{$bestTie} .= ",$end";
	    } else {
		$bestTiedBy{$bestTie} = $end;
	    }
	}
    }
    print STDERR "BEST_TIE: [$bestTie]\t$end\n\n";

    $end = "$piece.3";
    $endMark = $endMarks{$end};
    $bestTie = $endMark;
    unless ($endMark) {
	$bestTie = bestTie($end);
	$bestTie{$end} = $bestTie;

	if ($bestTie) {
	    if (exists($bestTiedBy{$bestTie})) {
		$bestTiedBy{$bestTie} .= ",$end";
	    } else {
		$bestTiedBy{$bestTie} .= $end;
	    }
	}
    }

    print STDERR "BEST_TIE: [$bestTie]\t$end\n\n";
}

# re-build the objects array if the hash has changed, i.e. some copy-objects got added during bestTie() 
if  ( scalar(@sortedByLen) < scalar(keys %onoObjectLengths) ){
    @sortedByLen = sort {($onoObjectLengths{$b} <=> $onoObjectLengths{$a}) || ($onoObjectDepths{$b} <=> $onoObjectDepths{$a})} (keys(%onoObjectLengths));
}

# re-try finding best ties on those ends that have been skipped but which ended up being tie targets and which had their collision conflicts resolved
if ($polymorphicMode == 2 && (scalar (@disambiguatedEnds))  ) {
    print STDERR "\n\nRetrying best ties on disambiguated ends...\n";

    foreach $end (@disambiguatedEnds) {
	print STDERR "\n=== disambiguated end $end ===\n";
	($piece) = $end =~ /^(.+)\.[35]$/;
	if (exists($suspended{$piece})) {
	    print STDERR "BEST_TIE: (suspended)\t$piece\t$suspended{$piece}\n";
	    next;
	}
	if (exists($bestTie{$end})) {
	    print STDERR "BEST_TIE: (pre-existing) [$bestTie{$end}]\t$end\n";
            next;
	}
	$bestTie = bestTie($end);
	$bestTie{$end} = $bestTie;
	if ($bestTie) {
	    if (exists($bestTiedBy{$bestTie})) {
		$bestTiedBy{$bestTie} .= ",$end";
	    } else {
		$bestTiedBy{$bestTie} = $end;
	    }
	    print STDERR  "BEST_TIE: [$bestTie]\t$end\n";
	}
    }
}

print STDERR "Done finding best ties\n";

# Place suspended where possible 

print STDERR "Inserting suspensions...\n";

my $nSuspendedPieces = keys(%suspended);

my $nInsertedSuspensions = 0;
foreach my $piece (@sortedByLen) {
    unless (exists($suspended{$piece})) {
	next;
    }

    my $end5 = "$piece.5";
    my $endMark5 = $endMarks{$end5};
    my $end3 = "$piece.3";
    my $endMark3 = $endMarks{$end3};

    if ( $endMark3 == 4  ||  $endMark5 == 4  ) {
	# NO_TIES
	print STDERR "UNINSERTED: ($endMark5) $piece ($endMark3)\n";
	next;
    }

    my @ties5 = ();
    if (exists($endTies{$end5})) {
	my @tieInfo = @{$endTies{$end5}};
	foreach my $t (@tieInfo) {
	    my ($nLinks,$gapEstimate,$gapUncertainty,$tiedEnd) = $t =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;
	    push(@ties5,$tiedEnd);
	}   
    } else {
	print STDERR "UNINSERTED: (NO_5_END_TIES) $piece ()\n";
	next;
    }
    my %ties3 = ();
    if (exists($endTies{$end3})) {
	my @tieInfo = @{$endTies{$end3}};
	foreach my $t (@tieInfo) {
	    my ($nLinks,$gapEstimate,$gapUncertainty,$tiedEnd) = $t =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;
	    $ties3{$tiedEnd} = 1;
	}   
    } else {
	print STDERR "UNINSERTED: () $piece (NO_3_END_TIES)\n";
	next;
    }

#    if ( exists $bestTie{$end5} || exists $bestTie{$end3})
#    {
#	print STDERR "UNINSERTED: () $piece (CONFLICTING_BEST_TIES)\n";
#	next;
#    }

    # buld a list of "triangles" where a 5' end's tie  has a bestTie that's also one of the  3' end's ties
    my %check = ();
    foreach my $t5 (@ties5) {
	if (exists($bestTie{$t5}) && exists($ties3{$bestTie{$t5}})) {
	    $check{$t5} = $bestTie{$t5};
	}
    }

    my @v5 = ();
    my @v3 = ();
    my $nValidSuspensions = 0;
    while (my ($t5,$t3) = each(%check)) {
	if (mutualUniqueBest($t5,$t3)) {
	    push(@v5,$t5);
	    push(@v3,$t3);
	    $nValidSuspensions++;
	}
    }

    if ($nValidSuspensions == 1) {
	# remove this piece from %suspended and insert it into the graph by updating links in %bestTie and %bestTiedby

	my $tie5 = $v5[0];
	my $tie3 = $v3[0];

	delete($suspended{$piece});

	if ( ! exists $bestTie{"$piece.5"})   { 	    # This is a crude workaround to a potentially buggy scenario.  Find a better solution!
	    $bestTie{"$piece.5"} = $tie5;
	    $bestTiedBy{$tie5} = "$piece.5";
	}
	else {
	    warn "WARNING: Suspended end $piece.5 already has a best tie outside of the suspension \"triangle\" (".$bestTie{"$piece.5"}.").  Will keep existing best tie...\n ";
	}
	$bestTie{$tie5} = "$piece.5";
	if (exists $bestTiedBy{"$piece.5"}) {
	    $bestTiedBy{"$piece.5"} .= ",$tie5";
	} else {
	    $bestTiedBy{"$piece.5"} = $tie5;
	}


	if ( ! exists $bestTie{"$piece.3"})   { 	    # This is a crude workaround to a potentially buggy scenario.  Find a better solution!
	    $bestTie{"$piece.3"} = $tie3;
	    $bestTiedBy{$tie3} = "$piece.3";
	}
	else {
	    warn "WARNING: Suspended end $piece.3 already has a best tie outside of the suspension \"triangle\" (".$bestTie{"$piece.3"}.").  Will keep existing best tie...\n ";
	}

	$bestTie{$tie3} = "$piece.3";
        if (exists $bestTiedBy{"$piece.3"}) {
	    $bestTiedBy{"$piece.3"} .= ",$tie3";
	} else {
	    $bestTiedBy{"$piece.3"} = $tie3;
	}

	$nInsertedSuspensions++;
	print STDERR "SUSPENDED-INSERTED:  $tie5\t$piece\t$tie3\n";
    
    } else {
	print STDERR "UNINSERTED: (N_VALID_SUSP: $nValidSuspensions) $piece ()\n";
    }
}

print STDERR "Done.\n";
print STDERR "Total suspended pieces: $nSuspendedPieces\n";
print STDERR "Total inserted suspensions: $nInsertedSuspensions\n";


# Lock ends together with no competing ties
my $nBT = scalar(keys %bestTie);
my $nBTB = scalar(keys %bestTiedBy);


print STDERR "\nTotal nr of best ties: $nBT\n";
print STDERR "Total nr of best-tied-by's : $nBTB\n\n";


print STDERR "Locking ends with no competing ties..\n";
my %endLocks = ();

while (my ($end,$ties) = each (%bestTiedBy)) {
    if (exists($endLocks{$end})) {
	next;
    } 
    my ($piece) = $end =~ /^(.+)\.[35]$/;
    if (exists($suspended{$piece})) {
	print STDERR "DEBUG: \t $piece is suspended... skipping\n" if ($verbose);
	next;
    }

    my @ties = split(/\,/,$ties);
    my $nTies = scalar(@ties);

    my $bestTie = "NONE";
    if (exists($bestTie{$end})) {
	$bestTie = $bestTie{$end};
    } else {
	print STDERR "DEBUG: \t $end  has no best ties.. skipping\n" if ($verbose);
	next;
    }
    print STDERR "\nDEBUG: $end ... best-tied-by: $ties\n" if ($verbose);

    my $existsReciprocalBT = 0;
#    if (($nTies == 1) && ($ties[0] eq $bestTie)) {
    foreach my $btb (@ties) {
	if ($btb eq $bestTie) {
	    $existsReciprocalBT =1;
	    last;
	}
    }
    if ($existsReciprocalBT) {
	my $tieInfo = getTieInfo($end,$bestTie);
	my ($nLinks,$gapEstimate,$gapUncertainty,$nextEnd) = $tieInfo =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;
	my $gap = sprintf("%.0f",$gapEstimate);
	$endLocks{$end} = "$bestTie><$gap:$gapUncertainty";
	print STDERR " DEBUG: locked end $end w. $endLocks{$end}\n "if ($verbose);
	$endLocks{$bestTie} = "$end><$gap:$gapUncertainty";
	print STDERR " DEBUG: locked bestTie $bestTie w. $endLocks{$bestTie}\n "if ($verbose);
    } else {
	print STDERR "DIVERGENCE:  $end, $ties\n ";
	$endLocks{$end} = "DIVERGENCE";

    }
}

foreach my $end (keys(%endLocks)) {
    print STDERR "LOCK: $end $endLocks{$end}\n";
}

# Traverse end locks to build scaffolds

my $scaffoldId = 1;
my %visitedObjects = ();

if ( $polymorphicMode == 2 ) {  # wite a list of contigIDs that are deemed to be diploid, along with the containing scaffold
    open(MASK, ">p${pairThreshold}.mask.out") || die "Can't open p${pairThreshold}.mask for writing!";
}


while (my ($object,$objectLen) = each (%onoObjectLengths)) {

    my $startObject = $object;
    my $startEnd = 5;

    if (exists($visitedObjects{$object})) {
	next;
    }


    # walk back up the lock end chain until we reach termination|divergence|convergence
    my $preState = "TERMINATION";
    my %loopCheck = ();
    while () {
	if (exists($loopCheck{$startObject})) {
	    last;
	}
	$loopCheck{$startObject} = 1;
	
	my $end = "$startObject.$startEnd";
	if (exists($endLocks{$end})) {
	    my $next = $endLocks{$end};
	    if (($next eq "CONVERGENCE") || ($next eq "DIVERGENCE")) {
		$preState = $next;
		last;
	    } else {
		($startObject,$startEnd) = $next =~ /(.+)\.([35])\>\</;
		$startEnd = ($startEnd == 3) ? 5 : 3;
		$preState = "EXTENSION";
	    }
	} else {
	    $preState = "TERMINATION";
	    last;
	}
    }
    
    %loopCheck = ();
    
    print STDERR "Scaffold$scaffoldId: $preState $startObject ($startEnd) ";

    my $inScaffold = 0;
    my $nextObject = $startObject;
    my $nextEnd = ($startEnd == 3) ? 5 : 3;
    my $nextGap = 0;
    my $nextGapUncertainty = 0;
    my $prevObject = $nextObject;
    my $prevEnd = $nextEnd;
    
    my $newScaffReport = "";
    my $scaffCoord = 1;
    my $scaffContigIndex = 1;

    my $maskList;  # (for polymorphicMode 2 only) list of DD-tigs that were incorporated into a larger scaffold   OR copy-tigs.  Will be masked out in later ono rounds (i.e. it won't contribute to any new ties)
    my $toMask;

    while () {
	if (exists($loopCheck{$nextObject})) {
	    print STDERR "$nextObject LOOP\n";
	    last;
	}
	$loopCheck{$nextObject} = 1;	    
	$visitedObjects{$nextObject} = 1;
	my $nextObjectLen = $onoObjectLengths{$nextObject};
	
	my $objectOri = "+";
	if ($nextEnd == 5) {
	    $objectOri = "-";
	}
	
	if ($inScaffold) {
	    $newScaffReport .= "Scaffold$scaffoldId\tGAP$scaffContigIndex\t$nextGap\t$nextGapUncertainty\n";
	    $scaffCoord += $nextGap;
	    $scaffContigIndex++;
	}

	if ($polymorphicMode == 2) {
	    $toMask = 0;
	    if  ($objectsMarked{$nextObject} == $objectLabels{"DD-TIG"} || $objectsMarked{$nextObject} == $objectLabels{"COPY-TIG"})  {
		# if object is a DD-tig or a COPY-tig, prepare to add the underlying contig to the mask  
		$toMask = 1;
	    }
	}
	
	if (defined($srfFile)) {  # objects are scaffolds from previous srf

	    my $oldReport = $scaffReport{$nextObject};

	    my @reportLines = split(/\n/,$oldReport);
	    if ($objectOri eq "-") {
		@reportLines = reverse(@reportLines);
	    }
	    foreach my $line (@reportLines) {
		my @cols = split(/\t/,$line);
		if ($cols[1] =~ /^CONTIG/) {
		    my ($originalContig,$p0,$p1,$depth) = @cols[2,3,4,5];
		    my $originalContigLength = $p1-$p0+1;
		    my ($originalContigOri,$originalContigName) = $originalContig =~ /^([\+\-])(.+)$/;

		    if ( $toMask) { $maskList .= "$originalContigName\n" }

		    if ($objectOri eq $originalContigOri) {
			$originalContigOri = "+";
		    } else {
			$originalContigOri = "-";
		    }

		    my $contigStartCoord = $scaffCoord;
		    my $contigEndCoord = $scaffCoord + $originalContigLength - 1;
#		    my $depth = $contigDepths{$originalContigName};
		    $newScaffReport .= "Scaffold$scaffoldId\tCONTIG$scaffContigIndex\t$originalContigOri$originalContigName\t$contigStartCoord\t$contigEndCoord\t$depth\n";
		    $scaffCoord += $originalContigLength;

		} else {   # a GAP

		    my ($originalGapSize,$originalGapUncertainty) = @cols[2,3];
		    $newScaffReport .= "Scaffold$scaffoldId\tGAP$scaffContigIndex\t$originalGapSize\t$originalGapUncertainty\n";
		    $scaffCoord += $originalGapSize;
		    $scaffContigIndex++;
		}
	    }

	} else {  # objects are contigs
	    my $contigOri = $objectOri;
	    my $nextContig = $nextObject;
	    my $contigStartCoord = $scaffCoord;
	    my $contigEndCoord = $scaffCoord + $nextObjectLen - 1;
	    my $depth = $onoObjectDepths{$nextObject};

	    if ($toMask){ $maskList .= "$nextContig\n" }

	    $newScaffReport .= "Scaffold$scaffoldId\tCONTIG$scaffContigIndex\t$contigOri$nextContig\t$contigStartCoord\t$contigEndCoord\t$depth\n";
	    $scaffCoord += $nextObjectLen;
	}

	$inScaffold = 1;

	
	my $end = "$nextObject.$nextEnd";
	if (exists($endLocks{$end})) {
	    my $next = $endLocks{$end};
	    if (($next eq "CONVERGENCE") || ($next eq "DIVERGENCE")) {
		print STDERR "$nextObject ($nextEnd) $next\n";
		if ($polymorphicMode == 2 && defined($maskList)) {
		    unless ( ($scaffContigIndex == 1) && ($objectsMarked{$nextObject} == 1) ) {
			# don't print the mask if the DD-tig object din't get incorporated into a larger scaffold, 
			# i.e. we will still consider it's conflicting links in the next round  
			print MASK $maskList;
		    }
		}
		last;

	    } else {
		print STDERR "$nextObject ($nextEnd) ";
		($prevObject,$prevEnd) = ($nextObject,$nextEnd);
		($nextObject,$nextEnd,$nextGap,$nextGapUncertainty) = $next =~ /(.+)\.([35])\>\<(.+)\:(.+)/;
		print STDERR "[$nextGap +/- $nextGapUncertainty] $nextObject ($nextEnd) ";
		$nextEnd = ($nextEnd == 3) ? 5 : 3;
	    }
	} else {
	    print STDERR "$nextObject ($nextEnd) TERMINATION\n";
	    if ($polymorphicMode == 2 && defined($maskList)) {
		unless ( ($scaffContigIndex == 1) && ($objectsMarked{$nextObject} == 1) ) {
		    # don't print the mask if the DD-tig object din't get incorporated into a larger scaffold, 
		    # i.e. we will still consider it's conflicting links in the next round  
		    print MASK $maskList;
		}
	    }
	    last;
	}
    }
    
    print $newScaffReport;
    $scaffoldId++;
    
}

# ---------------
# | SUBROUTINES |
# ---------------

sub mutualUniqueBest {
    my ($end1,$end2) = @_;

    my ($piece1) = $end1 =~ /^(.+)\.[35]$/;
    my ($piece2) = $end2 =~ /^(.+)\.[35]$/;
    if (exists($suspended{$piece1}) || exists($suspended{$piece2})) {
	return 0;
    }

    unless (exists($bestTie{$end1}) && exists($bestTie{$end2})) {
	return 0;
    }

    my $bestTie1 = $bestTie{$end1};
    my $bestTie2 = $bestTie{$end2};

    unless (($bestTie1 eq $end2) && ($bestTie2 eq $end1)) {
	return 0;
    }

    unless (exists($bestTiedBy{$end1}) && exists($bestTiedBy{$end2})) {
	return 0;
    }

    my @ties1 = split(/\,/,$bestTiedBy{$end1});
    my $nTies1 = scalar(@ties1);
    my @ties2 = split(/\,/,$bestTiedBy{$end2});
    my $nTies2 = scalar(@ties2);

    unless ( ($nTies1 == 1) && ($ties1[0] eq $end2) && ($nTies2 == 1) && ($ties2[0] eq $end1)) {
	return 0;
    }

    return 1;
}



sub bestTie {
    my ($end) = @_;
    print STDERR "End $end:  testing tied ends \n" if ($verbose);
    my @ties = ();
    if (exists($endTies{$end})) {
	@ties = @{$endTies{$end}};
    } else {
	return 0;
    }

    my ($testObject) = $end =~ /^(.+)\.[35]$/; 
    my $testObjectLen = $onoObjectLengths{$testObject};

    my $largeObject = 0;
    if ($testObjectLen > $suspendable) {
	$largeObject = 1;
    }

    my $nTies = scalar(@ties);
    my @tiedEnds = ();
    my @gapEstimates = ();
    my @gapUncertainties = ();
    my @objectLens = ();

    for (my $i = 0; $i < $nTies; $i++) {
	my $tie_i = $ties[$i];
	my ($nLinks_i,$gapEstimate_i,$gapUncertainty_i,$tiedEnd_i) = $tie_i =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;
	my $endMark_i = $endMarks{$tiedEnd_i};
	# Skip ties to previously marked ends  unless they're split DD-SD type ends - these we will try to resolve
	if ($endMark_i > 0 && $endMark_i != 5) { 
	    next;
	}
	my ($object_i) = $tiedEnd_i =~ /^(.+)\.[35]$/;
	# Skip ties to suspended objects
	if (exists $suspended{$object_i}){
	    next;
	}
 
	print STDERR "\tgood tie: $tie_i\n" if ($verbose);
	my $objectLen_i = $onoObjectLengths{$object_i};

	push(@tiedEnds,$tiedEnd_i);
	push(@gapEstimates,$gapEstimate_i);
	push(@gapUncertainties,$gapUncertainty_i);
	push(@objectLens,$objectLen_i);
    }

    my $nGoodTies = scalar(@tiedEnds);

    unless($nGoodTies > 0) {
	print STDERR "NoGoodTies\n";
	return 0;
    }

    my @sortedTieIndex = sort {($gapEstimates[$a] <=> $gapEstimates[$b]) || ($objectLens[$a] <=> $objectLens[$b])} (0..$#tiedEnds);

    # If a large object, return the closest large object if one exists
    # small objects and, if diploid-2, those with DD-SD collisions may be suspended between large objects
   
    if ($largeObject) {       
	print STDERR "\tLarge object. \n" if ($verbose);
	my $closestLarge = undef;
	my @testSuspended = ();
	for (my $i=0;$i<$nGoodTies;$i++) {
	    my $ti = $sortedTieIndex[$i];
	    my $tiedEnd_i = $tiedEnds[$ti];
	    my $objectLen_i = $objectLens[$ti];
	    print STDERR "\t\t $tiedEnd_i:" if ($verbose);
	    if ($objectLen_i > $suspendable  && !$endMarks{$tiedEnd_i} ){ 
		print STDERR "\t\t.. is closest large\n" if ($verbose);		
		$closestLarge = $tiedEnd_i;
		last;
	    } else {
		my ($sObject) = $tiedEnd_i =~ /^(.+)\.[35]$/; 
		push(@testSuspended,$sObject);
		if ($verbose) {
		    if ($endMarks{$tiedEnd_i}) {
			print STDERR "\t\t.. suspendable  (closest end marked ".$endMarks{$tiedEnd_i}.")\n";
		    } else {
			print STDERR "\t\t.. suspendable  (not large)\n";
		    }
		}
	    }
	}
	
	if (defined($closestLarge)) {
	    my $otherEnd = $closestLarge;
	    my $nSus = process_suspend_candidates(\%suspended, \@testSuspended, $end, $otherEnd);
	    print STDERR "\tSuspended objects: @testSuspended\n" if ($nSus);

	    print STDERR "\tBT return: closestLarge ($nSus suspended)\n";
	    return $closestLarge;	    
	}
	print STDERR "\tNo closest large found.. \n" if ($verbose);
    }

    # Return the closest extendable object if one exists.  Objects must be unmarked to qualify.
    # unextendable objects may be suspended

    my $closestExtendable = undef;
    @testSuspended = ();
    for (my $i=0;$i<$nGoodTies;$i++) {
    	my $ti = $sortedTieIndex[$i];
    	my $tiedEnd_i = $tiedEnds[$ti];
    	my ($object_i,$endSuffix_i) = $tiedEnd_i =~ /^(.+)\.([35])$/; 
    	my $otherEndSuffix_i = ($endSuffix_i eq "3") ? "5" : "3";
    	my $testOtherEnd_i = "$object_i.$otherEndSuffix_i";

    	print STDERR "\t\t $tiedEnd_i:" if ($verbose);
    	if ($endMarks{$tiedEnd_i} || $endMarks{$testOtherEnd_i}) {
	    # Note for diploid_mode 2: objects with a DD_TIE_SPLIT are a high risk for haplotype crossover, even after the split is resolved.
	    # We suspend these, but the suspensions will often remain unplaced if  the DD resolution on one end doesn't agree with the ties on the other.
	    print STDERR "\t\t.. suspendable (  ends are marked [$endMarks{$tiedEnd_i}, $endMarks{$testOtherEnd_i}])\n" if ($verbose);
	    push(@testSuspended,$object_i);
	    next;
    	}
	else {
    	    $closestExtendable = $tiedEnd_i;
    	    print STDERR "\t\t.. is closest extendable\n" if ($verbose);
    	    last;
    	}
    }
    
    if (defined($closestExtendable)) {
	my $otherEnd = $closestExtendable;
    	my $nSus = process_suspend_candidates(\%suspended, \@testSuspended, $end, $otherEnd);
    	print STDERR "\tSuspended objects: @testSuspended\n"  if ($nSus);

    	print STDERR "\tBT return: closestExtendable ($nSus suspended)\n";
    	return $closestExtendable;	
    }
    print STDERR "\tNo closest extendable found.. \n" if ($verbose);
 

    # Just return the closest object

    my $closestEnd  = $tiedEnds[$sortedTieIndex[0]];
    @testSuspended = ();
    print STDERR "\t\t$closestEnd .. is closest \n" if ($verbose);

    my ($closestObject,$endSuffix) = $closestEnd =~ /^(.+)\.([35])$/;
    my $otherEndSuffix = ($endSuffix eq "3") ? "5" : "3";
    my $testEnd = "$closestObject.$otherEndSuffix";
    if ($polymorphicMode == 2) {
	if ($endMarks{$testEnd} == 5) {
	    # reject if other end is a DD-SD tie split (we already know we can't suspend it)
	    print STDERR "\t\t.. REJECT: other end ($testEnd) is marked as DD-SD tie collision! \n" if ($verbose);
	    return 0;
	}
	elsif ( $objectsMarked{$closestObject} == 1 && $endMarks{$closestEnd} == 5) { 
	    # DD-SD tie split: try to resolve it
	    # Note: the DD-split may be wrt other nodes, in which case this collision won't be resolved, but we still don't want to make BTs to this end  
	    unless (resolve_DD_collisions($end, $closestObject, undef, undef, \%endTies, \%endMarks, \%onoObjectLengths, \%onoObjectDepths, \%scaffReport, \%objectsMarked) ) {
	    	print STDERR "\t\t\t Couldn't resolve DD-SD tie collision on $closestEnd - REJECT! \n" if ($verbose);
	    	return 0;
	    }
	}
    }
    print STDERR "\tBT return: closest\n";
    return $tiedEnds[$sortedTieIndex[0]];
}

sub getTieInfo {
    my ($end1,$end2) = @_;
    
    my @ties = ();
    if (exists($endTies{$end1})) {
	@ties = @{$endTies{$end1}};
    } else {
	return 0;
    }

    my $nTies = scalar(@ties);

    for (my $i = 0; $i < $nTies; $i++) {
	my $tie_i = $ties[$i];
	my ($nLinks_i,$gapEstimate_i,$gapUncertainty_i,$tiedEnd_i) = $tie_i =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;

	if ($tiedEnd_i eq $end2) {
	    return $tie_i;
	}
    }

    return 0;
}

sub isSplinted {
    my ($end1,$end2) = @_;

    my $linkedEnds = ($end1 lt $end2) ? "$end1<=>$end2" : "$end2<=>$end1";
    if (exists($links{$linkedEnds})) {
	my @linkData = @{$links{$linkedEnds}};
	foreach my $link (@linkData) {
	    my ($linkFile,$linkType,@linkInfo) = split(/:/,$link);
	    if ($linkType eq "SPLINT") {
		return 1;
	    }
	}
    }

    return 0;
}



sub isLinkedTo {
    my ($end1,$end2) = @_;
    
    my @ties = ();
    if (exists($endTies{$end1})) {
	@ties = @{$endTies{$end1}};
    } else {
	return 0;
    }

    my $nTies = scalar(@ties);

    for (my $i = 0; $i < $nTies; $i++) {
	my $tie_i = $ties[$i];
	my ($nLinks_i,$gapEstimate_i,$gapUncertainty_i,$tiedEnd_i) = $tie_i =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;

	if ($tiedEnd_i eq $end2) {
	    return 1;
	}
    }

    return 0;
}



sub markEnd {
    my ($end) = @_;
    my ($testObject) = $end =~ /^(.+)\.[35]$/; 
    my @ties = ();
    if (exists($endTies{$end})) {
	@ties = @{$endTies{$end}};
    } else {
	return "NO_TIES";
    }

    my $testObjectDepth = $onoObjectDepths{$testObject};

    my $nTies = scalar(@ties);
    my @tiedEnds = ();
    my @gapEstimates = ();
    my @gapUncertainties = ();
    my @objectLens = ();
    my $nCollisions = 0;
    my $maxStrain = 3;
    my $maxDepthDropoff = 0.8;  # used for marking ends of objects whose depth is significantly higher than that of any of its tied ends


    # evaluate all ties and mark THIS END if any anomalies are found

    for (my $i = 0; $i < $nTies; $i++) {
	my $tie_i = $ties[$i];
	my ($nLinks_i,$gapEstimate_i,$gapUncertainty_i,$tiedEnd_i) = $tie_i =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;
	my ($object_i) = $tiedEnd_i =~ /^(.+)\.[35]$/; 

	if ($object_i eq $testObject) {
	    print STDERR "SELF_TIE: $object_i <=> $testObject !!!\n" if ($verbose);
	    return "SELF_TIE";
	}

	my $objectDepth_i = $onoObjectDepths{$object_i};
	if ( (($testObjectDepth - $objectDepth_i)/$peakDepth) > $maxDepthDropoff) {    
	    print STDERR "DEPTH_DROP:  $object_i ($objectDepth_i) <=> $testObject ($testObjectDepth)\n" if ($verbose);

	    return "DEPTH_DROP";
	}

	my $objectLen_i = $onoObjectLengths{$object_i};

	push(@tiedEnds,$tiedEnd_i);
	push(@gapEstimates,$gapEstimate_i);
	push(@gapUncertainties,$gapUncertainty_i);
	push(@objectLens,$objectLen_i);
    }

    my @sortedTieIndex = sort {($gapEstimates[$a] <=> $gapEstimates[$b]) || ($objectLens[$a] <=> $objectLens[$b])} (0..$#ties);

    for (my $i=0;$i<$nTies-1;$i++) {   
	next if ($nTies < 2);
	my $ti = $sortedTieIndex[$i];	
	my $start_i = $gapEstimates[$ti];
	my $end_i = $start_i+$objectLens[$ti]-1;
	my $uncertainty_i = $gapUncertainties[$ti];

	my $tj = $sortedTieIndex[$i+1];
	my $start_j = $gapEstimates[$tj];
	my $end_j = $start_j+$objectLens[$tj]-1;
	my $uncertainty_j = $gapUncertainties[$tj];

	my $overlap = $end_i - $start_j + 1;
	if ($overlap > $merSize-2) {
	    my $excessOverlap = $overlap-($merSize-2);
	    my $strain = $excessOverlap / ($uncertainty_i+$uncertainty_j);
	    if ($strain > $maxStrain) {   
		print STDERR "DEBUG:  $tiedEnds[$ti] ~~ $tiedEnds[$tj]; OVERLAP: $overlap bp (strain: $strain,  uncert_i: $uncertainty_i, uncert_j: $uncertainty_j)\n " if ($verbose);
		# tie confilct - mark appropriately
		my $tiedEnd_i = $tiedEnds[$ti];
		my ($object_i,$objectEnd_i) = $tiedEnd_i =~ /^(.+)\.([35])$/;
		my $testEnd_i = $object_i;
		$testEnd_i .= ($objectEnd_i eq "3") ? ".5" : ".3";
		my $tiedEnd_j = $tiedEnds[$tj];
		my ($object_j,$objectEnd_j) = $tiedEnd_j =~ /^(.+)\.([35])$/;
		my $testEnd_j = $tiedEnd_j;

		my $isSplinted = isSplinted($testEnd_i,$testEnd_j);

		if ($isSplinted) {
		    print STDERR "Tie collision averted by SPLINT: $end -> $ties[$ti] ? $ties[$tj] \n" if $verbose;
		    return "UNMARKED";

		} elsif ( $polymorphicMode == 2 && is_dd_collision($testObject, $object_i, $object_j, \%onoObjectDepths)) { 

		    # If the depth ratio between the object and the conflicting ties agrees with the diploid depth model, this type of tie collision will get addressed separately
		    # (there could be other conflicts stemming from this object, but we only register the first one we encounter, and only if it's the closest one )
		    unless ($objectsMarked{$testObject}) {
			$objectsMarked{$testObject} = $objectLabels{"DD-TIG"};}
		    unless ($objectsMarked{$object_i}) {
			$objectsMarked{$object_i} = $objectLabels{"SD-TIG"};}
		    unless ($objectsMarked{$object_j}) {
			$objectsMarked{$object_j} = $objectLabels{"SD-TIG"};}


		    push ( @{$tieSplitsDD{$end}}, ($ties[$ti], $ties[$tj]));		    
		    
		    print STDERR "TIE_SPLIT_DD: $end -> $ties[$ti],  $end -> $ties[$tj]\n";
		    return "TIE_SPLIT_DD";
 
		} else {
		    print STDERR "TIE_COLLISION: $end -> $ties[$ti] ? $ties[$tj]\n";
		    return "TIE_COLLISION";
		}
	    }
	}
    }

    return "UNMARKED";

}


sub resolve_DD_collisions
{

    # 1. If the object has either end marked as TIE_SPLIT_DD  (mark 5), resolve the colisions by creating a copy of this object and assigning to it the conflicitng ties that are NOT with the "anchor ends".
    # 2. Update tie info with new ties
    # 3. Update %onoObjectLengths with a new copy-object entry

    my ($leftAnchorEnd, $object, $rightAnchorEnd, $suspendCandidatesAR, $endTiesHR, $endMarksHR, $onoObjectLengthsHR, $objectDepthsHR, $scaffReportHR, $objectsMarkedHR ) = @_; 
    # if the object is not a suspension candidate it could be anchored only at one end

    if (! defined($rightAnchorEnd)) {
	$rightAnchorEnd = "";
    }
    my $nConflictsResolved=0;

    my $end5 = "$object.5";
    my $end3 = "$object.3";

    # Define a copy of the current object.
    my $copyObject = "${object}.cp";    

    foreach my $end ($end5, $end3) {  
	my @tiesToReassign=();
	my $nTiesToKeep=0;

	next unless (exists $tieSplitsDD{$end});

	print STDERR "(resolve_DD_collisions):\tConflicting ties of end $end: @{$tieSplitsDD{$end}}\n" if ($verbose);

	my $copyEnd;
	($copyEnd = $end) =~ s/^$object/$copyObject/;

	foreach my $tie (@{$tieSplitsDD{$end}}) {
	    $tie =~ /\d+\.\-?\d+\:\d+\.(.+)$/;
	    my $tieEnd = $1;
	    my ($tiedObject) = $tieEnd =~ /^(.+)\.[35]$/; 
	    # keep any ties to this contig's anchors or to any other suspension candidates
	    if ($tieEnd eq $leftAnchorEnd || $tieEnd eq $rightAnchorEnd ) {
		$nTiesToKeep++;
	    }
	    elsif (defined $suspendCandidatesAR && $tiedObject ~~ @$suspendCandidatesAR) {
		$nTiesToKeep++;
	    }
	    else {
		push (@tiesToReassign, $tie);
	    }   
	}
	# for this to be a valid split, at least one tie should belong here and at least one should be reassigned
	unless ($nTiesToKeep && scalar(@tiesToReassign)) {
	    print STDERR "(resolve_DD_collisions): \tIgnoring tie conflict at end $end.\n" if ($verbose);
	    next ;
	}
	foreach my $tie (@tiesToReassign) {

	    # re-assign this tie to the copy-object
	    push (@{$endTiesHR->{$copyEnd}}, $tie);
	    print STDERR "(resolve_DD_collisions): \t\t\t tie $tie reassigned to $copyEnd..\n"  if ($verbose);	    

	    # remove this tie from this end's ties hash
	    my @tiesUpdated = grep  ! /$tie/, @{$endTiesHR->{$end}};
	    $endTiesHR->{$end} = \@tiesUpdated;

	    # Correct the target end of the new tie so that it points back to the copyEnd
	    $tie =~ /^\d+\.\-?\d+:\d+\.(.+\.[35])/ ;
	    foreach (@{$endTiesHR->{$1}}) {
		$_ =~ s/$object\./$copyObject\./;
	    }
	}
	$nConflictsResolved++;
	delete $tieSplitsDD{$end};

	# Since we've resolved the collision we will later re-attempt finding the end's best ties 
	push (@disambiguatedEnds, ($end, $copyEnd));
    }

    unless ($nConflictsResolved > 0) {
	print STDERR "(resolve_DD_collisions): \t No conflicts could be resolved.\n"  if ($verbose);
	return 0;
    }   

    unless (exists $onoObjectLengthsHR->{$copyObject} && exists $objectDepthsHR->{$copyObject}) {

	# add a new record to the original scaff report to reflect the copy
	if (defined($srfFile)) {
	    my $parentScafRecord = $scaffReportHR->{$object};
	    my @scaffoldLines = split(/\n/,$parentScafRecord);
	    my @newScaffoldLines;
	    foreach (@scaffoldLines) {
		my @cols = split(/\t/,$_);
		if ( $cols[1] =~ /^CONTIG/) {
		    $cols[0] .= "-CP";   #old scaff id
		    $cols[2] .= ".cp";   #contig name
		    (my $copyContigName) = $cols[2] =~ /[\+\-](.+)/;
		    if (exists $oldCopyContigs{$copyContigName}) {

			# This should never happen because we prevent contigs from which copies have been already made from carying any links (via mask fed to splinter/spanner)
			# but this is a safety net just in case.
			
			warn "\n\tWARNING: A copy of at least one of this object's underlying contigs already exists! Can't copy object! Undoing the tie reassignments..\n";
			# trash the copy-ends we just created and undo the tie changes
			foreach my $copyEnd ("$copyObject.5", "$copyObject.3")  {
			    next unless exists $endTiesHR->{$copyEnd};

			    foreach (@{$endTiesHR->{$copyEnd}}) {
				$_ =~ /^\d+\.\-?\d+:\d+\.(.+\.[35])/ ;
				foreach (@{$endTiesHR->{$1}}) {
				    $_ =~ s/$copyObject\./$object\./;
				}
			    }
			    delete $endTiesHR->{$copyEnd};
			    my $i = 0; $i++ until $disambiguatedEnds[$i] eq $copyEnd;
			    splice(@disambiguatedEnds, $i-1, 2);  #take out $end & $copyEnd
			}
			return 0;
		    }
		}
		push(@newScaffoldLines, (join("\t",@cols)) );
	    }   
	    $scaffReportHR->{$copyObject} = (join("\n",@newScaffoldLines));
	}
	
	$onoObjectLengthsHR->{$copyObject} = $onoObjectLengthsHR->{$object};
	$objectDepthsHR->{$object} /= 2;  # split the depth
	$objectDepthsHR->{$copyObject} = $objectDepthsHR->{$object};
	$objectsMarkedHR->{$object} = $objectLabels{"COPY-TIG"};
	$objectsMarkedHR->{$copyObject} = $objectLabels{"COPY-TIG"};
	$endMarksHR->{"$copyObject.3"} = 0;
	$endMarksHR->{"$copyObject.5"} = 0;



	print STDERR "New copy-object created: $copyObject\n";
    }    
    print STDERR "DEBUG (resolve_DD_collisions): $nConflictsResolved DD-SD tie splits resolved\n" if ($verbose);

    return 1;
}


sub process_suspend_candidates
{
    my ($allSuspendedHR, $suspendCandidatesAR, $end, $otherEnd)  = @_;
  
    my $nSus=0;
    foreach my $sObject (@{$suspendCandidatesAR}) {
	if ($polymorphicMode == 2 && $objectsMarked{$sObject} == 1) {
	    my $hasDDcollisions =0;
	    foreach my $sObjectEnd ("$sObject.5", "$sObject.3") {
		$hasDDcollisions = 1 if ($endMarks{$sObjectEnd} == 5);
	    }
	    if ( $hasDDcollisions ) {
		# Check both ends of suspended object for cases of DD-SD tie split and resolve them by creating a copy-object and re-assigning the alternative ties to it.  
		# If the collisions couldn't be resolved or no colisions were found,  don't suspend this object
		if ( resolve_DD_collisions($end, $sObject, $otherEnd, $suspendCandidatesAR, \%endTies, \%endMarks, \%onoObjectLengths, \%onoObjectDepths, \%scaffReport, \%objectsMarked) ){ 
		    $$allSuspendedHR{$sObject} =  1;
		    $nSus++;
		}
		else {
		    print STDERR "DEBUG (process_suspend_candidates) Couln't resolve DD-SD tie collisions on one of the ends of $sObject\n" if ($verbose);
		}
	    }
	}
	else {
	    $$allSuspendedHR{$sObject} =  1;
	    $nSus++;	    
	}
    }
    return $nSus;
}

sub is_dd_collision
{
    my ($testObject, $object_i, $object_j, $objectDepthsHR) = @_;

    if ($objectsMarked{$testObject} > 0 && $objectsMarked{$testObject} != $objectLabels{"DD-TIG"}) {  # if object is already labeled and is deemed to be anything other than a diploid-depth contig we don't go any further
	return 0;
    } 

    my $d1 = $objectDepthsHR->{$testObject};
    my $d2i = $objectDepthsHR->{$object_i};
    my $d2j = $objectDepthsHR->{$object_j};
    
    my $reptDepthRatio = 2;

    if ($d1 > $peakDepth*$reptDepthRatio) { #avoid likely repeat boundaries
	return 0;
    }
    if ( ( $d1 >=  $peakDepth/2  #use lax criteria here: suspect homozygous as long as it's larger than the expected avg het depth
	   && $d2i < $peakDepth  && $d2j < $peakDepth    #suspect variants if both smaller than the expected homozygous depth
	   && $d1/$d2i >= 1.8 && $d1/$d2j >= 1.8   #depth ratio close to 2:1
	   && $d1/$d2i <= 2.2 && $d1/$d2j <= 2.2
	 )
	 ||
	 ( $d1 >= $diploidDepthCutoff  # under these conditions we don't require a strict 2:1 depth ratio but make a leap of faith
	   &&
#	   ($objectsMarked{$object_i} == $objectLabels{"TRUE-SCAF"} && $objectsMarked{$object_j} == $objectLabels{"TRUE-SCAF"})  #if scaffolds, assume heterozygous
#	   ||
	   ($objectsMarked{$object_i} == $objectLabels{"DIPLOTIG"}  &&  $objectsMarked{$object_j} == $objectLabels{"DIPLOTIG"}) #if diplotigs, assuem heterozous
	 )
       ) {
	printf STDERR ("[is_dd_collision] %.1f -> %.1f, %.1f )\n", $d1, $d2i, $d2j) if ($verbose);		    
	return 1;
    }
    return 0;
}
