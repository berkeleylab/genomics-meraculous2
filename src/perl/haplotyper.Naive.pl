#!/usr/bin/env perl 
#
# haplophaser.pl


use warnings;
use strict;

use Getopt::Std;
#use Data::Dumper;

my %opts = ();
my $validLine = getopts('s:c:V', \%opts);
my @required = ("s","c");
my $nRequired = 0;
map {$nRequired += exists($opts{$_})} @required;
$validLine &= ($nRequired == @required);
if ($validLine != 1) {
    print "Usage: ./haplotyper.pl  <-s srfFile> <-c clonesMapFile> [ -V(erbose)] \n";
    exit;
}

my $srfFile = $opts{"s"};
my $clonesMapFile = $opts{"c"};
my $verbose = $opts{"V"};
my $min_clones = 1;


my $srfOutFile = $srfFile.".htyped";

my %contigsClonesHoH = ();  # diplotigName -> hash of clones that mapped to it


open(MAP,$clonesMapFile) || die "Can't open $clonesMapFile for reading";
while (my $line = <MAP>) {
    chomp $line;
    my @fields = split(/\t/,$line);
    my $c = $fields[0];
    my @clones =  split(/\,/,$fields[1]);
    for my $clone (@clones) {
	$contigsClonesHoH{$c}{$clone} = 1;  #hash of hashes for quick searching
    }
}
close MAP;

open (OUT,">$srfOutFile") || die "Couldn't open $srfOutFile for writing\n";

print STDERR "Reading scaffold report file: $srfFile...\n";
open (S,$srfFile) || die "Couldn't open $srfFile\n";


# Scaffolds are divided into three groups which will later get merged and re-sorted.
my %variant1SR;  # verified/fixed variant1 scaffolds 
my %variant2SR;  # single-diplotig variant2 scaffolds  (could contain _p1 or _p2 diplotigs)
my %tempVariant2SR;  # initial single-diplotig variant2  scaffolds ( contain only _p2 diplotigs)
my %invariantSR;  # haploid scaffolds (made up of isotigs only)

my @scaffoldDiplotigs;
my %convertedVariant2contigs;  #list of diplotigs that participated in a _p1 -> _p2 swap
my $scaffName;
my $scaffoldRecord;

while (my $line = <S>) {
    chomp $line;
    my @cols = split(/\t/,$line);

    if (! defined $scaffoldRecord) {  #first line
	@scaffoldDiplotigs = ();
	$scaffoldRecord = "$line\n";
	$scaffName = $cols[0]; 
    }
    elsif ( $cols[0] eq $scaffName  ) {  # same scaffold; continue adding to it
	$scaffoldRecord .= "$line\n";
    } 
    else { 	# new scaffold, update everything for the previous scaffold and reset
	$scaffName =~ /^Scaffold(\d+)/;
	my $sNum = $1;
	if ( scalar(@scaffoldDiplotigs) > 1 ) {
	    print STDERR "\n$scaffName:  multiple variant diplotigs, need to phase to a consistent haplotype...\n";
	    $variant1SR{$sNum} = phase_haplotypes($scaffoldRecord, \@scaffoldDiplotigs, \%contigsClonesHoH);
	    if (! defined $variant1SR{$sNum}) { die "Error: Could't haplotype all diplotigs in $scaffName\n"; }
	}		
	elsif ( scalar(@scaffoldDiplotigs) == 1 && $scaffoldDiplotigs[0] =~ /_p1/ ) {
	    $variant1SR{$sNum} = $scaffoldRecord;
	}
	elsif ( scalar(@scaffoldDiplotigs) == 1 && $scaffoldDiplotigs[0] =~ /_p2/ ) {
	    $tempVariant2SR{$sNum} = $scaffoldRecord;
	}
	else {
	    $invariantSR{$sNum} = $scaffoldRecord;
	}

	@scaffoldDiplotigs = (); 
	$scaffName = $cols[0]; 
	$scaffoldRecord = "$line\n";
    }

    if ( ($cols[1] =~ /^CONTIG/) && ($cols[2] =~ /diplotig\d+/)) {
	my ($contigName) = $cols[2] =~ /^[\+\-](.+)$/;
	push (@scaffoldDiplotigs, $contigName);
    }
}
# last scaffold read in is still unprocessed, so we do it here
$scaffName =~ /^Scaffold(\d+)/;
my $sNum = $1;
if ( scalar(@scaffoldDiplotigs) > 1 ) {
    print STDERR "\n$scaffName:  multiple variant diplotigs, need to phase to a consistent haplotype...\n";
    $variant1SR{$sNum} = phase_haplotypes($scaffoldRecord, \@scaffoldDiplotigs, \%contigsClonesHoH);
    if (! defined $variant1SR{$sNum}) { die "Error: Could't haplotype all diplotigs in $scaffName\n"; }
}		
elsif ( scalar(@scaffoldDiplotigs) == 1 && $scaffoldDiplotigs[0] =~ /_p1/ ) {
    $variant1SR{$sNum} = $scaffoldRecord;
}
elsif ( scalar(@scaffoldDiplotigs) == 1 && $scaffoldDiplotigs[0] =~ /_p2/ ) {
    $tempVariant2SR{$sNum} = $scaffoldRecord;
}
else {
    $invariantSR{$sNum} = $scaffoldRecord;
}

close S;


# when building variant-1 scaffolds we've swapped some _p2 diplotigs for _p1, so now we need to do the opposite swap in the corresponding variant-2 scaffolds 
for my $scaf (keys %tempVariant2SR) {
    my @cols = split(/\t/,$tempVariant2SR{$scaf});
    my ($ori, $contigName) = $cols[2] =~ /^([\+\-])(.+)$/;
    if (exists($convertedVariant2contigs{$contigName})) {
	my $altVarContigName = convert_to_alt_var_name($contigName);
	$cols[2] = $ori.$altVarContigName;
	$variant2SR{$scaf} = join("\t",@cols);
    }
    else {
	$variant2SR{$scaf} = $tempVariant2SR{$scaf};
    }
}

my %combinedSR = (%variant1SR, %variant2SR, %invariantSR);
for my $key (sort {$a<=>$b} keys %combinedSR ) {
    print OUT "$combinedSR{$key}";
} 
close OUT;


sub phase_haplotypes {
    # Enforce a consistent haplotype across all diplotigs in the scaffold
    # We do this via recursive binning of diplotigs based on common clones.  Since the clones were mapped
    # to haplotype-specific parts of the diplotigs, this ensures that any two diplotigs sharing the same clone must be from the same haplotype.
    my ($scaffReportEntry, $scaffoldDiplotigsAR, $contigsClonesHoHR ) = @_;
    
    my @haploGroup;  # a bin of diplotigs assigned to the "main" haplotype.  The alternative variant diplotigs of this set implicitly make up the other haplotype.
    my @scaffoldDiplotigs = @$scaffoldDiplotigsAR;
    my @altVarDiplotigs;
    for my $dVar1 (@scaffoldDiplotigs) {
	my $dVar2 = convert_to_alt_var_name($dVar1);
	push (@altVarDiplotigs, $dVar2);
    }
    # seed the group; 
    push(@haploGroup, $scaffoldDiplotigs[0]);
    for (my $n=1; $n <= $#scaffoldDiplotigs; $n++) {
	    # check this diplotig and its alt variant for linkage to groups 1 &2
	    next if ( $scaffoldDiplotigs[$n] ~~ @haploGroup || $altVarDiplotigs[$n] ~~ @haploGroup ); 		# already placed
	    my $blessedDiplotig = bless_variant($n, $scaffoldDiplotigs[$n], $altVarDiplotigs[$n], $contigsClonesHoHR, \@haploGroup);
	    if (defined($blessedDiplotig)) {
		unshift(@haploGroup, $blessedDiplotig);
		print STDERR "\tBlessed $blessedDiplotig into haplo-group 1.\n";
	    }
	    else {
		print STDERR "\t\tCouldn't haplotype diplotig pair $scaffoldDiplotigs[$n] & $altVarDiplotigs[$n] by clone linkage...\n";
		$blessedDiplotig = bless_through_dMax($scaffoldDiplotigs[$n], $altVarDiplotigs[$n], $contigsClonesHoHR);
		unshift(@haploGroup, $blessedDiplotig);
		print STDERR "\tBlessed $blessedDiplotig into haplo-group 1 by majority depth .\n";
	    }
    }
    print STDERR "Done.\n\n";

    unless ( scalar(@haploGroup) == scalar(@scaffoldDiplotigs) ) { 
	return undef;
    }
    
    # update the scaffold record so that it matches the variant-1 haplo-group content. If a diplotig is not in the haplo-group, we assume 
    # its alt. variant must be so we swap them 
    my $newScaffReportEntry = $scaffReportEntry;
    for (my $i=0; $i <= $#scaffoldDiplotigs; $i++) {
	unless ($scaffoldDiplotigs[$i] ~~ @haploGroup) {
	    $newScaffReportEntry =~ s/$scaffoldDiplotigs[$i]\t/$altVarDiplotigs[$i]\t/;
	    # record any swapped-in _p2 diplotigs so that we can update the variant-2 scaffolds later
	    $convertedVariant2contigs{$altVarDiplotigs[$i]} = 1;
	}
    }
    return $newScaffReportEntry;
}

sub bless_variant {
    # Check each variant diplotig from the pair against the diplotigs in the haplo-group and return the one with at least $min_clones common clones
    # If both diplotig are linking to the haplo-group, add them to the @suspendedIdx array and return undef
    my ($n, $d1, $d2, $contigsClonesHoHR, $haploGroupAR, $suspendedIdxAR) = @_;
    my $cloneCnt_d1 = 0;
    my $cloneCnt_d2 = 0;
    my $linkageFound =0;

    print STDERR "DEBUG: $d1 | $d2 ...\n";
    for my $blessedContig (@$haploGroupAR) {
	# check every clone mapped to $d1 for presence in the previously blessed diplotig
	for my $clone (keys %{ $contigsClonesHoHR->{$d1}}) {
	    if (exists( $contigsClonesHoHR->{$blessedContig}{$clone})) {
                # common clone found
		print STDERR "DEBUG: \t$d1, $blessedContig:   common clone $clone\n";
		$linkageFound=1;
                $cloneCnt_d1++;
		last if ($cloneCnt_d1 >= $min_clones);
            }
	}
	for my $clone (keys %{ $contigsClonesHoHR->{$d2}}) {
	    if (exists( $contigsClonesHoHR->{$blessedContig}{$clone})) {
		print STDERR "DEBUG: \t$d2, $blessedContig:   common clone $clone\n";
                # common clone found
		$linkageFound=1;
                $cloneCnt_d2++;
		last if ($cloneCnt_d2 >= $min_clones);
            }
	}
	if ( $linkageFound ) { last; }
    }
    unless ($linkageFound) {
	return undef;
    }

    my $blessed;
    if (($cloneCnt_d1 >= $min_clones) && ($cloneCnt_d2 >= $min_clones)) {
	print STDERR "\tWarning: ambiguous haplotype linkage  ($d1: $cloneCnt_d1 linking clones;  $d2: $cloneCnt_d2 linking clones).\n";
    }
    $blessed = ($cloneCnt_d1 >= $cloneCnt_d2) ? $d1 : $d2;
    return $blessed;
}

sub bless_through_dMax {
    # assign to haplo-group based strictly on clone depth 
    my ($d1, $d2, $contigsClonesHoHR) = @_;

    my $depth_d1 = scalar(keys %{ $contigsClonesHoHR->{$d1}});
    my $depth_d2 = scalar(keys %{ $contigsClonesHoHR->{$d2}});

    my $blessed = ($depth_d1 >= $depth_d2) ? $d1 : $d2;
    print STDERR "\tBlessed $blessed based on majority clone depth.\n";
    return $blessed;
}

sub convert_to_alt_var_name {
    # This is a generic function to convert between diplotig varian name into its alternative variant
    # Swap in new conversion rules as needed
    my $name = shift;
    my $newname;
    if ($name =~ /_p1/) { ($newname = $name) =~ s/_p1/_p2/ }
    elsif ($name =~ /_p2/) { ($newname = $name) =~ s/_p2/_p1/ }
    return $newname;
}
