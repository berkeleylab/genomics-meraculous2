#!/usr/bin/env perl 
#
# bubbleScout.pl 


use warnings;
use strict;

use Getopt::Std;


use Text::Wrap;
$Text::Wrap::columns = 50;



my %opts = ();
my $validLine = getopts('c:f:o:V', \%opts);
my @required = ("c","f");
my $nRequired = 0;
map {$nRequired += exists($opts{$_})} @required;
$validLine &= ($nRequired == @required);
if ($validLine != 1) {
    print "Usage: ./bubbleScout.pl <-c ceaFile> <-f fastaFile> [ -o outPrefix -V(erbose)] \n";
    exit;
}

my $ceaFile = $opts{"c"};
my $fastaFile = $opts{"f"};
my $outPref = $opts{"o"};
my $verbose = $opts{"V"};


$outPref = "" unless ( defined($outPref));

my $merLength = undef;
my $hairLength = undef;

my %tipInfo = ();     # map from mer-tip pair to all contigs they flank



print STDERR "Reading contig end data from $ceaFile...\n";

open (F,$ceaFile) || die "Couldn't open $ceaFile\n";

my $bubbleDataFile = $outPref."bubbleData";
open(BDATA, ">$bubbleDataFile") || die "Couldn't open file $bubbleDataFile for writing";

while (my $line = <F>) {
    chomp $line;
    my ($contig,$prevCode,$prevBase,$firstMer,$contigLength,$lastMer,$nextBase,$nextCode) = split(/\s+/,$line);
    unless (defined($merLength)) {
	$merLength = length($firstMer);
	$hairLength = 2*$merLength - 1;
    }

    ($prevCode) = $prevCode =~ /\[(..)\]/;
    ($nextCode) = $nextCode =~ /\[(..)\]/;
    ($prevBase) = $prevBase =~ /\((.)\)/;
    ($nextBase) = $nextBase =~ /\((.)\)/;

    my $prevMer = $prevBase . $firstMer;
    $prevMer = substr($prevMer,0,$merLength);

    my $nextMer = $lastMer . $nextBase; 
    $nextMer = substr($nextMer,1,$merLength);
    
    # hair and sub-hair must be FU-UF or UF-FU

    if ($contigLength <= $hairLength) {
	unless ( ( ($prevCode =~ /F[ACGT]/) && ($nextCode =~ /[ACGT]F/) ) || ( ($prevCode =~ /[ACGT]F/) && ($nextCode =~ /F[ACGT]/) ) ) {
	    next;
	}
    } 

    if (($prevCode =~ /F[ACGT]/) || ($nextCode =~ /[ACGT]F/)) {

	my $extensions = "";
	if ($prevCode =~ /F[ACGT]/) {
	    $extensions = "$prevMer.";
	} else {
# I think this is a bug
#	    $extensions = "0";    
# should be
	    $extensions = "0.";    
	}
	if ($nextCode =~ /[ACGT]F/) {
	    $extensions .= "$nextMer";
	} else {
	    $extensions .= "0";
	}

	print BDATA "LINKER $contig $extensions\n";

   } elsif (($prevCode =~ /[ACGT]F/) && ($nextCode =~ /F[ACGT]/)) {

	my $tips = $prevMer . "." . $nextMer;
	my $rcTips = reverse($tips);
	$rcTips =~ tr/ACGT/TGCA/;

	my $rcFlag = "+";
	my $tipKey = $tips;
	if ($rcTips lt $tips) {
	    $tipKey = $rcTips;
	    $rcFlag = "-";
	}
    
	if (exists($tipInfo{$tipKey})) {
	    $tipInfo{$tipKey} .= ",$rcFlag$contig";
	} else {
	    $tipInfo{$tipKey} = "$rcFlag$contig";
	}

    } else {

    }

}
close F;


print STDERR "Done.\n";



# store contig sequence 

print STDERR "Reading contig sequence from $fastaFile...\n";

my %contigSequences = ();
my $currentEntry = undef;
my $currentSeq = "";
open (F,$fastaFile) || die "Couldn't open $fastaFile\n";
while (my $line = <F>) {
    chomp $line;
    if ($line =~ /^>/) {
        if (defined($currentEntry)) {
	    $contigSequences{$currentEntry} = $currentSeq;
        }
        $currentSeq = "";
        ($currentEntry) = $line =~ /^>(.+)$/;
    } else {
        $currentSeq .= $line;
    }
}
if (defined($currentEntry)) {
    $contigSequences{$currentEntry} = $currentSeq;
}
close F;

print STDERR "Done.\n";



my $bubbleID = 0;

# find bubbles from convergent tips

print STDERR "Finding bubbles...\n";

while (my ($tipKey,$contigs) = each(%tipInfo)) {

    my @contigs = split(/\,/,$contigs);
    if ( scalar(@contigs) == 2 ) {        # a valid diploid bubble

	$bubbleID++;
	print BDATA "BUBBLE\t$bubbleID\t$tipKey\t$contigs\n";

	foreach my $contigID (@contigs) {
	    my ($contig) = $contigID =~ /^[\+\-](.+)/;
	    print ">$contig\n".wrap('','',$contigSequences{$contig})."\n";
	}
    }
}
close BDATA;

print STDERR "Done.\n";

print STDERR "Total bubbles: $bubbleID\n";



