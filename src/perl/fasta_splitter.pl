#!/usr/bin/env perl
#script by Eugene Goltsman <egoltsman@lbl.gov> 



$n_args = @ARGV;
if ($n_args != 2) {
    print "Subdivide a fasta file into subfiles of <entries> entries with roughly equal sequence size\n";
    print "Usage: ./fasta_splitter.pl <fasta file> <entries>\n";
    exit;
}

open(IN,$ARGV[0]) || die "Couldn't open file $ARGV[0]\n";
my $n_entries = $ARGV[1];
my @names = ();
my %seq = ();
my $name = '';

while (<IN>)
{
    chomp;
        if (/^>(.*)$/) { $name = $1; push @names, $name; next; }
    $seq{$name} .= $_;
}
close IN;


my $n_seq = scalar keys %seq;
my $total_sequence_size=0;
foreach my $name (keys %seq)
{
    $total_sequence_size += length $seq{$name};
}
print STDERR "$n_seq sequences, $total_sequence_size bp\n";

my $i = 0;
my $written = 0;

for (my $chunk = 1; $chunk <= $n_entries; $chunk++)
{
    my $should_write = sprintf("%.0f", $total_sequence_size * $chunk / $n_entries );
    $out_file = $ARGV[0] . "." . ($chunk - 1);

    open(OUT,'>',$out_file) or die "Error: Can't create file \"$out_file\"\n";
        while ($written < $should_write)
        {
            print OUT '>', $names[$i], "\n";
            while ($seq{$names[$i]} =~ /(.{1,60})/g)
            {
                print OUT $1, "\n";
            }
            $written += length($seq{$names[$i]}); 
            $i++;
        }
    close OUT;
}

