#define CATCH_CONFIG_MAIN

#include <boost/lexical_cast.hpp>
#include "catch.hpp"
#include "LV.hpp"


TEST_CASE( "LV extensions are computed", "[LV]" ) {
    LandauVishkin lv;
    int numTestCases = 13; // Must update this manually!
    int MISMATCH_PENALTY = -2;
    int K = 3;
    const char* testValues[] = {
	// pattern, text, edit dist (as string), pAlignLen, tAlignLen

	// test simple exact matches
	"AAAAA", "AAAAA", "0", "5", "5",
	"AAAA", "AAAA", "0", "4", "4",
	"A", "A", "0", "1", "1",
	"AAA", "AAA", "0", "3", "3",

	//allow overhangs
	"AAAAA", "A", "0", "1", "1",

	//single mismatch
	"AAAAA", "ACAAA", "1", "5", "5",
	//double mismatch
	"AAAAAAAAAA", "ACAAACAAAAA", "2", "10", "10",

	// triple gap in p
	"AAGTTGGCC", "AACCGGTTGGCC", "3", "9", "12",
	"AACCGGTTGGCC", "AAGTTGGCC", "3", "12", "9",
	"AACCGGTTGGCCCCCCCCCCCCCCCC", "AAGTTGGCC", "3", "12", "9", // extra overhang ignored in pattern

	//single gap in p
	"ACACGACGA", "ACGACGACG", "1", "8", "9",

	// gap + mismatch
	"ACACGATGACG", "ACGACGACGACG", "2", "11", "12",
	 
	// misc
	"TCCGGAACCGG", "TAACCGGATCCGG", "3", "11", "13",
	"AAAAAA", "AAACCC", "0", "3", "3",  // pick the short alignment instead

	"CGTCAAAA", "AAAAAAAA", "-1", "0", "0", // 4 errors > k==3, no alignment
	"AAAAAAAAAAAAAA", "AACCCCCCCCCCCCCCCC", "-1", "0", "0",
    };	  
     	
    // do each test case twice, flipping pattern and text 
    for ( int testI = 0; testI < numTestCases; testI++ ) 
    {
		int i = testI*5; // 5 elements per test
		
		const char *p = testValues[i];
		const char *t = testValues[i+1];
		int editDistance = boost::lexical_cast<int>(testValues[i+2]);
		int pAlignLength = boost::lexical_cast<int>(testValues[i+3]);
		int tAlignLength = boost::lexical_cast<int>(testValues[i+4]);
		
		// run test
		int pAlignLengthResult = 0;
		int tAlignLengthResult = 0;
	    int editDistanceResult = lv.computeEditDistance( p, strlen(p), t, strlen(t), K, 
			MISMATCH_PENALTY, &pAlignLengthResult, &tAlignLengthResult );

		// check result
		char section[ 1000 ];
		sprintf( section, "testI: %d  input of p: %s t: %s gives result: [e: %d pALen: "
			"%d tALen: %d] instead of [e: %d pALen: %d tALen: %d",
			testI, p, t, editDistanceResult, pAlignLengthResult, tAlignLengthResult, editDistance,
			pAlignLength, tAlignLength );
	 	
	 	SECTION( section ) 
		{
	    	REQUIRE( editDistanceResult == editDistance );
			REQUIRE( pAlignLengthResult == pAlignLength );
			REQUIRE( tAlignLengthResult == tAlignLength );
		}
    }
}
