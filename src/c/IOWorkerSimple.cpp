#include "IOWorkerSimple.hpp"
#include <boost/date_time.hpp>
#include <boost/thread/thread.hpp>
#include <boost/unordered_map.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <boost/thread/locks.hpp>
#include <boost/smart_ptr/detail/spinlock.hpp>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;


int sleepUs = 0;

void printTime()
{
	time_t rawtime;
	struct tm *timeinfo;
	char buffer [200];
	time( &rawtime );
	timeinfo = localtime( &rawtime );
	strftime (buffer,200,"%c",timeinfo);
	cerr << buffer << "\n";
}

void IOWorkerSimple::init( const char *inputDescriptorFile_, int numThreads_ )
{
	numThreads = numThreads_;
	inputDescriptorFile = inputDescriptorFile_;

	vector< InputFileDescriptor > vFd;
	char dummy[ 1000 ];
	ifstream fdIn( inputDescriptorFile );
	int i = 0;
	filesPerThread.resize( numThreads );
	while( fdIn.getline( dummy, 1000 ) )
	{
		vector< string > cols;
		boost::split( cols, dummy, boost::is_any_of( "\t" ), boost::token_compress_on );

		InputFileDescriptor fd;
		try
		{
			fd.path = cols[ 0 ];
			fd.format = cols[ 1 ];
			fd.compression = cols[ 2 ];
			fd.qualOffset = boost::lexical_cast< int >( cols[ 3 ] );
		}
		catch ( std::exception & e )
		{
			cerr << "illegal columns in input descriptor file: " << e.what() << endl;
			exit( -1 );
		}
		filesPerThread[ i % numThreads ].push_back( fd );
		i++;
	}
	fileIPerThread.resize( numThreads );

	for ( int i = 0; i < numThreads; i++ )
	{
		ifstream *in = new ifstream();

		inPerThread.push_back( in );
		readCsrPerThread.push_back( 0 );
	}
	for ( int i = 0; i < numThreads; i++ )
	{
		fileIPerThread[ i ] = -1; // this means file is not ready to read/end of all files

		if ( filesPerThread[ i ].size() > 0 )
		{
			inPerThread[ i ]->open( filesPerThread[ i ][ 0 ].path, ifstream::in );
			if( inPerThread[ i ]->fail())
			{
				cerr << "Couldn't open file " << filesPerThread[ i ][ 0 ].path.c_str() << endl;
				exit( -1 );
			}
			fileIPerThread[ i ] = 0;
		}
	}

	buf = new char[ IOWORKER_LINE_WIDTH * 4 * numThreads_ ];
}

std::wstring FormatTime(boost::posix_time::ptime now)
{
	using namespace boost::posix_time;
	static std::locale loc(std::wcout.getloc(),
		new wtime_facet(L"%Y%m%d_%H%M%S"));

	std::basic_stringstream<wchar_t> wss;
	wss.imbue(loc);
	wss << now;
	return wss.str();
}


// skipLineCnt_ = number of lines processed previously by this thread
// if OUT_lineBufAddr == 0, then don't bother
// input file should be already opened and ready for reading
// returns false on error/incomplete lines read
bool IOWorkerSimple::getLines( int threadId_,  char **OUT_lineBufAddr, int &OUT_numLines )
{
	OUT_numLines = 0;
	*OUT_lineBufAddr = 0;
	if ( fileIPerThread[ threadId_ ] == -1 ) { return( false ); }

	int qualDelta = 64 - filesPerThread[ threadId_ ][ fileIPerThread[ threadId_ ] ].qualOffset;

	int lines = 0;
	int csr = ( threadId_ * 4 + lines ) * IOWORKER_LINE_WIDTH;
	int startCsr = csr;
	bool bReadGood = false;
	while( lines < 4 )
	{
	  bReadGood = bool(inPerThread[ threadId_ ]->getline( &buf[ csr ], IOWORKER_LINE_WIDTH - 1 ));

		if ( !bReadGood )
		{
			inPerThread[ threadId_ ]->close();
			fileIPerThread[ threadId_ ]++;

			if ( fileIPerThread[ threadId_ ] < filesPerThread[ threadId_ ].size() )
			{
				inPerThread[ threadId_ ]->open( filesPerThread[ threadId_ ][ fileIPerThread[ threadId_ ] ].path, ifstream::in );
				continue;
			}
			else
			{
				fileIPerThread[ threadId_ ] = -1;
				return( false );
			}
		}

		char *x = &buf[ csr ];

		if ( lines % 4 == 3 && qualDelta != 0 )
		{
     		// qual line: adjust quality by offset: we store the qual line as Q64
			for( int i = 0; i < strlen( x ); i++ )
			{
				x[ i ] = x[ i ] + qualDelta;
			}
		}
		if ( lines == 0 ) { readCsrPerThread[ threadId_ ]++; }

		lines++;
		csr = ( threadId_ * 4 + lines ) * IOWORKER_LINE_WIDTH;

	}
	OUT_numLines = 4;
	*OUT_lineBufAddr = &buf[ startCsr ];

	return( true );
}
