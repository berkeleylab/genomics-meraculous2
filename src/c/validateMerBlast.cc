#include <time.h>
#include <boost/thread/thread.hpp>
#include <boost/unordered_map.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <boost/signals2/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <sparsehash/sparse_hash_map>
#include <boost/smart_ptr/detail/spinlock.hpp>
#include <mutex>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <utility>

#include "PackedDNASequence.hpp"  
#include "IOWorkerSimple.hpp"
#include "LV.hpp"

using google::sparse_hash_map;

#define PAGES_PER_BUFFER 1000
#define MAX_NUM_CONTIGS 10000000
#define MAX_SEQ_LEN 1000

// what percentage of a projection can be indels and still be trustworthy?
#define INDEL_PCT_MAX 0.2
#define max( x, y )  ( x > y ? x : y )
using namespace std;

typedef PackedDNASequence PackedMer;
typedef boost::unordered_map< int, int > IntHashTable;

class CompareKeys   
{
public: 
	inline bool operator()( const PackedMer &a, const PackedMer &b ) const
	{
		return( PackedDNASequence::equals( a, b ) );
	}
};


class HashKeys
{
public:
	inline std::size_t operator()( const PackedMer &a ) const
	{
		return PackedDNASequence::hash( a ); 
	}
};

struct AlignmentRecord {
  std::string readName;
  int readStart;
  int readStop;
  int readLen;
  std::string contigName;
  int contigStart;
  int contigStop;
  int contigLen;
  bool bStrandIsPositive;
};

typedef sparse_hash_map< PackedMer, std::pair< int, unsigned int >, HashKeys, CompareKeys > HashTable;
typedef sparse_hash_map< std::string, std::vector< AlignmentRecord > * > AlignmentHash;
typedef sparse_hash_map< std::string, int > NameToIdHash;

int g_extensionType = 1; // 0 = none, 1 = lv, 2 = memcmp hack
bool bDebugMode = false;
bool bUseMmap = 0;
int g_hashPrefixLen = 8;  /// must be 8 b/c of PackedDNASequence::encode() coupling
int g_numHashTables = pow( 4, g_hashPrefixLen );
int g_numMutexes = g_numHashTables;
int g_mismatchPenalty = -10;
bool g_bAllowMixedCase = false;

std::vector< HashTable * > v_merHash;
std::vector< boost::detail::spinlock * > v_pMerHashAccess;
IOWorkerSimple ioWorker;
long int *contigLenTable;
string *contigNames;
string *contigSeqTable;
long int g_maxNumContigs;

boost::detail::spinlock *contigHashLock;
AlignmentHash alignmentHash;
NameToIdHash contigNameToIdHash;

void printTime2()
{
	time_t rawtime;
	struct tm *timeinfo;
	char buffer [200];
	time( &rawtime );
	timeinfo = localtime( &rawtime );
	strftime (buffer,200,"%c",timeinfo);	
	cerr << buffer;
}

// checks that mer contains only [ACGT]
inline bool verifyMer( const char *s, int length )
{

	for ( int i =0; i < length; i++ )
	{
		switch( s[i] )
		{
			case 'A': case 'C': case 'G': case 'T': 
			break;
			default:
			return( 0 );
		}
	}
	return( 1 );
}


inline string revcomp( const string &s )
{
	string rs = s; // allocate same amount of memory
	
	int rsCsr = 0;
	for( int i = s.length() - 1; 
		i >= 0;
		i-- )
	{
		switch( s[ i ] )
		{
			case 'A': 
			rs[ rsCsr ] = 'T'; break;
			case 'T':
			rs[ rsCsr ] = 'A'; break;
			case 'C':
			rs[ rsCsr ] = 'G'; break;
			case 'G':
			rs[ rsCsr ] = 'C'; break;
			case 'a':
			rs[ rsCsr ] = 't'; break;
			case 't':
			rs[ rsCsr ] = 'a'; break;
			case 'c':
			rs[ rsCsr ] = 'g'; break;
			case 'g':
			rs[ rsCsr ] = 'c'; break;
			default:
			rs[ rsCsr ] = s[ i ];
		}
		rsCsr++;
	}
	return( rs );
}

inline char revcompChar( const char x )
{
	switch( x )
	{
		case 'A': return( 'T' ); 
		case 'C': return( 'G' ); 
		case 'G': return( 'C' ); 
		case 'T': return( 'A' ); 
		case 'a': return( 't' );
		case 'c': return( 'g' );
		case 'g': return( 'c' );
		case 't': return( 'a' );	
	}
	return( x );
}
inline void revcompCStr( const char *cStr, int numChars, char *newBuf )
{
    const char *cStrEnd = cStr + numChars - 1;
	while ( cStrEnd >= cStr )
	{
		*newBuf = revcompChar( *cStrEnd );
        newBuf++; cStrEnd--;
	}
}


inline bool isValid( const char x )
{
	switch( x )
	{
		case 'A':
		case 'C':
		case 'G':
		case 'T':
		return( true );
	}
	return( false );
}


void printHashes( int merSize ) 
{
	cerr << "printing hashes" << endl;
	for ( long int prefixCode = 0; prefixCode <= 65535; prefixCode++ ) {
	for ( HashTable::iterator iter = v_merHash[ prefixCode ]->begin(); 
		  iter != v_merHash[ prefixCode ]->end();
		  iter++ ) 
	{
		char mer[merSize];
		char *merPtr = mer;
		iter->first.convert( &merPtr );
		cerr << prefixCode << " " << mer << endl;
	}			
	}			
}					

/* A NOTE ON INDEXES:
the contigNamesToIdx hash converts between the name in the file and our internal id that indexes the contig-related parallel arrays
This idx goes from 1 to inf, -1 to -inf -- zero is illegal: this way both strands can be represented
I've wrapped any lookup operations involving the merHash in lookupMer(), which uses the 1-based contigId as an index across all structures.

*/
void prepareHashes( std::string contigsInName, int merSize )
{
	printTime2();

	ifstream contigsIn( contigsInName );
	
	bool revComp = 1;
	char str[ 10000 ];
	int cnt = 0;
	string contigSeq = "";
	// load and prepare mer hash				
	// In the hash, we store each mer seen in the contigs + their revcomp	 
	bool bLastLineUnprocessed = true;
	long int linesRead = 0;
	bool successBit = false;
			// inside my code, contigId goes from -MAX to MAX, where 0 is a sentinel value
	int contigId = 0;
	bool bFirstLine = true;
	std::string contigName = "";
	while( ( successBit = contigsIn.getline( &str[0], 10000 ) ) || bLastLineUnprocessed )
	{
		cnt++;
		bool bThisIsLastLine = false;
		if ( !successBit ) 
		{ 			
			bThisIsLastLine = true; 
		}
		if ( 0 == ( cnt % 10000 ) ) { cerr << "."; }

		PackedMer tempPackedMer;

		if ( bThisIsLastLine || str[0] == '>' )
		{
			if ( bFirstLine ) {
				bFirstLine = false;
			} else
			{
				//process 'previous' contig
				contigLenTable[ contigId ] = contigSeq.length();
				contigSeqTable[ contigId ] = contigSeq;

				string revcompSeq = revcomp( contigSeq );
				contigSeqTable[ -contigId ] = revcompSeq;
				contigLenTable[ -contigId ] = contigSeq.length();

				const char *s = contigSeq.c_str();

				for ( int i = 0; i < contigSeq.length() - merSize + 1; i++ )
				{
					string mer = contigSeq.substr( i, merSize );
   
					//if ( !g_bAllowMixedCase ) 
					{
						bool bIllegalMer = false;
						for ( int merI = 0; merI < mer.size(); merI++ ) {
							if ( !PackedDNASequence::isSupportedChar( mer.at( merI ) ) ) {
								bIllegalMer = true;
								break;
							}
						}
						// skip the mer that has unsupported chars
						if ( bIllegalMer ) { continue; }

					}
         
					unsigned int prefixCode1 =  PackedDNASequence::encode( mer.c_str() );
					unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( mer.c_str() + 4 );
					tempPackedMer.setSeq( mer.c_str() );


					// note: we add one to the contigId stored in the has b/c we use +/- to store strand as well,
					//    and 0 cannot be made negative...
					{
						std::pair<int, int> p( contigId, i );
						HashTable::value_type v( tempPackedMer, p );
						HashTable *h= v_merHash[ prefixCode ];

						boost::detail::spinlock &lock = *( v_pMerHashAccess[ prefixCode ] );
						{

							// begin critical section
							std::lock_guard< boost::detail::spinlock > writeLock( lock );

							HashTable::iterator iter = h->find( tempPackedMer );						
							if ( iter != v_merHash[ prefixCode ]->end() )
							{
								//cerr << "FAIL: mer " << mer << " already in hash in contig " << zeroBasedContigId << endl;
							}
							else
							{
								// new mer
								h->insert( v );								
							}
						}
					}

					string rcMer = revcomp( mer );
					prefixCode1 =  PackedDNASequence::encode( rcMer.c_str() );
					prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( rcMer.c_str() + 4 );
					tempPackedMer.setSeq( rcMer.c_str() );
		
					{
						std::pair<int,int> p( -( contigId ), contigSeq.length() - i - merSize );
						HashTable::value_type v( tempPackedMer, p );
						HashTable *h= v_merHash[ prefixCode ];

						boost::detail::spinlock &lock = *( v_pMerHashAccess[ prefixCode ] );
						{

							// begin critical section
							std::lock_guard< boost::detail::spinlock > writeLock( lock );

							HashTable::iterator iter = h->find( tempPackedMer );						
							if ( iter != v_merHash[ prefixCode ]->end() )
							{
								//cerr << "FAIL: mer " << mer << " already in hash in contig " << zeroBasedContigId << endl;
							}							
							else
							{
								// new mer
								h->insert( v );								
							}
						}
					}
				}
			}

			if ( !bThisIsLastLine )
			{
                
				// prepare the next contig
				contigId = abs(contigId) + 1; 
                assert(abs(contigId) < g_maxNumContigs );
				contigSeq = "";

				typedef std::vector< std::string > SplitVec;
				SplitVec nameCols;
				char *nameStart = str + 1;
				boost::algorithm::split( nameCols, nameStart, boost::is_any_of( "\t " ) );
				contigNames[ contigId ] =  nameCols[ 0 ];
				contigNames[ -contigId ] = nameCols[ 0 ];
				contigNameToIdHash[ nameCols[ 0 ] ] = contigId;
			}
			else 
			{
				break;
			}
		}
		else
		{
			contigSeq += str;
		}
		linesRead++;
	}

	contigsIn.close();
	printTime2();
	cerr << "...done prepping hashes" << endl;
}

void printAlignment( ofstream &out, string readName, int contigId, string strand, 
	int leftCsr, int rightCsr, 
	int leftContigPos, int rightContigPos, 
	int contigLen, int readLen, 
	int merSize, std::string readCsrStr )
{
	long int readStart = leftCsr + 1;
	long int readStop = rightCsr + merSize;
    int alignLen = readStop - readStart + 1;
	
	long int merContigStart = leftContigPos + 1;
	long int merContigStop = rightContigPos + merSize;
	
	// may be negative
	long int contigStart = merContigStart; 
	long int contigStop = merContigStop;
	// may be > contigLen
	if ( "Minus" == strand )
	{
		contigStart = contigLen - contigStart + 1;
		contigStop = contigLen - contigStop + 1;
        long int x = contigStart;
        contigStart = contigStop;
        contigStop = x;
	} 

	// print results:  we print a leading readCsrPos so that results can be merged efficiently across contig blocks
	out << readCsrStr << "\t" << "BLASTN\t" << readName << "\t" << readStart << "\t" << readStop << "\t" << readLen << "\t" << 
	contigNames[ contigId ] << "\t" << 
	contigStart << "\t" << contigStop << "\t" << contigLen << "\t" <<  strand << "\t" << 
	99999 << "\t" << "0" << "\t" << alignLen << "\t" << alignLen << "\n";
}

bool printAlignmentIfValid( ofstream &out, string readName, int contigId, bool bStrandIsPositive, int leftPos, int rightPos, 
	int leftCsrPos, int rightCsrPos, int merSize, int readLen, std::string readCsrPos )
{
	int MIN_ALIGNMENT_LENGTH = merSize;
	int alignLengthAlongContig = abs( rightPos - leftPos ) + merSize ;
	int alignLengthAlongRead = abs( rightCsrPos - leftCsrPos ) + merSize ;
	int wiggleRoom = max( 1, INDEL_PCT_MAX * alignLengthAlongRead );

	if ( abs( alignLengthAlongContig - alignLengthAlongRead ) <= wiggleRoom && 
		alignLengthAlongRead >= MIN_ALIGNMENT_LENGTH )
	{
       // within 5 bases
		string strand = bStrandIsPositive ? "Plus" : "Minus";
		long int contigPos = leftPos;
		long int contigLen = contigLenTable[ contigId ];  

		printAlignment( out, readName, contigId, strand, leftCsrPos, rightCsrPos, leftPos, rightPos,
			contigLen, readLen, merSize, readCsrPos );

		return true;
	}

	return false;
}

typedef struct 
{
	int	cid;
	int leftmostContigPos, rightmostContigPos;
	int leftmostCsrPos, rightmostCsrPos;
} MerMatch;

// if not found, returns false AND o_cid==0 && o_pos = -1
bool lookupMer( const char *seq, int *o_cid, int *o_pos, bool *o_bStrand )
{
	unsigned int prefixCode1 =  PackedDNASequence::encode( seq );
	unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( seq + 4 );
	PackedDNASequence tempPackedMer;
	tempPackedMer.setSeq( seq );

	HashTable *h = v_merHash[ prefixCode ];
	int s = h->size();
	*o_cid = 0; *o_pos = -1; *o_bStrand = 0;
	HashTable::iterator i = h->find( tempPackedMer );
	if ( i != h->end() )
	{
		*o_cid  = i->second.first;
		*o_pos = i->second.second;
		*o_bStrand = ( i->second.first > 0 );
		return( true );
	}
	return( false );
}

/*
first, hash all merblast alignments: key=readName, val = array of [readstart,
	readstop, contig, strand, start, stop ]

for each read, lookup alignments
  for each alignment
     scan the extent along the read alignment, checking for at least one hit in the hash that
        falls within the contig extent; break if found, else print error
*/

void hashAlignments( const char *bv3Filename ) 
{
  ifstream bv3In;
  bv3In.open( bv3Filename );
  std::string line;
  bool bFormatIsBlastn = false;
  while( std::getline( bv3In, line ) )
  {
  	if ( strncmp( line.c_str(), "BLAST_TYPE", strlen("BLAST_TYPE") ) == 0 ) {
  		bFormatIsBlastn = true; continue;
  	}

	typedef std::vector< std::string > SplitVec;
	SplitVec cols;
	boost::algorithm::split( cols, line, boost::is_any_of( "\t " ) ); 

  	if ( !bFormatIsBlastn ) {
  	  // merblast format has a leading sentinel column
  	  cols.erase( cols.begin() );	
  	}

  	std::string readId = cols[1];
  	int readStart = boost::lexical_cast<int>(cols[2]);
  	int readStop = boost::lexical_cast<int>(cols[3]);
  	int readLen = boost::lexical_cast<int>(cols[4]);
  	std::string contigName = cols[5];
  	int contigStart =  boost::lexical_cast<int>(cols[6]);
  	int contigStop =  boost::lexical_cast<int>(cols[7]);
  	int contigLen = boost::lexical_cast<int>(cols[8]);
  	bool bStrandIsPositive = cols[9].compare( "Plus" ) == 0;

  	AlignmentRecord ar;
  	ar.readName = readId;
  	ar.readStart = readStart;
  	ar.readStop = readStop;
  	ar.contigName = contigName;  
  	ar.contigStart = contigStart;
  	ar.contigStop = contigStop;
  	ar.contigLen = contigLen;
  	ar.bStrandIsPositive = bStrandIsPositive;
	AlignmentHash::data_type valuesArrayRef = NULL;

	AlignmentHash::iterator iter = alignmentHash.find( readId );						
	if ( iter != alignmentHash.end() )
	{
		valuesArrayRef = iter->second;
	}
	else
	{
		valuesArrayRef = new std::vector<AlignmentRecord>();
		AlignmentHash::value_type v( readId, valuesArrayRef );
		alignmentHash.insert( v );
	}
	valuesArrayRef->push_back( ar );
	//cerr << "added " << ar.readName << " to alignmentHash" << endl;
  }
}


void validateMers( int merSize )
{
	bool revComp = 1;
	long int totalEffSeqLen = 0;
	long int mersPastFiltering = 0;
	long int mersAlreadyInHash = 0; 
	long int mersLookedAt = 0;
	long int readsStartProcessed = 0;
	long unsigned int seqI = 0;
	string readName;
	int bufNumLines = ioWorker.getOutBufNumLines();
	int threadCnt_ = 1;
	char *bufLines = 0;	
	ofstream out;
	bool bFirstTime = true;
	seqI = 0;
	char *prevBufLines = 0; 
	LandauVishkin lv;
	int threadId = 0;

	long int noMerMatchFoundReads = 0;
	long int merMatchFoundReads = 0;

	//printHashes( merSize );

	while( ioWorker.getLines( threadId,  &bufLines, bufNumLines ) )
	{	   
		bFirstTime = false; 

		for( int i = 0; i < bufNumLines; )
		{	
			long int readCsr = ioWorker.getCsr( threadId );
			char *header = &bufLines[ i * ioWorker.getBufLineWidth() ];
			i++;
			char *seqPtr = &bufLines[ i * ioWorker.getBufLineWidth() ];
			i++;
			char *spacer = &bufLines[ i * ioWorker.getBufLineWidth() ];
			i++;
			char *qualPtr= &bufLines[ i * ioWorker.getBufLineWidth() ];
			i++;		

			readName = header; // strip on whitespace XXX
			readName.erase( 0, 1 );
			
			int seqLen = strlen( seqPtr );

			std::string revcompSeq = revcomp( seqPtr );

			// multiple alignments possible for a given read
			AlignmentHash::iterator iter = alignmentHash.find( readName );						
			if ( iter != alignmentHash.end() )
			{
#ifdef DEBUG
				cerr << ">alignment found for " << readName << endl;
#endif
				PackedDNASequence tempPackedMer;

				AlignmentHash::data_type valuesArrayRef = iter->second;
				for( unsigned int valuesI = 0; valuesI < valuesArrayRef->size(); valuesI++ ) {
					AlignmentRecord ar = (*valuesArrayRef)[ valuesI ];
#ifdef DEBUG
					cerr << ">: " << ar.readStart << " " << ar.readStop << " " << ar.contigName << " " <<
						ar.contigStart << " " << ar.contigStop << " " << ar.bStrandIsPositive << endl;
#endif

					bool bMerMatchFound = false;
					// validate that at least one exact k-mer match exists in the alignment 
					char *cSeqStr = seqPtr;
					for ( int seqI = ar.readStart - 1; seqI <= ar.readStop - merSize; seqI++ ) {
						const char *s = cSeqStr + seqI;

						unsigned int prefixCode1 =  PackedDNASequence::encode( s );
						unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( s + 4 );
						tempPackedMer.setSeq( s );

						char buffer[ 1000 ];
						char *bufPtr = buffer; 
						tempPackedMer.convert( &bufPtr );
						assert( strncmp( buffer, s, merSize ) == 0 );

						HashTable *h= v_merHash[ prefixCode ];
						HashTable::iterator iter = h->find( tempPackedMer );	

						if ( iter != v_merHash[ prefixCode ]->end() )
						{
							int contigId = abs(iter->second.first);
							long int contigPos = iter->second.second + 1;
							if ( ar.bStrandIsPositive) { contigPos = ar.contigLen - contigPos - merSize; }
							if ( contigId == contigNameToIdHash[ ar.contigName ] && 
								contigPos >= ar.contigStart && contigPos <= ar.contigStop ) {
								// alignment found
								bMerMatchFound = true;
								break;
							}
							else {
#ifdef DEBUG
								cerr << ">! contig mismatch for mer " << buffer << " " << contigId << " : " << contigNameToIdHash[ ar.contigName ] 
									<< " (" << ar.contigStart << " " << ar.contigStop << ") : " << contigPos << endl;
#endif
							}
						}
					
					}
					if (!bMerMatchFound) { 
						noMerMatchFoundReads++;
						cout << "no mer match for " << ar.readName << "[ " << ar.readStart << " " << ar.readStop << endl; 
					} else {
						merMatchFoundReads++;
					}
				}
			}
		}
	}
	cerr << "Final tally: " << merMatchFoundReads << " alignments had a mer match, " << noMerMatchFoundReads << " did not" << endl;
}


int main( int argc, char *argv[] )
{
	if ( argc < 4 )
	{
		cerr << "usage: [contigs_file] [mer_size] [input_descriptor_file] [alignment_file]" << endl; 
		exit( 0 );
	}
	
	string contigsWc = argv[1];
	string cmd = "ls " + ( string ) contigsWc;
	FILE *inputWC = popen( cmd.c_str(), "r" );
	std::vector< string > vContigsIn;
	char buf[ 255 ];
	while( fgets( buf, 255, inputWC ) != NULL )
	{
		cerr << "using file: " << buf << endl;
		buf[ strlen( buf ) - 1 ] = '\0'; // remove trailing newline
		vContigsIn.push_back( buf );
	}
	fclose( inputWC );
	
	int merSize = boost::lexical_cast< int, char * >( argv[ 2 ] );
	if ( merSize % 2 == 0 ) {
		cerr << "merSize must be odd ( to avoid pallindromes, which collide in the hash )" << endl;
	//	exit(-1);
	}
	if ( merSize < 8 ) {
		cerr << "merSize must be > 8" << endl;
		exit( -1 );
	}
	g_maxNumContigs = 10000000; // 10M by default seems safe most of the time
	
	contigLenTable = new long int[ g_maxNumContigs * 2 + 1 ];
	contigNames = new string[ g_maxNumContigs * 2 + 1 ];
	contigSeqTable = new string[ g_maxNumContigs * 2 + 1 ];

	contigLenTable += g_maxNumContigs;
	contigNames += g_maxNumContigs;
	contigSeqTable += g_maxNumContigs;

	int numThreads = 1;
	if ( numThreads < 1 ) { cerr << "numThreads must be greater than 0" << endl; exit( -1 ); }
	if ( numThreads > 54 ) { cerr << "numThreads must be less than 55" << endl; exit( -1 ); } // due to merBlast readCsr prefix ASCII limitations

	int numWorkerThreads = numThreads;
	ioWorker.init( argv[ 3 ], numWorkerThreads );
	
	const char *alignmentFileName = argv[4];
	

	long int numEntries = 1;
	PackedDNASequence::init( numEntries, merSize );
	for ( int i = 0; i < g_numHashTables; i++ )
	{
		HashTable *h = new HashTable( numEntries / g_numHashTables * 2 );
		v_merHash.push_back( h );     
	}		

	for( int i =0; i < g_numMutexes; i++ )
	{
		boost::detail::spinlock *m = new boost::detail::spinlock();
		v_pMerHashAccess.push_back( m );
	}
	contigHashLock = new boost::detail::spinlock();
	

	boost::thread threads[ numThreads ];
	for ( int i = 0; i < vContigsIn.size(); i++ )
	{	  
		threads[ i ] = boost::thread( prepareHashes, vContigsIn[ i ], merSize );
	}
	for ( int threadI = 0; threadI < vContigsIn.size(); threadI++ )
	{
		threads[ threadI ].join();
	}

	hashAlignments( alignmentFileName );
	validateMers( merSize );

	
    return( 0 );
}



