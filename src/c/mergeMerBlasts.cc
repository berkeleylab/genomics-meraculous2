#include <string>
#include <time.h>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <utility>

using namespace std;


static inline bool id1LessThan2( const char *id1, const char *id2 )
{
	//if ( strlen( id1 ) < 2 || strlen( id2 ) < 2 ) { return( strlen( id1 ) < strlen( id2 ) ); }

	if ( *id1 == *id2 )
	{
		// compare numeric suffixes
		long int idNum1 = boost::lexical_cast< long int >( id1 + 1 );
		long int idNum2 = boost::lexical_cast< long int >( id2 + 1 );
		return( idNum1 < idNum2 );
	}
	
	// ELSE compare the prefix letter
	return( *id1 < *id2 );
}



int main( int argc, char *argv[] )
{
	if ( argc == 1 )
	{
		exit( 0 );
	}
	char *inputFileWC = argv[ 1 ];
	bool bChompIdCol = boost::lexical_cast< bool >( argv[ 2 ] );

	string cmd = "ls " + ( string ) inputFileWC;
	FILE *inputWC = popen( cmd.c_str(), "r" );
	std::vector< ifstream * > vIn;
	char buf[ 10000 ];
	while( fgets( buf, 10000, inputWC ) != NULL )
	{
		cerr << "using file: " << buf << endl;
		buf[ strlen( buf ) - 1 ] = '\0'; // remove trailing newline
		ifstream *in = new ifstream();
		in->open( buf );

		vIn.push_back( in );

	}
	fclose( inputWC );
	int numFiles = vIn.size();
	int numFilesEOF = 0;

	std::vector< string > topIds( numFiles ); // the top lines of each file, the ones being considered for merging at each step
	std::vector< string > topLines( numFiles );  // the corresponding merBlast lines for the top ids

	// read in first lines from each file
	for ( int i = 0; i < numFiles; i++ )
	{
		if ( vIn[ i ]->getline( buf, 10000 ) )
		{
			//if ( *buf == '\0' )
			//{
		//		topIds.push_back( new string( "" ) );
	//			toplines.push_back( new string( "" ) );
	//		}
	//		else
			{
				char *csr = buf;
				while ( !iswspace( *csr ) ) { csr++; }
				*csr = '\0'; // split the string into two quickly
				char *id = &buf[ 0 ];
				char *line = csr + 1;

				topIds[ i ] = id;
				topLines[ i ] = line;
			}
		}
		else
		{
			numFilesEOF++;
			continue;
		}
	
	}

	cout << "BLAST_TYPE\tQUERY\tQ_START\tQ_STOP\tQ_LENGTH\tSUBJECT\tS_START\tS_STOP\tS_LENGTH\tSTRAND\tSCORE\t" 
		 << "E_VALUE\tIDENTITIES\tALIGN_LENGTH\n";

	long int lineNum = 0;
	while( true )
	{
		const char *leastVal = 0;
		int leastValI = -1;

		// find the least val amongst current topIDs
		for ( int i = 0; i < numFiles; i++ )
		{ 
			string &val = topIds[ i ];
			if ( !( ( *( val.c_str() ) ) == '\0' ) &&
					( leastVal == 0 || id1LessThan2( val.c_str(), leastVal ) ) )
			{ 
				leastVal = val.c_str(); 
				leastValI = i;
			}
		}
		if ( leastVal == 0 ) { break; }

		if ( !bChompIdCol )
		{
			cout << leastVal << "\t";
		}
		cout << topLines[ leastValI ] << endl;

		if ( vIn[ leastValI ]->getline( buf, 10000 ) )
		{
			char *csr = buf;
			while ( !iswspace( *csr ) ) { csr++; }

			*csr = '\0'; // split the string into two quickly
			char *id = buf;
			char *line = csr + 1;

			topIds[  leastValI ] = id;
			topLines[ leastValI ] = line;	
		}
		else
		{
			topIds[ leastValI ] = "";
		}
		lineNum++;
	
	}

	exit( 0 );
}



