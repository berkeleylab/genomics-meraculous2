#ifndef IOWORKER_HPP
#define IOWORKER_HPP

#ifndef IOWORKER_LINE_WIDTH
#define IOWORKER_LINE_WIDTH  1000000  // by default, good enough for short-reads
#endif

#define MAX_SIZE_BUF_BYTES ( (long int) 100000000 )
#define OUT_BUF_NUM_LINES 4   // how many lines to return at a time to requesting threads-must be multiple of 8 
		// due to fastq format and the fact pairs of reads should be packaed to the same thread

#include <boost/thread/locks.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <iostream>
#include <fstream>
#include <string>

/* Reads fastq files sequentially and distributes non-intersecting blocks of them to multiple threads.
   Uses lock-free pointers into a circular buffer for speed.  One thread is dedicated to I/O, while multiple client
   threads can each request data.
*/
typedef struct
{
  std::string path;    
  std::string format;  // suppported: 'F' = fastq
  std::string compression;  // supported: 'U' = uncompressed
  int qualOffset;  // usually 33 or 64 
} InputFileDescriptor;

class IOWorkerSimple
{	
	int numThreads;
	bool bEof;
	long int bufNumLines;
	char *buf;
	const char *inputDescriptorFile;
    std::vector< std::vector< InputFileDescriptor >> filesPerThread;  // each thread has a vector of input file descriptors
    std::vector< int > fileIPerThread;
    std::vector< std::ifstream * > inPerThread;
    std::vector< long int > readCsrPerThread;
public:
    long int getOutBufNumLines() { return( OUT_BUF_NUM_LINES ); }
    int getBufLineWidth() { return( IOWORKER_LINE_WIDTH ); }
	void init( const char* inputDescriptorFile, int numThreads );
	bool isEof( int threadId_ ) { return( fileIPerThread[ threadId_ ] == -1 ); }
	bool getLines( int threadId_, char **OUT_lineBufAddr, int &OUT_numLines );
	// loops/fetches data until EOF
	void fetchData();
	long int getCsr( int threadId ) { return readCsrPerThread[ threadId ]; }
	IOWorkerSimple() {}
};


#endif
