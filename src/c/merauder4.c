/* UPC version of gap-closing, Steven Hofmeyr (shofmeyr@lbl.gov), Nov 2014.
 *
 * Based on on merauder.pl by Jarrod Chapman <jchapman@lbl.gov> Tue Jun 2
 * 07:48:59 PDT 2009 Copyright 2009 Jarrod Chapman. All rights reserved.
*/

/*
 * There are a number of simplifications compared to the original perl
 * script.
 *
 * Three files are read in:
 * 1. The gap data, where all of the info is stored in an array of gap structs. 
 * 2. The contigs from a FASTA file, where only the contigs for relevant
 * sequences are stored.
 * 3. The scaffold report file, which is used to determine the start, end and
 * depth of the scaffold.
 */

#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <omp.h>
#include <argp.h>
#include <stdarg.h>
#include <limits.h>
#include <math.h>

#include "colors.h"
#include "htable.h"

const char *argp_program_version = "merauder 4.0 c";

// for debugging
//#define SOLO_GAP 7284

//#define PRINT_GAPS
//#define PRINT_CONTIGS
//#define PRINT_READ_CONTIGS
//#define PRINT_SCAFFOLDS
//#define PRINT_SCAFFOLD_DATA
//#define PRINT_QUALS
//#define PRINT_ANALYSE_QUALS
//#define PRINT_WALKS
//#define PRINT_LC_CLOSURE

#define FAIL(fmt,...)                                               \
    do {                                                            \
        fprintf(stderr, KLRED "Fail: " fmt KNORM "\n", ##__VA_ARGS__);  \
        exit(EXIT_FAILURE);                                         \
    } while (0)

#define WARN(fmt,...)                                           \
    do {                                                        \
        fprintf(stderr, KRED "WARN [%s:%d]: " fmt KNORM "\n",   \
                __FILE__, __LINE__, ##__VA_ARGS__);             \
    } while (0)

#define CHECK_ERR(cmd, err)                                      \
    do {                                                         \
        if ((cmd) == err)                                        \
            FAIL(#cmd " failed, error %s\n", strerror(errno));   \
    } while (0)

#define TRACE(...) do {fprintf(stderr, ##__VA_ARGS__);} while (0)

__attribute__ ((unused)) static inline double gettime(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (double)tv.tv_sec + (double)tv.tv_usec / 1000000.0;
}

#define NULL_STR(s) ((s) ? (s) : "")

#define DIRN_RIGHT 0
#define DIRN_LEFT 1

static const char *DIRNS[2] = {"right", "left"};

// This is a constraint in the original perl script. If this threshold is
// exceeded, the gap is skipped
#define MAX_FILL_READS 5001
// For general output buffers. Shouldn't need to be changed ever.
#define MAX_BUF 2000

// Limit for all htables. Note: exceeding the limit won't cause a failure, it
// will just cause a lot more chaining of entries in the hash table, and so be
// less efficient.
#define MAX_HTABLE_ENTRIES 30000
#define MAX_BEST_GUESS_SEQ_LEN 2000;
#define MAX_WALK_LEN 8000

struct config {
    int max_insert_size;
    char scaffold_report_file[255];
    int mer_size;
    char contig_fasta_file[255];
    char gap_data_file[255];
    int min_depth;
    int poly_mode;
    int verbose;
    double exclude_repeats;
    int qual_offset;
    int aggressive_closures;
} cfg;

typedef struct {
    char *id;
    double length;
    double depth;
    double weight;
} scaffold_t;

typedef struct {
    char *name;
    char *seq;
    int seq_len;
    char strand;
    scaffold_t *scaffold;
} contig_t;

typedef struct {
    char *nts;
    int nts_len;
    char *quals;
} filler_t;

typedef struct {
    scaffold_t *scaffold;
    contig_t *contig1;
    contig_t *contig2;
    int contig_ext1;
    int contig_ext2;
    char *primer1;
    char *primer2;
    int gap_size;
    double gap_uncertainty;
    int nfillers;
    filler_t *fillers;
} gap_t;

typedef struct {
    char *span;
    int freq;
} span_info_t;

typedef struct {
    char *mer;
    int freqs[4][4];
} qual_freqs_t;

typedef struct {
    char base;
    int rating;
    int n_hi_q;
    int n_ext;
} qual_t;

typedef struct {
    char *mer;
    char base;
    char fork_bases[4];
    int fork_n_ext[4];
} mer_base_t;

// resizable string
typedef struct {
	char *buf;
	int max_len;
} string_t;

static int max_gaps = 10000 + 1;

static gap_t **gaps;
static int ngaps = 0;
static htable_t contigs;
static htable_t scaffolds;
static int mygap = 0;

static double t_splinting = 0;
static double t_mer_walk = 0;
static double t_patch_left_right = 0;

DEFINE_HTABLE_PUT(htable_put_contig, contig_t);
DEFINE_HTABLE_GET(htable_get_contig, contig_t);
DEFINE_HTABLE_DEL(htable_del_contig, contig_t);

DEFINE_HTABLE_PUT(htable_put_scaffold, scaffold_t);
DEFINE_HTABLE_GET(htable_get_scaffold, scaffold_t);
DEFINE_HTABLE_DEL(htable_del_scaffold, scaffold_t);

DEFINE_HTABLE_PUT(htable_put_span, span_info_t);
DEFINE_HTABLE_GET(htable_get_span, span_info_t);
DEFINE_HTABLE_DEL(htable_del_span, span_info_t);

DEFINE_HTABLE_PUT(htable_put_qual_freqs, qual_freqs_t);
DEFINE_HTABLE_GET(htable_get_qual_freqs, qual_freqs_t);
DEFINE_HTABLE_DEL(htable_del_qual_freqs, qual_freqs_t);

DEFINE_HTABLE_PUT(htable_put_mer_base, mer_base_t);
DEFINE_HTABLE_GET(htable_get_mer_base, mer_base_t);
DEFINE_HTABLE_DEL(htable_del_mer_base, mer_base_t);

static const char BASES[4] = {'A', 'C', 'G', 'T'};

/*
static int get_mem_usage(void)
{
    int usage = 0;
    FILE *f = fopen("/proc/self/statm", "r");
    if (fscanf(f, "%d", &usage) != 1) 
        printf(KLRED "Warning: could not read memory usage\n");
    fclose(f);
    return usage * 4;
}
*/

static int get_max_mem_usage_mb(void)
{
    int usage;
    FILE *f = fopen("/proc/self/status", "r");
    char buf[1000];
    char units[5];
    while (fgets(buf, 999, f)) {
        if (strncmp(buf, "VmHWM:", 6) == 0) {
            sscanf(buf + 6, "%d %s\n", &usage, units);
            break;
        }
    }
    if (strcmp(units, "kB") == 0)
        usage /= 1024;
    return usage;
}

static inline int bankers_round(double x)
{
    int rx = x;
    if (x - rx > 0.5)
        rx++;
    if (x - rx == 0.5 && rx % 2)
        rx++;
    return rx;
}

// reverse in place
static inline char *reverse(char *s)
{
    int len = strlen(s);
    for (int i = 0; i < len / 2; i++) {
        char tmp = s[len - i - 1];
        s[len - i - 1] = s[i];
        s[i] = tmp;
    }
    return s;
}

static inline int endswith(char *s1, char *s2)
{
    int s1l = strlen(s1);
    int s2l = strlen(s2);
    if (s1l < s2l) 
        return -1;
    return strcmp(s1 + strlen(s1) - strlen(s2), s2);
}

error_t parse_opt(int key, char *optarg, struct argp_state *state) 
{
    struct config *c = state->input;
	switch (key) {
	case 'i': c->max_insert_size = atoi(optarg); return 0;
    case 's': strncpy(c->scaffold_report_file, optarg, 254); return 0;
    case 'm': c->mer_size = atoi(optarg); return 0;
    case 'c': strncpy(c->contig_fasta_file, optarg, 254); return 0;
    case 'g': strncpy(c->gap_data_file, optarg, 254); return 0;
    case 'D': c->min_depth = atoi(optarg); return 0;
    case 'P': c->poly_mode = 1; return 0;
    case 'V': c->verbose = 1; return 0;
    case 'R': c->exclude_repeats = atof(optarg); return 0;
    case 'Q': c->qual_offset = atoi(optarg); return 0;
    case 'A': c->aggressive_closures = 1; return 0;
	default: return ARGP_ERR_UNKNOWN;
	}
}

static void fail_reqd(struct argp *ap, char *msg) 
{
    argp_help(ap, stdout, ARGP_HELP_USAGE, msg);
    exit(EXIT_FAILURE);
}

static void get_config(int argc, char** argv) 
{
	static struct argp_option options[] = {
        {NULL, 'i', "int", 0, "Maximum insert size"},
        {NULL, 's', "char*", 0, "Scaffold report file"},
        {NULL, 'm', "int", 0, "Mer size"},
        {NULL, 'c', "char*", 0, "Contig FASTA file"},
        {NULL, 'g', "char*", 0, "Gap data file"},
        {NULL, 'D', "int", 0, "Minimum depth (default 2)"},
        {NULL, 'P', 0, 0, "Polymorphic mode"},
        {NULL, 'V', 0, 0, "Verbose mode"},
        {NULL, 'R', "double", 0, "Exclude repeats"},
        {NULL, 'Q', "int", 0, "Quality offset (default 33)"},
        {NULL, 'A', 0, 0, "Aggressive closures"},
		{0}
	};
    // defaults
    cfg.min_depth = 2;
    cfg.poly_mode = 0;
    cfg.verbose = 0;
    cfg.exclude_repeats = 0;
    cfg.qual_offset = 33;
    cfg.aggressive_closures = 0;
    // required
    cfg.max_insert_size = -1;
    strcpy(cfg.contig_fasta_file, "");
    strcpy(cfg.scaffold_report_file, "");
    strcpy(cfg.gap_data_file, "");
    cfg.mer_size = -1;

	static struct argp argp = {options, parse_opt, "", "Options:"}; 
	int arg_index = 1;
	argp_parse(&argp, argc, argv, ARGP_IN_ORDER, &arg_index, &cfg);

    if (cfg.max_insert_size == -1)
        fail_reqd(&argp, "max insert size is required (-i int)\n");
    if (strcmp(cfg.contig_fasta_file, "") == 0)
        fail_reqd(&argp, "need to specify FASTA file (-c char*)\n");
    if (strcmp(cfg.scaffold_report_file, "") == 0)
        fail_reqd(&argp, "need to specify scaffold report file (-s char*)\n");
    if (strcmp(cfg.gap_data_file, "") == 0)
        fail_reqd(&argp, "need to specify gap data file (-g char*)\n");
    if (cfg.mer_size == -1) 
        fail_reqd(&argp, "mer size is required (-m int)\n");
    
    TRACE("%s\n", argp_program_version);
    TRACE("  max_insert_size:      %d\n", cfg.max_insert_size);
    TRACE("  scaffold_report_file: %s\n", cfg.scaffold_report_file);
    TRACE("  mer_size:             %d\n", cfg.mer_size);
    TRACE("  contig_fasta_file:    %s\n", cfg.contig_fasta_file);
    TRACE("  gap_data_file:        %s\n", cfg.gap_data_file);
    TRACE("  min_depth:            %d\n", cfg.min_depth);
    TRACE("  qual_offset:          %d\n", cfg.qual_offset);
    TRACE("  exclude repeats:      %.4f\n", cfg.exclude_repeats);
    TRACE("  polymorphic mode:     %s\n", cfg.poly_mode ? "true" : "false");
    TRACE("  verbose mode:         %s\n", cfg.verbose ? "true" : "false");
    TRACE("  aggressive closures:  %s\n", cfg.aggressive_closures ? "true" : "false");
}

static int get_tokens(char *s, char *tokens[], int *tot_len, int ntoks)
{
    char *s_begin = s;
    char *s_end;
    int tok_i = 0;
    *tot_len = 0;
    while ((s_end = strchr(s_begin, '\t')) != NULL) {
        s_end[0] = '\0';
        tokens[tok_i] = s_begin;
        tok_i++;
        *tot_len = s_end - s;
        if (tok_i == ntoks)
            break;
        s_begin = s_end + 1;
    }
    // get end of line?
    if (tok_i < ntoks && (s_end = strchr(s_begin, '\n')) != NULL) {
        s_end[0] = '\0';
        tokens[tok_i] = s_begin;
        *tot_len = s_end - s;
        tok_i++;
    }
    return tok_i;
}

static scaffold_t *create_or_get_scaffold(char *scaff_id) 
{
    scaffold_t *scaffold = htable_get_scaffold(scaffolds, scaff_id);
    if (!scaffold) {
        scaffold = calloc(1, sizeof(scaffold_t));
        scaffold->id = strdup(scaff_id);
        htable_put_scaffold(scaffolds, scaffold->id, scaffold);
    }
    return scaffold;
}

static contig_t *create_or_get_contig(char *contig_id) 
{
    char *name = strndup(contig_id, strlen(contig_id) - 2);
    contig_t *contig = htable_get_contig(contigs, name);
    if (!contig) {
        contig = calloc(1, sizeof(contig_t));
        contig->name = name;
        contig->seq = NULL;
        htable_put_contig(contigs, contig->name, contig);
    } else {
        free(name);
    }
    return contig;
}

static inline char *get_short_scaff_name(char *scaff_id) 
{
    static const int sl = 8;//strlen("scaffold");
    return scaff_id + sl;
}

static char *get_report_line(gap_t *gap, char *buf)
{
    snprintf(buf, MAX_BUF - 1, 
             "REPORT:\tScaffold%s\t%s.%d\t%s\t%s.%d\t%s\t%d\t%.0f",
             gap->scaffold->id, gap->contig1->name, gap->contig_ext1, 
             gap->primer1, gap->contig2->name, gap->contig_ext2,
             gap->primer2, gap->gap_size, gap->gap_uncertainty);
    return buf;
}

static char *get_gap_info(gap_t *gap, char *buf)
{
    snprintf(buf, MAX_BUF - 1, "Scaffold%s:%s.%d<-[%d +/- %.0f]->%s.%d", 
             gap->scaffold->id, gap->contig1->name, gap->contig_ext1,
             gap->gap_size, gap->gap_uncertainty, gap->contig2->name,
             gap->contig_ext2);
    return buf;
}

static inline void fprint_gap(FILE *f, gap_t *gap)
{
    fprintf(f, "Scaffold%s\t%s.%d\t%s\t%s.%d\t%s\t%d\t%.0f", 
            gap->scaffold->id, gap->contig1->name, gap->contig_ext1, 
            gap->primer1, gap->contig2->name, gap->contig_ext2, 
            gap->primer2, gap->gap_size, gap->gap_uncertainty);
    for (int j = 0; j < gap->nfillers; j++) 
        fprintf(f, "\t%s:%s", gap->fillers[j].nts, gap->fillers[j].quals);
    fprintf(f, "\n");
}

// read info from gaps file, and store all contigs in a hash table "contigs" and 
// all scaffolds in a hash table "scaffolds"
// gap file format:
// scaffold_id contig1_id primer1 contig2_id primer2 gap_size gap_uncertainty fillers
static void read_gaps(void)
{
    TRACE("Reading %s...\n", cfg.gap_data_file);
    FILE *f = fopen(cfg.gap_data_file, "r");
    if (!f)
        FAIL("Could not open gap data file %s: %s\n", cfg.gap_data_file, strerror(errno));

    size_t buf_size = 4194304;
    char *buf = malloc(buf_size);
    const int ntoks = 7;
    char *tokens[ntoks];
    int tot_len;
    int line = 0;
    
    char *fillers[MAX_FILL_READS];
    int tot_filler_len;
    double t = gettime();
    while (fgets(buf, buf_size, f)) {
        if (strlen(buf) == buf_size - 1) {
            fseek(f, -buf_size + 1, SEEK_CUR);
            buf_size *= 2;
            buf = realloc(buf, buf_size);
            WARN("need a longer buffer, increasing to %ld", buf_size); 
            continue;
        }
        if (get_tokens(buf, tokens, &tot_len, ntoks) != ntoks) 
            FAIL("Couldn't get all tokens at line %d, file %s, size is %lu"
                 "%s\n", line, cfg.gap_data_file, strlen(buf), buf);
        if (strncmp(tokens[0], "Scaffold", 8) != 0) 
            FAIL("invalid line %d in %s", line, cfg.gap_data_file);
        gap_t *gap = malloc(sizeof(gap_t));
        gap->scaffold = create_or_get_scaffold(get_short_scaff_name(tokens[0]));
        gap->contig1 = create_or_get_contig(tokens[1]);
        gap->contig_ext1 = atoi(tokens[1] + strlen(gap->contig1->name) + 1);
        gap->primer1 = strdup(tokens[2]);
        gap->contig2 = create_or_get_contig(tokens[3]);
        gap->contig_ext2 = atoi(tokens[3] + strlen(gap->contig2->name) + 1);
        gap->primer2 = strdup(tokens[4]);
        gap->gap_size = atoi(tokens[5]);
        gap->gap_uncertainty = atof(tokens[6]);
        gap->nfillers = get_tokens(buf + tot_len + 1, fillers, &tot_filler_len, MAX_FILL_READS);
        if (gap->nfillers >= MAX_FILL_READS) {
            TRACE("\n*************\nGap excluded - excessive filler reads. "
                  "(nFillReads > %d) %s\n", MAX_FILL_READS, get_gap_info(gap, buf));
            TRACE("%s\tFAILED\tExcessReads=%d\n", get_report_line(gap, buf), gap->nfillers);
            //free(gap->primer1);
            //free(gap->primer2);
            //free(gap);
            line++;
            continue;
        }
        gap->fillers = malloc(sizeof(filler_t) * gap->nfillers);
        for (int i = 0; i < gap->nfillers; i++) {
            char *quals = strchr(fillers[i], ':');
            quals[0] = '\0';
            quals++;
            gap->fillers[i].nts = strdup(fillers[i]);
            gap->fillers[i].nts_len = strlen(fillers[i]);
            gap->fillers[i].quals = strdup(quals);
        }
        gaps[ngaps] = gap;
        ngaps++;
        if (ngaps == max_gaps) {
            max_gaps *= 2;
            gaps = realloc(gaps, max_gaps * sizeof(gap_t*));
            WARN("Too many gaps, increasing size to %d", max_gaps);
        }
        line++;
    }
    //free(buf);
    fclose(f);
    TRACE("... done in %.4f s\n", gettime() - t);
    
    TRACE("Number of contigs: %d\n", htable_num_entries(contigs));
#ifdef PRINT_CONTIGS
    htable_iter_t iter = htable_get_iter(contigs);
    contig_t *c;
    while ((c = htable_get_next(contigs, iter)) != NULL) 
        printf("%s\n", c->name);
#endif
    TRACE("Number of scaffolds: %d\n", htable_num_entries(scaffolds));
#ifdef PRINT_SCAFFOLDS
    htable_iter_t iter2 = htable_get_iter(scaffolds);
    scaffold_t *scaffold;
    while ((scaffold = htable_get_next(scaffolds, iter2)) != NULL) 
        printf("%s\n", scaffold->id);
#endif

#ifdef PRINT_GAPS
    for (int i = 0; i < ngaps; i++) {
        fprint_gap(stderr, gaps[i]);
    }
#endif
}

static inline void add_contig_seq(FILE *f, contig_t *contig)
{
    const int buf_size = 100;
    char buf[buf_size];
    int max_seq_len = 100000;
    contig->seq = malloc(max_seq_len);
    contig->seq[0] = '\0';
    int seq_len = 0;
    while (fgets(buf, buf_size, f)) {
		if (strlen(buf) == buf_size - 1) 
			FAIL("line too long in file > %d", buf_size);
        if (buf[0] == '>') {
            fseek(f, -strlen(buf), SEEK_CUR);
            break;
        }
        int len = strlen(buf);
        if (seq_len + len > max_seq_len) {
            max_seq_len = seq_len + len + 2000;
            WARN("contig %s, cannot store sequence length %d, increasing to %d", 
                 contig->name, seq_len + len, max_seq_len);
            contig->seq = realloc(contig->seq, max_seq_len + 1);
        }
        strncat(contig->seq, buf, len - 1);
        seq_len += (len - 1);
    }
    if (!seq_len)
        FAIL("Could not get sequence for %s", contig->name);
    // shrink seq to save memory
    contig->seq = realloc(contig->seq, seq_len + 1);
    contig->seq_len = seq_len;
}

// get list of contigs and add contig to contig id already in hash table
// file format:
// >contig_id
// contig (split over multiple lines)
static void read_contigs(void)
{
    TRACE("Reading %s...\n", cfg.contig_fasta_file);
    double t = gettime();

    int num_contigs_needed = htable_num_entries(contigs);
    FILE *f = fopen(cfg.contig_fasta_file, "r");
    if (!f)
        FAIL("Could not open FASTA file %s: %s", cfg.contig_fasta_file, strerror(errno));

    contig_t *contig;
    int num_entries = 0;
    const size_t buf_size = 100;
    char buf[buf_size];
    while (fgets(buf, buf_size, f)) {
		if (strlen(buf) == buf_size - 1)
			FAIL("line too long (>%ld) in %s", buf_size, cfg.contig_fasta_file);
        if (buf[0] == '>') {
            buf[strlen(buf) - 1] = '\0';
            contig = htable_get_contig(contigs, buf + 1);
            if (contig) {
                add_contig_seq(f, contig);
#ifdef PRINT_READ_CONTIGS
                printf("put contig %s into table, seq %d:\n%s\n",
                        contig->name, contig->seq_len, contig->seq);
#endif
                num_entries++;
                if (num_entries == num_contigs_needed) 
                    break;
            }
        }
    }
    fclose(f);
    TRACE("... done in %.2f s. Found %d contigs\n", gettime() - t, num_entries);
    int missing_contigs = num_contigs_needed - num_entries;
    if (missing_contigs) {
        TRACE(KLRED "Warning: couldn't find all contigs in %s, missing %d:\n" KNORM, 
              cfg.contig_fasta_file, missing_contigs);
        htable_iter_t iter = htable_get_iter(contigs);
        contig_t *c;
        int i = 0;
        while ((c = htable_get_next(contigs, iter)) != NULL) {
            if (!c->seq)
                TRACE("  %d: %s\n", i++, c->name);
            else 
                TRACE("%s\n", c->name);
        }
    }
}

// reads in scaffolds and computes weight and depth statistics
// Format of file:
// scaffold_id CONTIGi contig_id start end depth
// scaffold_id GAPi gap_size gap_uncertainty
// Note: it ignores the GAPi lines
static void read_scaffold_report(void)
{
    TRACE("Reading %s...\n", cfg.scaffold_report_file);
    double t = gettime();
    FILE *f = fopen(cfg.scaffold_report_file, "r");
    if (!f)
        FAIL("Could not open scaffold report file %s: %s", 
             cfg.scaffold_report_file, strerror(errno));

    const size_t buf_size = 256;
    char buf[buf_size];
    const int max_toks = 6;
    char *tokens[max_toks];
    int tot_len;
    int line = -1;
    int depth_info_available = 0;
    while (fgets(buf, buf_size, f)) {
		if (strlen(buf) == buf_size - 1)
			FAIL("line too long >%ld in %s", buf_size, cfg.scaffold_report_file);
        line++;
        int ntoks = get_tokens(buf, tokens, &tot_len, max_toks);
        if (tokens[1][0] != 'C') 
            continue;
        char *scaff_id = get_short_scaff_name(tokens[0]);
        scaffold_t *scaffold = htable_get_scaffold(scaffolds, scaff_id);
        if (!scaffold) 
            continue;
        char c_strand = tokens[2][0];
        char *c_name = tokens[2] + 1;
        int start = atoi(tokens[3]);
        int end = atoi(tokens[4]);
        contig_t *contig = htable_get_contig(contigs, c_name);
        if (contig) {
            if (contig->strand != '\0')
                FAIL("Current version only supports unique contig->scaffold mappings (%s)", c_name);
            contig->scaffold = scaffold;
            contig->strand = c_strand;
        }
        scaffold->length = atoi(tokens[4]);
        if (ntoks > 5) {
            depth_info_available = 1;
            double depth = atof(tokens[5]);
            double weight = end - start + 1;
            scaffold->depth += (depth * weight);
            scaffold->weight += weight;
        } else if (cfg.exclude_repeats > 0) {
            TRACE("Warning: Depth information not available. Repeat exclusion turned off.\n");
            cfg.exclude_repeats = 0;
        }
    }
    fclose(f);

    int peak_depth = 0;
    if (depth_info_available) {
        int max_depth = 2000;
        double *depth_hist = calloc(max_depth, sizeof(double));
        htable_iter_t iter = htable_get_iter(scaffolds);
        scaffold_t *scaffold;
        while ((scaffold = htable_get_next(scaffolds, iter)) != NULL) {
            double mean_depth = scaffold->depth / scaffold->weight;
            scaffold->depth = mean_depth;
            if ((int)mean_depth >= max_depth) {
                max_depth = 2 * mean_depth;
                WARN("mean depth %f too high, increasing to %d", 
                     mean_depth, max_depth);
                depth_hist = realloc(depth_hist, max_depth * sizeof(double));
            }
            if (isnan(mean_depth)) 
                FAIL("mean_depth is nan, id %s", scaffold->id);
            if ((int)mean_depth < 0) 
                FAIL("mean_depth %f < 0", mean_depth);
            int md = bankers_round(mean_depth);
            depth_hist[md] += scaffold->weight;
#ifdef PRINT_SCAFFOLD_DATA
            fTRACE(stderr, "Scaffold%s %.0f %.13f %.0f", 
                   scaffold->id, scaffold->weight, scaffold->depth, scaffold->length);
#endif
        }
        double max_weight = 0;
        for (int i = 0; i < max_depth; i++) {
            if (depth_hist[i] > max_weight) {
                peak_depth = i;
                max_weight = depth_hist[i];
            }
        }
        free(depth_hist);
        TRACE("Modal scaffold depth %d\n", peak_depth);
        iter = htable_get_iter(scaffolds);
        while ((scaffold = htable_get_next(scaffolds, iter)) != NULL) 
            scaffold->depth /= peak_depth;
    }
    TRACE("... done in %.2f s.\n", gettime() - t);
}

static inline void switch_code(char *seq) {
    char *p = seq;
    while (*p) {
        switch (*p) {
        case 'a': *p = 't'; break;
        case 'c': *p = 'g'; break;
        case 'g': *p = 'c'; break;
        case 't': *p = 'a'; break;
        case 'A': *p = 'T'; break;
        case 'C': *p = 'G'; break;
        case 'G': *p = 'C'; break;
        case 'T': *p = 'A'; break;
        }
        p++;
    }
}

static inline char *get_seq(contig_t *contig, char *primer, int dirn)
{
    if (!contig->seq)
        return NULL;
    char *seq = strdup(contig->seq);
    if (contig->strand == '-') 
        switch_code(reverse(seq));
    if ((dirn == DIRN_LEFT && endswith(seq, primer) != 0) ||
        (dirn == DIRN_RIGHT && strncmp(seq, primer, strlen(primer)) != 0))
        FAIL("Contig sequence doesn't match primer (%s:%s)", 
             contig->name, primer);
    return seq;
}

static char *last_strstr(char *haystack, char *needle)
{
    char *temp = haystack, *before = 0;
    while ((temp = strstr(temp, needle))) 
        before = temp++;
    return before;
}

static char *span(char *primer1, char *primer2, filler_t *reads, int nreads) 
{
    if (cfg.verbose)
        TRACE("Attempting span: %s -> %s (%d read(s) available)\n",
              primer1, primer2, nreads);

    char *ret = NULL;
    htable_t pure_spans = create_htable(MAX_HTABLE_ENTRIES, "pure_spans");
    span_info_t *first_span = NULL;
    char *span = NULL;
    for (int i = 0; i < nreads; i++) {
        char *nts = reads[i].nts;
        char *p1_substr = strstr(nts, primer1);
        // get the longests span
        char *p2_substr = last_strstr(nts, primer2);
        if (p1_substr && p2_substr) {
            if (p1_substr <= p2_substr) {
                // a string starting with primer1 and finishing with primer2
                char *span = strdup(p1_substr);
                span[p2_substr + strlen(primer2) - p1_substr] = '\0';
                span_info_t *span_info = htable_get_span(pure_spans, span);
                if (span_info) {
                    span_info->freq++;
                    free(span);
                } else {
                    span_info = malloc(sizeof(span_info_t));
                    span_info->freq = 1;
                    span_info->span = span; 
                    htable_put_span(pure_spans, span_info->span, span_info);
                    if (!first_span)
                        first_span = span_info;
                }
            }
        }
    }
    int nspans = htable_num_entries(pure_spans);
    if (cfg.verbose)
        TRACE("%d distinct spanning sequence(s) found\n", nspans);
    if (nspans == 1 && first_span->freq > 1) {
        TRACE("Unique spanning sequence found: %d/%d reads span the gap.\n",
              first_span->freq, nreads);
        ret = strdup(first_span->span);
    } else if (cfg.poly_mode) {
        int max_span_freq = 0;
        htable_iter_t iter = htable_get_iter(pure_spans);
        span_info_t *span_info = NULL;
        while ((span_info = htable_get_next(pure_spans, iter)) != NULL) {
            if (span_info->freq > max_span_freq) {
                span = span_info->span;
                max_span_freq = span_info->freq;
            } else if (span_info->freq == max_span_freq) {
                int new_len = strlen(span_info->span);
                int prev_len = strlen(span);
                if (new_len > prev_len) {
                    // always prefer the longest span for the same frequency
                    span = span_info->span;
                } else if (new_len == prev_len && strcmp(span_info->span, span) < 0) {
                    // prefer lexicographically smallest for consistency
                    span = span_info->span;
                }
            }
        }
        free(iter);
        if (max_span_freq > 1) {
            TRACE("Maximum frequency spanning sequence found: %d/%d reads span the gap.\n",
                  max_span_freq, nreads);
            ret = span;
        } else {
            if (cfg.verbose)
                TRACE("No spanning sequence with frequency > 1 found\n");
        }
    } else {
        if (cfg.verbose)
            TRACE("No unique spanning sequence with frequency > 1 found\n");
    }
    destroy_htable(pure_spans, FREE_VALS);
    return ret;
}

static int check_closure(char *closure, char *primer1, char *primer2,
                         int gap_size, double gap_uncertainty)
{
    int bad_closure = 0;
    int p1_match = strncmp(closure, primer1, strlen(primer1));
    int p2_match = endswith(closure, primer2);

    if (p1_match != 0 || p2_match != 0) {
        bad_closure++;
        if (cfg.verbose)
            TRACE("closure [%s] rejected because it disagrees with primers\n", closure);
    }
    int closed_gap_size = strlen(closure) - 2 * cfg.mer_size;
    int gap_diff = closed_gap_size - gap_size;
    
    if (abs(gap_diff) > gap_uncertainty) {
        bad_closure += 2;
        if (cfg.verbose)
            TRACE("closure [%s] rejected due to gap estimate differential: |%d| > %.4f\n",
                  closure, gap_diff, gap_uncertainty);
    }
    // In aggressive mode ignore size differences between closure and estimate
    if (cfg.aggressive_closures && (bad_closure == 2) && (gap_size < 2 * cfg.max_insert_size)) {
        bad_closure = 0;
        if (cfg.verbose) 
            TRACE("closure [%s] allowed via AGRESSIVE mode\n", closure);
    }
    return bad_closure;
}

static char *splinting_reads(gap_t *gap, int *span_check, char *report_note) 
{        
    double t = gettime();
    char *span_closure = span(gap->primer1, gap->primer2, gap->fillers, gap->nfillers);
    *span_check = 0;
    char buf[MAX_BUF];
    if (span_closure) {
        *span_check = check_closure(span_closure, gap->primer1, gap->primer2,
                                    gap->gap_size,gap->gap_uncertainty);
        snprintf(buf, MAX_BUF - 1, "spanClosure=%s;spanCheck=%d;", span_closure, *span_check);
        strcat(report_note, buf);
        if (*span_check != 0) 
            span_closure = NULL;
    }
    t_splinting += (gettime() - t);
    return span_closure;
}

static inline int get_base_index(char base) 
{
    switch (base) {
    case 'A': return 0;
    case 'C': return 1;
    case 'G': return 2;
    case 'T': return 3;
    }
    return -1;
}

static int get_valid_base_str(char *s, int max_len, int *start, int *len)
{
    *start = 0;
    int first_N = 0;
    for (int j = 0; j < *len; j++) {
        if (s[j] == 'N') {
            first_N = j;
            if (j - *start >= max_len) {
                *len = j - *start;
                break;
            } else {
                while (s[j] == 'N')
                    j++;
                if (j < *len - 1)
                    *start = j;
                else
                    *len = first_N - *start;
            }
        }
        if (j == *len - 1)
            *len -= *start;
    }
    if (*len >= max_len)
        return 1;
    else 
        return 0;
}

static int categorize_extension(int *ext)
{
    int min_viable = 3;
    if (min_viable > cfg.min_depth) {
        //TRACE("Warning: in categorizeExtension minViable reset to match "
        //      "minDepth (%d)\n", cfg.min_depth);
        min_viable = cfg.min_depth;
    }

    // 0 = No votes
    // 1 = One vote
    // 2 = nVotes < minViable
    // 3 = minDepth > nVotes >= minViable, nHiQ < minViable
    // 4 = minDepth > nVotes >= minViable ; nHiQ >= minViable
    // 5 = nVotes >= minDepth ; nHiQ < minViable
    // 6 = nVotes >= minDepth ; minViable < nHiQ < minDepth 
    // 7 = nHiQ >= minDepth 

    //    Ignore q<10 bases
    //    my $n = $ext[0]+$ext[1]+$ext[2]+$ext[3];
    int n = ext[1] + ext[2] + ext[3];
    int n_hi_q = ext[2] + ext[3];
    int category = -1;
    if (n == 0) {
        category = 0;
    } else if (n == 1) {
        category = 1;
    } else if (n < min_viable) {
        category = 2;
    } else {
        if ((n < cfg.min_depth) || (n == min_viable)) {
            if (n_hi_q < min_viable) 
                category = 3;
            else 
                category = 4;
        } else {
            if (n_hi_q < min_viable) 
                category = 5;
            else if (n_hi_q < cfg.min_depth) 
                category = 6;
            else 
                category = 7;
        }
    }

    if (category == -1)
        FAIL("Undefined extension category");
    return category;
}

static inline int cmp_quals(qual_t *q1, qual_t *q2)
{
    if (q1->rating > q2->rating)
        return 1;
    if (q1->rating < q2->rating)
        return -1;
    if (q1->n_hi_q > q2->n_hi_q)
        return 1;
    if (q1->n_hi_q < q2->n_hi_q)
        return -1;
    if (q1->n_ext > q2->n_ext)
        return 1;
    if (q1->n_ext < q2->n_ext)
        return -1;
    return 0;
}

static inline void sort_quals(qual_t **quals)
{
    for (int i = 1; i < 4; i++) {
        for (int k = i; k > 0 && cmp_quals(quals[k], quals[k - 1]) > 0; k--) {
            qual_t *tmp = quals[k];
            quals[k] = quals[k - 1];
            quals[k - 1] = tmp;
        }
    }
}

static void compute_qual_freqs(int mer_len, filler_t *reads, int nreads, htable_t full_info) 
{
    int max_read_len = 0;
    int mer_plus = mer_len + 1;
    for (int i = 0; i < nreads; i++) {
        int start = 0;
        int nts_len = reads[i].nts_len;

        if (nts_len > max_read_len)
            max_read_len = nts_len;

        if (mer_len >= nts_len) 
            continue;

        int subseq_len = nts_len;
        int subseq_pos = 0;
        while (get_valid_base_str(reads[i].nts + subseq_pos, mer_plus, &start, &subseq_len)) {
            char *nts = strndup(reads[i].nts + subseq_pos + start, subseq_len);
            char *quals = strndup(reads[i].quals + subseq_pos + start, subseq_len);

            for (int j = 0; j < subseq_len - mer_len; j++) {
                char *mer = strndup(nts + j, mer_len);
                char extension = (nts + j)[mer_len];

                int offs = j + mer_plus - 1;
                int q = (quals[offs] - cfg.qual_offset) / 10;
                if (q > 3)
                    q = 3;

                qual_freqs_t *qual_freqs = htable_get_qual_freqs(full_info, mer);
                if (!qual_freqs) {
                    qual_freqs = calloc(1, sizeof(qual_freqs_t));
                    qual_freqs->mer = mer;
                    htable_put_qual_freqs(full_info, mer, qual_freqs);
                } else {
                    free(mer);
                }
                int bi = get_base_index(extension);
                if (bi < 0) {
                    TRACE("[%d] %s\n%s\n", i, reads[i].nts, nts);
                    FAIL("Invalid base at %d: %d", offs, extension);
                }
                qual_freqs->freqs[bi][q]++;
            }
            free(nts);
            free(quals);

            subseq_pos += (start + subseq_len);
            if (subseq_pos >= nts_len) 
                break;
            subseq_len = nts_len - subseq_pos;
        }
    }
#ifdef PRINT_QUALS
    qual_freqs_t *qual_freqs;
    htable_iter_t iter = htable_get_iter(full_info);
    while ((qual_freqs = htable_get_next(full_info, iter)) != NULL) {
        printf("%d %d %s ", mer_len, mygap, qual_freqs->mer);
        for (int i = 0; i < 4; i++) 
            for (int j = 0; j < 4; j++) 
                printf("%d ", qual_freqs->freqs[i][j]);
        printf("\n");
    }
#endif
}

static void analyse_qual_freqs(htable_t full_info, htable_t mers) 
{
    //Full analysis of quality/frequency profile
    qual_freqs_t *qual_freqs;
    htable_iter_t iter = htable_get_iter(full_info);
    while ((qual_freqs = htable_get_next(full_info, iter)) != NULL) {
        qual_t *quals[4];
        int n_total = 0;
        for (int i = 0; i < 4; i++) {
            quals[i] = malloc(sizeof(qual_t));
            quals[i]->base = BASES[i];
            // ignore q<10 bases
            quals[i]->n_ext = qual_freqs->freqs[i][1] + qual_freqs->freqs[i][2] + 
                qual_freqs->freqs[i][3];
            quals[i]->n_hi_q = qual_freqs->freqs[i][2] + qual_freqs->freqs[i][3];
            n_total += quals[i]->n_ext;
            quals[i]->rating = categorize_extension(qual_freqs->freqs[i]);
        }

        sort_quals(quals);

        //Rules for choosing next base
        //ratings:
        //0 = No votes
        //1 = One vote
        //2 = nVotes < minViable
        //3 = minDepth > nVotes >= minViable, nHiQ < minViable
        //4 = minDepth > nVotes >= minViable ; nHiQ >= minViable
        //5 = nVotes >= minDepth ; nHiQ < minViable
        //6 = nVotes >= minDepth ; minViable <= nHiQ < minDepth 
        //7 = nHiQ >= minDepth 

        int top_rating = quals[0]->rating;
        int runner_up = quals[1]->rating;
        int top_rated_base = quals[0]->base;
        
        mer_base_t *mer_base = calloc(1, sizeof(mer_base_t));
        mer_base->mer = strdup(qual_freqs->mer);
        if (top_rating < 3) {         // must have at least minViable bases
            //mer_base->base = 'X';
        } else if (top_rating == 3) {    // must be uncontested   
            if (runner_up == 0) 
                mer_base->base = top_rated_base;
            else 
                mer_base->base = 0;
        } else if (top_rating < 6) {
            if (runner_up < 3)
                mer_base->base = top_rated_base;
            else 
                mer_base->base = 0;
        } else if (top_rating == 6) {  // viable and fair hiQ support
            if (runner_up < 4) 
                mer_base->base = top_rated_base;
            else 
                mer_base->base = 0;
        } else {                     // strongest rating trumps
            if (runner_up < 7) {       
                mer_base->base = top_rated_base;
            } else {
                int k = 0;
                for (int b = 0; b < 4; b++) {
                    if (quals[b]->rating == 7) {
                        mer_base->fork_bases[k] = quals[b]->base;
                        mer_base->fork_n_ext[k] = quals[b]->n_ext;
                        k++;
                    } else {
                        break;
                    }
                }
            }
        }
        htable_put_mer_base(mers, mer_base->mer, mer_base);
        for (int i = 0; i < 4; i++)
            free(quals[i]);
    }
#ifdef PRINT_ANALYSE_QUALS
    mer_base_t *mer_base;
    iter = htable_get_iter(mers);
    while ((mer_base = htable_get_next(mers, iter)) != NULL) {
        printf(">>%d '%s' ", mygap, mer_base->mer);
        if (mer_base->fork_bases[0] == 0) {
            printf("'%c'\n", mer_base->base);
        } else {
            printf("'F");
            for (int b = 0; b < 4; b++) {
                if (mer_base->fork_bases[b]) 
                    printf("%c%d", mer_base->fork_bases[b], 
                            mer_base->fork_n_ext[b]);
                else
                    break;
            }
            printf("'\n");
        }
    }
#endif
}

static char *get_fork_str(char *s, mer_base_t *b) 
{
    strcpy(s, "F");
    for (int i = 0; i < 4; i++) {
        if (!b->fork_bases[i])
            break;
        char buf[100];
        sprintf(buf, "%c%d", b->fork_bases[i], b->fork_n_ext[i]);
        strcat(s, buf);
    }
    return s;
}

static void extend_walk(string_t *walk, char base, int len, int line)
{
	if (len >= walk->max_len - 1) {
		WARN("%d walk too long, >= %d", line, walk->max_len);
		walk->max_len *= 2;
		walk->buf = realloc(walk->buf, walk->max_len);
	}
	strncat(walk->buf, &(base), 1);
}

static string_t *alloc_string(int max_len)
{
	string_t *s = malloc(sizeof(string_t));
	s->buf = malloc(max_len);
	s->max_len = max_len;
	return s;
}

static int walk_mers(char *primer1, char *walk_result, htable_t mers, htable_t full_info,
                     string_t *walk, char *true_primer2) 
{
	string_t *step = alloc_string(MAX_WALK_LEN);
    strcpy(step->buf, primer1);
	int step_len = strlen(step->buf);
	int step_pos = 0;
    int success = 0;
    int n_forks = 0;
    int n_steps = 0;
    htable_t loop_check = create_htable(10000, "loop_check");
    int val = 1;

    while (1) {
        if (htable_get(loop_check, step->buf + step_pos)) {
            strcpy(walk_result, "R");
            break;
        } else {
            char *k = strdup(step->buf + step_pos);
            htable_put(loop_check, k, &val);
        }
        mer_base_t *next = htable_get_mer_base(mers, step->buf + step_pos);
        if (next) {
            n_steps++;
            if (cfg.verbose) {
                TRACE("%d : %s->", n_steps, step->buf + step_pos);
                if (next->base) {
                    TRACE("%c\t", next->base);
                } else if (next->fork_bases[0]) {
                    char buf[100];
                    TRACE("%s\t", get_fork_str(buf, next));
                } else {
                    TRACE("X\t");
                }
                qual_freqs_t *qf = htable_get_qual_freqs(full_info, step->buf + step_pos);
                for (int q = 0; q < 4; q++) 
                    TRACE("[%d %d %d %d]", qf->freqs[q][0], qf->freqs[q][1], 
                          qf->freqs[q][2], qf->freqs[q][3]);
                TRACE("\n");
            }
            if (next->base) {
                step_pos++;
				step_len++;
                extend_walk(step, next->base, step_len, __LINE__);
                extend_walk(walk, next->base, strlen(walk->buf), __LINE__);
                if (endswith(walk->buf, true_primer2) == 0) {
                    success = 1;
                    break;
                }
            } else if (cfg.poly_mode && !n_forks && next->fork_bases[0]) {
                //  Biallelic positions only (for now) maximum vote path is taken
                char c = 0;
                if (next->fork_bases[1] && !next->fork_bases[2]) {
                    c = next->fork_n_ext[0] > next->fork_n_ext[1] ? 
                        next->fork_bases[0] : next->fork_bases[1];
                    if (cfg.verbose) 
                        TRACE("Polymorphic conditions met .. attempting max frequency resolution.\n");
                } else {
                    char buf[100];
                    strcpy(walk_result, get_fork_str(buf, next));
                    break;
                }
                step_pos++;
				step_len++;
                extend_walk(walk, c, strlen(walk->buf), __LINE__);
                extend_walk(step, c, step_len, __LINE__);
                n_forks++;
                if (endswith(walk->buf, true_primer2) == 0) {
                    success = 1;
                    walk_result[0] = 0;
                    break;
                }
            } else {
                if (next->fork_bases[0]) {
                    char buf[100];
                    strcpy(walk_result, get_fork_str(buf, next));
                } else { 
                    strcpy(walk_result, "X");
                }
                break;
            }
        } else {
            strcpy(walk_result, "X");
            break;
        }
    }
	free(step->buf);
	free(step);
#ifdef PRINT_WALKS
    printf("%d %s%s\n", mygap, walk->buf, walk_result);
#endif

    destroy_htable(loop_check, FREE_KEYS);

    return success;
}

static void bridge_iter(char *c1_seq, char *c2_seq, filler_t *seq_ref, 
                        int nreads, int dirn, char **dwalk, char *dfail)
{
    if (cfg.verbose) 
        TRACE("Attempting %s bridge: (%d read(s) available)\n", DIRNS[dirn], nreads);

    filler_t *reads = malloc(sizeof(filler_t) * nreads);
    int max_read_length = 0;

    for (int i = 0; i < nreads; i++) {
        filler_t *f = &reads[i];
        f->nts = strdup(seq_ref[i].nts);
        f->nts_len = seq_ref[i].nts_len;
        if (f->nts_len > max_read_length)
            max_read_length = f->nts_len;
        f->quals = strdup(seq_ref[i].quals);
        if (dirn == DIRN_LEFT) {
            switch_code(reverse(f->nts));
            reverse(f->quals);
        }
    }

    const int min_mer_len = 13;
    int mer_len = cfg.mer_size;
    int downshift = 0;
    int upshift = 0;

    char *max_walk = NULL;
    int max_walk_len = 0;

    char *c1s, *c2s;
    if (dirn == DIRN_LEFT) {
        c1s = c2_seq;
        c2s = c1_seq;
        switch_code(reverse(c1s));
        switch_code(reverse(c2s));
    } else {
        c1s = c1_seq;
        c2s = c2_seq;
    }

    int sl1 = strlen(c1s);
    
    char *true_primer2 = strndup(c2s, cfg.mer_size);
    char *primer1 = NULL;
    char *primer2 = NULL;
    char walk_result[100] = "";
    while (1) {
        walk_result[0] = 0;
        // Allow k-mers less than mer_size to be used 
        if (mer_len < cfg.mer_size) {
            int the_rest = cfg.mer_size - mer_len;
            primer1 = strndup(c1s + sl1 - cfg.mer_size, mer_len);
            primer2 = strndup(c2s + the_rest, mer_len);
        } else {
            primer1 = strdup(c1s + sl1 - mer_len);
            primer2 = strndup(c2s, mer_len);
        }
        if (strlen(primer1) < mer_len) {
            free(primer1);
            primer1 = NULL;
        }
        if (strlen(primer2) < mer_len) {
            free(primer2);
            primer2 = NULL;
        }
        if (!primer1 || !primer2) {
            if (cfg.verbose)
                TRACE("Unable to find k=%d seeds\n", mer_len);
            //if (max_walk) {
            //    *dwalk = max_walk;
                //} else {
            if (!max_walk) {
                //*dwalk = malloc(1);
                //*dwalk[0] = 0;
                max_walk = malloc(1);
                max_walk[0] = 0;
                strcpy(walk_result, "X");
                strcpy(dfail, walk_result);
            }
            break;
        }

        htable_t full_info = create_htable(MAX_HTABLE_ENTRIES, "full_info");
        compute_qual_freqs(mer_len, reads, nreads, full_info);
        htable_t mers = create_htable(MAX_HTABLE_ENTRIES, "mers");
        analyse_qual_freqs(full_info, mers);
        
		string_t *walk = alloc_string(MAX_WALK_LEN);
        strcpy(walk->buf, primer1);

        if (!max_walk) {
            max_walk = strdup(walk->buf);
            max_walk_len = strlen(max_walk);
            strcpy(dfail, walk_result);
        }

        int success = walk_mers(primer1, walk_result, mers, full_info, walk, true_primer2);

        // clean up
        destroy_htable(full_info, FREE_KEYS|FREE_VALS);
        destroy_htable(mers, FREE_KEYS|FREE_VALS);
        
        // Trim off extra lead bases if upshifted
        int additional_bases = mer_len - cfg.mer_size;
        if (additional_bases < 0) 
            additional_bases = 0;
        int walk_len = strlen(walk->buf) - additional_bases;
        if (success || (walk_len > max_walk_len)) {
            if (max_walk)
                free(max_walk);
            max_walk = strdup(walk->buf + additional_bases);
            max_walk_len = walk_len;
            strcpy(dfail, walk_result);
        }

        if (primer1)
            free(primer1);
        if (primer2)
            free(primer2);
        free(walk->buf);
		free(walk);
        primer1 = NULL;
        primer2 = NULL;

        if (walk_result[0] == 'F' || walk_result[0] == 'R') {
            mer_len += 2;
            upshift = 1;
            if (downshift == 1 || mer_len >= max_read_length) 
                break;
            if (cfg.verbose)
                TRACE("Degeneracy encountered; upshifting (k->%d)\n", mer_len);
        } else if (walk_result[0] == 'X') {
            mer_len -= 2;
            downshift = 1;
            if (upshift == 1 || mer_len < min_mer_len) 
                break;
            if (cfg.verbose)
                TRACE("Termination encountered; downshifting (k->%d)\n", mer_len);
        } else {
            break;
        }
    }
    
    *dwalk = max_walk;

    TRACE("MAX%s [%s%s]\n", dirn == DIRN_RIGHT ? "RIGHT" : "LEFT", 
          NULL_STR(*dwalk), dfail);

    free(true_primer2);
    for (int i = 0; i < nreads; i++) {
        filler_t *f = &reads[i];
        free(f->nts);
        free(f->quals);
    }
    free(reads);
    return;
}

static char *patch(char *right_walk, char *left_walk, int gap_size, double gap_uncertainty) 
{
    int min_acceptable_overlap = 10;
    int right_len = strlen(right_walk);
    int left_len = strlen(left_walk);
    int gu = bankers_round(gap_uncertainty);
    int ideal_len = 2 * cfg.mer_size + gap_size;
    int chop_left = left_len - ideal_len;
    int chop_right = right_len - ideal_len;

    if (cfg.verbose)
        TRACE("Attempting patch [%d][%d] (gap: %d +/- %.1f)\n", 
              right_len, left_len, gap_size, gap_uncertainty);

    char *rwalk = strdup(right_walk);
    char *lwalk = strdup(left_walk);
    char *test_right = rwalk;
    char *test_left = lwalk;
    if (chop_left > 0) 
        test_left += chop_left;
    int tl_len = strlen(test_left);
    if (chop_right > 0) 
        test_right[right_len - chop_right] = 0;
    int tr_len = strlen(test_right);

	int max_buf = MAX_BEST_GUESS_SEQ_LEN;
    char *best_guess_seq = malloc(max_buf);
    best_guess_seq[0] = 0;
    double best_guess_delta = gap_uncertainty + 1;
    int n_best_guesses = 0;
    int n_guesses = 0;
    for (int o = -gu; o <= gu; o++) {
        int overlap = tr_len + tl_len - (ideal_len + o);
        if (overlap < min_acceptable_overlap || overlap > tr_len || overlap > tl_len) 
            continue;
        char *p1_suffix = test_right + tr_len - overlap;
        char *p2_suffix = test_left + overlap;
        if (strncmp(p1_suffix, test_left, overlap) != 0) 
            continue;

        int delta = abs(o);
        n_guesses++;
		int l = strlen(test_right) + strlen(p2_suffix);
		if (l >= max_buf) {
			WARN("buf is too small for best_guess_seq length %d in patch; increasing to %d", 
				 l, l + 1);
			max_buf = l + 1;
			best_guess_seq = realloc(best_guess_seq, max_buf);
		}
        if (delta < best_guess_delta) {
            sprintf(best_guess_seq, "%s%s", test_right, p2_suffix);
            best_guess_delta = delta;
            n_best_guesses = 1;
        } else if (delta == best_guess_delta) {
            if (strlen(test_right) + strlen(p2_suffix) < strlen(best_guess_seq)) {
                sprintf(best_guess_seq, "%s%s", test_right, p2_suffix);
                best_guess_delta = delta;
            }
            n_best_guesses++;
        }
    }
    free(rwalk);
    free(lwalk);

    if (n_guesses) {
        TRACE("%d potential patches identified.  %d best guess(es) differ from gap estimate by %.0f\n",
              n_guesses, n_best_guesses, best_guess_delta);
        return best_guess_seq;
    }
    free(best_guess_seq);
    TRACE("No valid patches found.\n");
    return NULL;
}

static char *mer_walk_dirn(gap_t *gap, char *c1_seq, char *c2_seq, char *report_note, 
                           char **dwalk, char *dfail, int *check, int dirn) 
{
    double t = gettime();
    char buf[MAX_BUF];
    *dwalk = NULL;
    dfail[0] = 0;
    char *closure = NULL;
    bridge_iter(c1_seq, c2_seq, gap->fillers, gap->nfillers, dirn, dwalk, dfail);
    if (strcmp(*dwalk, "") == 0)
        FAIL("empty dwalk");
    if (!*dwalk) 
        return NULL;
    if (dirn == DIRN_LEFT) {
        switch_code(reverse(*dwalk));
        if (dfail[0] == 'F')
            switch_code(dfail);
    }
    if (!dfail[0]) {
        *check = check_closure(*dwalk, gap->primer1, gap->primer2, 
                               gap->gap_size, gap->gap_uncertainty);
        snprintf(buf, MAX_BUF - 1, "%sClosure=%s;%sCheck=%d;", 
                 DIRNS[dirn], *dwalk, DIRNS[dirn], *check);
        strcat(report_note, buf);
        if (*check == 0)
            closure = strdup(*dwalk);
    }
    t_mer_walk += (gettime() - t);
    return closure;
}

static void close_gaps(void)
{
    int success = 0;
    int n_success = 0;
    int n_failure = 0;
    char buf[MAX_BUF];
    char report_note[MAX_BUF] = "";
    char report_line[MAX_BUF] = "";
    //int tot_mem_usage = 0;
    for (int i = 0; i < ngaps; i++) {
        //int mem_usage = get_mem_usage();
        mygap = i;
        // for debugging
#ifdef SOLO_GAP
        if (mygap != SOLO_GAP) 
            continue;
#endif
         
        report_note[0] = '\0';
        gap_t *gap = gaps[i];
        get_report_line(gap, report_line);
        if (cfg.exclude_repeats) {
            if (gap->scaffold->depth > cfg.exclude_repeats) {
                TRACE("\n*************\nRepeat scaffold gap excluded. "
                      "(depth = %.6f) %s\n", 
                      gap->scaffold->depth, get_gap_info(gap, buf));
                TRACE("%s\tFAILED\tscaffDepth=%.6f", report_line,
                      gap->scaffold->depth);
                continue;
            }
        }
    
        TRACE("\n*************\nAttempt to close %d: %s\n", i, get_gap_info(gap, buf));
        
        //double t = gettime();

        gap->gap_uncertainty = 3 * gap->gap_uncertainty + 5.5;

        if (!gap->contig1->scaffold)
            FAIL("[%s] no scaffMapInfo", gap->contig1->name);

        int span_check;
        char *closure = splinting_reads(gap, &span_check, report_note);
        // If splints fail try a mer-walk
        int right_check = 0;
        char *right_walk = NULL;
        char right_fail[100] = "";
        int left_check = 0;
        char *left_walk = NULL;
        char left_fail[100] = "";

        if (!closure) {
            char *c1_seq = get_seq(gap->contig1, gap->primer1, DIRN_LEFT);
            char *c2_seq = get_seq(gap->contig2, gap->primer2, DIRN_RIGHT);
            if (c1_seq && c2_seq) {
                closure = mer_walk_dirn(gap, c1_seq, c2_seq, report_note, 
                                        &right_walk, right_fail, &right_check, DIRN_RIGHT);
                if (!closure) {
                    closure = mer_walk_dirn(gap, c1_seq, c2_seq, report_note, 
                                            &left_walk, left_fail, &left_check, DIRN_LEFT);
                }
                
            }
            if (c1_seq)
                free(c1_seq);
            if (c2_seq)
                free(c2_seq);
        }
        snprintf(buf, MAX_BUF - 1, "[%d\t%s\t%d\t%s]", gap->nfillers, gap->primer1, 
                 gap->gap_size, gap->primer2);

        // If walks failed to close, try to patch between left and right walk
        if (!closure && right_fail[0] && left_fail[0]) {
            closure = patch(right_walk, left_walk, gap->gap_size, gap->gap_uncertainty);
            if (closure) {
                int patch_check = check_closure(closure, gap->primer1, gap->primer2, 
                                                gap->gap_size, gap->gap_uncertainty);
                char buf2[MAX_BUF];
                snprintf(buf2, MAX_BUF - 1, "patchClosure=%s;patchCheck=%d;", 
						 closure, patch_check);
                strcat(report_note, buf2);
                if (patch_check != 0) {
                    free(closure);
                    closure = NULL;
                }
            }
        }
        if (closure) {
            int closed_gap_size = strlen(closure) - 2 * cfg.mer_size;
            success = 1;
            n_success++;
            TRACE("%s successfully closed gap %d: %d %s\n", 
                  buf, mygap, closed_gap_size, closure);
            if (strncmp(closure, gap->primer1, strlen(gap->primer1)) != 0)
                FAIL("closure for gap %d doesn't start with primer1", mygap);
            if (endswith(closure, gap->primer2) != 0)
                FAIL("closure for gap %d doesn't end with primer2", mygap);

            //  Lower case as little of the primer sequences as possible:
            int min_gap_mask = 2 * 5;
            int p1len = strlen(gap->primer1);
            int p2len = strlen(gap->primer2);
            int clen = strlen(closure);
            int start_gap = p1len;
            int end_gap = clen - p2len;
            if (clen < p1len + p2len + min_gap_mask) {
                start_gap = clen / 2 - min_gap_mask / 2;
                end_gap = clen / 2 + min_gap_mask / 2;
                if (clen % 2)
                    end_gap++;
            }
            for (int i = start_gap; i < end_gap; i++) 
                closure[i] = tolower(closure[i]);

            printf("Scaffold%s\t%s.%d\t%s\t%s.%d\t\%s\t%s\n",
                   gap->scaffold->id, gap->contig1->name, gap->contig_ext1, gap->primer1, 
                   gap->contig2->name, gap->contig_ext2, gap->primer2, closure);
        } else {
            success = 0;
            n_failure++;
            TRACE("%s failed to close gap %d (%d;%d:%s;%d:%s)\n",
                  buf, mygap, span_check, right_check, right_fail, left_check, left_fail);
        }
/*
        if (!success) {
            TRACE("OPEN:");
            fprint_gap(stdout, gap);
        }
*/
        if (right_walk)
            free(right_walk);
        if (left_walk)
            free(left_walk);
        if (closure)
            free(closure);

        TRACE("%s\n", report_line);
        TRACE("%s\t%s\n", success ? "SUCCESS" : "FAILED", report_note);
        //TRACE("\nClosing gap %d, size %d took %.6f s\n", i, gap->gap_size, gettime() - t);
        for (int i = 0; i < gap->nfillers; i++) {
            free(gap->fillers[i].nts);
            free(gap->fillers[i].quals);
        }
        free(gap->fillers);
        free(gap->primer1);
        free(gap->primer2);
        free(gap);

        //mem_usage = get_mem_usage() - mem_usage;
        //printf("mem +%d kB\n", mem_usage);
        //tot_mem_usage += mem_usage;
    }

    //printf("Tot mem usage %dkB (%d)\n", tot_mem_usage, get_mem_usage());

    TRACE("Done.  Successfully closed %d gaps. (%d failed to close)\n", 
          n_success, n_failure);
    TRACE("Splinting took %.4f s\n", t_splinting);
    TRACE("Mer walks took %.4f s\n", t_mer_walk);
    TRACE("Patch left/right took %.4f s\n", t_patch_left_right);
}

int main(int argc, char** argv)
{
    if (argc == 1)
        return 0;
    double t = gettime();
    get_config(argc, argv);
    gaps = malloc(max_gaps * sizeof(gap_t*));
    contigs = create_htable(MAX_HTABLE_ENTRIES, "contigs");
    scaffolds = create_htable(MAX_HTABLE_ENTRIES, "scaffolds");;
    read_gaps();
    read_contigs();
    read_scaffold_report();
    close_gaps();
    TRACE("Total elapsed time %.2f s\n", gettime() - t);
    TRACE("Peak memory usage: %d mb\n", get_max_mem_usage_mb());
    return 0;
}

