#include <time.h>
#include <boost/filesystem.hpp>
#include <boost/thread/thread.hpp>
#include <boost/unordered_map.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <boost/signals2/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <sparsehash/sparse_hash_map>
#include <boost/smart_ptr/detail/spinlock.hpp>
#include <mutex>
#include <iostream>
#include <fstream>
#include <vector>
#include <string> 
#include <algorithm>

#include "PackedDNASequence.hpp"  

using google::sparse_hash_map;

#define MAX_LINE_LEN 1000
#define PAGES_PER_BUFFER 1000
using namespace std;


  
typedef unsigned int HashValue;

typedef PackedDNASequence PackedMer;

class CompareKeys   
{
public: 
  inline bool operator()( const PackedMer &a, const PackedMer &b ) const
  {
    return( PackedDNASequence::equals( a, b ) );
  }
};


class HashKeys
{
public:
  inline std::size_t operator()( const PackedMer &a ) const
  {
   return PackedDNASequence::hash( a ); 
  }
};


typedef sparse_hash_map< PackedMer, HashValue, HashKeys, CompareKeys > HashTable;
int g_hashPrefixLen = 8;  // 4**g_hashPrefixLen = total number of hashes, must be 4 b/c of PackedDNASequence::encode() coupling
int g_numHashTables = pow( 4, g_hashPrefixLen );
int g_numMutexes = g_numHashTables;
std::vector< HashTable * > v_merHash;
std::vector< boost::detail::spinlock * > v_pMerHashAccess;
boost::detail::spinlock globalCountersAccess;
unsigned long int g_contigsStartProcessed = 0;
unsigned long int g_totalEffSeqlLen = 0;
unsigned long int g_mersAdded = 0;
unsigned long int g_mersPrinted = 0;
unsigned long int g_mersRead = 0;
unsigned long int g_mersExcluded = 0;

string decodeMer( unsigned int x, unsigned int merLen )
{
    int remainder = x;
    string mer = "";
    for( int slot = merLen - 1; slot >= 0; slot-- )
    {
        int valInSlot =  int( remainder / pow( 4,slot ) );
        char base = ' ';

        if( valInSlot == 0 ) { base = 'A'; }
        else if( valInSlot == 1 ) { base = 'C'; }
        else if( valInSlot == 2 ) { base = 'G'; }
        else if( valInSlot == 3 ) { base = 'T'; }
        else { assert( 0 ); }
         
        mer += boost::lexical_cast< string >( base );
        
        remainder -= valInSlot * pow( 4, slot  );
    }
    return( mer );
}

inline unsigned long int encode( const char *s, const int &s_len )
{
  unsigned int x = 0;

  unsigned int slotVal = 1;
  for ( int i=s_len-1; i>=0; i-- )
  {
    int val = -1;
    switch( s[ i ] )
    {
      case 'A': val = 0; break;
      case 'C': val = 1; break;
      case 'G': val = 2; break;
      case 'T': val = 3; break;
      default:
        return( -1 );
    }
    x += ( val * slotVal );
    slotVal *= 4;
  } 
  return( x );
}

inline string reverse( const string &s )
{
  string rs = s; // allocate same amount of memory
  int rsCsr = 0;
  for ( int i = s.length() - 1; 
	i >= 0;
	i-- )
  {
    rs[ rsCsr ] = s[ i ];
    rsCsr++;
  }
  return( rs );
}



inline string revcomp( const string &s )
{
	string rs = s;// allocate same amount of memory
	
	int rsCsr = 0;
	for( int i = s.length() - 1; 
		i >= 0;
		i-- )
	{
		switch( s[ i ] )
		{
			case 'A': 
				rs[ rsCsr ] = 'T'; break;
			case 'T':
				rs[ rsCsr ] = 'A'; break;
			case 'C':
				rs[ rsCsr ] = 'G'; break;
			case 'G':
				rs[ rsCsr ] = 'C'; break;
			default:
				rs[ rsCsr ] = s[ i ];
		}
		rsCsr++;
	}
	return( rs );
}

inline char revcomp( const char x )
{
  switch( x )
  {
	  case 'A': return( 'T' ); 
	  case 'C': return( 'G' ); 
	  case 'G': return( 'C' ); 
	  case 'T': return( 'A' ); 
  }
	return( x );
}
inline bool isValid( const char x )
{
  switch( x )
  {
     case 'A':
     case 'C':
     case 'G':
     case 'T':
       return( true );
  }
  return( false );
}

inline int getBaseIdx( char c )
{
  switch( c ) {
  case 'A': return( 0 ); break;
  case 'C': return( 1 ); break;
  case 'G': return( 2 ); break;
  case 'T': return( 3 ); break;
  case 'N': return( 4 ); break;
  case 'X': return( 5 ); break;
  default: 
    cerr << "Unsupported character " << c << endl;
	  return( 4 );
  }
}

inline bool isMerValid( const char *s, int len )
{
  for( int i = 0; i < len; i++ )
  {
    if ( !isValid( s[ i ] ) ) { return( false ); }
  }
  return( true );
}

void dumpTime()
{
	time_t rawtime;
	struct tm *timeinfo;
	char buffer [200];
	time( &rawtime );
	timeinfo = localtime( &rawtime );
	strftime (buffer,200,"%c",timeinfo);	
	cerr << buffer << endl;

}


void loadMers( string filename, int merSize, int minDepth, vector< const char * > validPrefixes )
{
	ifstream mercountIn( filename );
	assert( !mercountIn.eof() );
	cerr << "loading mers from " << filename << endl;
	// load and prepare mer hash		  
	PackedDNASequence tempPackedMer;	
	char str[ 1000 ];		 
	unsigned long int mersRead = 0;
	unsigned long int mersAdded = 0;
	while( mercountIn.getline( &str[0], 10000 ) )
	{
	    mersRead++;
		if ( ( mersAdded % 1000000 ) == 0 ) { cerr << "."; }
		typedef vector< string > SplitVec;
		SplitVec lineCols;
		
		boost::algorithm::split( lineCols, str, boost::is_any_of(" \t") );
		const char *cSeqStrPtr = ( lineCols[ 0 ].c_str() );
		unsigned int count = boost::lexical_cast< int >( lineCols[ 1 ].c_str() );
		
		if ( count < minDepth ) { continue; }
		
		unsigned int prefixCode1 =  PackedDNASequence::encode( cSeqStrPtr );
		unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( cSeqStrPtr + 4 );
		int j = 0;
		if ( validPrefixes.size() > 0 )
		{
			for ( ; j < validPrefixes.size(); j++ )
			{
				if ( strncmp( validPrefixes[ j ], cSeqStrPtr, strlen( validPrefixes[ j ] ) ) == 0 ) { break; }
			}
			if ( j == validPrefixes.size() ) { continue; }
		}
		tempPackedMer.setSeq( cSeqStrPtr );
		
		assert( prefixCode < g_numHashTables );
				
		HashTable::iterator i = v_merHash[ prefixCode ]->find( tempPackedMer );
		{
			// new mer
			HashTable *h= v_merHash[ prefixCode ];
			HashTable::value_type v( tempPackedMer, ( HashValue ) count );
			h->insert( v );								
				    
			mersAdded++;
		}
	}
	mercountIn.close();
	
	// begin critical section
	{
		std::lock_guard< boost::detail::spinlock > writeLock( globalCountersAccess );
	
		g_mersAdded += mersAdded;
		g_mersRead += mersRead;
	}
}


void processContigs( string contigsFileName, string outputFileName, int merSize, vector< const char * > validPrefixes, int numThreads, int threadI  )
{
    char str[ 10000 ];
	PackedDNASequence tempPackedMer;

    ofstream out;
    out.open( outputFileName );
    
	int seqI = 0;
	char *bufLines = 0;
	bool bFirstTime = true;
	long unsigned int contigsStartProcessed = 0;
	long unsigned int totalEffSeqLen = 0;
	long unsigned int mersExcluded = 0;
	long unsigned int mersPrinted = 0;
	cerr << "tallying mer extensions" << endl;
	ifstream contigsIn;
	contigsIn.open( contigsFileName );
	char lineBuf[ 65536 ];
	contigsIn.getline( lineBuf, 65536 );

	while( !contigsIn.eof() )
	{	  
	    // for each contig...  
		string header( lineBuf );
		bFirstTime = false;
		contigsStartProcessed++;
		long int n = 0;
		long int mean = 0;
		long int nDistinct = 0;
		unsigned int spaceI = 0;
		for( ; spaceI < header.length(); spaceI++ )
		{
			if ( isspace( *( header.c_str() + spaceI ) ) )
			{
			  break;
			}
		}
		assert( spaceI <= header.length() );
		assert( spaceI > 1 );
		string headerStr = header;
		string name = headerStr.substr( 1, spaceI - 1 );
		
		string seq = "";
		// at this point, lineBuf should contain the header or eof should be true
		while( !contigsIn.eof() )
		{	
				contigsIn.getline( lineBuf, 65536 );
				if ( *lineBuf == '>' ) { break; }
				seq += lineBuf; 
		}
		// at this point, lineBuf should contain the header or eof should be true; seq should contain the full seq for previous
		//    header
			
		if ( seqI % 1000 == 0 ) { cerr << "."; }
		if ( seqI % 100000 == 0 ) { cerr << endl; }
		seqI++;
				
		
		std::string nts = "X" + seq + "X";

		int seqLen = seq.length();
		if ( seqLen < merSize ) { continue; }
		totalEffSeqLen += seqLen;

		typedef sparse_hash_map< unsigned int, unsigned short > CountsHashTable;
		CountsHashTable counts;

		// compute for both fwd and rev strands   
		for ( int j = 0; j < 1; j++ )
		{
		  if ( j == 1 )
		  {
			nts = revcomp( nts );
		  }
		  
		  for ( int i = 1; i < nts.length() - merSize; i++ )
		  {
			string mer = nts.substr( i, merSize );	
			const char *cSeqStrPtr = mer.c_str();
		   
			unsigned int prefixCode1 =  PackedDNASequence::encode( cSeqStrPtr );
			unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( cSeqStrPtr + 4 );
			int j = 0;
			if ( 0 != validPrefixes.size() )
			{
				for ( ; j < validPrefixes.size(); j++ )
				{
					if ( strncmp( validPrefixes[ j ], cSeqStrPtr, strlen( validPrefixes[ j ] ) ) == 0 ) { break; }
				}
				if ( j == validPrefixes.size() ) { continue; }
			}
			if ( !isMerValid( cSeqStrPtr, merSize ) ) { continue; }
			
			tempPackedMer.setSeq( cSeqStrPtr );
			assert( prefixCode < g_numHashTables );
		

			boost::detail::spinlock &lock = *( v_pMerHashAccess[ prefixCode ] );
			{						
				// begin critical section
			   // boost::unique_lock< boost::shared_mutex > writeLock( mutex );
			   std::lock_guard< boost::detail::spinlock > writeLock( lock );
				
				HashTable::iterator iter = v_merHash[ prefixCode ]->find( tempPackedMer );
				if ( iter != v_merHash[ prefixCode ]->end() )
				{	
					unsigned int count = ( unsigned int ) iter->second; 
					mean += count;
					n++;
					
					CountsHashTable::iterator countsIter = counts.find( count );
					if ( countsIter != counts.end() )
					{
					   if ( countsIter->second < 65535 )
					   {
						   countsIter->second += 1;
					   }
					}
					else
					{
					   CountsHashTable::value_type v( count, 1 );
					   counts.insert( v );		
					   nDistinct++;						
					}
				}	
			}
		  }
		}				
		if ( n > 0 )
		{
			   
			   // find median
			   std::vector< unsigned int > countsList;
			   for( CountsHashTable::iterator countsIter = counts.begin(); countsIter != counts.end();
				   countsIter++ )
			   {
				  countsList.push_back( countsIter->first );
			   }
			   assert( counts.size() == nDistinct );
			   
			   std::sort( countsList.begin(), countsList.end() );
			  
			   long int midPoint = ( int ) n / 2;
			   long int soFar = 0;
			   unsigned int countIndex = 0;
			   while ( soFar < midPoint && countIndex < nDistinct - 1 )
			   {
				 CountsHashTable::iterator countsIter = counts.find( countsList[ countIndex ] );
				 soFar += countsIter->second;
				 countIndex++;
			   }
			   
			   unsigned int median = countsList[ countIndex ];
			   out << name << "\t" << n << "\t";
			   char meanStr[ 100 ];
			   sprintf( meanStr, "%.2f", ( float ) mean / n );
			   
			   out << meanStr;
			   out << "\t";
			   out << median << "\n";
			   
			   mersPrinted++;
		}  				
			

	}	
	
	out.close();
	    
	{
		std::lock_guard< boost::detail::spinlock > writeLock( globalCountersAccess );

		g_contigsStartProcessed += contigsStartProcessed;
		g_mersPrinted += mersPrinted;
		g_totalEffSeqlLen += totalEffSeqLen;
	
	}
}



/// Structure of the mergraph Ext array ( held for each mer )
// Slots:
// 0-5 = # of high-q extensions off left end for each baseCode
// 6-11 = # of high-q extensions off right end for each baseCode
// 12 - dummy
// 13 - 16 - log likelihoods per base for left end extensions
// 17 - 20 - log likelihoods per base for right end extensions

int main( int argc, char *argv[] )
{
  if ( argc < 6 )
  {
	cerr << "usage: [ mercount_file_prefix ] [ mer_size ] [ min_depth ] [ prefixListToHash ] [ numThreads ] [ outputPrefix ] [ contigsFile ]\n" << endl;
	cerr << "mercount_file_prefix: we assume numThreads number of such files" << endl;
    exit( 0 );
  }
  string mercountFilePrefix = boost::lexical_cast< string > ( argv[ 1 ] );
  int merSize = boost::lexical_cast< int >( argv[ 2 ] );
  bool revComp = 1;
  int minDepth = boost::lexical_cast< int >( argv[ 3 ] ); 
  const char *prefixList = argv[ 4 ];
  int numTotalThreads = boost::lexical_cast< int >( argv[ 5 ] );
  int numWorkerThreads = numTotalThreads;
  string outputPrefix = argv[ 6 ];
  const char *contigsFileName = argv[ 7 ];

  string testFilename = mercountFilePrefix + boost::lexical_cast< string >( "." ) + boost::lexical_cast< string >( numWorkerThreads );
  if ( boost::filesystem::exists( testFilename ) )
  {
  	cerr << "the number of mercount input files must match the number of threads" << endl;
  	exit( -1 );
  }


  boost::thread threads[ numWorkerThreads ];
  HashTable merHash;
  int cnt = 0;
  int prefixLength = 4;
  int numEntries = 1000000000;  // 1 billion
   PackedDNASequence::init( 0, merSize );

  vector< string > prefixes;
  vector< const char * > validPrefixes;

  if ( strcmp( "-", prefixList ) != 0 )
  {
    boost::split( prefixes, prefixList, boost::is_any_of( "+" ) ); 
    assert( prefixes.size() > 0 );
    for( int i = 0 ; i < prefixes.size(); i++ )
    {
 	    validPrefixes.push_back( prefixes[ i ].c_str() );
    }
  }
  for ( int i = 0; i < g_numHashTables; i++ )
  {
     HashTable *h = new HashTable( numEntries / g_numHashTables * 2 );
     v_merHash.push_back( h );     
  }		
  for( int i =0; i < g_numMutexes; i++ )
  {
     boost::detail::spinlock *m = new boost::detail::spinlock();
     v_pMerHashAccess.push_back( m );
  }
  



  for( int i = 0; i < numWorkerThreads; i++ )
  {
	  string filename = mercountFilePrefix + boost::lexical_cast< string >( "." ) + boost::lexical_cast< string >( i );
	  
	  threads[ i ] = boost::thread( loadMers, filename, merSize, minDepth, validPrefixes );
  }

  for( int i = 0; i < numWorkerThreads; i++ )
  {
		threads[ i ].join();
  }
  cerr << "done loading mers" << endl;
  dumpTime();
  
  cerr << "processing contigs" << endl;
	
  for( int threadI = 0; threadI < 1; threadI++ )
  {
  	 string outputFileName = outputPrefix;
     outputFileName += ".";
     outputFileName += boost::lexical_cast< string >( threadI );
	 threads[ threadI ] = boost::thread( processContigs, contigsFileName, outputFileName, merSize, validPrefixes, numWorkerThreads, threadI );	 
  }
  
  for ( int i =0; i < numWorkerThreads; i++ )
  {
     threads[ i ].join();
  }

    
  cerr <<  "Found " << g_contigsStartProcessed << " total contigs spanning " << g_totalEffSeqlLen << " total bases" << endl;
  cerr << "Read " << g_mersRead << " mers as input" << endl;
  cerr << "Added " << g_mersAdded << " mers to the hash ( after filtering for depth + prefix )" << endl;
  cerr << "Printed " << g_mersPrinted << " mers.\n";
  cerr << "Done." << endl;
  
  return( 0 );
}


