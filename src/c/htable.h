/*
 * Tessellation Many-core Operating System. V2. 
 * 
 * Cell runtime.
 *
 * Copyright (c) 2014 The Regents of the University of California.
 *
 * This code is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 only, as published by
 * the Free Software Foundation.
 *  
 * This code is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 only, as published by
 * the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License version 2 for more
 * details (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version 2
 * along with this work; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _HTABLE_H
#define _HTABLE_H

#include <sys/types.h>

#define NO_FREE 0
#define FREE_KEYS 1
#define FREE_VALS 2

typedef struct htable *htable_t;
typedef struct htable_iter *htable_iter_t;

// returns NULL on error and sets errno
htable_t create_htable(size_t min_capacity, char *description);
void destroy_htable(htable_t h, int to_free);

// returns 0 on success, errno otherwise
int htable_put(htable_t h, char *key, void *val);
// type safety
#define DEFINE_HTABLE_PUT(func_name, value_type)                    \
    int func_name(htable_t h, char *key, value_type *val)           \
    {                                                               \
        return htable_put(h, key, val);                             \
    }

// returns NULL if not found
void *htable_get(htable_t h, char *key);
// type safety
#define DEFINE_HTABLE_GET(func_name, value_type)              \
    value_type *func_name(htable_t h, char *key)              \
    {                                                         \
        return (value_type *)(htable_get(h, key));            \
    }

void *htable_del(htable_t h, char *key);
// type safety
#define DEFINE_HTABLE_DEL(func_name, value_type)              \
    value_type *func_name(htable_t h, char *key)              \
    {                                                         \
        return (value_type *)(htable_del(h, key));            \
    }

int htable_num_entries(htable_t h);

htable_iter_t htable_get_iter(htable_t h);
void *htable_get_next(htable_t h, htable_iter_t iter);

long mem_htable(htable_t h);

void test_htable(void);

#endif /* _HASHTABLE_H_ */

