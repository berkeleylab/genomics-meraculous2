#include <time.h>
#include <boost/thread/thread.hpp>
#include <boost/unordered_map.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <boost/signals2/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <sparsehash/sparse_hash_map>
#include <boost/smart_ptr/detail/spinlock.hpp>
#include <mutex>
#include <iostream>
#include <fstream>
#include <vector>
#include <string> 
#include <algorithm>

#include "PackedDNASequence.hpp"  

using google::sparse_hash_map;

#define MAX_LINE_LEN 1000
#define PAGES_PER_BUFFER 1000

#define NOT_RECIPROCAL 1
#define NOT_UU  2
#define NOT_INHASH 3
#define PREV_VISITED_BY_ME 4
#define PREV_VISITED_BY_OTHER 5
using namespace std;


typedef struct
{
  unsigned char prevCode;  // A/C/G/T/F/X
  unsigned char nextCode;
  int bVisited;
} HashValue;

typedef PackedDNASequence PackedMer;
boost::detail::spinlock contigIdAccess;
unsigned long int g_contigId = 1; // XXX



class CompareKeys   
{
public: 
  inline bool operator()( const PackedMer &a, const PackedMer &b ) const
  {
    return( PackedDNASequence::equals( a, b ) );
  }
};


class HashKeys
{
public:
  inline std::size_t operator()( const PackedMer &a ) const
  {
   return PackedDNASequence::hash( a ); 
 }
};

typedef sparse_hash_map< PackedMer, HashValue, HashKeys, CompareKeys > HashTable;
typedef sparse_hash_map< PackedMer, bool, HashKeys, CompareKeys > SecondaryHashTable;
typedef sparse_hash_map< PackedMer, std::pair< long int, bool >, HashKeys, CompareKeys > AuxHashTable;
typedef sparse_hash_map< long int, std::pair< long int, bool >> Id2AttribHashTable;
typedef sparse_hash_map< long int, std::string >Id2StringHashTable;
typedef sparse_hash_map< long int, PackedMer > SecondaryAuxHashTable;

inline bool packedMerLessThan( const PackedMer &a, const PackedMer &b )
{
  return( a < b );
}
#define THREAD_MAX 128

HashTable g_merHash; // the main mer hash, stores mer => ufx code
HashTable vHash[ THREAD_MAX ]; // temporary hashes for loading mers in parallel
std::vector< PackedMer > g_leastMers;


SecondaryHashTable g_currentTraversalHash[ THREAD_MAX ]; // tracks the ( canonical ) mers visited in the current traversal for a given thread
int merSize = -1;
string outputPrefix = "temp";
unsigned int g_minContigSize;
ofstream ceaOut;

// must lock these global vars
boost::detail::spinlock globalCountersAccess;
unsigned long int g_readsStartProcessed = 0;
unsigned long int g_totalEffSeqlLen = 0;
unsigned long int g_mersAdded = 0;
unsigned long int g_mersPrinted = 0;
unsigned long int g_mersRead = 0;
unsigned long int g_mersExcluded = 0;
unsigned long int g_basesDiscarded= 0;
unsigned long int g_basesPrinted = 0;
unsigned long int g_totalShortContigBases = 0;
unsigned long int g_nShortContigs = 0;
unsigned long int g_totalLongContigBases = 0;
unsigned long int g_nLongContigs = 0;

AuxHashTable g_leastMerPerContigHash; // marks the lexicographically least mer for
	// each completed contig.  This allows duplicate contigs from different threads to be discarded.
SecondaryAuxHashTable g_oldContigId2LeastMer, g_newContigId2LeastMer;

string decodeMer( unsigned int x, unsigned int merLen )
{
  int remainder = x;
  string mer = "";
  for( int slot = merLen - 1; slot >= 0; slot-- )
  {
    int valInSlot =  int( remainder / pow( 4,slot ) );
    char base = ' ';

    if( valInSlot == 0 ) { base = 'A'; }
    else if( valInSlot == 1 ) { base = 'C'; }
    else if( valInSlot == 2 ) { base = 'G'; }
    else if( valInSlot == 3 ) { base = 'T'; }
    else { assert( 0 ); }

    mer += boost::lexical_cast< string >( base );

    remainder -= valInSlot * pow( 4, slot  );
  }
  return( mer );
}

inline unsigned long int encode( const char *s, const int &s_len )
{
  unsigned int x = 0;

  unsigned int slotVal = 1;
  for ( int i=s_len-1; i>=0; i-- )
  {
    int val = -1;
    switch( s[ i ] )
    {
      case 'A': val = 0; break;
      case 'C': val = 1; break;
      case 'G': val = 2; break;
      case 'T': val = 3; break;
      default:
      return( -1 );
    }
    x += ( val * slotVal );
    slotVal *= 4;
  } 
  return( x );
}

inline string reverse( const string &s )
{
  string rs = s; // allocate same amount of memory
  int rsCsr = 0;
  for ( int i = s.length() - 1; 
   i >= 0;
   i-- )
  {
    rs[ rsCsr ] = s[ i ];
    rsCsr++;
  }
  return( rs );
}



inline string revcomp( const string &s )
{
	string rs = s;// allocate same amount of memory
	
	int rsCsr = 0;
	for( int i = s.length() - 1; 
		i >= 0;
		i-- )
	{
		switch( s[ i ] )
		{
			case 'A': 
      rs[ rsCsr ] = 'T'; break;
      case 'T':
      rs[ rsCsr ] = 'A'; break;
      case 'C':
      rs[ rsCsr ] = 'G'; break;
      case 'G':
      rs[ rsCsr ] = 'C'; break;
      default:
      rs[ rsCsr ] = s[ i ];
    }
    rsCsr++;
  }
  return( rs );
}

inline char revcomp( const char x )
{
  switch( x )
  {
   case 'A': return( 'T' ); 
   case 'C': return( 'G' ); 
   case 'G': return( 'C' ); 
   case 'T': return( 'A' ); 
 }
 return( x );
}

inline bool isABase( const char x )
{
  switch( x )
  {
   case 'A':
   case 'C':
   case 'G':
   case 'T':
   return( true );
 }
 return( false );
}

inline int getBaseIdx( char c )
{
  switch( c ) {
    case 'A': return( 0 ); break;
    case 'C': return( 1 ); break;
    case 'G': return( 2 ); break;
    case 'T': return( 3 ); break;
    case 'N': return( 4 ); break;
    case 'X': return( 5 ); break;
    default: 
    cerr << "Unsupported character " << c << endl;
    return( 4 );
  }
}

inline bool isMerValid( const char *s, int len )
{
  for( int i = 0; i < len; i++ )
  {
    if ( !isABase( s[ i ] ) ) { return( false ); }
  }
  return( true );
}

void dumpTime()
{
	time_t rawtime;
	struct tm *timeinfo;
	char buffer [200];
	time( &rawtime );
	timeinfo = localtime( &rawtime );
	strftime (buffer,200,"%c",timeinfo);	
	cerr << buffer << endl;

}

// load UFX files
void loadMers( int threadId, vector< string > vFilenames, int merSize )
{
  unsigned long int mersRead = 0;
  unsigned long int mersAdded = 0;

  for ( int i = 0; i < vFilenames.size(); i++ )
  {
    ifstream ufxIn( vFilenames[ i ] );
    assert( !ufxIn.eof() );
    cerr << "loading mers from " << vFilenames[ i ] << endl;

    // load and prepare mer hash		  
    PackedDNASequence tempPackedMer;	
    char str[ 1000 ];		 

    while( ufxIn.getline( &str[0], 10000 ) )
    {
      mersRead++;
      if ( ( mersAdded % 1000000 ) == 0 ) { cerr << "."; }
      typedef vector< string > SplitVec;
      SplitVec lineCols;

      // faster than boost::split()
      const char *x = str;
      const char *cSeqStrPtr = x;
      const char *prevNextCodes = x;
      while( *x != '\t' && *x != ' ' )
      {
        x++;
      }
      prevNextCodes = x + 1;

      HashValue value;
      value.prevCode = *prevNextCodes;
      value.nextCode = *(prevNextCodes + 1 );
      value.bVisited = 0;

		  // new mer
      tempPackedMer.setSeq( cSeqStrPtr );
      HashTable::value_type v( tempPackedMer, value );
      vHash[ threadId ].insert( v );								

      mersAdded++;
    }
    ufxIn.close();
  }
  int size = vHash[ threadId ].size();
	// begin critical section
  {
    std::lock_guard< boost::detail::spinlock > writeLock( globalCountersAccess );

    g_mersAdded += mersAdded;
    g_mersRead += mersRead;
  }
}

// as opposed to STL func, this one stops if you hit the end
template < typename T, typename Iter >
void stlSafeAdvance( T &c, Iter &iter, int x )
{
	int i = 0;
	while ( iter != c.end() && i != x )
	{
		iter++;
		i++;
	}
}

// looks up prev/next code in master hash and revcomps results if needed, but does not
//   report the visitation flags ( which depend on threadId )
bool lookUpMerSimple( string mer, HashValue *val )
{
  string rcMer = revcomp( mer );
  bool rcFlag = false;
  string canonicalMer = mer;
  if ( rcMer < mer )
  {
    canonicalMer = rcMer;
    rcFlag = true;
  }
  HashValue dummy;
  dummy.prevCode = '-';
  dummy.nextCode = '-';
  *val = dummy;

  PackedDNASequence packedMer;
  packedMer.setSeq( canonicalMer.c_str() );

  HashTable::iterator i = g_merHash.find( packedMer );
  if ( i != g_merHash.end() ) 
  {
    *val = i->second;
    if ( rcFlag )
    {
      val->prevCode = revcomp( i->second.nextCode );
      val->nextCode = revcomp( i->second.prevCode );
    }    
  }
  return( !( i == g_merHash.end() ) );
}

// returns: true if it exists in the G_merhash  
//   use the o_b* flags to determine visitation specifics
// Note that the hashValue appears in two different places, the *iter and the *val...BUT
//   the *iter is meant to be used only as a pointer in the hashtable ( for efficiency if you want to
//   overwrite the values there ).   Its contents refer to the canonicalMer associated with "mer".   
//   the "val" pointer's values however have been adjusted to match "mer" itself.  Thus the client should
//   use the HashValue provided by *val, not *iter, when referencing "mer".
bool lookUpMer( string mer, HashTable::iterator *iter, HashValue *val, int threadId,
  bool &o_bHasBeenVisitedByMe, bool &o_bHasBeenVisitedByAnother )
{
  string rcMer = revcomp( mer );
  bool rcFlag = false;
  string canonicalMer = mer;
  if ( rcMer < mer )
  {
    canonicalMer = rcMer;
    rcFlag = true;
  }
  o_bHasBeenVisitedByMe = false;
  o_bHasBeenVisitedByAnother = false; 
  

  HashValue dummy;
  dummy.prevCode = '-';
  dummy.nextCode = '-';
  *val = dummy;

  PackedDNASequence packedMer;
  packedMer.setSeq( canonicalMer.c_str() );

  SecondaryHashTable::iterator traversalI = g_currentTraversalHash[ threadId ].find( packedMer );
  o_bHasBeenVisitedByMe = traversalI != g_currentTraversalHash[ threadId ].end();

  HashTable::iterator i = g_merHash.find( packedMer );
  if ( i != g_merHash.end() ) 
  {
    *val = i->second;
    if ( rcFlag )
    {
      val->prevCode = revcomp( i->second.nextCode );
      val->nextCode = revcomp( i->second.prevCode );
    }    
    *iter = i;
    
    o_bHasBeenVisitedByAnother = ( i->second.bVisited != 0 );

  }
  return( !( i == g_merHash.end() ) );

}


void hideElement( string mer, int threadId )
{
  string rcMer = revcomp( mer );
  string canonicalMer = mer;
  if ( rcMer < mer )
  {
    canonicalMer = rcMer;
  }
  PackedDNASequence packedMer;
  packedMer.setSeq( canonicalMer.c_str() );
  
  SecondaryHashTable::value_type v( packedMer, true );
  g_currentTraversalHash[ threadId ].insert( v ); 
  
}

bool isMerGood( const string &mer, const string &nextMer, const HashValue &nextMerInfo )
{
  char nextPrevBase = nextMerInfo.prevCode;
  char nextNextBase = nextMerInfo.nextCode;

  return ( isABase( nextMerInfo.prevCode ) && isABase( nextMerInfo.nextCode ) &&
   ( nextPrevBase == *( mer.c_str() ) )
   );
}


string getCanonicalMer( string mer )
{
  string rcMer = revcomp( mer );
  string canonicalMer = mer;
  if ( rcMer < canonicalMer )
  {
    canonicalMer = rcMer;
  }

  return canonicalMer;
}



string walk( string currentMer, HashTable::iterator currentMerIter, HashValue currentMerInfo, 
 string &o_leastMer, int threadId, int &o_terminationCode, bool &o_bDiscardWalk, bool &o_bVisitedByMe )
{
  string addBase = currentMer.substr( currentMer.length() - 1, 1 );
  string walkStr = "";
  o_bDiscardWalk = false;
  
  while( true ) 
  {
    const char prevBase = currentMerInfo.prevCode;
    const char nextBase = currentMerInfo.nextCode;
    string nextMer = currentMer + nextBase;
    nextMer.erase( 0, 1 );
    HashValue nextMerInfo;
    HashTable::iterator nextMerIter;
    bool bVisitedByAnother = false;

    // due to weird border cases we must check the currentMer's visitation again ( e.g., pallindromes )
    lookUpMer( currentMer, &currentMerIter, &currentMerInfo, threadId, o_bVisitedByMe, bVisitedByAnother );
    if ( o_bVisitedByMe )
    {
      break;      
    }

    // the general principle: 
    // if you add the nextBase, then you officially have visited the currentMer, and
    //   thus the o_leastMer must be compared against / updated
    bool bInHash = lookUpMer( nextMer, &nextMerIter, &nextMerInfo, threadId, o_bVisitedByMe, bVisitedByAnother );
    char nextPrevBase = nextMerInfo.prevCode;
    char nextNextBase = nextMerInfo.nextCode;

    bool bIsUU = isABase( nextMerInfo.prevCode ) && isABase( nextMerInfo.nextCode );
    bool bIsReciprocal = nextPrevBase == *( currentMer.c_str() );

    bool bAddNextBase = true;
    bool bTerminateWalk = false;

    // decide whether to terminate and/or add the next base
    if ( bInHash && !o_bVisitedByMe )
    {
      if ( bIsUU )
      {
        if ( bIsReciprocal ) 
        {
          // the typical case: add the next base and keep iterating
        }
        else
        {
          o_terminationCode = NOT_RECIPROCAL;
          bTerminateWalk = true; 

          bAddNextBase = false;
        }
      }
      else // not UU
      {       
        o_terminationCode = NOT_UU;
        bTerminateWalk = true;
        bAddNextBase = true;
      }
    } 
    else // !bInHash or bVisitedByMe
    {
      bTerminateWalk = true;
      if ( o_bVisitedByMe ) { o_terminationCode = PREV_VISITED_BY_ME; }
      else { o_terminationCode = NOT_INHASH; }

      bAddNextBase = true;
    }

    // finish up
    if ( bAddNextBase )
    {
      walkStr += addBase;
      hideElement( currentMer, threadId );

      // you've officially visited "currentMer"
      if ( getCanonicalMer( currentMer ) < o_leastMer  || o_leastMer.size() == 0 )
      {
        o_leastMer = getCanonicalMer( currentMer ) ;
      }
    }
    if ( bTerminateWalk )
    {
      break;
    }

    // go to the next mer
    currentMer = nextMer;
    currentMerIter = nextMerIter;
    currentMerInfo = nextMerInfo;
    addBase = nextBase;

  } // END while loop
  
  return( walkStr );
}

void printCea( ofstream &out, int myContigId, string bases, string leastMer )
{
  string firstMer = bases.substr( 0, merSize );
  HashValue firstMerInfo; 
  bool dummy;
  bool x = lookUpMerSimple( firstMer, &firstMerInfo );
  if ( !isABase( firstMerInfo.prevCode ) )
  {
    cerr << "prevCode not a base: firstMer = " << firstMer << " prevCode: " << firstMerInfo.prevCode << " for contig " << myContigId << " bases " << bases << endl;
  }
  assert( isABase( firstMerInfo.prevCode ) );

  string prevMer = ( const char ) firstMerInfo.prevCode + firstMer;
  prevMer.erase( merSize, 1 );
  HashValue prevMerInfo;
  x = lookUpMerSimple( prevMer, &prevMerInfo );

  string lastMer = bases.substr( bases.length() - merSize, merSize );
  HashValue lastMerInfo;
  x = lookUpMerSimple( lastMer, &lastMerInfo );
  if ( !isABase( lastMerInfo.nextCode ) )
  {

    cerr << "nextCode not a base " << ( x ? "(found)" : "(NOT FOUND)" ) << " lastMer = " << lastMer << " nextCode: " << lastMerInfo.nextCode << " for contig " << myContigId << " bases " << bases << endl;
  }
  assert( isABase( lastMerInfo.nextCode ) );

  string nextMer = lastMer + ( const char ) lastMerInfo.nextCode;
  nextMer.erase( 0, 1 );
  HashValue nextMerInfo;
  x = lookUpMerSimple( nextMer, &nextMerInfo );

  out << "Contig" << myContigId << "\t"  
  "[" << prevMerInfo.prevCode << prevMerInfo.nextCode << "]\t" << 
  "(" << firstMerInfo.prevCode << ")\t" <<
  firstMer << "\t" <<
  bases.length() << "\t" <<
  lastMer << "\t" <<
  "(" << lastMerInfo.nextCode << ")\t" << 
  "[" << nextMerInfo.prevCode << nextMerInfo.nextCode << "] " << leastMer << endl;
}


void printFasta( ofstream &out, int myContigId, string bases, string leastMer, string seed, int threadId )
{	
  string firstMer = bases.substr( 0, merSize );
  HashTable::iterator firstMerIter;
  HashValue firstMerInfo;	
  bool dummy;
  bool x = lookUpMer( firstMer, &firstMerIter, &firstMerInfo, threadId, dummy, dummy );
  assert( isABase( firstMerInfo.prevCode ) );

  string prevMer = ( const char ) firstMerInfo.prevCode + firstMer;
  prevMer.erase( merSize, 1 );
  HashTable::iterator prevMerIter;
  HashValue prevMerInfo;
  x = lookUpMer( prevMer, &prevMerIter, &prevMerInfo, threadId, dummy, dummy );

  string lastMer = bases.substr( bases.length() - merSize, merSize );
  HashTable::iterator lastMerIter;
  HashValue lastMerInfo;
  x = lookUpMer( lastMer, &lastMerIter, &lastMerInfo, threadId, dummy, dummy );
  assert( isABase( lastMerInfo.nextCode ) );
  string nextMer = lastMer + ( const char ) lastMerInfo.nextCode;
  nextMer.erase( 0, 1 );
  HashTable::iterator nextMerIter;
  HashValue nextMerInfo;
  x = lookUpMer( nextMer, &nextMerIter, &nextMerInfo, threadId, dummy, dummy );

  out << ">Contig" << myContigId << endl;
  int bpl = 50;
  int csr = 0;
  while( csr < bases.length() )
  {
    out << bases.substr( csr, 50 ) << endl;
    csr += 50;
  }
  
  {
    std::lock_guard< boost::detail::spinlock > writeLock( globalCountersAccess );
    g_basesPrinted += bases.length();
  }
}

void generateContigs( int threadId, int threadCnt, string seedListFileName )
{  
  std::vector< string > seedList;
  char lineBuf[ 1000 ];
  if ( seedListFileName.length() > 0 )
  {
    ifstream seedListFile;
    seedListFile.open( seedListFileName );

    while( !seedListFile.eof() )
    {
      seedListFile.getline( lineBuf, 1000 );
      seedList.push_back( boost::lexical_cast< string >( lineBuf ) );
    }
  }

  string name = outputPrefix;
  name += ".";
  name += boost::lexical_cast< string > ( threadId );
  ofstream out( name );

  bool bUseSeedList = ( seedList.size() != 0 );
  HashTable::iterator iter = g_merHash.begin();
  stlSafeAdvance< HashTable, HashTable::iterator >( g_merHash, iter, threadId );

  std::vector< string >::iterator seedIter = seedList.begin();
  stlSafeAdvance< std::vector< string >, std::vector< string >::iterator >( seedList, seedIter, threadId );


  for( ; bUseSeedList ? seedIter != seedList.end() : iter != g_merHash.end();
    bUseSeedList ? stlSafeAdvance< std::vector< string >, std::vector< string >::iterator > ( seedList, seedIter, threadCnt ) :
    stlSafeAdvance< HashTable, HashTable::iterator > ( g_merHash, iter, threadCnt ) )
  {
    bool bVisitedByMe = false; 
    bool bVisitedByAnother = false;  
    if ( bUseSeedList )
    {
      HashValue seedInfo;
      lookUpMer( *seedIter, &iter, &seedInfo, threadId, bVisitedByMe, bVisitedByAnother );
    }
    string leastMer = "";

    char buf[ MAX_LINE_LEN ];  
    char *c = &buf[0];
    iter->first.convert( &c );
    string mer( buf );

    // already visited, do not use as seed
    if ( iter->second.bVisited ) { continue; }

    // not a U-U mer, do not use as seed
    const char prevCode = iter->second.prevCode;
    const char nextCode = iter->second.nextCode;
    if ( !( isABase( prevCode ) && isABase( nextCode ) ) )
    {
      continue;
    }
    
    bool bTakeFwdWalk = false;
    bool bTakeRevWalk = false;
    string nextMer = mer + nextCode;
    nextMer.erase( 0, 1 );
    
    HashTable::iterator nextMerIter;
    HashValue nextMerInfo;


    string rcMer = revcomp( mer );

    if ( lookUpMer( nextMer, &nextMerIter, &nextMerInfo, threadId, bVisitedByMe, bVisitedByAnother ) )    
    {
      bool bIsUU = isABase( nextMerInfo.prevCode ) && isABase( nextMerInfo.nextCode );  
      bool bIsReciprocal = nextMerInfo.prevCode == *( mer.c_str() );
      if ( bIsUU && !bIsReciprocal )
      {
        continue; // we bail on the entire contig if the seed fails to reciprocate correctly off the right end
      }
      bTakeFwdWalk = bIsUU && bIsReciprocal;   
    }
    
    string prevMer = boost::lexical_cast< string >( prevCode ) + mer;
    prevMer.erase( prevMer.length() - 1, 1 );	  
    string rcPrevMer = revcomp( prevMer );

    HashTable::iterator rcPrevMerIter;
    HashValue rcPrevMerInfo;
    bVisitedByMe = false; 
    bVisitedByAnother = false;

    if ( lookUpMer( rcPrevMer, &rcPrevMerIter, &rcPrevMerInfo, threadId, bVisitedByMe, bVisitedByAnother ) )
    {
      bool bIsUU = isABase( rcPrevMerInfo.prevCode ) && isABase( rcPrevMerInfo.nextCode );  
      bool bIsReciprocal = rcPrevMerInfo.prevCode == *( rcMer.c_str() );
      if ( bIsUU && !bIsReciprocal )
      {
        continue; // we bail on the entire contig if the seed fails to reciprocate correctly off the left end
      }
      bTakeRevWalk = bIsUU && bIsReciprocal;
    }
    
    string fwdWalk = "";
    string revWalk = "";

	  // you have officially visited the seed mer now
    string canonicalMer = mer;
    if ( rcMer < mer )
    {
      canonicalMer = rcMer;
    }
    if ( canonicalMer < leastMer || leastMer.size() == 0 )    
    {
      leastMer = canonicalMer;
    }

    hideElement( mer, threadId );  // you are a good mer in at least one direction, so mark yourself

    int terminationCodeFwd = 0;
    int terminationCodeRev = 0;

    bool bDiscardWalk = false;
    bool bVisitedByMeFwdWalk = false;
    if ( bTakeFwdWalk )
    {
      fwdWalk = walk( nextMer, nextMerIter, nextMerInfo, leastMer, threadId, terminationCodeFwd, bDiscardWalk,
        bVisitedByMeFwdWalk );
      if ( bDiscardWalk ) 
      { 
        {
          std::lock_guard< boost::detail::spinlock > writeLock( globalCountersAccess );
          g_basesDiscarded += fwdWalk.length();
        }
        continue; 
      }
    }

    bDiscardWalk = false;
    bool bVisitedByMeRevWalk = false;
    if ( bTakeRevWalk )
    {
      revWalk = walk( rcPrevMer, rcPrevMerIter, rcPrevMerInfo, leastMer, threadId, terminationCodeRev, bDiscardWalk,
        bVisitedByMeRevWalk );
      if ( bDiscardWalk ) 
      {   
        {
          std::lock_guard< boost::detail::spinlock > writeLock( globalCountersAccess );
          g_basesDiscarded += revWalk.length();
        }
        continue; 
      }
    }

    string result = revcomp( revWalk ) + mer + fwdWalk;
    bool bCheckInContig = false;  // count this contig as a real one, check it in

    long int contigId = 0;
    bool bMarkContig = false;
    bool bPrintContig = false;
    bool bIsCircular = bVisitedByMeFwdWalk || bVisitedByMeRevWalk;

    // if it's circular, you want to trim k - 1 bases right now and recompute leastMer
    if ( bIsCircular )
    {
      // if it's truly circular ( completely ) then the first k-1 bases should equal the last k-1 bases.
      if ( 0 != result.substr( 0, merSize - 1 ).compare( result.substr( result.length() - ( merSize - 1 ), merSize - 1 ) ) )
      {
          cerr << "contig flagged as circular but first/last k-1 bases don't match for bases: " << result << endl;
          
          continue;
      }
      leastMer = "";
      cerr << "circular contig: old = " << result << " ";

      result = result.substr( 0, result.length() - ( merSize - 1 ) );
      cerr << " new result = " << result << endl;

      if ( result.length() < merSize )
      {
        // too small, eject
        continue;
      }
        
        
      for ( int i = 0; i < result.length() - merSize + 1; i++ )
      {
        cerr << "i: " << i; 
        string tempMer = result.substr( i, merSize );
        string tempMerRc = revcomp( tempMer );
        string canonicalTempMer = ( tempMer.compare( tempMerRc ) < 0 ? tempMer : tempMerRc );

        if ( leastMer.length() == 0 || canonicalTempMer.compare( leastMer ) < 0 )
        {
          leastMer = canonicalTempMer;
        }
      }
      cerr << " new = " << result << " leastMer = " << leastMer << endl;
    }

    // complete the contig.  Check it in.
    PackedDNASequence packedLeastMer;
    packedLeastMer.setSeq( leastMer.c_str() );
    {  
      std::lock_guard< boost::detail::spinlock > writeLock( globalCountersAccess );

      // only print the contig if it hasn't been printed already and is long enough
      AuxHashTable::iterator lmpcIter = g_leastMerPerContigHash.find( packedLeastMer );
      if ( lmpcIter == g_leastMerPerContigHash.end() )
      {
        if ( result.length() >= g_minContigSize )
        {
          contigId = g_contigId;
          g_contigId++;

          std::pair< long int, bool > value( contigId, bIsCircular );

          AuxHashTable::value_type v( packedLeastMer, value );
          // new entry       
          g_leastMerPerContigHash.insert( v );

          SecondaryAuxHashTable::value_type v2( contigId, packedLeastMer );
          g_oldContigId2LeastMer.insert( v2 );
          cerr << "oldContigId2LeastMer: " << contigId << " " << leastMer.c_str() << endl;
          g_leastMers.push_back( packedLeastMer );
          bPrintContig = true;
        }

        bMarkContig = true;  	

      }
    }

    // this is split out for efficiency--don't wanna sustain lock over this stage
    if ( bMarkContig )
    {    	  
      // mark all the nodes you visited in the permanent hash
      for( SecondaryHashTable::iterator traversalI = g_currentTraversalHash[ threadId ].begin(); 
        traversalI != g_currentTraversalHash[ threadId ].end(); 
        traversalI++ )
      {
        HashTable::iterator merI = g_merHash.find( traversalI->first );
        if ( merI != g_merHash.end() )
        {
          g_merHash[ traversalI->first ].bVisited = true;
        }     
      }
      g_currentTraversalHash[ threadId ].clear();
      const char *terminationCodes[] = 
      { "-", "NOT_RECIPROCAL", "NOT_UU", "NOT_INHASH", "PREV_VISITED_BY_ME", "PREV_VISITED_BY_OTHER" };

      if ( bPrintContig )
      {
        printFasta( out, contigId, result, leastMer, mer, threadId );
	//        cerr << "Contig" << contigId << " seed: " << canonicalMer << " terminationCodes: ";
	//        cerr << terminationCodes[ terminationCodeFwd ] << " " << 
        // terminationCodes[ terminationCodeRev ] << endl;

        {
          std::lock_guard< boost::detail::spinlock > writeLock( globalCountersAccess );
          g_totalLongContigBases += result.length();
          g_nLongContigs++;
        }
      }
      else
      {
        std::lock_guard< boost::detail::spinlock > writeLock( globalCountersAccess );
        g_basesDiscarded += result.length();
  	  } // end of bPrintContig
    } // end of bMarkContig

  }// end main seed loop

  out.close();
}

void prettyPrint( std::ofstream &out, std::string &s, int width )
{
  long int i = 0;
  // this is slow but we don't care
  while ( i < s.size() )
  {
    out << s.substr( i, width ) << endl;
    i += width;
  }
}

class MySerializer {
public:
  bool operator()(FILE *fp, const HashTable::value_type &v ) {
    fwrite( &v.first, sizeof( v.first ), 1, fp );
    return( true );
  }
  bool operator()(FILE *fp,  HashTable::value_type *v ) { 
    fgets( ( char * ) &(v->first), sizeof( *v ), fp );
    return( true ); 
  }
};

void dumpHash( int i, int numWorkerThreads )
{
  int increment = ceil( 65536 / numWorkerThreads );
  int minHashI = i * increment;
  int maxHashI = minHashI + increment - 1;
  MySerializer serializer;

  FILE *out = fopen( "test.hash", "wb" );
      g_merHash.serialize( serializer, out );
  
  fclose( out );
}
char *to_base(int64_t num, int base)
{
  char *tbl = "0123456789abcdefghijklmnopqrstuvwxyz";
  char buf[66] = {'\0'};
  char *out;
  uint64_t n;
  int i, len = 0, neg = 0;
  if (base > 36) {
    fprintf(stderr, "base %d too large\n", base);
    return 0;
  }
 
  /* safe against most negative integer */ 
  n = ((neg = num < 0)) ? (~num) + 1 : num;
 
  do { buf[len++] = tbl[n % base]; } while(n /= base);
  buf[ len ] = '\0';
  out = (char*)malloc(len + neg + 1);
  out[ len ] = '\0';

  for (i = neg; len > 0; i++) out[i] = buf[--len];
  if (neg) out[0] = '-';
  
  return out;
}
 
const char *generateMer( int i, int merSize )
{
   // convert to base 4
   char *buf = to_base( i, 4 );
   // replace with A/C/G/T
   for ( int i=0; i< strlen( buf ); i++ ) {
     switch( buf[i] ) {
        case '0': buf[i] = 'A'; break;
        case '1': buf[i] = 'C'; break;
        case '2': buf[i] = 'G'; break;
        case '3': buf[i] = 'T'; break;
     }
   }

   std::string s = "";
   for ( int j = 0; j < merSize - strlen( buf ); j++ ) {
      s += "A";
   }
   s+=buf;
   return s.c_str();
}

void loadHash()
{
  MySerializer serializer;
  HashTable merHash;

  FILE *in = fopen( "test.hash", "rb" );
  merHash.unserialize( serializer, in );
  fclose( in );
}


int mainTestHash( int argc, char *argv[] )
{
  int merSize = 51;
  PackedDNASequence::init( 0, merSize );
  int numMers = 10000000; 
  HashValue value;
  boost::thread myThreads[ 24 ];

  cerr << "start loading mers" << endl;
  dumpTime();
  for (int i = 0; i < 2; i++ ) {
    myThreads[i] = boost::thread( loadHash );
  }
  for ( int i = 0; i < 2; i++ ) {
    myThreads[i].join();
  }
  cerr << "done loading mers" << endl;
  dumpTime();
/*
  for ( int i =0 ; i<numMers; i++ )
  {
    PackedMer p;
    const char *s = generateMer( i, merSize );
    p.setSeq( s  );

    g_merHash[ p ] = value;
  }
  cerr << "dumping hash" << endl;
  dumpTime();
  FILE *out = fopen( "test.hash", "wb" );
  g_merHash.serialize( serializer, out );
  dumpTime();
  cerr << "done dumping hash" << endl;
  fclose( out );

  g_merHash.clear();

  cerr << "loading hash" << endl;
  dumpTime();
  for ( int i = 0)
  FILE *in = fopen( "test.hash", "rb" );
  g_merHash.unserialize( serializer, in );
  dumpTime();
  cerr << "done loading hash" << endl;
  fclose( in );
  char *mer = new char[ merSize + 1 ];

  for ( HashTable::iterator i = g_merHash.begin(); i != g_merHash.end(); i++ ) {
    const PackedMer *p = &( i->first );
    p->convert( &mer );  
    cout << mer << endl;
  }
  */
}

int main( int argc, char *argv[] )
{
  if ( argc < 8 )
  {
    cerr << "usage: [ ufx_file_wildcard ] [ mer_size ] [ numThreads ] [ output_file ] [ expected_num_elements ] [ min contig size ] [ debugMode ] [ seed_list_filename ]" << endl;
    exit( 0 );
  }
  string ufxWildCard = boost::lexical_cast< string > ( argv[ 1 ] );
  merSize = boost::lexical_cast< int >( argv[ 2 ] );
  int numWorkerThreads = boost::lexical_cast< int >( argv[ 3 ] );
  if ( numWorkerThreads > THREAD_MAX ){ cerr << "max # of threads is " << THREAD_MAX << endl; exit( -1 ); }
  outputPrefix = argv[ 4 ];
  long expectedNumElements = boost::lexical_cast< long >( argv[ 5 ] );
  g_minContigSize = boost::lexical_cast< int >( argv[ 6 ] );
  bool bDebugMode = boost::lexical_cast< bool >( argv[ 7 ] );
  string seedListFileName = argv[ 8 ];

  string cmd = "ls " + ( string ) ufxWildCard;
  FILE *inputWC = popen( cmd.c_str(), "r" );
  std::vector< string > vUFXIn;
  char buf[ 255 ];
  while( fgets( buf, 255, inputWC ) != NULL )
  {
    cerr << "using file: " << buf << endl;
    buf[ strlen( buf ) - 1 ] = '\0'; // remove trailing newline
    vUFXIn.push_back( buf );
  }
  fclose( inputWC );

  PackedDNASequence::init( 0, merSize );
  vector< string > prefixes;
  
  dumpTime();
  boost::thread threads[ numWorkerThreads ];

  int ufxFileI = 0;
  float ufxAmortizedFileTotal = 0; // adds in the float ufxFilesPerWorker each time for better balancing 
  float ufxAmortizedFilesPerWorker = vUFXIn.size() / ( float ) numWorkerThreads;

  
  vector< vector< string > > vvFileNames( numWorkerThreads );

  for( int i = 0; i < vUFXIn.size(); i++ ) {
    int t = i % numWorkerThreads; 
    vvFileNames[ t ].push_back( vUFXIn[ i ] );
  }
  for( int t = 0; t < numWorkerThreads; t++ )
  {
    threads[ t ] = boost::thread( loadMers, t, vvFileNames[ t ], merSize );
  }
  for( int t = 0; t < numWorkerThreads; t++ )
  {
    threads[ t ].join();
  }

  cerr << "done loading mers" << endl;
  dumpTime();

  for ( int i = 0; i < numWorkerThreads; i++ )
  {
    int size = vHash[ i ].size();
    vHash[ i ].resize( expectedNumElements / numWorkerThreads );
    g_merHash.insert( vHash[ i ].begin(), vHash[ i ].end() );
    vHash[ i ].clear();
  }

  cerr << "done combining mers into one hash" << endl;
  dumpTime();
  int size = g_merHash.size();

  cerr << "generating contigs" << endl;
  if ( bDebugMode ) 
  { 
    // generate contigs
    numWorkerThreads = 1;
  }
  string ceaName = outputPrefix;
  ceaName += ".cea";
  ceaOut.open( ceaName );

  for( int i =0; i < numWorkerThreads; i++ )
  {
    threads[ i ] = boost::thread( generateContigs, i, numWorkerThreads, seedListFileName );
  }
  for( int i = 0; i < numWorkerThreads; i++ )
  {
    threads[ i ].join();
  }
  cerr << "done generating contigs" << endl;  
  dumpTime();


  for ( int threadI = 0; threadI < numWorkerThreads; threadI++ )
  {
    threads[ threadI ].join();
  }

  // ensure reproducibility of outputs
  // 1.  Rename the contigs based on sorted order of leastmer
  // 2.  If a contig is circular, reprint it s.t. it starts
  //     with the leastMer
  // 3. reprint all contigs so they are in the same strand as the canonicalMer

  // sort keys of the g_leastMerPerContig hash
  std::sort( g_leastMers.begin(),
    g_leastMers.end(), packedMerLessThan );

  int maxNewContigId = 0;
  // create a mapping between old and new contig ids
  Id2AttribHashTable id2AttribHash;
  long int newContigId = 1;
  for( std::vector< PackedMer >::iterator i2 = g_leastMers.begin();
   i2 != g_leastMers.end();
   i2++ )
  {
    AuxHashTable::iterator i = g_leastMerPerContigHash.find( *i2 );
    assert( i != g_leastMerPerContigHash.end() );

    long int oldContigId = i->second.first;
    bool bIsCircular = i->second.second;
    std::pair< long int, bool > newValue( newContigId, bIsCircular );
    id2AttribHash[ oldContigId ] = newValue;

    const PackedMer *p = &( i->first );
    char *mer = new char[ merSize + 1 ];
    p->convert( &mer );

    cerr << "oldContig->newContig mapping: " << mer << "\t" << oldContigId << "\t" << newContigId<< endl;
    delete mer;
    newContigId++;
  }
  maxNewContigId = newContigId - 1;
  ofstream finalOut( outputPrefix );

  // do the processing
  Id2StringHashTable id2ContigSeqHashTable;

  for ( int threadI = 0; threadI < numWorkerThreads; threadI++ )
  {
    char *leastMer = new char[ merSize + 1 ];
    string name = outputPrefix;
    name += ".";
    name += boost::lexical_cast< string > ( threadI );
    ifstream in( name );

    bool bIsCircular = false;
    bool bFirst = true;
    long int newContigId = 0;
    long int oldContigId = 0;
    std::string line;
    std::string seq;
    while( std::getline( in, line ) )
    {
      if ( line.at( 0 ) == '>' )
      {
        // process the prev contig 
        if ( !bFirst )
        {
          bool bRevComp = false;


          // ensure that the printed contig is of same strand as leastMer, and in the case 
          //    of circular contigs, begins with leastMer ( all other checks/mods on circular
          //    contigs have already been performed )
          if ( bIsCircular )
          {
            std::string::size_type n = seq.find( leastMer );
            if ( n == std::string::npos )
            {
              seq = revcomp( seq );
              n = seq.find( leastMer );
              assert( n != std::string::npos );
            }
            cerr << "Circular: contig " << newContigId << " leastMer = " << leastMer << " n = " << n << " " << seq << "->";


            string s = seq.substr( n, seq.size() - n ) + seq.substr( 0, n  );

            seq = s;
            cerr << seq << endl;

          }
          else
          {
            if ( seq.find( leastMer ) == string::npos )
            {
              seq = revcomp( seq );
              bRevComp = true;
            }
          }
          cerr << "Contig" << newContigId << " " << oldContigId << " " << leastMer << 
          " " << bRevComp << " " << bIsCircular << endl;

          id2ContigSeqHashTable[ newContigId ] = seq;

          seq.erase();
        }
        if ( bFirst ) { bFirst = false; }

        // header -- rename the contig
        oldContigId = boost::lexical_cast< long int >( line.c_str() + 7 );
        newContigId = id2AttribHash[ oldContigId ].first;
        bIsCircular = id2AttribHash[ oldContigId ].second;
        const PackedMer *leastMerPacked = &( g_oldContigId2LeastMer[ oldContigId ] );
        leastMerPacked->convert( &leastMer );

        SecondaryAuxHashTable::value_type v2( newContigId, *leastMerPacked );
        g_newContigId2LeastMer.insert( v2 );
        
      }
      else
      {
        seq += line;
      }
    }

    // process the final contig 
    if ( bIsCircular )
    {
      // chop off k - 1 from the original contig sequence to get true circular pattern
      if ( seq.length() - merSize - 1 >= g_minContigSize )
      {   
        seq = seq.substr( 0, seq.length() - merSize - 1 );


        std::string::size_type n = seq.find( leastMer );
        if ( n == std::string::npos )
        {
          seq = revcomp( seq );
          n = seq.find( leastMer );
          assert( n != std::string::npos );
        }

        string s = seq.substr( n, seq.size() - n ) + seq.substr( 0, n  );

        seq = s;
        // clean up for next contig
        bIsCircular = false;
      }
    }
    else
    {
      if ( seq.find( leastMer ) == string::npos )
      {
        seq = revcomp( seq );
      }
    }
    id2ContigSeqHashTable[ newContigId ] = seq;

    seq.erase();

    delete leastMer;

  }



  // print contigs sorted by asc contigId
  char *leastMer = new char[ merSize + 1 ];
  for ( int contigId = 1; contigId <= maxNewContigId; contigId++ )
  {
    if ( id2ContigSeqHashTable[ contigId ].size() == 0 )
    {
      continue;
    }
   finalOut << ">Contig" << contigId << endl;
   prettyPrint( finalOut, id2ContigSeqHashTable[ contigId ], 80 );

   const PackedMer *leastMerPacked = &( g_newContigId2LeastMer[ contigId ] );
   leastMerPacked->convert( &leastMer );

   printCea( ceaOut, contigId, id2ContigSeqHashTable[ contigId ], leastMer );
 }
 delete leastMer;

 cerr << "Total bases in long contigs: " << g_totalLongContigBases << endl;
 cerr << "Total bases in short contigs: " << g_totalShortContigBases << endl;
 cerr << "Total bases discarded ( duplicate ): " << g_basesDiscarded << " %: " << setprecision( 1 ); 
 int denom = ( g_totalLongContigBases + g_totalShortContigBases + g_basesDiscarded );
 if ( denom > 0 )
 {
  cerr << ( long int ) 100.0 * ( g_basesDiscarded / denom ) << endl;
  }

  dumpTime();

  return( 0 );
}


