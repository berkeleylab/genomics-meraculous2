#include <time.h>
#include <boost/filesystem.hpp>
#include <boost/thread/thread.hpp>
#include <boost/unordered_map.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <boost/signals2/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <sparsehash/sparse_hash_map>
#include <boost/smart_ptr/detail/spinlock.hpp>
#include <mutex>
#include <iostream>
#include <fstream>
#include <vector>
#include <string> 
#include <algorithm>

#include "PackedDNASequence.hpp"  
#include "IOWorkerSimple.hpp"

using google::sparse_hash_map;

#define MAX_LINE_LEN ( ioWorker.getBufLineWidth() )
#define PAGES_PER_BUFFER 1000
using namespace std;


  
typedef struct
{
   unsigned short v[12];
} HashValue;

typedef PackedDNASequence PackedMer;

class CompareKeys   
{
public: 
  inline bool operator()( const PackedMer &a, const PackedMer &b ) const
  {
    return( PackedDNASequence::equals( a, b ) );
  }
};


class HashKeys
{
public:
  inline std::size_t operator()( const PackedMer &a ) const
  {
   return PackedDNASequence::hash( a ); 
  }
};

typedef sparse_hash_map< PackedMer, HashValue, HashKeys, CompareKeys > HashTable;
int g_hashPrefixLen = 8;  // 4**g_hashPrefixLen = total number of hashes, must be 4 b/c of PackedDNASequence::encode() coupling
int g_numHashTables = pow( 4, g_hashPrefixLen );
int g_numMutexes = g_numHashTables;
std::vector< HashTable * > v_merHash;
std::vector< boost::detail::spinlock * > v_pMerHashAccess;
IOWorkerSimple ioWorker;
boost::detail::spinlock globalCountersAccess;
unsigned long int g_readsStartProcessed = 0;
unsigned long int g_totalEffSeqlLen = 0;
unsigned long int g_mersAdded = 0;
unsigned long int g_mersPrinted = 0;
unsigned long int g_mersRead = 0;
unsigned long int g_mersExcluded = 0;
int qualOffset = 64;

int minQual=19;
string decodeMer( unsigned int x, unsigned int merLen )
{
    int remainder = x;
    string mer = "";
    for( int slot = merLen - 1; slot >= 0; slot-- )
    {
        int valInSlot =  int( remainder / pow( 4,slot ) );
        char base = ' ';

        if( valInSlot == 0 ) { base = 'A'; }
        else if( valInSlot == 1 ) { base = 'C'; }
        else if( valInSlot == 2 ) { base = 'G'; }
        else if( valInSlot == 3 ) { base = 'T'; }
        else { assert( 0 ); }
         
        mer += boost::lexical_cast< string >( base );
        
        remainder -= valInSlot * pow( 4, slot  );
    }
    return( mer );
}


inline unsigned long int encode( const char *s, const int &s_len )
{
  unsigned int x = 0;

  unsigned int slotVal = 1;
  for ( int i=s_len-1; i>=0; i-- )
  {
    int val = -1;
    switch( s[ i ] )
    {
      case 'A': val = 0; break;
      case 'C': val = 1; break;
      case 'G': val = 2; break;
      case 'T': val = 3; break;
      default:
        return( -1 );
    }
    x += ( val * slotVal );
    slotVal *= 4;
  } 
  return( x );
}

inline string reverse( const string &s )
{
  string rs = s; // allocate same amount of memory
  int rsCsr = 0;
  for ( int i = s.length() - 1; 
	i >= 0;
	i-- )
  {
    rs[ rsCsr ] = s[ i ];
    rsCsr++;
  }
  return( rs );
}



inline string revcomp( const string &s )
{
	string rs = s;// allocate same amount of memory
	
	int rsCsr = 0;
	for( int i = s.length() - 1; 
		i >= 0;
		i-- )
	{
		switch( s[ i ] )
		{
			case 'A': 
				rs[ rsCsr ] = 'T'; break;
			case 'T':
				rs[ rsCsr ] = 'A'; break;
			case 'C':
				rs[ rsCsr ] = 'G'; break;
			case 'G':
				rs[ rsCsr ] = 'C'; break;
			default:
				rs[ rsCsr ] = s[ i ];
		}
		rsCsr++;
	}
	return( rs );
}

inline char revcomp( const char x )
{
  switch( x )
  {
	  case 'A': return( 'T' ); 
	  case 'C': return( 'G' ); 
	  case 'G': return( 'C' ); 
	  case 'T': return( 'A' ); 
  }
	return( x );
}
inline bool isValid( const char x )
{
  switch( x )
  {
     case 'A':
     case 'C':
     case 'G':
     case 'T':
       return( true );
  }
  return( false );
}

inline int getBaseIdx( char c )
{
  switch( c ) {
  case 'A': return( 0 ); break;
  case 'C': return( 1 ); break;
  case 'G': return( 2 ); break;
  case 'T': return( 3 ); break;
  case 'N': return( 4 ); break;
  case 'X': return( 5 ); break;
  default: 
    cerr << "Unsupported character " << c << endl;
	  return( 4 );
  }
}

inline bool isMerValid( const char *s, int len )
{
  for( int i = 0; i < len; i++ )
  {
    if ( !isValid( s[ i ] ) ) { return( false ); }
  }
  return( true );
}

void dumpTime()
{
	time_t rawtime;
	struct tm *timeinfo;
	char buffer [200];
	time( &rawtime );
	timeinfo = localtime( &rawtime );
	strftime (buffer,200,"%c",timeinfo);	
	cerr << buffer << endl;

}


void loadMers( string filename, int merSize, int minDepth, vector< const char * > validPrefixes )
{
	ifstream mercountIn( filename );
	assert( !mercountIn.eof() );
	cerr << "loading mers from " << filename << endl;
	// load and prepare mer hash		  
	PackedDNASequence tempPackedMer;	
	char str[ 1000 ];		 
	unsigned long int mersRead = 0;
	unsigned long int mersAdded = 0;
	while( mercountIn.getline( &str[0], 10000 ) )
	{
	    mersRead++;
		if ( ( mersAdded % 1000000 ) == 0 ) { cerr << "."; }
		typedef vector< string > SplitVec;
		SplitVec lineCols;
		
		boost::algorithm::split( lineCols, str, boost::is_any_of(" \t") );
		const char *cSeqStrPtr = ( lineCols[ 0 ].c_str() );
		int count = boost::lexical_cast< int >( lineCols[ 1 ].c_str() );
		
		if ( count < minDepth ) { continue; }
		
		unsigned int prefixCode1 =  PackedDNASequence::encode( cSeqStrPtr );
		unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( cSeqStrPtr + 4 );
		int j = 0;
		if ( validPrefixes.size() > 0 )
		{
			for ( ; j < validPrefixes.size(); j++ )
			{
				if ( strncmp( validPrefixes[ j ], cSeqStrPtr, strlen( validPrefixes[ j ] ) ) == 0 ) { break; }
			}
			if ( j == validPrefixes.size() ) { continue; }
		}
		tempPackedMer.setSeq( cSeqStrPtr );
		
		assert( prefixCode < g_numHashTables );
				
		HashTable::iterator i = v_merHash[ prefixCode ]->find( tempPackedMer );
		{
			HashValue emptyHashValue = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			// new mer
			HashTable *h= v_merHash[ prefixCode ];
			HashTable::value_type v( tempPackedMer, emptyHashValue );
			h->insert( v );								
				    
			mersAdded++;
		}
	}
	mercountIn.close();
	
	// begin critical section
	{
		std::lock_guard< boost::detail::spinlock > writeLock( globalCountersAccess );
	
		g_mersAdded += mersAdded;
		g_mersRead += mersRead;
	}
}


void tallyMerExtensions( int merSize, vector< const char * > validPrefixes, int numThreads, int threadI  )
{
    char str[ 10000 ];
	PackedDNASequence tempPackedMer;
	
	int bufNumLines = ioWorker.getOutBufNumLines();
	int origBufNumLines = bufNumLines;
	int skipLineCnt = numThreads;
	int seqI = 0;
	char *bufLines = 0;
	bool bFirstTime = true;
	long unsigned int readsStartProcessed = 0;
	long unsigned int totalEffSeqLen = 0;
	long unsigned int mersExcluded = 0;
	cerr << "tallying mer extensions" << endl;
	while( ioWorker.getLines( threadI,  &bufLines, bufNumLines )  )
	{	    
		bFirstTime = false;
		for( int i = 0; i < bufNumLines;)
		{	
				readsStartProcessed++;
				
				char *header = &bufLines[ i * ioWorker.getBufLineWidth() ];
				i++;
				char *seqPtr = &bufLines[ i * ioWorker.getBufLineWidth() ];
				i++;
				char *spacer = &bufLines[ i * ioWorker.getBufLineWidth() ];
				i++;
				char *qualPtr= &bufLines[ i * ioWorker.getBufLineWidth() ];
				i++;		
				
                if ( seqI % 1000 == 0 ) { cerr << "."; }
				if ( seqI % 100000 == 0 ) { cerr << endl; }
                seqI++;
                				
                string seq( seqPtr ), qual( qualPtr );
				std::string nts = "X" + seq + "X";
				std::string quals = "X" + qual + "X";

				int seqLen = seq.length();
				if ( seqLen < merSize ) { continue; }
				totalEffSeqLen += seqLen;

				// compute for both fwd and rev strands   
				for ( int j = 0; j < 2; j++ )
				{
				  if ( j == 1 )
				  {
					nts = revcomp( nts );
					quals = reverse( quals );
				  }
				  
				  for ( int i = 1; i < nts.length() - merSize; i++ )
				  {
					string mer = nts.substr( i, merSize );	
					const char *cSeqStrPtr = mer.c_str();
				   
					unsigned int prefixCode1 =  PackedDNASequence::encode( cSeqStrPtr );
					unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( cSeqStrPtr + 4 );
					int j = 0;
					if ( 0 != validPrefixes.size() )
					{
						for ( ; j < validPrefixes.size(); j++ )
						{
							if ( strncmp( validPrefixes[ j ], cSeqStrPtr, strlen( validPrefixes[ j ] ) ) == 0 ) { break; }
						}
						if ( j == validPrefixes.size() ) { continue; }
					}
					if ( !isMerValid( cSeqStrPtr, merSize ) ) { continue; }
					
					tempPackedMer.setSeq( cSeqStrPtr );
					assert( prefixCode < g_numHashTables );
				
		
					boost::detail::spinlock &lock = *( v_pMerHashAccess[ prefixCode ] );
					{						
						// begin critical section
					   // boost::unique_lock< boost::shared_mutex > writeLock( mutex );
					   std::lock_guard< boost::detail::spinlock > writeLock( lock );
						
						HashTable::iterator iter = v_merHash[ prefixCode ]->find( tempPackedMer );
						if ( iter != v_merHash[ prefixCode ]->end() )
						{	
							//update value in hash for this mer-key
							 
							// we care about this mer
						    // compute for both pre-mer and post-mer extensions
							for ( int preOrPostI = 0; preOrPostI < 2; preOrPostI++ )
							{
								int seqIdx = -1, tallyCntIdxStart = -1, tallyLikelihoodIdxStart = -1;
								switch( preOrPostI )
								{
									case 0: seqIdx = i - 1; tallyCntIdxStart = 0; tallyLikelihoodIdxStart = 13; break;
									case 1: seqIdx = i + merSize; tallyCntIdxStart = 6; tallyLikelihoodIdxStart = 17; break;
									default: exit( -1 );
								}
					
								char base = nts[ seqIdx ];
								int baseIdx = getBaseIdx( nts[ seqIdx ] );
								if ( baseIdx > -1 )
								{
									int qual = int( quals[ seqIdx ] ) - qualOffset;
					  
									if ( qual > minQual )
									{
										// tabulate simple high-q tallies
										if ( iter->second.v[ baseIdx + tallyCntIdxStart ] < 65535 )
										{
											iter->second.v[ baseIdx + tallyCntIdxStart ]++;
										}
									}	
								}
							 }
						}	
					}
				}
			}
		}
	}	
	
	{
		std::lock_guard< boost::detail::spinlock > writeLock( globalCountersAccess );

		g_readsStartProcessed += readsStartProcessed;
		g_totalEffSeqlLen += totalEffSeqLen;
	
	}
}


void dumpMers( string outputName, vector< unsigned int > hashTableCodes, int hashStartI, int hashEndI )
{
  cerr << "dumping " << outputName << " " << hashStartI << " to " << hashEndI << endl;
  ofstream out( outputName );
  assert( out.is_open() );
  char buf[ MAX_LINE_LEN ];
  long int mersTotal = 0, mersPrinted = 0, mersExcluded = 0;
  for ( int hashIt = hashStartI; hashIt <= hashEndI; hashIt++ )
  {
  	  unsigned int hashI = hashTableCodes[ hashIt ];
	  for ( HashTable::iterator i = v_merHash[ hashI ]->begin();
				  i != v_merHash[ hashI ]->end();
				  i++ )
	  {	      
	    mersTotal++;
	    
	    HashValue v = i->second;
		{
		  bool bZeroCounts = true;
		  for ( int j = 0; j < 12; j++ )
		  {
		  	if ( i->second.v[ j ] != 0 ) { bZeroCounts = false; break; }
		  }		  
		  if ( bZeroCounts )
		  {
		    mersExcluded++;
		    continue;
		  }
		 char *c = &buf[0];
		  i->first.convert( &c );
		  out << buf << "\t";
		  for ( int j = 0; j < 12; j++ )
		  {
		    int value = i->second.v[ j ];
			out << value << "\t";
		  } 
		  out << "0\t"; // extra dummy column
		  out << endl;
		  
		  mersPrinted++;
		}
	  }
	  //cerr << "Hash table # entries: " << v_merHash[ hashI ]->load_factor
  }	    
  { 						
 	// begin critical section
	std::lock_guard< boost::detail::spinlock > writeLock( globalCountersAccess );
	g_mersPrinted += mersPrinted;
	g_mersExcluded += mersExcluded;
  }
}

/// Structure of the mergraph Ext array ( held for each mer )
// Slots:
// 0-5 = # of high-q extensions off left end for each baseCode
// 6-11 = # of high-q extensions off right end for each baseCode
// 12 - dummy
// 13 - 16 - log likelihoods per base for left end extensions
// 17 - 20 - log likelihoods per base for right end extensions

int main( int argc, char *argv[] )
{
  if ( argc < 6 )
  {
	cerr << "usage: [ mercount_file_prefix ] [ mer_size ] [ min_depth ] [ prefixListToHash ] [ numThreads ] [ qualOffset ] [ outputPrefix ] [ input_descriptor_file ]\n" << endl;
	cerr << "mercount_file_prefix: we assume numThreads number of such files" << endl;
    exit( 0 );
  }
  string mercountFilePrefix = boost::lexical_cast< string > ( argv[ 1 ] );
  int merSize = boost::lexical_cast< int >( argv[ 2 ] );
  bool revComp = 1;
  int minDepth = boost::lexical_cast< int >( argv[ 3 ] ); 
  const char *prefixList = argv[ 4 ];
  int numWorkerThreads = boost::lexical_cast< int >( argv[ 5 ] );
  qualOffset = boost::lexical_cast< int >( argv[ 6 ] );
  string outputPrefix = argv[ 7 ];
  const char *inputDescriptor = argv[ 8 ];

  string testFilename = mercountFilePrefix + boost::lexical_cast< string >( "." ) + boost::lexical_cast< string >( numWorkerThreads );
  if ( boost::filesystem::exists( testFilename ) )
  {
  	cerr << "the number of mercount input files must match the number of threads" << endl;
  	exit( -1 );
  }
  
  HashTable merHash;
  int cnt = 0;
  int prefixLength = 4;
  int numEntries = 1000000000;  // 1 billion
   PackedDNASequence::init( 0, merSize );

  vector< string > prefixes;
  vector< const char * > validPrefixes;

  if ( strcmp( "-", prefixList ) != 0 )
  {
    boost::split( prefixes, prefixList, boost::is_any_of( "+" ) ); 
    assert( prefixes.size() > 0 );
    for( int i = 0 ; i < prefixes.size(); i++ )
    {
 	    validPrefixes.push_back( prefixes[ i ].c_str() );
    }
  }
  for ( int i = 0; i < g_numHashTables; i++ )
  {
     HashTable *h = new HashTable( numEntries / g_numHashTables * 2 );
     v_merHash.push_back( h );     
  }		
  for( int i =0; i < g_numMutexes; i++ )
  {
     boost::detail::spinlock *m = new boost::detail::spinlock();
     v_pMerHashAccess.push_back( m );
  }
  



  boost::thread threads[ numWorkerThreads ];
	
  for( int i = 0; i < numWorkerThreads; i++ )
  {
	  string filename = mercountFilePrefix + boost::lexical_cast< string >( "." ) + boost::lexical_cast< string >( i );
	  
	  threads[ i ] = boost::thread( loadMers, filename, merSize, minDepth, validPrefixes );
  }

  ioWorker.init( inputDescriptor, numWorkerThreads );
  for( int i = 0; i < numWorkerThreads; i++ )
  {
		threads[ i ].join();
  }
  cerr << "done loading mers" << endl;
  dumpTime();
  cerr << "tallying extensions" << endl;
	
  for( int threadI = 0; threadI < numWorkerThreads; threadI++ )
  {
		threads[ threadI ] = boost::thread( tallyMerExtensions, merSize, validPrefixes, numWorkerThreads, threadI );
  }
  
  for ( int i =0; i < numWorkerThreads; i++ )
  {
     threads[ i ].join();
  }
 
  int curI = 0;

  vector< unsigned int > hashTableCodes;
  
  if ( strcmp( "-", prefixList ) != 0 )
  {
    boost::split( prefixes, prefixList, boost::is_any_of( "+" ) ); 
    assert( prefixes.size() > 0 );
    
    // create a list of binary-packed hashtable codes ( 8-mers ) from prefix list ( used for fast dumping of mers )
    for( int i = 0 ; i < prefixes.size(); i++ )
    {
 	    validPrefixes.push_back(prefixes[ i ].c_str() );
 	    
 	    vector< unsigned int > newHashTableCodes;
 	    if ( prefixes[ i ].length() < 8 )
 	    {
 	       int lengthToGenerate = 8 - prefixes[ i ].length();
 	       
 	       for ( int j = 0; j < pow( 4, lengthToGenerate ); j++ )
 	       {
 	          string newPrefix = prefixes[ i ] + decodeMer( j, lengthToGenerate ) ;
 			  unsigned int prefixCode1 =  PackedDNASequence::encode( newPrefix.c_str() );
			  unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( newPrefix.c_str() + 4 );
		          
 	          newHashTableCodes.push_back( prefixCode );
 	       }  
 	    }
 	    else
 	    {
 	     	unsigned int prefixCode1 =  PackedDNASequence::encode( prefixes[ i ].c_str() );
			unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( prefixes[ i ].c_str() + 4 );
		          
 	        newHashTableCodes.push_back( prefixCode );
 	    }
 	    
 	    // add candidate codes iff they don't exist already
 	    for( int j = 0; j < newHashTableCodes.size(); j++ )
 	    {
 	      if ( std::find( hashTableCodes.begin(), hashTableCodes.end(), newHashTableCodes[ j ] ) == hashTableCodes.end() )
 	      {
 	        hashTableCodes.push_back( newHashTableCodes[ j ] );
 	      }
 	    }
    }
  }
  else
  {
  	for ( int i =0; i < 65536; i++ ) { hashTableCodes.push_back( i ); }
  }
  
  cerr << "dumping mers..." << endl;
  for ( int i = 0; i < numWorkerThreads; i++ )
  {
     int hashTablesPerThread = (hashTableCodes.size() / numWorkerThreads ) + 1; // sloppy approx ceil function
     int endI = curI + hashTablesPerThread - 1;
     if ( endI >= hashTableCodes.size() ) { endI = hashTableCodes.size() - 1; }
     string outputFileName = outputPrefix;
     outputFileName += ".";
     outputFileName += boost::lexical_cast< string >( i );
     threads[ i ] = boost::thread( dumpMers, outputFileName, hashTableCodes, curI, endI );
     curI += hashTablesPerThread;
  }
  for ( int i = 0; i < numWorkerThreads; i++ )
  {
     threads[ i ].join();
  }
 
  cerr <<  "Found " << g_readsStartProcessed << " total reads spanning " << g_totalEffSeqlLen << " total bases" << endl;
  cerr << "Read " << g_mersRead << " mers as input" << endl;
  cerr << "Added " << g_mersAdded << " mers to the hash ( after filtering for depth + prefix )" << endl;
  cerr << "Excluded " << g_mersExcluded << " mers for having no extensions" << endl;
  cerr << "Printed " << g_mersPrinted << " mers and their corresponding count graph.\n";
  cerr << "Done." << endl;
  
  return( 0 );
}


