#include <time.h>
#include <boost/thread/thread.hpp>
#include <boost/unordered_map.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <boost/signals2/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <sparsehash/sparse_hash_map>
#include <boost/smart_ptr/detail/spinlock.hpp>
#include <mutex>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <utility>

#include "PackedDNASequence.hpp"  
#include "IOWorkerSimple.hpp"
#include "LV.hpp"

using google::sparse_hash_map;

#define PAGES_PER_BUFFER 1000
#define MAX_NUM_CONTIGS 10000000
#define MAX_SEQ_LEN 1000

// what percentage of a projection can be indels and still be trustworthy?
#define INDEL_PCT_MAX 0.2
#define max( x, y )  ( x > y ? x : y )
using namespace std;

typedef struct
{
  unsigned char dummyPrevCode;  // these are just dummy values...do not rely on them A/C/G/T/F/X
  unsigned char dummyNextCode;
  int contigId; // use this for contig Id as well when dumping hashtable
  unsigned int pos; // use this for dumping hashtable
} HashValue;

typedef PackedDNASequence PackedMer;
typedef boost::unordered_map< int, int > IntHashTable;
typedef boost::unordered_map< int, bool > ContigIdsPrintedTable;

class CompareKeys   
{
public: 
	inline bool operator()( const PackedMer &a, const PackedMer &b ) const
	{
		return( PackedDNASequence::equals( a, b ) );
	}
};


class HashKeys
{
public:
	inline std::size_t operator()( const PackedMer &a ) const
	{
		return PackedDNASequence::hash( a ); 
	}
};


typedef sparse_hash_map< PackedMer, HashValue, HashKeys, CompareKeys > HashTable;

int g_extensionType = 1; // 0 = none, 1 = lv, 2 = memcmp hack
bool bDebugMode = false;
bool bUseMmap = 0;
int g_hashPrefixLen = 8;  /// must be 8 b/c of PackedDNASequence::encode() coupling
int g_numHashTables = pow( 4, g_hashPrefixLen );
int g_numMutexes = g_numHashTables;
int g_mismatchPenalty = -10;
bool g_bAllowMixedCase = false;
bool g_bLoadBinaryMerhash = false; // binary hash imples use of a single g_merHash instead of v_merHash[]/locks
LandauVishkin lv2;

HashTable g_merHash; // used when loading from a binary hash

std::vector< HashTable * > v_merHash;  // used when NOT loading from binary hash
std::vector< boost::detail::spinlock * > v_pMerHashAccess;

IOWorkerSimple ioWorker;
long int *contigLenTable;
string *contigNames;
string *contigSeqTable;
long int g_maxNumContigs;

boost::detail::spinlock *contigHashLock;

 
void printTime2()
{
	time_t rawtime;
	struct tm *timeinfo;
	char buffer [200];
	time( &rawtime );
	timeinfo = localtime( &rawtime );
	strftime (buffer,200,"%c",timeinfo);	
	cerr << buffer;
}

// checks that mer contains only [ACGT]
inline bool verifyMer( const char *s, int length )
{

	for ( int i =0; i < length; i++ )
	{
		switch( s[i] )
		{
			case 'A': case 'C': case 'G': case 'T': 
			break;
			default:
			return( 0 );
		}
	}
	return( 1 );
}


inline string revcomp( const string &s )
{
	string rs = s; // allocate same amount of memory
	
	int rsCsr = 0;
	for( int i = s.length() - 1; 
		i >= 0;
		i-- )
	{
		switch( s[ i ] )
		{
			case 'A': 
			rs[ rsCsr ] = 'T'; break;
			case 'T':
			rs[ rsCsr ] = 'A'; break;
			case 'C':
			rs[ rsCsr ] = 'G'; break;
			case 'G':
			rs[ rsCsr ] = 'C'; break;
			case 'a':
			rs[ rsCsr ] = 't'; break;
			case 't':
			rs[ rsCsr ] = 'a'; break;
			case 'c':
			rs[ rsCsr ] = 'g'; break;
			case 'g':
			rs[ rsCsr ] = 'c'; break;
			default:
			rs[ rsCsr ] = s[ i ];
		}
		rsCsr++;
	}
	return( rs );
}

inline char revcompChar( const char x )
{
	switch( x )
	{
		case 'A': return( 'T' ); 
		case 'C': return( 'G' ); 
		case 'G': return( 'C' ); 
		case 'T': return( 'A' ); 
		case 'a': return( 't' );
		case 'c': return( 'g' );
		case 'g': return( 'c' );
		case 't': return( 'a' );	
	}
	return( x );
}

inline void revcompCStr( const char *cStr, int numChars, char *newBuf )
{
    const char *cStrEnd = cStr + numChars - 1;
	while ( cStrEnd >= cStr )
	{
		*newBuf = revcompChar( *cStrEnd );
        newBuf++; cStrEnd--;
	}
}

inline bool isValid( const char x )
{
	switch( x )
	{
		case 'A':
		case 'C':
		case 'G':
		case 'T':
		return( true );
	}
	return( false );
}

void appendMersToHash(string contigSeq, int merSize, int contigId) 
{
	PackedMer tempPackedMer;

	for ( int i = 0; i < contigSeq.length() - merSize + 1; i++ )
	{
		string mer = contigSeq.substr( i, merSize );

		//if ( !g_bAllowMixedCase ) 
		{
			bool bIllegalMer = false;
			for ( int merI = 0; merI < mer.size(); merI++ ) {
				if ( !PackedDNASequence::isSupportedChar( mer.at( merI ) ) ) {
					bIllegalMer = true;
					break;
				}
			}
			// skip the mer that has unsupported chars
			if ( bIllegalMer ) { continue; }
		}

		unsigned int prefixCode1 =  PackedDNASequence::encode( mer.c_str() );
		unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( mer.c_str() + 4 );
		tempPackedMer.setSeq( mer.c_str() );


		// note: we add one to the contigId stored in the has b/c we use +/- to store strand as well,
		//    and 0 cannot be made negative...
		{
			//std::pair<int, int> p( contigId, i );
			HashValue val;
			val.contigId = contigId;
			val.pos = i;

			//  unsigned char dummyPrevCode;  // these are just dummy values...do not rely on them A/C/G/T/F/X
			//  unsigned char dummyNextCode;
  			//	int contigId; // use this for contig Id as well when dumping hashtable
  			// unsigned int pos; // use this for dumping hashtable
			HashTable::value_type v( tempPackedMer, val );
			HashTable *h= v_merHash[ prefixCode ];

			boost::detail::spinlock &lock = *( v_pMerHashAccess[ prefixCode ] );
			{

				// begin critical section
				std::lock_guard< boost::detail::spinlock > writeLock( lock );

				HashTable::iterator iter = h->find( tempPackedMer );						
				if ( iter != v_merHash[ prefixCode ]->end() )
				{
					//cerr << "FAIL: mer " << mer << " already in hash in contig " << zeroBasedContigId << endl;
				}
				else
				{
					// new mer
					h->insert( v );								
				}
			}
		}

		string rcMer = revcomp( mer );
		prefixCode1 =  PackedDNASequence::encode( rcMer.c_str() );
		prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( rcMer.c_str() + 4 );
		tempPackedMer.setSeq( rcMer.c_str() );

		{
			HashValue val;
			val.contigId = -contigId;
			val.pos = contigSeq.length() - i - merSize;

			HashTable::value_type v( tempPackedMer, val );
			HashTable *h= v_merHash[ prefixCode ];

			boost::detail::spinlock &lock = *( v_pMerHashAccess[ prefixCode ] );
			{

				// begin critical section
				std::lock_guard< boost::detail::spinlock > writeLock( lock );

				HashTable::iterator iter = h->find( tempPackedMer );						
				if ( iter != v_merHash[ prefixCode ]->end() )
				{
					//cerr << "FAIL: mer " << mer << " already in hash in contig " << zeroBasedContigId << endl;
				}							
				else
				{
					// new mer
					h->insert( v );								
				}
			}
		}
	}
} 

/* A NOTE ON INDEXES:
the contigNamesToIdx hash converts between the name in the file and our internal id that indexes the contig-related parallel arrays
This idx goes from 1 to inf, -1 to -inf -- zero is illegal: this way both strands can be represented
I've wrapped any lookup operations involving the merHash in lookupMer(), which uses the 1-based contigId as an index across all structures.
*/
void prepareHashes(std::string contigsInName, int merSize, int prefixLength)
{
	printTime2();

	ifstream contigsIn( contigsInName );
	
	bool revComp = 1;
	char str[ 10000 ];
	int cnt = 0;
	string contigSeq = "";
	// load and prepare mer hash				
	// In the hash, we store each mer seen in the contigs + their revcomp	 
	bool bLastLineUnprocessed = true;
	long int linesRead = 0;
	bool successBit = false;
			
	// inside my code, contigId goes from -MAX to MAX, where 0 is a sentinel value
	int contigId = 0;
	bool bFirstLine = true;
	std::string contigName = "";
	while( ( successBit = bool(contigsIn.getline( &str[0], 10000 )) ) || bLastLineUnprocessed )
	{
		cnt++;
		bool bThisIsLastLine = false;
		if ( !successBit ) 
		{ 			
			bThisIsLastLine = true; 
		}
		if ( 0 == ( cnt % 10000 ) ) { cerr << "."; }

		if ( bThisIsLastLine || str[0] == '>' )
		{
			if ( bFirstLine ) {
				bFirstLine = false;
			} 
			else
			{
				//process 'previous' contig
				if ( contigSeq.length() < merSize ) {
					cerr << "Contig size must be >= k: " << contigName[contigId];
					exit( -1 );
				}
				contigLenTable[ contigId ] = contigSeq.length();
				contigSeqTable[ contigId ] = contigSeq;

				string revcompSeq = revcomp( contigSeq );
				contigSeqTable[ -contigId ] = revcompSeq;
				contigLenTable[ -contigId ] = contigSeq.length();				
			
				if (!g_bLoadBinaryMerhash) {
					appendMersToHash(contigSeq, merSize, contigId);
				}
			}

			if ( !bThisIsLastLine )
			{   
				// prepare the next contig
				contigId = abs(contigId) + 1; 
                assert(abs(contigId) < g_maxNumContigs );
				contigSeq = "";

				typedef std::vector< std::string > SplitVec;
				SplitVec nameCols;
				char *nameStart = str + 1;
				boost::algorithm::split( nameCols, nameStart, boost::is_any_of( "\t " ) );
				contigNames[ contigId ] =  nameCols[ 0 ];
				contigNames[ -contigId ] = nameCols[ 0 ];
			}
			else 
			{
				break;
			}
		}
		else
		{
			contigSeq += str;
		}
		linesRead++;
	}

	contigsIn.close();
	printTime2();
	cerr << "...done prepping hashes" << endl;
}

void printAlignment( ofstream &out, string readName, int contigId, string strand, 
	int leftCsr, int rightCsr, 
	int leftContigPos, int rightContigPos, 
	int contigLen, int readLen, 
	int merSize, std::string readCsrStr )
{
	long int readStart = leftCsr + 1;
	long int readStop = rightCsr + merSize;
    int alignLen = readStop - readStart + 1;
	
	long int merContigStart = leftContigPos + 1;
	long int merContigStop = rightContigPos + merSize;
	
	// may be negative
	long int contigStart = merContigStart; 
	long int contigStop = merContigStop;
	// may be > contigLen
	if ( "Minus" == strand )
	{
		contigStart = contigLen - contigStart + 1;
		contigStop = contigLen - contigStop + 1;
        long int x = contigStart;
        contigStart = contigStop;
        contigStop = x;
	} 

	// print results:  we print a leading readCsrPos so that results can be merged efficiently across contig blocks
	out << readCsrStr << "\t" << "BLASTN\t" << readName << "\t" << readStart << "\t" << readStop << "\t" << readLen << "\t" << 
	contigNames[ contigId ] << "\t" << 
	contigStart << "\t" << contigStop << "\t" << contigLen << "\t" <<  strand << "\t" << 
	99999 << "\t" << "0" << "\t" << alignLen << "\t" << alignLen << "\n";
}


bool printAlignmentIfValid( ofstream &out, string readName, int contigId, bool bStrandIsPositive, 
	int leftPos, int rightPos, 
	int leftCsrPos, int rightCsrPos, int merSize, int readLen, std::string readCsrPos, ContigIdsPrintedTable &contigIdsPrinted)
{
	 if (contigIdsPrinted.find(contigId) != contigIdsPrinted.end()) {
    	// already in there
    	return false;
    }

	int MIN_ALIGNMENT_LENGTH = merSize;
	int alignLengthAlongContig = abs( rightPos - leftPos ) + merSize ;
	int alignLengthAlongRead = abs( rightCsrPos - leftCsrPos ) + merSize ;
	int wiggleRoom = max( 1, INDEL_PCT_MAX * alignLengthAlongRead );

	if ( abs( alignLengthAlongContig - alignLengthAlongRead ) <= wiggleRoom && 
		alignLengthAlongRead >= MIN_ALIGNMENT_LENGTH )
	{
       // within 5 bases
		string strand = bStrandIsPositive ? "Plus" : "Minus";
		long int contigPos = leftPos;
		long int contigLen = contigLenTable[ contigId ];  

		printAlignment( out, readName, contigId, strand, leftCsrPos, rightCsrPos, leftPos, rightPos,
			contigLen, readLen, merSize, readCsrPos );

		ContigIdsPrintedTable::value_type v(contigId, true);

		contigIdsPrinted.insert(v);

		return true;
	}

	return false;
}

typedef struct 
{
	int	cid;
	int leftmostContigPos, rightmostContigPos;
	int leftmostCsrPos, rightmostCsrPos;
} MerMatch;

// if not found, returns false AND o_cid==0 && o_pos = -1
// beware that the hash may contain 0 for cid...this means that the mer is not
//    in the actual contigs file and should be skipped (this was an optimization)
bool lookupMer( const char *seq, int *o_cid, int *o_pos, bool *o_bStrand )
{
	PackedDNASequence tempPackedMer;
	tempPackedMer.setSeq( seq );

	HashTable *h = &g_merHash;
	if (!g_bLoadBinaryMerhash) {
		unsigned int prefixCode1 =  PackedDNASequence::encode( seq );
		unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( seq + 4 );
	
		h = v_merHash[ prefixCode ];
	}

	int s = h->size();
	*o_cid = 0; *o_pos = -1; *o_bStrand = 0;
	HashTable::iterator i = h->find( tempPackedMer );
	if ( i != h->end() )
	{
		*o_cid  = i->second.contigId;
		*o_pos = i->second.pos;
		*o_bStrand = ( i->second.contigId > 0 );

		return(*o_cid != 0);
	}
	return( false );
}

void lvExtendRight(const char *seqPtr, const int &seqLen, const int &merSize, 
	const int &rightEndCsr, const int &rightEndPos, const int &rightEndCid,
	int *o_readAlignLen, int *o_genomeAlignLen)
{
	LandauVishkin lv;

 	int readSegmentStart = rightEndCsr + merSize;
    int contigSegmentStart = rightEndPos + merSize;
    
    *o_readAlignLen = 0;
    *o_genomeAlignLen = 0;

    // for haplotype test, allow mixed-case contigs...we want to not seed on lower-case contig regions, but
    //    do want to extend into them
    int cFragLen = contigLenTable[ rightEndCid ] - contigSegmentStart;
    char *caseInsensitiveContigFrag = new char[cFragLen];
    strncpy(caseInsensitiveContigFrag, contigSeqTable[ rightEndCid ].c_str() + contigSegmentStart, 
    	cFragLen);

    if ( g_bAllowMixedCase )
    {
	   	 // convert to upper case
	   	 for ( int i = 0; i < cFragLen; i++ ) {
   			caseInsensitiveContigFrag[i] = toupper(caseInsensitiveContigFrag[i]);
    	}
	}

  	int lvScore = lv.computeEditDistance( 
  		seqPtr + readSegmentStart,
 		seqLen - readSegmentStart,
  		caseInsensitiveContigFrag,
  		cFragLen,
 		MAX_K,
        g_mismatchPenalty,
 		o_readAlignLen,
 		o_genomeAlignLen );

 	delete caseInsensitiveContigFrag;
}

void lvExtendLeft(const char *seqPtr, const int &leftEndCsr, const int &leftEndPos, 
	const int &leftEndCid, 
	int *o_readAlignLen, int *o_genomeAlignLen)
{
	LandauVishkin lv;

	// params: seqPtr, leftEndCsr, leftEndCid, leftEndPos
    char revcompSeqPartial[ MAX_SEQ_LEN ];
    const char *readDummyQuality = "";

    *o_readAlignLen = 0;
    *o_genomeAlignLen = 0;

    // revcomp the left extending read segment
    revcompCStr(seqPtr, leftEndCsr, revcompSeqPartial );

    int cFragLen = leftEndPos;
    char *caseInsensitiveContigFrag = new char[cFragLen];
 
    strncpy(caseInsensitiveContigFrag, contigSeqTable[ -leftEndCid ].c_str() + contigLenTable[ leftEndCid ] - leftEndPos,
    	cFragLen);

    if ( g_bAllowMixedCase ) {
	    // convert to upper case
	    for ( int i = 0; i < cFragLen; i++ ) {
	   		caseInsensitiveContigFrag[i] = toupper(caseInsensitiveContigFrag[i]);
	    }
	}

  	int lvScore = lv.computeEditDistance( 
  		revcompSeqPartial,
 		leftEndCsr,
  		caseInsensitiveContigFrag,
      	cFragLen,
 		MAX_K,
        g_mismatchPenalty,
 		o_readAlignLen,
 		o_genomeAlignLen );
 	
 	delete caseInsensitiveContigFrag;	 
}

bool extendAndPrintAlignmentIfValid(ofstream &out, const char *seqPtr, const int &seqLen, 
	const int &merSize, string readName, const int &contigId, 
	const bool &bStrandIsPositive, const int &leftPos, const int &rightPos, 
	const int &leftCsrPos, const int &rightCsrPos, const int &readLen, 
	const string &readCsrPos, ContigIdsPrintedTable &contigIdsPrinted)
{
    int rightReadExtensionLen = 0;
    int rightGenomeExtensionLen = 0;
    int leftReadExtensionLen = 0;
    int leftGenomeExtensionLen = 0;
                		     
    if (contigIdsPrinted.find(contigId) != contigIdsPrinted.end()) {
    	// already in there
    	return false;
    }

    // XXX contigId <-> bStrandIsPositive handled correctly here?
	lvExtendRight(seqPtr, seqLen, merSize, rightCsrPos, rightPos, 
				contigId, &rightReadExtensionLen, &rightGenomeExtensionLen);

	lvExtendLeft(seqPtr, leftCsrPos, leftPos, contigId, &leftReadExtensionLen, 
                		&leftGenomeExtensionLen);

	return printAlignmentIfValid(out, readName, contigId, bStrandIsPositive, leftPos - leftGenomeExtensionLen, 
		rightPos + rightGenomeExtensionLen, 
		leftCsrPos - leftReadExtensionLen, rightCsrPos + rightReadExtensionLen, 
		merSize, readLen, readCsrPos, contigIdsPrinted);
}


void lookupMers( int merSize, std::vector< unsigned int > validPrefixes, 
	int skipLineCnt, int threadId, string outputFilePrefix )
{
	bool revComp = 1;
	long int totalEffSeqLen = 0;
	long int mersPastFiltering = 0;
	long int mersAlreadyInHash = 0; 
	long int mersLookedAt = 0;
	long int readsStartProcessed = 0;
	long unsigned int seqI = 0;
	string readName;
	int bufNumLines = ioWorker.getOutBufNumLines();
	int threadCnt_ = skipLineCnt;
	char *bufLines = 0;	
	ofstream out;
	string name = outputFilePrefix + ".";
	name += boost::lexical_cast<std::string>( threadId );
	out.open( name );
	bool bFirstTime = true;
	seqI = 0;
	char *prevBufLines = 0; 

	while( ioWorker.getLines( threadId,  &bufLines, bufNumLines ) )
	{	   
		bFirstTime = false; 

		for( int i = 0; i < bufNumLines; )
		{	
			ContigIdsPrintedTable contigIdsPrinted; // avoid printing two hits to same contigId for same read

			long int readCsr = ioWorker.getCsr( threadId );
			char *header = &bufLines[ i * ioWorker.getBufLineWidth() ];
			i++;
			char *seqPtr = &bufLines[ i * ioWorker.getBufLineWidth() ];
			i++;
			char *spacer = &bufLines[ i * ioWorker.getBufLineWidth() ];
			i++;
			char *qualPtr= &bufLines[ i * ioWorker.getBufLineWidth() ];
			i++;		

			int charsScanned = 0;

			if ( seqI % 1000 == 0 ) { cerr << "."; }
			seqI++;
			int colonsFound = 0;			

			readName = header; // strip on whitespace XXX
			readName.erase( 0, 1 );
			
			int seqLen = strlen( seqPtr );
			int firstValidMerI = 0;  
			int currentValidMerLen = 0;
			const char *cSeqStr = seqPtr;
			// search for mers
			int effSeqLen = seqLen - merSize + 1; // effective seq length 
			
			totalEffSeqLen += effSeqLen;
			
			string readCsrStr = "";
			readCsrStr += ( char ) ( ( ( int ) 'A' ) + threadId );
			readCsrStr += boost::lexical_cast< std::string > ( readCsr );
			
			/*				
			 	find leftmost mer match.  Try to extend it in 3 frames going to left end of read. 
			 	find rightmost mer match.  Try to extend it in 3 frames, going to right end of read.

			 	if they are the same contig id and alignment is valid 
			 		print alignment
			 	Else
			 		continue from left segment, extending as you scan right. If a new contig id is
			 		   found, evaluate the projection formed by the left segment, print if valid.
			*/
			int leftEndCid = 0; // 0 is the correct sentinel value b/c contig ids go from 0 upwards
			int leftEndPos = -1;
			int leftEndCsr = 0;
			bool leftEndStrand = false;
			while ( leftEndCsr < effSeqLen && !lookupMer( seqPtr + leftEndCsr, &leftEndCid, &leftEndPos, &leftEndStrand ) )
			{
				leftEndCsr++;
			}
      		if ( leftEndCid == 0 ) { continue; } // no mer found
      
 			// used to delineate an alignment's boundaries 
      		int leftEndLeftmostContigPos = leftEndPos;
    		int leftEndLeftmostCsrPos = leftEndCsr;
            if ( leftEndCsr > 0 && g_extensionType )
    		{
                if ( g_extensionType == 1 )
                {
                	int readAlignLen = 0;
                	int genomeAlignLen = 0;

                	lvExtendLeft(seqPtr, leftEndCsr, leftEndPos, leftEndCid, &readAlignLen, 
                		&genomeAlignLen);

			    	leftEndLeftmostCsrPos -= readAlignLen;
			    	leftEndLeftmostContigPos -= genomeAlignLen;
                }
                else
                {
		    		// try to extend left alignment to the left end of read
			      	for ( int strategy = 0; strategy < 3; strategy++ )
			      	{
				 		// default case: SNP is a non-indel mismatch
			      		int readSegmentEnd = leftEndCsr - 2;
			      		int contigSegmentEnd = leftEndPos - 2;

			      		switch( strategy )
			      		{
			 				// alignment ends at a non-indel mismatch: DEFAULT
			      			case 0: break; 
							// alignment ends at a insertion in read  
			      			case 1:
			      				contigSegmentEnd++; break;
							// alignment ends at a deletion in read
			      			case 2:
			      				readSegmentEnd++; break;
			      			default: assert( 0 );	
			      		}
			      		int compareLen = min( readSegmentEnd, contigSegmentEnd ) + 1;
			      		int readSegmentStart = readSegmentEnd + 1 - compareLen;
						int contigSegmentStart = contigSegmentEnd + 1 - compareLen;
			 					
			      		if ( 0 == memcmp( seqPtr + readSegmentStart, 
			      			contigSeqTable[ leftEndCid ].c_str() + contigSegmentStart, 
			      			compareLen ) )
			      		{
			      			leftEndLeftmostCsrPos = readSegmentStart;
			      			leftEndLeftmostContigPos = contigSegmentStart;
			      			break;
			      		}
			      	}
                }
	    	}
 
	    	int rightEndCid = 0;
	    	int rightEndPos = -1;
	    	bool rightEndStrand = false;
     		int rightEndCsr = effSeqLen - 1;
     		while( rightEndCsr >= 0 && !lookupMer( seqPtr + rightEndCsr, &rightEndCid, &rightEndPos, &rightEndStrand ) )
     		{
      			rightEndCsr--;
      		}

      		// note, there must be a right end mer match, b/c there was a left mer match that allowed us to get this far
        	int rightEndRightmostContigPos = rightEndPos;
	      	int rightEndRightmostCsrPos = rightEndCsr;
	      	const char *readDummyQuality = "";
            if ( rightEndCsr < effSeqLen - 1 && g_extensionType )
	      	{
                if ( g_extensionType == 1 )
                {		
	                int readAlignLen = 0;
                	int genomeAlignLen = 0;
                		     
					lvExtendRight(seqPtr, seqLen, merSize, rightEndCsr, rightEndPos, 
						rightEndCid, &readAlignLen, &genomeAlignLen);
			    	
			    	rightEndRightmostCsrPos += readAlignLen;
			    	rightEndRightmostContigPos += genomeAlignLen;
                }
                else
                {
			 		// try to extend right alignment to the right end of read
			      	{
				      	for ( int strategy = 0; strategy < 3; strategy++ )
				      	{
							// default case: SNP is a mismatch, no indel
				      		int readSegmentStart = rightEndCsr + merSize + 1;
				      		int contigSegmentStart = rightEndPos + merSize + 1;

				      		switch( strategy )
				      		{
								// alignment ends at a non-indel mismatch: DEFAULT
				      			case 0: break; 
								// alignment ends at a insertion in read  
				      			case 1:
				      				contigSegmentStart--; break;
								// alignment ends at a deletion in read
				      			case 2:
				      				readSegmentStart--; break;
				      			default: assert( 0 );
				      		}
				      		int compareLen = min( ( long int ) seqLen - readSegmentStart,
				      			contigLenTable[ rightEndCid ] - contigSegmentStart );

				      		int readSegmentEnd = readSegmentStart + compareLen - 1;
				      		int contigSegmentEnd = contigSegmentStart + compareLen - 1;
				      		if ( 0 == memcmp( seqPtr + readSegmentStart, 
				      			contigSeqTable[ rightEndCid ].c_str() + contigSegmentStart, 
				      			compareLen ) )
				      		{
				      			rightEndRightmostCsrPos = readSegmentEnd - ( merSize - 1 );
				      			rightEndRightmostContigPos = contigSegmentEnd - ( merSize - 1 );
				      			break;
				      		}
				      	}
		    		}
                }
    		}

    		if ( leftEndCid == rightEndCid && 0 != leftEndCid && 
      			printAlignmentIfValid( out, readName, leftEndCid, leftEndStrand,
      			leftEndLeftmostContigPos, rightEndRightmostContigPos, 
      			leftEndLeftmostCsrPos, rightEndRightmostCsrPos, 
      			merSize, seqLen, readCsrStr, contigIdsPrinted ) )
      		{
				// you're done with this read
     		}
      		else
		    {
				// enumerate all internal matching fragments
		      	int leftmostContigPos = -1, leftmostCsrPos = -1, rightmostContigPos = -1, prevCid = leftEndCid;
		      	int rightmostCsrPos = -1;
		      	int csr = leftEndCsr;
		      	bool prevStrand = 0;

				// continue where we left off....
		      	leftmostContigPos = leftEndLeftmostContigPos;
		      	leftmostCsrPos = leftEndLeftmostCsrPos;
		      	rightmostContigPos = leftEndPos;
		      	rightmostCsrPos = leftEndCsr;

		      	// read up THRU rightmost segment from initial stage
		      	int cid = leftEndCid; 
		      	int pos = leftEndPos;
		      	bool strand = 0;
				for ( ; csr <= rightEndCsr - 1; csr++ )
		      	{
		      		// invariants: Cid and prevCid are always > -1. 	      
		      		int tempCid = 0;  // used to preserve invariant that cid/prevCid != 0
		      		int tempPos = -1;
		      		bool tempStrand = 0;
		      		lookupMer( seqPtr + csr, &tempCid, &tempPos, &tempStrand );

		      		if ( tempCid != 0 ) 
		      		{
		      			cid = tempCid; 
		      			strand = tempStrand;
		      			pos = tempPos;
		      		}

		      		// if a 'break' is found in the alignment ( tolerating no errors/gaps )
		      		if ( cid != prevCid ) // || tempStrand == Plus ? tempPos != pos + 1 : tempPos != pos - 1
		      		{
		      			extendAndPrintAlignmentIfValid(out, seqPtr, seqLen, merSize, readName, prevCid, prevStrand, 
		      				leftmostContigPos, 
		      				rightmostContigPos, 
		      				leftmostCsrPos, rightmostCsrPos, seqLen, readCsrStr, contigIdsPrinted );
		     
		      			leftmostContigPos = pos;
		      			leftmostCsrPos = csr;						
		      		}
		      		prevCid = cid;
		      		prevStrand = strand;
		      		if ( tempCid != 0 )
		      		{
		      			rightmostContigPos = pos;
		      			rightmostCsrPos = csr;
		      		}
		      	}

	      		if ( prevCid == rightEndCid )
	      		{
	      			// this last fragment belongs to the rightEnd fragment found initially
	      			extendAndPrintAlignmentIfValid( out, seqPtr, seqLen, merSize, readName, 
	      				prevCid, prevStrand, leftmostContigPos, rightEndRightmostContigPos, 
	      				leftmostCsrPos, rightEndRightmostCsrPos, seqLen, readCsrStr, contigIdsPrinted );
	      		}
	      		else
	      		{
	      			extendAndPrintAlignmentIfValid( out, seqPtr, seqLen, merSize, readName, prevCid, prevStrand, leftmostContigPos, rightmostContigPos, 
		      			leftmostCsrPos, rightmostCsrPos, seqLen, readCsrStr, contigIdsPrinted );

       
	      			// this one's already extended
	      			printAlignmentIfValid( out, readName, rightEndCid, rightEndStrand, rightEndPos, rightEndRightmostContigPos, 
	      				rightEndCsr, rightEndRightmostCsrPos, merSize, seqLen, readCsrStr, contigIdsPrinted );
	      		}
		    }
		}
  	    prevBufLines = bufLines;
    }
    
	printTime2();
	cerr << "...done looking up mers" << endl;

	cerr << "reads start processed " << readsStartProcessed << " seqI " 
		<< seqI << " totalEffSeqLen " << totalEffSeqLen << endl;
		cerr << "Mers looked at " << mersLookedAt << " past filtering " << mersPastFiltering << " already in hash " << mersAlreadyInHash << endl;
}

void dumpMerHash() {
	char *seq = new char[100];

	for (HashTable::iterator i = g_merHash.begin(); i != g_merHash.end(); i++) {

		PackedMer m = i->first;
		HashValue v = i->second;

		m.convert(&seq);
		cerr << seq << " " << v.contigId << " " << v.pos << endl;
	}
	delete seq;
}
int main( int argc, char *argv[] )
{
	if ( argc < 4 )
	{
		cerr << "usage: [contigs_file] [contigs_hash_file] [mer_size] [input_descriptor_file] [output_file_prefix] [ numThreads ] < [num_contigs_in_file] <mismatch_penalty> <extension_type> <allowMixedCaseContigs>\n" << endl;
		exit( 0 );
	}
	
	string contigsWc = argv[1];
	string cmd = "ls " + ( string ) contigsWc;
	FILE *inputWC = popen( cmd.c_str(), "r" );
	std::vector< string > vContigsIn;
	char buf[ 255 ];
	while( fgets( buf, 255, inputWC ) != NULL )
	{
		cerr << "using file: " << buf << endl;
		buf[ strlen( buf ) - 1 ] = '\0'; // remove trailing newline
		vContigsIn.push_back( buf );
	}
	fclose( inputWC );

	
	const char *contigsHashFile = argv[2];
	g_bLoadBinaryMerhash = *contigsHashFile != '-';

	if (g_bLoadBinaryMerhash) {
		cerr << "loading merHash" << endl;
		printTime2();

		FILE *in = fopen( contigsHashFile, "rb" );

		g_merHash.unserialize(HashTable::NopointerSerializer(), in);
		fclose(in);

		cerr << "done loading merHash" << endl;
		printTime2();
	}

	int merSize = boost::lexical_cast< int, char * >( argv[ 3 ] );
	if ( merSize % 2 == 0 ) {
		cerr << "merSize must be odd ( to avoid pallindromes, which collide in the hash )" << endl;
	//	exit(-1);
	}
	if ( merSize < 8 ) {
		cerr << "merSize must be > 8" << endl;
		exit( -1 );
	}
	int numThreads = boost::lexical_cast< int >( argv[ 6 ] );
	g_maxNumContigs = 10000000; // 10M by default seems safe most of the time
	if ( argc >= 8 ) { g_maxNumContigs = boost::lexical_cast< long int >( argv[ 7 ] ); }
    if ( argc >= 9 ) { g_mismatchPenalty = boost::lexical_cast< int > ( argv[ 8 ] ); }
    if ( argc >= 10 ) { g_extensionType = boost::lexical_cast< int > ( argv[ 9 ] ); }
    if ( argc >= 11 ) { g_bAllowMixedCase = boost::lexical_cast< bool >( argv[ 10 ] ); }

	contigLenTable = new long int[ g_maxNumContigs * 2 + 1 ];
	contigNames = new string[ g_maxNumContigs * 2 + 1 ];
	contigSeqTable = new string[ g_maxNumContigs * 2 + 1 ];

	contigLenTable += g_maxNumContigs;
	contigNames += g_maxNumContigs;
	contigSeqTable += g_maxNumContigs;

	if ( numThreads < 1 ) { cerr << "numThreads must be greater than 0" << endl; exit( -1 ); }
	if ( numThreads > 54 ) { cerr << "numThreads must be less than 55" << endl; exit( -1 ); } // due to merBlast readCsr prefix ASCII limitations

	int numWorkerThreads = numThreads;
	ioWorker.init( argv[ 4 ], numWorkerThreads );
	
	
	string outputFilePrefix = argv[ 5 ];  

	long int numEntries = 1;
	PackedDNASequence::init( numEntries, merSize );

	if (!g_bLoadBinaryMerhash) {
		for ( int i = 0; i < g_numHashTables; i++ )
		{
			HashTable *h = new HashTable( numEntries / g_numHashTables * 2 );
			v_merHash.push_back( h );     
		}		

		for( int i =0; i < g_numMutexes; i++ )
		{
			boost::detail::spinlock *m = new boost::detail::spinlock();
			v_pMerHashAccess.push_back( m );
		}
	}

	contigHashLock = new boost::detail::spinlock();
	
	// load main merHash

	cerr << "preparing hashes ( auxiliary info )..." << endl;
	boost::thread threads[ numThreads ];
	for ( int i = 0; i < vContigsIn.size(); i++ )
	{	  
		threads[ i ] = boost::thread( prepareHashes, vContigsIn[ i ], merSize, 8  );
	}
	for ( int threadI = 0; threadI < vContigsIn.size(); threadI++ )
	{
		threads[ threadI ].join();
	}
	cerr << "done preparing hashes ( auxiliiary info )..." << endl;

	// file prep
	int myFileI = 0;	
	for( int threadI = 0; threadI < numWorkerThreads; threadI++ )
	{
		std::vector< unsigned int > validPrefixCodes; /// empty -- accept all prefixes for now

		threads[ threadI ] = boost::thread( lookupMers, merSize, validPrefixCodes, numWorkerThreads, threadI, outputFilePrefix );      
		string outputFile = outputFilePrefix;
		outputFile += ".";
		outputFile += boost::lexical_cast< string >( threadI );

	}

	for ( int threadI = 0; threadI < numWorkerThreads; threadI++ )
	{
		threads[ threadI ].join();
	}
    return( 0 );
}



