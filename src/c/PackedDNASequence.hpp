#ifndef PACKED_DNA_SEQUENCE_HPP
#define PACKED_DNA_SEQUENCE_HPP

#include <math.h>
#include <string>
#include <assert.h>
#include <iostream>
#include <string.h>

// BYTES_PER_MER must be defined externally!
//    choose a BYTES_PER_MER by taking the ceiling function of ( your maximum mersize / 4 )

using namespace std;

class PackedDNASequence 
{
  // this  table associates a 4-mer's ascii value with a packed char value, in essence 
  //   converting a 4-byte value ( unsigned int ) to a 8-bit value ( single byte )
  
  // when mer length != a multiple of 4,
    // in effect, we pad the sequence with extra "AA"'s to align to the next 4-mer boundary
    // this way we can test for equality by comparing the blocks for equality
  
  // To use it, you must first call init() before creating any PackedDNASequence objects.  Further, 
  //    all such objects must have the same merSize, as specified in the parameters to init().
  static unsigned char *fourMerToPackedCode;
  static unsigned int packedCodeToFourMer[ 256 ];
  static unsigned char m_packedLen; // length of data buffer
  static unsigned char m_len;  // actual length of unpacked sequence

  unsigned char m_data[ BYTES_PER_MER ];
public:
  PackedDNASequence( const char *seq_ );
  PackedDNASequence();
  
  PackedDNASequence &operator=( const PackedDNASequence &that );
  PackedDNASequence( const PackedDNASequence &seq );

  bool operator<( const PackedDNASequence &rhs ) const
  {
      // would be faster to do this in 8-byte chunks...
      for( int i = 0; i < BYTES_PER_MER; i++ )
      {
          if ( m_data[ i ] != rhs.m_data[ i ] )
          {
              return( m_data[ i ] < rhs.m_data[ i ] );
          }
      }
      return( false );
  }
    
  // numInitialEntries is just an initial recommendation to avoid having to resize the table too often ( not critical )
  static void init( unsigned long int numInitialEntries, unsigned int merSize ); // must be called first

  // useful for debugging: returns raw compressed data
  char *getData() { return(( char * ) m_data ); }
  
  // set the sequence
  void setSeq( const char *seq_ );
  
  static inline bool isSupportedChar( const char c ) {
    return ( c == 'A' || c == 'C' || c == 'G' || c == 'T' );
  }
  // translates only the prefix ( currently set to 4-mer ) to a unique numeric id
  static inline unsigned char encode( const char *fourMer )
  {
    return( fourMerToPackedCode[ *( ( unsigned int * ) ( fourMer ) ) ] );
  }
 
  // converts to readable string, client retains ownership of buffer
  void convert( char **buffer ) const;  
  
  // test for equality, assume they are the same length
  static inline bool equals( const PackedDNASequence &seq, const PackedDNASequence &seq2 ) 
  {  
    return( ( memcmp( seq.m_data, seq2.m_data, m_packedLen ) == 0 ) );  
  }
  
  // hash func
  static unsigned long hash( const PackedDNASequence &s )  //djb2 function
  {
  	  unsigned long hash = 5381;
	  int c;
	  const unsigned char *a = s.m_data;
	  for( int i = 0; i < m_packedLen; i++ )
	  {
		 c = *a++;
		 hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
	   }
	   return hash;
  }
  ~PackedDNASequence();

};


#endif
