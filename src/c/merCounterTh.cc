#include <time.h>
#include <boost/thread/thread.hpp>
#include <boost/unordered_map.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <boost/signals2/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <sparsehash/sparse_hash_map>
#include <boost/smart_ptr/detail/spinlock.hpp>
#include <mutex>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

#include "PackedDNASequence.hpp"  
#include "IOWorkerSimple.hpp"

using google::sparse_hash_map;

#define MAX_LINE_LEN ( ioWorker.getBufLineWidth() )
#define PAGES_PER_BUFFER 1000

using namespace std;

typedef PackedDNASequence PackedMer;
class CompareKeys   
{
public: 
	inline bool operator()( const PackedMer &a, const PackedMer &b ) const
	{
		return( PackedDNASequence::equals( a, b ) );
	}
};


class HashKeys
{
public:
	inline std::size_t operator()( const PackedMer &a ) const
	{
		return PackedDNASequence::hash( a ); 
	}
};
typedef sparse_hash_map< PackedMer, unsigned short, HashKeys, CompareKeys > HashTable;

int g_hashPrefixLen = 8;  // 4**g_hashPrefixLen = total number of hashes, must be 4 b/c of PackedDNASequence::encode() coupling
int g_numHashTables = pow( 4, g_hashPrefixLen );
int g_numMutexes = g_numHashTables;
std::vector< HashTable * > v_merHash;
std::vector< boost::detail::spinlock * > v_pMerHashAccess;
boost::detail::spinlock outputLock;
IOWorkerSimple ioWorker;
int numColonsInName = 4;
int qualOffset = 64;
int minCountToReport = 0;



string decodeMer( unsigned int x, unsigned int merLen )
{
	int remainder = x;
	string mer = "";
	for( int slot = merLen - 1; slot >= 0; slot-- )
	{
		int valInSlot =  int( remainder / pow( 4,slot ) );
		char base = ' ';

		if( valInSlot == 0 ) { base = 'A'; }
		else if( valInSlot == 1 ) { base = 'C'; }
		else if( valInSlot == 2 ) { base = 'G'; }
		else if( valInSlot == 3 ) { base = 'T'; }
		else { assert( 0 ); }

		mer += boost::lexical_cast< string >( base );

		remainder -= valInSlot * pow( 4, slot  );
	}
	return( mer );
}

inline string revcomp( const string &s )
{
	string rs = s;// allocate same amount of memory
	
	int rsCsr = 0;
	for( int i = s.length() - 1; 
		i >= 0;
		i-- )
	{
		switch( s[ i ] )
		{
			case 'A': 
			rs[ rsCsr ] = 'T'; break;
			case 'T':
			rs[ rsCsr ] = 'A'; break;
			case 'C':
			rs[ rsCsr ] = 'G'; break;
			case 'G':
			rs[ rsCsr ] = 'C'; break;
			default:
			rs[ rsCsr ] = s[ i ];
		}
		rsCsr++;
	}
	return( rs );
}


inline char revcomp( const char x )
{
	switch( x )
	{
		case 'A': return( 'T' ); 
		case 'C': return( 'G' ); 
		case 'G': return( 'C' ); 
		case 'T': return( 'A' ); 
	}
	return( x );
}
inline bool isValid( const char x )
{
	switch( x )
	{
		case 'A':
		case 'C':
		case 'G':
		case 'T':
		return( true );
	}
	return( false );
}

/* We want to avoid validating each mer individually ( O( k * n ) ).  So we find the first 
   valid mer, thereafter we just check the trailing new character; if invalid, we zoom 
   forward all the way past that char and start anew ( more like 2 * n ). 
*/
void countMersInString( const char *seq, int merSize, std::vector< const char * > validPrefixes ) 
{
	int seqLen = strlen(seq);
	int effSeqLen = seqLen - merSize + 1; // effective seq length 

	for( int i = 0; i < effSeqLen; i++ )
	{
		int j = -1;
		bool bSearchForValidMer = false;

		if ( i == 0 ) { 
			// first time through, start at head
			bSearchForValidMer = true; j = i;
		}
		else if ( !isValid( seq[ i + merSize - 1 ] ) ) {
			// not first time through, that means you had a valid mer already...check the
			//    newest trailing char 
			bSearchForValidMer = true; j = i + merSize;
		}

		if ( bSearchForValidMer ) 
		{
			int currentValidMerLen = 0;
			while( currentValidMerLen < merSize ) {
				if ( j >= seqLen ) { return; }

				if ( isValid( seq[j]) ) { 
					currentValidMerLen++;
				} else {
					currentValidMerLen = 0; 
				}
				j++;
			}
			// valid mer found ending at j
			i=j-merSize;
		}

		// mer is valid at this point
		// use the 4-mer encoding for dna packing for the hashtable prefix as well			
		unsigned int prefixCode1 =  PackedDNASequence::encode( seq+i );
		unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( seq + i + 4);
		int prefixI = 0;
		bool bValidPrefixFound = true;
		if ( validPrefixes.size() > 0 )
		{
			bValidPrefixFound = false;
			for ( ; prefixI < validPrefixes.size(); prefixI++ )
			{
				if ( strncmp( validPrefixes[ prefixI ], seq + i, strlen( validPrefixes[ prefixI ] ) ) == 0 ) 
				{
					bValidPrefixFound = true;
					break;
				}
			}
		}
		
		PackedDNASequence tempPackedMer;
		if ( bValidPrefixFound )
		{
			tempPackedMer.setSeq( seq + i );
			assert( prefixCode < g_numHashTables );

			boost::detail::spinlock &lock = *( v_pMerHashAccess[ prefixCode ] );
			{						
			   // begin critical section
				std::lock_guard< boost::detail::spinlock > writeLock( lock );
				
				HashTable::iterator i = v_merHash[ prefixCode ]->find( tempPackedMer );
				if ( i != v_merHash[ prefixCode ]->end() )
				{	
					 //update value in hash for this mer-key
					if ( i->second < 65535 ) { i->second++; }
				}
				else
				{
					// new mer
					HashTable *h= v_merHash[ prefixCode ];
					HashTable::value_type v( tempPackedMer, 1 );
					h->insert( v );								
				}
				// end critical section
			}
		}
	}
}


// should be called one-per-thread: not reentrant
void countMers( int merSize, std::vector< const char * > validPrefixes, 
	int skipLineCnt, int threadId, bool bTrim )
{	
	bool revComp = 1;
	char str[ 10000 ];
	int cnt = 0;
	long int totalNumLinesFromBuffer = 0;
	long int totalEffSeqLen = 0;
	long int mersPastFiltering = 0;
	long int mersAdded = 0;
	long int mersAlreadyInHash = 0; 
	long int mersLookedAt = 0;
	long int readsStartProcessed = 0; 
	long unsigned int seqI = 0;
	char *dummy = new char[ MAX_LINE_LEN ];
	PackedMer tempPackedMer;
	char *lineBuf = new char[ MAX_LINE_LEN ];
	seqI = 0;
	char specialTrimCharacter = ( char ) ( qualOffset + 2 );
	char *lineBufBak = new char[ MAX_LINE_LEN ];
	int bufNumLines = ioWorker.getOutBufNumLines();
	int threadCnt_ = skipLineCnt;
	int origBufNumLines = bufNumLines;  // the actual # you grab may differ from the original intended number due to EOF  
	char *bufLines = 0;
	bool bFirstTime = true;
	while( ioWorker.getLines( threadId,  &bufLines, bufNumLines ) )
	{	    
		totalNumLinesFromBuffer += bufNumLines;
		bFirstTime = false;
		char *seq, *qual;
		for( int i = 0; i < bufNumLines;  )
		{	
			char *header = &bufLines[ i * ioWorker.getBufLineWidth() ];
			i++;
			char *seq = &bufLines[ i * ioWorker.getBufLineWidth() ];
			i++;
			char *spacer = &bufLines[ i * ioWorker.getBufLineWidth() ];
			i++;
			char *qual= &bufLines[ i * ioWorker.getBufLineWidth() ];
			i++;

			if ( seqI % 1000 == 0 ) { cerr << "."; }
			seqI++;
			
			int seqLen = strlen( seq );
			if ( seqLen < merSize ) { continue; }
			int firstValidMerI = -1;
			int currentValidMerLen = 0;
			for (unsigned j = 0; j < seqLen; ++j )
			{
				// truncate sequence at first sign of poor quality ( qual value = 2 )
				if ( bTrim && qual[j] == specialTrimCharacter ) { seqLen = j; seq[ j ] = '\0'; break; }
			}
			
			readsStartProcessed++;                
			countMersInString( seq, merSize, validPrefixes );

			// perform revcomp in-place on seq
			char *qualEnd = qual + seqLen - 1;
			char *seqEnd = seq + seqLen - 1;
			int seqSlots = ( int ) ( seqLen / 2.0 + 0.5 );
			for ( int i = 0; i < seqSlots; i++ )
			{
				char x = revcomp( *( seq + i ) );

				*( seq + i ) = revcomp( *( seqEnd - i ) );
				*( seqEnd - i ) = x;

				char q = *( qual + i );
				*( qual + i ) = *( qualEnd - i );
				*( qualEnd - i ) = q;
			}
			countMersInString( seq, merSize, validPrefixes );
		}
	}	
	/*
	{
		std::lock_guard< boost::detail::spinlock > writeLock( outputLock );

		cerr << "Total num lines from buffer " << totalNumLinesFromBuffer << endl;
		cerr << "Reads start processed " << readsStartProcessed << endl;
		cerr << "Total eff seq len " << totalEffSeqLen << endl;
		cerr << "Mers looked at " << mersLookedAt << endl;
		cerr << "Mers past filtering " << mersPastFiltering << endl;
		cerr << "Mers added " << mersAdded << endl;
		cerr << "Mers already in hash " << mersAlreadyInHash << endl;
	}*/
}

void calcHistogram()
{
	unsigned long int bins[ 65536 ];
	unsigned long int maxCnt = 0;
	unsigned long int maxNonZeroBin = 0;

	bzero( bins, 65536 * sizeof( unsigned long int ) );
	for ( int hashI = 0; hashI < g_numHashTables; hashI++ )
	{
		for( HashTable::iterator i = v_merHash[ hashI ]->begin();
			i != v_merHash[ hashI ]->end();
			i++ )
		{
			bins[ i->second ]++;
			if ( bins[ i->second ] > maxCnt ) { maxCnt = bins[ i->second ]; }
			if ( i->second > maxNonZeroBin ) { maxNonZeroBin = i->second; }
		}
	}

	cerr << "Histogram of counts " << endl;
	double scalingFactor = ( double ) 120.0 / ( double ) maxCnt;  // use up to 60 columns to show hist bars
	for ( unsigned int i = 1; i <= maxNonZeroBin; i++ )
	{
		cerr << "||" << i << "\t" << bins[ i ] << "\t" << "|";
		for ( int xCnt = 0; xCnt < ( long int )( bins[ i ] * scalingFactor ); xCnt++ )
		{
			cerr << "X";
		}
		cerr << "\n";
	}
	cerr << "End histogram of counts" << endl;
}
void dumpMers( string outputName, vector< unsigned int > validPrefixCodes, int startI, int endI )
{
	ofstream out( outputName );
	assert( out.is_open() );
	char *buf = new char[ MAX_LINE_LEN ];
	long int mersTotal = 0, mersPrinted = 0;

	for ( int j = startI; j <= endI; j++ )
	{
		int hashI = validPrefixCodes[ j ];
		for ( HashTable::iterator i = v_merHash[ hashI ]->begin();
			i != v_merHash[ hashI ]->end();
			i++ )
		{	      
			mersTotal++;

			char *c = &buf[0];

			i->first.convert( &c );

			if ( i->second >= minCountToReport )
			{		

				out << buf << "\t";		
				out << i->second;	   
				out << endl;

				mersPrinted++;
			}
		}
	}	 

	out.close();   
}





int main( int argc, char *argv[] )
{   

	if ( argc < 4 )
	{
		cerr << "sizeof std::pair < PackedDNASequence, int > " << sizeof( std::pair< PackedDNASequence, int > ) << std::endl;
		cerr << "usage: [ mer_size ] [ minCountToReport ] [ numThreads ] [ prefixListToHash ] [ outputPrefix ] [ fastq_descriptor_file ] [ trim_flag ]\n" << endl;
		exit( 0 );
	}  	
	for ( int i =0; i < argc; i++ )
	{
		cerr << "Arg " << i << ": " << argv[ i ] << endl;  
	}    
	
	int merSize = boost::lexical_cast< int, char * >( argv[ 1 ] );
	minCountToReport = boost::lexical_cast< int, char *> ( argv[ 2 ] );
	int numThreads = boost::lexical_cast< int, char *>( argv[ 3 ] );


	const char *prefixList = argv[ 4 ];
	const char *outputPrefix = argv[ 5 ];
	const char *inputDescriptor = argv[ 6 ];
	const char *trimFlag = argv[ 7 ];
	bool bTrim = boost::lexical_cast< int >( trimFlag );

	int numEntries = 1000000;
	PackedDNASequence::init( numEntries, merSize );
	PackedDNASequence temp;
	vector< string > prefixes;
	vector< const char * > validPrefixes;
	vector< unsigned int > hashTableCodes;
	if ( strcmp( "-", prefixList ) != 0 )
	{
		boost::split( prefixes, prefixList, boost::is_any_of( "+" ) ); 
		assert( prefixes.size() > 0 );

    // create a list of binary-packed hashtable codes ( 8-mers ) from prefix list ( used for fast dumping of mers )
		for( int i = 0 ; i < prefixes.size(); i++ )
		{
			validPrefixes.push_back(prefixes[ i ].c_str() );

			vector< unsigned int > newHashTableCodes;
			if ( prefixes[ i ].length() < 8 )
			{
				int lengthToGenerate = 8 - prefixes[ i ].length();

				for ( int j = 0; j < pow( 4, lengthToGenerate ); j++ )
				{
					string newPrefix = prefixes[ i ] + decodeMer( j, lengthToGenerate ) ;
					unsigned int prefixCode1 =  PackedDNASequence::encode( newPrefix.c_str() );
					unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( newPrefix.c_str() + 4 );

					newHashTableCodes.push_back( prefixCode );
				}  
			}
			else
			{
				unsigned int prefixCode1 =  PackedDNASequence::encode( prefixes[ i ].c_str() );
				unsigned int prefixCode = ( prefixCode1 << 8 ) + PackedDNASequence::encode( prefixes[ i ].c_str() + 4 );

				newHashTableCodes.push_back( prefixCode );
			}

 	    // add candidate codes iff they don't exist already
			for( int j = 0; j < newHashTableCodes.size(); j++ )
			{
				if ( std::find( hashTableCodes.begin(), hashTableCodes.end(), newHashTableCodes[ j ] ) == hashTableCodes.end() )
				{
					hashTableCodes.push_back( newHashTableCodes[ j ] );
				}
			}
		}
	}
	else
	{
		for( int j = 0; j < 65536; j++ )
		{
			hashTableCodes.push_back( j );
		}
	}
	
	for ( int i = 0; i < g_numHashTables; i++ )
	{
		HashTable *h = new HashTable( numEntries / g_numHashTables * 2 );
		v_merHash.push_back( h );     
	}		

	for( int i =0; i < g_numMutexes; i++ )
	{
		boost::detail::spinlock *m = new boost::detail::spinlock();
		v_pMerHashAccess.push_back( m );
	}

  // file prep
	boost::thread threads[ numThreads ];
	unsigned int prefixStart = 0;
	unsigned int prefixEnd = 65536;
	
	ioWorker.init( inputDescriptor, numThreads );

	int myFileI = 0;
	for( int threadI = 0; threadI < numThreads; threadI++ )
	{
		std::vector< string > myFileNames;

		threads[ threadI ] = boost::thread( countMers, merSize, validPrefixes, numThreads, threadI, bTrim );       
	}

	for ( int threadI = 0; threadI < numThreads; threadI++ )
	{
		threads[ threadI ].join();
	}

	cerr << "Done with counting" << endl;
	time_t rawtime;
	struct tm *timeinfo;
	char buffer [200];
	time( &rawtime ); 
	timeinfo = localtime( &rawtime );
	strftime (buffer,200,"%c",timeinfo);	

	int curI = 0;
	vector< string > vOutputFiles;

	for ( int i = 0; i < numThreads; i++ )
	{
		int hashTablesPerThread = ceil( ( hashTableCodes.size() / numThreads ) + 0.5 );
		int endI = curI + hashTablesPerThread - 1;
		if ( endI >= hashTableCodes.size() ) { endI = hashTableCodes.size() - 1; }
		string outputFile = outputPrefix;
		outputFile += ".";
		outputFile += boost::lexical_cast< string >( i );

		vOutputFiles.push_back( outputFile );
		threads[ i ] = boost::thread( dumpMers, outputFile, hashTableCodes, curI, endI );
		curI += hashTablesPerThread;
	}
	for ( int i = 0; i < numThreads; i++ )
	{
		threads[ i ].join();
	}

	calcHistogram();

	time( &rawtime );
	timeinfo = localtime( &rawtime );
	strftime( buffer, sizeof( buffer ), "%c", timeinfo );


	return( 0 );

}

