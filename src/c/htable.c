/*
 * Tessellation Many-core Operating System. V2. 
 * 
 * Cell runtime.
 *
 * Copyright (c) 2014 The Regents of the University of California.
 *
 * This code is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 only, as published by
 * the Free Software Foundation.
 *  
 * This code is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 only, as published by
 * the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License version 2 for more
 * details (a copy is included in the LICENSE file that accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version 2
 * along with this work; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "htable.h"
#include "colors.h"

#if 0
#define DEBUG(fmt, ...)                                          \
    do {                                                         \
        printf("[%s:%d] " fmt, __FILE__, __LINE__, __VA_ARGS__); \
    } while (0)
#else
#define DEBUG(...)
#endif

#define WARN(fmt,...)                                   \
    do {                                                \
        fprintf(stderr, KRED "WARN [%s:%d]: " fmt KNORM "\n",   \
               __FILE__, __LINE__, ##__VA_ARGS__);      \
    } while (0)


static const unsigned int primes[] = {
    53, 97, 193, 389, 769, 1543, 3079, 6151, 12289, 24593, 49157, 98317,
    196613, 393241, 786433, 1572869, 3145739, 6291469, 12582917, 25165843,
    50331653, 100663319, 201326611, 402653189, 805306457, 1610612741
};

static const unsigned int num_primes = sizeof(primes) / sizeof(primes[0]);

struct hentry {
    char *key;
    char *val;
    size_t hash;
    struct hentry *next;
};

struct htable {
    size_t capacity;
    size_t num_entries;
    size_t max_entries;
    size_t load_threshold;
    size_t collisions;
    size_t threshold_crossings;
    struct hentry **entries;
    char *description;
};

struct htable_iter {
    size_t index;
    struct hentry *next;
    int end;
};

// simple DJB2 hash
static size_t djb2_hash(char *key)
{
    size_t h = 5381;
    int c;
    while ((c = *key++))
        h = ((h<< 5) + h) + c; /* h * 33 + c */
    return h;
}

#define GET_INDEX(hash) ((hash) % (h->capacity))

struct htable *create_htable(size_t min_capacity, char *description)
{
    size_t capacity;
    int prime_found = 0;
    // get the prime just above the min capacity
    for (int i = 0; i < num_primes; i++) {
        if (primes[i] > min_capacity) { 
            capacity = primes[i]; 
            prime_found = 1;
            break; 
        }
    }
    if (!prime_found) {
        WARN("prime not found > %lu", min_capacity);
        errno = ENOMEM;
        return NULL;
    }
    DEBUG("Chose prime %lu for htable capacity\n", capacity);
    struct htable *h = (struct htable *)malloc(sizeof(struct htable));
    if (!h) {
        WARN("No memory for malloc of htable");
        errno = ENOMEM;
        return NULL;
    }
    h->entries = (struct hentry **)malloc(sizeof(struct hentry *) * capacity);
    if (!h->entries) {
        WARN("No memory for malloc of htable entries");
        free(h); 
        errno = ENOMEM;
        return NULL; 
    }
    memset(h->entries, 0, sizeof(struct hentry *) * capacity);
    h->capacity  = capacity;
    h->num_entries = 0;
    // for warnings about heavily loaded table
    h->load_threshold = capacity * 0.65;
    h->collisions = 0;
    h->threshold_crossings = 0;
    h->max_entries = 0;
    h->description = strdup(description);
    return h;
}

void destroy_htable(struct htable *h, int to_free)
{
	for (int i = 0; i < h->capacity; i++) {
        struct hentry *entry = h->entries[i];
        while (entry) {
			struct hentry *prev_entry = entry;
			entry = entry->next;
            if (to_free & FREE_KEYS)
                free(prev_entry->key);
            if (to_free & FREE_VALS)
                free(prev_entry->val);
			free(prev_entry);
		}
	}
    free(h->entries);
    free(h);
}

int htable_num_entries(struct htable *h) 
{
    return h->num_entries;
}

int htable_put(struct htable *h, char *key, void *val)
{
    // make it unique by first checking for a duplicate
    size_t hash = djb2_hash(key);
    size_t index = GET_INDEX(hash);
    for (struct hentry *entry = h->entries[index]; entry; entry = entry->next) {
        if (hash == entry->hash && strcmp(key, entry->key) == 0)
            return EEXIST;
    }

    struct hentry *entry = (struct hentry *)malloc(sizeof(struct hentry));
    if (!entry) 
        return ENOMEM; 

    entry->key = key;
    entry->val = val;
    entry->hash = hash;
    entry->next = h->entries[index];
    if (entry->next) {
        h->collisions++;
        DEBUG("collision at %d, tot %lu\n", index, h->collisions);
    }
    h->entries[index] = entry;
    if (h->num_entries++ == h->load_threshold) {
        WARN("Hit load threshold %lu on hash table %s", h->load_threshold, h->description);
        h->threshold_crossings++;
    }
    if (h->num_entries > h->max_entries)
        h->max_entries = h->num_entries;
    return 0;
}

void *htable_get(struct htable *h,  char *key)
{
    size_t hash = djb2_hash(key);
    for (struct hentry *entry = h->entries[GET_INDEX(hash)]; entry; entry = entry->next) {
        if (hash == entry->hash && strcmp(key, entry->key) == 0)
            return entry->val;
    }
    return NULL;
}

void *htable_del(struct htable *h,  char *key)
{
    size_t hash = djb2_hash(key);
    struct hentry **prev_link = &(h->entries[GET_INDEX(hash)]);
    for (struct hentry *entry = h->entries[GET_INDEX(hash)]; entry; entry = entry->next) {
        if (hash == entry->hash && strcmp(key, entry->key) == 0) {
            *prev_link = entry->next;
            h->num_entries--;
            void *val = entry->val;
            free(entry);
            return val;
        }
        prev_link = &(entry->next);
    }
    return NULL;
}

struct htable_iter *htable_get_iter(struct htable *h)
{
    struct htable_iter *iter = malloc(sizeof(struct htable_iter));
    if (!iter)
        return NULL;
    iter->index = 0;
    iter->next = NULL;
    return iter;
}

void *htable_get_next(struct htable *h, struct htable_iter *iter)
{
    while (iter->index < h->capacity) {
        struct hentry *entry = (iter->next ? iter->next : h->entries[iter->index]);
        if (entry) {
            void *val = entry->val;
            iter->next = entry->next;
            if (!entry->next)
                iter->index++;
            return val;
        } else {
            iter->index++;
        }
    }
    return NULL;
}

long mem_htable(struct htable *h)
{
    return sizeof(*h);
}

void test_htable(void)
{
    const int min_capacity = 500;
    const int num_elems = 1000;
    const int num_rounds = 10000;
    
    struct htable *h = create_htable(min_capacity, "test");
    if (!h) {
        WARN("cannot create htable");
        return;
    }
    
    struct elem {
        char name[8];
        char data[8];
        int in_ht;
    };
    
    struct elem *elems = malloc(num_elems * sizeof(struct elem));

    for (int i = 0; i < num_elems; i++) {
        sprintf(elems[i].name, "node%d", i);
        sprintf(elems[i].data, "data%d", i);
        elems[i].in_ht = 0; 
    }
    srand(31);
    struct elem *e;
    int puts = 0, gets = 0, dels = 0;
    for (int i = 0; i < num_rounds; i++) {
        int k = rand() % num_elems;
        int op = rand() % 3;
        if (op == 0) {
            if (!elems[k].in_ht) {
                int err = htable_put(h, elems[k].name, &elems[k]);
                if (err)
                    WARN("Could not put %s into table", elems[k].name);
                elems[k].in_ht = 1;
                puts++;
            }
        } else {
            if (op == 1) {
                e = htable_get(h, elems[k].name);
                gets++;
            } else {
                e = htable_del(h, elems[k].name);
                dels++;
            }
            if (elems[k].in_ht) {
                if (!e)
                    WARN("Could not find %s in table", elems[k].name);
                else
                    if (strcmp(elems[k].name, e->name) != 0)
                        WARN("Found wrong elem %s should be %s", 
                             e->name, elems[k].name);
            } else if (e) {
                WARN("Found %s in table when it should not be there", e->name);
            }
            if (op == 2)
                elems[k].in_ht = 0;
        }
        if (!(i % (num_rounds / 10)))
            printf("htable test round %d\n", i);
    }
    printf("Htable test concluded, capacity %lu, max elems: %lu (%.3f)\n"
           "%d puts, %d gets, %d dels, %lu collisions (%.3f), %lu crossings\n",
           h->capacity, h->max_entries, (double)h->max_entries / h->capacity,
           puts, gets, dels, h->collisions, (double)h->collisions / puts, 
           h->threshold_crossings);

    for (int i = 0; i < num_elems; i++) {
        if (elems[i].in_ht)
            htable_del(h, elems[i].name);
    }

    for (int i = 19; i >= 0; i--) {
        htable_put(h, elems[i].name, &elems[i]);
    }

    struct htable_iter *iter = htable_get_iter(h);
    printf("Listing table via iterator: %d elems\n", htable_num_entries(h));
    int i = 0;
    while ((e = htable_get_next(h, iter)) != NULL) {
        printf("%d: name \"%s\" data \"%s\"\n", i, e->name, e->data);
        i++;
    }

}

