#include <math.h>
#include <string>
#include <assert.h>
#include <iostream>
#include <string.h>
#include "PackedDNASequence.hpp"

using namespace std;

unsigned char *PackedDNASequence::fourMerToPackedCode;
unsigned int PackedDNASequence::packedCodeToFourMer[ 256 ];
unsigned char PackedDNASequence::m_len;
unsigned char PackedDNASequence::m_packedLen;

//
// main buffer 
// 
void PackedDNASequence::setSeq( const char *seq_ )
{
    assert( strlen( seq_ ) >= m_len ); // should maybe throw exception instead of assert, but wth

    int j = 0;     // coordinate along unpacked string ( matches with m_len )
    int i = 0;     // coordinate along packed string ( matches with m_packedLen )
	 
    // do the leading seq in blocks of 4
    for ( ; j <= m_len - 4; i++, j+=4 )
    {
	   *( m_data + i ) = fourMerToPackedCode[ ( *( ( unsigned int * ) ( seq_ + j ) ) ) ] ;
    }
    
    // last block is special case if m_len % 4 != 0: append "A"s as filler        
    int remainder = m_len % 4;
    char blockSeq[5] = "AAAA";
    for( int i = 0; i < remainder; i++ )
    {
      *( blockSeq + i ) = *( seq_ + j + i );
    }
    unsigned int block = *( ( unsigned int * ) ( blockSeq ) );       
    *( m_data + i ) = fourMerToPackedCode[ block ] ;
}

// convert from internal packed code to char sequence
void PackedDNASequence::convert( char **seq_ ) const
{
	 char *seq = *seq_;
	 int j = 0; 
	 int i = 0;
	 for( ; i < m_packedLen ; i++, j += 4 )
	 {
		*( ( unsigned int * )( seq + j ) ) = packedCodeToFourMer[ *( m_data + i ) ];
	 }
	 *( seq + m_len ) = '\0';
}


PackedDNASequence::PackedDNASequence( const char *seq_ )
{
	 setSeq( seq_ );
} 

void PackedDNASequence::init( unsigned long int initialNumEntries, unsigned int merSize )
{
  int maxMerSize = BYTES_PER_MER * 4;
  if ( merSize > BYTES_PER_MER * 4 ) 
  { 
    cerr << "requested mer size of " << merSize << " is too big for compiled setting BYTES_PER_MER: please ";
    cerr << " increase BYTES_PER_MER to at least " << ceil( merSize / 4.0 ) << " and recompile" << endl;
    exit( -1 );
  }
  fourMerToPackedCode = new unsigned char[ 2000000000 ];
  int merLen = 4;
  for ( int i = 0; i < 256; i++ )
  {
    // convert a packedcode to a 4-mer
    int remainder = i;
    string mer = "";
    for( int slot = merLen-1; slot >= 0; slot-- )
    {
        int valInSlot =  int( remainder / pow( 4, slot ) );
        char base;

        if( valInSlot == 0 ) { base = 'A'; }
        else if( valInSlot == 1 ) { base = 'C'; }
        else if( valInSlot == 2 ) { base = 'G'; }
        else if( valInSlot == 3 ) { base = 'T'; }
        else{ assert( 0 ); }
        
        mer += base;

        remainder -= valInSlot * pow( 4, slot );
    }
        
    unsigned int *merAsUInt = ( unsigned int * ) mer.c_str();    
  	fourMerToPackedCode[ *merAsUInt ] =  ( unsigned char ) i;  
  	packedCodeToFourMer[ ( unsigned char ) i ] = *merAsUInt;
  }

  m_len = ( unsigned char ) merSize ;
  m_packedLen = ceil( m_len / 4.0 );
}



PackedDNASequence &PackedDNASequence::operator=( const PackedDNASequence &that )
{
  if ( this != &that )
  {
    memcpy( m_data, that.m_data, that.m_packedLen );
  }
}

PackedDNASequence::PackedDNASequence()
{
}
PackedDNASequence::PackedDNASequence( const PackedDNASequence &that )
{
  memcpy( m_data, that.m_data, that.m_packedLen );
}

PackedDNASequence::~PackedDNASequence()
{
}

