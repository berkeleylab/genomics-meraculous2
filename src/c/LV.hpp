#include "Compat.h"
#include <stdio.h>
#include <string.h>
#define MAX_K 6

void memsetint(int* p, int value, int count)
{
  //volatile;  needed for gcc optmization bug?
  int *q = p;
  for (int i = 0; i < count; i++) 
  {
    q[i] = value;
  }
}

using namespace std;

/* 
Landau-Vishkin algorithm explained:

This algo attempts to find the highest-scoring semi-global alignment of pattern p along text t 
with a max # of errors k.  

This means we must include the first base of p in any alignment and try to extend consecutively
along p as far as we can till penalties overwhelm us / ma.

First we explain the vanilla version that returns the longest match with a max of e errors
( no semi-global alignments, no mismatch penalty )

Given an alignment matrix between pattern p and text t, we keep track of:
L[d][e] = furthest along t you can traverse ending on diagonal d with a max of e errors ( starting
from [0][0]) 

init L[0][0] = # of initial matches between p and t
for e in 1 to MAX_E
  for d in 0, 1, -1, 2, -2 ... -e
    best = max( L[d][e-1] + 1, // match/mismatch
                L[d-1][e-1] + 1, // insertion
                L[d+1][e-1] ) // deletion
     best += number of matching bases along diagonal d starting from the position chosen above
     L[d][e] = best; 
     if ( best == patternLen - abs( d ) ) you are done

To add a mismatch penalty, we compute a score each time we fill in an L[][] value.
   score = best * matchBonus + e * gap/mismatchPenalty;
Since we don't distinguish between gaps and mismatches nothing fancier is needed. We just need 
   to track the current highest scoring terminal location in L[][]; when done traversing,
   this value is the winner ( as opposed to always the final value traversed in the vanilla case )

To add extension alignment ( overhangs must go off the right end ), first recognize that we actually do not want to report any overhanging bases on the read. So really nothing special needs to be done---we just want to report the best alingment we can find.  It is similar to local alignment except we must count all bases starting from the left of the read, but may ignore bases off the right end.


Implementation notes:
- p is the search string, t is the text to search through.  They are not interchangeable because 
we are trying to go along the full length of p but not t. 
- p is on the y-axis of the (unstored) main alignment matrix, while t is x-axis.  This matters 
because diagonal d + 1 for instance means we are moving one along the text not the pattern, 
thus introducing a gap in the pattern.
- the return values pAlignLen,tAlignLen represent the furthest base of p/t respectively that 
the alignment covers.  Thus:

p: AACCGGGGG
t:  AA--GGGGGGGGGGG
means pAlignLen = 9, tAlignLen =  7.  

pAlignLen thus always equals the ‘best’ value stored in L[][].  tAlignLen equals pAlignLen + d.

- L[0][MAX_K] in the code is equiv. to L[0][0] in our pseudocode, b/c we want to allow for adding 
 negative d to the index
- the first loop up to done1: is just computing the initial match using fast 64-bit bitwise XOR
- dTable[] is just a speedup for computing the next d in the loop sequence

*/


// Computes the edit distance between two strings without returning the edits themselves.
class LandauVishkin {
public:
    LandauVishkin(int cacheSize = 0)
    {
        memsetint(L[0], -2, (MAX_K+1)*(2*MAX_K+1));
        memset(A[0], 32, (MAX_K+1)*(2*MAX_K+1));

        //
        // Initialize dTable, which is used to avoid a branch misprediction in our inner loop.
        // The d values are 0, -1, 1, -2, 2, etc.
        //
        for (int i = 0, d = 0; i < 2 * (MAX_K + 1) + 1; i++, d = (d > 0 ? -d : -d+1)) {
            dTable[i] = d;
        }
    }

    ~LandauVishkin()
    {
    }

    void printMatrices( int e, int d )
    {
        cerr << "L: e = " << e << " d = " << d << endl;
        for ( int i = 0; i < MAX_K + 1; i++ )
        {
            for ( int j = 0; j < 2 * MAX_K + 1; j++ )
            {
                cerr << L[i][j] << " ";
            }
            cerr << endl;
        }
        cerr << endl;
        cerr << "A:" << endl;
        for ( int i = 0; i < MAX_K + 1; i++ )
        {
            for ( int j = 0; j < 2 * MAX_K + 1; j++ )
            {
                cerr << A[i][j] << " ";                
            }
            cerr << endl;
        }
        cerr << endl;        

    }
    // Compute the edit distance between two strings, if it is <= k, or return -1 otherwise.
    // looks for the pattern in the text.  pattern/text are not interchangeable because
    //   our heuristic is to maximize alignment length along pattern not text ( gaps factor in ).
    int computeEditDistance(
        const char* pattern,  // the query ( we align along the full length of this )
        int patternLen,
        const char* text,  // the database ( typically longer than pattern )
        int textLen, 
        int k,
        int mismatchPenalty,
        int *pAlignLength, // furthest base along p that the alignment covers
        int *tAlignLength )
    {
        // clear memory first 
        memsetint(L[0], -2, (MAX_K+1)*(2*MAX_K+1));
        memset(A[0], 32, (MAX_K+1)*(2*MAX_K+1));
        
        // to add semiglobal functionality, just trim off any excess pattern by decreasing patternLen.
        //  then add this value back into the reported pAlignLength
        int hiScore = -1;
        int hiScoreE = -1; // the number of errors in the current highest-scoring alignment
        int hiScoreD = 0; // the terminal diagonal for the highest-scoring alignment
        int matchBonus = 1;

        _ASSERT(k < MAX_K);
        k = __min(MAX_K - 1, k); // enforce limit even in non-debug builds
        if (NULL == text) {
            // This happens when we're trying to read past the end of the genome.
            return -1;
        }

        const char* p = pattern;
        const char* t = text;
        int end = __min(patternLen, textLen);
        const char* pend = pattern + end;

        // count number of consecutive matches along main diagonal and store in L[0][MAX_K]
        // if this covers entire alignment, then exit       
        while (p < pend) {
            _uint64 x = *((_uint64*) p) ^ *((_uint64*) t); // store differences as 1-bits in 8-byte window

            if (x) 
            {
                // mismatch found
                unsigned long zeroes;
                CountTrailingZeroes(x, zeroes); // this actually counts the number of leading 
                    // zeros ( perfect matches ) if you look in from left to right
                zeroes >>= 3; // ??? needed to shift, take on faith

                L[0][MAX_K] = __min((int)(p - pattern) + (int)zeroes, end);
                
                // mismatch only counts if it happens within alignment window
                if ( L[0][MAX_K] != end )
                {
                    int score = L[0][MAX_K] * matchBonus;
                    if ( score > hiScore )
                    {
                        hiScore = score;
                        hiScoreD = 0;
                        hiScoreE = 0;
                    }
                    goto mismatchFound;
                }
            }
            p += 8;
            t += 8;
        }
        {
            *pAlignLength = end;
            *tAlignLength = end;
            return 0; // perfect match
        }

mismatchFound:   
        int e = 1;
        int d = 0;
        int best = -1; // the furthest aligning, not necessarily the highest scoring

        // Our scoring function is unusual.  Because we cannot easily track indel information,
        //   our 'matchBonus' gets added to mismatches and indels as well.  We conflate 
        //   mismatches and gaps into a single penalty, and award bonuses to every additional
        //   base along the read that aligns, even if it is a mismatch.  Thus you should probably
        //   set the mismatch penalty higher than you would in a typical scoring function.

        for ( ; e <= k; e++) {
            // Search d's in the order 0, 1, -1, 2, -2, etc to find an alignment with as few indels as possible.
            // dTable is just precomputed d = (d > 0 ? -d : -d+1) to save the branch misprediction from (d > 0)
            int i =0;
            int d = 0;
            for (; d != e+1 ; i++, d = dTable[i]) {
                best = L[e-1][MAX_K+d] + 1; // up
                A[e][MAX_K+d] = 'X';
                int left = L[e-1][MAX_K+d-1];
                if (left > best) {
                    best = left;
                    A[e][MAX_K+d] = 'D';
                }
                int right = L[e-1][MAX_K+d+1] + 1;
                if (right > best) {
                    best = right;
                    A[e][MAX_K+d] = 'I';
                }


                const char* p = pattern + best;
                const char* t = (text + d ) + best;
                int oldBest = best;
                if (*p == *t) {
                    int end = __min(patternLen, textLen - d);
                    const char* pend = pattern + end;

                    while (true) {
                        _uint64 x = *((_uint64*) p) ^ *((_uint64*) t);
                        if (x) {
                            unsigned long zeroes;
                            CountTrailingZeroes(x, zeroes);
                            zeroes >>= 3;
                            best = __min((int)(p - pattern) + (int)zeroes, end);
                            break;
                        }
                        p += 8;
                        if (p >= pend) {
                            best = end;
                            break;
                        }
                        t += 8;
                    }
                }
                L[e][MAX_K+d] = best;
                int score = best * matchBonus + e * mismatchPenalty;
                if ( score > hiScore )
                {
                    hiScore = score;
                    hiScoreD = d;
                    hiScoreE = e;
                }
                if (best == patternLen) 
                {
                    goto done;
                } 
            }
        }

done:
        int straightMismatches = 0;
        int netIndel = 0;

        if ( hiScore == -1 )
        {
            // no positive scoring extension was found, return 0s
            *pAlignLength = 0;
            *tAlignLength = 0;
            return k;
        }
        *pAlignLength = L[hiScoreE][MAX_K+hiScoreD];
        *tAlignLength = *pAlignLength + hiScoreD;
        
        return hiScoreE;

    }

private:
    int L[MAX_K+1][2 * MAX_K + 1];

    //
    // Table of d values for the inner loop in computeEditDistance.  This allows us to 
    // avoid the line d = (d > 0 ? -d : -d+1), which causes
    // a branch misprediction every time.
    //
    int dTable[2 * (MAX_K + 1) + 1];
    
    // Action we did to get to each position: 'D' = deletion, 'I' = insertion, 'X' = substitution.  This is needed to compute match probability.
    char A[MAX_K+1][2 * MAX_K + 1];

};
