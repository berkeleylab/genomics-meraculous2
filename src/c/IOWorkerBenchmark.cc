#include "IOWorker.hpp"
#include "IOWorkerSimple.hpp"

#include <boost/date_time.hpp>
#include <boost/thread/thread.hpp>
#include <boost/unordered_map.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <boost/thread/locks.hpp>
#include <boost/smart_ptr/detail/spinlock.hpp>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

void printTime2()
{
	time_t rawtime;
	struct tm *timeinfo;
	char buffer [200];
	time( &rawtime );
	timeinfo = localtime( &rawtime );
	strftime (buffer,200,"%c",timeinfo);	
	cerr << buffer << "\n";
} 

IOWorker worker; 

long int total = 0;

void scanFile( int threadId, int numWorkerThreads ) {
	char lineBuf[ 10000 ];
	 int numLinesRead = 0;
	 char *p = &lineBuf[0];
	while( worker.getLines( threadId, numWorkerThreads,&p, numLinesRead ) ) {
		total = ( total + (int)lineBuf[0] ) % 1000;
	}
	printf( "complexTotal threadId: %d checksum = %d\n", threadId, total );
}

IOWorkerSimple simpleWorker;

void fetchData() {
	worker.fetchData();
}
void scanFileSimple( const char *fileName, int threadId ) {
	char lineBuf[ 10000 ];
	 int numLinesRead = 0;
	 char *p = &lineBuf[0];

	ifstream fdIn( fileName );
    while( fdIn.getline( lineBuf, 1000 ) )
    {
        vector< string > cols;
	    boost::split( cols, lineBuf, boost::is_any_of( "\t" ), boost::token_compress_on ); 
	    
	    InputFileDescriptor fd;
	    fd.path = cols[ 0 ];
	    fd.format = cols[ 1 ];
	    fd.compression = cols[ 2 ];
	    fd.qualOffset = boost::lexical_cast< int >( cols[ 3 ] ); 
	    
	    ifstream in;
	    in.open( fileName, ifstream::in );
		if( in.fail())
		{
		  exit( -1 );
		}
	    char lineBuf[500];
		while( in.getline( &lineBuf[0], 500) )
		{			
			total = ( total + ( int)lineBuf[0] ) % 1000;
		}
	}
	printf( "simpleTotal checksum = %d\n", total );
}


int main( int argc, char *argv[] ) {
  int numWorkerThreads = boost::lexical_cast<int>( argv[2] ) - 1;
  const char *infoFile = argv[1];
  worker.init( infoFile,  numWorkerThreads );
  boost::thread threads[ numWorkerThreads + 1 ];

  // for each thread
  printTime2();

  boost::thread workerThread = boost::thread( fetchData);

  for( int i = 0; i < numWorkerThreads; i++ ) {
  	threads[ i ] = boost::thread( scanFile, i, numWorkerThreads );
  }

  for ( int i = 0; i < numWorkerThreads; i++ ) {
  	threads[ i ].join();
  }
  workerThread.join();
  cerr << "done with IOWorker" << endl;
  printTime2();

  cerr << "starting IOWorkerSimple" << endl;
  numWorkerThreads++;
  simpleWorker.init( argv[1], numWorkerThreads);
  for( int i = 0; i < numWorkerThreads; i++ ) {
  	threads[ i ] = boost::thread( scanFileSimple, infoFile, i );
  }
  for( int i = 0; i < numWorkerThreads; i++ ) {
  	threads[ i ].join();
  }
  cerr << "done with IOWorkerSimple" << endl;
  printTime2();
}
