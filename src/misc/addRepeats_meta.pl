#!/usr/bin/env perl

use warnings;
use Getopt::Std;

my %opts = ();
getopts('m:l:p:s:V', \%opts);

my $merSize = $opts{"m"};
my $linkDataFile = $opts{"l"};
my $srfFile = $opts{"s"};
my $pairThreshold = 2;
if (exists($opts{"p"})) {
    $pairThreshold = $opts{"p"};
}


my $verbose = 0;
if (exists($opts{"V"})) {
    $verbose = 1;
}


# Read linkData file and store library information

my %libInfo = ();
my @libs = ();
open (L,$linkDataFile) || die "Couldn't open $linkDataFile\n";
while (my $line = <L>) {
    chomp $line;
    my ($libs,$insertSizes,$stdDevs,$linkFile) = split(/\t/,$line);
    my @l = split(/\,/,$libs);
    my @i = split(/\,/,$insertSizes);
    my @s = split(/\,/,$stdDevs);

    my $nLibs = scalar(@l);
    for (my $l = 0; $l < $nLibs; $l++) {
	$libInfo{$l[$l]} = [$i[$l],$s[$l],$linkFile];
	push(@libs,$l[$l]);
    }
}
close L;

my @sortedLibs = sort {$libInfo{$a}->[0] <=> $libInfo{$b}->[0]} @libs;

my %linkFiles = ();
my @linkFileOrder = ();
foreach my $lib (@sortedLibs) {
    my $linkFile = $libInfo{$lib}->[2];
    unless (exists($linkFiles{$linkFile})) {
	push(@linkFileOrder,$linkFile);
	$linkFiles{$linkFile} = 1;
    }
}

my $nLinkFiles = scalar(@linkFileOrder);

my %onoObjectLengths = ();
my %onoObjectDepths = (); 
my %contigDepths = ();

my $maxAvgInsertSize = $libInfo{$sortedLibs[-1]}->[0];
print STDERR "Maximum insert size average across all input libs: $maxAvgInsertSize\n";
my $insStd = $libInfo{$sortedLibs[-1]}->[1];

my %scaffReport = ();  # scaffoldName -> srfFileLines 


print STDERR "Reading scaffold report file: $srfFile...\n";
open (S,$srfFile) || die "Couldn't open $srfFile\n";

my %scaffRealBases = ();
my %scaffDepthSum = ();
my %scaffContigs = ();

while (my $line = <S>) {
    chomp $line;
    my @cols = split(/\t/,$line);
    if (scalar(@cols) < 4 || scalar(@cols) > 6) {
	die "$srfFile  not in exlpected format:\n$line\n";
    }

    if (exists($scaffReport{$cols[0]})) {
	$scaffReport{$cols[0]} .= "$line\n";
    } else {
	$scaffReport{$cols[0]} = "$line\n";
    }

    if ($cols[1] =~ /^CONTIG/) {
	my ($scaffID,$contigID,$sStart,$sEnd) = @cols[0,2,3,4];
	my ($contigOri, $contigName) = $contigID =~ /^([\+\-])(.+)$/;
	push(@{$scaffContigs{$scaffID}}, $contigName);
	
	my $depth = $cols[5];
	my $cLen = $sEnd-$sStart+1;
	$contigDepths{$contigName} = $depth; 
	$scaffDepthSum{$scaffID} += $cLen*$depth;
	$scaffRealBases{$scaffID} += $cLen;

	if (exists($onoObjectLengths{$scaffID})) {
	    if ($sEnd > $onoObjectLengths{$scaffID}) {
		$onoObjectLengths{$scaffID} = $sEnd;
	    }
	} else {
	    $onoObjectLengths{$scaffID} = $sEnd;
	}
    }
}
close S;

while (my ($s,$d) = each(%scaffDepthSum)) {
    $onoObjectDepths{$s} = $d/$scaffRealBases{$s};
}



print STDERR "Done.\n";



# Build up contig linkage table using linkFiles

my %links = ();
for (my $lf = 0; $lf < $nLinkFiles; $lf++) {
    my $linkFile = $linkFileOrder[$lf];
    print STDERR "Reading link file $linkFile...\n";

    open (L,$linkFile) || die "Couldn't open $linkFile\n";
    while (my $line = <L>) {
	chomp $line;
	my @cols = split(/\t/,$line);
	my $linkType = $cols[0];
	my $ends = $cols[1]; # /^(.+)\.([35])<=>(.+)\.([35])$/;    
	my @linkData = @cols[2..$#cols];
	my $linkInfo = join(":",$lf,$linkType,@linkData);

	if (exists($links{$ends})) {
	    push(@{$links{$ends}},$linkInfo);
	}
	else {
	    $links{$ends} = [$linkInfo];
	}
    }
    close L;

    print STDERR "Done.\n";
}



# Build ties between each pair of object ends consolidating splint/span links

my %endTies = ();

foreach my $linkedEnds (keys(%links)) {

    my @linkData = @{$links{$linkedEnds}};
    my @splints = ();
    my @spans = ();

    my $maxLikelihoodSplint = undef;
    my $maxSplintCount = 0;
    my $minUncertaintySpan = undef;
    my $minSpanUncertainty = 100000;

    foreach my $link (@linkData) {
	my ($linkFile,$linkType,@linkInfo) = split(/:/,$link);
	if ($linkType eq "SPLINT") {
	    
	    my ($splintCounts,$gapEstimate) = @linkInfo;
	    my ($nUsedSplints,$nAnomalousSplints,$nAllSplints) = $splintCounts =~ /^(\d+)\|(\d+)\|(\d+)$/;
	    if ($nUsedSplints >= $pairThreshold) {
		my $splint = "$linkFile:$nUsedSplints:$gapEstimate";
		push(@splints,$splint);
		unless (defined($maxLikelihoodSplint)) {
		    $maxLikelihoodSplint = $splint;
		    $maxSplintCount = $nUsedSplints;
		}
		if ($nUsedSplints > $maxSplintCount) {
		    $maxLikelihoodSplint = $splint;
		    $maxSplintCount = $nUsedSplints;
		}
	    }

	} elsif ($linkType eq "SPAN") {

	    my ($spanCounts,$gapEstimate,$gapUncertainty) = @linkInfo;
	    my ($nAnomalousSpans,$nUsedSpans) = $spanCounts =~ /^(\d+)\|(\d+)$/;
	    if ($nUsedSpans >= $pairThreshold) {
		my $span = "$linkFile:$nUsedSpans:$gapEstimate:$gapUncertainty";
		push(@spans,$span);
		unless (defined($minUncertaintySpan)) {
		    $minUncertaintySpan = $span;
		    $minSpanUncertainty = $gapUncertainty;
		}
		if ($gapUncertainty < $minSpanUncertainty) {
		    $minUncertaintySpan = $span;
		    $minSpanUncertainty = $gapUncertainty;
		}
	    }

	} else {
	    die "Invalid linkType [$linkType] in link: $link\n";
	}
    }

    my $minimumGapSize = -($merSize-2);

    my $nSplintGap = undef;
    my $splintGapEstimate = undef;
    my $maxSplintError = 2;
    if (defined($maxLikelihoodSplint)) {
    	my ($l0,$n0,$g0) = split(/:/,$maxLikelihoodSplint);
    	$splintGapEstimate = ($g0 < $minimumGapSize) ? $minimumGapSize : $g0;
	$splintGapEstimate = $g0;
	$nSplintGap = $n0;
    }

    my $nSpanGap = undef;
    my $spanGapEstimate = undef;
    my $spanGapUncertainty = undef;
    my $maxSpanZ = 3;
    if (defined($minUncertaintySpan)) {
    	my ($l0,$n0,$g0,$u0) = split(/:/,$minUncertaintySpan);
#    	$spanGapEstimate = ($g0 < $minimumGapSize) ? $minimumGapSize : $g0;
	# don't trust negative spans ( normally we wouldn't have any, so this is just a safety net ).
	unless ($g0 < $minimumGapSize) {
	    $spanGapEstimate = $g0;
	    $spanGapUncertainty = ($u0 < 1) ? 1 : $u0;
	    $nSpanGap = $n0;
	}
    }

    my ($end1,$end2) = $linkedEnds =~ /(.+)\<\=\>(.+)/;

    my $gapEstimate = undef;
    my $gapUncertainty = undef;
    my $nGapLinks = undef;

    if (defined($splintGapEstimate)) {
	$gapEstimate = $splintGapEstimate;
	$gapUncertainty = 1;
	$nGapLinks = $nSplintGap;
    } elsif (defined($spanGapEstimate)) {
	my ($piece1) = $end1 =~ /(.+)\.[35]$/; 
	my ($piece2) = $end2 =~ /(.+)\.[35]$/; 
#	# only trust spans if at least one of the objects is large
	# only trust spans if BOTH objects are large
	# if ($onoObjectLengths{$piece1} < ($maxAvgInsertSize+$insStd)  &&
	#     $onoObjectLengths{$piece2} < ($maxAvgInsertSize+$insStd) ) {
	if ($onoObjectLengths{$piece1} < ($maxAvgInsertSize)  ||
	    $onoObjectLengths{$piece2} < ($maxAvgInsertSize) ) {

	    print STDERR "DEBUG: at least one of the objects linked by the span is too small;  Will not use link $linkedEnds \n" if ($verbose);
	    next;
	}
	$gapEstimate = $spanGapEstimate;
	$gapUncertainty = $spanGapUncertainty;
	$nGapLinks = $nSpanGap;
    } else {
	next;
    }

    if (defined($spanGapEstimate) && defined($splintGapEstimate)) {
    	my $gapZ = ($splintGapEstimate-$spanGapEstimate)/$spanGapUncertainty;
    	if (abs($gapZ) > $maxSpanZ) {
    	    warn "Warn: splint/span anomaly for $linkedEnds ($nSplintGap:$splintGapEstimate) not consistent with ($nSpanGap:$spanGapEstimate:$spanGapUncertainty)\n" if ($verbose);
    	}
    }


    
    if (exists($endTies{$end1})) {
	push (@{$endTies{$end1}},"$nGapLinks.$gapEstimate:$gapUncertainty.$end2");
    } else {
	$endTies{$end1} = ["$nGapLinks.$gapEstimate:$gapUncertainty.$end2"];
    }
    
    if (exists($endTies{$end2})) {
	push (@{$endTies{$end2}},"$nGapLinks.$gapEstimate:$gapUncertainty.$end1");
    } else {
	$endTies{$end2} = ["$nGapLinks.$gapEstimate:$gapUncertainty.$end1"];
    }
}

my @sortedByLen = sort {$onoObjectLengths{$b} <=> $onoObjectLengths{$a}} (keys(%onoObjectLengths));


print STDERR "Finding best ties...\n";
# only repeats are considered; bestTie is always a copy-object
my $end;
my $bestTie;
my %copyObjectsCnt = ();
my %bestTie = ();
my %bestTiedBy = ();

foreach my $piece (@sortedByLen) {
    print STDERR "\n === $piece ===\n";
    $end = "$piece.5";
    $bestTie = bestTie($end, \%copyObjectsCnt);
    $bestTie{$end} = $bestTie;
    
    if ($bestTie) {
	if (exists($bestTiedBy{$bestTie})) {
	    $bestTiedBy{$bestTie} .= ",$end";
	} else {
	    $bestTiedBy{$bestTie} = $end;
	}
	# since it's copy-object, make the reciprocal bestTie/bestTiedBy pair
	$bestTie{$bestTie} = $end;
	if (exists($bestTiedBy{$end})) {
	    $bestTiedBy{$end} .= ",$bestTie";   
	} else {
	    $bestTiedBy{$end} = $bestTie;
	}
    }
    my $notice = "BESTTIE: [$bestTie]\t$piece\t";

    $end = "$piece.3";
    $bestTie = bestTie($end, \%copyObjectsCnt);
    $bestTie{$end} = $bestTie;
    

    if ($bestTie) {
	if (exists($bestTiedBy{$bestTie})) {
	    $bestTiedBy{$bestTie} .= ",$end";
	} else {
	    $bestTiedBy{$bestTie} .= $end;
	}
	# since it's copy-object, make the reciprocal bestTie/bestTiedBy pair
	$bestTie{$bestTie} = $end;
	if (exists($bestTiedBy{$end})) {
	    $bestTiedBy{$end} .= ",$bestTie";   
	} else {
	    $bestTiedBy{$end} = $bestTie;
	}

    }
    

    $notice .= "[$bestTie]\n";

    print STDERR $notice;

}
print STDERR "Done finding best ties\n";



# Lock ends together with no competing ties

print STDERR "Locking ends with no competing ties..\n";
my %endLocks = ();

while (my ($end,$ties) = each (%bestTiedBy)) {
    if (exists($endLocks{$end})) {
	next;
    } 
    my ($piece) = $end =~ /^(.+)\.[35]$/;
    my @ties = split(/\,/,$ties);
    my $nTies = scalar(@ties);

    my $bestTie = "NONE";
    if (exists($bestTie{$end})) {
	$bestTie = $bestTie{$end};
    } else {
	print STDERR "DEBUG: \t $end  has no best ties.. skipping\n" if ($verbose);
	next;
    }
    print STDERR "\nDEBUG: $end ... ties: $ties\n" if ($verbose);
    if (($nTies == 1) && ($ties[0] eq $bestTie)) {
	my $tieInfo = getTieInfo($end,$bestTie);
	my ($nLinks,$gapEstimate,$gapUncertainty,$nextEnd) = $tieInfo =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;
	my $gap = sprintf("%.0f",$gapEstimate);
	$endLocks{$end} = "$bestTie><$gap:$gapUncertainty";
	print STDERR " DEBUG: locked end $end w. $endLocks{$end}\n "if ($verbose);
	$endLocks{$bestTie} = "$end><$gap:$gapUncertainty";
	print STDERR " DEBUG: locked bestTie $bestTie w. $endLocks{$bestTie}\n "if ($verbose);
    } else {
	print STDERR "DIVERGENCE:  $end, $ties\n ";
	$endLocks{$end} = "DIVERGENCE";

    }
}

foreach my $end (keys(%endLocks)) {
    print STDERR "LOCK: $end $endLocks{$end}\n";
}

# Traverse end locks to build scaffolds

my $scaffoldId = 1;
my %visitedObjects = ();


while (my ($object,$objectLen) = each (%onoObjectLengths)) {

    my $startObject = $object;
    my $startEnd = 5;

    if (exists($visitedObjects{$object})) {
	next;
    }


    # walk back up the lock end chain until we reach termination|divergence|convergence
    my $preState = "TERMINATION";
    my %loopCheck = ();
    while () {
	if (exists($loopCheck{$startObject})) {
	    last;
	}
	$loopCheck{$startObject} = 1;
	
	my $end = "$startObject.$startEnd";
	if (exists($endLocks{$end})) {
	    my $next = $endLocks{$end};
	    if (($next eq "CONVERGENCE") || ($next eq "DIVERGENCE")) {
		$preState = $next;
		last;
	    } else {
		($startObject,$startEnd) = $next =~ /(.+)\.([35])\>\</;
		$startEnd = ($startEnd == 3) ? 5 : 3;
		$preState = "EXTENSION";
	    }
	} else {
	    $preState = "TERMINATION";
	    last;
	}
    }
    
    %loopCheck = ();
    
    print STDERR "Scaffold$scaffoldId: $preState $startObject ($startEnd) ";

    my $inScaffold = 0;
    my $nextObject = $startObject;
    my $nextEnd = ($startEnd == 3) ? 5 : 3;
    my $nextGap = 0;
    my $nextGapUncertainty = 0;
    my $prevObject = $nextObject;
    my $prevEnd = $nextEnd;
    
    my $newScaffReport = "";
    my $scaffCoord = 1;
    my $scaffContigIndex = 1;

    while () {
	if (exists($loopCheck{$nextObject})) {
	    print STDERR "$nextObject LOOP\n";
	    last;
	}
	$loopCheck{$nextObject} = 1;	    
	$visitedObjects{$nextObject} = 1;
	my $nextObjectLen = $onoObjectLengths{$nextObject};
	
	my $objectOri = "+";
	if ($nextEnd == 5) {
	    $objectOri = "-";
	}
	
	if ($inScaffold) {
	    $newScaffReport .= "Scaffold$scaffoldId\tGAP$scaffContigIndex\t$nextGap\t$nextGapUncertainty\n";
	    $scaffCoord += $nextGap;
	    $scaffContigIndex++;
	}

	my $oldReport = $scaffReport{$nextObject};
	
	my @reportLines = split(/\n/,$oldReport);
	if ($objectOri eq "-") {
	    @reportLines = reverse(@reportLines);
	}
	foreach my $line (@reportLines) {
	    my @cols = split(/\t/,$line);
	    if ($cols[1] =~ /^CONTIG/) {
		my ($originalContig,$p0,$p1,$depth) = @cols[2,3,4,5];
		my $originalContigLength = $p1-$p0+1;
		my ($originalContigOri,$originalContigName) = $originalContig =~ /^([\+\-])(.+)$/;
				
		if ($objectOri eq $originalContigOri) {
		    $originalContigOri = "+";
		} else {
		    $originalContigOri = "-";
		}
		
		my $contigStartCoord = $scaffCoord;
		my $contigEndCoord = $scaffCoord + $originalContigLength - 1;
#		    my $depth = $contigDepths{$originalContigName};
		$newScaffReport .= "Scaffold$scaffoldId\tCONTIG$scaffContigIndex\t$originalContigOri$originalContigName\t$contigStartCoord\t$contigEndCoord\t$depth\n";
		$scaffCoord += $originalContigLength;
		
	    } else {   # a GAP
		
		my ($originalGapSize,$originalGapUncertainty) = @cols[2,3];
		$newScaffReport .= "Scaffold$scaffoldId\tGAP$scaffContigIndex\t$originalGapSize\t$originalGapUncertainty\n";
		$scaffCoord += $originalGapSize;
		$scaffContigIndex++;
	    }
	}

	$inScaffold = 1;

	
	my $end = "$nextObject.$nextEnd";
	if (exists($endLocks{$end})) {
	    my $next = $endLocks{$end};
	    if (($next eq "CONVERGENCE") || ($next eq "DIVERGENCE")) {
		print STDERR "$nextObject ($nextEnd) $next\n";
		last;
	    } else {
		print STDERR "$nextObject ($nextEnd) ";
		($prevObject,$prevEnd) = ($nextObject,$nextEnd);
		($nextObject,$nextEnd,$nextGap,$nextGapUncertainty) = $next =~ /(.+)\.([35])\>\<(.+)\:(.+)/;
		print STDERR "[$nextGap +/- $nextGapUncertainty] $nextObject ($nextEnd) ";
		$nextEnd = ($nextEnd == 3) ? 5 : 3;
	    }
	} else {
	    print STDERR "$nextObject ($nextEnd) TERMINATION\n";
	    last;
	}
    }
    
    print $newScaffReport;
    $scaffoldId++;
    
}



# ---------------
# | SUBROUTINES |
# ---------------

sub getTieInfo {
    my ($end1,$end2) = @_;
    
    my @ties = ();
    if (exists($endTies{$end1})) {
	@ties = @{$endTies{$end1}};
    } else {
	return 0;
    }

    my $nTies = scalar(@ties);

    for (my $i = 0; $i < $nTies; $i++) {
	my $tie_i = $ties[$i];
	my ($tiedEnd_i) = $tie_i =~ /\d+\.\-?\d+\:\d+\.(.+)/;

	if ($tiedEnd_i eq $end2) {
	    return $tie_i;
	}
    }

    return 0;
}

sub isSplinted {
    my ($end1,$end2) = @_;

    my $linkedEnds = ($end1 lt $end2) ? "$end1<=>$end2" : "$end2<=>$end1";
    if (exists($links{$linkedEnds})) {
	my @linkData = @{$links{$linkedEnds}};
	foreach my $link (@linkData) {
	    my ($linkFile,$linkType,@linkInfo) = split(/:/,$link);
	    if ($linkType eq "SPLINT") {
		return 1;
	    }
	}
    }

    return 0;
}



sub isLinkedTo {
    my ($end1,$end2) = @_;
    
    my @ties = ();
    if (exists($endTies{$end1})) {
	@ties = @{$endTies{$end1}};
    } else {
	return 0;
    }

    my $nTies = scalar(@ties);

    for (my $i = 0; $i < $nTies; $i++) {
	my $tie_i = $ties[$i];
	my ($nLinks_i,$gapEstimate_i,$gapUncertainty_i,$tiedEnd_i) = $tie_i =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;

	if ($tiedEnd_i eq $end2) {
	    return 1;
	}
    }

    return 0;
}


sub bestTie {
    my ($sourceEnd, $copyObjectsCntHR) = @_;
    print STDERR "DEBUG BT: End $sourceEnd:  testing tied ends \n" if ($verbose);
    my @ties = ();
    if (exists($endTies{$sourceEnd})) {
	@ties = @{$endTies{$sourceEnd}};
    } else {
	return 0;
    }

    my ($object) = $sourceEnd =~ /^(.+)\.[35]$/; 

    my $nTies = scalar(@ties);
    my @tiedEnds = ();
    my @gapEstimates = ();
    my @gapUncertainties = ();
    my @objectLens = ();

    print STDERR "DEBUG: BT: ties:\n" if ($verbose);
    for (my $i = 0; $i < $nTies; $i++) {
	my $tie_i = $ties[$i];
	my ($nLinks_i,$gapEstimate_i,$gapUncertainty_i,$tiedEnd_i) = $tie_i =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;
	my ($object_i) = $tiedEnd_i =~ /^(.+)\.[35]$/; 
	my $objectLen_i = $onoObjectLengths{$object_i};

	print STDERR "DEBUG: BT: \t$tie_i length: ". $objectLen_i ." depth: " . $onoObjectDepths{$object_i} . "\n" if ($verbose);

	push(@tiedEnds,$tiedEnd_i);
	push(@gapEstimates,$gapEstimate_i);
	push(@gapUncertainties,$gapUncertainty_i);
	push(@objectLens,$objectLen_i);
    }

    my $nGoodTies = scalar(@tiedEnds);
    unless($nGoodTies > 0) {
	print STDERR "BT: NoGoodTies\n";
	return 0;
    }

    my @sortedTieIndex = sort {($gapEstimates[$a] <=> $gapEstimates[$b]) || ($objectLens[$a] <=> $objectLens[$b])} (0..$#tiedEnds);

    my $collidingAR = has_tie_collision($object, $sourceEnd, \@tiedEnds, \@gapEstimates, \@gapUncertainties, \@objectLens, \@sortedTieIndex);
    if (defined $collidingAR) {
	    return 0;
    }

    # return a copy of the closest object if it fits the repeat profile

    my $closestEnd  = $tiedEnds[$sortedTieIndex[0]];
    print STDERR "BT: \t\t\t $closestEnd .. is closest \n" if ($verbose);

    my ($closestObject,$endSuffix) = $closestEnd =~ /^(.+)\.([35])$/;    

    if ( is_tied_to_repeat($object, $sourceEnd, $closestObject, $closestEnd)) {

	$bestTie = create_copy_object($sourceEnd, $closestObject, $closestEnd, \%endTies, \%scaffReport, $copyObjectsCntHR);
	if (! $bestTie) {
	    print STDERR "BT: Could not create a copy object from $closestObject\n"; 
	    return 0;
	}
	return $bestTie;
    }
    else { return 0 }
}

 
sub is_tied_to_repeat {
    my ($myObject, $myEnd, $testObject, $testTiedEnd) = @_;

    my $myObjDepth = $onoObjectDepths{$myObject}; 
    my $testObjectDepth = $onoObjectDepths{$testObject};
    my $testObjectLen = $onoObjectLengths{$testObject};
    my $maxRepeatLen = 10000;
    my $maxDepthRatio = 500;  
#    my $minDepthRatio = 2;  # require at least a 2x dropoff to consider testObject a repeat
    my $minDepthRatio = 1;

    my @ties = @{$endTies{$testTiedEnd}};
    my $nTies = scalar(@ties);
    my @tiedEnds = ();
    my @gapEstimates = ();
    my @gapUncertainties = ();
    my @objectLens = ();

    print STDERR "\t\tTesting if $testObject is a repeat.. \n" if ($verbose);  
    print STDERR "\t\tReverse ties:\n" if ($verbose);
    for (my $i = 0; $i < $nTies; $i++) {
	my $tie_i = $ties[$i];
	print STDERR "\t\t\t$tie_i " if ($verbose);
	my ($nLinks_i,$gapEstimate_i,$gapUncertainty_i,$tiedEnd_i) = $tie_i =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;
	my ($object_i) = $tiedEnd_i =~ /^(.+)\.[35]$/; 
	my $objectLen_i = $onoObjectLengths{$object_i};

	print STDERR "depth: $onoObjectDepths{$object_i}\n" if $verbose;
	push(@tiedEnds,$tiedEnd_i);
	push(@gapEstimates,$gapEstimate_i);
	push(@gapUncertainties,$gapUncertainty_i);
	push(@objectLens,$objectLen_i);
    }
    my @sortedTieIndex = sort {($gapEstimates[$a] <=> $gapEstimates[$b]) || ($objectLens[$a] <=> $objectLens[$b])} (0..$#tiedEnds);

    my $collidingAR = has_tie_collision($testObject, $testTiedEnd, \@tiedEnds, \@gapEstimates, \@gapUncertainties, \@objectLens, \@sortedTieIndex);
    unless (defined $collidingAR) {
	return 0;
    }

    if ($testObjectDepth/$myObjDepth < $minDepthRatio) {
	print STDERR "\t\tOmitted [MIN_DEPTH_RATIO]: ($testObjectDepth : $myObjDepth < $minDepthRatio)\n" if ($verbose);
	return 0;
    }

    if ($testObjectDepth/$myObjDepth > $maxDepthRatio) {
	print STDERR "\t\tOmitted [MAX_DEPTH_RATIO]: ($testObjectDepth : $myObjDepth > $maxDepthRatio)\n" if ($verbose);
	return 0;
    }



    ## Colliding objects can't have have higher combined depth than the repeat candidate.
    # my $sumCollidingDepths = 0;
    # foreach my $collidingObj (@$collidingAR) {
    # 	$sumCollidingDepths += $onoObjectDepths{$collidingObj};
    # }
    # if ($sumCollidingDepths > ($testObjectDepth * 1.5)) {
    # 	print STDERR "\t\tOmitted [DEPTH_VIOLATION]: ($sumCollidingDepths > 1.5 x $testObjectDepth)\n" if ($verbose);
    # 	return 0;		    
    # }
    if ($testObjectLen > $maxRepeatLen) {
	print STDERR "\t\tOmitted [SIZE]: ($testObjectLen)\n" if ($verbose);
	return 0;
    }
    else {
	print STDERR "\t\tREPEAT_BOUNDARY: $myObject ($onoObjectLengths{$myObject}, $myObjDepth) <=> $testObject ($testObjectLen, $testObjectDepth)\n" if ($verbose);
	    return 1;
    }
}

sub has_tie_collision {
    my ($testObject, $end, $tiedEnds, $gapEstimates, $gapUncertainties, $objectLens, $sortedTieIndex) = @_;

    my $nTies = scalar(@{$tiedEnds});
    my @collidingObjects = ();
    my $maxStrain = 1;

    for (my $i=0;$i<$nTies-1;$i++) {   
	next if ($nTies < 2);
	my $ti = $$sortedTieIndex[$i];	
	my $start_i = $$gapEstimates[$ti];
	my $end_i = $start_i+$$objectLens[$ti]-1;
	my $uncertainty_i = $$gapUncertainties[$ti];

	my $tj = $$sortedTieIndex[$i+1];
	my $start_j = $$gapEstimates[$tj];
	my $end_j = $start_j+$$objectLens[$tj]-1;
	my $uncertainty_j = $$gapUncertainties[$tj];



	my $overlap = $end_i - $start_j + 1;
	print STDERR "DEBUG: checking ties $$tiedEnds[$ti] & $$tiedEnds[$tj] for collision  (overlap by $overlap) ...  \n" if $verbose;
	if ($overlap > $merSize-2) {
	    my $excessOverlap = $overlap-($merSize-2);
	    my $maxUncertaintly = ($uncertainty_i >= $uncertainty_j) ? $uncertainty_i : $uncertainty_j;
	    my $strain = $excessOverlap / $maxUncertaintly;
	    if ($strain > $maxStrain) {   
		# tie confilct - mark appropriately
		my $tiedEnd_i = $$tiedEnds[$ti];
		my ($object_i,$objectEnd_i) = $tiedEnd_i =~ /^(.+)\.([35])$/;
		my $testEnd_i = $object_i;
		$testEnd_i .= ($objectEnd_i eq "3") ? ".5" : ".3";
		my $tiedEnd_j = $$tiedEnds[$tj];
		my ($object_j,$objectEnd_j) = $tiedEnd_j =~ /^(.+)\.([35])$/;
		my $testEnd_j = $tiedEnd_j;

		my $isSplinted = isSplinted($testEnd_i,$testEnd_j);

		if ($isSplinted) {
		    print STDERR "\t\tTie collision averted by SPLINT: $end -> $$tiedEnds[$ti] ? $$tiedEnds[$tj] \n" if $verbose;
		} else {
		    print STDERR "\t\tTIE_COLLISION: $end -> $$tiedEnds[$ti] ? $$tiedEnds[$tj] \n";
		    push(@collidingObjects, $object_i,$object_j)
		}
	    }
#	    else {
#		last; # we only care about the closest set of collisions
#	    }
	}
	else {
	    last; # we only care about the closest set of collisions
	}
    }
    unless (@collidingObjects) { return undef }

    my @collidingObjectsU;
    my %seen;
    foreach (@collidingObjects) {
	if (! $seen{$_}++) {
	    push @collidingObjectsU, $_;
	}
    }
    return \@collidingObjectsU;
}


sub create_copy_object {
    my ($anchorEnd, $object, $end, $endTiesHR, $scaffReportHR, $copyObjectsCntHR) = @_;

    my $copyEnd;
    my $tieToReassign;
    my $copyCnt;

    if (exists $$copyObjectsCntHR{$object}) {
	$copyCnt = $$copyObjectsCntHR{$object} + 1;
    }
    else {
	$copyCnt =1;
    }
    my $copyObject = "${object}.cp".$copyCnt;
    $$copyObjectsCntHR{$object} = $copyCnt;

    ($copyEnd = $end) =~ s/^$object/$copyObject/;


    foreach my $tie (@{$endTiesHR->{$end}}) {
	    $tie =~ /\d+\.\-?\d+\:\d+\.(.+)$/;
	    my $tieEnd = $1;
	    if ($tieEnd eq $anchorEnd ) {
		$tieToReassign = $tie;
	    }
    }
    unless (defined $tieToReassign) {
	print STDERR "DEBUG (create_copy_object): \t\t\t no ties to reassing at end $end.\n" if ($verbose);
	return 0;
    }

    # assign to the copy-object
    push (@{$endTiesHR->{$copyEnd}}, $tieToReassign);
    print STDERR "DEBUG (create_copy_object): \t\t\t tie $tieToReassign reassigned to $copyEnd..\n"  if ($verbose);	    
    
    # remove this tie from this end's ties hash
    my @tiesUpdated = grep  ! /$tieToReassign/, @{$endTiesHR->{$end}};
    $endTiesHR->{$end} = \@tiesUpdated;
    
    # Correct the target end of the new tie so that it points back to the copyEnd
    foreach (@{$endTiesHR->{$anchorEnd}}) {
	    s/$object\./$copyObject\./;
    }    

    # add a new record to the original scaff report to reflect the copy
    my $parentScafRecord = $scaffReportHR->{$object};
    my @scaffoldLines = split(/\n/,$parentScafRecord);
    my @newScaffoldLines;
    foreach (@scaffoldLines) {
	my @cols = split(/\t/,$_);
	if ( $cols[1] =~ /^CONTIG/) {
	    $cols[0] .= "-CP$copyCnt";   #old scaff id
	    $cols[2] .= ".cp$copyCnt";   #contig name
	}
	push(@newScaffoldLines, (join("\t",@cols)) );
    }   
    $scaffReportHR->{$copyObject} = (join("\n",@newScaffoldLines));

	
    $onoObjectLengthsHR->{$copyObject} = $onoObjectLengthsHR->{$object};
    
    
    print STDERR "New copy-object created: $copyObject\n";
        
    return $copyEnd;

}

