#!/usr/bin/env perl
#
# oNo_meta.pl by Jarrod Chapman & Eugene Goltsman <egoltsman@lbl.gov>
#

use strict;
use warnings;
use Getopt::Std;
#use Data::Dumper;

my %opts = ();
my $validLine = getopts('m:l:p:s:c:o:B:R:O:VL:', \%opts);
my @required = ("m","l","R");
my $nRequired = 0;
map {$nRequired += exists($opts{$_})} @required;
$validLine &= ($nRequired == @required);

$validLine &= (exists($opts{"s"}) || exists($opts{"c"}));

if ($validLine != 1) {
    print "Usage: ./oNo_meta.pl <-m merSize> <-l linkDataFile> <-R rRNAHitsList> (<-s scaffoldReportFile> || <-c contigReportFile>)  <<-p pairThreshold>> <<-B blessedLinkFile>> <<-O IDoffset>> <<-o srfOutputFileName>> <<-V(erbose)>> <<-L(ogfile) logPath>>\n";
    exit;
}

our $LOG = *STDERR;

my $date = `date`;
chomp $date;
my $outbuffer = "$date\noNo_bestPath";
while (my ($opt,$val) = each(%opts)) {
    $outbuffer .= " -$opt $val";
}
$outbuffer .= "\n";
if (defined $opts{"L"}) {
    # append to a log file
    $LOG = undef;
    open($LOG, ">>", $opts{"L"}) || die("Could not append to log file " . $opts{"L"});
}

my $merSize = $opts{"m"};
my $linkDataFile = $opts{"l"};
my $rRNAHitsFile = $opts{"R"};

my $srfFile = undef;
my $crfFile = undef;
if (exists($opts{"s"})) {
    $srfFile = $opts{"s"};
} elsif (exists($opts{"c"})) {
    $crfFile = $opts{"c"};
}

my $pairThreshold = 2;
if (exists($opts{"p"})) {
    $pairThreshold = $opts{"p"};
}

my $srfOutFile = $linkDataFile.".srf";
if (exists($opts{"o"})) {
    $srfOutFile = $opts{"o"};
}

my $blessedLinkFile = undef;
my %blessedLinks = ();
if (exists($opts{"B"})) {
    $blessedLinkFile = $opts{"B"};
    open (B,$blessedLinkFile) || die "Couldn't open $blessedLinkFile\n";
    while (my $line = <B>) {
	chomp $line;
	my ($s1,$s2) = split(/\s+/,$line);
	my $bless = ($s1 lt $s2) ? "$s1.$s2" : "$s2.$s1";
	$blessedLinks{$bless} = 1;
    }
    close B;
}

my $srfOffset =0;
if (exists($opts{"O"})) {
    $srfOffset = $opts{"O"};
}

my $verbose = 0;
if (exists($opts{"V"})) {
    $verbose = 1;
}

my $maxStrain = 2;
#my $maxDepthDropoff = 2;  




# Read linkData file and store library information

my %libInfo = ();
my @libs = ();
open (L,$linkDataFile) || die "Couldn't open $linkDataFile\n";
while (my $line = <L>) {
    chomp $line;
    my ($libs,$insertSizes,$stdDevs,$linkFile) = split(/\t/,$line);
    my @l = split(/\,/,$libs);
    my @i = split(/\,/,$insertSizes);
    my @s = split(/\,/,$stdDevs);

    my $nLibs = scalar(@l);
    for (my $l = 0; $l < $nLibs; $l++) {
	$libInfo{$l[$l]} = [$i[$l],$s[$l],$linkFile];
	push(@libs,$l[$l]);
    }
}
close L;

my @sortedLibs = sort {$libInfo{$a}->[0] <=> $libInfo{$b}->[0]} @libs;

my %linkFiles = ();
my @linkFileOrder = ();
foreach my $lib (@sortedLibs) {
    my $linkFile = $libInfo{$lib}->[2];
    unless (exists($linkFiles{$linkFile})) {
	push(@linkFileOrder,$linkFile);
	$linkFiles{$linkFile} = 1;
    }
}

my %scaffReport = ();  # scaffoldName -> srfFileLines 
my %onoObjectLengths = ();
my %onoObjectDepths = (); 
my %scaffContigDepths = ();

my $maxAvgInsertSize = $libInfo{$sortedLibs[-1]}->[0];
my $maxAvgInsertSdev = $libInfo{$sortedLibs[-1]}->[1];
print $LOG "Maximum average insert size: $maxAvgInsertSize +/- $maxAvgInsertSdev\n";
my $SUSPENDABLE = $maxAvgInsertSize;
#my $LONG = 500;
my $LONG = $SUSPENDABLE;



# load contig/scaffold info
if (defined($crfFile)) {
    parse_crfFile($crfFile, \%onoObjectLengths, \%onoObjectDepths);
}
elsif (defined($srfFile)) { 
    parse_srfFile($srfFile, \%scaffReport, \%onoObjectLengths, \%onoObjectDepths);
}


#load rRNA db hits
my %rRNAdb = ();
parse_rRNAHits($rRNAHitsFile, \%rRNAdb);


# Build up contig linkage table using linkFiles
my %links = ();
parse_linkFiles(\@linkFileOrder, \%links);


# Build ties between each pair of object ends consolidating splint/span links
my %endTies = ();
build_ties(\%links, \%endTies);


my @sortedByLen = sort {($onoObjectLengths{$b} <=> $onoObjectLengths{$a}) || ($onoObjectDepths{$b} <=> $onoObjectDepths{$a}) || ($a cmp $b) } (keys(%onoObjectLengths));

my %endLabels = ();
$endLabels{"UNMARKED"} = 0;
$endLabels{"SELF_TIE"} = 1;
$endLabels{"DEPTH_DROP"} = 2;
$endLabels{"TIE_COLLISION"} = 3;
$endLabels{"NO_TIES"} = 4;
$endLabels{"RRNA"} = 6;

my %endMarks = ();
my %endMarkCounts =();

print $LOG "Marking ends... \n";

foreach my $piece (@sortedByLen) {
    my $end = "$piece.5";
    my $endMark = markEnd($end);
    $endMarks{$end} = $endLabels{$endMark};
    
    $end = "$piece.3";
    $endMark = markEnd($end);
    $endMarks{$end} = $endLabels{$endMark};
}

print $LOG "Done marking ends.\n";


# find a "best tie" for every end.
my %bestTie = ();
my %bestTiedBy = ();
my %suspended = ();
my %copyObjectsCnt = ();
find_bestTies(\@sortedByLen, \%endTies, \%bestTie, \%bestTiedBy, \%suspended, \%copyObjectsCnt);

# Lock ends together with no competing ties
my %endLocks = ();
lock_ends(\%endLocks);


# Traverse end locks to build scaffolds
my $nScaffolds = 0;
$nScaffolds = build_scaffolds(\%endLocks);



print $LOG "\n\nSummary stats:\n\n";

my $totBT = keys %bestTie;
my $totSusp = keys %suspended;
my $nCopied = 0;
my $nCopiesMade =0; 
for (keys %copyObjectsCnt) {
    $nCopied++;
    $nCopiesMade += $copyObjectsCnt{$_};
}


for (keys %endMarkCounts) {
    print $LOG "End marked $_ : $endMarkCounts{$_}\n";
}
print $LOG "Best ties: $totBT\n";
print $LOG "Original objects copied: $nCopied\n";
print $LOG "Copy-objects made: $nCopiesMade\n";
print $LOG "Scaffolds: $nScaffolds\n";


print $nScaffolds;  

#END



# ---------------
# | SUBROUTINES |
# ---------------


sub parse_crfFile {
    my ($crfFile, $onoObjectLengthsHR, $onoObjectDepthsHR) = @_;
    
    print $LOG "Parsing $crfFile...\n";
    open (C,$crfFile) || die "Couldn't open $crfFile\n";
    while (my $line = <C>) {
	chomp $line;
	my @cols = split(/\s+/,$line);
	if ($#cols != 2) {
	    die "$crfFile  not in exlpected format\n";
	}
	my $contig= $cols[0];
	my $length = $cols[1];
	
	$$onoObjectLengthsHR{$contig} = $length;

	my $depth = $cols[2];
	$$onoObjectDepthsHR{$contig} = $depth;
    }
    close C;
    print $LOG "Done.\n";
}

sub parse_srfFile {
    my ($srfFile, $scaffReportHR, $onoObjectLengthsHR, $onoObjectDepthsHR) = @_;

    print $LOG "Parsing scaffold report file: $srfFile...\n";
    open (S,$srfFile) || die "Couldn't open $srfFile\n";

    my %scaffRealBases = ();
    my %scaffDepthSum = ();
    my %scaffContigs = ();

    while (my $line = <S>) {
        chomp $line;
        my @cols = split(/\t/,$line);
	if (scalar(@cols) < 4 || scalar(@cols) > 6) {
	    die "$srfFile  not in exlpected format:\n$line\n";
	}

	if (exists($$scaffReportHR{$cols[0]})) {
	    $$scaffReportHR{$cols[0]} .= "$line\n";
	} else {
	    $$scaffReportHR{$cols[0]} = "$line\n";
	}

        if ($cols[1] =~ /^CONTIG/) {
            my ($scaffID,$contigID,$sStart,$sEnd) = @cols[0,2,3,4];
	    my ($contigOri, $contigName) = $contigID =~ /^([\+\-])(.+)$/;
	    push(@{$scaffContigs{$scaffID}}, $contigName);
	    
	    my $depth = $cols[5];
	    my $cLen = $sEnd-$sStart+1;
	    $scaffContigDepths{$contigName} = $depth; 
	    $scaffDepthSum{$scaffID} += $cLen*$depth;
	    $scaffRealBases{$scaffID} += $cLen;

            if (exists($$onoObjectLengthsHR{$scaffID})) {
                if ($sEnd > $$onoObjectLengthsHR{$scaffID}) {
                    $$onoObjectLengthsHR{$scaffID} = $sEnd;
                }
            } else {
                $$onoObjectLengthsHR{$scaffID} = $sEnd;
            }
	}
    }
    close S;

    while (my ($s,$d) = each(%scaffDepthSum)) {
	$$onoObjectDepthsHR{$s} = $d/$scaffRealBases{$s};
    }
    print $LOG "Done.\n";
}

sub parse_rRNAHits {
    my ($rRNAHitsFile, $rRNAdbHR, $onoObjectLengthsHR) = @_;

    print $LOG "Parsing rRNA hits ...\n";
    open (RRNA,$rRNAHitsFile) || die "Couldn't open $rRNAHitsFile\n";
    while (<RRNA>) {
	chomp;
	my ($contig,$start,$end,$strand,$taxonomy) = split(/\t/, $_);
	unless (grep { defined($_) } ($contig,$start,$end)) { die "rRNA reference file $rRNAHitsFile is not in expected format!\n" };
	
	
	if (defined $strand && $strand eq "-") {
	    my $cLen = $$onoObjectLengthsHR{$contig};
	    $start = $cLen - $start + 1;
	    $end = $cLen - $end + 1;
	}

	if ($taxonomy) {
	    $taxonomy =~ s/;$//;  #trim off final semicolon
	    # trim off extra strain identifiers from species names
	    if ($taxonomy =~ /\s/) {
		my @tmp = split(';',$taxonomy);
		my $species = $tmp[-1];
		$species =~ s/(\w+\s\w+).*$/$1/;
		$tmp[-1] = $species;
		$taxonomy = join(';',@tmp);
	    }
	}
	
	if (exists $$rRNAdbHR{$contig}) {
	    # A contigs may have multiple hits (lsu,ssu,etc);
	    # Update to the longest taxonomy as long as one is contained in the other, i.e. we don't have two different assignments
	    if (defined $taxonomy &&  length $taxonomy > length $$rRNAdbHR{$contig}->[2]) {
		next unless ($taxonomy =~ /^$$rRNAdbHR{$contig}->[2]/);
		$$rRNAdbHR{$contig}->[2] = $taxonomy;
	    }
	    elsif (defined $taxonomy && length $taxonomy <= length $$rRNAdbHR{$contig}->[2]) {
		next unless ($$rRNAdbHR{$contig}->[2] =~ /^$taxonomy/);
	    }
	    
	    # update start/end positions to maximal range
	    if ($start < $$rRNAdbHR{$contig}->[0]) {
		$$rRNAdbHR{$contig}->[0] = $start;
	    }
	    if ($end > $$rRNAdbHR{$contig}->[1]) {
		$$rRNAdbHR{$contig}->[1] = $end;
	    }
	}
	else {
	    $$rRNAdbHR{$contig} = [$start,$end,$taxonomy];
	}
    }
    close RRNA;
    print $LOG "Done.\n";

}

sub parse_linkFiles {

    my ($linkFileOrderAR, $linksHR) = @_;
    my $nLinkFiles = scalar @$linkFileOrderAR;

    for (my $lf = 0; $lf < $nLinkFiles ; $lf++) {
	my $linkFile = $$linkFileOrderAR[$lf];
	print $LOG "Reading link file $linkFile...\n";
	
	open (L,$linkFile) || die "Couldn't open $linkFile\n";
	while (my $line = <L>) {
	    chomp $line;
	    my @cols = split(/\t/,$line);
	    my $linkType = $cols[0];
	    my $ends = $cols[1]; # /^(.+)\.([35])<=>(.+)\.([35])$/;    
	    my @linkData = @cols[2..$#cols];
	    my $linkInfo = join(":",$lf,$linkType,@linkData);
	    
	    if (exists($$linksHR{$ends})) {
		push(@{$$linksHR{$ends}},$linkInfo);
	    } else {
		$$linksHR{$ends} = [$linkInfo];
	    }
	}
	close L;
	
	print $LOG "Done.\n";
    }

}


sub build_ties {
    my ($linksHR, $endTiesHR) = @_;

    foreach my $linkedEnds (keys(%$linksHR)) {
	my @linkData = @{$linksHR->{$linkedEnds}};
	my @splints = ();
	my @spans = ();

	my $maxLikelihoodSplint = undef;
	my $maxSplintCount = 0;
	my $minUncertaintySpan = undef;
	my $minSpanUncertainty = 100000;

	my $blessedLink = 0;
	if (defined($blessedLinkFile)) {
	    my ($s1,$s2) = $linkedEnds =~ /^(.+)\.[35]<=>(.+).[35]$/;
	    my $bless = ($s1 lt $s2) ? "$s1.$s2" : "$s2.$s1";
	    if (exists($blessedLinks{$bless})) {
		$blessedLink = 1;
		print $LOG "BLESSED: $linkedEnds\n";
	    }
	}

	foreach my $link (@linkData) {
	    my ($linkFile,$linkType,@linkInfo) = split(/:/,$link);
	    if ($linkType eq "SPLINT") {
		
		my ($splintCounts,$gapEstimate) = @linkInfo;
		my ($nUsedSplints,$nAnomalousSplints,$nAllSplints) = $splintCounts =~ /^(\d+)\|(\d+)\|(\d+)$/;
		if ( ($nUsedSplints >= $pairThreshold) || $blessedLink) {
		    my $splint = "$linkFile:$nUsedSplints:$gapEstimate";
		    push(@splints,$splint);
		    unless (defined($maxLikelihoodSplint)) {
			$maxLikelihoodSplint = $splint;
			$maxSplintCount = $nUsedSplints;
		    }
		    if ($nUsedSplints > $maxSplintCount) {
			$maxLikelihoodSplint = $splint;
			$maxSplintCount = $nUsedSplints;
		    }
		}

	    } elsif ($linkType eq "SPAN") {

		my ($spanCounts,$gapEstimate,$gapUncertainty) = @linkInfo;
		my ($nAnomalousSpans,$nUsedSpans) = $spanCounts =~ /^(\d+)\|(\d+)$/;
		if (($nUsedSpans >= $pairThreshold) || $blessedLink) {
		    my $span = "$linkFile:$nUsedSpans:$gapEstimate:$gapUncertainty";
		    push(@spans,$span);
		    unless (defined($minUncertaintySpan)) {
			$minUncertaintySpan = $span;
			$minSpanUncertainty = $gapUncertainty;
		    }
		    if ($gapUncertainty < $minSpanUncertainty) {
			$minUncertaintySpan = $span;
			$minSpanUncertainty = $gapUncertainty;
		    }
		}

	    } else {
		die "Invalid linkType [$linkType] in link: $link\n";
	    }
	}

	my $minimumGapSize = -($merSize-2);

	my $nSplintGap = undef;
	my $splintGapEstimate = undef;
	my $maxSplintError = 2;
	my $splintAnomaly = 0;
	if (defined($maxLikelihoodSplint)) {
	    my ($l0,$n0,$g0) = split(/:/,$maxLikelihoodSplint);
	    $splintGapEstimate = ($g0 < $minimumGapSize) ? $minimumGapSize : $g0;
	    $nSplintGap = $n0;
	    # foreach my $splint (@splints) {
	    #     my ($l,$n,$g) = split(/:/,$splint);
	    #     my $splintGapDiff = abs($g-$splintGapEstimate);
	    #     if ($splintGapDiff > $maxSplintError) {
	    # 	$splintAnomaly = 1;
	    # 	warn "Warn: splint anomaly for $linkedEnds ($l,$n,$g) not consistent with ($l0,$n0,$g0) [$splintGapEstimate]\n" if ($verbose);
	    #     }
	    # }
	}

	my $nSpanGap = undef;
	my $spanGapEstimate = undef;
	my $spanGapUncertainty = undef;
	my $maxSpanZ = 3;
	my $spanAnomaly = 0;
	if (defined($minUncertaintySpan)) {
	    my ($l0,$n0,$g0,$u0) = split(/:/,$minUncertaintySpan);
	    $spanGapEstimate = ($g0 < $minimumGapSize) ? $minimumGapSize : $g0;
	    $spanGapUncertainty = ($u0 < 1) ? 1 : $u0;
	    $nSpanGap = $n0;
	    # foreach my $span (@spans) {
	    #     my ($l,$n,$g,$u) = split(/:/,$span);
	    #     my $spanGapZ = abs($g-$spanGapEstimate)/($u+1);
	    #     if ($spanGapZ > $maxSpanZ) {
	    # 	$spanAnomaly = 1;
	    # 	warn "Warn: span anomaly for $linkedEnds ($l,$n,$g,$u) not consistent with ($l0,$n0,$g0,$u0) [$spanGapEstimate:$spanGapUncertainty]\n" if ($verbose);
	    #     }
	    # }
	}

	my ($end1,$end2) = $linkedEnds =~ /(.+)\<\=\>(.+)/;

	my $gapEstimate = undef;
	my $gapUncertainty = undef;
	my $nGapLinks = undef;

	if (defined($splintGapEstimate)) {
	    $gapEstimate = $splintGapEstimate;
	    $gapUncertainty = 1;
	    $nGapLinks = $nSplintGap;
	} elsif (defined($spanGapEstimate)) {
	    my ($piece1) = $end1 =~ /(.+)\.[35]$/;
	    my ($piece2) = $end2 =~ /(.+)\.[35]$/;

	    # limit SPAN usage to only large objects  (pending rework of bmaToLinks)
#	    if ($onoObjectLengths{$piece1} <= $SUSPENDABLE || $onoObjectLengths{$piece2} <= $SUSPENDABLE) {
	    if ($onoObjectLengths{$piece1} + $onoObjectLengths{$piece2} <= $SUSPENDABLE) {
		next;
	    }
	    $gapEstimate = $spanGapEstimate;
	    $gapUncertainty = $spanGapUncertainty;
	    $nGapLinks = $nSpanGap;
	} else {
	    next;
	}

#    if (defined($spanGapEstimate) && defined($splintGapEstimate)) {
#	my $gapZ = ($splintGapEstimate-$spanGapEstimate)/$spanGapUncertainty;
#	if (abs($gapZ) > $maxSpanZ) {
#	    warn "Warn: splint/span anomaly for $linkedEnds ($nSplintGap:$splintGapEstimate) not consistent with ($nSpanGap:$spanGapEstimate:$spanGapUncertainty)\n" if ($verbose);
#	}
#    }

	if (exists($$endTiesHR{$end1})) {
	    push (@{$$endTiesHR{$end1}},"$nGapLinks.$gapEstimate:$gapUncertainty.$end2");
	} else {
	    $endTiesHR->{$end1} = ["$nGapLinks.$gapEstimate:$gapUncertainty.$end2"];
	}
	
	if (exists($endTiesHR->{$end2})) {
	    push (@{$endTiesHR->{$end2}},"$nGapLinks.$gapEstimate:$gapUncertainty.$end1");
	} else {
	    $endTiesHR->{$end2} = ["$nGapLinks.$gapEstimate:$gapUncertainty.$end1"];
	}
    }
    
}

sub mutualUniqueBest {
    my ($end1,$end2) = @_;

    my ($piece1) = $end1 =~ /^(.+)\.[35]$/;
    my ($piece2) = $end2 =~ /^(.+)\.[35]$/;
    if (exists($suspended{$piece1}) || exists($suspended{$piece2})) {
	return 0;
    }

    unless (exists($bestTie{$end1}) && exists($bestTie{$end2})) {
	return 0;
    }

    my $bestTie1 = $bestTie{$end1};
    my $bestTie2 = $bestTie{$end2};

    unless (($bestTie1 eq $end2) && ($bestTie2 eq $end1)) {
	return 0;
    }

    unless (exists($bestTiedBy{$end1}) && exists($bestTiedBy{$end2})) {
	return 0;
    }

    my @ties1 = split(/\,/,$bestTiedBy{$end1});
    my $nTies1 = scalar(@ties1);
    my @ties2 = split(/\,/,$bestTiedBy{$end2});
    my $nTies2 = scalar(@ties2);

    unless ( ($nTies1 == 1) && ($ties1[0] eq $end2) && ($nTies2 == 1) && ($ties2[0] eq $end1)) 
#    unless (grep(/^$end2$/, @ties1) && grep(/^$end1$/, @ties2)) 
    {
	return 0;
    }

    return 1;
}


sub find_bestTies {

    # Super-function to evaluate all size-sorted objects' ends and secure a "best tie"

    # Unmarked ends are subject to conserative bestTie() selection while those with the RRNA mark  ($endMarks{$end} == 6) 
    # initiate an aggressive path traversal which finds a path through the repeat structure to a long rRNA "peer" object. 

    # Pieces can be inserted between earlier established bestTies (i.e. suspensions) and/or copied in the process. 

    my ($sortedByLenAR, $endTiesHR, $bestTieHR, $bestTiedByHR, $suspendedHR, $copyObjectsCntHR) = @_;

    my %rPathObjects = ();    # store rRNA r-path objects
    my %rPathTerminals = ();  # store terminal ends of rRNA r-paths
    my %rPathEdgesALL =();        # store edges (ends) that are part of rRNA r-paths

    print $LOG "Finding best ties...\n";

    foreach my $piece (@$sortedByLenAR) {
	print $LOG "\n === $piece ===\n" if ($verbose);
	if (exists($suspendedHR->{$piece})) {
	    print $LOG "\n(ineligible - suspended)\t$piece\t$$suspendedHR{$piece}\n" if ($verbose);
	    next;
	}

	my $end = "$piece.5";
	print $LOG "\n end $end:\n" if ($verbose);
	my @bestPath5 = ();

	my $notice = "";	
	my $endMark = $endMarks{$end};
	my $bestTie;
	if ( ! $endMark && ! exists $bestTieHR->{$end} ) {
	    $bestTie = bestTie($end);
	    if ($bestTie) {
		$bestTieHR->{$end} = $bestTie;
		if (exists($bestTiedByHR->{$bestTie})) {
		    $bestTiedByHR->{$bestTie} .= ",$end";
		} else {
		    $bestTiedByHR->{$bestTie} = $end;
		}
		$notice = "BEST_TIE: [$bestTie]\t$end\n";
	    }
	    else {
		$notice = "BEST_TIE: [none]\t$end\n";
	    }
	}
	elsif ( $endMark == 6 && $onoObjectLengths{$piece} >= $SUSPENDABLE  && 
		(! exists $rPathTerminals{$end}) ) { 
	    # Long piece with an rRNA hit;  
	    # unless this is already a terminal of a another r-path, find a path to a long peer based on rRNA taxonomy (r-path)

	    my %seen = ();
	    my $rRNAcovered = $rRNAdb{$piece}->[1];   # end pos; we assume at least this much of the rRNA is already present in the contig
	    my $taxonomyStr = $rRNAdb{$piece}->[2];
	    @bestPath5 = bestPathToPeer($piece, 3, 0, $onoObjectDepths{$piece}, $taxonomyStr, $endTiesHR, \%seen, $rRNAcovered, 0);
	    shift @bestPath5; #clip first node - algorithm idiosyncracy

	    @bestPath5 = reverse(@bestPath5);  
	    $notice = "BEST_PATH: [ @bestPath5 ]\t$end\n";
	}
	else {
	    print $LOG " - ineligible ($endMark)\n" if ($verbose);
	    $notice = "BEST_TIE: [$endMark]\t$end\n";
	}

	


	$end = "$piece.3";
	print $LOG "\n end $end:\n" if ($verbose);
	my @bestPath3 = ();

	$endMark = $endMarks{$end};
	undef $bestTie;
	if ( ! $endMark && ! exists $bestTieHR->{$end}) {
	    $bestTie = bestTie($end);
	    if ($bestTie) {
		$bestTieHR->{$end} = $bestTie;
		if (exists($bestTiedByHR->{$bestTie})) {
		    $bestTiedByHR->{$bestTie} .= ",$end";
		} else {
		    $bestTiedByHR->{$bestTie} .= $end;
		}
		$notice .= "BEST_TIE: $end\t[$bestTie]\n";
	    }
	    else {
		$notice .= "BEST_TIE: $end\t[none]\n";
	    }

	}
	elsif ( $endMark == 6 && $onoObjectLengths{$piece} >= $SUSPENDABLE  && 
		(! exists $rPathTerminals{$end}) ) { 
	    # long piece with an rRNA hit;  
	    #Unless this is already a terminal of a another r-path, find a path to a long peer based on rRNA taxonomy (r-path)

	    my %seen = ();
	    my $rRNAcovered = $onoObjectLengths{$piece} - $rRNAdb{$piece}->[0] + 1;   # start pos relative to 3'
	    my $taxonomyStr = $rRNAdb{$piece}->[2];
	    @bestPath3 = bestPathToPeer($piece, 5, 0, $onoObjectDepths{$piece}, $taxonomyStr, $endTiesHR, \%seen, $rRNAcovered, 0);
	    shift @bestPath3; #clip first node - algorithm idiosyncracy

	    $notice .= "BEST_PATH: $end\t[ @bestPath3 ]\n";
	}
	else {
	    print $LOG " - ineligible\n" if ($verbose);
	    $notice .= "BEST_TIE: $end\t[$endMark]\n";
	}

	print $LOG $notice if ($verbose);

	#if best-path-to-long was employed, evaluate the paths and make it into a serieas of best ties, copying nodes if necessary.
	if (@bestPath5 || @bestPath3) {

	    foreach my $pathAR (\@bestPath5, \@bestPath3) {
		next unless (@$pathAR);
		my @path =@$pathAR;

		print $LOG "\nBuilding best_ties from path [ @path ]\n" if $verbose; 

		###  if ANY of the end-nodes have already been incorporated into another r-path, make copies and build @uniqPath
		my $prevObj = "";
		my $copyObject = "";
		my @uniqPath;
		my @uniqPathObjects;
		for (my $i=0; $i<=$#path; $i++) {
		    my ($pObject,$pEnd) = $path[$i] =~ /(.+)\.([35])$/;
		    if (exists $rPathObjects{$pObject}) {
			if ( ! exists $rPathEdgesALL{$path[$i]} &&
			     ($i == 0 || $i == $#path)  ) {
			    # SPECIAL CASE: We've encountered this object but not this end,
			    # AND this path does't go beyoud this end.  LEAVE AS IS to help end locking later)
			    $uniqPath[$i]=$path[$i];
			}
			else {
			    # create copy object without any tie info assignment 
			    unless ($pObject eq $prevObj) {
				$copyObject  = create_copy_object(undef, $pObject, undef, undef, \%scaffReport, \$copyObjectsCntHR->{$pObject});
			    }
			    (my $copyEnd = $path[$i]) =~ s/^.+\.([35])/$copyObject\.$1/;
			    $uniqPath[$i] =$copyEnd;  
			    push(@uniqPathObjects, $copyObject);
			}
		    } 
		    else {
			$uniqPath[$i] = $path[$i];
			push(@uniqPathObjects, $pObject);
		    }
		    $prevObj = $pObject;
		}
		for (my $i=0; $i<=$#uniqPath; $i=$i+2) {
		    #update hash of unique r-path edges
		    $rPathEdgesALL{$uniqPath[$i]} = $uniqPath[$i+1];
		    $rPathEdgesALL{$uniqPath[$i+1]} = $uniqPath[$i];
		}
		foreach my $pObj (@uniqPathObjects) {
		    $rPathObjects{$pObj} =1;
		}

		convert_path_to_BestTies(\@uniqPath, $bestTieHR, $bestTiedByHR);
		
		$rPathTerminals{$uniqPath[0]} = $uniqPath[-1];
		$rPathTerminals{$uniqPath[-1]} = $uniqPath[0];
	    }
	}
    }

    # Insert suspended pieces where possible 
    place_suspended($sortedByLenAR, $suspendedHR, $endTiesHR, $bestTieHR, $bestTiedByHR, $copyObjectsCntHR);


    #update tie info for any copy objects that got created without explicit tie reassignment
    while (my @edge = each %rPathEdgesALL) {
	foreach my $rPathEnd (@edge) {
	    if ( ! exists $endTiesHR->{$rPathEnd}) {     # if so, this must be a copy-end
		unless ($rPathEnd =~ /(.+)\.cp.*\.([35])$/) {   #extract original pre-copy object name
		    die("$rPathEnd lacks tie information but isn't a copy object.");
		}
		my $tieInfoSourceEnd = $1.".".$2;
		
		my $otherEnd = $rPathEdgesALL{$rPathEnd};
		my $tieTo;
		if (exists $endTiesHR->{$otherEnd}) {
		    $tieTo = $otherEnd;
		} else {  # also a copy-end we haven't yet "registered"
		    $otherEnd =~ /(.+)\.cp.*\.([35])$/;   #extract original pre-copy object name
		    $tieTo = $1.".".$2;
		}
		copy_tie_info($tieInfoSourceEnd, $rPathEnd, $tieTo , $endTiesHR);
	    }
	}
    }

    print $LOG "Done finding best ties\n";
}

sub place_suspended {
    my ($sortedByLenAR, $suspendedHR, $endTiesHR, $bestTieHR, $bestTiedByHR, $copyObjectsCntHR) = @_;

    print $LOG "Inserting suspensions...\n";

    my $nSuspendedPieces = keys(%{$suspendedHR});

    my $nInsertedSuspensions = 0;
    foreach my $piece (@$sortedByLenAR) {
	unless (exists($suspendedHR->{$piece})) {
	    next;
	}
	my $nInsertedThisPiece = 0;
	my $end5 = "$piece.5";
	my $endMark5 = $endMarks{$end5};
	my $end3 = "$piece.3";
	my $endMark3 = $endMarks{$end3};

	if ( $endMark3 == 4  ||  $endMark5 == 4  ) {
	    # NO_TIES
	    print $LOG "UNINSERTED: ($endMark5) $piece ($endMark3)\n" if ($verbose);
	    next;
	}

	my @ties5 = ();
	if (exists($endTiesHR->{$end5})) {
	    my @tieInfo = @{$endTiesHR->{$end5}};
	    foreach my $t (@tieInfo) {
		my ($nLinks,$gapEstimate,$gapUncertainty,$tiedEnd) = $t =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;
		push(@ties5,$tiedEnd);
	    }   
	} else {
	    print $LOG "UNINSERTED: (NO_5_END_TIES) $piece ()\n" if ($verbose);
	    next;
	}
	my %ties3 = ();
	if (exists($endTiesHR->{$end3})) {
	    my @tieInfo = @{$endTiesHR->{$end3}};
	    foreach my $t (@tieInfo) {
		my ($nLinks,$gapEstimate,$gapUncertainty,$tiedEnd) = $t =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;
		$ties3{$tiedEnd} = 1;
	    }   
	} else {
	    print $LOG "UNINSERTED: () $piece (NO_3_END_TIES)\n" if ($verbose);
	    next;
	}

	if ( exists $bestTieHR->{$end5} || exists $bestTieHR->{$end3})
	{
	    print $LOG "UNINSERTED: () $piece (BEST_TIES_EXIST)\n" if ($verbose);
	    next;
	}

	# buld a list of "triangles" where a 5' end's tie  has a bestTie that's also one of the  3' end's ties
	my %check = ();
	foreach my $t5 (@ties5) {
	    if (exists($ties3{$t5})) { # same end tied to both 5' and 3';
		next;
	    }
	    if (exists($bestTieHR->{$t5}) && exists($ties3{$bestTieHR->{$t5}})) {
		$check{$t5} = $bestTieHR->{$t5};
	    }
	}

	my @v5 = ();
	my @v3 = ();
	my $nValidSuspensions = 0;
	
	foreach (sort { $a cmp $b } keys(%check) ){ #sort for reproducibility
	    my $t5 = $_;
	    my $t3 = $check{$_};
       
	    if (mutualUniqueBest($t5,$t3)) {
		push(@v5,$t5);
		push(@v3,$t3);
		$nValidSuspensions++;
	    }
	}

	if ($nValidSuspensions) { 
	    # remove this piece from %suspended and insert it into the graph by updating links in %bestTie and %bestTiedby
	    
	    for (my $i=0; $i<$nValidSuspensions; $i++) {
		# for suspended collapsed repeats, multiple valid insertions are possible, and we handle this by inserting copy-objects of the suspended candidate

		my $tie5 = $v5[$i];
		my $tie3 = $v3[$i];

		my $pieceToInsert = $piece;
		if ($nInsertedThisPiece > 0) {
		    #Create a copy object to insert
		    $pieceToInsert = create_copy_object($tie5, $piece, $tie3, $endTiesHR, \%scaffReport, \$copyObjectsCntHR->{$piece});
		    last unless ($pieceToInsert);
		}
		
#		if ( ! exists $bestTieHR->{"$pieceToInsert.5"})   {
		$bestTieHR->{"$pieceToInsert.5"} = $tie5;
		$bestTiedByHR->{$tie5} = "$pieceToInsert.5";
#		}
#		else {
#		    warn "WARNING: Suspended end $pieceToInsert.5 already has a best tie outside of the suspension \"triangle\" (".$bestTieHR->{"$pieceToInsert.5"}.").  Will keep existing best tie...\n ";
#		}
		$bestTieHR->{$tie5} = "$pieceToInsert.5";
#		if (exists $bestTiedByHR->{"$pieceToInsert.5"}) {
#		    $bestTiedByHR->{"$pieceToInsert.5"} .= ",$tie5";
#		} else {
		$bestTiedByHR->{"$pieceToInsert.5"} = $tie5;
#		}


#		if ( ! exists $bestTieHR->{"$pieceToInsert.3"})   {
		$bestTieHR->{"$pieceToInsert.3"} = $tie3;
		$bestTiedByHR->{$tie3} = "$pieceToInsert.3";
#		}
#		else {
#		    warn "WARNING: Suspended end $pieceToInsert.3 already has a best tie outside of the suspension \"triangle\" (".$bestTieHR->{"$pieceToInsert.3"}.").  Will keep existing best tie...\n ";
#		}

		$bestTieHR->{$tie3} = "$pieceToInsert.3";
#		if (exists $bestTiedByHR->{"$pieceToInsert.3"}) {
#		    $bestTiedByHR->{"$pieceToInsert.3"} .= ",$tie3";
#		} else {
		$bestTiedByHR->{"$pieceToInsert.3"} = $tie3;
#		}

		$nInsertedSuspensions++;  $nInsertedThisPiece++;
		print $LOG "SUSPENDED-INSERTED:  $tie5\t$pieceToInsert\t$tie3\n" if ($verbose);
		
	    }
	    delete($suspendedHR->{$piece});    

	} else {
	    print $LOG "UNINSERTED: (N_VALID_SUSP: $nValidSuspensions) $piece ()\n" if ($verbose);
	}
    }

    print $LOG "Done. Inserted $nInsertedSuspensions suspensions. ($nSuspendedPieces total suspended pieces)\n";

}


sub bestTie {
    my ($end) = @_;
    print $LOG "BT: testing tied ends \n" if ($verbose);
    my @ties = ();
    if (exists($endTies{$end})) {
	@ties = @{$endTies{$end}};
    } else {
	return 0;
    }

    my ($object) = $end =~ /^(.+)\.[35]$/; 
    my $objectLen = $onoObjectLengths{$object};
    my $largeObject = 0;
    if ($objectLen > $SUSPENDABLE) {
	$largeObject = 1;
    }

    my $nTies = scalar(@ties);
    my @tiedEnds = ();
    my @gapEstimates = ();
    my @gapUncertainties = ();
    my @objectLens = ();
    my @objectDepths = ();

    print $LOG "BT: ties:\n" if ($verbose);
    for (my $i = 0; $i < $nTies; $i++) {
	my $tie_i = $ties[$i];
	print $LOG "BT: \t$tie_i\n" if ($verbose);
	my ($nLinks_i,$gapEstimate_i,$gapUncertainty_i,$tiedEnd_i) = $tie_i =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;

	### skip ends that already have a bestTie to a different end
	if (exists $bestTie{$tiedEnd_i} && $bestTie{$tiedEnd_i} ne $end) {
	    print $LOG "BT: \t\t... Already bestTie'd to a different end; Skipping.\n" if ($verbose);
	    next;
	}
	my $endMark_i = $endMarks{$tiedEnd_i};
	if ($endMark_i == 1) {  #self-tie
	    next;
	}
	if ($endMark_i == 4) {  #no ties
	    next;
	}
	my ($object_i) = $tiedEnd_i =~ /^(.+)\.[35]$/; 
	my $objectLen_i = $onoObjectLengths{$object_i};
	my $objectDep_i = $onoObjectDepths{$object_i};

	push(@tiedEnds,$tiedEnd_i);
	push(@gapEstimates,$gapEstimate_i);
	push(@gapUncertainties,$gapUncertainty_i);
	push(@objectLens,$objectLen_i);
	push(@objectDepths,$objectDep_i);
    }

    my $nGoodTies = scalar(@tiedEnds);

    unless($nGoodTies > 0) {
	print $LOG "BT: NoGoodTies\n" if ($verbose);
	return 0;
    }
    # sort by smallest gap; if equal, by shortest length;  if equal, by least id
    my @sortedTieIndex = sort {($gapEstimates[$a] <=> $gapEstimates[$b]) || ($objectLens[$a] <=> $objectLens[$b]) || ($tiedEnds[$a] cmp $tiedEnds[$b])  } (0..$#tiedEnds);

    my @testSuspended = ();

    # # If a large object, return the closest large object if one exists.
    # # Small objects may be suspended between large objects
    if ($largeObject) {       
    	print $LOG "BT: \tLarge object. \n" if ($verbose);
	my $closestLarge = undef;
	for (my $i=0;$i<$nGoodTies;$i++) {
	    my $ti = $sortedTieIndex[$i];
	    my $tiedEnd_i = $tiedEnds[$ti];
#	    print $LOG "BT: \t\t $tiedEnd_i:" if ($verbose);

	    my ($object_i,$endSuffix_i) = $tiedEnd_i =~ /^(.+)\.([35])$/;
	    my $objectLen_i = $objectLens[$ti];
	    my $otherEndSuffix_i = ($endSuffix_i eq "3") ? "5" : "3";
	    my $testOtherEnd_i = "$object_i.$otherEndSuffix_i";

#	    if ($objectLen_i > $SUSPENDABLE && $endMarks{$tiedEnd_i} != 3 )  {
	    if ($objectLen_i > $SUSPENDABLE )  {
#		print $LOG ".. is closest large\n" if ($verbose);		
		$closestLarge = $tiedEnd_i;
		last;
	    } else {
		push(@testSuspended,$object_i);
#		print $LOG " .. is suspendable \n" if ($verbose);
	    }
	}
	
	if (defined($closestLarge)) {
	    my $otherEnd = $closestLarge;
	    my $nSus = process_suspend_candidates(\%suspended, \@testSuspended, $end, $otherEnd);
	    print $LOG "BT return: closestLarge ($nSus suspended)\n" if ($verbose);
	    return $closestLarge;	    
	}
	print $LOG "BT: no closest large found.. \n" if ($verbose);
    }

    # Return the closest extendable object if one exists.  Objects must be unmarked to qualify.
    # unextendable objects may be suspended

    my $closestExtendable = undef;
    @testSuspended = ();
    for (my $i=0;$i<$nGoodTies;$i++) {
    	my $ti = $sortedTieIndex[$i];
    	my $tiedEnd_i = $tiedEnds[$ti];
    	my ($object_i,$endSuffix_i) = $tiedEnd_i =~ /^(.+)\.([35])$/; 
    	my $otherEndSuffix_i = ($endSuffix_i eq "3") ? "5" : "3";
    	my $testOtherEnd_i = "$object_i.$otherEndSuffix_i";

#    	print $LOG "BT: \t\t $tiedEnd_i:" if ($verbose);
    	if ($endMarks{$tiedEnd_i} || $endMarks{$testOtherEnd_i}) {
	    push(@testSuspended,$object_i);
#	    print $LOG ".. is suspendable (  ends are marked [$endMarks{$tiedEnd_i}, $endMarks{$testOtherEnd_i}])\n" if ($verbose);
    	} else {
    	    $closestExtendable = $tiedEnd_i;
#    	    print $LOG "..is closest extendable\n" if ($verbose);
    	    last;
    	}
    }
    
    if (defined($closestExtendable)) {
	my $otherEnd = $closestExtendable;
    	my $nSus = process_suspend_candidates(\%suspended, \@testSuspended, $end, $otherEnd);
    	print $LOG "BT return: closestExtendable ($nSus suspended)\n" if ($verbose);
    	return $closestExtendable;	
    }
    print $LOG "BT: no closest extendable found.. \n" if ($verbose);
 

    # Just return the closest object

    @testSuspended = ();
    my $closestEnd  = $tiedEnds[$sortedTieIndex[0]];
    print $LOG "BT return: closest\n" if ($verbose);
    return $closestEnd;

}

sub getTieInfo {
    my ($end1,$end2) = @_;
    
    my @ties = ();
    if (exists($endTies{$end1})) {
	@ties = @{$endTies{$end1}};
    }
    else {
	return 0;
    }

    my $nTies = scalar(@ties);

    for (my $i = 0; $i < $nTies; $i++) {
	my $tie_i = $ties[$i];
	my ($nLinks_i,$gapEstimate_i,$gapUncertainty_i,$tiedEnd_i) = $tie_i =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;

	if ($tiedEnd_i eq $end2) {
	    return $tie_i;
	}
    }

    return 0;
}

sub isSplinted {
    my ($end1,$end2) = @_;

    # my $linkedEnds = ($end1 lt $end2) ? "$end1<=>$end2" : "$end2<=>$end1";
    # if (exists($links{$linkedEnds})) {
    # 	my @linkData = @{$links{$linkedEnds}};
    # 	foreach my $link (@linkData) {
    # 	    my ($linkFile,$linkType,@linkInfo) = split(/:/,$link);
    # 	    if ($linkType eq "SPLINT") {
    # 		return 1;
    # 	    }
    # 	}
    # }

    # Since links will contain low-support splints and other junk, rely on the endTies instead;  
    #The uncertaintly of 1 is an approximation for a splint. In rare cases, a span can have an uncertainty of 1. To handle this - need to add an explicit "isSplint" field into tie info
    my $tieInfo = getTieInfo($end1,$end2);
    if ($tieInfo) {
	my ($gapUncertainty) = $tieInfo =~ /.+\:(\d+)\..+/;
	if ($gapUncertainty == 1) {
	    return 1;
	}
    }
    return 0;
}

sub markEnd {
    # evaluate all ties and mark THIS END if any anomalies are found
    my ($end) = @_;
    my ($testObject, $prime) = $end =~ /^(.+)\.([35])$/; 
    my @ties = ();
    if (exists($endTies{$end})) {
	@ties = @{$endTies{$end}};
    } else {
	$endMarkCounts{'NO_TIES'}++;
	return "NO_TIES";
    }


    if (exists $rRNAdb{$testObject}) {
	my $closestHit = ($prime == 5) ? $rRNAdb{$testObject}->[0]  : $onoObjectLengths{$testObject} - $rRNAdb{$testObject}->[1] + 1;  
	my $hitLen = $rRNAdb{$testObject}->[1] - $rRNAdb{$testObject}->[0] + 1;
	if ($closestHit <= $LONG && $hitLen >= $merSize) {   #these are pretty arbtitrary thresholds; the idea is to make sure that hits extend close to the end we're looking at
	    $endMarkCounts{'RRNA'}++;
	    return "RRNA";
	}
    }

    my $nTies = scalar(@ties);
    my @tiedEnds = ();
    my @gapEstimates = ();
    my @gapUncertainties = ();
    my @objectLens = ();
    my @objectDepths = ();

    for (my $i = 0; $i < $nTies; $i++) {
	my $tie_i = $ties[$i];
	my ($nLinks_i,$gapEstimate_i,$gapUncertainty_i,$tiedEnd_i) = $tie_i =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;
	my ($object_i) = $tiedEnd_i =~ /^(.+)\.[35]$/; 

	if ($object_i eq $testObject) {
#	    print $LOG "SELF_TIE: $object_i <=> $testObject !!!\n" if ($verbose);
	    $endMarkCounts{'SELF_TIE'}++;
	    return "SELF_TIE";
	}
	my $objectDepth_i = $onoObjectDepths{$object_i};
	# if ($testObjectDepth/$objectDepth_i > $maxDepthDropoff) {    
	#     print $LOG "DEPTH_DROP:  $object_i ($objectDepth_i) <=> $testObject ($testObjectDepth)\n" if ($verbose);

	#     return "DEPTH_DROP";
	# }

	my $objectLen_i = $onoObjectLengths{$object_i};

	push(@tiedEnds,$tiedEnd_i);
	push(@gapEstimates,$gapEstimate_i);
	push(@gapUncertainties,$gapUncertainty_i);
	push(@objectLens,$objectLen_i);
	push(@objectDepths,$objectDepth_i);
    }

    my @sortedTieIndex = sort {($gapEstimates[$a] <=> $gapEstimates[$b]) || ($objectLens[$a] <=> $objectLens[$b]) || ($tiedEnds[$a] cmp $tiedEnds[$b]) } (0..$#ties);

    if ($nTies >= 2) {
	for (my $i=0;$i<$nTies-1;$i++) {   
  	    my $ti = $sortedTieIndex[$i];	
	    my $start_i = $gapEstimates[$ti];
	    my $end_i = $start_i+$objectLens[$ti]-1;
	    my $uncertainty_i = $gapUncertainties[$ti];

	    my $tj = $sortedTieIndex[$i+1];
	    my $start_j = $gapEstimates[$tj];
	    my $end_j = $start_j+$objectLens[$tj]-1;
	    my $uncertainty_j = $gapUncertainties[$tj];

	    my $overlap = $end_i - $start_j + 1;
	    if ($overlap > $merSize-2) {
		my $excessOverlap = $overlap-($merSize-2);
		my $strain = $excessOverlap / ($uncertainty_i+$uncertainty_j-1);
		if ($strain > $maxStrain) {   
		    # tie confilct - try to resolve by various heuristics. May include changes to %endTies and @sortedTieIndex
		    print $LOG "Potential tie collision: $end -> $ties[$ti] ? $ties[$tj] ... \n" if $verbose;
		    if ( resolve_tie_collision($end, $ti, $tj, \@tiedEnds, \@gapEstimates, \@gapUncertainties, \@objectLens, \@objectDepths, \%endTies)) {
			next;;
		    }
		    else {
			print $LOG "TIE_COLLISION: $end -> $ties[$ti] ? $ties[$tj]\n" if $verbose;
			$endMarkCounts{'TIE_COLLISION'}++;
			return "TIE_COLLISION";
		    }
		}
	    }
	}

    }
    $endMarkCounts{'UNMARKED'}++;
    return "UNMARKED";
}

sub process_suspend_candidates
{
    my ($allSuspendedHR, $suspendCandidatesAR, $end, $otherEnd)  = @_;
  
    my $nSus=0;
    foreach my $sObject (@{$suspendCandidatesAR}) {
    	print $LOG "BT: \tsuspended object $sObject\n"  if ($verbose);
	$$allSuspendedHR{$sObject} =  1;
	$nSus++;	    
    }
    return $nSus;
}


sub create_copy_object {
    my ($t5, $object, $t3, $endTiesHR, $scaffReportHR, $copyObjectsCntRef) = @_;

    my $copyEnd;
    my $copyCnt = ++${$copyObjectsCntRef};
    
    my $copyObject = "${object}.cp".$copyCnt;

    for my $e (5,3) {
	my $end = "$object.$e";
	my $copyEnd = "$copyObject.$e";
	my $myTiedEnd = ($e == 5) ? $t5 : $t3;
	next unless (defined $myTiedEnd);

	reassign_tie_info($end, $copyEnd, $myTiedEnd, $endTiesHR) || return undef;
    }
    

    if (defined $srfFile) {
	# add a new record to the original scaff report to reflect the copy
	my $parentScafRecord = $scaffReportHR->{$object};
	my @scaffoldLines = split(/\n/,$parentScafRecord);
	my @newScaffoldLines;
	foreach (@scaffoldLines) {
	    my @cols = split(/\t/,$_);
	    if ( $cols[1] =~ /^CONTIG/) {
		$cols[0] .= "-CP$copyCnt";   #old scaff id
		$cols[2] .= ".cp$copyCnt";   #contig name
	    }
	    push(@newScaffoldLines, (join("\t",@cols)) );
	}   
	$scaffReportHR->{$copyObject} = (join("\n",@newScaffoldLines));

    }	
    $onoObjectLengths{$copyObject} = $onoObjectLengths{$object};
#    $onoObjectDepths{$object} /= 2;  # split the depth 
    $onoObjectDepths{$copyObject} = $onoObjectDepths{$object};

    
    print $LOG "New copy-object created: $copyObject\n" if ($verbose);
        
    return $copyObject;

}

sub correct_gap_estimate
{
    my ($endTiesHR, $end1, $end2, $newGapSizeEst) = @_;

    my @ties1 = @{$endTiesHR->{$end1}};
    my $idxToFix;
    for (my $i=0; $i <= $#ties1; $i++) {
	if ($ties1[$i] =~ /$end2/) {
	    $idxToFix = $i;
	    last;
	}
    }
    unless (defined $idxToFix) {return 0};
    my $tie1 = ${$endTiesHR->{$end1}}[$idxToFix];
    #"$nGapLinks.$gapEstimate:$gapUncertainty.$end1"
    $tie1 =~ s/^(\d+)\.-?\d+\:/$1\.$newGapSizeEst\:/;
    ${$endTiesHR->{$end1}}[$idxToFix] = $tie1;
    # update reciprocal tie
    my @ties2 = @{$endTiesHR->{$end2}};
    $idxToFix = undef;
    for (my $i = 0; $i <= $#ties2; $i++) {
	if ($ties2[$i] =~ /$end1/) {
	    $idxToFix = $i;
	    last;
	}
    }
    unless (defined $idxToFix) {return 0};
    my $tie2 = ${$endTiesHR->{$end2}}[$idxToFix];
    $tie2 =~ s/^(\d+)\.-?\d+\:/$1\.$newGapSizeEst\:/;
    ${$endTiesHR->{$end2}}[$idxToFix] = $tie2;

    return 1;
}


sub resolve_tie_collision
{
    my ($end, $ti, $tj, $tiedEndsAR, $gapEstimatesAR, $gapUncertaintiesAR, $objectLensAR, $objectDepthsAR, $endTiesHR) = @_;
    my $tiedEnd_i = $tiedEndsAR->[$ti];
    my ($object_i,$objectEnd_i) = $tiedEnd_i =~ /^(.+)\.([35])$/;
    my $otherEnd_i = $object_i;
    $otherEnd_i .= ($objectEnd_i eq "3") ? ".5" : ".3";

    my $tiedEnd_j = $tiedEndsAR->[$tj];
    my ($object_j,$objectEnd_j) = $tiedEnd_j =~ /^(.+)\.([35])$/;
    my $otherEnd_j = $object_j; 
    $otherEnd_j .= ($objectEnd_j eq "3") ? ".5" : ".3";

    my $start_j = $gapEstimatesAR->[$tj]; 


    ### use splint info to detect false strains
    if (isSplinted($otherEnd_i,$tiedEnd_j)) {
	print $LOG ".... averted by SPLINT \n" if $verbose;
	return 1;
    }
    elsif (isSplinted($otherEnd_j,$tiedEnd_i)) {  # splint suggests that current relative order is wrong (most likely due to bad span-based gap estimate)
	my $newGapEstimate = $start_j + $objectLensAR->[$tj] -($merSize-2)+1;   #make new distance to tiedEnd_i larger than dist to tiedEnd_j and update %endTies
	correct_gap_estimate($endTiesHR, $end, $tiedEnd_i, $newGapEstimate) || die "Failed to correct gap estimate for end $tiedEnd_i!";
	print $LOG ".... averted by correcting estimated gap size (relative order invalidated by SPLINT) \n" if $verbose;
	return 1;
    }

    ### if the colision involves a SPAN, and the SPLINTed object is small,  make new distance to  SPAN'ed object larger as we suspect the gap estimate to be negatively biased
    if ($gapUncertaintiesAR->[$ti] > 1 && $gapUncertaintiesAR->[$tj] == 1 && $objectLensAR->[$tj] <= ($maxAvgInsertSize + (2*$maxAvgInsertSdev)) ) {
	my $start_j = $gapEstimatesAR->[$tj];
    	my $newGapEstimate = $start_j+$objectLensAR->[$tj] - ($merSize-2); 
    	correct_gap_estimate($endTiesHR, $end, $tiedEnd_i, $newGapEstimate) || die "Failed to correct gap estimate for end $tiedEnd_i!";
    	print $LOG ".... averted by correcting estimated gap size for the SPAN \n" if $verbose;
    	return 1;
    }
    elsif ($gapUncertaintiesAR->[$tj] > 1 && $gapUncertaintiesAR->[$ti]  == 1 && $objectLensAR->[$ti] <= ($maxAvgInsertSize + (2*$maxAvgInsertSdev)) ) { 
	my $start_i = $gapEstimatesAR->[$ti];
    	my $newGapEstimate = $start_i+$objectLensAR->[$ti] - ($merSize-2);
    	correct_gap_estimate($endTiesHR, $end, $tiedEnd_j, $newGapEstimate) || die "Failed to correct gap estimate for end $tiedEnd_j!";
    	print $LOG ".... averted by correcting estimated gap size for the SPAN  \n" if $verbose;
    	return 1;
    }

    return 0;
}



sub bestPathToPeer
{
    # Recursive function for depth-first traversal of a linked graph;
    # We traverse until we find a "peer" object, i.e. one which is a) long and b)  matches the starting node by rRNA taxonomy (if defined at runtime) or otherwise by depth similarity
    my ($object, $inEndLabel, $gapEst, $rootNodeDepth, $rootNodeTxnmy, $endTiesHR, $seenHR, $totDistance, $stepsCnt) = @_;

    my $maxDist = 6000;  #expected max size of a single ribosomal RNA
    my $maxSteps = 30;
    my $peerDepthMinRatio = 0.67;
    my $minTxnmyDiverenceLvl =6;  #genus

    $seenHR->{$object} = 1;


    my $inEnd = "$object.$inEndLabel";

    unless ($stepsCnt == 0 ) {  # skip the original "root" end

#	print $LOG "DEBUG-BPTP: (s:$stepsCnt,d:$totDistance +$onoObjectLengths{$object})  $inEnd\n";

	if ($onoObjectLengths{$object} >= $LONG) {
	    unless ($endMarks{$inEnd} == 6) {  #we've reached a non-rRNA long object, end traversing this branch here
#		print $LOG "DEBUG-BPTP: non-rRNA long - step back.\n\n";
		return ();
	    }
	    my $thisTxnmy = $rRNAdb{$object}->[2];
	    if (defined $thisTxnmy) { 
#		print $LOG "DEBUG-BPTP: Compare taxonomies:\n\t$rootNodeTxnmy\n\t$thisTxnmy\n" if $verbose;
		my $divergenceLvl = taxonomy_divergence_level($thisTxnmy,$rootNodeTxnmy);
		if (! defined($divergenceLvl) || $divergenceLvl >= $minTxnmyDiverenceLvl){
		    return ($inEnd);
		} 
		else {
		    return (); #if taxonomies diverge stop traversing this branch here
		}
	    }
	    elsif ($rootNodeDepth / $onoObjectDepths{$object} >= $peerDepthMinRatio &&
		  $rootNodeDepth / $onoObjectDepths{$object} <= 1/$peerDepthMinRatio) { 
		return ($inEnd);
	    }
#	    print $LOG "DEBUG-BPTP: peerDepthMinRatio not satisfied - continue traversal...\n";
	}
#	print $LOG "DEBUG-BPTP: short - continue traversal...\n";
	$totDistance += $gapEst + $onoObjectLengths{$object};  
    }


    $stepsCnt++;

    if (($stepsCnt >= $maxSteps || $totDistance >= $maxDist) ) {
#	print $LOG "DEBUG-BPTP: (s:$stepsCnt,d:$totDistance) limit reached  - step back.\n\n ";
    } 
    else {

	my $outEnd = $object;
	$outEnd .= ($inEndLabel eq "3") ? ".5" : ".3";
	my @tiesOut = ();
	if (exists $endTiesHR->{$outEnd}) {
	    @tiesOut = @{$endTiesHR->{$outEnd}};
	}
	else {
	    return ();
	}

	# sort ties by distance
	my @tiedEnds = ();
	my @gapEstimates = ();
	my @objectDepths = ();
	my $nTies = scalar(@tiesOut);
	for (my $i = 0; $i < $nTies; $i++) {
	    my $tie_i = $tiesOut[$i];
	    my ($gapEstimate_i,$gapUncert_i,$tiedEnd_i) = $tie_i =~ /\d+\.(\-?\d+)\:(\d+)\.(.+)/;
	    my ($object_i) = $tiedEnd_i =~ /^(.+)\.[35]$/;

	    next if ($onoObjectLengths{$object_i} < $LONG);  # traverse using large objets only

	    push(@tiedEnds,$tiedEnd_i);
	    push(@gapEstimates,$gapEstimate_i);
	    push(@objectDepths,$onoObjectDepths{$object_i});
	}

	# start with closest and deepest objects 
	my @sortedTieIndex = sort {($gapEstimates[$a] <=> $gapEstimates[$b]) || ($objectDepths[$b] <=> $objectDepths[$a]) || ($tiedEnds[$a] cmp  $tiedEnds[$b])} (0..$#tiedEnds);

	for (my $i=0;$i<=$#tiedEnds;$i++) {
	    my $ti = $sortedTieIndex[$i];
	    my $tiedEnd_i = $tiedEnds[$ti];
	    my ($obj_i,$obj_i_endLabel) = $tiedEnd_i =~ /^(.+)\.([35])$/;
	    # avoid loops
	    next if ($seenHR->{$obj_i});  
#	    #don't follow large depth drops or jumps
	    next if ($rootNodeDepth / $objectDepths[$ti] > 10  || $rootNodeDepth / $objectDepths[$ti] < 0.1);
	    my @path = bestPathToPeer($obj_i,$obj_i_endLabel,$gapEstimates[$ti],$rootNodeDepth, $rootNodeTxnmy,$endTiesHR, $seenHR, $totDistance, $stepsCnt);
	    if (@path) {
		unshift(@path, ($inEnd, $outEnd));  #backtrace the path
#		print $LOG "DFS: backtrace path: @path\n";
		return @path;
	    }
	}
#	print $LOG "DEBUG-BPTP: all exhausted  - step back.\n\n ";
    }
    return ();
}




sub convert_path_to_BestTies
{
    # Aggressively turn the path into bestTies & bestTiedBy entries, overwriting anything that's already present. 
    # This is meant to force a particular path through the graph with no regard to linkage safety.  PRONE TO MISASSEMBLY!

    my ($bestPathAR, $bestTieHR, $bestTiedByHR) = @_;

    for (my $i=0; $i < scalar(@$bestPathAR); $i=$i+2) {
	my ($tied_end_A, $tied_end_B) = ($bestPathAR->[$i], $bestPathAR->[$i+1]);

	my ($obj_A) = $tied_end_A =~ /^(.+)\.[35]$/;
	my ($obj_B) = $tied_end_B =~ /^(.+)\.[35]$/;


	if (exists $bestTieHR->{$tied_end_A}) {
	    delete $bestTiedByHR->{$bestTieHR->{$tied_end_A}}; 
	    delete $bestTieHR->{$tied_end_A};
	}
	if (exists $bestTiedByHR->{$tied_end_A}) {
	    delete $bestTieHR->{$bestTiedByHR->{$tied_end_A}}; 
	    delete $bestTiedByHR->{$tied_end_A};
	}
	if (exists $suspended{$obj_A}) {
	    delete($suspended{$obj_A});
	}


	if (exists $bestTieHR->{$tied_end_B}) {
	    delete $bestTiedByHR->{$bestTieHR->{$tied_end_A}}; 
	    delete $bestTieHR->{$tied_end_B};
	}
	if (exists $bestTiedByHR->{$tied_end_B}) {
	    delete $bestTieHR->{$bestTiedByHR->{$tied_end_B}}; 
	    delete $bestTiedByHR->{$tied_end_B};
	}
	if (exists $suspended{$obj_B}) {
	    delete($suspended{$obj_B});
	}

	$bestTieHR->{$tied_end_A} = $tied_end_B;
	$bestTiedByHR->{$tied_end_B} = $tied_end_A;

	$bestTieHR->{$tied_end_B} = $tied_end_A;
	$bestTiedByHR->{$tied_end_A} = $tied_end_B;

    }

    return 1;    
}


sub convert_path_to_BT_copies
{
    # make copy objects of every element of the path, populate the tie info hash, then create reciprocal bestTies for every tie
    my ($bestPathAR, $bestTieHR, $bestTiedByHR, $copyObjectsCntHR, $endTiesHR) = @_;


    my @bestPath_cp = ();
    my $prevObj = "";
    foreach my $e (@$bestPathAR) {
	my ($object, $endLabel) = $e =~ /^(.+)\.([35])$/;
	my $copyObject;
	if ($object eq $prevObj) {  #we just created a copy object for this one
	    $copyObject = "${object}.cp".$copyObjectsCntHR->{$object};
	}
	else {
	    $copyObject = create_copy_object(undef, $object, undef, undef, \%scaffReport, \$copyObjectsCntHR->{$object});
	}
	push (@bestPath_cp, "$copyObject.$endLabel");
	$prevObj = $object;
    }
    

    # copy full tie info pfrom participating ties
    for (my $i=0; $i < scalar(@$bestPathAR); $i=$i+2) {

	my ($tied_end_A, $tied_end_B) = ($bestPathAR->[$i], $bestPathAR->[$i+1]);
	my ($copyEnd_A, $copyEnd_B) = ($bestPath_cp[$i], $bestPath_cp[$i+1]);
	
	foreach my $tie (@{$endTiesHR->{$tied_end_B}}) {
	    if ($tie =~ /.+$tied_end_A$/) {
		(my $cp_tie = $tie) =~ s/$tied_end_A/$copyEnd_A/;
		push (@{$endTiesHR->{$copyEnd_B}}, $cp_tie);
	    }
	}
	foreach my $tie (@{$endTiesHR->{$tied_end_A}}) {
	    if ($tie =~ /.+$tied_end_B$/) {
		(my $cp_tie = $tie) =~ s/$tied_end_B/$copyEnd_B/;
		push (@{$endTiesHR->{$copyEnd_A}}, $cp_tie);
	    }
	}


	$bestTieHR->{$copyEnd_A} = $copyEnd_B;
	$bestTiedByHR->{$copyEnd_B} = $copyEnd_A;
	$bestTieHR->{$copyEnd_B} = $copyEnd_A;
	$bestTiedByHR->{$copyEnd_A} = $copyEnd_B;

    }
    print $LOG "Copy-path created: [@bestPath_cp]\n" if $verbose;    
}

sub reassign_tie_info
{
    # Reassing tie info between $end and $tiedEnd  to $copyEnd

    my ($end, $copyEnd, $myTiedEnd, $endTiesHR) = @_;

    my $tieToReassign;
    foreach my $tieInfo (@{$endTiesHR->{$end}}) {
	$tieInfo =~ /\d+\.\-?\d+\:\d+\.(.+)$/;
	if ($1 eq $myTiedEnd ) {
	    $tieToReassign = $tieInfo;
	    last;
	}
    }
    unless (defined $tieToReassign) {
	print $LOG "DEBUG (reassign_tie_info): \t\t\t Oops.. no ties to reassign at end $end.\n" if ($verbose);
	return undef;
    }
    
    # assign to the copy-object
    push (@{$endTiesHR->{$copyEnd}}, $tieToReassign);
    print $LOG "DEBUG (reassign_tie_info): \t\t\t tie $tieToReassign reassigned to $copyEnd..\n"  if ($verbose);	    
	
    # remove this tie from the old end's tie info hash
    my @tiesUpdated = grep  ! /$tieToReassign/, @{$endTiesHR->{$end}};
    $endTiesHR->{$end} = \@tiesUpdated;
    
    # Correct the tied end's tie info so that it points back to the new copyEnd
    @{$endTiesHR->{$myTiedEnd}} = map {s/$end/$copyEnd/; $_; } @{$endTiesHR->{$myTiedEnd}}
}

sub copy_tie_info
{
    # Copy tie info between $end and $tiedEnd  to $copyEnd
    my ($end, $copyEnd, $myTiedEnd, $endTiesHR) = @_;
    my $tieToCopy;

    foreach my $tieInfo (@{$endTiesHR->{$end}}) {
	$tieInfo =~ /\d+\.\-?\d+\:\d+\.(.+)$/;
	if ($1 eq $myTiedEnd ) {
	    $tieToCopy = $tieInfo;
	    last;
	}
    }
    unless (defined $tieToCopy) {
	print $LOG "DEBUG (copy_tie_info): \t\t\t Oops.. no ties to copy at end $end.\n" if ($verbose);
	return undef;
    }    
    push (@{$endTiesHR->{$copyEnd}}, $tieToCopy);
#    print $LOG "DEBUG (copy_tie_info): \t\t\t tie $tieToCopy copied to $copyEnd..\n"  if ($verbose);	    
	
    undef $tieToCopy;
    # For the target end, add a new tie info pointing to the copyEnd
    foreach (@{$endTiesHR->{$myTiedEnd}}) {
	$_ =~ /\d+\.\-?\d+\:\d+\.(.+)$/;
	if ($1 eq $end) {
	    ($tieToCopy = $_) =~ s/$end/$copyEnd/;
	    last;
	}
    }
    unless (defined $tieToCopy) {
	print $LOG "DEBUG (copy_tie_info): \t\t\t Oops.. no ties to copy at end $myTiedEnd.\n" if ($verbose);
	return undef;
    }    
    push (@{$endTiesHR->{$myTiedEnd}}, $tieToCopy);
}



# sub commmon_taxonomy_level
# {
#     my ($txaA, $txaB) = @_;
#     my $level;

# #     # if one string is fully contained within the other, we happily assume the best outcome and return the highest taxonomy level
# #     if ($txaA =~ /^$txaB/ || $txaB  =~ /^$txaA/) {
# # 	return 7;
# #     }
#     my @txaA = split(";",$txaA);
#     my @txaB = split(";",$txaB);

#     my @shortest = (@txaA < @txaB) ? @txaA : @txaB; 
#     for (my $i=0; $i<=$#shortest; $i++) {
# 	last if ($txaA[$i] ne $txaB[$i]);
# 	$level = $i+1;  #we want it one-based
#     } 
    
#     return $level;
# }


sub taxonomy_divergence_level {
    # compares two taxonomy strings and returns the level where they diverge; If they don't diverge return undef
    my ($txaA, $txaB) = @_;
    my $level;

    my @txaA = split(";",$txaA);
    my @txaB = split(";",$txaB);

    my @shortest = (@txaA < @txaB) ? @txaA : @txaB; 
    for (my $i=0; $i<=$#shortest; $i++) {
	$level = $i+1;  #we want it one-based
	if ($txaA[$i] ne $txaB[$i]) {
	    return $level;
	}
    } 
    return undef;
}

sub lock_ends {

    my $endLocksHR = shift;

    print $LOG "Locking ends with no competing ties..\n";
    while (my ($end,$ties) = each (%bestTiedBy)) {
	if (exists($endLocksHR->{$end})) {
	    next;
	} 
	my ($piece) = $end =~ /^(.+)\.[35]$/;
	if (exists($suspended{$piece})) {
#	    print $LOG "DEBUG: \t $piece is suspended... skipping\n" if ($verbose);
	    next;
	}

	my @ties = split(/\,/,$ties);
	my $nTies = scalar(@ties);

	my $bestTie = "NONE";
	if (exists($bestTie{$end})) {
	    $bestTie = $bestTie{$end};
	} else {
#	    print $LOG "DEBUG: \t $end  has no best ties.. skipping\n" if ($verbose);
	    next;
	}
#    print $LOG "\nDEBUG: $end ... ties: $ties\n" if ($verbose);

#    if (($nTies == 1) && ($ties[0] eq $bestTie)) {
	my $reciprocalBT = undef;
	for my $t (@ties) {
	    if ($t eq $bestTie) {
		$reciprocalBT = 1;
		last;
	    }
	}
	if ($reciprocalBT) {
	    my $tieInfo = getTieInfo($end,$bestTie);
	    my ($nLinks,$gapEstimate,$gapUncertainty,$nextEnd) = $tieInfo =~ /(\d+)\.(\-?\d+)\:(\d+)\.(.+)/;
	    my $gap = sprintf("%.0f",$gapEstimate);
	    $endLocksHR->{$end} = "$bestTie><$gap:$gapUncertainty";
#	    print $LOG " DEBUG: locked end $end w. " . $endLocksHR->{$end} . "\n "if ($verbose);
	    $endLocksHR->{$bestTie} = "$end><$gap:$gapUncertainty";
#	    print $LOG " DEBUG: locked bestTie $bestTie w. " . $endLocksHR->{$bestTie} . "\n "if ($verbose);
	} else {
	    print $LOG "DIVERGENCE:  $end, $ties\n " if ($verbose);
	    $endLocksHR->{$end} = "DIVERGENCE";

	}
    }

    foreach my $end (keys(%endLocks)) {
	print $LOG "LOCK: $end $endLocks{$end}\n" if ($verbose);
    }

}

sub build_scaffolds {

    my $endLocksHR = shift;
    my $scaffoldId = $srfOffset+1 || 1;
    my %visitedObjects = ();


    open(SRF_OUT, ">$srfOutFile") || die "Can't open $srfOutFile for writing!";
    while (my ($object,$objectLen) = each (%onoObjectLengths)) {
	
	my $startObject = $object;
	my $startEnd = 5;

	if (exists($visitedObjects{$object})) {
	    next;
	}

	# walk back up the lock end chain until we reach termination|divergence|convergence
	my $preState = "TERMINATION";
	my %loopCheck = ();
	while () {
	    if (exists($loopCheck{$startObject})) {
		last;
	    }
	    $loopCheck{$startObject} = 1;
	    
	    my $end = "$startObject.$startEnd";
	    if (exists($endLocksHR->{$end})) {
		my $next = $endLocksHR->{$end};
		if (($next eq "CONVERGENCE") || ($next eq "DIVERGENCE")) {
		    $preState = $next;
		    last;
		} else {
		    ($startObject,$startEnd) = $next =~ /(.+)\.([35])\>\</;
		    $startEnd = ($startEnd == 3) ? 5 : 3;
		    $preState = "EXTENSION";
		}
	    } else {
		$preState = "TERMINATION";
		last;
	    }
	}
	
	%loopCheck = ();
	
	print $LOG "Scaffold$scaffoldId: $preState $startObject ($startEnd) " if ($verbose);

	my $inScaffold = 0;
	my $nextObject = $startObject;
	my $nextEnd = ($startEnd == 3) ? 5 : 3;
	my $nextGap = 0;
	my $nextGapUncertainty = 0;
	my $prevObject = $nextObject;
	my $prevEnd = $nextEnd;
	
	my $newScaffReport = "";
	my $scaffCoord = 1;
	my $scaffContigIndex = 1;

	while () {
	    if (exists($loopCheck{$nextObject})) {
		print $LOG "$nextObject LOOP\n" if ($verbose);
		last;
	    }
	    $loopCheck{$nextObject} = 1;	    
	    $visitedObjects{$nextObject} = 1;
	    my $nextObjectLen = $onoObjectLengths{$nextObject};
	    
	    my $objectOri = "+";
	    if ($nextEnd == 5) {
		$objectOri = "-";
	    }
	    
	    if ($inScaffold) {
		$newScaffReport .= "Scaffold$scaffoldId\tGAP$scaffContigIndex\t$nextGap\t$nextGapUncertainty\n";
		$scaffCoord += $nextGap;
		$scaffContigIndex++;
	    }
	    
	    if (defined($srfFile)) {  # objects are scaffolds from previous srf

		my $oldReport = $scaffReport{$nextObject};

		my @reportLines = split(/\n/,$oldReport);
		if ($objectOri eq "-") {
		    @reportLines = reverse(@reportLines);
		}
		foreach my $line (@reportLines) {
		    my @cols = split(/\t/,$line);
		    if ($cols[1] =~ /^CONTIG/) {
			my ($originalContig,$p0,$p1,$depth) = @cols[2,3,4,5];
			my $originalContigLength = $p1-$p0+1;
			my ($originalContigOri,$originalContigName) = $originalContig =~ /^([\+\-])(.+)$/;

			if ($objectOri eq $originalContigOri) {
			    $originalContigOri = "+";
			} else {
			    $originalContigOri = "-";
			}

			my $contigStartCoord = $scaffCoord;
			my $contigEndCoord = $scaffCoord + $originalContigLength - 1;
			$newScaffReport .= "Scaffold$scaffoldId\tCONTIG$scaffContigIndex\t$originalContigOri$originalContigName\t$contigStartCoord\t$contigEndCoord\t$depth\n";
			$scaffCoord += $originalContigLength;

		    } else {   # a GAP

			my ($originalGapSize,$originalGapUncertainty) = @cols[2,3];
			$newScaffReport .= "Scaffold$scaffoldId\tGAP$scaffContigIndex\t$originalGapSize\t$originalGapUncertainty\n";
			$scaffCoord += $originalGapSize;
			$scaffContigIndex++;
		    }
		}

	    } else {  # objects are contigs
		my $contigOri = $objectOri;
		my $nextContig = $nextObject;
		my $contigStartCoord = $scaffCoord;
		my $contigEndCoord = $scaffCoord + $nextObjectLen - 1;
		my $depth = $onoObjectDepths{$nextObject};

		$newScaffReport .= "Scaffold$scaffoldId\tCONTIG$scaffContigIndex\t$contigOri$nextContig\t$contigStartCoord\t$contigEndCoord\t$depth\n";
		$scaffCoord += $nextObjectLen;
	    }

	    $inScaffold = 1;

	    
	    my $end = "$nextObject.$nextEnd";
	    if (exists($endLocksHR->{$end})) {
		my $next = $endLocksHR->{$end};
		if (($next eq "CONVERGENCE") || ($next eq "DIVERGENCE")) {
		    print $LOG "$nextObject ($nextEnd) $next\n" if ($verbose);
		    last;
		} else {
		    print $LOG "$nextObject ($nextEnd) " if ($verbose);
		    ($prevObject,$prevEnd) = ($nextObject,$nextEnd);
		    ($nextObject,$nextEnd,$nextGap,$nextGapUncertainty) = $next =~ /(.+)\.([35])\>\<(.+)\:(.+)/;
		    print $LOG "[$nextGap +/- $nextGapUncertainty] $nextObject ($nextEnd) " if ($verbose);
		    $nextEnd = ($nextEnd == 3) ? 5 : 3;
		}
	    } else {
		print $LOG "$nextObject ($nextEnd) TERMINATION\n" if ($verbose);
		last;
	    }
	}

#	print $newScaffReport;
	print SRF_OUT $newScaffReport;
	$scaffoldId++;
    }
    close SRF_OUT;

    return $scaffoldId-$srfOffset-1;
}
