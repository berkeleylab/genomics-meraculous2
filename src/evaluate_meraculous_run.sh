#!/bin/bash

#set -e


if [ $# -lt 2 ] 
then
  echo "Usage: $0 <meraculous run dir>  <output dir> " >&2
  exit 0
fi


if [ -z "${MERACULOUS_ROOT}" ] ; then echo "Need to set MERACULOUS_ROOT"; exit 0; fi

FASTA_STATS=$MERACULOUS_ROOT/bin/fasta_stats


Stage[0]='meraculous_import'
Stage[1]='meraculous_mercount'
Stage[2]='meraculous_mergraph'
Stage[3]='meraculous_ufx'
Stage[4]='meraculous_contigs'
Stage[5]='meraculous_bubble'
Stage[6]='meraculous_merblast'
Stage[7]='meraculous_ono'
Stage[8]='meraculous_gap_closure'
Stage[9]='meraculous_final_results'





rundir="$1"
if [ ! -d "$rundir" ]
then 
    echo "Ooops.. Run directory $rundir doesn't exist" >&2
    exit 1
fi

outdir="$PWD/$2"
mkdir -p $outdir

# stdout redirection
exec > $outdir/RUN_SUMMARY.txt




### inputs summary

echo -e "\n\n\n##### BRIEF SUMMARY OF INPUTS & OUTPUTS ######\n\n\n" 

cat $rundir/${Stage[9]}/SUMMARY.txt
echo

echo -e "\n\n\n##### RUN PROGRESS REPORT ######\n"


### restart/resume attempts
restarts=`grep -c RESTARTING $rundir/log/meraculous.log`
echo "Number of restarts:  $restarts"
resumes=`grep -c RESUMING $rundir/log/meraculous.log`
echo "Number of resumes:  $resumes"


### failed stages
failedStages=`grep 'Stage [a-z]*_[a-z]* failed' $rundir/log/errors.log`

echo "Failed stages: $failedStages" 


### job set stats

grep "bulk command" $rundir/log/meraculous.log > $outdir/bulk_commands.from_log
numJobs=`grep "bulk command" $outdir/bulk_commands.from_log | awk '{sum+=$11} END {print sum}'`
numJobSets=`cat $outdir/bulk_commands.from_log | wc -l`
numSuccess=`grep -c "Successfully ran the job set" $rundir/log/meraculous.log `
numIncomplete=`grep -c "Not all jobs validated" $rundir/log/errors.log `
timeSuccess=`grep "Successfully ran the job set" $rundir/log/meraculous.log  | sed 's|[()]||g' |awk '{sum+=$(NF-1)} END {print sum}'`
timeIncomplete=`grep "Unable to run the job set" $rundir/log/meraculous.log  | sed 's|[()]||g' |awk 'BEGIN {sum=0}{sum+=$(NF-1)} END {print sum}'`

echo -e "\n\n\n-----JOB SETS\n" 
echo "Total number of jobs:                             $numJobs" 
echo "Total number of job sets:                         $numJobSets" 
echo "Number of successful job sets:                    $numSuccess"
echo "Number of incomplete or failed job sets:          $numIncomplete"
echo "Time spent in successful job sets:                $timeSuccess s"
echo "Time spent in incomplete or failed job sets:      $timeIncomplete s"

cluster=`grep -c "job set run type: cluster" $rundir/log/meraculous.log `

if [[ $cluster > 0 ]]  #one or more run attempt was done with use_cluster on
then
    numQsubJobs=`grep -c "jobID: "  $rundir/log/meraculous.log`
    numQsubFailed=`grep "System command at line .* failed:" $rundir/log/errors.log | grep -c qsub`
    echo -e "\n\n\n-----CLUSTER SUBMISSIONS\n" 
    echo "Number of submitted task arrays:                             $numQsubJobs" 
    echo "Number of failed qsub submissions:                           $numQsubFailed" 
    #echo "Average time in qw: $qwAvg"  
    #echo "Max time in qw: $qwMax"  
fi



### stage memtimes

echo -e "\n\n\n-----STAGES\n" 


printf "%24s\t %22s\t %22s\t %22s\t %22s\t %22s\n" STAGE  TIME_IN_STAGE\(s\)  PROC_TIME_SUM\(s\)  PROC_TIME_MAX\(s\) PROC_TIME_SUM-MAX\(s\) PROC_MEM_MAX\(kb\)

for stage in "${Stage[@]}"
do
    if [ -d $rundir/${stage} ] 
    then
	find $rundir/$stage -not -path "*JOB_SET_DIR*" -name "*.memtime" -exec cat {} \; > $outdir/${stage}.memtimes-local.tmp
	elapsedMax=0
	memMax=0
	elapsedTot=0
	elapsedMaxTot=0

	stageOKSumTime=`grep "STAGE COMPLETED: $stage" $rundir/log/meraculous.log | sed 's|.*COMPLETED:||' | sed 's|[()]||g' |awk '{sum+=$2} END {printf "%d", sum}'`
	if [ -z $stageOKSumTime  ]; then stageOKSumTime=0; fi
	stageFailedSumTime=`grep "Stage $stage failed" $rundir/log/meraculous.log | sed 's|.*failed||' | sed 's|[()]||g' |awk '{sum+=$1} END {printf "%d", sum}'`
	if [ -z $stageFailedSumTime ]; then stageFailedSumTime=0; fi

	stageTime=$(($stageFailedSumTime+$stageOKSumTime))

	# grab memtimes for serial, local processes
	elapsedMax=`awk 'BEGIN {max = 0} { if ( $(NF-10) > max ){ max=$(NF-10)} } END {printf "%d", max}'  $outdir/${stage}.memtimes-local.tmp`
	memMax=`awk 'BEGIN {max = 0} {sub("KB","",$NF); if ( $(NF) > max ) { max=$(NF)} } END {printf "%d", max}'  $outdir/${stage}.memtimes-local.tmp`
	elapsedTot=`awk '{sum+=$5} END {printf "%d", sum}'  $outdir/${stage}.memtimes-local.tmp`
	elapsedMaxTot=$elapsedTot

	# grab memtimes for parallel processes
	for jobset in `ls -trd $rundir/$stage/JOB_SET_DIR.* 2> /dev/null`
	do
	    find $jobset -name "*memtime" -exec cat {} \; > $outdir/js.tmp
	    if [ ! -s $outdir/js.tmp ]; then continue; fi
	    JSelapsedTot=`cat  $outdir/js.tmp | awk '{sum+=$5} END {printf "%d", sum}'`
	    JSelapsedMax=`cat  $outdir/js.tmp | awk 'BEGIN {max = 0} { if ( $(NF-10) > max ){ max=$(NF-10)} } END {printf "%d", max}'`
	    JSmemMax=`cat  $outdir/js.tmp | awk 'BEGIN {max = 0} {sub("KB","",$NF); if ( $(NF) > max ) { max=$(NF)} } END {printf "%d", max}'`

	    # check if it's a global max
	    if [ $JSelapsedMax -gt $elapsedMax ]; then elapsedMax=$JSelapsedMax; fi
	    if [ $JSmemMax -gt $memMax ]; then memMax=$JSmemMax; fi

	    elapsedTot=$(($elapsedTot + $JSelapsedTot))
	    elapsedMaxTot=$(($elapsedMaxTot + $JSelapsedMax))	# here we sum the longest processes only; with perfect parallelization this will be the true elapsed time
	    rm $outdir/js.tmp
	done

	printf  "%24s\t %22s\t %22s\t %22s\t %22s\t %22s\n" $stage $stageTime $elapsedTot $elapsedMax $elapsedMaxTot $memMax 


    else
	echo -e "$stage:\tno info\n"
    fi
done
    

echo -e "\n\n\n##### INTERMEDIATE RESULTS SUMMARY #######" 



### mercount 
echo -e "\n\n\n-----MERCOUNTS:\n"
cat $rundir/${Stage[1]}/stage_validation.*


## mergraph
echo -e "\n\n\n-----MERGRAPH:\n"
cat $rundir/${Stage[2]}/stage_validation.*

### contigs
echo -e "\n\n\n-----CONTIGING:\n" 
$FASTA_STATS $rundir/${Stage[4]}/UUtigs.fa | grep contig

if [ -f $rundir/${Stage[5]}/haplotigs.err ]
then
    ### bubble
    echo -e "\n\n\n-----BUBBLE-TIGGING:\n"
    grep Total $rundir/${Stage[5]}/haplotigs.err
    echo
    $FASTA_STATS $rundir/${Stage[5]}/haplotigs.fa | grep contig
fi

### mapping analysis


echo -e "\n\n\n-----LIBRARY MAPPING ANALYSIS:"

stage=${Stage[7]};

echo -e "\nGap-splinting:\n"
printf "%16s %24s %16s\n" total_mapped hits_omitted\(rate\) total_splints
totalMapped=`grep "Total alignments found:" $rundir/$stage/SPLINTING/*.splints.bma.err | awk 'BEGIN {SUM = 0} {SUM += $4} END {print SUM}'`
totalOmitted=`grep "Total unused =" $rundir/$stage/SPLINTING/*.splints.bma.err | awk 'BEGIN {SUM = 0} {SUM += $NF} END {print SUM}'`
omittedUninformative=`grep UNINFORMATIVE $rundir/$stage/SPLINTING/*.splints.bma.err | awk 'BEGIN {SUM = 0} {SUM += $NF} END {print SUM}'`
#omittedTrunc=`grep TRUNCATED $rundir/$stage/SPLINTING/*.splints.bma.err | awk 'BEGIN {SUM = 0} {SUM += $NF} END {print SUM}'`
#omittedSingleton=`grep SINGLETON $rundir/$stage/SPLINTING/*.splints.bma.err | awk 'BEGIN {SUM = 0} {SUM += $NF} END {print SUM}'`
omittedInformative=$(echo "$totalOmitted - $omittedUninformative" | bc)
ommitRate=$(echo "scale=3; $omittedInformative / $totalMapped" | bc -l)

splintsFound=`grep "Total splints found" $rundir/$stage/SPLINTING/*.splints.bma.err |  awk 'BEGIN {SUM = 0} {SUM += $NF} END {print SUM}'`
printf "%16s %18s(%s) %16s\n" $totalMapped $omittedInformative $ommitRate $splintsFound

echo -e "\nGap-spanning:\n"
printf "%12s %16s %24s %16s\n" library total_mapped hits_omitted\(rate\) total_spans

for lib in `grep 'lib_q' $rundir/checkpoints/${stage}.local.params | awk '{print $2}'`;
do
    if ! `ls $rundir/$stage/ROUND_*/blastMap.${lib}.*.spans.bma.err  >/dev/null 2>&1`; then continue; fi;
    totalMapped=`grep "Total alignments found:" $rundir/$stage/ROUND_*/blastMap.${lib}.*.spans.bma.err | awk 'BEGIN {SUM = 0} {SUM += $4} END {print SUM}'`
    totalOmitted=`grep "Total unused =" $rundir/$stage/ROUND_*/blastMap.${lib}.*.spans.bma.err | awk 'BEGIN {SUM = 0} {SUM += $NF} END {print SUM}'`
    omittedUninformative=`grep UNINFORMATIVE $rundir/$stage/ROUND_*/blastMap.${lib}.*.spans.bma.err | awk 'BEGIN {SUM = 0} {SUM += $NF} END {print SUM}'`
    omittedInformative=$(echo "$totalOmitted - $omittedUninformative" | bc)
    ommitRate=$(echo "scale=3; $omittedInformative / $totalMapped" | bc -l)

    spansFound=`grep "Total spans found =" $rundir/$stage/ROUND_*/blastMap.${lib}.*.spans.bma.err |  awk 'BEGIN {SUM = 0} {SUM += $NF} END {print SUM}'`

    printf "%12s %16s %18s(%s) %16s\n" $lib $totalMapped $omittedInformative $ommitRate $spansFound
done


echo -e "\n\t\tMapped reads omitted for following reasons:\n"

printf "%12s %16s %16s %16s\n"  library truncated singleton minlen 
for lib in `grep 'lib_q' $rundir/checkpoints/${stage}.local.params | awk '{print $2}'`;
do
    if ! `ls $rundir/$stage/ROUND_*/blastMap.${lib}.*.spans.bma.err >/dev/null 2>&1`; then continue; fi;
     hitsOmitTrunc=`grep TRUNCATED $rundir/$stage/ROUND_*/blastMap.${lib}.*.spans.bma.err | awk 'BEGIN {SUM = 0} {SUM += $NF} END {print SUM}'`
     hitsOmitSingleton=`grep SINGLETON $rundir/$stage/ROUND_*/blastMap.${lib}.*.spans.bma.err | awk 'BEGIN {SUM = 0} {SUM += $NF} END {print SUM}'`
     hitsOmitMinlen=`grep MINLEN $rundir/$stage/ROUND_*/blastMap.${lib}.*.spans.bma.err | awk 'BEGIN {SUM = 0} {SUM += $NF} END {print SUM}'`
     printf "%12s %16s %16s %16s %16s\n" $lib $hitsOmitTrunc $hitsOmitSingleton $hitsOmitMinlen
done



### scaffolding
echo -e "\n\n\n-----SCAFFOLDING:\n" 

lastRound=`ls -1d $rundir/$stage/ROUND_* | awk -F_ 'BEGIN {max = 0} {if ( $NF > max ) { max=$NF }} END {print max}'`

printf "%12s %12s %12s %12s %12s %12s %12s %12s %12s %12s\n" round avgSpanGap totalSpans anomalous anomRate avgScfGap nLinks bestMinLinksCutoff scaffoldN50
for r in $(eval echo "{1..$lastRound}"); 
do 
  avgSpanGapSize=`grep SPAN $rundir/$stage/ROUND_$r/linkFile.$r | awk '{sum+=$4} END { print sum/NR}'`
  totalSpans=`grep SPAN $rundir/$stage/ROUND_$r/linkFile.$r | cut -f3 | awk -F'|' '{sum+=$2} END {print sum}'`
  anomalous=`grep SPAN $rundir/$stage/ROUND_$r/linkFile.$r | cut -f3 | awk -F'|' '{sum+=$1} END {print sum}'`
  anomalyRate=$(echo "scale=2; $anomalous / $totalSpans" | bc -l)
  nLinks=`wc -l $rundir/$stage/ROUND_$r/linkFile.$r |awk '{print $1}'`

  best_pN50=0;
  for p in {3..10}; 
  do 
      q=$rundir/$stage/ROUND_$r/p$p.$r.N50;
      n50=`cat $q | awk '$5 > 0.5 {print $3}' | head -1`; 
      if [ $n50 -gt $best_pN50 ]; then best_pN50=$n50; bestP=$p; fi; 
  done
  avgScfGapSize=`grep GAP $rundir/$stage/ROUND_$r/p$bestP.$r.srf | awk '{sum+=$3} END { print sum/NR}'`
  printf "%12s %12s %12s %12s %12s %12s %12s %12s %12s %12s\n" $r $avgSpanGapSize $totalSpans $anomalous $anomalyRate $avgScfGapSize $nLinks $bestP $best_pN50

done






### gap closing 

echo -e "\n\n\n-----GAP CLOSING:\n" 

stage=${Stage[8]};

summary=`grep 'Done.  Successfully closed'  $rundir/$stage/GAPS/gapData.*.closures.err | sed 's|[()]||g'  | awk '{sumClosed+=$4; sumFailed+=$6}END{print sumClosed,sumFailed}'`
parsed=($summary)  #parses space-separated words into array
closedGaps=${parsed[0]}
failedToClose=${parsed[1]}
closureRate=$(echo "scale=2; $closedGaps / ( $closedGaps + $failedToClose )" | bc -l)
gapFillingReads=`grep 'Total reads placed in gaps' $rundir/$stage/gapData.*.err | awk '{sum+=($7+$10)}END{print sum}'`

echo 
echo "Gap-filling reads: $gapFillingReads" 
echo "Gaps closed:  $closedGaps" 
echo "Gaps failed to close: $failedToClose" 
echo "Efficiency of gap closure: $closureRate" 


### aggregate conting/scaffold stats
echo
echo -e "\n\n\n##### UNFILTERED SCAFFOLDS #######\n"
if [ -f ${Stage[7]}/alt.variant.scaffold.list ]
then
    echo -e "      (includes all diploid variants)\n"
fi

$FASTA_STATS $rundir/$stage/final.scaffolds.fa.unfiltered > $outdir/final.scaffolds.fa.unfiltered.stats
cat $outdir/final.scaffolds.fa.unfiltered.stats

echo
echo -e "\n\n\n##### FINAL SCAFFOLDS ( 1kb+) #######\n"

$FASTA_STATS $rundir/$stage/final.scaffolds.fa > $outdir/final.scaffolds.fa.stats
cat $outdir/final.scaffolds.fa.stats


rm $outdir/bulk_commands.from_log
rm  $outdir/*.tmp
echo "Done." >&2



