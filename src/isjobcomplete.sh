#!/bin/bash -f

## Author: Douglas Jacobsen <dmjacobsen@lbl.gov>
## Copyright (c) The Regents of the University of California 
##
## SGE qstat | SLURM squeue logging wrapper; 
## this wrapper has two functions:
## 1) looks for fail to receive a jobid and will retry
## 2) when 'qs' is available, sends qstat command, user, hostname, datetime, and execution to a remote logger
## the purpose of the fxn 2 is to allow determination of 1) who is running qstat, 2) from where, 3) and how frequently.
## by collecting the execution time we can try to correlate how well the scheduler is speaking to different hosts at
## different times; the timeseries may provide a reference for correlating poor user experience to system events

qsbin=`which qs 2>/dev/null`   # JGI-specific; will not be present on external systems. But it's ok, we'll fall back on qstat


maxretries=5
delayscaling=3
delaytime=1

## runQS - checks to see if job is in cached qstat first
## returns 0 if job is found
## returns 1 if job is not found
## returns -1 if qs exit status is non-zero or WARNING detected
function runQS {
	jobid=$1
	output=`$qsbin -j $jobid`
	retcode=$?
	warn=`echo $output | grep "WARNING" | wc -l`
	#if [ $retcode != 0 ] || [ $warn != 0 ]; then ## Commented out because if qs is in WARNING, we don't want to generate additional qstat load
	#	return 2
	#fi
	lineCount=`echo $output | grep "No jobs matched search criteria" | wc -l`
	if [ $lineCount != 0 ]; then
		return 1
	fi
	return 0
}

function runQStat {
	jobid=$1
	output=`qstat -j $jobid 2>&1`
	retcode=$?
	if [ $retcode != 0 ]; then
		return 2
	fi
	lineCount=`echo $output | grep "do not exist" | wc -l`
	if [ $lineCount -gt 0 ]; then
		return 1
	fi
	return 0
}
function runSQueue {
	jobid=$1
	output=`squeue --job $jobid 2>/dev/null`
	retcode=$?
	if [ $retcode != 0 ]; then
		return 2
	fi
	lineCount=`echo "$output" | wc -l`  
	if [ $lineCount -le 1 ]; then #only header line is printed
		return 1
	fi

	return 0
}



# tests if job is still running
# if qs is found on the system use qs to check cache version first
# if no cached output shows the jobid, then check qstat instead
## returns 0 if job is found
## returns 1 if job is not found
## returns -1 if neither qs doesn't find jobid and qstat fails to return 0 status
function testJobRunning {
	jobid=$1
	if [[ $SLURM_ROOT ]] ; then
	    runSQueue $jobid
            return $?
	elif [ -e $qsbin ]; then
	    runQS $jobid
	    qsRet=$?
	    if [ $qsRet != 0 ]; then
		runQStat $jobid
		return $?
	    fi
	    return $qsRet
	elif [[ $SGE_ROOT ]] ; then
	    runQStat $jobid
            return $?
	else
	    echo "Error: no cluster scheduling software found!"
	    exit 0;
	fi

}

## who, where, and when
user=`id -un`
hostname=`hostname`
currtime=`date`

if [ "$1" == "" ]; then
	echo "Usage: $0 jobid"
	echo "Specify jobid to check if the job is presently queued or running"
	exit 0
fi
jobids="$1"

#for (( i = 1; i <= $# ; i++ )); do
#	if [ -n $jobids ]; then
#		jobids="$jobids,$i"
#	else
#		jobids=$i
#	fi
#done
testJobRunning $jobids
isJobRunning=$?
if [ $isJobRunning == 0 ]; then
	echo "$jobids queued/running"
	exit 1
else 
	echo "$jobids not queued/running"
	exit 0
fi
