#!/bin/bash

## Author: Eugene Goltsman <egoltsman@lbl.gov>
## DOE Joint Genome Institute
##
## This script creates a directory structure necessary to start a new Meraculous run bootstrapping from another existing run's stage.
## Symbolic links to stages from the old run are added, up to and including the specified stage.  Links to the checkpoint and internal 
## parameter files are also added.  
## After running this script user should be able to start a run with a normal meraculous.pl command, e.g.:
##
## meraculous.pl -c config.txt -dir <new_run_dir> -restart -start <next_stage>
##


me=`basename $0`
if [[ $# -eq 0 ]] ; then
    echo "Usage:  $me -o <old_run_dir> -n <new_run_dir> -s <from_stage>"
    exit 0                                                                                                                  
fi 


stages=( "meraculous_import" 
    "meraculous_mercount" 
    "meraculous_mergraph"
    "meraculous_ufx"
    "meraculous_contigs"
    "meraculous_bubble"
    "meraculous_merblast"
    "meraculous_ono"
    "meraculous_gap_closure"
    "meraculous_final_results" )

old_run_dir=
new_run_dir=
from_stage=

while getopts o:n:s: opt; do
  case $opt in
  o)  # the run directory you're bootstrapping from
      old_run_dir=`realpath $OPTARG`
      ;;
  n)  # the nre run directory
      new_run_dir=$OPTARG
      ;;
  s)  # the stage to bootstrap from
      from_stage=$OPTARG
      ;;
  esac
done

shift $((OPTIND - 1))

if !  [[ ${stages[@]} =~ " $from_stage " ]] 
then
    echo "Error: invalid stage name: $from_stage"
    exit 1
fi

mkdir  $new_run_dir
mkdir  $new_run_dir/checkpoints
touch  $new_run_dir/run_pids.history


for stage in ${stages[*]}
do

    if [ ! -d ${old_run_dir}/${stage} ]
    then
	    echo "Error: stage $stage is missing in the original run folder"
	    exit 1
    fi
    ln -si ${old_run_dir}/${stage} ${new_run_dir}/


    if [ ! -f ${old_run_dir}/checkpoints/${stage}.ckpt ]
    then
	    echo "Error: checkpoint is missing for stage $stage   (${old_run_dir}/checkpoints/${stage}.ckpt)"
	    exit 1
    fi
    ln -si ${old_run_dir}/checkpoints/${stage}.ckpt ${new_run_dir}/checkpoints/


    if [ ! -f ${old_run_dir}/checkpoints/${stage}.local.params ]
    then
	    echo "Error: local parameter file is missing for stage $stage   (${old_run_dir}/checkpoints/${stage}.local.params)"
	    exit 1
    fi
    ln -si ${old_run_dir}/checkpoints/${stage}.local.params ${new_run_dir}/checkpoints/
    [[ $stage == $from_stage ]] && break
done

echo -e "\nCreated a pseudo-run structure in ${new_run_dir}\n\n"
