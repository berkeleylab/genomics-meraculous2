#
# M_LibData.pm
#
# This class takes in a hash of key/rule information for parsing the
# sequence data information. The hash itself is a hash of hashes
# where the parent key is the key name to be loaded into the object.


# Initial Version:     10/24/2014
# Latest Revision:     10/24/2014
# Author:              Eugene Goltsman
#




#####################################################################
# Module Header                                                     #
#####################################################################

#
# Some boilerplate statements.
#
package M_LibData;
use Exporter;
@ISA = qw(Exporter);


# Insert a line-return separated list of the exported constants
# in the "Constants" portion of the hash.
#
%EXPORT_TAGS = (
    Functions => [qw()],
    Constants => [qw(
                     LIB_PROP_fastqs
                     LIB_PROP_name
                     LIB_PROP_estInsAvg
                     LIB_PROP_estInsSdev
                     LIB_PROP_trueInsAvg
                     LIB_PROP_trueInsSdev
                     LIB_PROP_readLen
                     LIB_PROP_hasInnieArtifact
                     LIB_PROP_isRevComped
                     LIB_PROP_forContigging
                     LIB_PROP_forScaffRound
                     LIB_PROP_forGapClosing
                     LIB_PROP_5primeWiggleRoom
                     LIB_PROP_qOffset
                     LIB_PROP_downsampleRate
                     )]
    );

Exporter::export_tags('Constants');


use strict;
use warnings;


use M_Constants;
use M_Generic_Config;

#
# Constants for library properties:   will serve only as keys to the hash
#
use constant LIB_PROP_fastqs => "fq";
use constant LIB_PROP_name => "name";
use constant LIB_PROP_estInsAvg => "EstInsAv";
use constant LIB_PROP_estInsSdev => "EstInsdev";
use constant LIB_PROP_trueInsAvg => "TruInsav";
use constant LIB_PROP_trueInsSdev => "TruInsdev";
use constant LIB_PROP_readLen => "rl";
use constant LIB_PROP_hasInnieArtifact => "innie";
use constant LIB_PROP_isRevComped => "rev";
use constant LIB_PROP_forContigging => "forCont";
use constant LIB_PROP_forScaffRound => "forScaf";
use constant LIB_PROP_forGapClosing => "forGaps";
use constant LIB_PROP_5primeWiggleRoom => "wr5";
use constant LIB_PROP_qOffset => "qOff";
use constant LIB_PROP_downsampleRate => "dsRate";



#####################################################################
# Class Methods                                                     #
#####################################################################


#
# Method: new
# -----------
# The method used to create new instances of a <My Class> object.
# This method takes the following arguments:
#
# $_[0]     Either the class name of the to-be-created object,
#           or a reference to a pre-existing object of the
#           to-be-created one.
#
# $_[1]     user parameters object to load the lib_seq multi-value parameters from
#
# $_[2]     local parameters object to load any previously auto-detected library information from (e.g. qOffset)
#
# $_[3]     The logging object
#
#
sub new
{
    # Please refer to the above method description for details of
    # what these variables are.
    my($type, $pUserParams, $pLocalParams, $pLog) = @_;

    # The class name for the current object.
    my $class;

    # A reference to the to-be-created object.
    my $this;

    # A string to identify this method.
    my $function_id = "new";


    #
    # Initialize the object as an empty hash ref
    #
    $this = {};

    #
    # Determine the class name for the current object.
    #
    $class = ref($type) || $type;

    #
    # bless the (currently empty) hash into the class.
    #
    bless($this, $class);



    if ($this->load_seq_data($pUserParams, $pLog) != JGI_SUCCESS)
    {
	$pLog->error("Unable to load user-defined library info!");
        return undef();
    }

    if ($this->load_local_lib_info($pLocalParams, $pLog) != JGI_SUCCESS)
    {
	$pLog->error("Unable to load local library info!");
        return undef();
    }

    #
    # If one gets here, everything must have worked, so return
    # a reference to the newly-minted object.
    #
    return $this;
}




#
# Method: load_seq_data
# -----------------------
# This method stores the sequencing library information in a two-dimensional hash (there could be multiple libraries)
# given a reference to the parameters object containing the user-defined seq_data values 
# (or rather refs to arrays of values for each parameter)
#
sub load_seq_data
{
    my ($this, $pUserParams, $pLog) = @_;


    my( @libWildCards, @libNames, @libInsertSizeAvg, @libInsertSizeSdev );
    my ( @libAvgReadLen, @libIsIlluminaLongInsert,  @libIsRevComped );
    my ( @libUseForContigs, @libOnoSetId, @libUseForGapClosing, @lib5p_wiggleRooms, @libDownsampleRates );

    $pUserParams->get_key_values( $pLog, USER_PARAM_lib_seq, 
				  \@libWildCards,
				  \@libNames,
				  \@libInsertSizeAvg,
				  \@libInsertSizeSdev,
				  \@libAvgReadLen,
				  \@libIsIlluminaLongInsert,
				  \@libIsRevComped,
				  \@libUseForContigs,
				  \@libOnoSetId,
				  \@libUseForGapClosing,
				  \@lib5p_wiggleRooms,
	                          \@libDownsampleRates);


# get_key_values returns arrays of values for each parameter (vertical);  we need to convert it into a two-dimensional hash

    for( my $i=0; $i < scalar(@libNames); $i++ )
    {
	my $name = $libNames[$i];
	if (exists $this->{$name}) 
	{
	    $pLog->error("Error: Multiple user parameter entries for library $name!");
	    return JGI_FAILURE;
	}
	else 
	{
	    $this->{$name}->{&LIB_PROP_fastqs} = $libWildCards[$i];
	    $this->{$name}->{&LIB_PROP_name} = $libNames[$i];  #this basically points to itself so we don't really need this property 
	    $this->{$name}->{&LIB_PROP_estInsAvg} = $libInsertSizeAvg[$i];
	    $this->{$name}->{&LIB_PROP_estInsSdev} = $libInsertSizeSdev[$i];
	    $this->{$name}->{&LIB_PROP_readLen} = $libAvgReadLen[$i];
	    $this->{$name}->{&LIB_PROP_hasInnieArtifact} = $libIsIlluminaLongInsert[$i];
	    $this->{$name}->{&LIB_PROP_isRevComped} = $libIsRevComped[$i];
	    $this->{$name}->{&LIB_PROP_forContigging} = $libUseForContigs[$i];
	    $this->{$name}->{&LIB_PROP_forScaffRound} = $libOnoSetId[$i];
	    $this->{$name}->{&LIB_PROP_forGapClosing} = $libUseForGapClosing[$i];
	    $this->{$name}->{&LIB_PROP_5primeWiggleRoom} = $lib5p_wiggleRooms[$i];
	    $this->{$name}->{&LIB_PROP_downsampleRate} = $libDownsampleRates[$i];

	    # initialize additional properties for this library, which will get assigned values later
	    $this->{$name}->{&LIB_PROP_trueInsAvg} = undef;
	    $this->{$name}->{&LIB_PROP_trueInsSdev} = undef;
	    $this->{$name}->{&LIB_PROP_qOffset} = undef;
	}
    }
   
    return JGI_SUCCESS;
}


#
# Method: load_local_lib_info
# -----------------------
# Similar to load_seq_data, this method loads and re-structures additional library data as properties of existing libraries.  
# The main input is a reference to the object containing the internal (local) parameters (or rather refs to arrays of values for each parameter).
# Note: The input object can be empty or it may contain other parameters, but we're only interested in library-related entries here.
#
sub load_local_lib_info
{
    my ($this, $pLocalParams, $pLog) = @_;

    my (@libNames, @libSizesTrue, @libStdevsTrue, @libQoffsets);


    $pLocalParams->get_key_values( $pLog, LOCAL_PARAM_lib_insert_size_recalc, \@libNames, \@libSizesTrue, \@libStdevsTrue );
    for( my $i=0; $i < scalar(@libNames); $i++ )
    {
	my $name = $libNames[$i];
	unless ( exists $this->{$name} )
	{
	    $pLog->error("Error: unknown library: $name;  Libraries must first be defined in the user params file" );
	    return JGI_FAILURE;
	}
	$this->{$name}->{&LIB_PROP_trueInsAvg} = $libSizesTrue[$i];
	$this->{$name}->{&LIB_PROP_trueInsSdev} = $libStdevsTrue[$i];
    }


    @libNames = ();
    $pLocalParams->get_key_values( $pLog, LOCAL_PARAM_lib_qoffset, \@libNames, \@libQoffsets);
    for( my $i=0; $i < scalar(@libNames); $i++ )
    {
	my $name = $libNames[$i];
	unless ( exists $this->{$name} )
	{
	    $pLog->error("Error: unknown library: $name;  Libraries must first be defined in the user params file" );
	    return JGI_FAILURE;
	}
	$this->{$name}->{&LIB_PROP_qOffset} = $libQoffsets[$i];
    }
    return JGI_SUCCESS;
}

#
# Method: get_lib_names
# -----------------------
# This method retruns an array of library names from the object 
#
# $_[0]    A reference to the object on which the method is being called.
#
# $_[1]    The logging object
sub get_lib_names
{
    my $this = shift;
    my $pLog = shift;

    my @libArr  = keys %$this;

    return \@libArr;
}



#
# Method: get_lib_prop
# -----------------------
# This method gets a property value given a library name and a property name as the key 
#
# $_[0]    A reference to the object on which the method is being called.
#
# $_[1]    The library name
#
# $_[2]    The property name
#
# $_[3]    The logging object
#
sub get_lib_prop
{
    my ($this, $lib, $prop, $pLog) = @_;
    if ( ! exists $this->{$lib}->{$prop} )
    {
	$pLog->error("Error: property $prop doen't exist for library $lib");
	return undef();  # can't return JGI_FAILURE since 1 or 0 can be valid values
    }
    my $value = $this->{$lib}->{$prop};
    return $value;
}





#
# Method: set_lib_prop
# -----------------------
# This method stores a value given a library name and a property name
# which are keys to a two-dimensional hash
#
# $_[0]    A reference to the object on which the method is being called.
#
# $_[1]    The library name 
#
# $_[2]    The property name
#
# $_[3]    The value
#
# $_[4]    The logging object
#
sub set_lib_prop
{
    my ($this, $lib, $prop, $val, $pLog) = @_;

    # to prevent spurious properties from being added using this method we require that the
    # property be already present and set to some null value
    unless ( exists $this->{$lib}->{$prop} )
    {
    	$pLog->error( "Error: invalid library property!");
    	die;
    }
    $this->{$lib}->{$prop} = $val;
    return JGI_SUCCESS;
}



# Method: create_set_info
# ---------------------------
# Creates an array of library sets to be used in, say, scaffolding rounds
# This function takes the following arguments:
#
# $_[0]                  A reference to the object on which the method is being called.
#
# This function returns the following values:
#
# @setInfo             -  array where id = set_id and  value = array of lib names in that set
#
sub create_set_info
{
    my ( $this ) = @_;
    my @setInfo;
    foreach my $libName (keys %$this)
    {
	my $setId = $this->{$libName}->{&LIB_PROP_forScaffRound};
	push @{ $setInfo[ $setId ] }, $libName;
    }
    return( @setInfo );
}





#
# Method: DESTROY
# ---------------
# This method cleans up when a <My Class> object is going away.
# By default, this method doesn't do anything; it's included just
# in case something needs to be added in the future.
# This method takes the following arguments:
#
# $_[0]     A reference to the to-be-destroyed object.
#
sub DESTROY
{
    # Please refer to the above method description for details of
    # what these variables are.
    my ($this) = @_;

    # A string to identify this function.
    my $function_id = "DESTROY";


    # Nothing to do here, yet.
}

#
# A seemingly gratuitous statement, included so that the interpreter
# doesn't complain.
#
1;
