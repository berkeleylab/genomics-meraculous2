=head1 NAME

M_Utility - Utility functions used in Meraculous

=head1 SYNOPSIS

 Module for utility functions that are generally useful exported as functions.

=head1 DESCRIPTION

 Utility functions such as file and directory handling.

=head1 VERSION

 Latest Revision: 4/17/2013

=head1 AUTHORS

 James Han
 Joel Martin
 Jeff Froula
 Harris Shapiro
 Eugene Goltsman

=cut





#####################################################################
# Module Header                                                     #
#####################################################################

#
# Some boilerplate statements.
#
package M_Utility;
use Exporter;
@ISA = qw(Exporter);

#
# Insert a line-return separated list of function names in the
# "Functions" portion of the hash.
# Insert a line-return separated list of the exported constants
# in the "Constants" portion of the hash.
#
%EXPORT_TAGS = (
		Functions => [qw(
				 create_checkpoint
				 create_dir_structure
				 create_file_dir_structure
				 create_timestamp
				 determine_checkpoint
				 determine_checkpoint_index
                                 get_common_taxa_level
                                 get_fastq_info
                                 getFastaSizeInfo
                                 get_qscore_offset
                                 fasta_lengths
                                 mate_name
                                 parse_fastq_header
				 run_local_cmd
				 std_entry_processing
                                 sum
				 )],
		);

Exporter::export_tags('Functions');

#####################################################################
# Exported Constant Definitions                                     #
#####################################################################


#####################################################################
# Local Constant Definitions                                        #
#####################################################################

# A string to identify this module.
my $SCRIPT_ID = 'M_Utility.pm';

# A string to identify this module's version.
my $VERSION_ID = 'Version 07/25/2014';

# A string to identify this module's author.
my $AUTHOR_ID = 'James Han, Joel Martin, Jeff Froula, Harris Shapiro, Eugene Goltsman';



#####################################################################
# Included Modules                                                  #
#####################################################################

#
# Enforce good programming style.
#
use strict;
use warnings;
use Carp;

#
# Tools for parsing a full pathname of a file.
#
use File::Basename;

#
# Include some file/path functions.
#
use File::Path;

#
# Tools for safely copying and moving files.
#
use File::Copy;

#
# Tools for safely copying and moving files.
#
use File::Temp;

#
# Tools for getting the current working directory.
#
use Cwd;

#
# Module for tracking elapsed time.
#
use Time::HiRes;

#
# Needed for ceiling function.
#
use POSIX qw(ceil pause);


#
# Include the needed JGI modules.
#
use M_Constants;



#####################################################################
# Exported Functions                                                #
#####################################################################

=head1 Exported Functions

=cut

### Exported function header template ###

=head2 
 
 Title    : 
 Function : 
 Usage    : 
 Args     : 
 Returns  : 
 Comments : 

=cut







=head2 create_checkpoint

 Title    : create_checkpoint
 Function : This function creates a checkpoint file, in the specified 
            checkpoint files subdirectory, containing a timestamp for
            when it was created, as well as information about the script
            that created it.  If the checkpoint files subdirectory doesn't
            already exist, this function creates it.
 Usage    : create_checkpoint ( $checkpoint, \$checkpoint_dir, $log )
 Args     : 1) A string describing the checkpoint that should be
               created.
            2) The directory in which the checkpoint file should
               be created.
            3) A reference to a logging object.  
 Returns  : JGI_SUCCESS: The checkpoint file was successfully created.
            JGI_FAILURE: The checkpoint file could not be created.
 Comments : None.

=cut

sub create_checkpoint
{
    # Please refer to the above function description for details of
    # what these variables are.
    my ($checkpoint_id, $checkpoint_dir, $log) = @_;

    # The name of the to-be-created checkpoint file.
    my $checkpoint_file;

    # A timestamp for the checkpoint file.
    my $timestamp;

    # A string to identify this function.
    my $function_id = "create_checkpoint";


    
    #
    # Initialize sundry variables.
    #
    $timestamp = create_timestamp();
    $checkpoint_file = "${checkpoint_dir}/${checkpoint_id}" .
	QC_CHECKPOINT_SUFFIX;

    #
    # If the checkpoint files directory doesn't exist, create it.
    #
    if ( ! -d $checkpoint_dir)
    {
	if (system("mkdir $checkpoint_dir"))
	{
	    $log->error("Unable to create the checkpoint " .
				       "files directory!\n" .
				       "Directory:\t$checkpoint_dir");
	    return JGI_FAILURE;
	}
    }

    #
    # Open the checkpoint file for reading.
    #
    if (!open(CHECKPOINT,">$checkpoint_file"))
    {
	$log->error("Unable to open the checkpoint file for " .
	                   "writing!\n" . "File:\t$checkpoint_file");
	return JGI_FAILURE;
    }

    #
    # Record some useful information in the checkpoint file.
    #
    print CHECKPOINT "Checkpoint file for $checkpoint_id\n";
    print CHECKPOINT "Created by $SCRIPT_ID ($VERSION_ID), $function_id\n";
    print CHECKPOINT "Timestamp:\t$timestamp\n";

    #
    # Close the checkpoint file.
    #
    if (!close(CHECKPOINT))
    {
	$log->error("Unable to close the checkpoint file!\n" .
	                   "File:\t$checkpoint_file");
	return JGI_FAILURE;
    }
 
    #
    # If one gets here, everything must have worked, so return
    # success.
    #
    return JGI_SUCCESS;
} # End of the create_checkpoint function.



=head2 create_dir_structure

 Title    : create_dir_structure
 Function : Creates a directory structure where integer-indexed 
            subdirectories are created under the specified top-level
            directory, starting at an index of 0.
 Usage    : create_dir_structure ( $top_level_dir, $dir_total, $log )
 Args     : 1) A string specifying the top-level directory under which
               the partitioned subdiretories will be created.
            2) The number of subdirectories to create.
            3) A reference to a JGI_Log object.
 Returns  : JGI_SUCCESS: The directories were successfully created.
            JGI_FAILURE: The directories could not be created.
 Comments : None.

=cut

sub create_dir_structure 
{
    # Please refer to the above function description for details of
    # what these variables are.
    my ($work_dir, $number_of_dir, $log) = @_;

    # A string to identify this function.
    my $function_id = "create_dir_structure";
    

    #
    # Make sure that the top-level directory exists.
    #
    if (! -d $work_dir) 
    {
	$log->error("Directory does not exist! ($work_dir)");
	return JGI_FAILURE; 
    }
    
    my $i;
    for ($i=0; $i<$number_of_dir; $i++)
    {
	if (! -d "$work_dir/$i")  { qx "mkdir $work_dir/$i"; }
    }

    #
    # If one gets here, everything must have worked, so return success.
    #
    return JGI_SUCCESS;
} # End of the create_dir_structure function.




=head2 create_file_dir_structure

 Title    : create_file_dir_structure
 Function : Generate a list of filenames partitioned into subdirectories,
            and can also create the directory structure into which they'll
            be placed (into integer-indexed subdirectories with a start
            index of 0.
 Usage    : create_file_dir_structure ( )
 Args     : 1) A string specifying the top-level directory under which the 
               partitioned subdiretories will be created.
            2) The number of files to create.
            3) A flag to indicate whether the (empty) directories should
               be created.
            4) [Optional] The number of files to create per subdirectory.
               If undefined, the default value of 100 will be used.
            5) [Optional] The prefix to use for the file names.  If 
               undefined, the file names will start with the file index.
            6) [Optional] The suffix to use for the file names.  If
               undefined, the file names will end with the file index.
            7) A reference to an array in which the file names will be
               returned, with full path information prepended.
            8) A reference to a JGI_Log object
 Returns  : JGI_SUCCESS: The directories were successfully created.
            JGI_FAILURE: The directories could not be created.
 Comments : None.

=cut

sub create_file_dir_structure 
{
    # Please refer to the above function description for details of
    # what these variables are.
    my ($work_dir, $total_number_files, $should_create_dir, 
	$number_files_per_subdir, $filename_prefix, $filename_suffix, 
	$filenames_array_ref, $log) = @_;

    # A string to represent this function.
    my $function_id = "create_file_dir_structure";

    
    #
    # Make sure that the top-level directory exists.
    #
    if (! -d $work_dir) 
    {
	if (system("mkdir -p $work_dir"))
	{
	    $log->error("Unable to create the top-level directory! " .
			"($work_dir)");
	    return JGI_FAILURE; 
	}
    }

    #
    # default to 100 if files per subdirectory is not specified
    #
    if (! defined($number_files_per_subdir))
    {
	$number_files_per_subdir = 100;
    }
    my $number_of_dir = ceil($total_number_files / $number_files_per_subdir);

    # create the directory structure
    if ($should_create_dir) 
    { 
	if (create_dir_structure($work_dir, $number_of_dir, $log) 
	    == JGI_FAILURE) 
	{ 
	    return JGI_FAILURE;
	}
    }
    
    my $file_id = 0;
    my ($i,$j);
    for ($i=0; $i<$number_of_dir; $i++)
    {
	for ($j=0; $j<$number_files_per_subdir; $j++) 
	{
	    my $filename = "$work_dir/$i/";
	    if (defined($filename_prefix))
	    {
		$filename .= $filename_prefix;
	    }
	    $filename .= $file_id;
	    if (defined($filename_suffix))
	    {
		$filename .= $filename_suffix;
	    }
	    $$filenames_array_ref[$file_id] = $filename;
	    $file_id++;
	    if ($file_id >= $total_number_files) 
	    { 
		last;
	    }
	}
    }

    #
    # If one gets here, everything must have worked, so return success.
    #
    return JGI_SUCCESS;
} # End of the create_file_dir_structure funciton.



=head2 create_timestamp

 This function returns the current time as a string with the 
 following format:

 <year><month><day><hour><minute><second>

 The result is suitable for use in creating a uniquely named
 log file.


 This function returns the following values:
                    
 A timestamp 

=cut

sub create_timestamp
{
    # The number of seconds since 1/1/1970.
    my $total_seconds;

    # Variables to hold the results of converting the total number
    # of seconds to year, month, etc. format.
    my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst);

    # The final formatted timestamp string.
    my $timestamp;


    #
    # Get the total number of seconds since 1/1/1970.
    #
    $total_seconds = time();

    #
    # Convert the total number of seconds into more usable items.
    #
    ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) =
	localtime($total_seconds);

    #
    # Make the following changes to the various fields:
    # 
    # - Correct the month by adding 1 to it.
    #
    # - Correct the year by adding 1,900 to it.
    #
    # - Add leading zeroes to the following fields, if any of them
    # consist of single digits: the month, the day, the hour, the
    # minute, and the second.
    #
    $mon++;
    $year += 1900;

    if ($mon < 10)
    {
	$mon = "0$mon";
    }
    if ($mday < 10)
    {
	$mday = "0$mday";
    }
    if ($hour < 10)
    {
	$hour = "0$hour";
    }
    if ($min < 10)
    {
	$min = "0$min";
    }
    if ($sec < 10)
    {
	$sec = "0$sec";
    }

    #
    # Construct the final timestamp string and return it.
    #
    $timestamp = "${year}${mon}${mday}${hour}${min}${sec}";
    return $timestamp;
} # End of the create_timestamp function.



=head2 determine_checkpoint

 Title    : determine_checkpoint
 Function : This function determines the checkpoint at which a
            script should resume its calculations.
 Usage    : determine_checkpoint ( \@checkpoint_list, $checkpoint_dir,
                                   \$checkpoint, $log )
 Args     : 1) A reference to an array containing the ordered list
               of checkpoints for the script.
            2) The directory containing the checkpoint files.
            3) A reference to the variable that will store the
               checkpoint label.
            4) A reference to a logging object.
 Returns  : JGI_SUCCESS: The checkpoint was successfully determined.
            JGI_FAILURE: The checkpoint could not be determined.
 Comments : None.

=cut

sub determine_checkpoint
{
    # Please refer to the above function description for details of
    # what these variables are.
    my ($checkpoint_list_ref, $checkpoint_dir, $checkpoint_ref, 
        $log) = @_;

    # An integer for indexing the list of checkpoints.
    my $checkpoint_index;

    # The current checkpoint file to look for.
    my $checkpoint_file;
    
    # A string to identify this function.
    my $function_id = "determine_checkpoint";


    #
    # Initialize sundry variables.  Note that one starts at the next
    # to last checkpoint, as the final "checkpoint" should be just
    # a mechanism for skipping directly to the end of the script,
    # without performing any calculations.
    #
    $checkpoint_index = $#$checkpoint_list_ref - 1;

    #
    # Cycle through the set of possible checkpoint files until one
    # encounters one furthest along that exists.  The desired starting
    # point is then the checkpoint immediately after that one.
    #
    while ($checkpoint_index >= 0)
    {
        $checkpoint_file = "${checkpoint_dir}/" .
            $$checkpoint_list_ref[$checkpoint_index] . QC_CHECKPOINT_SUFFIX;

        #
        # If the current checkpoint file exists, set the starting
        # checkpoint to the one immediately after it and return
        # success.
        #
        if ( -f $checkpoint_file)
        {
            $$checkpoint_ref = $$checkpoint_list_ref[$checkpoint_index+1];
            return JGI_SUCCESS;
        }

	#
	# Continue down the list of possible checkpoints.
	#
	$checkpoint_index--;
    }

    #
    # If one gets here, then the calling script hasn't done anything
    # yet.  So set the starting point to its initial checkpoint, which 
    # should be the first one in its list of checkpoints.  Then return 
    # success.
    #
    $$checkpoint_ref = $$checkpoint_list_ref[0];
    return JGI_SUCCESS;
} # End of the determine_checkpoint function.



=head2 determine_checkpoint_index

 Title    : determine_checkpoint_index
 Function : This function determines the checkpoint at which
            a script should resume its calculations.  Unlike the
            determine_checkpoint function, it returns the index
            of the checkpoint in the array, instead of a name
            name string.
 Usage    : determine_checkpoint_index ( \@checkpoint_list,
                                         $checkpoint_dir,
                                         $checkpoint_index, $log )
 Args     : 1) A reference to an array containing the ordered
               list of checkpoints for th escript.
            2) The directory containing the checkpoint files.
            3) A reference to the variable that will store the
               checkpoint index.
            4) A reference to a legacy logging object.
 Returns  : JGI_SUCCESS: The checkpoint index was successfully
                         determined.
            JGI_FAILURE: The checkpoint index could not be
                         successfully determined.
 Comments : None.

=cut

sub determine_checkpoint_index
{
    # Please refer to the above function description for details of
    # what these variables are.
    my ($checkpoint_list_ref, $checkpoint_dir, $checkpoint_index_ref, 
        $log) = @_;

    # An integer for indexing the list of checkpoints.
    my $checkpoint_index;

    # The current checkpoint file to look for.
    my $checkpoint_file;
    
    # A string to identify this function.
    my $function_id = "determine_checkpoint_index";



    #
    # Initialize sundry variables.  Note that one starts at the next
    # to last checkpoint, as the final "checkpoint" should be just
    # a mechanism for skipping directly to the end of the script,
    # without performing any calculations.
    #
    $checkpoint_index = $#$checkpoint_list_ref - 1;

    #
    # Cycle through the set of possible checkpoint files until one
    # encounters one furthest along that exists.  The desired starting
    # point is then the checkpoint immediately after that one.
    #
    while ($checkpoint_index >= 0)
    {
        $checkpoint_file = "${checkpoint_dir}/" .
            $$checkpoint_list_ref[$checkpoint_index] . QC_CHECKPOINT_SUFFIX;

        #
        # If the current checkpoint file exists, set the starting
        # checkpoint to the one immediately after it and return
        # success.
        #
        if ( -f $checkpoint_file)
        {
            $$checkpoint_index_ref = $checkpoint_index;
            return JGI_SUCCESS;
        }

        #
        # Continue down the list of possible checkpoints.
        #
        $checkpoint_index--;
    }

    #
    # If one gets here, then the calling script hasn't done anything
    # yet.  So set the starting point to its initial checkpoint, which 
    # should be the first one in its list of checkpoints.  Then return 
    # success.
    #
    $$checkpoint_index_ref = $checkpoint_index;
    return JGI_SUCCESS;
} # End of the determine_checkpoint_index function.






=head2 run_local_cmd

 Title    : run_local_cmd
 Function : A function for running a single local system command,
            optionally returning its output, while logging the
            command itself.
 Usage    : run_local_cmd( $command, \$return_value, $log, \$command_result )
 Args     : 1) A string containing the to-be-run command.
            2) A reference to a variable that will store the command's
               return value.
            3) [Optional] A reference to a JGI_Log object.  If this variable
               isn't defined, command logging will be skipped.
            4) [Optional] A reference to a variable that will store
               the output of the command.
 Returns  : JGI_SUCCESS: The command was successfully run.
            JGI_FAILURE: The command could not be run.
 Comments : Any stdout/stderr redirection needs to be bolted onto the
            command string itself, instead of passed in separately.
            The return value corresponds to the $? variable.  To generate
            the actual exit value, one needs to divide by 256.

=cut

sub run_local_cmd
{
    # Please refer to the above function description for details of
    # what these variables are.
    my ($command, $return_value_ref, $log, $command_output_ref) = @_;

    # A given log message.
    my $message;

    # A string to identify this function.
    my $function_id = "run_local_cmd";
    

    #
    # Clear out any pre-existing contents of the return value.
    #
    undef($$return_value_ref);

    #
    # Log the command in the relevant file.
    #
    $log->debug("Local command:\t$command");

    #
    # The method of running the specified command will depend on
    # whether the user wants the command's output back.
    #
    if ($#_ == 3)
    {
      
	#
	# If the function was called with 4 arguments, one needs
	# to return the output of the command.
	#
	$$command_output_ref = `$command`;
	
	#
	# If the command failed, generate an error.
	#
	if ($$return_value_ref = $?)
	{
	    $message = "Command failed! Return value: " .
		"($$return_value_ref) Command: ($command)\n" .
		"For more clues on what went wrong please examine the output and stderr files produced by this command!";
	    $log->error($message);
	    return JGI_FAILURE;
	}

	#
	# Remove any leading or trailing whitespace from the result.
	#
	std_entry_processing($command_output_ref);
    }
    else
    {
	#
	# The function was called without a variable to store the
	# command results, so just verify that the command ran
	# successfully.
	#
	if ($$return_value_ref = system("$command"))
	{
	    $message = "Command failed! Return value: " .
		"($$return_value_ref) Command: ($command)\n" .
		"For more clues on what went wrong please examine the output and stderr files produced by this command!";
	    $log->error($message);
	    return JGI_FAILURE;
	}
    }

    #
    # If one gets here, everything must have worked, so return
    # success.
    #
    return JGI_SUCCESS;
} # End of the run_local_cmd function.



=head2 std_entry_processing
 
 Title    : std_entry_processing
 Function : Removes leading and trailing whitespace from the passed-in
            string.
 Usage    : std_entry_processing( \$string )
 Args     : 1) A reference to the scalar string that will be edited.
 Returns  : Nothing.
 Comments : None.

=cut

sub std_entry_processing
{
    # A reference to the to-be-edited string.
    my ($entry_ref) = @_;

    # A string to identify this function.
    my $function_id = "std_entry_processing";

    $$entry_ref =~ s/^\s+//;
    $$entry_ref =~ s/\s+$//;
} # End of the std_entry_processing function.



=head2  get_fastq_info
 
 Title    :   get_fastq_info
 Function :   Compile stats on a fastq file.   We assume that fastq reads have been validated prior to this
 Usage    :   get_fastq_info( $fh, $qOffset )
 Args     :   1) a filehandle to a fastq file
              2) q-score encoding offset (normally 33 or 64)
 Returns  :   \%returnVal  - a reference to a hash of results:

              $returnVals{ "numBases" }
              $returnVals{ "numReads" }
              $returnVals{ "qMin" }
              $returnVals{ "qMax" }
              $returnVals{ "numQ20Bases" }
              $returnVals{ "qAvg" }

 Comments :   None

=cut
sub get_fastq_info
{
   my ( $fh, $offset, $log ) = @_;
   my %returnVals = ();

   my $numReads = 0;
   my $numBases = 0;
   my $qMax = -1;
   my $qMin = -1;
   my $numQ20Bases = 0;
   my $qRunningAverage = 0;

   while(  my $header = <$fh>, my $seq = <$fh>, my $foo = <$fh>, my $qual = <$fh> )
   {
       chomp $qual;
       my $qSumForRead = 0;

       my $readLen = length( $qual );
       if (! $readLen) { 
	   $log->error("Error: Could not determine read length ($header)");
	   return JGI_FAILURE;
       }
       for( my $i=0; $i< $readLen; $i++ )
       {
	   my $q = int( ord( substr( $qual, $i, 1 ) ) - $offset );
	   $qSumForRead += $q;
	   if ( $qMin == -1 || $q < $qMin ) { $qMin = $q }
	   if ( $qMax == -1 || $q > $qMax ) { $qMax = $q }
	   if ( $q >= 20 ) { $numQ20Bases++ }
       }
       
       # tally
       my $qReadAverage = $qSumForRead / $readLen;
       $numReads++;
       $numBases += $readLen;
       # due to overflow issues, we compute the average like this
       $qRunningAverage =  $qRunningAverage * ( $numBases - $readLen ) / ( $numBases ) + $qReadAverage * ( $readLen / $numBases );
   }

   $returnVals{ "numBases" } = $numBases;
   $returnVals{ "numReads" } = $numReads;
   $returnVals{ "qMin" }  = $qMin;
   $returnVals{ "qMax" }  = $qMax;
   $returnVals{ "numQ20Bases" } = $numQ20Bases;
   $returnVals{ "qAvg" } = $qRunningAverage;

   return( \%returnVals );
}



=head2    getFastaSizeInfo
 
 Title    : getFastaSizeInfo
 Function : Compile stats on a fasta file
 Usage    : getFastaSizeInfo( $file, $log )
 Args     : 1) a fasta sequence file
            2) a reference to the logging object
            3) genome size for NG50 calculation (optional) 
 Returns  : \%results   -  a reference to a hash of results
 Comments : None

=cut
sub getFastaSizeInfo
{
  my ( $file, $log, $genome_size ) = @_;

  my %results = ();

  # executables
  my $n50          =  $ENV{'MERACULOUS_ROOT'}.'/'.PATH_N50;
  
  my $cnt = `grep -c ">" $file`;
  chomp $cnt;

  my ($totalBases, $minSize, $maxSize) = (0,0,0);

  my $fastaLengthsHashRef = fasta_lengths($file, $log);
  $minSize = $$fastaLengthsHashRef{ ( keys %$fastaLengthsHashRef )[0] };
  for ( keys %$fastaLengthsHashRef )
  {
      $totalBases += $$fastaLengthsHashRef{$_};
      if ($$fastaLengthsHashRef{$_} > $maxSize) { $maxSize = $$fastaLengthsHashRef{$_}; }
      if ($$fastaLengthsHashRef{$_} < $minSize) { $minSize = $$fastaLengthsHashRef{$_}; }
      
  }

  my @N50 = split( /\t/, `perl $n50 -G $genome_size $file 0 | perl -ane 'if ( \$F[4] > 0.5 ) { print \$F[0]."\t".\$F[2]."\t".\$F[3]."\n"; exit }'` );

  $results{ "cnt" } = $cnt;
  $results{ "totalBases" } = $totalBases;
  $results{ "N50Cnt" } = $N50[ 0 ];
  $results{ "N50Size" } = $N50[ 1 ];
  $results{ "N50TotalBases" } = $N50[ 2 ];
  $results{ "minSize" } = $minSize;
  $results{ "maxSize" } = $maxSize;

  $results{ "NG50Cnt" } = "";
  $results{ "NG50Size" } = "";
  $results{ "NG50TotalBases" } = "";

  if ($genome_size) {
      my @NG50 = split( /\t/, `perl $n50 -G $genome_size $file 0 | perl -ane 'if ( \$F[5] > 0.5 ) { print \$F[0]."\t".\$F[2]."\t".\$F[3]."\n"; exit }'` );
      $results{ "NG50Cnt" } = $NG50[ 0 ];
      $results{ "NG50Size" } = $NG50[ 1 ];
      $results{ "NG50TotalBases" } = $NG50[ 2 ];
  }


  return( \%results );
}


sub fasta_lengths
{
    my $file = shift;
    my $log = shift;
    open(F,$file) || die $log->error("Couldn't open $file");
    
    my $sequence = "";
    my $id = "NO_CURRENT_ID";
    my $n_bases = 0;
    my %lengths;

    while (my $i = <F>) {
        chomp $i;

        if ($i =~ /^>/) {

            if ($id ne "NO_CURRENT_ID") {
                $n_bases = length($sequence);
		$lengths{$id} = $n_bases;
                $sequence = "";
            }

            ($id) = $i =~ /^>(\S+)/;

        } else {

            $sequence .= $i;
        }
    }
    close F;
    $n_bases = length($sequence);
    $lengths{$id} = $n_bases;
    
    return \%lengths;
}

=head2    get_qscore_offset
 
 Title    :   get_qscore_offset
 Function :   Reads the first read in the fastq file, and based on the ASCII values
              in the quality field determines whether Phred-33 or Phred-64 encoding is used

 Usage    :   get_qscore_offset( $file, \$offset, $log )
 Args     :   1) a path to a fastq file
              2) a reference to a variable to store the qscore offset
              3) a reference to the logging object

 Returns  :   JGI_SUCCESS: A valid qscore encoding was detected and asigned to $$offset.
              JGI_FAILURE: A valid qscore encoding could not be determined.

 Comments :   None

=cut
sub get_qscore_offset
{
    my $file = shift;
    my $qOffsetRef = shift;
    my $log = shift;

    $$qOffsetRef = test_qualities($file, $log) // return JGI_FAILURE;

    $log->debug("Q-score encoding offset detected as $$qOffsetRef");    
    return JGI_SUCCESS;

    sub iterator{
	my $handle = shift // die $!;
	my %return;

	return sub{ ## actual iterator
	    my %return;
	    $return{'head'} = readline($handle) // return; #if the next line exists , get it otherwise return null
	    $return{'seq'} = readline($handle);
	    $return{'head2'} = readline($handle);
	    $return{'quals'} = readline($handle);
	    map {chomp $return{$_}} keys %return;
	    return \%return;
	};
    }

    sub test_qualities{
	my $file = shift;
	my $log = shift;
	my $TEST;

	if (basename($file) =~ /[\.gz|\.z]$/) {
	    open ($TEST,  "gunzip -c $file |") || die $log->error("Can't open compressed file $file: $!");
	}
	elsif (basename($file) =~ /\.bz2$/) {
	    open ($TEST,  "bunzip2 -c $file |") || die $log->error("Can't open compressed file $file: $!");
	}
	else {
	    open ($TEST, '<' , $file) || die $log->error("Can't open file $file: $!");
	}

	my $iterator = iterator($TEST);
	# scan through quals until we find one that identifies one encoding method and rules out the other
	while (my $record = $iterator->()){
	    map {if ($_ > 75) {return 64} elsif ($_ < 66) {return 33}} unpack("W*" , $record->{'quals'});
	}
	close $TEST;
    }
}



=head2 mate_name

  Title: mate_name
  Function:  Infers the name of the input read's pair, given a known naming convention
  Args:      1) read name (typically a fastq header line)
             2) a reference to the logging object
 
  Returns:   Name of the input read's pair 
=cut

sub mate_name
{
    my $read = shift;
    my $log = shift;
    my $err;
    my $mateDirection;
    my $mateName;

    my ($format, $prefix, $direction, $index)  = parse_fastq_header($read);

    $mateDirection = ( $direction eq '1' ? '2' : '1' );

    if ( ($format eq "ILMN-1.5") || ($format eq "generic-paired") )
    {
	$mateName = $prefix.$index."/".$mateDirection;
    }
    elsif ($format eq "ILMN-1.8") 
    {
	$mateName = $prefix." ".$mateDirection.$index;
    }
    elsif ($format eq "HA")
    {
	$mateName = $prefix.$index."-R".$mateDirection;
    }
    else
    {
	$err = "Error: Could not determine mate name!";
	if (defined $log) { $log->error($err) }
	else { print STDERR "$err\n" }
	die;
    }

    return $mateName;
}



=head2 parse_fastq_header
 Title: parse_fastq_header
 Function : Determines the naming convention of fastq sequences

 Args     : 1) A string containing the fastq header line
            2) [Optional] A reference to a logging object. This is optional bc external scripts call this function without any logging objects

 Returns  : 1)  The naming convention detected
            2)  The "location" portion of the read name  (e.g. FlowcellID:lane:tile:Xcoord:Ycoord)
            3)  The direction of number of the read
            4)  The "index" potion of the name

=cut
sub parse_fastq_header
{
    my $header = shift;
    my $log = shift;
    my $err;
    my ($format, $prefix, $direction, $index);

    #if ($header =~ /^@?(\S+)\s+([12])(\:[YN]\:\d+\:[\sACTGN_0-9+]*).*$/)  # illumina 1.8 naming
    if ( $header =~ /^@?(\S+)\s+([12])(\:[YN]\:\d+)(\:[\sACTGN_0-9+])*.*$/)  # illumina 1.8 naming w optional index sequence
    {
	$format = "ILMN-1.8"; $prefix = $1; $direction = $2; $index = $3 . $4;
    }
    elsif ( $header =~ /^@?(\S+)(\#?[ACTGNactgn0]*)\/([12])$/)   # illumina 1.5 naming
    {
	$format = "ILMN-1.5"; $prefix = $1; $direction = $3; $index = $2; 
    }
    elsif ( $header =~ /^@?(\S+)\-R([12])/ )  #HudsonAlpha naming
    {
	$format = "HA"; $prefix = $1; $direction = $2; $index="";
    }
    elsif ( $header =~ /^@?(\S+)\/([12])/ )  # generic header ending with /1 or /2
    {
	$format = "generic-paired"; $prefix = $1; $direction = $2; $index="";
    }
    else
    {
	$err = "Error: Could not recognize fastq header naming convention!";
	if (defined $log) { $log->error($err) }
	else { print STDERR "$err\n" }
	return undef;	
    }

    return ($format, $prefix, $direction, $index);
   
}

=head2 get_common_taxa_level
 Title: get_common_taxa_level
 Function :

 Args     : 1) 1st string of semicolon-separated taxonomy, e.g. "Bacteria;Firmicutes;Clostridia;Clostridiales;Peptococcaceae;Desulfotomaculum" 
            2) 2nd string to compare with  
            3) [Optional] A reference to a logging object. This is optional bc external scripts call this function without any logging objects

 Returns  : 1) Highest common taxonomy level  (int)

=cut
sub get_common_taxa_level
{
    my $taxaStr1 = shift;
    my $taxaStr2 = shift;
    my $log = shift;
    
    my @t1 = split(';', $taxaStr1);
    my @t2 = split(';', $taxaStr2);

    unless (@t1 && @t2) {
	$log->error("Could not parse input taxonomy string(s)!");
	return JGI_FAILURE;
    }
    my $shortestIdx = ($#t1 > $#t2) ? $#t2 : $#t1;

    my $lastCommon;
    for (my $i = 0; $i<=$shortestIdx; $i++) {
	if ($t1[$i] ne $t2[$i]) {
	    return $lastCommon;
	}
	$lastCommon = $i;
    }
    return $lastCommon;
}



=head2 sum
Function: sum
---------------------------------------
 sums up values in a specified field of a string in an array of strings
 This function takes the following arguments:

 $_[0]     a reference to an array of strings
 $_[1]     an integer specifying the column to do the sum on

 This function returns the following values:

 $tot      sum of the input values 
=cut

sub sum
{ 
    my $listRef = shift;
    my $col = shift;
    my $tot=0;
    foreach my $i (@$listRef) {
	chomp $i;
	my @cols = split(/\s+/,$i);
	$tot += $cols[$col];
    }
    return $tot;
}




######################################################################
# Local Functions                                                   #
#####################################################################

#
# Define local functions here, this is an example comment bloc.
#
# Add comment blocks as with exported functions if they should be
# visibly documented.  If not, include the same information with
# '#' comments
#

=head1 Local Functions

=cut

#
# End of module, 1 for true so perl knows it loaded the module succesfully
#

1;

