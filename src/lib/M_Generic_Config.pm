#
# M_Generic_Config.pm
#
# This class takes in a hash of key/rule information for parsing a
# configuration file for loading.  The hash itself is a hash of hashes
# where the parent key is the key name to be loaded into the object.
# The child keys are three required mandatory fields with information
# about parsing each key line in a given configuration file. Please see 
# the below exported constants on available key value types.  
#
# Note: The third field CONFIG_KEY_TYPE takes in a reference to an 
# array holding the various value types for a given key entry. This
# array may hold one key value type if there is a key entry contains
# only the key and value.  Else, for key and multiple values, the array
# should include an ordered list of value types.
#
#  
# Example: my %params_hash = ('key'=>{CONFIG_KEY_REQ=>CONFIG_TRUE,
#			              CONFIG_UNIQ_KEY=>CONFIG_FALSE,
#			              CONFIG_KEY_TYPE=>/@value_type_array},
#
#          my @value_type_array = ("TYPE_STRING", "TYPE_POS_INT"); 
#                            
#
#
# Initial Version:     11/18/2005
# Latest Revision:     4/25/2014
# Author:              Jasmyn Pangilinan, Harris Shapiro, Eugene Goltsman
#




#####################################################################
# Module Header                                                     #
#####################################################################

#
# Some boilerplate statements.
#
package M_Generic_Config;
use Exporter;
@ISA = qw(Exporter);


#
# Insert a line-return separated list of function names in the
# "Functions" portion of the hash.  Note that it is not necessary to
# explicitly export class methods, as the act of blessing the object
# results in it magically knowing what everything is.
# Insert a line-return separated list of the exported constants
# in the "Constants" portion of the hash.
#
%EXPORT_TAGS = (
		Functions => [qw(
				 )],
		Constants => [qw(
				 )]
		);


#
# Some additional boilerplate.
#
Exporter::export_tags('Functions');
Exporter::export_tags('Constants');


#####################################################################
# Included Modules                                                  #
#####################################################################

#
# Enforce good programming style.
#
use strict;
use warnings;

#
# Tools for parsing a full pathname of a file.
#
use File::Basename;

#
# Tools for getting the current working directory.
#
use Cwd;

#
# Use other Meraculous modules.
#
use M_Constants;
use M_Constants qw(:Gen_config);
use M_Utility;


#####################################################################
# Exported Constant Definitions                                     #
#####################################################################

#
# A constant to refer to the soon-to-be defined key_rule hash
# that will be stored in this object.
# As it turns out, this one isn't actually exported....
#
use constant DEF_KEY_RULES => "DEF_KEY_RULES";

#
# Each such constant then needs to be added to the value of the
# "Constants" key in the %EXPORT_TAGS hash above.



#####################################################################
# Local Constant Definitions                                        #
#####################################################################

# Include here any local-use (global) constant definitions.

# A string to identify this module.
my $SCRIPT_ID = "M_Generic_Config";

# A string to identify this module's version.
my $VERSION_ID = "Version 4/25/2014";

# A string to identify this module's author.
my $AUTHOR_ID = "Jasmyn Pangilinan, Harris Shapiro, Eugene Goltsman";

# Since Perl classes are almost always implemented as hashes,
# this is usually a good place to define a set of constants to represent
# the hash's fields.
# Since it's always a good idea to include versioning information,
# that field is listed as an example.  This one is used in the
# example implementation of the "new" (constructor) method below.

my $OBJECT_VERSION = "VERSION";

# A hash to store the acceptable types of hash keys.
my %HASH_KEYS = (
		 CONFIG_KEY_TYPE => "string",
		 CONFIG_KEY_REQ => "boolean",
		 CONFIG_UNIQ_KEY => "boolean"
		 );

# A hash to store the type of key rules.
my %KEY_RULE_TYPES = (
		      (CONFIG_TYPE_STRING) => "string",
		      (CONFIG_TYPE_INT) => "int",
		      (CONFIG_TYPE_POS_INT) => "pos_int",
		      (CONFIG_TYPE_FLOAT) => "float",
		      (CONFIG_TYPE_FILE) => "file",
		      (CONFIG_TYPE_DIR) => "dir",
		      (CONFIG_TYPE_DATE) => "date",
		      (CONFIG_TYPE_LIST) => "list",
		      (CONFIG_TYPE_INT_LIST) => "int_list",
		      (CONFIG_TYPE_POS_INT_LIST) => "pos_int_list",
		      (CONFIG_TYPE_FLOAT_LIST) => "float_list"
		      );

my $CONFIG = "CONFIG_FILE";


#####################################################################
# Class Methods                                                     #
#####################################################################

# Insert the definitions of the class' methods here.  Outlines of the
# required constructor and destructor methods are provided, as well
# as a method for returning the version of the object and printing
# its contents.CONFIG_

#
# Method: new
# -----------
# The method used to create new instances of a <My Class> object.
# This method takes the following arguments:
#
# $_[0]     Either the class name of the to-be-created object,
#           or a reference to a pre-existing object of the
#           to-be-created one.
#
# $_[1]     The definition file to create the rules hash
#
# $_[2]     The logging object
#
# $_[3]     The configuration file whose content will be store by the object.
# 
#
sub new
{
    # Please refer to the above method description for details of
    # what these variables are.
    my($type, $def_file, $log, $config_file) = @_;

    # The class name for the current object.
    my $class;

    # A reference to the to-be-created object.
    my $this;

    # A string to identify this method.
    my $function_id = "new";


    #
    # Initialize the object as an empty hash.
    #
    $this = {};

    #
    # Determine the class name for the current object.
    #
    $class = ref($type) || $type;

    #
    # bless the (currently empty) hash into the class.
    #
    bless($this, $class);

    #
    # Store a version string to identify what produced this
    # object.
    #
    $this->{$OBJECT_VERSION} = "$SCRIPT_ID ($VERSION_ID)";


    #
    # If the user provided a configuration file, 
    # load in the configuration key rules, then load in the 
    # contents of the configuration file.
#    if ((defined($config_file)) && (defined($log)) && 
#	(defined($def_file)))
    if ($this->load_file($def_file, $log, $config_file)
	!= JGI_SUCCESS)
    {
	if (defined($log))
	{
	    $log->error("Unable to load in the configuration file!");
	}
	return undef();
    }

    #
    # If one gets here, everything must have worked, so return
    # a reference to the newly-minted object.
    #
    return $this;
}



#
# Method: version
# ---------------
# A method for retrieving the version information for the
# current object.
# This method takes the following arguments:
#
# $_[0]     A reference to the object on which the method is being
#           called.
#
# This method returns the version string for the object for which
# it's called.
#
sub version
{
    # Please refer to the above method description for details of
    # what these variables are.
    my ($this) = @_;


    #
    # Return the version string for the object.
    #
    return $this->{$OBJECT_VERSION};
}



#
# Method: print
# -------------
# This method prints the contents of the specified <My Class>
# object.
# This method takes the following arguments:
#
# $_[0]     A reference to the object whose contents will be printed.
#
# $_[1]     If present, a reference to a logging object to which to
#           write the object's contents.  If this argument isn't 
#           specified, the contents will be printed to stdout.
#
# This method returns the following values:
#
# JGI_SUCCESS     The object's contents were successfully printed.
#
# JGI_FAILURE     The object's contents could not be printed.
#
sub print
{
    # Please refer to the above method description for details of
    # what these variables are.
    my ($this, $log) = @_;

    # The formatted string that will be printed.
    my $output_string;

    # A given set of key/value hash entries.
    my ($key, $value);


    # A string to identify this method.
    my $function_id = "print";


    #
    # Initialize sundry variables.
    #
    $output_string = "Object Contents:\n" .
	"Produced by $SCRIPT_ID ($VERSION_ID), $function_id\n";

    #
    # Attach the various parameter values to the output string.
    #
    while (($key, $value) = each (%{$this}))
    {
	#if the value is a hash ref, print the contents
	if (ref($value) eq 'HASH') { 
	    $output_string .= "\t$key:\t$value->{$_}\n" for keys %{$value};
	}
	else
	{
	    $output_string .= "\t$key:\t$value\n";
	}
    }

    #
    # If the user provided a logging object, write the final output
    # string to its results file.  Otherwise, print the output
    # string to stdout.
    #
    if (defined($log))
    {
	$log->info("$output_string");
    }
    else
    {
	print "$output_string\n";
    }

    #
    # If one gets here, everything must have worked, so return
    # success.
    #
    return JGI_SUCCESS;
}



#
# Method: DESTROY
# ---------------
# This method cleans up when a <My Class> object is going away.
# By default, this method doesn't do anything; it's included just
# in case something needs to be added in the future.
# This method takes the following arguments:
#
# $_[0]     A reference to the to-be-destroyed object.
# 
sub DESTROY
{
    # Please refer to the above method description for details of
    # what these variables are.
    my ($this) = @_;

    # A string to identify this function.
    my $function_id = "DESTROY";


    # Nothing to do here, yet.
}



#####################################################################
# Local Functions                                                   #
#####################################################################

#
# Method: load_file
# -----------------
# This method reads in the contents of the specified configuration 
# file, storing the information in the relevant object.  It also 
# does error checking on the rule hash itself to ensure that the
# types of rules are defined by this object.
# This method takes the following arguments:
#
# $_[0]    A reference to the object on which the method is being called.
# 
# $_[1]    The definition file to create the hash
#
# $_[2]    The name of the to-be-loaded configuration file.
#
# $_[3]    The logging object.
#
sub load_file
{
    my($this, $def_file, $log, $config_file) = @_;
    my $function_id = "load_file";
    my %rules_hash;
    my ($config_entry, @config_fields, $keyword);
    my ($value, $value_type);
    my $index;
    my @value_type_array;
    my $config_value;
    my $required_bool;
    my $config_dir;

    # If the configuration definitions file doesn't exist, return error.
    #
    if ((!defined($def_file)) || (! -f $def_file))
    {
	if (defined($log))
	{
	    $log->error("Undefined definition file!");
	}
	return JGI_FAILURE;
    }

    
    #
    # Create the rules hash from the definitions file
    if (create_rules_hash($def_file, $log, \%rules_hash) != JGI_SUCCESS)
    {
	if (defined($log))
	{
	    $log->error("The rules hash could not be created from definitions file!".
	                "File: $def_file");
	}
	return JGI_FAILURE;
    }

    #
    # Make sure that the rules have valid defined types and 
    # the correct number of entries per rule.
    #
    if (check_key_rules(\%rules_hash, $log) != JGI_SUCCESS)
    {
	if (defined($log))
	{
	    $log->error("Configuration rules 'hash' has invalid parameters!");
	}
	return JGI_FAILURE;
    }
    #
    # else save the key rules in object's key_rules hash
    #
    else
    {
	if (load_def_key_rules($this, \%rules_hash, $log) != JGI_SUCCESS)
	{
	    if (defined($log))
	    {
		$log->error("Unable to load key rules hash!");
	    }
	    return JGI_FAILURE;
	}
    }
    

    #
    # If a configuration file has already been specified, return error.
    #
    if (defined($this->{$CONFIG}))
    {
	if (defined($log))
	{
	    $log->error("There is already a configuration file loaded!\n" .
	                "Previous File:\t" . $this->{$CONFIG} . "\n" .
	                "Current File:\t$config_file");
	}
	return JGI_FAILURE;
    }

    #
    # If the configuration file doesn't exist, return error.
    #
    if ((!defined($config_file)) || (! -f $config_file))
    {
	if (defined($log))
	{
	    $log->error("Undefined configuration file! (File:\t$config_file)");
	}
	return JGI_FAILURE;
    }


    #
    # Save the configuration file including path.
    #
    $config_dir = dirname($config_file);
    if ((! defined($config_dir)) || (! -d $config_dir))
    {
        if (defined($log))
        {
            $log->error("Could not get basename for $config_file");
        }
        return JGI_FAILURE;
    }

    
    # If directory is current directory, extract out full directory path
    if ($config_dir eq ".")
    {
	$config_dir = cwd();
	if (! defined($config_dir))
	    {
		if (defined($log))
		{
		    $log->error("Cannot identify current working directory.");
		}
		return JGI_FAILURE;
	    }
        $this->{$CONFIG} = "${config_dir}/${config_file}";
    }
    else
    {
	$this->{$CONFIG} = $config_file;
    }

    #
    # Open the configuration file for reading.
    #
    if (!open(CONFIG_FILE,"<$config_file"))
    {
	if (defined($log))
	{
	    $log->error("Unable to open the configuration file for reading!\n" .
	                "File:\t$config_file");
	}
        return JGI_FAILURE;
    }
    
    #
    # Cycle through the entries in the configuration file,
    # storing each in the configuration object.
    #
    while ($config_entry = <CONFIG_FILE>)
    {
        #
        # Remove any trailing newline characters, as well as any
        # leading or trailing whitespace.
        #
        std_entry_processing(\$config_entry);
 
        #
        # If the current entry is empty or contains a comment,
        # skip it.  Otherwise, split the entry by whitespace and
        # initialize the current entry's keyword.
        #
   
        if (($config_entry =~ /^$/) || 
	    ($config_entry =~ /^#/))
	{
            next;
        }

	@config_fields = split /\s+/, $config_entry;
	$keyword = $config_fields[0];

        #
        # Verify that the keyword exists.
        #
        if (!defined($rules_hash{$keyword}))
        {
	    if (defined($log))
	    {
		$log->error("Unknown keyword: $keyword " . "(File:\t$config_file)");
	    }
            return JGI_FAILURE;
        }
        else
        {
	    my $i = $#{$rules_hash{$keyword}->{CONFIG_KEY_TYPE}} + 1;

	    #
	    # Verify the number of fields for the given keyword.
	    #
        if (($#{$rules_hash{$keyword}->{CONFIG_KEY_TYPE}} + 1) != 
               $#config_fields)
        {
		if (defined($log))
		{
		    $log->error("Mis-formatted configuration file! (File:\t$config_file)\n" .
                        "Entry:\t$config_entry");
		}
                return JGI_FAILURE;
        }
	}

	
	#
	# Compare the keyword values to the defined types. 
	#
	@value_type_array = @{$rules_hash{$keyword}->{CONFIG_KEY_TYPE}};
	$index = 0;
	while ($index < $#value_type_array + 1)
	{
	    $value = $config_fields[$index+1];
	    $value_type = $value_type_array[$index];

	    #
	    # Check that the value is of the proper defined format.
	    #
	    if (check_value_types($value, $value_type, $log) 
		!= JGI_SUCCESS)
	    {
		if (defined($log))
		{
		    $log->error("Value $value has invalid " . "value type!\nKeyword: $keyword");
		}
		return JGI_FAILURE;
	    }
		
	    $index++;
	}

	#
	# Prepare the value to be stored.
	#
	$index = 1;
	$config_value = "";
	while ($index < $#config_fields + 1)
	{
	    $config_value .= "$config_fields[$index] ";
	    $index++;
	}

	#
	# Based on the uniqueness of the keyword, store the values
	# for the keyword in the hash accordingly.
	#
	if ($rules_hash{$keyword}->{CONFIG_UNIQ_KEY})
	{
	    #
	    # Check if key has already been defined.
	    #
	    if (defined($this->{$keyword}))
	    {
		if (defined($log))
		{
		    $log->error("Multiple entries for unique " . "keyword: $keyword\n" . "File:\t$config_file");
		}
                return JGI_FAILURE;
	    }
	    #
	    # else, define the keyword and value
	    #
	    else
	    {
		$this->{$keyword} = $config_value;
	    }
	}
	else
	{
	    # This assumes that for a multi-value entry, the first
	    # field of the multi-value will be unique to be stored
	    # as a hash key, i.e., library name for an insert size.
	    if (defined($this->{$keyword}->{$config_fields[1]}))
	    {
		if (defined($log))
		{
		    $log->error("Multiple entries for " . "keyword/value pair: $keyword " . "$config_fields[1]\n" .
                        "File:\t$config_file");
		}
                return JGI_FAILURE;
	    }
	    #
	    # else, define it.
	    else
	    {
		$this->{$keyword}->{$config_fields[1]} = $config_value;
	    }
	}


    }

    if (!close(CONFIG_FILE))
    {
	    if (defined($log))
	    {
            $log->error("Unable to close the configuration " . "file!\n" . "File:\t$config_file");
	    }
        return JGI_FAILURE;
    }

    #
    # Check that required fields have been found.    
    # For each keyword in the rules hash, check if key has been defined.
    #
    $required_bool = CONFIG_FALSE;
    foreach $keyword (keys %rules_hash)
    {
    if ($rules_hash{$keyword}->{CONFIG_KEY_REQ} eq CONFIG_TRUE)
    {
        if (!defined($this->{$keyword}))
        {
            $required_bool = CONFIG_TRUE;
            if (defined($log))
            {
                $log->error("Required key not defined in " . "configuration file!\n" . "Key:\t$keyword");
            }
        }
    }
    }
    if ($required_bool eq CONFIG_TRUE)
    {
        return JGI_FAILURE;
    }

    return JGI_SUCCESS;
}

#
# Method: check_key_rules
# -----------------------
# This method reads in the contents of the key rules hash
# and stores the various information for the configuration keys
# such as the number columns to expect for each key, if the key is
# unique (i.e, does not have multiple entries), and if it is required.
# This method takes the following arguments:
#
# $_[0]    The reference to the rule hash
#
# $_[1]    The logging object.
#
sub check_key_rules
{
    my ($hash_ref, $log) = @_;

    my $function_id = "check_key_rules";

    my ($hash_keys_ct, $prereq_key_ct);
   
    my ($config_key, $prereq_key);

    my ($value, $array_value);  
    
    # The hash that will store all the various configuration rules.
    my %rules_hash = %{$hash_ref};

    #
    # Check that each hash key has valid child hash keys and 
    # the number of keys is valid of the objects constant defined type.
    #
    foreach $config_key (keys %rules_hash)
    {
    # Check that the number of config hash values matches 
    # the number of keys in the defined HASH_KEYS hash.
    $hash_keys_ct = keys %HASH_KEYS;
    $prereq_key_ct = keys %{$rules_hash{$config_key}};

    if ($hash_keys_ct != $prereq_key_ct)
    {
        if (defined($log))
        {
            $log->error("Number of prereq keys for" . "config key $config_key invalid!");
        }
        return JGI_FAILURE;
    }
       
    while (($prereq_key,$value) = each(%{$rules_hash{$config_key}}))
    {
        #
        # Make sure the prereq_key type exists in the HASH_KEYS hash.
        #
        if (!exists($HASH_KEYS{$prereq_key}))
        {
            if (defined($log))
            {
                $log->error("Hash key type $prereq_key " . "does not exist!");
            }
            return JGI_FAILURE;
        }
	    
        #
        # Make sure that boolean keys use constant config boolean values.
        # 
        if (($prereq_key eq CONFIG_UNIQ_KEY) || 
                ($prereq_key eq CONFIG_KEY_REQ))
        {
            if (!($value eq CONFIG_TRUE || $value eq CONFIG_FALSE))
            {
                if (defined($log))
                {
                    $log->error("Invalid key, value pair:  " . "$prereq_key $value\n" . 
                                "Hash key type $prereq_key " . "must have value of type " .
                                "CONFIG_TRUE or CONFIG_FALSE.");
                }
                return JGI_FAILURE;
            }
        }
	    
        if ($prereq_key eq CONFIG_KEY_TYPE)
        {
            if ($#$value+1 < 1) 
            {
                if (defined($log))
                {
                    $log->error("Number of fields in rule hash " . "must be at " .
                                "least two for keyword and " . "value!");
                }
                return JGI_FAILURE;
            }

            foreach $array_value (@$value)
            {
                if (!exists($KEY_RULE_TYPES{$array_value}))
                {
                    if (defined($log))
                    {
                        $log->error("Key type $array_value " . "does not exist!");
                    }
                    return JGI_FAILURE;
                }
            } #end foreach
        } #endif prereq_key eq CONFIG_KEY_TYPE
    } #while loop key=>value
    } # for each $config_key loop
} 

#
# Method: load_def_key_rules
# --------------------------
#
#
# $_[0]  The reference to the rule hash
#
sub load_def_key_rules
{
    my ($this, $hash_ref, $log) = @_;
    my ($key,$value);

    while (($key, $value) = each(%{$hash_ref}))
    {
        $this->{DEF_KEY_RULES}->{$key} = $value;
    }
}


#
# Method: check_value_types
# -------------------------
# This method takes in a value and given value type and does 
# error checking to ensure that the value is valid for the type
# defined.
#
# $_[0]    The value to be checked
#
# $_[1]    The value type to check
#
# $_[2]    The logging object
#
sub check_value_types
{
    my ($val, $val_type, $log) = @_;
    my $function_id = "check_value_types";
    
    #
    # If type if directory, check for existence.
    #
    if ($val_type eq CONFIG_TYPE_DIR)
    {
        if (! -d $val)
        {
            if (defined($log))
            {
                $log->error("Directory does not exist! Make sure " . "value is absolute path!\n" .
                            "Directory: $val");
            }
            return JGI_FAILURE;
        }
    }
    #
    # Else if type if file, check for existence.
    #
    elsif ($val_type eq CONFIG_TYPE_FILE)
    {
        if (! -e $val)
        {
            if (defined($log))
            {
                $log->error("File does not exist! Make sure value  " . "contains full path of file!\n" .
                            "File: $val");
            }
            return JGI_FAILURE;
        }
    }
    #
    # Else if type if integer..
    #
    elsif ($val_type eq CONFIG_TYPE_INT)
    {	
        if ($val !~ /^-?\d+$/)
        {
            if (defined($log))
            {
                $log->error("Value $val is not of the format " . "TYPE_INT!");
            }
            return JGI_FAILURE;
        }
    }
    #
    # Else if type is positive integer.
    #
    elsif ($val_type eq CONFIG_TYPE_POS_INT)
    {
        if ($val !~ /^\d+$/)
        {
            if (defined($log))
            {
                $log->error("Value $val is not of the format " . "TYPE_POS_INT!");
            }
            return JGI_FAILURE;
        }
    }
    #
    # Else if type is float, check for format 0.0 
    #
    elsif ($val_type eq CONFIG_TYPE_FLOAT)
    {
        if ($val !~ /^-?\d+\.?\d*$/)  #reject .2
        {
            if (defined($log))
            {
                $log->error("Value $val is not of the format " . "TYPE_FLOAT!");
            }
            return JGI_FAILURE;
        }
    }
    #
    # Else if type is date, check for format "YYYYMMDD" or "YYYY-MM-DD"
    #
    elsif ($val_type eq CONFIG_TYPE_DATE)
    {
        if (! (($val =~ /^(\d){8}$/) ||
               ($val =~
                m/(19|20)\d\d-((0[1-9])|10|11|12)-(31|[123]0|[012]?[1-9])/)))
        {
            if (defined($log))
            {
                $log->error("Value $val is not of the format " . "TYPE_DATE!\nMust be \"YYYYMMDD\"" .
                            " or \"YYYY-MM-DD\"");
            }
            return JGI_FAILURE;
        }
    }
    #
    # Else if type is list, check that the string is comma delimited.
    # (Note: pattern cannot start or end with comma)
    #
    elsif ($val_type eq CONFIG_TYPE_LIST)
    {
        if ($val !~ /^([^,]+)(,[^,]+)*$/)  
        {
            if (defined($log))
            {
                $log->error("Value \"$val\" is not of the format " . "TYPE_LIST!");
            }
            return JGI_FAILURE;
        } 
    }
    #
    # Else if type is list, check that it is a comma-delimited 
    # list of integers.
    #
    elsif ($val_type eq CONFIG_TYPE_INT_LIST)
    {
        if ($val !~ /^(-?\d+)(,-?\d+)*$/)  
        {
            if (defined($log))
            {
                $log->error("Value \"$val\" is not of the format " . "TYPE_INT_LIST!");
            }
            return JGI_FAILURE;
        } 
    }
    #
    # Else if type is list, check that it is a comma-delimited
    # list of positive integers.
    #
    elsif ($val_type eq CONFIG_TYPE_POS_INT_LIST)
    {
        if ($val !~ /^(\d+)(,\d+)*$/)  
        {
            if (defined($log))
            {
                $log->error("Value \"$val\" is not of the format " . "TYPE_POS_INT_LIST!");
            }
            return JGI_FAILURE;
        } 
    }
    elsif ($val_type eq CONFIG_TYPE_FLOAT_LIST)
    {
        if ($val !~ /^(-?\d+\.?\d*)(,-?\d+\.?\d*)*$/)  
        {
            if (defined($log))
            {
                $log->error("Value \"$val\" is not of the format " . "TYPE_FLOAT_LIST!");
            }
            return JGI_FAILURE;
        }
    }

    return JGI_SUCCESS;
}


#
# Method: get_key_values
# ----------------------
# This method takes in a key value and 
# and does error checking to ensure that the value is valid for the type
# defined.
#
# $_[0]    A reference to the object on which the method is being called.
#
# $_[1]    The logging object
#
# $_[2]    The key to obtains the corresponding values
#
# $_[3]    [Optional]  Reference to array or variable to store value(s)
#
sub get_key_values
{
    my $function_id = "get_key_values";

    #
    # An array to store the variable list of arguments.
    #
    my @arg_array = @_;

    #
    # The required passed in parameters assigned to corresponding variables.
    #
    my $this = $arg_array[0];
    my $log = $arg_array[1];
    my $key = $arg_array[2];

    #
    # The starting index of the reference variables after the
    # required variables have been assigned.
    #
    my ($arg_ref, $ref_index);

    #
    # The number of defined values for the specified keyword.
    #
    my $values_ct;

    #
    # The number of reference parameters passed to the subroutine.
    #
    my $passed_ref_ct;
    

    my ($value_entry, $value_index, $value_id);
    my $value_field_index;
    my @value_fields;
    
    if (!defined($this->{DEF_KEY_RULES}->{$key}))
    {
        if (defined($log))
        {
            $log->error("Key not defined!\n" . "Key: $key");
        }
        return JGI_FAILURE;
    }

    $values_ct = $#{$this->{DEF_KEY_RULES}->{$key}->{CONFIG_KEY_TYPE}} +1;
    $passed_ref_ct = $#arg_array - 2;

    if ($values_ct != $passed_ref_ct)
    {
        if (defined($log))
        {
            $log->error("Incorrect number of passed-in reference " . "values!\nKey has $values_ct value(s) " .
                        "which must correspond to $values_ct passed-in " . "reference variable(s)\nKey: $key");
        }
        return JGI_FAILURE;
    }

    #
    # If key is unique, return value is a scalar, else return value is
    # reference to an array.
    #
    if ($this->{DEF_KEY_RULES}->{$key}->{CONFIG_UNIQ_KEY} == CONFIG_TRUE)
    {
        $value_entry = $this->{$key};

    # If a value exists, split for multiple value fields
    if ($value_entry)
    {
        @value_fields = split /\s+/, $value_entry;
        $value_index = 0;
        $ref_index = 3;
        while ($value_index < ($#value_fields + 1))
        {
            $arg_ref = $arg_array[$ref_index];
            $$arg_ref = $value_fields[$value_index];
            $value_index++;
            $ref_index++;
        }
    }
    }
    else
    {
        $value_index = 0;
        while (($value_id,$value_entry) = each(%{$this->{$key}}))
        {
            if ($value_entry)
            {
                @value_fields = split /\s+/, $value_entry;
		
                $ref_index = 3;
                $value_field_index = 0;
                while ($value_field_index < ($#value_fields + 1))
                {
                    $arg_ref = $arg_array[$ref_index];
                    $$arg_ref[$value_index] = 
                    $value_fields[$value_field_index];
                    $value_field_index++;
                    $ref_index++
                }
                $value_index++;
            }
        }
    }

    return JGI_SUCCESS;
}


#
# Method: store_key_value
# -----------------------
# This method stores a value, given a key and does 
# error checking to ensure that the value is valid for the type
# defined. 
# Note: For non-unique keys, It does not store multiples values 
# at one time per key
#
#
# $_[0]    A reference to the object on which the method is being called.
#
# $_[1]    The key associated with the value to store
#
# $_[2]    The value to be stored for the given key
#
# $_[3]    The logging object
#
sub store_key_value
{
    my ($this, $key, $value_entry, $log) = @_;
    my $function_id = "store_key_values";
    my (@value_fields, @value_types);
    my ($value_fields_ct, $values_ct);
    my $value_type;
    my $index;

    if (!defined($this->{DEF_KEY_RULES}->{$key}))
    {
        if (defined($log))
        {
            $log->error("Key not defined!\n" . "Key: $key");
        }
        return JGI_FAILURE;
    }

    # If the value entry is a multiple-value entry, store each in array.
    @value_fields = split /\s+/, $value_entry;

    # Store the number of value fields passed in
    $value_fields_ct = $#value_fields + 1;

    # Store the value types in an array
    @value_types = @{$this->{DEF_KEY_RULES}->{$key}->{CONFIG_KEY_TYPE}};

    # Get the number of defined values for the key
    $values_ct = $#{$this->{DEF_KEY_RULES}->{$key}->{CONFIG_KEY_TYPE}} +1;

    if ($values_ct != $value_fields_ct)
    {
        if (defined($log))
        {
            $log->error("Number of value fields not equal" . " to expected number of values!\n" .
                        "Key: $key");
        }
        return JGI_FAILURE;
    }

    # Check that each value corresponds to its value type
    $index = 0;
    while ($index < $#value_fields + 1)
    {
        if (check_value_types($value_fields[$index], 
                        $value_types[$index], $log)
                        != JGI_SUCCESS)
        {
            if (defined($log))
            {
                $log->error("Value $value_fields[$index] " . "has invalid value type!\n" .
                            "Key: $key");
            }
            return JGI_FAILURE;
        }
        $index++;
    }

    #
    # Based on the uniqueness of the keyword, store the values
    # for the keyword in the hash accordingly.
    #
    if ($this->{DEF_KEY_RULES}->{$key}->{CONFIG_UNIQ_KEY})
    {
        $this->{$key} = $value_entry;
    }
    else
    {
        $this->{$key}->{$value_fields[0]} = $value_entry;
    }

   
    return JGI_SUCCESS;
}



#
# Method: put_param
# -----------------------
# Saves a key-value pair in a params object.
#
# This function takes the following arguments:
#
# $_[0]     A reference to a params object
# $_[2]     A string that represents  the key
# $_[3]     A string that represents the value
# $_[4]     A reference to the logging object
#
# This function returns the following values:
#
# JGI_SUCCESS        The function successfully finished
#
# JGI_FAILURE        Something went wrong
#
sub put_param
{
  my ( $this, $key, $value, $log) = @_;
  if ( $this->store_key_value( $key, $value, $log ) != JGI_SUCCESS )
  {
    $log->error( "Could not set key $key to value $value!", called_from() );
    return JGI_FAILURE;
  }
  return JGI_SUCCESS;
}



#
# Method: get_param
# -----------------------
# Gets the value(s) associated with a key that is optionally passed as an argument.
#
# This function takes the following arguments:
#
# $_[0]     A reference to the object on which the method is being called.
# $_[1]     A string that represents  the key
# $_[2]     A reference to the logging object.
# $_[3]     A reference to the array or variable that will store the value(s)
#
# This function returns the following values:
#
# $return_ref  The reference to the array or variable containing the value(s)
#
sub get_param
{
  my ( $this, $key, $log, $return_ref ) = @_;
  if ( not defined $return_ref )
  {
    $return_ref = \$return_ref;
  }
  if ( $this->get_key_values( $log, $key, $return_ref ) != JGI_SUCCESS )
  {
    die $log->error( "Could not get value associated with the key $key!", called_from() );
  }
  undef $return_ref if $return_ref eq \$return_ref;
  return $return_ref;
}



#
# Method: config_print_all
# ------------------------
# This method overwrites the initial configuration
# file that was passed in with the key values of a hash.  If 
# the optional parameter for the file is given, it will write to 
# this file instead.
#
#
# $_[0]    A reference to the object on which the method is being called.
#
# $_[1]    The logging object
#
# $_[2]    [Optional]  The name of the file to write the config key/values
#
sub config_print_all
{
    my ($this, $log, $config_file) = @_;
    my $function_id = "config_print_all";
    my ($key, $value);
    my ($sub_key, $sub_value);
    my $config_dir;

    if (!($config_file))
    {
        $config_file = $this->{$CONFIG};
    }

    # Assume that the previous config file was stored at 
    # initialization and rename it to have "_old" extension.
    my $old_config = $this->{$CONFIG} . "_old";

    # Open the config file for writing
    if (!open(CONFIG_FILE,">$config_file"))
    {
        if (defined($log))
        {
            $log->error("Unable to open the configuration file for writing!\n" .
                        "File:\t$config_file");
        }
        return JGI_FAILURE;
    }

    while (($key, $value) = each(%{$this}))
    {
        if ($key eq $CONFIG)
        {
            next;
        }
        if ($key eq DEF_KEY_RULES)
        {
            next;
        }
        if ($key eq $OBJECT_VERSION)
        {
            next;
        }

        # Extract the value for unique and non-unique keys
        if ($this->{DEF_KEY_RULES}->{$key}->{CONFIG_UNIQ_KEY} == CONFIG_TRUE)
        {
            #    print "$key\t$value\n";
            print CONFIG_FILE "$key\t$value\n";
        }
        else
        {
            while (($sub_key,$sub_value) = each(%{$this->{$key}}))
            {
                # print "$key\t$sub_value\n";
                print CONFIG_FILE "$key\t$sub_value\n";	    
            }
        }
    }

    if (!close(CONFIG_FILE))
    {
        if (defined($log))
        {
            $log->error("Unable to close the configuration " . "file!\n" .
                        "File:\t$config_file");
        }
        return JGI_FAILURE;
    }

    # Update the config file to point to the newly created config file
    $config_dir = dirname($config_file);
    if ((! defined($config_dir)) || (! -d $config_dir))
    {
        if (defined($log))
        {
            $log->error("Could not get basename for $config_file");
        }
        return JGI_FAILURE;
    }


    # If directory is current directory, extract out full directory path
    if ($config_dir eq ".")
    {
	$config_dir = cwd();
	if (! defined($config_dir))
	    {
		if (defined($log))
		{
		    $log->error("Could not get full path to cwd");
		}
		return JGI_FAILURE;
	    }
        $this->{$CONFIG} = "${config_dir}/${config_file}";
    }
    else
    {
        $this->{$CONFIG} = $config_file;
    }
    
    return JGI_SUCCESS;
}

#
# Method: create_rules_hash
# -------------------------
# This method reads the definitions file and loads the 
# entries into a rules hash.  Only error checking is done
# for the correct number of fields per line.  More error
# checking is done in check_key_rules function
#
sub create_rules_hash
{
    my ($def_file, $log, $rules_hash_ref) = @_;
    my $function_id = "create_rules_hash";
    my $def_entry;
    my @def_fields;
    my @value_types;
    my $values_arrayref;
    my ($keyword, $req_type, $uniq_type);


    if (!open(DEF_FILE,"<$def_file"))
    {
        if (defined($log))
        {
            $log->error( "Unable to open the definition " . "file for reading!\n" . 
                         "File:\t$def_file");
        }
        return JGI_FAILURE;
    }

    #
    # Cycle through the entries in the configuration file,
    # storing each in the configuration object.
    #
    while ($def_entry = <DEF_FILE>)
    {
        #
        # Remove any trailing newline characters, as well as any
        # leading or trailing whitespace.
        #
        std_entry_processing(\$def_entry);

        #
        # If the current entry is empty or contains a comment,
        # skip it.  Otherwise, split the entry by whitespace and
        # initialize the current entry's keyword.
        #

        if (($def_entry =~ /^$/) ||
            ($def_entry =~ /^#/))
        {
            next;
        }

        @def_fields = split /\s+/, $def_entry;
	
        if ($#def_fields != 3)
        {
            if (defined($log))
            {
                $log->error("Incorrect format of rule definition" . "in file!\n" . 
                            "File:\t$def_file");
            }
            return JGI_FAILURE;
        }

        #
        # Fields defined into variables, for clarity purposes
        #
        $keyword = $def_fields[0];

        if ($def_fields[1] eq CONFIG_REQ)
        {
            $req_type = CONFIG_TRUE;
        } 
        elsif ($def_fields[1] eq CONFIG_UNREQ)
        {
            $req_type = CONFIG_FALSE;
        }
 
        if ($def_fields[2] eq CONFIG_UNIQ)
        {
            $uniq_type = CONFIG_TRUE;
        } 
        elsif ($def_fields[2] eq CONFIG_NONUNIQ)
        {
            $uniq_type = CONFIG_FALSE;
        }

        @value_types = split /=/, $def_fields[3];
	
        # Create an anonymous array reference.
        $values_arrayref = [ @value_types ];

        if (defined($rules_hash_ref->{$keyword}))
        {
            if (defined($log))
            {
                $log->error("Rule format has already been defined " . "for key!" .
                            "Key:\t$keyword");
            }
            return JGI_FAILURE;
        }
        else
        {
            $rules_hash_ref->{$keyword} = {
            	                            CONFIG_KEY_REQ=>$req_type,
                                            CONFIG_UNIQ_KEY=>$uniq_type,
                                            CONFIG_KEY_TYPE=>$values_arrayref,
                                          };
        }
    } # End while <DEF_FILE>>

    if (!close(DEF_FILE))
    {
        if (defined($log))
        {
            $log->error("Unable to close the definition " . "file!\n" . 
                        "File:\t$def_file");
        }
        return JGI_FAILURE;
    }

    return JGI_SUCCESS;    
}
 

#
# Method: called_from
# -----------------------
# Returns the name of the function from which a function is called from
#
# Uses perl's caller() function, but also filters out (eval) (which counts as a stack frame)
# ( caller(x) returns info about the x'th stack frame up in the stack, 0 is the current frame)
#
# Takes no arguments
#
sub called_from
{
    my $i           = 2;    # two up in the stack ( 0 is this func, 1 is caller of this)
    my $function_id = "";
  while ( defined($function_id) )
  {
      $function_id = ( caller($i) )[3];    # get name of caller
      last if not defined($function_id);
      last if ( $function_id ne '(eval)' );    # got an actual function, exit loop
      $i++;    # got an eval, try next frame up;
  }

  if ( not defined($function_id) )    # got to the top
  {
      return 'main';
  }

  $function_id =~ /.*::(.*)/;                               # gets rid of module name (CF perhaps we should not?)
    return $1;
}




#
# A seemingly gratuitous statement, included so that the interpreter
# doesn't complain.
#
1;

########################################################################
# Format Definitions                                                   #
########################################################################

# Place any output format specifiers here.


