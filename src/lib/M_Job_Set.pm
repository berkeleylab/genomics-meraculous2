#
# M_Job_Set.pm
#

###################################################################
# POD Block                                                       #
###################################################################

#
# POD Notes:
#
# Spacing can be critical to the correct presentation of the text within
# the POD block.
# Blank lines function as paragraph marks.
# Indentation will produce variable results, and should be tested.
# Use perldoc to view and test your POD.

=head1 NAME

    M_Job_Set.pm

=head1 SYNOPSIS

    A class interface for running sets of jobs.

=head1 DESCRIPTION

    The goal of this class is to facilitate the runnings of sets of
    related jobs, either locally or on a cluster.

=head1 VERSION

    Initial Version: 7/17/2008
    Latest Revision: 04/10/2013

=head1 AUTHORS
    
    Harris Shapiro
    Isaac Ho
    Eugene Goltsman

=cut

#####################################################################
# Class Header                                                      #
#####################################################################

#
# Define class name
#
package M_Job_Set;



#####################################################################
# Inherit from Exporter                                             #
#####################################################################

#
# if your class has something to export, export it here.
#
use base qw( Exporter );

#
# Insert a line-return separated list of function names in the
# "Functions" portion of the hash.
#
# Note that it is not necessary to explicitly export class methods,
# as the act of blessing the object results in it magically knowing
# what everything is.
#
#
# Insert a line-return separated list of the exported constants
# in the "Constants" portion of the hash.
#
our %EXPORT_TAGS =
    (
     Functions => [
		   qw(
                        run_job_set
		   )
     ],
     Constants => [
		   qw( 
                        JOB_SET_SKIP_COMPLETED
                        JOB_SET_RUN_TYPE_LOCAL
                        JOB_SET_RUN_TYPE_CLUSTER
                        JOB_SET_SUBMIT_MISC_OPTIONS
                        JOB_SET_CLUSTER_QUEUE
                        JOB_SET_RUN_DIR
                        JOB_SET_RUN_TYPE
                        JOB_SET_MAX_SUBMIT
                        JOB_SET_MAX_RETRIES
                        JOB_SET_COMMANDS
                        JOB_SET_STDERRS
                        JOB_SET_STDOUTS
                        JOB_SET_EXECUTION_DIRS
                        JOB_SET_LINKED_SCRIPT_TEMPLATE
                        JOB_SET_VALIDATION_FUNCTOR
                        JOB_SET_REMOTE_SENTINEL
                        CLUSTER_DELETE_JOB_EXEC
                      )
     ],

    );

Exporter::export_tags('Functions');
Exporter::export_tags('Constants');

#####################################################################
# Pragma Inclusions                                                 #
#####################################################################

#
# Enforce good programming style.
#
# It is critcal that these pragmas are not included prior to declaration of
# base classes.
# Otherwise, unpredictable behavior will result.
#
use strict;
use warnings;


#####################################################################
# Module Inclusions                                                 #
#####################################################################

#
# Tools for getting the current working directory.
#
use Cwd;

#
# Needed to get the flags for the waitpid function.
#
use POSIX "sys_wait_h";

#
# Needed for high-resolution time functions.
#
use Time::HiRes;

#
# Include Data::Dumper, which provides stringified representations of
# Perl data structures.
#
use Data::Dumper;

#
# Include Carp, which prints stack sibley on exit
#
use Carp qw(carp cluck croak confess);

#
# Needed to create log objects
#
use Log::Log4perl qw(:easy);

#
# Meraculous modules
#
use M_Constants;
use M_Utility;


#
# Exported Constant Definitions
#

use constant {
    JOB_SET_SKIP_COMPLETED => 'skip_completed',
    JOB_SET_RUN_TYPE_LOCAL => 'local',
    JOB_SET_RUN_TYPE_CLUSTER => 'cluster',
    JOB_SET_SUBMIT_MISC_OPTIONS => 'submit_misc_options',
    JOB_SET_CLUSTER_QUEUE => 'cluster_queue',
    JOB_SET_RUN_DIR => "run_dir",
    JOB_SET_RUN_TYPE => 'run_type',
    JOB_SET_MAX_SUBMIT => 'max_submit',
    JOB_SET_MAX_RETRIES => 'max_retries',
    JOB_SET_COMMANDS => 'commands',
    JOB_SET_STDERRS => 'stderrs',
    JOB_SET_STDOUTS => 'stdouts',
    JOB_SET_EXECUTION_DIRS => 'execution_dirs',
    JOB_SET_LINKED_SCRIPT_TEMPLATE => 'linked_script_template',
    JOB_SET_VALIDATION_FUNCTOR => 'validation_functor',
    JOB_SET_REMOTE_SENTINEL => 'remote_job.sentinel',
    CLUSTER_DELETE_JOB_EXEC => (defined $ENV{'SLURM_ROOT'}) ? 'scancel' : 'qdel'
};



#
# Supported local job status values.
#
use constant JOB_SET_STATUS_COMPLETED => "status_completed";
use constant JOB_SET_STATUS_ERROR => "status_error";
use constant JOB_SET_STATUS_FAILED => "status_failed";
use constant JOB_SET_STATUS_ON_HOLD => "status_on_hold";
use constant JOB_SET_STATUS_PENDING => "status_pending";
use constant JOB_SET_STATUS_REQUEUED => "status_requeued";
use constant JOB_SET_STATUS_RUNNING => "status_running";
use constant JOB_SET_STATUS_UNKNOWN => "status_unknown";





# The various default values for the honor existing touchfiles flag.
#
my %JOB_SET_DEFAULT_SKIP_COMPLETED =
    (
     (JOB_SET_RUN_TYPE_LOCAL) => (JGI_FLAG_ON),
     (JOB_SET_RUN_TYPE_CLUSTER) => (JGI_FLAG_ON),
     );

#
# The various default maximum job retry values.
#
my %JOB_SET_DEFAULT_MAX_RETRIES =
    (
     (JOB_SET_RUN_TYPE_LOCAL) => 0,
     (JOB_SET_RUN_TYPE_CLUSTER) => 0,
     );

#
# The various default maximum concurrent job submission values.
#
my %JOB_SET_DEFAULT_MAX_SUBMITS =
    (
     (JOB_SET_RUN_TYPE_LOCAL) => 1,
     (JOB_SET_RUN_TYPE_CLUSTER) => 1,
     );

#
# Assorted default values.
#
my $JOB_SET_DEFAULT_FILE_PREFIX = "job_set.";
my $JOB_SET_DEFAULT_FILES_PER_SUBDIRECTORY = 100;
my $JOB_SET_DONT_RETRY = JGI_FLAG_OFF;

#
# Assorted standard subdirectory names.
#
my $JOB_SET_DEFAULT_SCRIPT_FILES_DIR = "script_files";
my $JOB_SET_DEFAULT_STDERRS_DIR = "stderr_files";
my $JOB_SET_DEFAULT_STDOUTS_DIR = "stdout_files";
my $JOB_SET_DEFAULT_TOUCHFILES_DIR = "touchfiles";
my $JOB_SET_DEFAULT_LINKED_SCRIPTS_DIR = "linked_script_files";


#
# Cluster type-dependent environment variable names
#
my $CLUSTER_TYPE;
my $JOB_ID_ENV_VAR_NAME;  
my $TASK_ID_ENV_VAR_NAME;

if (defined $ENV{'SGE_ROOT'}) {
    $CLUSTER_TYPE = "sge";
    $JOB_ID_ENV_VAR_NAME = "SGE_JOB_ID";  
    $TASK_ID_ENV_VAR_NAME = "SGE_TASK_ID";

}
elsif (defined $ENV{'SLURM_ROOT'}) {
    $CLUSTER_TYPE = "slurm";
    $JOB_ID_ENV_VAR_NAME = "SLURM_ARRAY_JOB_ID";  
    $TASK_ID_ENV_VAR_NAME = "SLURM_ARRAY_TASK_ID";
}

#
# Create a logging object instance
#
my $log = Log::Log4perl->get_logger('main');



#####################################################################
# Local Functions / Methods                                         #
#####################################################################

=head1 Local Functions/Methods

=cut

=head2 run_job_set

 Title    : run_job_set
 Function : This method initializes and validates the parameters relefant to the job set and calls 
            the appropriate run_job_set_* method to execute the jobs
 Usage    : run_job_set( )
 Args     : A hash ref containing the parameter names and values.  
            The keys are defined by the constants in this module (see  above), and
            the values are defined externally.
 Returns  : JGI_SUCCESS: The job set was successfully run.
            JGI_FAILURE: One or more of the jobs in the set could not be run.
 Comments : Please refer to the individual run_job_set_* functions to
            see the requirements for each job set run type.

=cut

sub run_job_set
{
    my $job_params_hash_ref = shift;

    # A flag to indicate that one or more jobs failed.
    my $failed_job_flag;

    # Variables to tracking how long it took to run the job set.
    my ($start_time, $total_time);

    # A string to identify this method.
    my $function_id = "run_job_set";

    # Note the start time.
    $start_time = [Time::HiRes::gettimeofday()];

    # Set values to variables pertaining specifially to this job set. Received as args to the funcion.
    my $job_set_run_dir = $job_params_hash_ref->{ (JOB_SET_RUN_DIR) };
    my $job_set_run_type = $job_params_hash_ref->{ (JOB_SET_RUN_TYPE) };
    my $job_set_max_submit = $job_params_hash_ref->{ (JOB_SET_MAX_SUBMIT) };
    my $job_set_max_retries = $job_params_hash_ref->{ (JOB_SET_MAX_RETRIES) };
    my $job_set_skip_completed = $job_params_hash_ref->{ (JOB_SET_SKIP_COMPLETED) };
    my $job_set_cluster_queue = $job_params_hash_ref->{ (JOB_SET_CLUSTER_QUEUE) };

    my $command_set_ref = $job_params_hash_ref->{ (JOB_SET_COMMANDS) };
    my $stderr_set_ref = $job_params_hash_ref->{ (JOB_SET_STDERRS) };
    my $stdout_set_ref = $job_params_hash_ref->{ (JOB_SET_STDOUTS) };
    my $execution_dir_set_ref = $job_params_hash_ref->{ (JOB_SET_EXECUTION_DIRS) };

    my $linked_script_template = $job_params_hash_ref->{ (JOB_SET_LINKED_SCRIPT_TEMPLATE) };
    my $validation_function_ref = $job_params_hash_ref->{ (JOB_SET_VALIDATION_FUNCTOR) };
    my $job_submit_misc_options = $job_params_hash_ref->{ (JOB_SET_SUBMIT_MISC_OPTIONS) };

    # Initialize output directory vars
    my ($cwd, $stdout_files_dir, $stderr_files_dir, $job_script_files_dir, $linked_script_files_dir,$touchfiles_dir);

    # Inititalize ref to anonymous arrays which will hold the individual job script file names
    my $job_script_set_ref = [];
    my $touchfile_set_ref = [];

    # Inititalize ref to anonymous array which will hold status of each job 
    my $status_set_ref = [];


    #
    # Run the standard set of checks for required and optional parameters,
    # initializing any of the latter that are missing.
    #

    # Assorted variables used to create sets of files and directories.
    my ($create_directories_flag, $files_per_subdirectory, $file_prefix, $file_suffix);

    # Index and counter vars for cycling through arrays
    my ($command_index, $command_total);

    # Initialize sundry variables.
    $command_index = 0;
    $files_per_subdirectory = $JOB_SET_DEFAULT_FILES_PER_SUBDIRECTORY;
    $file_prefix = $JOB_SET_DEFAULT_FILE_PREFIX;



    # If the set of commands is undefined, generate an error.
    if (!defined($command_set_ref))
    {
	    $log->error("Undefined command set!");
	    return JGI_FAILURE;
    }

    # If the command set is empty, generate an error
    if ( @$command_set_ref == 0 )
    {
	    $log->error("Empty command set!");
	    return JGI_FAILURE;
    }

    $command_total = $#$command_set_ref + 1;



    # If the directory to run the job set in isn't defined, set it to cwd
    if (!defined($job_set_run_dir))
    {
	$cwd = cwd();
	if (!defined($cwd))
	{
	    $log->error("Cannot identify current working directory!");
	    return JGI_FAILURE;
	}
	$job_set_run_dir = $cwd;
    }

    # If the maximum concurrent job retries parameter isn't defined, set it
    # to an appropriate default value.
    if (!defined($job_set_max_retries))
    {
	$job_set_max_retries = $JOB_SET_DEFAULT_MAX_RETRIES{$job_set_run_type};
    }

    # If the maximum concurrent job submit parameter isn't defined, set it
    # to an appropriate default value.
    if (!defined($job_set_max_submit))
    {
	$job_set_max_submit =  $JOB_SET_DEFAULT_MAX_SUBMITS{$job_set_run_type};
    }

    # If the skip completed jobs flag isn't specified,
    # set it to an appropriate default value.
    if (!defined($job_set_skip_completed))
    {
	$job_set_skip_completed = JGI_FLAG_ON;
    }

    # If any of the commands don't have a corresponding execution directory defined, default to cwd
    while ($command_index < $command_total)
    {
	# If the current command's execution directory isn't specified,
	# set it to the current working directory.
	if (!defined($execution_dir_set_ref->[$command_index]))
	{
	    $cwd = cwd();
	    if (!defined($cwd))
	    {
		$log->error("Cannot identify current working directory!");
		return JGI_FAILURE;
	    }
	    $execution_dir_set_ref->[$command_index] = $cwd;
	}
	#
	# Initialize the current command's status
	#
	$status_set_ref->[$command_index] = JOB_SET_STATUS_PENDING; 


	#
	# Move on to the next command.
	#
	$command_index++;

    } # End of the command-related parameters preflight check.



    # 
    #
    # If the command stdout files weren't specified, construct them
    # Note that to be really safe, one would need to verify that
    # the number of provided stderr files matched the number of commands,
    # and that no elements of the array were undefined.
    #
    $stdout_files_dir =
	$job_set_run_dir . "/${JOB_SET_DEFAULT_STDOUTS_DIR}";
    $create_directories_flag = JGI_FLAG_ON;
    if (!defined($stdout_set_ref) ) 
    {
	$stdout_set_ref = [];
	$log->debug("Stdout files were not specified. " .
		   "Creating the defaults stdout file directory structure...");
	if (create_file_dir_structure($stdout_files_dir, $command_total,
				      $create_directories_flag,
				      $files_per_subdirectory,
				      $file_prefix, QC_STDOUT_SUFFIX,
				      $stdout_set_ref,
				      $log)
	    != JGI_SUCCESS)
	{
	    $log->error("Unable to create the stdout file " .
			"directory structure!");
	    return JGI_FAILURE;
	}
    }

    #
    # If the command stderr files weren't specified, construct them
    # Note that to be really safe, one would need to verify that
    # the number of provided stderr files matched the number of commands,
    # and that no elements of the array were undefined.
    #
    $stderr_files_dir =
	$job_set_run_dir . "/${JOB_SET_DEFAULT_STDERRS_DIR}";
    $create_directories_flag = JGI_FLAG_ON;
    if (!defined($stderr_set_ref))
    {
	$stderr_set_ref = [];
	$log->debug("Stderr files were not specified. " .
		   "Creating the defaults stderr file directory structure..");
	if (create_file_dir_structure($stderr_files_dir, $command_total,
				      $create_directories_flag,
				      $files_per_subdirectory,
				      $file_prefix, QC_STDERR_SUFFIX,
				      $stderr_set_ref,
				      $log)
	    != JGI_SUCCESS)
	{
	    $log->error("Unable to create the stderr file " .
			"directory structure!");
	    return JGI_FAILURE;
	}
    }


    #
    # A number of command- and run-related parameters are only relevant
    # for cluster jobs.
    #
    if ($job_set_run_type eq JOB_SET_RUN_TYPE_CLUSTER)
    {

	unless (defined $CLUSTER_TYPE) {
	    $log->error("Undefined cluster type!");
            return JGI_FAILURE;
	}

	#
	# Initialize sundry variables.
	#
	$job_script_files_dir =
	    $job_set_run_dir . "/${JOB_SET_DEFAULT_SCRIPT_FILES_DIR}";
	$linked_script_files_dir =
	    $job_set_run_dir . "/${JOB_SET_DEFAULT_LINKED_SCRIPTS_DIR}";
	$touchfiles_dir =
	    $job_set_run_dir . "/${JOB_SET_DEFAULT_TOUCHFILES_DIR}";
	$create_directories_flag = JGI_FLAG_ON;


	#
	# Initialize the set of job script files.
	#

	$log->debug("Creating the job script file file directory structure..");
	if (create_file_dir_structure($job_script_files_dir, $command_total,
				      $create_directories_flag,
				      $files_per_subdirectory,
				      $file_prefix, QC_SCRIPT_SUFFIX,
				      $job_script_set_ref,
				      $log)
	    != JGI_SUCCESS)
	{
	    $log->error("Unable to create the job script file " .
			"directory structure!");
	    return JGI_FAILURE;
	}
	
	#
	# Initialize the set of touchfiles.
	#
	$log->debug("Creating the touchfile file directory structure..");
	if (create_file_dir_structure($touchfiles_dir, $command_total,
				      $create_directories_flag,
				      $files_per_subdirectory,
				      $file_prefix, QC_TOUCHFILE_SUFFIX,
				      $touchfile_set_ref,
				      $log)
	    != JGI_SUCCESS)
	{
	    $log->error("Unable to create the touchfile " .
			"directory structure!");
	    return JGI_FAILURE;
	}
	
	###  Note: Another directory structure that gets created is the linked_script_files_dir,
	###  but since it gets populated and re-populated dynamically depending on what needs 
	###  to be rerun, the creation of this structure happens later (in submit_and_monitor_and_assess_linkfile_jobs)


    } # End of the cluster run type preflight check.


    # Set a reference to point to the appropriate function depending on whether this was specified
    # to be a local or a cluster run
    my $run_job_set_method_ref;
    my @job_set_method_params;
    if ($job_set_run_type eq JOB_SET_RUN_TYPE_CLUSTER)
    {
	$log->debug("job set run type: cluster");
	$run_job_set_method_ref = \&run_job_set_array_cluster;
	@job_set_method_params = (   $log,
		                     $command_set_ref, 
				     $stderr_set_ref, 
				     $stdout_set_ref,
				     $execution_dir_set_ref,
				     $job_script_set_ref, 
				     $touchfile_set_ref,
				     $validation_function_ref,
				     $job_set_max_submit,
				     $job_set_skip_completed,
				     $linked_script_template,
				     $linked_script_files_dir,
				     $job_submit_misc_options   );
    }
    elsif ($job_set_run_type eq JOB_SET_RUN_TYPE_LOCAL)
    {
	$log->debug("job set run type: local");
	$run_job_set_method_ref = \&run_job_set_local;
	@job_set_method_params = (   $log,
		                     $command_set_ref, 
				     $stderr_set_ref, 
				     $stdout_set_ref,
				     $execution_dir_set_ref,
				     $status_set_ref,
				     $job_set_max_submit,
				     $job_set_skip_completed,
				     $validation_function_ref   );
    }
    else
    {
	$log->error("Unsupported job set run type! ($job_set_run_type)");
	return JGI_FAILURE;
    }




    #
    # Call the job set run method, until either all of the jobs succeed,
    # or the retry count is exceeded.
    #
    my $retry_count = 0;

    while ($retry_count <= $job_set_max_retries && $JOB_SET_DONT_RETRY == JGI_FLAG_OFF)
    {
	if (&$run_job_set_method_ref(@job_set_method_params) != JGI_SUCCESS)
	{
	    #
	    # One or more commands failed, log an error, increment the
	    # retry counter and try again.
	    # Note that the skip completed jobs flag is automatically
	    # toggled on after each attempt, no matter what its
	    # initial setting was.
	    #
	    $log->error("Job set run attempt " . $retry_count . " failed! " .
			"($job_set_run_type)");
	    $failed_job_flag = JGI_FLAG_ON;
	    $job_set_skip_completed = JGI_FLAG_ON;
	    $retry_count++;
	}
	else
	{
	    #
	    # All of the jobs succeeded, so break out of the retry
	    # loop.
	    #
	    $failed_job_flag = JGI_FLAG_OFF;
	    last;
	}
    }

    #
    # If, after all retries, there are still failed jobs,  exit here
    #
    $total_time = Time::HiRes::tv_interval($start_time);
    if ($failed_job_flag eq JGI_FLAG_ON)
    {
	$log->error("Job set failed! ($job_set_run_type) " .
		   "($total_time seconds)");
	return JGI_FAILURE;
    }
    else
    {
	#
	# Unless running locally, remove any temporary job set files.
	#
	unless ($job_set_run_type eq JOB_SET_RUN_TYPE_LOCAL)
	{
	    if (cleanup($job_script_files_dir ,$touchfiles_dir) != JGI_SUCCESS)
	    {
		$log->warn("Unable to clean up the temporary job set " .
			   "files!");
	    }
	}
	    
	$log->debug("Successfully ran the job set. ($job_set_run_type) " .
		   "($total_time seconds)\n");


	return JGI_SUCCESS;
    }

} # End of the run_job_set method.




=head2 run_job_set_local
 Title    : run_job_set_local
 Function : This method runs the job set locally using the provided job parameters. 
 Usage    : run_job_set_local( )
 Args     : 1) a logging object reference
            2) a reference to a set of commands to run 
            3) a reference to a set of stderr files to be created
            4) a reference to a set of stdout files to be created
            5) a reference to a set of execution directories where the commands will be executed by the system 
            6) a reference to a set of status strings to store current state of each process
            7) maximum number of processes to fork out n parallel  (NOTE: this is independent of any threading done by an individual process)
            8) a binary flag to skip processes deemed to be already completed  ( 0 to disable )
            9) a reference to a validation function that will check the outputs ( 0 to disable )

 Returns  : JGI_SUCCESS: The job set was successfully run.
            JGI_FAILURE: One or more of the jobs in the set could not be run.
 Comments : Please refer to the individual run_job_set_* functions to
            see the requirements for each job set run type.
=cut


sub run_job_set_local
{
    # Reference to the logging object
    my $log = shift;

    # References to the sets of commands, stdout files, stderr files, execution directories,
    # job status, plus user-set run-related parameters
    my ( $command_set_ref,  $stderr_set_ref, $stdout_set_ref, $execution_dir_set_ref, $status_set_ref,
	 $max_submit, $skip_completed_flag, $validation_function_ref) = @_;

    my $numJobs = scalar(@$command_set_ref);

    # Hard-coded run-related parameters
    my $blocking_flag = JGI_FLAG_ON;  # For now, one can only run in blocking mode.

    # References to arrays to hold the start and stop times for the
    # various commands.
    my ($start_time_set_ref, $stop_time_set_ref);

    # Respectively, an array to hold the child process IDs, and an
    # integer for indexing them.
    my (@process_ids, $process_id_index);

    # A given child process ID and its waitpid() return value.
    my ($process_id, $waitpid_return);

    # An integer for indexing the set of commands, and a given command,
    # execution directory, stdout file, stderr file, and status
    my ($command_index, $command, $execution_dir, $stdout_file,
	$stderr_file, $command_status);

    # The command for creating an execution directory,
    my $mkdir_cmd;

    # A hash for mapping process IDs to command indexes.
    my %process_id_cmd_map;

    # A reference to an array in which to store the various child process
    # return values, the index of a given entry in it, a given return value,
    # and a flag to indicate if any commands have failed.
    my ($return_value_set_ref, $return_value_index, $return_value,
	$failed_job_flag);

    # A counter for the number of validated process completions
    my $numValidations = 0;

    # A string to identify this method.
    my $function_id = "run_job_set_local";


    #
    # If there's only one local command to run, just call the system
    # command wrapper directly.
    #
    if ($numJobs == 1)
    {

	#
	# Construct and run the single local command.
	#
	$command = "cd $$execution_dir_set_ref[0]; $$command_set_ref[0] " .
	    "> $$stdout_set_ref[0] 2> $$stderr_set_ref[0]";
	$$start_time_set_ref[0] = [Time::HiRes::gettimeofday()];
	if (run_local_cmd($command, \$return_value, $log) != JGI_SUCCESS)
	{
	    #
	    # Store the return value, log an error, and return failure.
	    #
	    $$return_value_set_ref[0] = $return_value;
	    $$stop_time_set_ref[0] = [Time::HiRes::gettimeofday()];
	    $$status_set_ref[0] = JOB_SET_STATUS_FAILED;
	    $log->error("Local command returned a non-zero exit status! Check stderr outputs for more clues!\n " .
			"Return value " .
			"($$return_value_set_ref[0]) " .
			"Command ($$command_set_ref[0])");
	    return JGI_FAILURE;
	}
	else
	{
	    #
	    # Since there was only one command to run, store the return
	    # value and return success.
	    #
	    if ( ! $validation_function_ref || &$validation_function_ref( $$stdout_set_ref[0], $log ) == JGI_SUCCESS )
	    {
		$$return_value_set_ref[0] = $return_value;
		$$stop_time_set_ref[0] = [Time::HiRes::gettimeofday()];
		$$status_set_ref[0] = JOB_SET_STATUS_COMPLETED;
		return JGI_SUCCESS;
	    }
	    else
	    {
		$log->error( "Unable to validate stdout  for the command! " .
			     "Command: ($$command_set_ref[0])");
		return JGI_FAILURE;
	    }
	}
    } # End of the single local command block.



    #
    # Initialize sundry variables.
    #
    $command_index = 0;
    $failed_job_flag = JGI_FLAG_OFF;

    #
    # Cycle through the set of commands, launching each in turn,
    # subject to the limit of the maximum that can run at once.
    #
    while (1)
    {
	#
	# Initialize sundry variables.
	#
	$process_id_index = 0;

	#
	# Check for any empty slots in the set of currently running 
	# child processes.
	#
	while ($process_id_index < $max_submit)
	{
	    undef($waitpid_return);
	    if ((!defined($process_ids[$process_id_index])) ||
		(($waitpid_return = waitpid($process_ids[$process_id_index],
					    WNOHANG))
		 == $process_ids[$process_id_index]))
	    {
		#
		# Store the child process return value.
		#
		$return_value = $?;
		if (defined($process_ids[$process_id_index]))
		{
		    $return_value_index =
			$process_id_cmd_map{$process_id_index};
		    $$return_value_set_ref[$return_value_index] =
			$return_value;
		    $$stop_time_set_ref[$return_value_index] =
			[Time::HiRes::gettimeofday()];
		    $process_ids[$process_id_index] = undef();
		}

		#
		# Either the current child process slot has yet to have
		# anything assigned to it, or the previously-assigned one
		# has finished.  So it's time to launch the next command
		# in the set.
		#
		if ($command_index > $#$command_set_ref)
		{
		    #
		    # There are no more commands to run, so break out of
		    # the command-launching loop.
		    #
		    goto ALL_LOCAL_COMMANDS_LAUNCHED;
		}
		else
		{
		    #
		    # Retrieve the information for the next command.
		    #
		    $command = $$command_set_ref[$command_index];
		    $execution_dir = $$execution_dir_set_ref[$command_index];
		    $stdout_file = $$stdout_set_ref[$command_index];
		    $stderr_file = $$stderr_set_ref[$command_index];
		    $command_status = $$status_set_ref[$command_index];

		    #
		    # If the command has already been successfully run
		    # as evident by the JOB_SET_STATUS_COMPLETED flag
		    # (i.e. this is an automatic retry of a partially
		    # failed job set), and the skip completed jobs flag is set,
		    # move on to the next one.  Otherwise, construct
		    # the full version of it.
		    if (($skip_completed_flag eq JGI_FLAG_ON) && ($command_status eq JOB_SET_STATUS_COMPLETED))  
		    {
			$log->debug("Skipping job $command_index ..");
			$command_index++;
			next;
		    }
		    else
		    {
			$command = "cd $execution_dir; " .
			    "$command > $stdout_file 2> $stderr_file";
		    }

		    #
		    # Fork off a new process to run the current command.
		    #
		    if ($process_id = fork())
		    {
			#
			# The parent process.  Record the process ID
			# in the appropriate array entry, increment the
			# number of launched jobs, note the start time,
			# and update the status.
			#
			$process_ids[$process_id_index] = $process_id;
			$process_id_cmd_map{$process_id_index} =
			    $command_index;
			$$start_time_set_ref[$command_index] =
			    [Time::HiRes::gettimeofday()];
			$$status_set_ref[$command_index] =  JOB_SET_STATUS_RUNNING;
			$command_index++;
		    }
		    else
		    {
			#
			# The child process.  Check
			# done to see if the fork failed.
			#
			if ($process_id < 0)
			{
			    $log->error("Could not fork a child process! " .
					"($command)");
			    exit(JGI_FAILURE);
			}

			#
			# Excecute the current command.
			#
			$log->debug("child: $command");
			unless (exec($command))
			{
			    #
			    # If one gets here, something must have gone very
			    # wrong, so generate an error and exit.
			    # The error is written to stderr, as the use of
			    # the logging object in the child process
			    # runs the risk of causing problems.
			    #
			    print STDERR "exec() error!\n" .
				"Command:\t$command\n";
			    $log->error("exec() error! ($command)");
			    exit(JGI_FAILURE);
			}
		    }
		}
	    }

	    #
	    # Move on to the next entry of the process IDs array.
	    #
	    $process_id_index++;
	}

	#
	# Sleep  before trying to submit additional jobs.
	#
	sleep(10);
    }

    #
    # A label to jump to when all of the commands have been launched.
    #
  ALL_LOCAL_COMMANDS_LAUNCHED:
    #
    # If the user requested that one wait for all of the commands to
    # finish before returning, enter a monitoring loop.
    #
    if ($blocking_flag eq JGI_FLAG_ON)
    {
	$process_id_index = 0;
	while ($process_id_index < $max_submit)
	{
	    #
	    # If there's a child process in the current slot, wait for it
	    # to finish.
	    #
	    if (defined($process_ids[$process_id_index]))
	    {
		$process_id = $process_ids[$process_id_index];
		$waitpid_return = waitpid($process_id, 0);
		$return_value = $?;
		
		if (($waitpid_return != $process_id) && 
		    ($waitpid_return != -1))
		{
		    $log->error("waitpid() returned an unexpected value! " .
				"(Return Value: ($waitpid_return)) " .
				"(Process ID:\ ($process_id))");
		}

		#
		# Store the child process return value.
		#
		$return_value_index = $process_id_cmd_map{$process_id_index};
		$$return_value_set_ref[$return_value_index] = $return_value;
		$$stop_time_set_ref[$return_value_index] =
		    [Time::HiRes::gettimeofday()];
		$process_ids[$process_id_index] = undef();
	    }
	    
	    #
	    # Move on to the next child process.
	    #
	    $process_id_index++;
	}
    }

    #
    # Log any failed jobs.
    #
    $command_index = 0;
    while ($command_index < $numJobs)
    {
	#
	# If the command has already been successfully run
	# as evident by the JOB_SET_STATUS_COMPLETED flag
	# (i.e. this was an automatic retry of a partially
	# failed job set) and the SKIP_COMPLETED flag is set, 
        # move on to the next one.
	if ( ($skip_completed_flag eq JGI_FLAG_ON) &&  ($$status_set_ref[$command_index] eq JOB_SET_STATUS_COMPLETED) ) 
	{
	    $numValidations++;
	    next;
	}

	#
	# Otherwise, see if the command failed.
	#
	if ($$return_value_set_ref[$command_index])
	{
	    $$status_set_ref[$command_index] = JOB_SET_STATUS_FAILED;
	    $log->error("Local command returned a non-zero exit status! Check stderr outputs for more clues!\n " .
			"Job ($command_index) " .
			"Return value " .
			"($$return_value_set_ref[$command_index]) " .
			"Command ($$command_set_ref[$command_index])");
	    $failed_job_flag = JGI_FLAG_ON;
	}
	else
	{
	    # validate that the expected stdout files exist are are not empty (the validation function should be used only when the output is going to stdout)
	    if ( ! $validation_function_ref || &$validation_function_ref( $stdout_set_ref->[ $command_index ], $log ) == JGI_SUCCESS )
	    {
		$$status_set_ref[$command_index] = JOB_SET_STATUS_COMPLETED;
		$numValidations++;
	    }
	    else
	    {
		$$status_set_ref[$command_index] = JOB_SET_STATUS_FAILED;
		$log->error( "Unable to validate stdout and stderr for the command! The command ran to completion but didn't produce the expected output. " .
			     "Job: ($command_index) " .
			     "Command: ($$command_set_ref[$command_index])");
	    }
	}
    }
    continue
    {
	#
	# Move on to the next command.
	#
	$command_index++;
    }
    
    #
    # What to return depends on whether there were any failed jobs.
    #
    if ($failed_job_flag eq JGI_FLAG_ON)
    {
	return JGI_FAILURE;
    }
    else
    {
	if ( $validation_function_ref ) 
	{	    
	    $log->debug( "$numValidations validations out of $numJobs jobs" );
	    if ( $numValidations < $numJobs)
	    {
		$log->error( "Not all jobs validated!" );
		return JGI_FAILURE;
	    }
	}
	return JGI_SUCCESS;

    }
} # End of the run_job_set_local method.




=head2 run_job_set_array_cluster

 Title    : run_job_set_array_cluster
 Function : This methods runs the set of commands stored in the object on an SGE-like cluster, using the stored job parameters.
            NOTE: Since it is more likely for some but not all of the array tasks to fail (e.g. due to unbalanced load or unequal
            resources across the nodes), here we rely on touchfiles to signal a successful task completion, which can be used in
            subsequent attempts to skip already completed tasks.  The creation of the touchfiles is done by the individual task 
            scripts themselves rather than relying on exit values returned by the scheduler. This is different from 
            run_job_set_local we rely directly on command exit values and don't write any touchfiles.

 Usage    : run_job_set_array_cluster( args )
 Args     : 1) a logging object reference
            2) a reference to a set of commands to run ( will be wrapped into job scripts )
            3) a reference to a set of stderr files to be created
            4) a reference to a set of stdout files to be created
            5) a reference to a set of execution directories where the commands will be executed by the system 
            6) a reference to a set of job script names will be farmed off to the cluster
            7) a reference to a set of touchfiles that will be created upon successfull command execution
            8) a reference to a validation function that will check the outputs ( 0 to disable )
            9) maximum number of jobs to submit at once  (currently not used)
           10) a binary flag to skip any already-completed commands (1=on, 0=off)
           11) a template file that will contain expressions to define job script file/link names using the $[SGE|SLURM]_TASK_ID variable
           12) a directory with links to script files
           13) miscelaneous submitter options for this job set

 Returns  : JGI_SUCCESS: The job set was successfully run on the
                         cluster.
            JGI_FAILURE: One or more of the stored jobs could not be
                         run on the cluster.
 Comments : This method is intended to be called internally.
            This method assumes that  all needed parameters are defined.

=cut

sub run_job_set_array_cluster
{

    # Reference to the logging object
    my $log = $_[0];

    # References to the sets of commands, stdout files, stderr files, and  execution directories
    my ( $command_set_ref,  $stderr_set_ref, $stdout_set_ref, $execution_dir_set_ref ) = @_[1..4];

    # References to the sets of job scripts, links, touchfiles, and other variables used
    # for preparing and submitting jobs to the cluster
    my ( $job_script_set_ref, $touchfile_set_ref, $validation_function_ref,
         $job_set_max_submit, $job_set_skip_completed,
	 $linked_script_template, $linked_script_files_dir, $job_submit_misc_options) = @_[5..12];

    # A string to identify this function.
    my $function_id = "run_job_set_array_cluster";


    # Check if any of the outputs for this job set are already present (from previous run attempts)
    # and validate them.  

    my $numJobsInTotal = scalar( @{$command_set_ref} );
    my $numLinkfiles = 0;
    my @job_script_idxs_to_run;

    for ( my $i = 0; $i < $numJobsInTotal; $i++ )
    {
	# Unless evidence of successful job script completion exists AND we specified that we want to skip those, add this script id to the list
	unless ( -e $touchfile_set_ref->[ $i ]  &&  ( ! $validation_function_ref || &$validation_function_ref( $stdout_set_ref->[ $i ], $log ) == JGI_SUCCESS  )  &&  $job_set_skip_completed == JGI_FLAG_ON )   
	{
	    push @job_script_idxs_to_run, $i;
	    
	    $numLinkfiles++;
	}
    }
    if (! @job_script_idxs_to_run ) 
    {
      $log->debug( "Current job set already has all the outputs in place.  Moving on." );      
      return (JGI_SUCCESS);
    }  

   
    # Create job scripts for the enitre set. Liks to specific scripts to execute are created later
    for ( my $i=0; $i < scalar( @$command_set_ref ); $i++ )
    {
	my $job_script_file = $job_script_set_ref->[ $i ];
	my $touchfile = $touchfile_set_ref->[ $i ];
	my $execution_dir = $execution_dir_set_ref->[ $i ];
	my $command = $command_set_ref->[ $i ];
	my $stdout = $stdout_set_ref->[ $i ];
	my $stderr = $stderr_set_ref->[ $i ];

	if (!open(JOB_SCRIPT,">$job_script_file"))
	{
	    $log->error("Unable to open the job script file for writing! " .
			"($job_script_file");
	    return JGI_FAILURE;
	}

	print JOB_SCRIPT "set -o pipefail\n";
	print JOB_SCRIPT "cd $execution_dir; ";
	print JOB_SCRIPT "$command";
	if ($stdout) { 
	    print JOB_SCRIPT " > $stdout";
	}
	if ($stderr) { 
	    print JOB_SCRIPT " 2> $stderr";
	}
	print JOB_SCRIPT "\n";
	
	print JOB_SCRIPT
	    "if [ \$? -eq 0 ]; then\n" .
	    "\ttouch $touchfile \n" .
	    "\techo \${".$JOB_ID_ENV_VAR_NAME."}-\${".$TASK_ID_ENV_VAR_NAME."} > $touchfile \n" .
	    "\texit 0\n" .
	    "else\n" .
	    "\techo \"\${".$JOB_ID_ENV_VAR_NAME."}-\${".$TASK_ID_ENV_VAR_NAME."}: \$0: FAILED with status \$? \" >&2 \n" .
	    "\texit -1\n" .
	    "fi\n";
	
	if (!close(JOB_SCRIPT))
	{
	    $log->warn("Unable to close the job script file! " .
		       "($job_script_file)");
	}
    }

    # create linked script template
    if ( !open ( LINKED_SCRIPT_TEMPLATE, ">$linked_script_template" ) )
    {
	$log->error( "Couldn't open $linked_script_template for writing" );
	return JGI_FAILURE;
    }
    print LINKED_SCRIPT_TEMPLATE "#!/bin/bash\n".
	"TASK_ID=`expr \$".$TASK_ID_ENV_VAR_NAME." - 1`; subdir=\$((\${TASK_ID}/100)); bash $linked_script_files_dir/\${subdir}/".$JOB_SET_DEFAULT_FILE_PREFIX."\${TASK_ID}.sh\n";
    close LINKED_SCRIPT_TEMPLATE;
    ### Prep work finished

    #### Run 
    #   Note: A single user batch of jobs => a single
    #   set of job scripts/touchfiles/stdout/stderr, while multiple passes of varying subsets can
    #   be run/rerun within each user batch depending on errors.  These multiple passes all share the same
    #   job scripts and touchfiles, but vary in their linkfiles. 

    # performs a single pass for a given subset of jobScriptIdxs 
    my ($assessments_ref, $killedCnt) = 
	submit_and_monitor_and_assess_linkfile_jobs( $job_submit_misc_options,
						     $job_script_set_ref,
						     \@job_script_idxs_to_run,
						     $linked_script_files_dir,
						     $linked_script_template,
						     $stdout_set_ref,
						     $stderr_set_ref,
						     $touchfile_set_ref,
						     $validation_function_ref,
						     $log );

    if ($assessments_ref eq JGI_FAILURE) {return JGI_FAILURE;}
    my @assessments = @$assessments_ref;

    my $numTouchFilesFound = 0;
    my $numValidations = 0;

    for ( my $i = 0; $i < $numLinkfiles; $i++ )
    {
	if ( $assessments[ $i ] =~ /t/ )
	{
	    $numTouchFilesFound++;
	}
	if ( $assessments[ $i ] =~ /v/ )
	{
	    $numValidations++;
	}
    }
    if ($killedCnt > 0 )  # the overall number of tasks in this array that exited with staus 143
    {
	$log->debug( "Out of the $numLinkfiles jobs submitted, $killedCnt jobs were killed by the scheduler. Will not attempt to retry this job set!" );
	$JOB_SET_DONT_RETRY = JGI_FLAG_ON;
    }

    if ($validation_function_ref) 
    {
	$log->debug( "$numValidations validated outputs out of $numLinkfiles jobs" );
	if ( $numValidations != $numLinkfiles ) 
	{
	    $log->error( "Not all jobs validated !" );
	    return( JGI_FAILURE );
	}
    }
    
    $log->debug( "$numTouchFilesFound touchfiles out of $numLinkfiles jobs" );
    if ( $numTouchFilesFound != $numLinkfiles )
    {
	$log->error( "Not all jobs have a touchfile !" );
	return( JGI_FAILURE );
    }

    return( JGI_SUCCESS );
}    


=head2 submit_and_monitor_and_assess_linkfile_jobs
 Title    : submit_and_monitor_and_assess_linkfile_jobs
 Function : This method runs the job set locally using the provided job parameters. 
 Args     : 1)  a string containing submitter parameters and their arguments
            2)  a reference to the complete set of job script file names   (all or just some of these will actually be executed)
            3)  a reference to a set of indexes corresponding to the specific jobs to run  (will be used to draw from the total pool specified in #2 )
            4)  a full path to a directory where the executables (or symlinks to them) will be stored
            5)  a template script containing expressions to define job script file/link names using the $[SGE|SLURM]_TASK_ID variable
            6)  a reference to a set of stdout files that the jobs/tasks will create -  here used only for validation
            7)  a reference to a set of stderr files that the jobs/tasks will create -  here used for validation
            8)  a reference to a set of touchfiles that will be created upon each job/task -  here used for validation
            9)  a reference to a validation function that will verify that the expected output are in place ( 0 to disable )
           10)  a logging object reference

    
 Returns  : JGI_SUCCESS: The job set was successfully run.
            JGI_FAILURE: One or more of the jobs in the set could not be run.
=cut

sub submit_and_monitor_and_assess_linkfile_jobs
{
    my ( $submitter_options, $job_script_set_ref, 
    $job_script_idxs_to_run_ref, $linked_script_files_dir, $linked_script_template, $stdout_set_ref, 
    $stderr_set_ref, $touchfile_set_ref, $validation_function_ref, $log  ) = @_;

    my $submitter_script = $ENV{'MERACULOUS_ROOT'} . "/bin/cluster_submit.sh";
    my $isjobcomplete    = $ENV{'MERACULOUS_ROOT'} . "/bin/isjobcomplete.sh";

    my $remote_job_sentinel = JOB_SET_REMOTE_SENTINEL;
    my $clusterDeleteJobBin = CLUSTER_DELETE_JOB_EXEC;

    my $num_linkfiles = scalar( @$job_script_idxs_to_run_ref );
    $submitter_options .=" -t $num_linkfiles"; 

    my $MAX_QSTAT_SUCCESSIVE_RETRIES = 10;

    my $date = `date +%Y%m%d-%H%M%S`;
    chomp($date);


  # prepare linkFiles
  # prepare linked script files for current job set
  my $linked_script_set_ref = [];
    if ( -d $linked_script_files_dir ) 
    {
	run_local_cmd( "rm -r $linked_script_files_dir", undef, $log );
    }
    create_file_dir_structure( $linked_script_files_dir, $num_linkfiles, 1, 
     $JOB_SET_DEFAULT_FILES_PER_SUBDIRECTORY, $JOB_SET_DEFAULT_FILE_PREFIX, QC_SCRIPT_SUFFIX, $linked_script_set_ref, $log );

  # actually link the files
  my $linkI = 0;
  for( my $i = 0 ; $i < scalar( @$job_script_idxs_to_run_ref ); $i++ )
  {
    if ( system("ln -s ".$job_script_set_ref->[ $job_script_idxs_to_run_ref->[ $i ] ]." ".$linked_script_set_ref->[ $linkI ]) != 0 )
    {
	$log->error( "Couldn't create link file for command $i:  $!" );
	return JGI_FAILURE;

    }
    $linkI++;
  }

    # submit the job via the generic cluster submitter script

    my $submiter_err = $linked_script_template.".submit.${date}.err";
    my $cmd = "$submitter_script $submitter_options $linked_script_template 2> $submiter_err ";

    my $submit_result;
    my $return_value;
    if ( JGI_SUCCESS != run_local_cmd( $cmd, \$return_value, $log, \$submit_result ) )    
    {
	$log->error("Unable to submit the array job to the cluster! ($return_value)" );
	return JGI_FAILURE;
    }
    my $jobId;
    if ( $submit_result =~ /^(\d+)/)   # the submitter script should print just the job id string ( could contain task id extension)
    {
	$jobId = $1;
	$log->info( "Submitted jobID: $jobId" );
	open (SENTINEL, ">$remote_job_sentinel");
	print SENTINEL "$jobId";
	close SENTINEL
    }
    else
    {
	$log->error( "No valid JobId was returned from the submitter script: $submit_result" );
	return JGI_FAILURE;
    }

    $log->debug( "Job $jobId is queued/running.." );

    $cmd = "$isjobcomplete $jobId > /dev/null";
    while ( (system($cmd) >> 8) == 1)  #the real return value is offet by 8 bytes
       # not using run_local_cmd() here because the different interpretation of 0 and 1 exit codes
    {
	sleep( 10 );
    }
    system("rm -f ./$remote_job_sentinel");

    # now catch any exceptions; isjobcomplete should return 0 when the job is no longer in the queue (and we already handled 1 )
    # - any other status means there was a problem
    if ($? == -1) 
    {
	$log->error("command \"$cmd\" failed to execute: $!");
	`$clusterDeleteJobBin $jobId`;
	return JGI_FAILURE;
    }
    $return_value = $? >> 8;  
    if ( $return_value != 0 ) 
    {
	$log->error("command \"$cmd\" exited with status $return_value: $!");
	`$clusterDeleteJobBin $jobId`;
	return JGI_FAILURE;
    }


    $log->info( "done processing array job $jobId" );
    # give time for qacct logs to update
    sleep( 10 );  
    

    my $killedCnt = 0;
    if (defined $ENV{'SGE_ROOT'}) {
	if (`qqacct`) {  # qqacct is installed on this system, so use it
	    # log the qacct for the job post-mortem
	    $cmd = "qqacct -q \'job_number== $jobId\' -c job_number,task_number,hostname,\'datetime(start)\',\'datetime(end)\',\'memory(maxvmem,\"G\")\',wallclock,failed,exit_status";

	    if ( JGI_SUCCESS != run_local_cmd( $cmd, \$return_value, $log, \$submit_result ) )    
	    {
		$log->error("Unable to get qacct job info for $jobId!  ($return_value)" );	
		return JGI_FAILURE;
	    }
	    
	    # parse the qqacct output and flag any tasks that had a non-zero exit status.
	    $log->debug("job_number,task_number,hostname,start_time,end_time,maxvmem,wallclock,failed,exit_status");
	    my @lines = split /\n/, $submit_result;
	    foreach my $line (@lines) {
		$log->debug("$line");
		my @fields= split(/,/, $line);
		if ($fields[7] != 0) {
		    $log->debug( "FAILED ARRAY TASK $fields[0]:$fields[1]" );
		    if ($fields[7] == 100 && $fields[8] == 143) {
			$log->debug( "Cause: killed by the scheduler on the cluster (exit status $fields[8]);  Check walltime and memory requirements for this stage!" );
			$killedCnt++;
		    }
		    else  {
			$log->debug( "Cause: unknown (exit status $fields[8]); Check task outputs!" );
		    }
		}
	    }
	}
	else {
	    $log->debug( "WARNING: qqacct was not found on your system. Install it to see details about this job submission." );
	}
    }	
    elsif (defined $ENV{'SLURM_ROOT'}) {
	$cmd = "sacct -j $jobId  --format JobID,JobName,NodeList,Start,End,ReqMem,MaxRSS,Elapsed,State,ExitCode";
	if ( JGI_SUCCESS != run_local_cmd( $cmd, \$return_value, $log, \$submit_result ) )
	{
	    $log->error("Unable to get sacct job info for $jobId!  ($return_value)" );
	    return JGI_FAILURE;
	}
	
	# parse the sacct output 
	$log->debug("\n$submit_result");

	my @lines = split /\n/, $submit_result;
	for (my $i=0; $i<=$#lines; $i++) { 
	    next if ($i<2); #skip header
	    my $line = $lines[$i];
	    my @fields= split(/\s+/, $line);

	    my $jobName = $fields[1];
	    my $state = $fields[-2];
	    if ($state eq 'CANCELED' || $state eq 'FAILED') {
		$log->debug( "FAILED ARRAY TASK $fields[0]" );
		my $exitCodes = $fields[-1];
		my ($exitCode,$termSignal) = split(/:/, $exitCodes);
		
		if ($termSignal == 9) {
		    $log->debug( "Cause: task terminated by the system (term signal $termSignal);  Check memory requirements for this stage!" );
		}
		elsif ($termSignal == 15) {
		    $log->debug( "Cause: task killed by the cluster scheduler (term signal $termSignal);  Check walltime requirements for this stage!" );
		}
		else  {
		    $log->debug( "Cause: task killed externally - reason unknown (term signal $termSignal); Check task outputs for more qlues!" );
		}
		$killedCnt++;		
	    }
	    elsif ($state eq 'TIMEOUT') {
		$log->debug( "Cause: process timed out." );		    
	    }
	}
    }

  my @assessment;

  # validate the tasks output
  for ( my $i = 0; $i < $num_linkfiles; $i++ ) 
  {
    my $task_id = $i+1;

    # check that each job script that was run in this attempt (as determined by the link files) has a corresponding touchfile  
    if ( -f $touchfile_set_ref->[ $job_script_idxs_to_run_ref->[ $i ] ] )
    {
	open(F,"$touchfile_set_ref->[ $job_script_idxs_to_run_ref->[ $i ] ]");
	my @touchfile=<F>;close F;
	if (grep /^${jobId}-${task_id}$/, @touchfile)      # this touchfile is indeed from the right job array and wasn't created by some other runaway job
	{
	    $assessment[ $i ] = "t";
	}
	else 
	{
	    $log->error( "Error: $touchfile_set_ref->[ $job_script_idxs_to_run_ref->[ $i ] ] doesn't have the stamp of its parent job's id in it. This could happen if another job/task executed the same job script and overwrote the touchfiles, which could be disasterous!  Exiting here." );
	    return JGI_FAILURE;
	}
    }
    else
    {
	$assessment[ $i ] = "";
    }
    
    # validate that the outputs exist are are not empty
    if ( $validation_function_ref &&  &$validation_function_ref( $stdout_set_ref->[ $i ], $log ) == JGI_SUCCESS )
    {
	$assessment[ $i ] .= "v";
    }

  }
    return( \@assessment, $killedCnt )
}


=head2 cleanup

 Title    : cleanup
 Function : This method cleans up the myriad temporary files created
            while running a set of jobs.
 Usage    : $job_set->cleanup( )
 Args     : 1) A reference to the object on which the method is being
               called.
 Returns  : JGI_SUCCESS: The temporary job set files were successfully
                         cleaned up.
            JGI_FAILURE: The temporary job set files could not be cleaned
                         up.
 Comments : This method is intended to be called internally.

=cut

sub cleanup
{
    # Please refer to the above function description for details of
    # what these variables are.

    my ($job_script_files_dir, $touchfiles_dir) = @_;

    # A command for removing a directory, and the return value from
    # running it.
    my ($rmdir_cmd, $return_value);

    # A string to identify this function.
    my $function_id = "cleanup";


    #
    # If necessary, delete the job script directory.
    #
    if (defined($job_script_files_dir) && ( -d $job_script_files_dir))
    {
	$log->debug("Deleting the job script file directory!");
	$rmdir_cmd = "rm -rf $job_script_files_dir";
	if (run_local_cmd($rmdir_cmd, \$return_value, $log)
	    != JGI_SUCCESS)
	{
	    $log->warn("Unable to delete the job script file directory!");
	}
    }

    #
    # If necessary, delete the touchfile directory.
    #
    if (defined($touchfiles_dir) && ( -d $touchfiles_dir))
    {
	$log->debug("Deleting the touchfile directory!");
	$rmdir_cmd = "rm -rf $touchfiles_dir";
	if (run_local_cmd($rmdir_cmd, \$return_value, $log)
	    != JGI_SUCCESS)
	{
	    $log->warn("Unable to delete the touchfile directory!");
	}
    }

    #
    # If one gets here, everything must have worked (or at least not
    # failed too badly :), so return success.
    #
    return JGI_SUCCESS;
} # End of the cleanup method.





#
# A seemingly gratuitous statement, included so that the interpreter
# doesn't complain.
#

1;
