#
# M_Constants.pm
#
# This module contains the constants used by Meraculous.
# Constants will be packaged by groups.
#
# This module exports the following functions:
#
# <Insert a list of exported functions, along with brief summaries
# of what they do.>
#
# Initial Version:     5/22/2006
# Latest Revision:     10/10/2013
# Author:              Harris Shpiro, Eugene Goltsman
#




#####################################################################
# Module Header                                                     #
#####################################################################

#
# Some boilerplate statements.
#
package M_Constants;

use Exporter;
@ISA = qw(Exporter);

%EXPORT_TAGS = (
		Return_values => [qw( JGI_SUCCESS
				      JGI_FAILURE
				      JGI_UNDEFINED
				      JGI_FLAG_ON
				      JGI_FLAG_OFF
                                      JGI_MSG_ERROR
                                      JGI_MSG_LOG
                                      JGI_MSG_WARNING
				      )],

		Suffixes      => [qw( QC_CHECKPOINT_SUFFIX
				      QC_SCRIPT_SUFFIX
				      QC_STDOUT_SUFFIX
				      QC_STDERR_SUFFIX
				      QC_TOUCHFILE_SUFFIX
				      )],
		Gen_config    => [qw( CONFIG_KEY_TYPE
				      CONFIG_KEY_REQ
				      CONFIG_UNIQ_KEY
				      CONFIG_TRUE
				      CONFIG_FALSE
				      CONFIG_REQ
				      CONFIG_UNREQ
				      CONFIG_UNIQ
				      CONFIG_NONUNIQ
				      CONFIG_TYPE_STRING
				      CONFIG_TYPE_INT
				      CONFIG_TYPE_POS_INT
				      CONFIG_TYPE_FLOAT
				      CONFIG_TYPE_FILE
				      CONFIG_TYPE_DIR
				      CONFIG_TYPE_DATE
				      CONFIG_TYPE_LIST
				      CONFIG_TYPE_INT_LIST
				      CONFIG_TYPE_POS_INT_LIST
				      CONFIG_TYPE_FLOAT_LIST
				      CONFIG_TYPE_FREE
				      )],
                 User_params  => [qw( 
                                      USER_PARAM_genus               
                                      USER_PARAM_species             
                                      USER_PARAM_strain              
                                      USER_PARAM_use_cluster         
                                      USER_PARAM_cluster_queue                 
                                      USER_PARAM_cluster_project                 
                                      USER_PARAM_genome_size           
                                      USER_PARAM_mer_size           
                                      USER_PARAM_min_depth_cutoff
                                      USER_PARAM_mergraph_depth_pct_cutoff 
                                      USER_PARAM_lib_seq                
                                      USER_PARAM_num_prefix_blocks                
                                      USER_PARAM_min_mercount_to_report           
                                      USER_PARAM_cluster_submit_options        
                                      USER_PARAM_cluster_num_jobs 
                                      USER_PARAM_cluster_max_retries 
                                      USER_PARAM_cluster_slots_per_task 
                                      USER_PARAM_cluster_num_nodes 
                                      USER_PARAM_cluster_walltime 
                                      USER_PARAM_cluster_walltime_meraculous_import 
                                      USER_PARAM_cluster_walltime_meraculous_mercount 
                                      USER_PARAM_cluster_walltime_meraculous_mergraph 
                                      USER_PARAM_cluster_walltime_meraculous_ufx 
                                      USER_PARAM_cluster_walltime_meraculous_contigs 
                                      USER_PARAM_cluster_walltime_meraculous_bubble 
                                      USER_PARAM_cluster_walltime_meraculous_merblast 
                                      USER_PARAM_cluster_ram_request 
                                      USER_PARAM_cluster_ram_meraculous_import 
                                      USER_PARAM_cluster_ram_meraculous_mercount 
                                      USER_PARAM_cluster_ram_meraculous_mergraph 
                                      USER_PARAM_cluster_ram_meraculous_ufx 
                                      USER_PARAM_cluster_ram_meraculous_contigs 
                                      USER_PARAM_cluster_ram_meraculous_bubble 
                                      USER_PARAM_cluster_ram_meraculous_merblast 
                                      USER_PARAM_local_num_procs 
                                      USER_PARAM_local_max_memory 
                                      USER_PARAM_no_read_validation
                                      USER_PARAM_num_procs_meraculous_import 
                                      USER_PARAM_num_procs_meraculous_mercount 
                                      USER_PARAM_num_procs_meraculous_mergraph 
                                      USER_PARAM_num_procs_meraculous_ufx 
                                      USER_PARAM_num_procs_meraculous_contigs 
                                      USER_PARAM_num_procs_meraculous_merblast 
                                      USER_PARAM_num_procs_meraculous_bubble 
                                      USER_PARAM_local_max_retries 
                                      USER_PARAM_wiggle_room_for_bma 
                                      USER_PARAM_gap_close_aggressive 
                                      USER_PARAM_gap_close_rpt_depth_ratio 
                                      USER_PARAM_external_contigs_file 
                                      USER_PARAM_diploid_mode
                                      USER_PARAM_bubble_depth_threshold
                                      USER_PARAM_fallback_on_est_insert_size
                                      USER_PARAM_no_strict_haplotypes
                                      )],
                Local_params  => [qw(
                                      LOCAL_PARAM_autoselected_mer_size
                                      LOCAL_PARAM_autocalibrated_min_depth_cutoff 
                                      LOCAL_PARAM_autocalibrated_bubble_depth_threshold
                                      LOCAL_PARAM_local_resume_checkpoint 
                                      LOCAL_PARAM_prev_round_srf 
                                      LOCAL_PARAM_prev_round_max_insert
                                      LOCAL_PARAM_lib_insert_size_recalc
                                      LOCAL_PARAM_lib_qoffset
                                      )],
                Executables   => [qw( 
                                      PATH_SCREEN_LIST2 
                                      PATH_FASTASPLITTER 
                                      PATH_HISTOGRAM 
                                      PATH_ISJOBCOMPLETE
                                      PATH_KHA 
                                      PATH_N50 
                                      PATH_RANDOM_LIST2 
                                      PATH_MER2UFX 
                                      PATH_MERACULOUS_56 
                                      PATH_MERACULOUS_128 
                                      PATH_MERCOUNTER_56 
                                      PATH_MERCOUNTER_128 
                                      PATH_MERGRAPH_56 
                                      PATH_MERGRAPH_128 
                                      PATH_MERBLAST_56 
                                      PATH_MERBLAST_128 
                                      PATH_MERGE_BLASTS 
                                      PATH_BLASTMAPANALYZER 
                                      PATH_ONO 
                                      PATH_HAPLOTYPER
                                      PATH_MERAUDER 
                                      PATH_GAPDIVIDER 
                                      PATH_SCAFFOLD2CONTIG 
                                      PATH_FINDDMIN 
                                      PATH_LOAD_BALANCE_MERCOUNT 
                                      PATH_UNIQUE 
                                      PATH_MEMTIME 
                                      PATH_CONTIG_MER_DEPTH_56 
                                      PATH_CONTIG_MER_DEPTH_128 
                                      PATH_BUBBLE_SCOUT 
                                      PATH_BUBBLE_POPPER
                                      PATH_BMA_TO_LINKS 
                                      PATH_OPTIMIZE 
                                      PATH_SCAFF_REPORT_TO_FASTA 
                                      PATH_SPLIT_READS
                                      PATH_SPLINTER
                                      PATH_SPANNER
                                      PATH_GAP_PLACER
                                      )],
		);
# Export these by default
our @EXPORT  = (
    @{$EXPORT_TAGS { 'Return_values' }},
    @{$EXPORT_TAGS { 'Suffixes' }},
    @{$EXPORT_TAGS { 'User_params' }},
    @{$EXPORT_TAGS { 'Local_params' }},
    @{$EXPORT_TAGS { 'Executables' }},
    );
# Export these only upon explicit request
our @EXPORT_OK = @{ $EXPORT_TAGS { 'Gen_config' } };


#####################################################################
# Included Modules                                                  #
#####################################################################

#
# Enforce good programming style.
#
use strict;


    
#####################################################################
# Exported Constant Definitions                                     #
#####################################################################

# RETURN VALUE CONSTANTS
#
use constant JGI_SUCCESS => 0;
use constant JGI_FAILURE => 1;
use constant JGI_UNDEFINED => 2;

#
# Constants to represent the allowed states of flag variables.
#
use constant JGI_FLAG_OFF => 0;
use constant JGI_FLAG_ON => 1;
use constant JGI_MSG_ERROR => 0;
use constant JGI_MSG_LOG => 1;
use constant JGI_MSG_WARNING => 2;


# FILE SUFFIX CONSTANTS
#
use constant QC_CHECKPOINT_SUFFIX => ".ckpt";
use constant QC_SCRIPT_SUFFIX => ".sh";
use constant QC_STDOUT_SUFFIX => ".out";
use constant QC_STDERR_SUFFIX => ".err";
use constant QC_TOUCHFILE_SUFFIX => ".touchfile";


# GENERIC CONFIG TYPE CONSTANTS
#
use constant CONFIG_KEY_TYPE => "CONFIG_KEY_TYPE";
use constant CONFIG_KEY_REQ => "CONFIG_KEY_REQ";
use constant CONFIG_UNIQ_KEY => "CONFIG_UNIQ_KEY";
use constant CONFIG_TRUE => 1;
use constant CONFIG_FALSE => 0;
use constant CONFIG_REQ => "required";
use constant CONFIG_UNREQ => "optional";
use constant CONFIG_UNIQ => "uniq_key";
use constant CONFIG_NONUNIQ => "nonuniq_key";
use constant CONFIG_TYPE_STRING => "string";
use constant CONFIG_TYPE_INT => "int";
use constant CONFIG_TYPE_POS_INT => "pos_int";
use constant CONFIG_TYPE_FLOAT => "float";
use constant CONFIG_TYPE_FILE => "file";
use constant CONFIG_TYPE_DIR => "dir";
use constant CONFIG_TYPE_DATE => "date";
use constant CONFIG_TYPE_LIST => "list";
use constant CONFIG_TYPE_INT_LIST => "int_list";
use constant CONFIG_TYPE_POS_INT_LIST => "pos_int_list";
use constant CONFIG_TYPE_FLOAT_LIST => "float_list";
use constant CONFIG_TYPE_FREE => "free";



# USER PARAMETER CONSTANTS
#
use constant {
  USER_PARAM_genus               => 'genus',
  USER_PARAM_species             => 'species',
  USER_PARAM_strain              => 'strain',
  USER_PARAM_use_cluster         => 'use_cluster',
  USER_PARAM_cluster_queue                 => 'cluster_queue',
  USER_PARAM_cluster_project                 => 'cluster_project',
  USER_PARAM_genome_size           => 'genome_size',
  USER_PARAM_mer_size           => 'mer_size',
  USER_PARAM_min_depth_cutoff   => 'min_depth_cutoff',
  USER_PARAM_mergraph_depth_pct_cutoff   => 'mergraph_depth_pct_cutoff',
  USER_PARAM_lib_seq                => 'lib_seq',
  USER_PARAM_num_prefix_blocks                => 'num_prefix_blocks',
  USER_PARAM_min_mercount_to_report           => 'min_mercount_to_report',
  USER_PARAM_cluster_submit_options        => 'cluster_submit_options',
#  USER_PARAM_num_linking_pairs_cutoff => 'meraculous_num_linking_pairs_cutoff',
#  USER_PARAM_min_contig_size_for_scaffold_links => 'min_contig_size_for_scaffold_links',
  USER_PARAM_cluster_num_jobs => 'cluster_num_jobs',
  USER_PARAM_cluster_max_retries => 'cluster_max_retries',
  USER_PARAM_cluster_slots_per_task => 'cluster_slots_per_task',
  USER_PARAM_cluster_num_nodes => 'cluster_num_nodes',
  USER_PARAM_cluster_walltime => 'cluster_walltime',
  USER_PARAM_cluster_walltime_meraculous_import => 'cluster_walltime_meraculous_import',
  USER_PARAM_cluster_walltime_meraculous_mercount => 'cluster_walltime_meraculous_mercount',
  USER_PARAM_cluster_walltime_meraculous_mergraph => 'cluster_walltime_meraculous_mergraph',
  USER_PARAM_cluster_walltime_meraculous_ufx => 'cluster_walltime_meraculous_ufx',
  USER_PARAM_cluster_walltime_meraculous_contigs => 'cluster_walltime_meraculous_contigs',
  USER_PARAM_cluster_walltime_meraculous_bubble => 'cluster_walltime_meraculous_bubble',
  USER_PARAM_cluster_walltime_meraculous_merblast => 'cluster_walltime_meraculous_merblast',
  USER_PARAM_cluster_ram_request => 'cluster_ram_request',
  USER_PARAM_cluster_ram_meraculous_import => 'cluster_ram_meraculous_import',
  USER_PARAM_cluster_ram_meraculous_mercount => 'cluster_ram_meraculous_mercount',
  USER_PARAM_cluster_ram_meraculous_mergraph => 'cluster_ram_meraculous_mergraph',
  USER_PARAM_cluster_ram_meraculous_ufx => 'cluster_ram_meraculous_ufx',
  USER_PARAM_cluster_ram_meraculous_contigs => 'cluster_ram_meraculous_contigs',
  USER_PARAM_cluster_ram_meraculous_bubble => 'cluster_ram_meraculous_bubble',
  USER_PARAM_cluster_ram_meraculous_merblast => 'cluster_ram_meraculous_merblast',
  USER_PARAM_local_num_procs => 'local_num_procs',
  USER_PARAM_local_max_memory => 'local_max_memory',
  USER_PARAM_no_read_validation => 'no_read_validation',
  USER_PARAM_num_procs_meraculous_import => 'num_procs_meraculous_import',
  USER_PARAM_num_procs_meraculous_mercount => 'num_procs_meraculous_mercount',
  USER_PARAM_num_procs_meraculous_mergraph => 'num_procs_meraculous_mergraph',
  USER_PARAM_num_procs_meraculous_ufx => 'num_procs_meraculous_ufx',
  USER_PARAM_num_procs_meraculous_contigs => 'num_procs_meraculous_contigs',
  USER_PARAM_num_procs_meraculous_merblast => 'num_procs_meraculous_merblast',
  USER_PARAM_num_procs_meraculous_bubble => 'num_procs_meraculous_bubble',
  USER_PARAM_local_max_retries => 'local_max_retries',
  USER_PARAM_wiggle_room_for_bma => 'wiggle_room_for_bma',
  USER_PARAM_gap_close_aggressive => 'gap_close_aggressive',
  USER_PARAM_gap_close_rpt_depth_ratio => 'gap_close_rpt_depth_ratio',
  USER_PARAM_external_contigs_file => 'external_contigs_file',
#  USER_PARAM_min_qual_to_keep => 'min_qual_to_keep',
  USER_PARAM_diploid_mode => 'diploid_mode',
  USER_PARAM_bubble_depth_threshold => 'bubble_depth_threshold',
  USER_PARAM_fallback_on_est_insert_size => 'fallback_on_est_insert_size',
  USER_PARAM_no_strict_haplotypes => 'no_strict_haplotypes'
  
#  USER_PARAM_email_addr => "email_addr"
};


# INTERNAL PARAMETER CONSTANTS
#
use constant {
  LOCAL_PARAM_autoselected_mer_size => 'autoselected_mer_size', 
  LOCAL_PARAM_autocalibrated_min_depth_cutoff => 'autocalibrated_min_depth_cutoff',
  LOCAL_PARAM_autocalibrated_bubble_depth_threshold => 'autocalibrated_bubble_depth_threshold',
  LOCAL_PARAM_local_resume_checkpoint => 'local_resume_checkpoint',
  LOCAL_PARAM_prev_round_srf => 'prev_round_srf',
  LOCAL_PARAM_prev_round_max_insert => 'prev_round_max_insert',
  LOCAL_PARAM_lib_insert_size_recalc => 'lib_insert_size_recalc',
  LOCAL_PARAM_lib_qoffset => 'lib_qoffset',
};



# EXECUTABLES
#
use constant {
  PATH_SCREEN_LIST2 => 'bin/screen_list2.pl',
  PATH_FASTASPLITTER => 'bin/fasta_splitter.pl',
  PATH_HISTOGRAM => 'bin/histogram2.pl',
  PATH_ISJOBCOMPLETE => 'bin/isjobcomplete.sh',
  PATH_KHA => 'bin/kmerHistAnalyzer.pl',
  PATH_N50 => 'bin/N50.pl',
  PATH_RANDOM_LIST2 => 'bin/randomList2.pl',
  PATH_MER2UFX => 'bin/meraculous4h.pl',
  PATH_MERACULOUS_56 => 'bin/meraculousTh_56mer',
  PATH_MERACULOUS_128 => 'bin/meraculousTh_128mer',
  PATH_MERCOUNTER_56 => 'bin/merCounterTh_56mer',
  PATH_MERCOUNTER_128 => 'bin/merCounterTh_128mer',
  PATH_MERGRAPH_56 => 'bin/mergraphTh_56mer',
  PATH_MERGRAPH_128 => 'bin/mergraphTh_128mer',
  PATH_MERBLAST_56 => 'bin/merBlast_56mer',
  PATH_MERBLAST_128 => 'bin/merBlast_128mer',
  PATH_MERGE_BLASTS => 'bin/mergeMerBlasts',
  PATH_BLASTMAPANALYZER => 'bin/blastMapAnalyzer2.pl',
  PATH_ONO => 'bin/oNo7.pl',
  PATH_HAPLOTYPER => 'bin/haplotyper.pl',
  PATH_MERAUDER => 'bin/merauder',
  PATH_GAPDIVIDER => 'bin/gapDivider.pl',
  PATH_SCAFFOLD2CONTIG => 'bin/scaffold2contig.pl',
  PATH_FINDDMIN => 'bin/findDMin2.pl',
  PATH_LOAD_BALANCE_MERCOUNT => 'bin/loadBalanceMers.pl',
  PATH_UNIQUE => 'bin/unique.pl',
  PATH_MEMTIME => 'bin/memtime',
  PATH_CONTIG_MER_DEPTH_56 => 'bin/contigMerDepth_56mer',
  PATH_CONTIG_MER_DEPTH_128 => 'bin/contigMerDepth_128mer',
  PATH_BUBBLE_SCOUT => 'bin/bubbleScout.pl',
  PATH_BUBBLE_POPPER => 'bin/bubblePopper.pl',
  PATH_BMA_TO_LINKS => 'bin/bmaToLinks.pl',
  PATH_OPTIMIZE => 'bin/optimize2.pl',
  PATH_SCAFF_REPORT_TO_FASTA => 'bin/scaffReportToFasta.pl',
  PATH_SPLIT_READS => 'bin/split_and_validate_reads.pl',
  PATH_SPLINTER => 'bin/splinter.pl',
  PATH_SPANNER => 'bin/spanner.pl',
  PATH_GAP_PLACER => 'bin/gapPlacer.pl'
};


#####################################################################
# Local Constant Definitions                                        #
#####################################################################

# Include here any local-use (global) constant definitions.

# A string to identify this module.
my $SCRIPT_ID = "Constants.pm";

# A string to identify this module's version.
my $VERSION_ID = "Version 4/10/2013";

# A string to identify this module's author.
my $AUTHOR_ID = "H.Shapiro, E.Goltsman";



#####################################################################
# Exported Functions                                                #
#####################################################################

# Insert the definitions of exported functions here.



#####################################################################
# Local Functions                                                   #
#####################################################################

# Insert the definitions of local functions here.





#
# A seemingly gratuitous statement, included so that the interpreter
# doesn't complain.
#
1;

########################################################################
# Format Definitions                                                   #
########################################################################

# Place any output format specifiers here.
