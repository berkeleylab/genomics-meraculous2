use strict;

# perform diff on bv3 files
my %aValues = ();

my $keySeparator = "XXX";

if ( scalar( @ARGV ) != 4 ) 
{
  die "usage: <fileA> <ignoreFirstColInA> <fileB> <ignoreFirstColInB>\n";
}
my $fileA = $ARGV[0];
my $fileAIgnoreFirstCol = $ARGV[1];  # merblast has a dummy first column
my $fileB = $ARGV[2];
my $fileBIgnoreFirstCol = $ARGV[3];

open( A, $fileA ) || die;
open( B, $fileB ) || die;

my $cntDupsInA = 0;
my $cntDupsInB = 0;
my $cntA = 0;
my $cntB = 0;
my $cntShared = 0;
my $cntExactMatches = 0;
my $cntInAOnly = 0;
my $cntInBOnly = 0;
my $cntTotalDiff = 0;
my $cntTotalNetDiff = 0;
my $cntDiffQStart = 0;
my $cntDiffQStop = 0;
my $cntDiffSStart = 0;
my $cntDiffSStop = 0;

sub min
{
	my ( $x, $y ) = @_;
	return( $x < $y ? $x : $y );
}
# hash file A
while( <A> )
{
	$cntA++;

	my @a = split;
	if ( $fileAIgnoreFirstCol ) { splice @a, 1, 1; }

	my $key = $a[ 1 ].$keySeparator.$a[5].$keySeparator.$a[9];
	if ( !exists( $aValues{ $key } ) )
	{ 	
		$aValues{ $key } = $a[2].$keySeparator.$a[3].$keySeparator.$a[6].$keySeparator.$a[7];
	}
	else
	{
		$cntDupsInA++;
	}
}
print STDERR "processing b...\n";
my @b = "";
my $prevKey = "";
my @prevB = 0;
while( <B> )
{
	$cntB++;
	my @b = split;

	if ( $fileBIgnoreFirstCol ) { splice @b, 1, 1; }
	my $key = $b[1].$keySeparator.$b[5].$keySeparator.$b[9];
	if ( !( $key eq $prevKey ) && !( $prevKey eq "" ) )
	{	
		# process $prevKey

		if ( exists( $aValues{ $prevKey } ) )
		{
			my $aValue = $aValues{ $prevKey };

			my @a = split( /$keySeparator/, $aValue );
			$cntShared++;
			my $diffQStart = $prevB[2] - $a[0];
			my $diffQStop = $prevB[3] - $a[1];
			my $diffSStart = $prevB[6] - $a[2];
			my $diffSStop = $prevB[7] - $a[3];

			# compute the difference in coordinates
			my $thisDiff = ( abs( $a[0] - $prevB[2] ) + abs( $a[1] - $prevB[3] ) + abs( $a[2] - $prevB[6] ) +
				abs( $a[3] - $prevB[7] ) );

			if ( $thisDiff == 0 )
			{
				$cntExactMatches++;
			}
			else
			{
				print STDERR "!".$prevKey."\t".$a[0]." ".$a[1]." ".$a[2]." ".$a[3]."\t".$prevB[2]." ".$prevB[3]." ".$prevB[6]." ".$prevB[7]."\n";

				$cntTotalDiff += $thisDiff;
				$cntTotalNetDiff += $diffQStart + $diffQStop + $diffSStart + $diffSStop;

				$cntDiffQStart += $diffQStart;
				$cntDiffQStop += $diffQStop;
				$cntDiffSStart += $diffSStart;
				$cntDiffSStop += $diffSStop;
			}

			# 'mark' this key in the hash
			$aValues{ $prevKey } = "";
		}
		else
		{
			print STDERR ">".$prevKey."\t".$prevB[2]." ".$prevB[3]." ".$prevB[6]." ".$prevB[7]."\n";

			$cntInBOnly++;
		}
	
	}
	else
	{
		$cntDupsInB++;
	}
	$prevKey = $key;
    @prevB = @b;
}

# process the last key
if ( exists( $aValues{ $prevKey } ) )
{
	my $aValue = $aValues{ $prevKey };

	my @a = split( /$keySeparator/, $aValue );
	$cntShared++;

	# compute the difference in coordinates
	my $diffQStart = $prevB[2] - $a[0];
	my $diffQStop = $prevB[3] - $a[1];
	my $diffSStart = $prevB[6] - $a[2];
	my $diffSStop = $prevB[7] - $a[3];

	my $thisDiff = abs( $diffQStart ) + abs( $diffQStop ) + abs( $diffSStart ) + abs( $diffSStop );
	if ( $thisDiff == 0 )
	{
		$cntExactMatches++;
	}
	else
	{
		print STDERR "!".$prevKey."\t".$a[0]." ".$a[1]." ".$a[2]." ".$a[3]."\t".$prevB[2]." ".$prevB[3]." ".$prevB[6]." ".$prevB[7]."\n";
		$cntTotalDiff += $thisDiff;
		$cntDiffQStart += $diffQStart;
		$cntDiffQStop += $diffQStop;
		$cntDiffSStart += $diffSStart;
		$cntDiffSStop += $diffSStop;
	}

	# 'mark' this key in the hash
	$aValues{ $prevKey } = "";
}
else
{
	$cntInBOnly++;
}

foreach my $k ( keys( %aValues ) )
{
	my $v = $aValues{ $k };
	if ( !( $v eq "" ) )
	{
		$cntInAOnly++;
		print STDERR "<".$k."\t".$v."\n";
	}
}

print "File A: $ARGV[0]\n";
print "File B: $ARGV[2]\n";
print "Total alignments in A: $cntA\n";
print "Total alignments in B: $cntB\n";
print "Dups in A: $cntDupsInA\n";
print "Dups in B: $cntDupsInB\n";
print "Shared: $cntShared ";
printf "%.3f", 100.0 * ( $cntShared / min( $cntA, $cntB ) );
print "%\n";

print "-- Exact matches: $cntExactMatches ";
printf "%.2f", 100.0 * ( $cntExactMatches / $cntShared );
print "%\n";

my $cntInexactMatches = $cntShared - $cntExactMatches;
print "-- Average coordinate deltas in inexact shared matches: ";
printf "%.1f", ( $cntTotalDiff / $cntInexactMatches );
print "abs bp ";
printf "%.1f", ( $cntTotalNetDiff / $cntInexactMatches );
print "net bp [ ";
printf "%.1f %.1f %.1f %.1f",  $cntDiffQStart / $cntInexactMatches, $cntDiffQStop / $cntInexactMatches,
	$cntDiffSStart / $cntInexactMatches, $cntDiffSStop / $cntInexactMatches;
print " ]\n";

print "In A only: $cntInAOnly\n";
print "In B only: $cntInBOnly\n";
